<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-banner" xml:lang="cs">

  <info>
    <link type="guide" xref="login#appearance"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zobrazit doplňující informace na přihlašovací obrazovce.</desc>
  </info>

  <title>Zobrazení textové zprávy na přihlašovací obrazovce</title>

  <p>Na přihlašovací obrazovce můžete nechat zobrazovat doplňující text, jako třeba kontakt na podporu, nastavením klíčů GSettings <sys>org.gnome.login-screen.banner-message-enable</sys> a <sys>org.gnome.login-screen.banner-message-text</sys>.</p>

  <steps>
    <title>Aby se zobrazila textová zpráva na přihlašovací obrazovce:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item><p>Vytvořte v <file its:translate="no">/etc/dconf/db/gdm.d/01-banner-message</file> soubor s klíči <sys>gdm</sys> pro celosystémové nastavení:</p>
      <code its:translate="no">[org/gnome/login-screen]
banner-message-enable=true
banner-message-text='<input its:translate="yes">Zde napište text zprávy.</input>'
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>Pro zprávu neexistuje žádné omezení na počet znaků. <sys>gnome-shell</sys> automaticky zjistí delší úseky textu a přepne do dvousloupcového režimu.</p>
    <p>Text zprávy nelze načítat z externího souboru.</p>
  </note>

</page>
