<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="gsettings-browse" xml:lang="cs">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf"/>
    <link type="seealso" xref="overrides"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jaký nástroj můžete používat pro procházení nastavení systému a aplikací.</desc>
  </info>

  <title>Procházení nastavení GSettings pro vaše aplikace</title>

  <p>Existují dva nástroje, které můžete použít k procházení předvoleb systému a aplikací uložených jako hodnoty GSettings: grafický nástroj <app>dconf-editor</app> a pomůcka příkazové řádky <cmd>gsettings</cmd>.</p>

  <p>Oba nástroje umožňují rovněž úpravy předvoleb pro aktuálního uživatele.</p>

  <note style="warning"><p>Pamatujte, že tyto nástroje pracují vždy s databází GSettings aktuálního uživatele, takže byste je neměli spouštět pod uživatelem root.</p>
  
  <p>Jak <app>dconf-editor</app>, tak <cmd>gsettings</cmd>, vyžadují sběrnici sezení D-Bus, aby mohli provádět změny. Je to proto, že démon <sys>dconf</sys> musí být aktivován pomocí D-Bus.</p>

  <p>Požadovanou sběrnici sezení můžete získat spuštění <cmd>gsettings</cmd> pomocí pomůcky <cmd>dbus-launch</cmd>, nějak takto:</p>

  <screen><output>$ </output><input>dbus-launch gsettings set org.gnome.desktop.background draw-background true</input></screen>
  </note>

  <p><app>dconf-editor</app> může být lepší volbou, pokud nejste sběhlí v dostupných nastaveních aplikace. Ukazuje hierarchii nastavení ve stromové podobě a k tomu zobrazuje ke každému nastavení doplňující informace, včetně popisu, typu a výchozí hodnoty.</p>

  <p><cmd>gsettings</cmd> je mnohem mocnější, než <app>dconf-editor</app>. Je pro něj k dispozici automatické doplňování v Bashi a můžete psát skripty, které využívají příkazy <cmd>gsettings</cmd> k automatizaci nastavení.</p>

  <p>Úplný seznam voleb pro <cmd>gsettings</cmd> najdete v manuálové stránce <link its:translate="no" href="man:gsettings"><cmd>gsettings</cmd>(1)</link>. </p>

</page>
