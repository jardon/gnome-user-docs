<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overrides" xml:lang="ko">

  <info>
    <link type="guide" xref="setup"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>배포판에서는 기본 설정을 바꾸는 용도로 GSettings 우선 적용값을 사용합니다.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>GSettings 우선 적용 값을 사용하면 안되는 이유가 뭐죠?</title>

  <p>GSettings 우선 적용 값은 그놈 데스크톱과 앱의 기본 설정을 배포판에서 조절할 때 사용합니다. dconf에서 사용하는 우선 적용값은 시스템 관리자가 그놈 데스크톱과 앱 필수 설정 값과 기본 설정 값을 조절할 수 있도록 설계했습니다.</p>

  <section id="what-are-vendor-overrides">
  <title>제조사 중복 우선 설정 값은 무엇인가요?</title>

   <p>기본값은 프로그램에서 설치한 스키마에 정의해두었습니다. 때로는 제조사나 배포판에서 이 기본값을 조절해야 할 때가 있습니다.</p>

   <p>스키마의 XML 소스를 패치해서 불편해지거나 오류가 자주 발생한다면, <link its:translate="no" href="man:glib-compile-schemas">
   <sys>glib-compile-schemas</sys></link>에서는 <em>제조사 우선 적용값</em> 파일을 불러옵니다. XML 스키마 소스가 들어 있는 동일한 디렉터리에 키 파일이 있으며, 이 파일로 하여금 기본 값을 중복 우선 지정할 수 있습니다.</p>

  </section>

</page>
