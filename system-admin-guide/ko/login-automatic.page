<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-automatic" xml:lang="ko">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="review"/>
    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.12" date="2014-06-18" status="review"/>
    
    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>시작시 사용자 계정으로 자동으로 로그인합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>자동 로그인 설정</title>

  <p its:locNote="TRANSLATORS: 'Administrator' and 'Automatic Login' are   strings from the Users dialog in the Settings panel user interface."><em>관리자</em> 계정 형식의 사용자는 <link href="help:gnome-help/user-autologin"><app>설정</app> 창에서 <em>자동 로그인</em> 활성</link>이 가능합니다. 또한 다음과 같이 <sys its:translate="no">GDM</sys> 개별 설정 파일에 자동 로그인을 직접 설정할 수 있습니다.</p>
  
  <steps>
    <item>
      <p><file its:translate="no">/etc/gdm/custom.conf</file> 파일을 편집하여 파일의 <code>[daemon]</code> 섹션에서 다음 내용을 지정하는지 확인하십시오:</p>
      <listing its:translate="no">
        <title><file>custom.conf</file></title>
<code>
[daemon]
AutomaticLoginEnable=<input>True</input>
AutomaticLogin=<input>username</input>
</code>
      </listing>
    </item>
    <item>
      <p><input its:translate="no">username</input> 값을 자동으로 로그인하려는 사용자로 바꾸십시오.</p>
    </item>
  </steps>

  <note>
    <p><file its:translate="no">custom.conf</file> 파일은 보통 <file its:translate="no">/etc/gdm/</file> 디렉터리에 있지만 해당 위치는 배포판 별로 다를 수 있습니다.</p>
  </note>

</page>
