<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application" xml:lang="ko">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>개별 MIME 형식을 열 때 어떤 프로그램으로 열 지 따로 지정합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

    <title>모든 사용자에게 기본 등록 프로그램 우선 적용</title>
    <p><file>/usr/share/applications/mimeapps.list</file>파일과 <file>/usr/share/applications/gnome-mimeapps.list</file> 파일은 기본으로 개별 MIME 형식을 열 때 사용할 등록 프로그램을 지정합니다. 이 파일은 배포판에서 제공합니다.</p>

      <p>시스템의 모든 사용자에게 시스템 기본값으로 우선 적용하려면, 우선 적용할 기본 등록 프로그램과 MIME 형식 목록이 들어간 <file>/etc/xdg/mimeapps.list</file>파일 또는 <file>/etc/xdg/gnome-mimeapps.list</file> 파일을 만들어야합니다.</p>

      <note>
      <p><file>/etc/xdg/</file> 디렉터리에 있는 파일은 <file>/usr/share/applications/</file> 디렉터리에 들어간 파일보다 우선합니다. 게다가 <file>/etc/xdg/gnome-mimeapps.list</file> 파일은 <file>/etc/xdg/mimeapps.list</file> 파일보다 우선하지만, <file>~/.config/mimeapps.list</file>의 사용자 설정을 우선할 수는 없습니다.</p>
      </note>

    <steps>
      <title>모든 사용자에게 기본 등록 프로그램 우선 적용</title>
      <item>
        <p>어떤 기본 등록 프로그램으로 바꿀지 MIME 형식을 결정하는 <file>/usr/share/applications/mimeapps.list</file> 파일을 찾아보십시오. 예를 들어 다음 예제의 <file>mimeapps.list</file> 파일에는 <code>text/html</code> MIME 형식과 <code>application/xhtml+xml</code> MIME 형식에 대한 기본 등록 프로그램을 지정합니다:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>기본 프로그램(<app>에피파니</app>)은 해당 <file>.desktop</file> 파일(<file>epiphany.desktop</file>)에서 지정합니다. 다른 프로그램의 <file>.desktop</file> 파일 기본 위치는 <file>/usr/share/applications/</file> 입니다.</p>
      </item>
      <item>
        <p><file>/etc/xdg/mimeapps.list</file> 파일을 만드십시오. 이 파일에 MIME 형식과 관련 기본 등록 프로그램을 지정하십시오:</p>
        <code>[Default Applications]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Added Associations]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>이 예제에서는 <code>text/html</code> MIME 형식의 기본 등록 프로그램을 <code>myapplication1.desktop</code> 파일로 지정하고, <code>application/xhtml+xml</code> MIME 형식의 기본 등록 프로그램을 <code>myapplication2.desktop</code> 파일로 지정합니다.</p>
        <p>이 설정이 제대로 동작하려면, <file>myapplication1.desktop</file> 파일과 <file>myapplication2.desktop</file> 파일이 <file>/usr/share/applications/</file> 디렉터리에 있어야합니다.</p>
      </item>
      <item>
        <p><cmd>gio mime</cmd> 명령을 실행하여 기본 등록 프로그램을 제대로 설정했는지 확인할 수 있습니다:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
	epiphany.desktop
Recommended applications:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
