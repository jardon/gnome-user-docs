<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-logout" xml:lang="ko">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>사용자의 로그아웃 및 사용자 전환을 막습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조재은</mal:name>
      <mal:email>ckr971028@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2019, 2021, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>사용자 로그아웃, 사용자 전환 막기</title>

  <p>사용자 로그아웃 막기는 특별한 목적으로 그놈 데스크톱을 배포할 경우에 유용합니다(무인 키오스크, 공용 인터넷 활용 터미널 등).</p>

  <note style="important">
  <p>사용자는 다른 사용자 전환 방식을 통해 로그아웃 잠금을 회피할 수 있습니다. 이런 이유로 시스템을 설정할 때 <em>사용자 전환</em> 막기를 추천합니다.</p>
  </note>

  <steps>
  <title>사용자 로그아웃, 사용자 전환 막기</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p><sys>local</sys> 데이터베이스에 정보를 제공할 <file>/etc/dconf/db/local.d/00-logout</file> 키 파일을 만드십시오:</p>
<screen>
[org/gnome/desktop/lockdown]
# Prevent the user from logging out
disable-log-out=true

# Prevent the user from user switching
disable-user-switching=true
</screen>
  </item>
  <item>
  <p>사용자 설정 대신 적용하고 <file>/etc/dconf/db/local.d/locks/lockdown</file> 파일 설정을 사용자가 바꾸지 못하도록 막으려면:</p>
<screen>
# Lock user logout
/org/gnome/desktop/lockdown/disable-log-out

# Lock user switching
/org/gnome/desktop/lockdown/disable-user-switching
</screen>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item>
  <p>시스템 전체에 해당하는 설정대로 동작하게 하려면 시스템을 다시 시작하십시오.</p>
  </item>
  </steps>

</page>
