<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-custom-defaults" xml:lang="sv">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ställ in systemomfattande standardinställningar med <sys its:translate="no">dconf</sys>-profiler.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Anpassade standardvärden för systeminställningar</title>

  <p>Systemomfattande standardinställningar kan ställas in genom att tillhandahålla ett standardvärde för en nyckel i en <sys its:translate="no">dconf</sys>-profil. Dessa standardvärden kan åsidosättas av användaren.</p>

<section id="example">
  <title>Ställ in ett standardvärde</title>

  <p>För att ställa in ett standardvärde för en nyckel måste profilen <sys>user</sys> existera och värdet för nyckeln måste läggas till i en <sys its:translate="no">dconf</sys>-databas.</p>

  <steps>
    <title>Ett exempel som ställer in standardbakgrunden</title>
    <item>
      <p>Skapa profilen <file its:translate="no">user</file>:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/profile/user</file></title>
<code>
user-db:user
system-db:local
</code>
      </listing>
      <p><input its:translate="no">local</input> är namnet på en <sys its:translate="no">dconf</sys>-databas.</p>
    </item>
    <item>
      <p>Skapa en <em>nyckelfil</em> för databasen <input its:translate="no">local</input> som innehåller standardinställningarna:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/db/local.d/01-background</file></title>
<code>
# <span its:translate="yes">dconf-sökväg</span>
[org/gnome/desktop/background]

# <span its:translate="yes">dconf-nyckelnamn och deras motsvarande värden</span>
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'
picture-options='scaled'
primary-color='000000'
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>När profilen <sys its:translate="no">user</sys> skapas eller ändras måste användaren logga ut och logga in igen för att ändringarna ska verkställas.</p>
  </note>

  <p>Om du vill undvika att skapa profilen <sys its:translate="no">user</sys> kan du använda kommandoradsverktyget <cmd>dconf</cmd> för att läsa och skriva individuella värden eller hela kataloger till eller från en <sys its:translate="no">dconf</sys>-databas. För mer information, se manualsidan för <link its:translate="no" href="man:dconf"><cmd>dconf</cmd>(1)</link>.</p>

</section>

</page>
