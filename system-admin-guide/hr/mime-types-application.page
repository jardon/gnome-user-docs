<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application" xml:lang="hr">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prilagodba koja aplikacija otvara određenu MIME vrstu.</desc>
  </info>

    <title>Zaobilaženje zadane registrirane aplikacije za sve korisnike</title>
    <p>Datoteke <file>/usr/share/applications/mimeapps.list</file> i <file>/usr/share/applications/gnome-mimeapps.list</file> određuju koja je aplikacija registrirana za otvaranje određenih MIME vrsta po zadanome. Ove datoteke omogućuje distribucija.</p>

      <p>Kako bi zaobišli zadane postavke sustava za sve korisnike u sustavu, morate stvoriti <file>/etc/xdg/mimeapps.list</file> ili <file>/etc/xdg/gnome-mimeapps.list</file> datoteku s popisom MIME vrsta za koje želite zaobići zadanu registriranu aplikaciju.</p>

      <note>
      <p>Datoteke koje se nalaze u <file>/etc/xdg/</file> imaju prednost nad datotekama koje se nalaze u <file>/usr/share/applications/</file>. Dodatno, <file>/etc/xdg/gnome-mimeapps.list</file> ima prednost nad <file>/etc/xdg/mimeapps.list</file>, ali ga može zaobići korisničko podešavanje u <file> ~/.config/mimeapps.list</file>.</p>
      </note>

    <steps>
      <title>Zaobilaženje zadane registrirane aplikacije za sve korisnike</title>
      <item>
        <p>Pogledajte <file>/usr/share/applications/mimeapps.list</file> datoteku kako bi odredili MIME vrste za koje želite promijeniti zadanu registriranu aplikaciju. Primjerice, sljedeći primjer datoteke <file>mimeapps.list</file> određuje zadanu registriranu aplikaciju za <code>text/html</code> i <code>application/xhtml+xml</code> MIME vrste:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Zadana aplikacija (<app>Epiphany</app>) definirana je određivanjem odgovarajuće <file>.desktop</file> datoteke (<file>epiphany.desktop</file>). Zadana lokacija za <file>.desktop</file> datoteke ostalih aplikacija je <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Stvorite <file>/etc/xdg/mimeapps.list</file> datoteku. U datoteci navedite MIME vrste i njihove odgovarajuće zadane registrirane aplikacije:</p>
        <code>[Default Applications]
text/html=<var>mojaaplikacija1.desktop</var>
application/xhtml+xml=<var>mojaaplikacija2.desktop</var>

[Added Associations]
text/html=<var>mojaaplikacija1.desktop</var>;
application/xhtml+xml=<var>mojaplikacija2.desktop</var>;</code>
      <p>Ovo postavlja zadanu registriranu aplikaciju za <code>text/html</code> MIME vrstu na <code>mojaaplikacij1.desktop</code>, i zadanu registriranu aplikaciju za <code>application/xhtml+xml</code> MIME vrstu na <code>mojaaplikacija2.desktop</code>.</p>
        <p>Kako bi ove postavke ispravno funkcionirale, provjerite jesu li datoteke <file>mojaplikacija1.desktop</file> i <file>mojaplikacija2.desktop</file> smještene u <file>/usr/share/applications/</file> direktorij.</p>
      </item>
      <item>
        <p>Možete koristiti <cmd>gio mime</cmd> naredbu za provjeriu je li zadana registrirana aplikacija ispravno postavljena:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: mojaaplikacija1.desktop
Registered applications:
	mojaaplikacija1.desktop
	epiphany.desktop
Recommended applications:
	mojaaplikacija1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
