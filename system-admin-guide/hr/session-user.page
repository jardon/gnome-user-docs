<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-user" xml:lang="hr">

  <info>
    <link type="guide" xref="sundry#session"/>
    <link type="guide" xref="login#management"/>
    <link type="seealso" xref="session-custom"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="draft"/>
    <revision pkgversion="3.8" date="2013-08-06" status="review"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Određivanje zadane sesije za korisnika.</desc>
   </info>

  <title>Podešavanje zadane sesije korisnika</title>

  <p>Zadana sesija određuje se programom zvanim <app>AccountsService</app>. <app>AccountsService</app> pohranjuje ove informacije u <file>/var/lib/AccountsService/users/</file> direktorij.</p>

<note style="note">
  <p>U GNOME 2, <file>.dmrc</file> datoteka u osobnom direktoriju korisnika korištena je za stvaranje zadanih sesija. Ova <file>.dmrc</file> datoteka se više ne koristi.</p>
</note>

  <steps>
    <title>Određivanje zadane sesije za korisnika</title>
    <item>
      <p>Provjerite imate li instaliran <sys>gnome-session-xsession</sys> paket na vašem sustavu.</p>
    </item>
    <item>
      <p>Idite do <file>/usr/share/xsessions</file> direktorija gdje možete pronaći <file>.desktop</file> datoteke za svaku od dostupnih sesija. Pogledajte sadržaj <file>.desktop</file> datoteka kako bi odabrali sesiju koju želite koristiti.</p>
    </item>
    <item>
      <p>Kako bi odredili zadanu sesiju za korisnika, nadopunite korisničku <sys>uslugu računa</sys> u <file>/var/lib/AccountsService/users/<var>korisničkoime</var></file> datoteci:</p>
<code>[User]
Language=
XSession=gnome-classic</code>
       <p>U ovom primjeru, <link href="help:gnome-help/gnome-classic">GNOME Classic</link> je postavljen kao zadana sesija, koristeći <file>/usr/share/xsessions/gnome-classic.desktop</file> datoteku.</p>
     </item>
  </steps>

  <p>Nakon određivanja zadane sesije korisnika, ta će se sesija koristiti sljedeći puta kada se korisnik prijavi, osim ako korisnik ne odabere drugu sesiju na zaslonu prijave.</p>

</page>
