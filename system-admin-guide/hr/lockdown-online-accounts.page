<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-online-accounts" xml:lang="hr">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

   <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Omogućavanje ili onemogućavanje pojedinih ili svih mrežnih računa.</desc>
  </info>
  <title>Omogućavanje ili onemogućavanje mrežnih računa</title>

  <p><app>GNOME mrežni računi</app> (GOA) koriste se za integraciju osobnih mrežnih računa s GNOME radnom površinom i aplikacijama. Korisnik može dodati svoje mrežne račune, poput Googla, Facebooka, Flickra, ownClouda i ostalih pomoću aplikacije <app>Mrežni računi</app>.</p>

  <p>Kao administrator sustava, možete:</p>
  <list>
    <item><p>omogućiti mrežne račune;</p></item>
    <item><p>omogućiti pojedine mrežne račune;</p></item>
    <item><p>onemogućiti sve mrežne račune.</p></item>
  </list>

<steps>
  <title>Podešavanje mrežnih računa</title>
  <item><p>Provjerite imate li instaliran <sys>gnome-online-accounts</sys> paket na vašem sustavu.</p>
  </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Stvorite datoteku ključa <file>/etc/dconf/db/local.d/00-goa</file> kako bi pružili informacije za <sys>lokalnu</sys> bazu podataka koja sadrži sljedeće podešavanje.</p>
   <list>
   <item><p>Za omogućavanje pojedinih pružatelja usluga:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['google', 'facebook']
</code>
  </item>
   <item><p>Za onemogućavanje svih pružatelja usluga:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['']
</code>
  </item>
  <item><p>Za omogućavanje svih dostupnih pružatelja usluga:</p>
<code>
[org/gnome/online-accounts]
whitelisted-providers= ['all']
</code>
  <p>Ovo je zadana postavka.</p></item>
   </list>
  </item>
  <item>
    <p>Kako bi spriječili korisnika da zaobiđe ove postavke, stvorite datoteku <file>/etc/dconf/db/local.d/locks/goa</file> sa sljedećim sadržajem:</p>
    <listing>
    <title><file>/etc/dconf/db/local.db/locks/goa</file></title>
<code>
# Zaključavanje popisa pružatelja usluga kojima je dopušteno učitavanje
/org/gnome/online-accounts/whitelisted-providers
</code>
    </listing>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
</steps>

</page>
