<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="hr">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Dodavanje dodatnih slova za sve korisnike.</desc>
  </info>

  <title>Dodavanje dodatnih slova za sve korisnike</title>

  <p>Možete instalirati dodatna slova koja će biti dostupna korisnicima u aplikacijama koje koriste <sys>fontconfig</sys> za rukovanje slovima.</p>

  <steps>
    <title>Instalacija dodatnih slova</title>
    <item>
      <p>Kopirajte slovo u direktorij <file>/usr/local/share/fonts/</file> kako bi ga instalirali.</p>
    </item>
    <item>
      <p>Možda ćete morati pokrenuti sljedeću naredbu za nadopunu predmemorije slova:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>Možda ćete morati ponovno pokrenuti pokrenute aplikacije kako bi vidjeli promjene. Korisničke sesije se ne moraju ponovno pokretati.</p>
  
  <p>Alternativno, možete instalirati slova u drugi direktorij sustava osim <file>/usr/local/share/fonts/</file> ako je taj direktorij naveden u <file>/etc/fonts/fonts.conf</file> datoteci. Ako nije, tada morate stvoriti vlastitu datoteku podešavanja cijelog sustava u <file>/etc/fonts/local.conf</file> koja sadrži direktorij koji želite koristiti. Za više informacija pogledajte <cmd>fonts-conf</cmd>(5) stranicu priručnika.</p>
  <p>Ako koristite alternativni direktorij, ne zaboravite navesti naziv direktorija kada nadopunjavate predmemoriju slova pomoću <cmd>fc-cache</cmd> naredbe:</p>
  <screen><output>$ </output><input>fc-cache <var>naziv_direktorija</var></input></screen>

</page>
