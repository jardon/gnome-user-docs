<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="fonts" xml:lang="gl">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="fonts-user"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <desc>Engadir tipos de letra adicionais para todos os usuarios.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  </info>

  <title>Engadir un tipo de letra adicional para todos os usuarios</title>

  <p>You can install an extra font which will be available to users in
  applications that use <sys>fontconfig</sys> for font handling.</p>

  <steps>
    <title>Instalar un tipo de letra adicional</title>
    <item>
      <p>Copie o tipo de letra ao cartafol <file>/usr/local/share/fonts/</file> para instalala.</p>
    </item>
    <item>
      <p>You may need to run the following command to update the font cache:</p>
      <screen><output>$ </output><input>fc-cache /usr/local/share/fonts/</input></screen>
    </item>
  </steps>

  <p>You may need to restart running applications to see the changes. User
  sessions do not need to be restarted.</p>
  
  <p>Alternatively, you can also install fonts in another system directory than
    <file>/usr/local/share/fonts/</file> if that directory is listed in the
    <file>/etc/fonts/fonts.conf</file> file. If it is not, then you need
    to create your own machine-wide configuration file in
    <file>/etc/fonts/local.conf</file> containing the directory you
    want to use. See the <cmd>fonts-conf</cmd>(5) man page for more
    information.</p>
  <p>If you are using an alternative directory, remember to specify
    the directory name when updating the font cache with the <cmd>fc-cache</cmd>
    command:</p>
  <screen><output>$ </output><input>fc-cache <var>nome_cartafol</var></input></screen>

</page>
