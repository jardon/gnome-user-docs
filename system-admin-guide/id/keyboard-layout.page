<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-layout" xml:lang="id">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>

    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengisi pemilih tata letak papan tik pada layar login.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2023</mal:years>
    </mal:credit>
  </info>

  <title>Menampilkan beberapa tata letak papan tik pada layar login</title>

  <p>Anda dapat mengubah pengaturan tata letak papan tik sistem, menambahkan tata letak papan tik alternatif untuk dipilih pengguna pada layar login. Ini bisa membantu pengguna yang biasanya memakai tata letak papan tik yang berbeda dari baku dan yang ingin tata letak papan tik tersebut tersedia di layar login.</p>

  <steps>
    <title>Mengubah pengaturan tata letak papan tik sistem</title>
    <item>
      <p>Temukan kode tata letak bahasa yang diinginkan di berkas <file>/usr/share/x11/xkb/rules/base.lst</file> di bawah bagian bernama <sys>! layout</sys>.</p>
    </item>
    <item>
      <p>Gunakan alat <cmd>localectl</cmd> untuk mengubah pengaturan tata letak papan tik sistem sebagai berikut:</p>
      <screen><cmd>localectl set-x11-keymap <var>layout</var></cmd></screen>
      <p>Anda dapat menentukan beberapa tata letak sebagai daftar yang dipisahkan koma. Misalnya, untuk menetapkan <sys>es</sys> sebagai tata letak baku, dan <sys>us</sys> sebagai tata letak sekunder, jalankan perintah berikut:</p>
      <screen><output>$ </output><input>localectl set-x11-keymap es,us</input></screen>
    </item>
    <item>
      <p>Keluar untuk menemukan bahwa tata letak yang ditetapkan tersedia di bilah bagian atas pada layar login.</p>
    </item>
  </steps>
  <p>Perhatikan bahwa Anda juga dapat menggunakan alat <cmd>localectl</cmd> untuk menentukan model, varian, dan pilihan papan tik baku di seluruh mesin. Lihat halaman man <cmd>localectl</cmd>(1) untuk informasi lebih lanjut.</p>

  <section id="keyboard-layout-no-localectl">
  <title>Menampilkan beberapa tata letak papan tik tanpa menggunakan localectl</title>

  <p>Pada sistem yang tidak menyediakan alat <cmd>localectl</cmd>, Anda dapat mengubah pengaturan tata letak papan ketik sistem dengan mengedit berkas konfigurasi di <file>/usr/share/x11/xorg.conf.d/</file>.</p>

  <steps>
    <title>Mengubah pengaturan tata letak papan tik sistem</title>
    <item>
      <p>Temukan kode tata letak bahasa yang diinginkan di berkas <file>/usr/share/x11/xkb/rules/base.lst</file> di bawah bagian bernama <sys>! layout</sys>.</p>
    </item>
    <item>
      <p>Tambahkan kode tata letak ke <file>/usr/share/x11/xorg.conf.d/10-evdev.conf</file> dengan cara berikut:</p>
<screen>
Section "InputClass"
  Identifier "evdev keyboard catchall"
  MatchIsKeyboard "on"
  MatchDevicePath "/dev/input/event*"
  Driver "evdev"
  <input>Option "XkbLayout" "en,fr"</input>
EndSection
</screen>
      <p>Beberapa tata letak dapat ditambahkan sebagai daftar yang dipisah koma, seperti yang ditunjukkan pada contoh untuk tata letak Inggris (<sys>en</sys>) dan Prancis (<sys>fr</sys>).</p>
    </item>
    <item>
      <p>Keluar untuk menemukan bahwa tata letak yang ditetapkan tersedia di bilah bagian atas pada layar login.</p>
    </item>
  </steps>

  </section>

</page>
