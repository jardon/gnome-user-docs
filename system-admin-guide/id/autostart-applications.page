<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="id">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bagaimana saya dapat menambahkan aplikasi autostart untuk semua pengguna?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2023</mal:years>
    </mal:credit>
  </info>

  <title>Menambahkan aplikasi autostart untuk semua pengguna</title>

  <p>Untuk memulai aplikasi secara otomatis saat pengguna masuk, Anda perlu membuat berkas <file>.desktop</file> untuk aplikasi tersebut di direktori <file>/etc/xdg/autostart/</file>.</p>

<steps>
  <title>Untuk menambahkan aplikasi autostart bagi semua pengguna:</title>
  <item><p>Buat berkas <file>.desktop</file> di direktori <file>/etc/xdg/autostart/</file>:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Files</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Ganti <var>Files</var> dengan nama aplikasi.</p></item>
  <item><p>Ganti <var>nautilus -n</var> dengan perintah yang ingin Anda gunakan untuk menjalankan aplikasi.</p></item>
  <item><p>Anda dapat menggunakan kunci <code>AutostartCondition</code> untuk memeriksa nilai kunci GSettings.</p>
  <p>Manajer sesi menjalankan aplikasi secara otomatis jika nilai kunci benar. Jika nilai kunci berubah dalam sesi berjalan, manajer sesi memulai atau menghentikan aplikasi, tergantung pada apa nilai sebelumnya untuk kunci tersebut.</p>
</item>
</steps>

</page>
