<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="mime-types" xml:lang="id">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <revision pkgversion="3.12" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>MIME types are used to identify the format of a file.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2020-2023</mal:years>
    </mal:credit>
  </info>

    <title>What are MIME types?</title>
    <p>
      In GNOME, MIME (<em>Multipurpose Internet Mail Extension</em>)
      types are used to identify the format of a file. The GNOME Desktop
      uses MIME types to:
    </p>
    <list>
      <item>
        <p>
          Determine which application should open a specific file format by
          default.
        </p>
      </item>
      <item>
        <p>
          Register other applications that can also open a specific file format.
        </p>
      </item>
      <item>
        <p>
          Provide a string describing the type of a file, for example,
          in a file properties dialog of the <app>Files</app>
          application.
        </p>
      </item>
      <item>
        <p>
          Provide an icon representing a specific file format, for
          example, in a file properties dialog of the <app>Files</app>
          application.
        </p>
      </item>
    </list>
    <p>
      MIME type names follow a given format:
    </p>
<screen>
<var>media-type</var>/<var>subtype-identifier</var>
</screen>
<p>
      <sys>image/jpeg</sys> is an example of a MIME type where
      <sys>image</sys> is the media type, and <sys>jpeg</sys>
      is the subtype identifier.
</p>
    <p>
      GNOME follows the <em>freedesktop.org Shared MIME Info</em>
      specification to determine:
    </p>
    <list>
    <item>
      <p>
        The machine-wide and user-specific location to store all MIME type
        specification files.
      </p>
    </item>
    <item>
      <p>
        How to register a MIME type so that the desktop environment knows which
        applications can be used to open a specific file format.
      </p>
    </item>
    <item>
      <p>
        How the user can change which applications should open what file formats.
      </p>
    </item>
    </list>
    <section id="mime-database">
      <title>Apa yang dimaksud dengan basis data MIME?</title>
      <p>
        The MIME database is a collection of all MIME type specification files
        that GNOME uses to store information about known MIME types.
      </p>
      <p>Bagian terpenting dari basis data MIME dari sudut pandang administrator sistem adalah direktori <file>/usr/share/mime/packages/</file> di mana berkas terkait jenis MIME yang menentukan informasi tentang jenis MIME yang diketahui disimpan. Salah satu contoh dari berkas tersebut adalah <file>/usr/share/mime/packages/freedesktop.org.xml</file>, menentukan informasi tentang jenis MIME standar yang tersedia pada sistem secara baku. Berkas tersebut disediakan oleh paket <sys>shared-mime-info</sys>.</p>
    </section>
    <section id="mime-types-more-information">
    <title>Mendapatkan informasi lebih lanjut</title>
    <p>
      For detailed information describing the MIME type system, see the
      <em>freedesktop.org Shared MIME Info specification</em> located at the
      freedesktop.org website:
    </p>
    <list>
      <item>
        <p><link href="http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/"> http://www.freedesktop.org/wiki/Specifications/shared-mime-info-spec/</link></p>
      </item>
    </list>
    </section>
</page>
