<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-userlist-disable" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="login#appearance"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Faça os usuários digitarem seus nomes de usuário na tela autenticação.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Desabilitando a lista de usuário</title>

  <p>Você pode desabilitar a lista de usuários mostrada na tela de autenticação definindo a chave GSettings <sys>org.gnome.login-screen.disable-user-list</sys>.</p>
  <p>Quando a lista de usuário estiver desabilitada, usuários precisam digitar seus nome de usuário e senha na tela de autenticação.</p>
  <steps>
    <title>Definindo a chave org.gnome.login-screen.disable-user-list</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item>
      <p>Crie um arquivo de chave <sys>gdm</sys> para configuração de toda máquina em <file>/etc/dconf/db/gdm.d/00-login-screen</file>:</p>
        <code>[org/gnome/login-screen]
# Não mostra a lista de usuário
disable-user-list=true
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
