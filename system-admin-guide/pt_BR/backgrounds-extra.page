<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backgrounds-extra" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Como posso disponibilizar planos de fundo extras para meus usuários?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2020</mal:years>
    </mal:credit>
  </info>

  <title>Adicionando planos de fundo extras</title>

  <p>Você pode disponibilizar planos de fundo adicionais para os usuários em seu sistema seguindo as etapas abaixo.</p>

  <steps>
  <title>Definindo planos de fundo extras</title>
  <item>
  <p>Crie um arquivo XML, por exemplo <file><var>nome-de-arquivo</var>.xml</file>. Neste arquivo, use chaves de esquema GSettings <sys>org.gnome.desktop.background</sys> para especificar planos de fundo extras e sua aparência.</p>

  <p>Abaixo está uma lista das chaves mais usadas:</p>

  <table frame="top bottom" rules="all" shade="rows">
    <title>Chaves GSettings de esquema org.gnome.desktop.background</title>
    <tbody>
    <tr>
      <td><p>Nome da chave</p></td>
      <td><p>Valores possíveis</p></td>
      <td><p>Descrição</p></td>
    </tr>
    <tr>
      <td><p>picture-options</p></td>
      <td><p>"none", "wallpaper", "centered", "scaled", "stretched", "zoom", "spanned"</p></td>
      <td><p>Determina como a imagem definida por <var>wallpaper_filename</var> é renderizada.</p></td>
    </tr>
    <tr>
      <td><p>color-shading-type</p></td>
      <td><p>"horizontal", "vertical" e "solid"</p></td>
      <td><p>Como sombrear a cor do plano de fundo.</p></td>
    </tr>
    <tr>
      <td><p>primary-color</p></td>
      <td><p>padrão: #023c88</p></td>
      <td><p>Cor esquerda ou superior ao desenhar degradês ou a cor sólida.</p></td>
    </tr>
    <tr>
      <td><p>secondary-color</p></td>
      <td><p>padrão: #5789ca</p></td>
      <td><p>Cor direita ou inferior ao desenhar degradês, não usados para cores sólidas.</p></td>
    </tr>
  </tbody>
  </table>

  <p>Você pode ver uma lista completa das chaves <sys>org.gnome.desktop.background</sys> e possíveis valores usando o <app>dconf-editor</app> ou o utilitário de linha de comando <cmd>gsettings</cmd>. Veja <link xref="gsettings-browse"/> para mais informações.</p>
  <p>Abaixo está um exemplo de arquivo <file><var>nome-de-arquivo</var>.xml</file>:</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Papel de parede corporativo&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/papel-de-parede-corporativo.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;

</code>

  </item>
  <item>
  <p>Coloque o arquivo <file><var>nome-de-arquivo</var>.xml</file> no diretório <file>/usr/share/gnome-background-properties/</file>.</p>
  <p>Usuários terão planos de fundo extras disponíveis para configuração em <guiseq><gui>Configurações</gui> <gui>Plano de fundo</gui></guiseq>.</p>
  </item>
  </steps>


  <section id="backgrounds-extra-two-wallpapers">
  <title>Especificando vários planos de fundo</title>
  <p>Em um arquivo de configuração, você pode especificar vários elementos <code>&lt;wallpaper&gt;</code> para adicionar mais fundos.</p>
  <p>Veja o exemplo a seguir com dois elementos <code>&lt;wallpaper&gt;</code>, adicionando dois planos de fundo diferentes:</p>

<code mime="application/xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE wallpapers SYSTEM "gnome-wp-list.dtd"&gt;
&lt;wallpapers&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Papel de parede corporativo&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/papel-de-parede-corporativo.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ffffff&lt;/pcolor&gt;
    &lt;scolor&gt;#000000&lt;/scolor&gt;
  &lt;/wallpaper&gt;
  &lt;wallpaper deleted="false"&gt;
    &lt;name&gt;Papel corporativo 2&lt;/name&gt;
    &lt;name xml:lang="de"&gt;Firmenhintergrund 2&lt;/name&gt;
    &lt;filename&gt;/usr/local/share/backgrounds/papel-de-parede-corporativo-2.jpg&lt;/filename&gt;
    &lt;options&gt;zoom&lt;/options&gt;
    &lt;shade_type&gt;solid&lt;/shade_type&gt;
    &lt;pcolor&gt;#ff0000&lt;/pcolor&gt;
    &lt;scolor&gt;#00ffff&lt;/scolor&gt;
  &lt;/wallpaper&gt;
&lt;/wallpapers&gt;
</code>

</section>
</page>
