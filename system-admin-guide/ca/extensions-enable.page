<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="extensions-enable" xml:lang="ca">

  <info>
    <link type="guide" xref="software#extension"/>
    <link type="seealso" xref="extensions-lockdown"/>
    <link type="seealso" xref="extensions"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Activeu les extensions del GNOME Shell per a tots els usuaris.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Activar les extensions per a tot l'equip</title>
  
  <p>Per a fer les extensions disponibles per a tots els usuaris del sistema, instal·leu-les al directori <file>/usr/share/gnome-shell/extensions</file>.Fixeu-vos que les noves extensions instal·lades a l'equip, per defecte estan desactivades.</p>

  <p>Heu de definir la clau <code>org.gnome.shell.enabled-extensions</code> per a configurar les extensions habilitades per defecte. Tanmateix, actualment no hi ha cap manera d'habilitar extensions addicionals per als usuaris que ja s’han iniciat la sessió. Això no s’aplica als usuaris existents que han instal·lat i habilitat les seves pròpies extensions GNOME.</p>

  <steps>
    <title>Configura la clau org.gnome.shell.enabled-extensions</title>
    <item>
      <p>Crear un perfil d'<code>user</code> a <file>/etc/dconf/profile/user</file>:</p>
      <listing>
        <code>
user-db:user
system-db:local
</code>
      </listing>
    </item>
    <item>
      <p>Creeu una base de dades <code>local</code> per a la configuració de l'equip a <file>/etc/dconf/db/local.d/00-extensions</file>:</p>
      <listing>
        <code>
[org/gnome/shell]
# Llista totes les extensions que es vol que estiguin actives per a tots els usuaris
enabled-extensions=['<input>myextension1@myname.example.com</input>', '<input>myextension2@myname.example.com</input>']
</code>
      </listing>
      <p>La clau <code>enabled-extensions</code> especifica les extensions actives utilitzant les extensions uuid (<code>myextension1@myname.example.com</code> i <code>myextension2@myname.example.com</code>).</p>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
