<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-repartitioning" xml:lang="ca">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.14" date="2014-12-10" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Evitar que l'usuari pugui canviar les particions del disc.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar reparticionar</title>

  <p><sys>polkit</sys> us permet establir permisos per a realitzar operacions individuals. <sys>udisks2</sys>, la utilitat per a gestionar els serveis de disc, té la configuració localitzada a <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>. Aquest fitxer conté un conjunt d'accions i valors predeterminats, que poden ser anul·lats per l'administrador del sistema.</p>

  <note style="tip">
    <p>La configuració de <sys>polkit</sys> a <file>/etc</file> substitueix el paquet enviat pels paquets a <file>/usr/share</file>.</p>
  </note>

  <steps>
    <title>Desactivar reparticionar</title>
    <item>
      <p>Crea un fitxer amb el mateix contingut que a <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>: <cmd>cp /usr/share/polkit-1/actions/org.freedesktop.udisks2.policy /etc/share/polkit-1/actions/org.freedesktop.udisks2.policy</cmd></p>
      <note style="important">
        <p>No modifiqueu el fitxer <file>/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy</file>, els canvis es sobreescriuran a la següent actualització del paquet.</p>
      </note>
    </item>
    <item>
      <p>Elimineu totes les accions que no necessiteu dins de l'element <code>policyconfig</code> i afegiu les línies següents al fitxer <file>/etc/polkit-1/actions/org.freedesktop.udisks2.policy</file>:</p>
      <listing>
<code>
  &lt;action id="org.freedesktop.udisks2.modify-device"&gt;
     &lt;description&gt;Modifica la configuració de la unitat&lt;/description&gt;
     &lt;message&gt;Cal autentificar-se per modificar la configuració de la unitat settings&lt;/message&gt;
    &lt;defaults&gt;
      &lt;allow_any&gt;no&lt;/allow_any&gt;
      &lt;allow_inactive&gt;no&lt;/allow_inactive&gt;
      &lt;allow_active&gt;yes&lt;/allow_active&gt;
    &lt;/defaults&gt;
&lt;/action&gt;
</code>
      </listing>
      <p>Reemplaceu <code>no</code> per <code>auth_admin</code> si voleu assegurar-vos que només l'usuari root podrà dur a terme l'acció.</p>
    </item>
    <item>
      <p>Desar els canvis.</p>
    </item>
  </steps>

  <p>Quan l'usuari intenta canviar les opcions de disc, es mostra el següent missatge: <gui>Cal autentificar-se per modificar la configuració del disc</gui>.</p>

</page>
