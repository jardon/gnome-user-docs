<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-banner" xml:lang="ca">

  <info>
    <link type="guide" xref="login#appearance"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Matthias Clasen</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostrar text extra a la pantalla d'autentificació.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Mostrar un text com a bàner a la pantalla d'autentificació</title>

  <p>Podeu mostrar text extra a la pantalla d'autentificació, com per exemple amb qui contactar per a obtenir suport, activant les claus GSettings <sys>org.gnome.login-screen.banner-message-enable</sys> i <sys>org.gnome.login-screen.banner-message-text</sys> .</p>

  <steps>
    <title>Mostrar un text com a bàner a la pantalla d'autentificació:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item><p>Creeu una clau <sys>gdm</sys> per a la configuració de l'equip a <file its:translate="no">/etc/dconf/db/gdm.d/01-banner-message</file>:</p>
      <code its:translate="no">[org/gnome/login-screen]
banner-message-enable=true
banner-message-text='<input its:translate="yes">Escriu el text del bàner aquí.</input>'
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>No hi ha límit de caràcters per al missatge del bàner. <sys> gnome-shell </sys> autodetecta extensions de text més llargues i entra en el mode de dues columnes.</p>
    <p>El missatge del bàner no es pot llegir des d'un fitxer extern.</p>
  </note>

</page>
