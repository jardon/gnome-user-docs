<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="nl">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hoe kan ik een autostarttoepassing toevoegen voor alle gebruikers?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Een autostarttoepassing toevoegen voor alle gebruikers</title>

  <p>To start an application automatically when the user logs in,
  you need to create a <file>.desktop</file> file for that application in the
  <file>/etc/xdg/autostart/</file> directory.</p>

<steps>
  <title>Om een autostarttoepassing toe te voegen voor alle gebruikers:</title>
  <item><p>Maak een <file>.desktop</file>-bestand aan in de map <file>/etc/xdg/autostart/</file>:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Bestanden</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Vervang <var>Bestanden</var> door de naam van de toepassing.</p></item>
  <item><p>Vervang <var>nautilus -n</var> door de opdracht waarmee u de toepassing wilt uitvoeren..</p></item>
  <item><p>Met de sleutel <code>AutostartCondition</code> kunt u de waarde van een GSettings-sleutel controleren.</p>
  <p>Het sessiebeheer voert de toepassing automatisch uit als de waarde van de sleutel ‘waar’ is. Als de waarde van de sleutel verandert tijdens de sessie zal het sessiebeheer de toepassing starten of stoppen, afhankelijk van de voorgaande waarde van de sleutel.</p>
</item>
</steps>

</page>
