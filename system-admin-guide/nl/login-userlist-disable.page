<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-userlist-disable" xml:lang="nl">

  <info>
    <link type="guide" xref="login#appearance"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Laat gebruikers hun gebruikersnaam intypen op het aanmeldingsscherm.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>De gebruikerslijst uitschakelen</title>

  <p>U kunt de gebruikerslijst weergegeven op het aanmeldingsscherm uitschakelen door de GSettings-sleutel <sys>org.gnome.login-screen.disable-user-list</sys> in te stellen.</p>
  <p>Wanneer de gebruikerslijst uitgeschakeld is moeten gebruikers hun gebruikersnaam en wachtwoord intypen om zich aan te melden.</p>
  <steps>
    <title>De sleutel org.gnome.login-screen.disable-user-list instellen</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item>
      <p>Maak een <sys>gdm</sys>-sleutelbestand voor machinebrede instellingen aan in <file>/etc/dconf/db/gdm.d/00-login-screen</file>:</p>
        <code>[org/gnome/login-screen]
# Geef de gebruikerslijst niet weer
disable-user-list=true
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
