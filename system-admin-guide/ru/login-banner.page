<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-banner" xml:lang="ru">

  <info>
    <link type="guide" xref="login#appearance"/>
    <!--<link type="seealso" xref="gdm-restart"/>-->
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Матиас Класен (Matthias Clasen)</name>
      <email>matthias.clasen@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Показывать дополнительный текст на экране входа в систему.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Отображение текстового баннера на экране входа в систему</title>

  <p>Вы можете показать дополнительный текст на экране входа в систему, например, к кому следует обратиться за технической поддержкой, установив необходимые параметры ключей GSettings <sys>org.gnome.login-screen.banner-message-enable</sys> и <sys>org.gnome.login -screen.banner-message-text</sys> .</p>

  <steps>
    <title>Отображение текстового баннера на экране входа в систему:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item><p>Создайте файл ключа <sys>gdm</sys> для общесистемных настроек в <file its:translate="no">/etc/dconf/db/gdm.d/01-banner-message</file>:</p>
      <code its:translate="no">[org/gnome/login-screen]
banner-message-enable=true
banner-message-text='<input its:translate="yes">Здесь введите текст баннерного сообщения.</input>'
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>Для баннерного сообщения нет ограничения на количество слов. <sys>gnome-shell</sys> автоматически определяет длинные текстовые фрагменты и переходит в двухколоночный режим.</p>
    <p>Баннерное сообщение не может быть прочитано из внешнего файла.</p>
  </note>

</page>
