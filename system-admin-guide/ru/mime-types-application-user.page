<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="ru">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Настройка приложения, которое открывает данные определенного типа MIME для отдельного пользователя.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

    <title>Переопределить зарегистрированное по умолчанию приложение для отдельных пользователей</title>

    <p>Файлы <file>/usr/share/applications/mimeapps.list</file> и <file>/usr/share/applications/gnome-mimeapps.list</file> указывают, какое приложение зарегистрировано для открытия определенных типов MIME по умолчанию. Эти файлы предоставляются дистрибутивом.</p>
    <p>Чтобы переопределить системные значения, установленные по умолчанию для отдельных пользователей, необходимо создать файл <file>~/.config/mimeapps.list</file> со списком типов MIME, для которых вы хотите переопределить зарегистрированное по умолчанию приложение.</p>
    <steps>
      <title>Переопределить зарегистрированное по умолчанию приложение для отдельных пользователей</title>
      <item>
        <p>Ознакомьтесь с файлом <file>/usr/share/applications/mimeapps.list</file>, чтобы определить типы MIME, для которых вы хотите изменить зарегистрированное по умолчанию приложение. Например, в следующем образце файла <file>mimeapps.list</file> указано зарегистрированное по умолчанию приложение для типов MIME <code>text/html</code> и <code>application/xhtml+xml</code>:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Приложение по умолчанию (<app>Epiphany</app>) определяется указанием соответствующего файла <file>.desktop</file> (<file>epiphany.desktop</file>). Системное расположение по умолчанию для файлов <file>.desktop</file> других приложений находится в каталоге <file>/usr/share/applications/</file>. Файлы <file>.desktop</file> отдельных пользователей могут храниться в <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Создайте файл <file>~/.config/mimeapps.list</file>. В файле укажите типы MIME и соответствующие им зарегистрированные по умолчанию приложения:</p>
        <code>[Default Applications]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Added Associations]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>Это действие установит зарегистрированное приложение для использования по умолчанию для типа MIME <code>text/html</code> в <code>myapplication1.desktop</code> и для <code>application/xhtml+xml</code> типа MIME в <code>myapplication2.desktop</code>.</p>
        <p>Чтобы эти настройки работали правильно, убедитесь, что файлы <file>myapplication1.desktop</file> и <file>myapplication2.desktop</file> размещены в каталоге <file>/usr/share/applications/</file>. Файлы <file>.desktop</file> отдельных пользователей могут храниться в <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Вы можете использовать команду <cmd>gio mime</cmd>, чтобы убедиться, что зарегистрированное по умолчанию приложение установлено правильно:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
	epiphany.desktop
Recommended applications:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
