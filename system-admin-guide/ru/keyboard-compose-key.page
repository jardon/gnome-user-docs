<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-compose-key" xml:lang="ru">

  <info>
    <link type="guide" xref="user-settings"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Включить клавишу «compose» по умолчанию для всех пользователей.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Включить клавишу «compose»</title>

  <p>Чтобы включить клавишу «Compose» и настроить определенную клавишу на клавиатуре в качестве клавиши «Compose», установите ключ GSettings <sys>org.gnome.desktop.input-sources.xkb-options</sys>. Таким образом, этот параметр будет включён по умолчанию для всех пользователей вашей системы.</p>

  <steps>
    <title>Установить правую клавишу Alt в качестве клавиши «Compose»</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Создайте базу данных <sys>local</sys> для общесистемных настроек в <file>/etc/dconf/db/local.d/00-input-sources</file>:</p>
        <code>[org/gnome/desktop/input-sources]
# Установить правую клавишу Alt в качестве клавиши «Compose» и включите её
xkb-options=['compose:<var>ralt</var>']</code>
      <p>Если вы хотите установить клавишу, отличную от правой <key>Alt</key>, замените <var>ralt</var> именем этой клавиши, как указано на справочной странице <link href="man:xkeyboard-config"> <cmd>xkeyboard-config</cmd>(7)</link>, раздел <em>Положение клавиши Compose</em>.</p>
    </item>
    <item>
      <p>Переопределите пользовательскую настройку и запретите пользователю внесение изменений в <file>/etc/dconf/db/local.d/locks/input-sources</file>:</p>
      <code># Заблокировать список включённых параметров XKB
/org/gnome/desktop/input-sources/xkb-options
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
