<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-userlist-disable" xml:lang="ru">

  <info>
    <link type="guide" xref="login#appearance"/>
    <revision pkgversion="3.11" date="2014-01-29" status="draft"/>
    <revision pkgversion="3.14" date="2014-06-17" status="review"/>

    <credit type="author copyright">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Обязать пользователей вводить своё имя пользователя на экране входа в систему.</desc>
   
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Отключение списка пользователей</title>

  <p>Вы можете отключить отображение списка пользователей на экране входа в систему, установив ключ GSettings <sys>org.gnome.login-screen.disable-user-list</sys>.</p>
  <p>Когда список пользователей отключён, при появлении запроса на вход в систему пользователям будет необходимо вводить вручную своё имя и пароль.</p>
  <steps>
    <title>Установка значений ключа org.gnome.login-screen.disable-user-list</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-gdm'])"/>
    <item>
      <p>Создайте файл ключа <sys>gdm</sys> для общесистемных настроек в <file>/etc/dconf/db/gdm.d/00-login-screen</file>:</p>
        <code>[org/gnome/login-screen]
# Не показывать список пользователей
disable-user-list=true
</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

</page>
