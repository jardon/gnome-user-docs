<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-command-line" xml:lang="ru">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="guide" xref="user-settings#lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Петр Ковар (Petr Kovar)</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author copyright">
      <name>Яна Сварова (Jana Svarova)</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Запретить пользователям доступ к командной строке.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Отключить доступ к командной строке</title>

  <p>Чтобы отключить доступ к командной строке для пользователя, вам необходимо внести изменения в ряде контекстов. Имейте в виду, что следующие шаги не удаляют доступ пользователя к командной строке, а скорее устраняют способы, которыми пользователь может получить доступ к командной строке.</p>

  <list>
    <item>
      <p>Установите ключ GSettings <code>org.gnome.desktop.lockdown.disable-command-line</code>, что не позволит пользователю получить доступ к терминалу или указать командную строку для выполнения (комбинация клавиш <keyseq><key>Alt </key> <key>F2</key></keyseq>).</p>
    </item>
    <item>
      <p>Запретите пользователям доступ к командной строке посредством комбинации клавиш <keyseq><key>Alt</key><key>F2</key> </keyseq>.</p>
    </item>
    <item>
      <p>Отключите переключение на виртуальные терминалы (ВТ) с помощью комбинаций клавиш <keyseq> <key>Ctrl</key><key>Alt</key><key><var>функциональная клавиша</var></key></keyseq> путем изменения конфигурации X-сервера.</p>
    </item>
    <item>
      <p>Удалите <app>Терминал</app> и все другие приложения-терминалы из <gui>Обзора</gui> в GNOME Shell. Вам также необходимо запретить пользователю устанавливать новые приложения-терминалы.</p>
    </item>
  </list>

<section id="command-prompt">
  <title>Отключить командную строку</title>

  <steps>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <item>
      <p>Создайте базу данных <sys>local</sys> для общесистемных настроек в <file>/etc/dconf/db/local.d/00-lockdown</file>:</p>
      <code># Указать путь dconf
[org/gnome/desktop/lockdown]

# Отключить командную строку
disable-command-line=true</code>
    </item>
    <item>
      <p>Переопределите пользовательскую настройку и запретите пользователю внесение изменений в <file>/etc/dconf/db/local.d/locks/lockdown</file>:</p>
      <code># Перечислить ключи, используемые для настройки блокировки
/org/gnome/desktop/lockdown/disable-command-line</code>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>
</section>

<section id="virtual-terminal">
  <title>Отключить переход на виртуальный терминал</title>

  <p>Обычно пользователи могут использовать комбинации клавиш <keyseq><key>Ctrl</key><key>Alt</key><key><var>функциональная клавиша</var></key></keyseq> (например, <keyseq><key>Ctrl</key><key>Alt</key><key>F2</key></keyseq>), чтобы переключиться с рабочего стола GNOME на виртуальный терминал.</p>

  <p>Если на компьютере запущена <em>X Window System</em>, вы можете отключить доступ ко всем виртуальным терминалам, добавив параметр <code>DontVTSwitch</code> в раздел <code>Serverflags</code> файла настроек X в каталоге <file>/etc/X11/xorg.conf.d/</file>.</p>

  <steps>
    <item>
      <p>Создайте или отредактируйте файл настроек X в <file>/etc/X11/xorg.conf.d/</file>. Например, <file>/etc/X11/xorg.conf.d/10-xorg.conf</file>:</p>
    <listing>
    <title><file>/etc/X11/xorg.conf.d/10-xorg.conf</file></title>
<code>Section "Serverflags"

Option "DontVTSwitch" "yes"

EndSection
</code>
    </listing>
    </item>
    <item>
      <p>Перезапустите X-сервер, чтобы изменения вступили в силу.</p>
    </item>
  </steps>

</section>

<!-- TODO: add section for removing applications from the Activities overview. -->
</page>
