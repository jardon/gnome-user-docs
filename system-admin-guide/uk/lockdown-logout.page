<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="lockdown-logout" xml:lang="uk">

  <info>
    <link type="guide" xref="user-settings#lockdown"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Заборона користувачу виходу з системи і перемикання користувача.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Вимикання виходу користувача та перемикання користувача</title>

  <p>Запобігання виходу користувача з системи є корисним для особливих випадків розгортання GNOME (автоматизованих кіосків, громадських терміналів доступу до інтернету тощо).</p>

  <note style="important">
  <p>Користувачі можуть обходити блокування виходу з системи перемиканням на іншого користувача. Ось чому рекомендуємо також вимкнути <em>перемикання користувача</em> при налаштовуванні системи.</p>
  </note>

  <steps>
  <title>Вимикання виходу користувача та перемикання користувача</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
  <item>
  <p>Створіть файл ключів <file>/etc/dconf/db/local.d/00-logout</file> для надання відомостей для бази даних <sys>local</sys>:</p>
<screen>
[org/gnome/desktop/lockdown]
# Забороняємо користувачу виходити з системи
disable-log-out=true

# Забороняємо користувачу перемикати користувача
disable-user-switching=true
</screen>
  </item>
  <item>
  <p>Перевизначення параметра користувача і заборона користувачеві внесення змін у <file>/etc/dconf/db/local.d/locks/lockdown</file>:</p>
<screen>
# Блокуємо вихід користувача з системи
/org/gnome/desktop/lockdown/disable-log-out

# Блокуємо перемикання користувача
/org/gnome/desktop/lockdown/disable-user-switching
</screen>
  </item>
  <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  <item>
  <p>Щоб загальносистемні параметри було застосовано, слід перезапустити систему.</p>
  </item>
  </steps>

</page>
