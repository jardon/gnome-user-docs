<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dconf-custom-defaults" xml:lang="uk">

  <info>
    <link type="guide" xref="setup"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <link type="seealso" xref="dconf"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email>desrt@desrt.ca</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Встановлення загальносистемних типових параметрів за допомогою профілів <sys its:translate="no">dconf</sys>.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовувані типові значення для параметрів системи</title>

  <p>Загальносистемні типові параметри можна встановити задавши типове значення для ключа у профілі <sys its:translate="no">dconf</sys>. Ці типові значення може бути перевизначено користувачем.</p>

<section id="example">
  <title>Встановлення типового значення</title>

  <p>Щоб встановити типове значення для ключа, має існувати профіль <sys>user</sys>, а значення ключа має бути додано до бази даних <sys its:translate="no">dconf</sys>.</p>

  <steps>
    <title>Приклад встановлення типового фонового зображення</title>
    <item>
      <p>Створіть профіль <file its:translate="no">user</file>:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/profile/user</file></title>
<code>
user-db:user
system-db:local
</code>
      </listing>
      <p><input its:translate="no">local</input> — назва бази даних <sys its:translate="no">dconf</sys>.</p>
    </item>
    <item>
      <p>Створіть <em>файл ключа</em> для бази даних <input its:translate="no">local</input>, які містить типове значення параметра:</p>
      <listing its:translate="no">
        <title><file>/etc/dconf/db/local.d/01-background</file></title>
<code>
# <span its:translate="yes">шлях dconf</span>
[org/gnome/desktop/background]

# <span its:translate="yes">назви ключів dconf і їхні значення</span>
picture-uri='file:///usr/local/share/backgrounds/wallpaper.jpg'
picture-options='scaled'
primary-color='000000'
secondary-color='FFFFFF'
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
  </steps>

  <note>
    <p>При створенні або зміні профілю <sys its:translate="no">user</sys> користувач має вийти з системи і увійти до неї знову, перш ніж зміни набудуть чинності.</p>
  </note>

  <p>Якщо ви хочете уникнути створення профілю <sys its:translate="no">user</sys>, ви можете скористатися інструментом командного рядка <cmd>dconf</cmd> для читання і запису окремих значень або цілих каталогів бази даних <sys its:translate="no">dconf</sys>. Щоб дізнатися більше, ознайомтеся із сторінкою підручника <link its:translate="no" href="man:dconf"><cmd>dconf</cmd>(1)</link>.</p>

</section>

</page>
