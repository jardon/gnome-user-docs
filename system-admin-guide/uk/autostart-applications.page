<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="uk">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Як додати програми для автоматичного запуску для усіх користувачів?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Додавання автоматичного запуску програми для усіх користувачів</title>

  <p>Щоб програма автоматично запускалася при вході користувача до системи, вам слід створити файл <file>.desktop</file> для цієї програми у каталозі <file>/etc/xdg/autostart/</file>.</p>

<steps>
  <title>Щоб додати автоматичний запуск програми для усіх користувачів, виконайте такі дії:</title>
  <item><p>Створіть файл <file>.desktop</file> у каталозі <file>/etc/xdg/autostart/</file> із таким вмістом:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Files</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Замініть <var>Files</var> на назву програми.</p></item>
  <item><p>Замініть <var>nautilus -n</var> на команду, якою ви хочете скористатися для запуску програми.</p></item>
  <item><p>Ви можете скористатися ключем <code>AutostartCondition</code> для перевірки значення ключа GSettings.</p>
  <p>Засіб керування сеансами автоматично запускає програму, якщо значенням ключа є true. Якщо значення ключа змінюється протягом поточного сеансу, засіб керування сеансами запускає або зупиняє програму, залежно від попереднього значення ключа.</p>
</item>
</steps>

</page>
