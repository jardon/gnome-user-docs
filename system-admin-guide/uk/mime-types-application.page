<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application" xml:lang="uk">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-application-user"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Налаштовування програми, яка відкриває дані певного типу MIME.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

    <title>Перевизначення типової зареєстрованої програми для усіх користувачів</title>
    <p>Файли <file>/usr/share/applications/mimeapps.list</file> і <file>/usr/share/applications/gnome-mimeapps.list</file> визначають, яку програму зареєстровано для типового відкриття певних типів MIME. Ці файли надаються дистрибутивом.</p>

      <p>Щоб перевизначити типові параметри системи для усіх користувачів системи, вам слід створити файл <file>/etc/xdg/mimeapps.list</file> або <file>/etc/xdg/gnome-mimeapps.list</file> зі списком типів MIME, для яких ви хочете перевизначити типову зареєстровану програму.</p>

      <note>
      <p>Файли, які зберігаються в <file>/etc/xdg/</file> мають вищий пріоритет за файли, які зберігаються у <file>/usr/share/applications/</file>. Крім того, <file>/etc/xdg/gnome-mimeapps.list</file> є пріоритетнішим за <file>/etc/xdg/mimeapps.list</file>, але його можна перевизначити налаштуваннями користувача у <file>~/.config/mimeapps.list</file>.</p>
      </note>

    <steps>
      <title>Перевизначення типової зареєстрованої програми для усіх користувачів</title>
      <item>
        <p>Ознайомтеся із вмістом файла <file>/usr/share/applications/mimeapps.list</file>, щоб визначити типи MIME, для яких ви хочете змінити типову зареєстровану програму. Наприклад, у наведеному нижче зразку файла <file>mimeapps.list</file> вказано типову зареєстровану програму для типів MIME <code>text/html</code> і <code>application/xhtml+xml</code>:</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>Типову програму (<app>Epiphany</app>) визначено вказаним відповідним файлом <file>.desktop</file> (<file>epiphany.desktop</file>). Типовим розташуванням файлів <file>.desktop</file> інших програм є <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Створіть файл <file>/etc/xdg/mimeapps.list</file>. У файлі вкажіть типи MIME і їхні відповідні типові зареєстровані програми:</p>
        <code>[Default Applications]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Added Associations]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>Це змінюється типову зареєстровану програму для типу MIME <code>text/html</code> на <code>myapplication1.desktop</code>, а типову зареєстровану програму для типу MIME <code>application/xhtml+xml</code> на <code>myapplication2.desktop</code>.</p>
        <p>Щоб ці параметри працювали належним чином, обидва файли, <file>myapplication1.desktop</file> і <file>myapplication2.desktop</file>, мають зберігатися у каталозі <file>/usr/share/applications/</file>.</p>
      </item>
      <item>
        <p>Ви можете скористатися командою <cmd>gio mime</cmd> для перевірки того, що типову зареєстровану програму було встановлено належним чином:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
	epiphany.desktop
Recommended applications:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
