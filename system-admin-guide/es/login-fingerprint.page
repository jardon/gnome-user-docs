<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-fingerprint" xml:lang="es">

  <info>
    <link type="guide" xref="login#management"/>
    <link type="guide" xref="user-settings#lockdown"/>
    <revision pkgversion="3.12" date="2014-06-17" status="candidate"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Evitar que el usuario pueda iniciar sesión usando un lector de huella digital.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>No permitir el inicio de sesión con huella dactilar</title>

  <p>Los usuarios con un lector de huella digital puede usar sus huellas digitales en vez de una contraseña para iniciar sesión. El inicio de sesión con huella digital <link href="help:gnome-help#session-fingerprint"> requiere configuración por parte del usuario</link> antes de poder usarlo.</p>

  <p>Los lectores de huellas dactilares no son siempre fiables, por lo tanto, puede querer desactivar el inicio de sesión usando el lector por razones de seguridad.</p>

  <steps>
    <title>Desactivar el inicio de sesión usando un lector de huellas dactilares:</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Cree el archivo <file>/etc/dconf/db/local.d/00-login</file> para proporcionar información a la base de datos <sys>local</sys>.</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/00-login</file></title>
<code>
# Specify the dconf path
[org/gnome/login-screen]

# Disable fingerprint reader
enable-fingerprint-authentication=false
</code>
      </listing>
    </item>
    <item>
      <p>Para evitar que un usuario pueda cambiar estos ajustes, cree el archivo <file>/etc/dconf/db/local.d/locks/login</file> con el siguiente contenido:</p>
      <listing>
        <title><file>/etc/dconf/db/local.d/locks/fingerprintreader</file></title>
<code>
# List the keys used to configure login
/org/gnome/login-screen/enable-fingerprint-authentication
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
