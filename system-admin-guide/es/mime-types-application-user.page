<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="es">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Personalizar qué aplicación debe abrir un tipo MIME específico de manera predeterminada por cada usuario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

    <title>Ignorar la aplicación registrada predeterminada para usuarios individuales</title>

    <p>Los archivos <file>/usr/share/applications/mimeapps.list</file> y <file>/usr/share/applications/gnome-mimeapps.list</file> especifican qué aplicación está registrada para abrir tipos MIME específicos de forma predeterminada. Estos archivos los aporta la distribución.</p>
    <p>Para ignorar la configuración predeterminada del sistema por usuarios individuales, necesita crear un archivo <file>~/.config/mimeapps.list</file> con una lista de los tipos MIME para los que quiera ignorar la aplicación registrada predeterminada.</p>
    <steps>
      <title>Ignorar la aplicación registrada predeterminada para usuarios individuales</title>
      <item>
        <p>Consulte el archivo <file>/usr/share/applications/mimeapps.list</file> para determinar los tipos MIME para los que quiere modificar la aplicación predeterminada registrada. Por ejemplo, el siguiente ejemplo del archivo <file>mimeapps.list</file> especifica la aplicación predeterminada registrada para los tipos MIME <code>text/html</code> y <code>application/xhtml+xml</code>.</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>La aplicación predeterminada (<app>Epiphany</app>) se define especificando su archivo <file>.desktop</file> correspondiente (<file>epiphany.desktop</file>).La localización predeterminada del sistema de los archivos <file>.desktop</file> de otras aplicaciones es <file>/usr/share/applications/</file>. Los archivos <file>.desktop</file> de usuarios individuales se pueden guardar en <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Cree el archivo <file>~/.config/mimeapps.list</file>. En el archivo, especifique los tipos MIME y sus aplicaciones registradas predeterminadas.</p>
        <code>[Default Applications]
text/html=<var>myapplication1.desktop</var>
application/xhtml+xml=<var>myapplication2.desktop</var>

[Added Associations]
text/html=<var>myapplication1.desktop</var>;
application/xhtml+xml=<var>myapplication2.desktop</var>;</code>
      <p>Esto cambia la aplicación registrada predeterminada para el tipo MIME <code>text/html</code> a <code>myapplication1.desktop</code>, y la aplicación registrada predeterminada para el tipo MIME <code>application/xhtml+xml</code> a <code>myapplication2.desktop</code>.</p>
        <p>Para que estos ajustes funcionen correctamente, asegúrese que tanto el archivo <file>myapplication1.desktop</file> como el archivo <file>myapplication2.desktop</file> están localizados en la carpeta <file>/usr/share/applications/</file>. Los archivos <file>.desktop</file> del usuario se pueden guardar en la carpeta <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Puede utilizar el comando <cmd>gio mime</cmd> para verificar que la aplicación registrada predeterminada ha sido configurada correctamente:</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Default application for “text/html”: myapplication1.desktop
Registered applications:
	myapplication1.desktop
	epiphany.desktop
Recommended applications:
	myapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
