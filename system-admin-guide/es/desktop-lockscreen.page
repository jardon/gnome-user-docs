<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-lockscreen" xml:lang="es">

  <info>
    <link type="guide" xref="appearance"/>
    <link type="seealso" xref="dconf-profiles"/>
    <link type="seealso" xref="dconf-lockdown"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <desc>Hace que la pantalla se bloquee automáticamente para que el usuario tenga que introducir su contraseña después de estar inactivo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Bloquear la pantalla cuando el usuario esté inactivo</title>

  <p>Puede hacer que la pantalla se bloquee automáticamente cuando un usuario está inactivo durante cierto tiempo. Esto es útil si sus usuarios pueden dejar sus ordenadores desatendidos en lugares públicos o inseguros.</p>

  <steps>
    <title>Activar el bloqueo de pantalla automático</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Cree el archivo <file>/etc/dconf/db/local.d/00-screensaver</file> para proporcionar información a la base de datos <sys>local</sys>.</p>
      <listing>
      <title><file>/etc/dconf/db/local.d/00-screensaver</file></title>
<code>
# Especificar la ruta dconf
[org/gnome/desktop/session]

# Número de segundos de inactividad antes de que la pantalla quede en blanco
# Especificar 0 segundos si quiere desactivar el salvapantallas
idle-delay=uint32 180

# Especificar la ruta dconf
[org/gnome/desktop/screensaver]

# Número de segundos para bloquear la pantalla después de que la pantalla quede en blanco.
lock-delay=uint32 0
</code>
      </listing>
      <p>Debe incluir el <code>uint32</code> junto a los valores enteros de las claves como se muestra.</p>
    </item>
    <item>
      <p>Para prevenir que el usuario ignore estas preferencias, cree el archivo <file>/etc/dconf/db/local.d/locks/screensaver</file> con el siguiente contenido:</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/screensaver</file></title>
<code>
# Bloquear los ajustes del protector de pantalla
/org/gnome/desktop/session/idle-delay
/org/gnome/desktop/screensaver/lock-delay
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</page>
