<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="autostart-applications" xml:lang="es">

  <info>
    <link type="guide" xref="software#management"/>
    <revision pkgversion="3.30" date="2019-02-08" status="draft"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>¿Cómo puedo añadir una aplicación que se inicie automáticamente para todos los usuarios?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2017 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oliver Gutiérrez</mal:name>
      <mal:email>ogutsua@gmail.com</mal:email>
      <mal:years>2018 - 2020</mal:years>
    </mal:credit>
  </info>

  <title>Añadir una aplicación que se inicie automáticamente para todos los usuarios</title>

  <p>Para iniciar una aplicación automaticamente cuando el usuario inicia sesión, debe crear un archivo <file>.desktop</file> para esa aplicación en la carpeta <file>/etc/xdg/autostart/</file>.</p>

<steps>
  <title>Para añadir una aplicación que se inicie automáticamente para todos los usuarios:</title>
  <item><p>Cree un archivo <file>.desktop</file> en <file>/etc/xdg/autostart/</file>:</p>
<code>[Desktop Entry]
Type=Application
Name=<var>Archivos</var>
Exec=<var>nautilus -n</var>
OnlyShowIn=GNOME;
AutostartCondition=<var>GSettings org.gnome.desktop.background show-desktop-icons</var></code>
  </item>
  <item><p>Reemplace <var>Archivos</var> por el nombre de la aplicación.</p></item>
  <item><p>Reemplace <var>nautilus -n</var> por el comando que quiera para ejecutar la aplicación.</p></item>
  <item><p>Puede usar la clave <code>AutostartCondition</code> para comprobar el valor de una clave GSettings.</p>
  <p>El administrador de sesiones ejecuta la aplicación automáticamente si el valor de la clave es verdadero. Si el valor de la clave cambia durante la sesión en curso, el administrador de sesiones inicia o detiene la aplicación, dependiendo del valor que tenía la clave previamente.</p>
</item>
</steps>

</page>
