<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="login-automatic" xml:lang="fr">

  <info>
    <link type="guide" xref="login#management"/>
    <revision pkgversion="3.4.2" date="2012-12-01" status="review"/>
    <revision pkgversion="3.8" date="2013-04-04" status="review"/>
    <revision pkgversion="3.12" date="2014-06-18" status="review"/>
    
    <credit type="author copyright">
      <name>minnie_eg</name>
      <email>amany.elguindy@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Connecter automatiquement un compte utilisateur au démarrage.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Thibault Leclair</mal:name>
      <mal:email>thibaultleclair@yahoo.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mathieu Bousquet</mal:name>
      <mal:email>mathieu.bousquet2@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Irénée Thirion</mal:name>
      <mal:email>irenee.thirion@e.email</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Configurer la connexion automatique</title>

  <p its:locNote="TRANSLATORS: 'Administrator' and 'Automatic Login' are   strings from the Users dialog in the Settings panel user interface.">Un utilisateur disposant des droits <em>Administrateur</em> peut <link href="help:gnome-help/user-autologin">activer la <em>connexion automatique</em> à partir de <app>Paramètres</app></link>. Vous pouvez également configurer manuellement la connexion automatique dans le fichier de configuration personnalisé <sys its:translate="no">GDM</sys>. Pour cela :</p>
  
  <steps>
    <item>
      <p>Modifiez le fichier <file its:translate="no">/etc/gdm/custom.conf</file> et assurez-vous que la section <code>[daemon]</code> du fichier contienne :</p>
      <listing its:translate="no">
        <title><file>custom.conf</file></title>
<code>
[daemon]
AutomaticLoginEnable=<input>True</input>
AutomaticLogin=<input>username</input>
</code>
      </listing>
    </item>
    <item>
      <p>Remplacez <input its:translate="no">username</input> par l’utilisateur qui sera connecté automatiquement</p>
    </item>
  </steps>

  <note>
    <p>Le fichier <file its:translate="no">custom.conf</file> est généralement situé dans <file its:translate="no">/etc/gdm/</file>, mais l’emplacement peut différer selon votre distribution Linux.</p>
  </note>

</page>
