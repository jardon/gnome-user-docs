<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mime-types-application-user" xml:lang="fr">

  <info>
    <link type="guide" xref="software#management"/>
    <link type="seealso" xref="mime-types-custom-user"/>
    <link type="seealso" xref="mime-types-custom"/>
    <revision pkgversion="3.30" date="2019-02-08" status="review"/>

    <credit type="author copyright">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Personnaliser, par utilisateur, l’application ouvrant un type MIME spécifique.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Thibault Leclair</mal:name>
      <mal:email>thibaultleclair@yahoo.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mathieu Bousquet</mal:name>
      <mal:email>mathieu.bousquet2@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Irénée Thirion</mal:name>
      <mal:email>irenee.thirion@e.email</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

    <title>Remplacer les applications par défaut d’un utilisateur</title>

    <p>Les fichiers <file>/usr/share/applications/mimeapps.list</file> et <file>/usr/share/applications/gnome-mimeapps.list</file> spécifient l’application par défaut à utiliser pour ouvrir des types MIME spécifiques. Ces fichiers sont fournis par votre système d’exploitation.</p>
    <p>Pour remplacer les valeurs système par défaut d’un utilisateur, vous devez créer un fichier <file>~/.config/mimeapps.list</file> contenant une liste des types MIME pour lesquels remplacer l’application qui est utilisée par défaut.</p>
    <steps>
      <title>Remplacer les applications par défaut d’un utilisateur</title>
      <item>
        <p>Consultez le fichier <file>/usr/share/applications/mimeapps.list</file> pour déterminer les types MIME dont l’application par défaut est à changer. Par exemple, l’extrait suivant du fichier <file>mimeapps.list</file> spécifie l’application par défaut des types MIME <code>text/html</code> et <code>application/xhtml+xml</code> :</p>
        <code>[Default Applications]
text/html=epiphany.desktop
application/xhtml+xml=epiphany.desktop</code>
        <p>L’application par défaut (<app>Web</app>) est définie en spécifiant son fichier <file>.desktop</file> correspondant (<file>epiphany.desktop</file>). L’emplacement système par défaut des fichiers <file>.desktop</file> est <file>/usr/share/applications/</file>. Les fichiers <file>.desktop</file> d’un utilisateur peuvent être enregistrés dans <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Créez le fichier <file>~/.config/mimeapps.list</file>. Dans ce fichier, spécifiez les types MIME et leurs applications correspondantes utilisées par défaut :</p>
        <code>[Default Applications]
text/html=<var>monapplication1.desktop</var>
application/xhtml+xml=<var>monapplication2.desktop</var>

[Added Associations]
text/html=<var>monapplication1.desktop</var>;
application/xhtml+xml=<var>monapplication2.desktop</var>;</code>
      <p>Cet exemple défini <code>monapplication1.desktop</code> comme application par défaut pour le type MIME <code>text/html</code> et <code>monapplication2.desktop</code> comme application par défaut pour le type MIME <code>application/xhtml+xml</code>.</p>
        <p>Pour que ces paramètres fonctionnent correctement, assurez-vous que les fichiers <file>monapplication1.desktop</file> et <file>monapplication2.desktop</file> sont situés dans le répertoire <file>/usr/share/applications/</file>. Les fichiers <file>.desktop</file> d’un utilisateur peuvent être enregistrés dans <file>~/.local/share/applications/</file>.</p>
      </item>
      <item>
        <p>Vous pouvez utilisez la commande <cmd>gio mime</cmd> pour vérifier que l’application par défaut ai été correctement définie :</p>
        <screen><output>$ </output><input>gio mime text/html</input>
Application par défaut pour « text/html » : monapplication1.desktop
Applications inscrites :
	monapplication1.desktop
	epiphany.desktop
Applications recommandées :
	monapplication1.desktop
	epiphany.desktop</screen>
      </item>
    </steps>
</page>
