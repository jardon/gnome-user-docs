<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="desktop-favorite-applications" xml:lang="fr">

  <info>
    <link type="guide" xref="appearance"/>
    <revision pkgversion="3.30" date="2019-02-22" status="review"/>

    <credit type="author">
      <name>Aruna Sankaranarayanan</name>
      <email>aruna.evam@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
      <years>2019</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Personnaliser les favoris par défaut dans la vue d’ensemble des activités.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Thibault Leclair</mal:name>
      <mal:email>thibaultleclair@yahoo.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mathieu Bousquet</mal:name>
      <mal:email>mathieu.bousquet2@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Irénée Thirion</mal:name>
      <mal:email>irenee.thirion@e.email</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>Définir les applications favorites par défaut</title>

  <p>Les applications favorites sont celles visibles sur le <link href="help:gnome-help/shell-introduction#activities">Lanceur</link> du Shell de GNOME. Vous pouvez utiliser <sys its:translate="no">dconf</sys> pour définir les applications favorites d’un ou de tous les utilisateurs. Dans les deux cas, vous devrez préalablement modifier le profil <sys its:translate="no">dconf</sys> situé dans <file its:translate="no">/etc/dconf/profile</file>.</p>

<section id="per-user">
  <title>Définir des applications favorites différentes pour des utilisateurs différents</title>

  <p>Vous pouvez définir des applications favorites par défaut pour chaque utilisateur en modifiant leur base de données correspondante dans <file its:translate="no">~/.config/dconf/user</file>. L’exemple de code suivant utilise <sys its:translate="no">dconf</sys> pour définir <app>Gedit</app>, <app>Terminal</app> et <app>Fichiers</app> (<sys>Nautilus</sys> ) comme favoris par défaut pour un utilisateur. Il permet également aux utilisateurs de modifier la liste ultérieurement, s’ils le souhaitent.</p>

  <listing>
    <title>Contenu de <file its:translate="no">/etc/dconf/profile</file> :</title>
<code its:translate="no">
# <span its:translate="yes">Cette ligne permet à l’utilisateur de modifier ultérieurement les favoris par défaut</span>
user-db:user
</code>
  </listing>

  <listing>
    <title>Contenu de <file its:translate="no">~/.config/dconf/user</file> :</title>
<code its:translate="no">
# <span its:translate="yes">Définir Gedit, Terminal et Nautilus comme applications favorites par défaut</span>
[org/gnome/shell]
favorite-apps = [<var>'gedit.desktop'</var>, <var>'gnome-terminal.desktop'</var>, <var>'nautilus.desktop'</var>]
</code>
  </listing>

  <note style="tip">
    <p>Vous pouvez également <link xref="dconf-lockdown">verrouiller</link> les paramètres ci-dessus pour empêcher leurs modifications par les utilisateurs.</p>
  </note>

</section>

<section id="all-users">
  <title>Définir les mêmes applications favorites pour tous les utilisateurs</title>

  <p>Pour que tous les utilisateurs aient les mêmes favoris, vous devez modifier les fichiers de base de données système à l’aide des <link xref="dconf-keyfiles">fichiers de clés dconf</link>. Les étapes suivantes modifient le profil <sys its:translate="no">dconf</sys>, puis créent un fichier de clés définissant les applications favorites par défaut pour tous les utilisateurs dans la base de données de configuration <code>local</code>.</p>

  <steps>
    <title>Définir les applications favorites</title>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-profile-user-dir'])"/>
    <item>
      <p>Créez le fichier de clés <file>/etc/dconf/db/local.d/00-favorite-apps</file> pour fournir les informations à la base de données <sys>local</sys>.</p>
      <listing>
      <title>Contenu de <file its:translate="no">/etc/dconf/db/local.d/00-favorite-apps</file> :</title>
<code>
# Ce code définit Gedit, Terminal et Nautilus comme favoris par défaut pour tous les utilisateurs
[org/gnome/shell]
favorite-apps = [<var>'gedit.desktop'</var>, <var>'gnome-terminal.desktop'</var>, <var>'nautilus.desktop'</var>]
</code>
      </listing>
    </item>
    <item>
      <p>Pour empêcher l’utilisateur de remplacer ces paramètres, créez le fichier <file>/etc/dconf/db/local.d/locks/favorite-apps</file> avec le contenu suivant :</p>
      <listing>
      <title><file>/etc/dconf/db/local.db/locks/favorite-apps</file></title>
<code>
# Verrouiller les applications favorites par défaut
/org/gnome/shell/favorite-apps
</code>
      </listing>
    </item>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-update'])"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="dconf-snippets.xml" xpointer="xpointer(/*/*[@xml:id='dconf-logoutin'])"/>
  </steps>

</section>

</page>
