<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ändern Sie in den Einstellungen zur <gui>Bildschirmsperre</gui>, wie lange gewartet werden soll, bevor der Bildschirm gesperrt wird.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Der Bildschirm sperrt sich zu schnell selbst</title>

  <p>Wenn Sie Ihren Rechner für ein paar Minuten verlassen, sperrt sich der Bildschirm automatisch selbst, so dass Sie Ihr Passwort eingeben müssen, um ihn wieder zu benutzen. Dies geschieht aus Sicherheitsgründen (damit sich niemand an Ihren Sachen zu schaffen macht, wenn Sie den Rechner unbeaufsichtigt lassen), aber es kann lästig sein, wenn sich der Bildschirm zu schnell sperrt.</p>

  <p>So legen Sie eine längere Wartezeit vor dem automatischen Sperren des Bildschirms fest:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Screen Lock</gui>.</p>
    </item>

    <item>
      <p>Click on <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p>Wenn <gui>Automatische Bildschirmsperre</gui> aktiviert ist, können Sie den Wert in der Auswahlliste <gui>Verzögerung bis zur automatischen Bildschirmsperre</gui> ändern.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Wenn Sie nicht wollen, dass der Bildschirm jemals automatisch gesperrt wird, schalten Sie den Schalter für <gui>Automatische Bildschirmsperre</gui> aus.</p>
  </note>

</page>
