<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0 ui/1.0" id="screen-shot-record" xml:lang="de">
      
  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ein Foto oder Video Ihres Bildschirms aufnehmen, um zu zeigen, was auf Ihrem Bildschirm passiert.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

<title>Bildschirmfotos und Bildschirmaufzeichnungen</title>

  <list>
    <item><p>Erstellen Sie ein Foto von Ihrem Bildschirm oder einem Teil davon</p></item>
    <item><p>Senden Sie es als Datei oder fügen Sie es aus der Zwischenablage ein</p></item>
    <item><p>Erstellen Sie ein Video von Ihrem Bildschirm</p></item>
  </list>

  <media type="image" src="figures/screenshot-tool.png" width="500"/>

  <p>Sie können eine Aufnahme Ihres Bildschirms (ein <em>Bildschirmfoto</em>) aufnehmen oder ein Video der Abläufe auf dem Bildschirm aufzeichnen (eine <em>Bildschirmaufzeichnung</em>). Diese ist nützlich, um zum Beispiel Anderen zu zeigen, wie etwas auf dem Rechner erledigt wird. Bildschirmfotos und Bildschirmaufzeichnungen sind gewöhnliche Bild- und Video-Dateien, die per E-Mail versendet und im Internet zur Verfügung gestellt werden können.</p>

<section id="screenshot">
  <title>Ein Bildschirmfoto aufnehmen</title>

  <steps>
    <item>
      <p>Drücken Sie die <key>Druck</key>-Taste oder starten Sie <app>Ein Bildschirmfoto aufnehmen</app> aus der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht.</p>
    </item>
    <item>
      <p>Daraufhin überlagert der Bildschirmfoto-Dialog Ihren Bildschirm. Sie finden ein Rechteck mit Griffpunkten vor, mit denen Sie den Aufnahmebereich auswählen können. <media its:translate="no" type="image" src="figures/camera-photo-symbolic.svg"/> zeigt an, dass Sie sich im Bildschirmfoto-Modus (einzelnes Bild) befinden.</p>
       <note>
         <p>Klicken Sie auf den Knopf mit dem Mauszeiger, um den Mauszeiger in das Bildschirmfoto miteinzubeziehen.</p>
       </note>
    </item>
    <item>
      <p>Klicken und ziehen Sie den Bereich für das Bildschirmfoto mit den Griffpunkten oder dem Fadenkreuz-Zeiger.</p>
    </item>
    <item>
       <p>Um den ausgewählten Bereich aufzunehmen, klicken Sie auf den großen runden Knopf.</p>
    </item>
    <item>
       <p>Um den gesamten Bildschirm aufzunehmen, klicken Sie auf <gui>Bildschirm</gui> und klicken Sie anschließend auf den großen runden Knopf.</p>
    </item>
    <item>
       <p>Um ein Fenster aufzunehmen, klicken Sie auf <gui>Fenster</gui>. Es wird eine Übersicht aller offenen Fenster angezeigt, wobei das aktive Fenster angekreuzt ist. Klicken Sie auf ein Fenster, um es auszuwählen, und klicken Sie anschließend auf den großen runden Knopf.</p>
    </item>
  </steps>

  </section>
  
  <section id="locations">
  <title>Wo landen sie?</title>

  <list>
    <item>
      <p>Ein Bildschirmfoto wird automatisch im Ordner <file>Bilder/Bildschirmfotos</file> in Ihrem persönlichen Ordner gespeichert. Der Dateiname beginnt mit <file>Bildschirmfoto</file> und enthält das Datum und den Zeitpunkt der Aufnahme.</p>
    </item>
    <item>
      <p>Das Bild wird außerdem in der Zwischenablage gespeichert. Sie können es also sofort in ein Bildbearbeitungsprogramm einfügen oder in den sozialen Medien teilen.</p>
    </item>
    <item>
      <p>Das Video der Bildschirmaufzeichnung wird automatisch im Ordner <file>Videos/Bildschirmaufzeichnungen</file> in Ihrem persönlichen Ordner gespeichert. Der Dateiname beginnt mit <file>Bildschirmaufzeichnung</file> und enthält außerdem das Datum und den Zeitpunkt der Aufnahme.</p>
    </item>
  </list>
    
  </section>

<section id="screencast">
  <title>Eine Bildschirmaufzeichnung erstellen</title>

  <p>Sie können ein Foto oder Video Ihres Bildschirms aufnehmen, um zu zeigen, was dort passiert:</p>

  <steps>
    <item>
      <p>Drücken Sie die <key>Druck</key>-Taste oder starten Sie <app>Ein Bildschirmfoto aufnehmen</app> aus der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht.</p>
    </item>
    <item>
      <p>Klicken Sie auf <media its:translate="no" type="image" src="figures/camera-video-symbolic.svg"/>, um in den Bildschirmaufnahme-Modus zu wechseln.</p>
       <note>
         <p>Klicken Sie auf den Knopf mit dem Mauszeiger, um den Mauszeiger in die Bildschirmaufzeichnung miteinzubeziehen.</p>
       </note>
    </item>
    <item>
      <p>Wählen Sie <gui>Auswahl</gui> oder <gui>Bildschirm</gui>. Für <gui>Auswahl</gui> klicken und ziehen Sie den Bereich für die Bildschirmaufnahme, indem Sie die Griffpunkte oder den Fadenkreuz-Zeiger verwenden.</p>
    </item>
    <item>
      <p>Klicken Sie auf großen, runden, roten Knopf, um Ihren Bildschirm aufzunehmen.</p>
      <p>Ein rotes Hinweissymbol, das die verstrichenen Sekunden zeigt, wird in der rechten oberen Ecke des Bildschirms angezeigt, während die Aufzeichnung läuft.</p>
    </item>
    <item>
      <p>Sobald Sie fertig sind, klicken Sie auf das rote Hinweissymbol oder drücken Sie <keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>R</key></keyseq>, um die Aufzeichnung anzuhalten.</p>
    </item>
  </steps>

</section>
  
  <section id="keyboard-shortcuts">
    <title>Tastenkombinationen</title>

    <p>Wenn Sie die Bildschirmfoto-Funktion verwenden, stehen Ihnen diese Tastenkombinationen zur Verfügung:</p>

<table rules="rows" frame="top bottom">

  <tr>
    <td><p><key>S</key></p></td>
    <td><p>Bereich auswählen</p></td>
  </tr>
  <tr>
    <td><p><key>C</key></p></td>
    <td><p>Bildschirm aufnehmen</p></td>
  </tr>
  <tr>
    <td><p><key>W</key></p></td>
    <td><p>Fenster aufnehmen</p></td>
  </tr>
  <tr>
    <td><p><key>P</key></p></td>
    <td><p>Den Mauszeiger ein- oder ausblenden</p></td>
  </tr>
  <tr>
    <td><p><key>V</key></p></td>
    <td><p>Zwischen Bildschirmfoto und Bildschirmaufzeichnung umschalten</p></td>
  </tr>
  <tr>
    <td><p><key>Eingabetaste</key></p></td>
    <td><p>Aufnehmen, ebenfalls aktiviert durch <key>Leertaste</key> oder <keyseq><key>Strg</key><key>C</key></keyseq></p></td>
  </tr>
</table>

    <p>Mit diesen Tastenkombinationen können Sie die Bildschirmfoto-Funktion umgehen:</p>

<table rules="rows" frame="top bottom">
  <tr>
    <td><p><keyseq><key>Alt</key><key>Druck</key></keyseq></p></td>
    <td><p>Das Fenster aufnehmen, auf dem aktuell der Fokus liegt</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key><key>Druck</key></keyseq></p></td>
    <td><p>Den gesamten Bildschirm aufnehmen</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>R</key></keyseq></p></td>
    <td><p>Bildschirmaufzeichnung beginnen und beenden</p></td>
  </tr>
</table>

  </section>

</page>
