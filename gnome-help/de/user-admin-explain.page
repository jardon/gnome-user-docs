<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="user-admin-explain" xml:lang="de">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
        <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sie benötigen Systemverwalterrechte, um Änderungen an wichtigen Teilen Ihres Systems vorzunehmen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Wie funktionieren Systemverwalter-Privilegien?</title>

  <p>Neben den Dateien, die <em>Sie</em> erstellen, befindet sich auf Ihrem Rechner eine Reihe von Dateien, die vom System benötigt werden, damit es einwandfrei funktioniert. Wenn diese wichtigen <em>Systemdateien</em> unsachgemäß verändert werden, können sie allerlei Schaden anrichten, weswegen sie standardmäßig vor Änderungen geschützt sind. Bestimmte Anwendungen verändern auch wichtige Teile des Systems und sind somit ebenfalls geschützt.</p>

  <p>Sie werden in der Weise geschützt, dass nur Benutzer mit <em>Systemverwalterrechten</em> diese Dateien ändern oder Anwendungen benutzen können. Im alltäglichen Gebrauch werden Sie keine Systemdateien ändern oder diese Anwendungen benutzen müssen, deswegen haben Sie standardmäßig keine Systemverwalterrechte.</p>

  <p>Manchmal müssen Sie diese Anwendungen benutzen, deswegen können Sie vorübergehend Systemverwalterrechte erhalten, um die Änderungen durchführen zu können. Wenn eine Anwendung Systemverwalterrechte erfordert, fragt sie Sie nach Ihrem Passwort. Wenn Sie zum Beispiel eine neue Software installieren wollen, wird die Software-Installation (Paketmanager) Sie nach Ihrem Systemverwalter-Passwort fragen, damit er die neue Anwendung zum System hinzufügen kann. Ist dies geschehen, werden Ihnen die Systemverwalterrechte wieder entzogen.</p>

  <p>Systemverwalterrechte sind mit Ihrem Benutzerkonto verknüpft. Manche Benutzer dürfen als <gui>Systemverwalter</gui> fungieren, <gui>Standard</gui>-Benutzer nicht. Ohne Systemverwalterrechte dürfen Sie zum Beispiel keine Software installieren. Manche Benutzerkonten (zum Beispiel das »root«-Konto) haben dauerhafte Systemverwalterrechte. Sie sollten nicht die ganze Zeit Systemverwalterrechte benutzen, weil Sie sonst unabsichtlich eine wichtige Datei ändern oder etwas kaputt machen könnten, beispielsweise eine wichtige Systemdatei löschen.</p>

  <p>Kurzum: Systemverwalterrechte ermöglichen es Ihnen, wichtige Teile Ihres Systems zu ändern, wenn es notwendig ist, hindern Sie aber daran, dies versehentlich zu tun.</p>

  <note>
    <title>Was bedeutet »superuser«?</title>
    <p>Ein Benutzer mit Systemverwalterrechten wird manchmal <em>superuser</em> – also »Superbenutzer« – genannt, einfach weil dieser Benutzer mehr Rechte hat als normale Benutzer. Sie werden vielleicht einmal mitbekommen, dass Dinge wie <cmd>su</cmd> und <cmd>sudo</cmd> erörtert werden; das sind Programme zum vorübergehenden Verleihen von »superuser«-, also Systemverwalterrechten.</p>
  </note>

<section id="advantages">
  <title>Warum sind Systemverwalter-Privilegien sinnvoll?</title>

  <p>Die Erfordernis, dass Benutzer Systemverwalter-Privilegien vor dem Ändern wichtiger Systemeinstellungen erlangen müssen, ist sinnvoll, weil es Ihr System davor schützt, absichtlich oder unabsichtlich beschädigt zu werden.</p>

  <p>Wenn Sie die ganze Zeit Systemverwalterrechte hätten, könnten Sie unabsichtlich eine wichtige Datei ändern oder eine Anwendung starten, die versehentlich etwas Wichtiges ändert. Systemverwalterrechte nur vorübergehend und nur dann zu erhalten, wenn Sie sie brauchen, verringert das Risiko solcher Versehen.</p>

  <p>Nur bestimmten, vertrauenswürdigen Benutzern sollte es erlaubt sein, Systemverwalterrechte zu erhalten. Dies hindert andere Benutzer daran, am Rechner herumzupfuschen und Dinge anzustellen, wie etwa Anwendungen zu entfernen, die Sie brauchen, oder Anwendungen zu installieren, die Sie nicht haben wollen, oder wichtige Dateien zu ändern. In Hinblick auf die Sicherheit ist das nützlich.</p>

</section>

</page>
