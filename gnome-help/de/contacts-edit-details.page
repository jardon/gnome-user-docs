<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="de">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Information für jeden Kontakt bearbeiten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

<title>Kontaktdetails bearbeiten</title>

  <p>Durch Bearbeiten von Kontaktdetails halten Sie die Informationen in Ihrem Adressbuch aktuell und vollständig.</p>

  <steps>
    <item>
      <p>Wählen Sie den Kontakt in der Kontaktliste aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">Mehr anzeigen</span></media> rechts oben im Fenster und wählen Sie <gui style="menuitem">Bearbeiten</gui>.</p>
    </item>
    <item>
      <p>Kontaktdetails bearbeiten.</p>
      <p>Um weitere <em>Details</em> wie eine neue Telefonnummer oder E-Mail-Adresse hinzuzufügen, füllen Sie einfach das nächste leere Feld des gewünschten Typs (Telefonnummer, E-Mail-Adresse usw.) aus.</p>
      <note style="tip">
        <p>Klicken Sie auf das Feld <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Mehr anzeigen</span></media> am unteren Rand, um weitere verfügbare Details auszuklappen, darunter Felder wie <gui>Webseite</gui> und <gui>Geburtstag</gui>.</p>
      </note>
    </item>
    <item>
      <p>Klicken Sie zum Beenden der Bearbeitung des Kontakts auf <gui style="button">Fertig</gui>.</p>
    </item>
  </steps>

</page>
