<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="de">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Automatisch die Bildschirmhelligkeit regeln, um den Akku zu schonen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Automatische Helligkeit einschalten</title>

  <p>Wenn Ihr Rechner über einen integrierten Umgebungslichtsensor verfügt, so kann dieser eingesetzt werden, um automatisch die Bildschirmhelligkeit anzupassen. So ist der Bildschirm auch bei wechselndem Umgebungslicht stets gut zu lesen und der Energieverbrauch wird reduziert.</p>

  <steps>

    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Leistung</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Leistung</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Stellen Sie im Abschnitt <gui>Energiespareinstellungen</gui> die <gui>Automatische Bildschirmhelligkeit</gui> ein.</p>
    </item>

  </steps>

  <p>Um die automatische Helligkeit auszuschalten, stellen Sie den Schalter aus.</p>

</page>
