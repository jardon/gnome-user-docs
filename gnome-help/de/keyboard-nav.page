<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="de">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
       <name>Ekaterina Gerasimova</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Benutzen Sie Anwendungen und die Arbeitsumgebung ohne Maus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Tastatursteuerung</title>

  <p>Dieser Abschnitt beschreibt detailliert die Tastaturnavigation für Benutzer, die keine Maus oder ein anderes Zeigegerät verwenden können, oder die ihre Tastatur so viel wie möglich nutzen wollen. In <link xref="shell-keyboard-shortcuts"/> finden Sie Tastenkombinationen, die für alle Benutzer nützlich sind.</p>

  <note style="tip">
    <p>Wenn es Ihnen schwer fällt, eine Maus oder andere Zeigegeräte zu verwenden, können Sie den Mauszeiger mit Hilfe des Nummernblocks Ihrer Tastatur bewegen. Weitere Details finden Sie in <link xref="mouse-mousekeys"/>.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>Navigieren in Benutzeroberflächen</title>
  <tr>
    <td><p><key>Tabulatortaste</key> und</p>
    <p><keyseq><key>Strg</key><key>Tabulator</key></keyseq></p>
    </td>
    <td>
      <p>Bewegen Sie den Tastaturfokus zwischen den Bedienelementen. <keyseq><key>Strg</key> <key>Tab</key></keyseq> bewegt den Fokus zwischen Gruppen von Bedienelementen, wie beispielsweise von der Seitenleiste zum Hauptteil des Fensters. <keyseq><key>Strg</key><key>Tab</key></keyseq> kann auch Bedienelemente ansteuern, die selbst <key>Tab</key> verwenden, wie ein Textbereich.</p>
      <p>Halten Sie die <key>Umschalttaste</key> gedrückt, um den Fokus in umgekehrter Reihenfolge zu bewegen.</p>
    </td>
  </tr>
  <tr>
    <td><p>Pfeiltasten</p></td>
    <td>
      <p>Bewegen Sie die Auswahl zwischen Objekten in einem einzelnen Bedienelement, oder in einem Bereich aus aufeinander bezogenen Elementen. Verwenden Sie die Pfeiltasten, um die Knöpfe in einer Werkzeugleiste zu fokussieren oder die Objekte einer Listen- oder Symbolansicht oder einen Radioknopf aus einer Gruppe auszuwählen.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key>Pfeiltasten</keyseq></p></td>
    <td><p>In einer Listen- oder Symbolansicht bewegen Sie den Tastaturfokus zu einem anderen Objekt, ohne die Auswahl eines Objektes zu ändern.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key>Pfeiltasten</keyseq></p></td>
    <td><p>In einer Listen- oder Symbolansicht wählen Sie alle Objekte vom aktuell ausgewählten Objekt bis zum neu fokussierten Objekt aus.</p>
    <p>In einer Baumansicht können Objekte eingeklappt oder ausgeklappt sein. Um diese anzuzeigen, drücken Sie <keyseq><key>Umschalttaste</key><key>→</key></keyseq> oder zum Einklappen <keyseq><key>Umschalttaste</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Leertaste</key></p></td>
    <td><p>Aktivieren Sie ein fokussiertes Objekt wie einen Knopf, ein Ankreuzfeld oder einen Listeneintrag.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>Leertaste</key></keyseq></p></td>
    <td><p>In einer Listen- oder Symbolansicht wählen Sie das fokussierte Objekt aus oder ab, ohne den Auswahlzustand anderer Objekte zu ändern.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p>Halten Sie die <key>Alt</key>-Taste gedrückt, um <em>Zugriffstasten</em> zu erreichen: unterstrichene Buchstaben in Menüeinträgen, Knöpfen und anderen Bedienelementen. Drücken Sie die <key>Alt</key>-Taste und zusätzlich den unterstrichenen Buchstaben, um das Bedienelement zu aktivieren, genau so, als wenn Sie es anklicken würden.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>Ein Menü, Aufklappfenster, Dialogfenster oder einen Wechsler beenden.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>Öffnen Sie das erste Menü der Menüleiste eines Fensters. Verwenden Sie die Pfeiltasten, um durch die Menüs zu navigieren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>Öffnen Sie das Anwendungsmenü im oberen Panel.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Umschalttaste</key><key>F10</key></keyseq> oder</p>
    <p><key xref="keyboard-key-menu">Menü</key></p></td>
    <td>
      <p>Das Kontextmenü für die aktuelle Auswahl einblenden, so als ob Sie mit der rechten Maustaste klicken würden.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>F10</key></keyseq></p></td>
    <td><p>In der Dateiverwaltung das Kontextmenü für den aktuellen Ordner öffnen, wie mit einem Klick mit der rechten Maustaste auf den Hintergrund und nicht auf ein Objekt.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Strg</key><key>Bild↑</key></keyseq></p>
    <p>und</p>
    <p><keyseq><key>Strg</key><key>Bild↓</key></keyseq></p></td>
    <td><p>Wechseln Sie in einer Reiteransicht zum Reiter links oder rechts.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Navigieren durch den Schreibtisch</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Durch Fenster der gleichen Anwendung blättern. Halten Sie die <key>Alt</key>-Taste gedrückt und drücken Sie <key>F6</key>, bis das gewünschte Fenster hervorgehoben wird, dann lassen Sie die <key>Alt</key>-Taste los. Dies ähnelt dem <keyseq><key>Alt</key><key>`</key></keyseq>-Funktionsmerkmal.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>Durch alle geöffneten Fenster einer Arbeitsfläche blättern.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Öffnen Sie die Benachrichtigungsliste.</link> Drücken Sie <key>Esc</key> zum Schließen.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>Durch Fenster navigieren</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Das aktuelle Fenster schließen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> oder <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Ein maximiertes Fenster auf seine ursprüngliche Größe wiederherstellen. Verwenden Sie <keyseq><key>Alt</key><key>F10</key></keyseq> zum Maximieren. <keyseq><key>Alt</key><key>F10</key></keyseq> wird sowohl maximieren als auch wiederherstellen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>Das aktuelle Fenster verschieben. Drücken Sie <keyseq><key>Alt</key><key>F7</key></keyseq> und verschieben Sie dann das Fenster mit den Pfeiltasten. Drücken Sie die <key>Eingabetaste</key>, um den Verschiebevorgang zu beenden oder <key>Esc</key>, um das Fenster an seinen ursprünglichen Ort zurückzusetzen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>Das aktuelle Fenster wiederherstellen. Drücken Sie <keyseq><key>Alt</key><key>F8</key></keyseq> und verwenden Sie dann die Pfeiltasten, um die Größe des Fensters zu ändern. Drücken Sie die <key>Eingabetaste</key>, um die Größenänderung zu beenden oder <key>Esc</key>, um zur ursprünglichen Größe zurückzukehren.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> oder <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p>Ein Fenster <link xref="shell-windows-maximize">maximieren</link>. Drücken Sie erneut <keyseq><key>Alt</key><key>F10</key></keyseq> oder <keyseq><key>Super</key><key>↓</key></keyseq>, um die ursprüngliche Größe eines maximierten Fensters wiederherzustellen.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>Ein Fenster maximieren.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>Ein Fenster an der linken Seite des Bildschirms maximieren. Drücken Sie die Kombination erneut, um die ursprüngliche Größe wiederherzustellen. Drücken Sie <keyseq><key>Super</key><key>→</key></keyseq>, um die Seiten zu wechseln.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>Ein Fenster an der rechten Seite des Bildschirms maximieren. Drücken Sie die Kombination erneut, um die ursprüngliche Größe wiederherzustellen. Drücken Sie <keyseq><key>Super</key><key>←</key></keyseq>, um die Seiten zu wechseln.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Leertaste</key></keyseq></p></td>
    <td><p>Das Fenstermenü öffnen, ähnlich dem Klick mit der rechten Maustaste auf die Titelleiste.</p></td>
  </tr>
</table>

</page>
