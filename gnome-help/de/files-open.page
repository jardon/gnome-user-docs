<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="de">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dateien mit einer Anwendung öffnen, die nicht die Standardanwendung für diesen Dateityp ist. Sie können die Standardanwendung auch wechseln.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

<title>Dateien mit anderen Anwendungen öffnen</title>

  <p>Wenn Sie in der Dateiverwaltung auf eine Datei doppelt klicken (oder mit der mittleren Maustaste), dann wird sie mit der Standardanwendung für diesen Dateityp geöffnet. Sie können sie aber auch mit einer anderen Anwendung öffnen, online nach Anwendungen suchen oder die Standardanwendung für alle Dateien des gleichen Typs ändern.</p>

  <p>Um eine Datei mit einer anderen als der Standardanwendung zu öffnen, klicken Sie mit der rechten Maustaste auf die Datei und wählen Sie die Anwendung oben im Menü. Wenn die gewünschte Anwendung nicht dabei ist, klicken Sie auf <gui>Mit anderer Anwendung öffnen</gui>. Die Dateiverwaltung zeigt Ihnen nur die Anwendungen, von denen bekannt ist, dass Sie mit der Datei umgehen können. Um alle Anwendungen auf Ihrem Rechner zu durchsuchen, klicken Sie auf <gui>Alle Anwendungen anzeigen</gui>.</p>

<p>Wenn die gewünschte Anwendung auch hier nicht auffindbar ist, können Sie nach weiteren Anwendungen suchen, indem Sie auf <gui>Neue Anwendungen suchen</gui> klicken. Die Dateiverwaltung sucht dann online nach Softwarepaketen mit Anwendungen, die mit diesem Dateityp umgehen können.</p>

<section id="default">
  <title>Ändern der Standardanwendung</title>
  <p>Sie können die Standardanwendung für einen bestimmten Dateityp ändern. Das erlaubt Ihnen, Ihre bevorzugte Anwendung zu öffnen, wenn Sie auf die Datei doppelklicken. Sie möchten zum Beispiel, dass Ihre Lieblingsmusikwiedergabe geöffnet wird, wenn Sie auf eine MP3-Datei doppelklicken.</p>

  <steps>
    <item><p>Wählen Sie eine Datei des Typs, dessen Standardanwendung Sie wechseln möchten. Um zum Beispiel die Anwendung zu ändern, die MP3-Dateien öffnet, wählen Sie eine <file>.mp3</file>-Datei.</p></item>
    <item><p>Klicken Sie mit der rechten Maustaste auf die Datei und wählen Sie <gui>Eigenschaften</gui>.</p></item>
    <item><p>Wählen Sie den Reiter <gui>Öffnen mit</gui>.</p></item>
    <item><p>Wählen Sie die gewünschte Datei aus und klicken Sie auf <gui>Als Vorgabe festlegen</gui>.</p>
    <p>Wenn <gui>Weitere Anwendungen</gui> eine Anwendung enthält, die Sie manchmal verwenden, aber nicht als Vorgabe festlegen wollen, wählen Sie diese Anwendung und klicken Sie auf <gui>Hinzufügen</gui>. Dadurch wird sie zur Rubrik <gui>Empfohlene Anwendungen</gui> hinzugefügt. Sie können diese Anwendung danach verwenden, indem Sie mit der rechten Maustaste auf eine Datei klicken und sie aus der Liste auswählen.</p></item>
  </steps>

  <p>Das ändert die Standardanwendung nicht nur für die gewählte Datei, sondern für alle Dateien dieses Typs.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
