<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="de">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einige Online-Konten sind in der Lage, auf mehrere Dienste zuzugreifen, wie Kalender und E-Mail. Sie können festlegen, welche dieser Dienste von Anwendungen verwendet werden dürfen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Festlegungen zum Zugriff eines Kontos auf bestimmte Dienste</title>

  <p>Einige Anbieter von Online-Konten erlauben den Zugriff auf verschiedene Dienste mit demselben Benutzerkonto. Beispielsweise ermöglichen Google-Konten den Zugriff auf Kalender, E-Mail und Kontakte. Vielleicht wollen Sie Ihr Konto für einige der Dienste nutzen, für andere dagegen eher nicht. Zum Beispiel könnten Sie Ihr Google-Konto zwar für E-Mail nutzen wollen, aber nicht für den Kalender, weil Sie ein anderes Online-Konto dafür verwenden.</p>

  <p>Sie können einige der von einem Online-Konto bereitgestellten Dienste deaktivieren:</p>

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Online-Konten</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Online-Konten</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie das zu ändernde Konto in der Liste auf der rechten Seite aus.</p>
    </item>
    <item>
      <p>Eine Liste der für dieses Konto verfügbaren Dienste wird unter <gui>Verwenden für</gui> angezeigt. Lesen Sie <link xref="accounts-which-application"/> um zu erfahren, welche Anwendungen auf welche Dienste zugreifen.</p>
    </item>
    <item>
      <p>Schalten Sie die Dienste ab, die Sie nicht verwenden wollen.</p>
    </item>
  </steps>

  <p>Sobald ein Dienst eines Kontos deaktiviert ist, können die Anwendungen Ihres Rechners keine Verbindung mehr zu diesem Dienst herstellen.</p>

  <p>Um einen zuvor deaktivierten Dienst wieder einzuschalten, gehen Sie zurück zum Fenster der <gui>Online-Konten</gui> und schalten Sie ihn ein.</p>

</page>
