<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="de">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sie müssen die Option <gui>Für alle Benutzer verfügbar</gui> in den Verbindungseinstellungen des Netzwerks deaktivieren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Andere Benutzer können die Netzwerkverbindungen nicht bearbeiten</title>

  <p>Falls Sie eine Netzwerkverbindung bearbeiten können, aber andere Benutzer Ihres Rechners dazu nicht berechtigt sind, könnten Sie die Verbindung als <em>Für alle Benutzer verfügbar</em> eingerichtet haben. Dadurch kann sich jeder Benutzer des Rechners mit dieser Verbindung <em>verbinden</em>.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Netzwerk</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Netzwerk</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Drahtlosnetzwerk</gui> in der linken Liste aus.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Knopf <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">Einstellungen</span></media>, um die Verbindungsdetails anzuzeigen.</p>
    </item>
    <item>
      <p>Wählen Sie <gui>Identität</gui> im linken Teilfenster aus.</p>
    </item>
    <item>
      <p>Wählen Sie unten im Teilfenster <gui>Identität</gui> die Option <gui>Anderen Benutzern zur Verfügung stellen</gui> aus, um die Netzwerkverbindung für andere freizugeben.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Anwenden</gui>, um die Änderungen zu speichern.</p>
    </item>
  </steps>

</page>
