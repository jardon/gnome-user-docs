<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-manual" xml:lang="de">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>
    <revision pkgversion="3.33.3" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Sie müssen die Netzwerkeinstellungen angeben, falls diese nicht automatisch zugeordnet werden.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Netzwerkeinstellungen manuell festlegen</title>

  <p>Falls Ihr Netzwerk Ihrem Rechner keine Einstellungen automatisch zuweist, müssen Sie die Einstellungen manuell eingeben. Dieses Thema setzt voraus, dass Sie die korrekten Einstellungen bereits kennen. Falls nicht, fragen Sie Ihren Systemverwalter oder schauen Sie nach den Einstellungen Ihres Routers oder Switches.</p>

  <steps>
    <title>So richten Sie Ihre Netzwerkeinstellungen manuell ein:</title>
    <item>
      <p>Öffnen Sie die <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht und tippen Sie <gui>Einstellungen</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Einstellungen</gui>.</p>
    </item>
    <item>
      <p>Sofern Sie ein kabelgebundenes Netzwerk anschließen, klicken Sie auf <gui>Netzwerk</gui>. Andernfalls wählen Sie <gui>WLAN</gui>.</p>
      <p>Stellen Sie sicher, dass Ihr Funknetzwerkadapter eingeschaltet oder das Netzwerkkabel eingesteckt ist.</p>
    </item>
    <item>      
      <p>Klicken Sie auf den Knopf <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">Einstellungen</span></media>.</p>
      <note>
        <p>Bei einer <gui>WLAN</gui>-Verbindung befindet sich der Knopf <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">Einstellungen</span></media> neben dem aktiven Netzwerk.</p>
      </note>
    </item>
    <item>
      <p>Wählen Sie den Reiter <gui>IPv4</gui> oder <gui>IPv6</gui> und ändern Sie <gui>Methode</gui> auf <gui>Manuell</gui>.</p>
    </item>
    <item>
      <p>Geben Sie <gui xref="net-what-is-ip-address">IP-Adresse</gui> und <gui>Gateway</gui> sowie die entsprechende <gui>Netzmaske</gui> ein.</p>
    </item>
    <item>
      <p>Stellen Sie <gui>Automatisch</gui> aus im Abschnitt <gui>DNS</gui>. Geben Sie die IP-Adresse des DNS-Servers ein, den Sie verwenden möchten. Geben Sie zusätzliche DNS-Server-Adressen mit Hilfe des Knopfes <gui>+</gui> ein.</p>
    </item>
    <item>
      <p>Stellen Sie im Abschnitt <gui>Routen</gui> <gui>Automatisch</gui> aus. Geben Sie <gui>Adresse</gui>, <gui>Netzmaske</gui>, <gui>Gateway</gui> und <gui>Metrik</gui> für eine Route ein, die Sie verwenden möchten. Geben Sie zusätzliche Routen mit Hilfe des Knopfes <gui>+</gui> ein.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Anwenden</gui>. Falls Sie nicht mit dem Netzwerk verbunden sind, öffnen Sie das <gui xref="shell-introduction#systemmenu">Systemmenü</gui> in der oberen Leiste rechts und stellen Sie eine Verbindung her. Überprüfen Sie die Netzwerkeinstellungen, indem Sie beispielsweise eine Internet-Seite aufrufen oder nach im Netzwerk freigegebenen Dateien suchen.</p>
    </item>
  </steps>

</page>
