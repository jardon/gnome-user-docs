<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="de">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verwalten und Organisieren von Dateien mit der Dateiverwaltung.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

<title>Dateien und Ordner durchsuchen</title>

<p>Verwenden Sie die Dateiverwaltung <app>Dateien</app>, um die Dateien auf Ihrem Rechner zu durchsuchen und zu organisieren. Sie können sie auch verwenden, um Dateien auf Speichergeräten (wie externen Festplatten), auf <link xref="nautilus-connect">Dateiservern</link> und Netzwerkfreigaben zu verwalten.</p>

<p>Um die Dateiverwaltung zu öffnen, öffnen Sie <app>Dateien</app> in der <gui xref="shell-introduction#activities">Aktivitäten</gui>-Übersicht. Sie können in der Übersicht auch nach Dateien und Ordnern auf die gleiche Weise suchen, wie Sie nach <link xref="shell-apps-open">Anwendungen suchen</link>.</p>

<section id="files-view-folder-contents">
  <title>Den Inhalt von Ordnern durchsuchen</title>

<p>Klicken Sie in der Dateiverwaltung doppelt auf einen beliebigen Ordner, um seinen Inhalt einzusehen. Klicken Sie zweimal oder <link xref="mouse-middleclick">mit der mittleren Maustaste</link> auf eine Datei, um sie mit der Standardanwendung dieser Datei zu öffnen. Klicken Sie mit der mittleren Maustaste auf einen Ordner, um diesen in einem neuen Reiter zu öffnen. Sie können auch mit der rechten Maustaste auf einen Ordner klicken, um ihn in einem neuen Reiter oder einem neuen Fenster zu öffnen.</p>

<p>Wenn Sie die Dateien eines Ordners durchsuchen, können Sie für jede Datei durch Drücken der Leertaste <link xref="files-preview">eine Vorschau anzeigen lassen</link>, um sicherzugehen, dass es die richtige Datei ist, bevor Sie sie öffnen, kopieren oder löschen.</p>

<p>Die <em>Pfadleiste</em> über der Liste der Dateien und Ordner zeigt Ihnen an, welchen Ordner Sie gerade betrachten sowie seine übergeordneten Ordner. Klicken Sie auf einen übergeordneten Ordner in der Pfadleiste, um dorthin zu gelangen. Klicken Sie mit der rechten Maustaste auf einen beliebigen Ordner in der Pfadleiste, um ihn in einem neuen Reiter oder Fenster zu öffnen oder seine Eigenschaften einzusehen.</p>

<p>Wenn Sie im oder unter dem Ordner, den Sie gerade betrachten, schnell <link xref="files-search">eine Datei suchen</link> wollen, beginnen Sie den Namen einzugeben. Eine <em>Suchleiste</em> erscheint oben im Fenster und nur Dateien, die Ihrer Suche entsprechen, werden angezeigt. Drücken Sie <key>Esc</key>, um die Suche abzubrechen.</p>

<p>Greifen Sie schnell auf häufig verwendete Orte in der <em>Seitenleiste</em> zu. Wenn Sie die Seitenleiste nicht sehen, klicken Sie auf den Menüknopf rechts oben im Fenster und wählen Sie <gui>Seitenleiste anzeigen</gui>. Sie können <link xref="nautilus-bookmarks-edit">für Ordner, die Sie häufig verwenden, Lesezeichen anlegen</link>, die dann in der Seitenleiste angezeigt werden.</p>

</section>

</page>
