<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-order" xml:lang="de">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zusammenführen und die Reihenfolge beim Drucken umkehren.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013, 2017-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2018-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Philipp Kiemle</mal:name>
      <mal:email>philipp.kiemle@gmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jürgen Benvenuti</mal:name>
      <mal:email>gastornis@posteo.org</mal:email>
      <mal:years>2022, 2023.</mal:years>
    </mal:credit>
  </info>

  <title>Drucken von Seiten in einer anderen Reihenfolge</title>

  <section id="reverse">
    <title>Rückwärts</title>

    <p>Die erste Seite wird von den meisten Druckern zuerst und die letzte Seite zuletzt ausgegeben, wodurch die Seiten in umgekehrter Reihenfolge übereinander liegen, wenn Sie diese aus dem Drucker entnehmen. Falls gewünscht, können Sie diese Druckreihenfolge umkehren.</p>

    <steps>
      <title>So kehren Sie die Reihenfolge um:</title>
      <item>
        <p>Drücken Sie <keyseq><key>Strg</key><key>P</key></keyseq>, um den Druckdialog zu öffnen.</p>
      </item>
      <item>
        <p>Aktivieren Sie das Ankreuzfeld <gui>Umkehren</gui> im Reiter <gui>Allgemein</gui> des Druckfensters unter <gui>Kopien</gui>. Die letzte Seite wird zuerst gedruckt usw.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Zusammentragen</title>

  <p>Wenn Sie mehr als eine Kopie eines Dokuments drucken, werden die Ausdrucke nach Seitenzahl sortiert (z.B. werden erst alle Kopien von Seite 1 gedruckt, dann alle von Seite 2 usw.). Mit <em>Zusammentragen</em> werden die Seiten stattdessen in zusammengehörigen Gruppen und der richtigen Reihenfolge ausgegeben.</p>

  <steps>
    <title>So lassen Sie zusammentragen:</title>
    <item>
     <p>Drücken Sie <keyseq><key>Strg</key><key>P</key></keyseq>, um den Druckdialog zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie das Ankreuzfeld <gui>Zusammentragen</gui> im Reiter <gui>Allgemein</gui> unter <gui>Kopien</gui> aus.</p>
    </item>
  </steps>

</section>

</page>
