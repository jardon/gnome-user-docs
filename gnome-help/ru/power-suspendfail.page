<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="ru">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Аппаратное обеспечение некоторых компьютеров вызывает проблемы с режимом ожидания.</desc>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Почему мой компьютер не выходит из режима ожидания?</title>

<p>Если вы переведёте компьютер в <link xref="power-suspend">режим ожидания</link>, а затем попытаетесь возобновить его работу, вы можете обнаружить, что он работает не так, как вы ожидали. Причина может быть в том, что приостановка работы не поддерживается должным образом вашим оборудованием.</p>

<section id="resume">
  <title>Компьютер не может выйти из режима ожидания</title>
  <p>Если нажать клавишу мышки или клавиатуры компьютера, находящегося в режиме ожидания, он должен «проснуться» и показать экран ввода пароля. Если этого не произошло, попробуйте нажать кнопку питания (не удерживать, а просто нажать один раз).</p>
  <p>Если это не помогло, проверьте, включен ли монитор и попробуйте снова нажать клавишу на клавиатуре.</p>
  <p>В качестве последнего средства, выключите компьютер, удерживая кнопку питания нажатой 5-10 секунд. При этом вы потеряете все несохранённые результаты работы, но зато сможете потом опять включить компьютер.</p>
  <p>Если это повторяется каждый раз при переводе компьютера в режим ожидания, то этот режим, возможно, не работает на вашем оборудовании.</p>
  <note style="warning">
    <p>Если в электросети пропало напряжение, и компьютер не оборудован альтернативным источником питания (например, аккумулятором), он выключится.</p>
  </note>
</section>

<section id="hardware">
  <title>Моё беспроводное соединение (или другое устройство) не работает после «пробуждения» компьютера</title>
  <p>Если вы перевели компьютер в режим ожидания, а затем снова вернули его в рабочее состояние, может оказаться, что соединение с интернетом, мышь или какое-то другое устройство перестало правильно работать. Это может быть связано с тем, что драйвер устройства не поддерживает должным образом переход в режим ожидания. Это <link xref="hardware-driver">проблема с драйвером</link>, а не с самим устройством.</p>
  <p>Если устройство снабжено выключателем питания, попробуйте отключить его и снова включить. В большинстве случаев оно снова начнёт работать. Если устройство подключается через USB или другой подобный кабель, попробуйте отсоединить устройство и подсоединить его снова и посмотрите, заработает ли оно.</p>
  <p>Если нет возможности выключить или отсоединить устройство, или это не помогает, попробуйте перезагрузить компьютер, чтобы устройство снова начало работать.</p>
</section>

</page>
