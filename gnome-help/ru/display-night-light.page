<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="ru">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ночной свет изменяет цвет ваших дисплеев в зависимости от времени суток.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Отрегулируйте цветовую температуру экрана</title>

  <p>Монитор компьютера излучает синий свет, который способствует бессоннице и усталости глаз после наступления темноты. <gui>Ночной свет</gui> изменяет цвет ваших дисплеев в зависимости от времени суток, делая его теплее вечером. Чтобы включить <gui>Ночной свет</gui>:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мониторы</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Мониторы</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите <gui>Ночной свет</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Убедитесь, что переключатель <gui>Ночной свет</gui> включен.</p>
    </item>
    <item>
      <p>В разделе <gui>Расписание</gui> выберите <gui>От заката до восхода солнца</gui>, чтобы цвет экрана соответствовал времени заката и восхода для вашего местоположения. Или <gui>Установленное вручную</gui>, чтобы установить в разделе <gui>Время</gui> индивидуальное расписание.</p>
    </item>
    <item>
      <p>Воспользуйтесь ползунком для настройки параметра <gui>Цветовая температура</gui> для получения более тёплых или холодных цветов.</p>
    </item>
  </steps>
      <note>
        <p>В <link xref="shell-introduction">верхней панели</link> отображается включение <gui>Ночного света</gui>. Его можно временно отключить из системного меню.</p>
      </note>



</page>
