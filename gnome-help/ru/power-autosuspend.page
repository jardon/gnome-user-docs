<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="ru">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Настройка компьютера на автоматический переход в режим ожидания.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Установить автоматический переход в режим ожидания</title>

  <p>Вы можете настроить компьютер так, чтобы он автоматически переходил в режим ожидания при простое. Временные интервалы могут быть настроены для работы от батареи или от сети.</p>

  <steps>

    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Электропитание</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Электропитание</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>В разделе <gui>Опции энергосбережения</gui> нажмите <gui>Автоматический режим ожидания</gui>.</p>
    </item>
    <item>
      <p>Выберите <gui>При работе от батареи</gui> или <gui>При подключении</gui>, установите переключатель в положение «Включено» и выберите <gui>Задержка</gui>. Можно настроить оба варианта.</p>

      <note style="tip">
        <p>Для стационарного компьютера есть ещё вариант: <gui>При простое</gui>.</p>
      </note>
    </item>

  </steps>

</page>
