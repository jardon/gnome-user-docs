<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="ru">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Батист Милле-Матиас (Baptiste Mille-Mathias)</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андрэ Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Приложения могут использовать учётные записи, созданные в <app>Сетевых учётных записях</app> и предоставляемые ими услуги.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Онлайн-сервисы и приложения</title>

  <p>После добавления сетевой учётной записи любое приложение может использовать эту учётную запись для любых доступных служб, которые вы не <link xref="accounts-disable-service">отключили</link>. Разные провайдеры предоставляют разные услуги. На этой странице перечислены различные службы и некоторые приложения, которые, как известно, ими пользуются.</p>

  <terms>
    <item>
      <title>Календарь</title>
      <p>Служба календаря позволяет просматривать, добавлять и редактировать события в онлайн-календаре. Она используется такими приложениями, как <app>Календарь</app>, <app>Evolution</app> и <app>California</app>.</p>
    </item>

    <item>
      <title>Chat</title>
      <p>Служба общения позволяет вам общаться с вашими контактами на популярных платформах обмена мгновенными сообщениями. Она используется приложением <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Контакты</title>
      <p>Служба контактов позволяет просматривать опубликованные сведения о ваших контактах в различных службах. Она используется такими приложениями, как <app>Контакты</app> и <app>Evolution</app>.</p>
    </item>

<!-- See https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/commit/32f35cc07fcbee839978e3630972b67ed55f0da6
    <item>
      <title>Documents</title>
      <p>The Documents service allows you to view your online documents
      such as those in Google docs. You can view your documents using the
      <app>Documents</app> application.</p>
    </item>
-->
    <item>
      <title>Файлы</title>
      <p>Служба «Файлы» добавляет удалённое местоположение файла, как если бы вы добавили его с помощью функции <link xref="nautilus-connect">Подключиться к серверу</link> в файловом менеджере. Вы можете получить доступ к удалённым файлам с помощью файлового менеджера, а также через диалоги открытия и сохранения файлов в любом приложении.</p>
    </item>

    <item>
      <title>Mail</title>
      <p>Почтовая служба позволяет отправлять и получать электронную почту через поставщика услуг электронной почты, например Google. Она используется в <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->
<!-- TODO: Not sure what this does in which app. Seems to support e.g. VKontakte.
    <item>
      <title>Music</title>
    </item>
-->
    <item>
      <title>Фотографии</title>
      <p>Сервис «Фотографии» позволяет вам просматривать свои онлайн-фотографии, например те, которые вы публикуете на Facebook. Просматривать свои фотографии вы можете с помощью приложения <app>Фотографии</app>.</p>
    </item>

    <item>
      <title>Принтеры</title>
      <p>Служба «Принтеры» позволяет отправить копию PDF поставщику услуг печати из диалогового окна печати любого приложения. Поставщик может предоставлять услуги печати или просто сохранения PDF-файла, который вы сможете загрузить и распечатать позже.</p>
    </item>

  </terms>

</page>
