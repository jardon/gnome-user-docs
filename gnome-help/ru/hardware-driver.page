<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="ru">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Драйвер позволяет компьютеру использовать подключённые к нему устройства.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Что такое драйвер?</title>

<p>Устройства — это физические «части» вашего компьютера. Они могут быть <em>внешними</em>, как принтеры и мониторы, или <em>внутренними</em>, как графические и звуковые карты.</p>

<p>Чтобы компьютер мог использовать эти устройства, ему нужно знать, как взаимодействовать с ними. Эту работу выполняют небольшие программы, называемые <em>драйверами устройств</em>.</p>

<p>Чтобы устройство, подключённое к компьютеру, работало нормально, для него должен быть установлен подходящий драйвер. Например, если для подключённого принтера отсутствует драйвер, то печатать на этом принтере вы не сможете. Как правило, для каждой модели устройства существует собственный драйвер, несовместимый с драйверами для других моделей.</p>

<p>В Linux драйверы для большинства устройств устанавливаются по умолчанию, так что всё должно работать при подключении. Тем не менее, может потребоваться ручная установка драйверов, или они вообще могут быть недоступны.</p>

<p>Кроме того, некоторые существующие драйверы являются неполными или частично нефункциональными. Например, вы можете обнаружить, что принтер не может выполнять двустороннюю печать, но в остальном полностью работоспособен.</p>

</page>
