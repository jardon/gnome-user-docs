<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="ru">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Как часто следует делать резервные копии важных файлов, чтобы обеспечить их сохранность.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Периодичность создания резервных копий</title>

  <p>Периодичность создания резервных копий зависит от типа копируемых данных. Например, для сервера, на котором хранятся данные, важные для всей сети, даже ежедневного резервного копирования может быть недостаточно.</p>

  <p>С другой стороны, для домашнего компьютер создавать резервные копии данных каждый час вряд ли необходимо. При планировании расписания резервного копирования может быть полезно учесть:</p>

<list style="compact">
  <item><p>Количество времени, проводимого вами за компьютером.</p></item>
  <item><p>Как часто и насколько сильно изменяются данные на компьютере.</p></item>
</list>

  <p>Если копируемые данные не очень важны, или изменяются редко (например, музыка, электронная почта, семейные фотографии), то еженедельного или даже ежемесячного резервного копирования может быть вполне достаточно. А вот для данных налоговой проверки может понадобиться более частое создание резервных копий.</p>

  <p>Можно принять в качестве общего правила, что промежуток времени между созданием резервных копий не должен быть больше, чем количество времени, которое вы готовы потратить на повторное создание утраченных данных. Например, если провести неделю, заново набирая пропавшие документы — слишком долго для вас, то следует создавать резервные копии хотя бы раз в неделю.</p>

</page>
