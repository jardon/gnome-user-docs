<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="ru">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андрэ Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Набирайте символы, отсутствующие на клавиатуре, включая символы иностранных алфавитов, математические символы, emoji и другие символы.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Ввод специальных символов</title>

  <p>Можно просмотреть и ввести тысячи различных символов из большинства систем письма народов мира, даже если эти символы отсутствуют на клавиатуре. Ниже перечислены несколько различных способов ввода специальных символов.</p>

  <links type="section">
    <title>Способы ввода символов</title>
  </links>

  <section id="characters">
    <title>Символы</title>
    <p>Приложение таблица символов позволяет находить и вставлять необычные символы, включая emoji, путем просмотра категорий символов или поиска по ключевым словам.</p>

    <p>Вы можете запустить приложение <app>Символы</app> из режима «Обзор».</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Вставить emoji</title>
      <item>
        <p>Нажмите <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Просмотрите категории в нижней части диалогового окна или начните вводить описание в поле поиска.</p>
      </item>
      <item>
        <p>Выберите emoji для вставки.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Клавиша Compose</title>
    <p>Клавиша Compose — это специальная клавиша, позволяющая вводить специальные символы с помощью определённых комбинаций клавиш. Например, чтобы ввести символ <em>é</em>, нужно нажать <key>Compose</key>, затем <key>'</key> и <key>e</key>.</p>
    <p>Клавиши Compose нет на клавиатуре, но можно назначить для выполнения её функций одну из имеющихся клавиш.</p>

    <steps>
      <title>Назначение клавиши Compose</title>
      <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Настройки</gui>.</p>
      </item>
      <item>
        <p>Нажмите на <gui>Настройки</gui>.</p>
      </item>
      <item>
        <p>Нажмите <gui>Клавиатура</gui> на боковой панели, чтобы открыть этот раздел настроек.</p>
      </item>
      <item>
        <p>В разделе <gui>Ввод специальных символов</gui> нажмите <gui>Создать ключ</gui>.</p>
      </item>
      <item>
        <p>Включите переключатель <gui>Кнопка Compose</gui>.</p>
      </item>
      <item>
        <p>Установите флажок рядом с клавишей, которую вы хотите установить в качестве клавиши «Compose».</p>
      </item>
      <item>
        <p>Закрыть диалог.</p>
      </item>
    </steps>

    <p>С помощью клавиши Compose можно вводить некоторые часто используемые символы, например:</p>

    <list>
      <item><p>Нажмите <key>Compose</key>, затем <key>'</key>, затем букву, чтобы ввести букву с диакритическим знаком акут над ней (например, <em>é</em>).</p></item>
      <item><p>Нажмите <key>Compose</key>, затем <key>`</key> и букву, чтобы ввести букву с диакритическим знаком гравис (например, <em>è</em>).</p></item>
      <item><p>Нажмите <key>Compose</key>, затем <key>"</key> и букву, чтобы ввести букву со знаком умляут над ней (например, <em>ë</em>).</p></item>
      <item><p>Нажмите <key>Compose</key>, затем <key>-</key> и букву, чтобы ввести букву со знаком макрон над ней (например, <em>ē</em>).</p></item>
    </list>
    <p>Другие комбинации клавиш с участием Сompose можно найти в <link href="https://ru.wikipedia.org/wiki/Compose">статье о клавише Compose в Википедии</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Коды символов</title>

  <p>Можно ввести любой символ Unicode, набрав на клавиатуре его четырёхзначный код. Чтобы узнать код символа, найдите этот символ в приложении <gui>Символы</gui>. Код символа — это четыре символа после <gui>U+</gui>.</p>

  <p>Чтобы ввести символ по его коду, нажмите <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, затем введите четырехзначный код и нажмите <key>Пробел</key> или <key>Enter</key>. Если вы часто используете символы, которые сложно ввести другими методами, может быть полезно запомнить коды таких символов, чтобы быстро их вводить.</p>

</section>

  <section id="layout">
    <title>Раскладки клавиатуры</title>
    <p>Можно сделать так, чтобы ваша клавиатура вела себя как клавиатура для другого языка, независимо от того, какие буквы изображены на клавишах. Можно даже легко переключаться между различными раскладками клавиатуры, используя значок в верхней панели. О том, как это делать, смотрите <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Методы ввода</title>

  <p>Метод ввода дополняет предыдущие методы, позволяя вводить символы не только с клавиатуры, но и с помощью любых других устройств ввода. Например, можно вводить символы жестами мыши или вводить японские символы с помощью клавиатуры с латинскими буквами.</p>

  <p>Чтобы выбрать метод ввода, нажмите правой кнопкой на текстовом виджете и в меню <gui>Методы ввода</gui> выберите нужный вам метод. Метод ввода по умолчанию не предусмотрен, поэтому обратитесь к документации по методам ввода, чтобы узнать, как ими пользоваться.</p>

</section>

</page>
