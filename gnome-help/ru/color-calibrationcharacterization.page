<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-calibrationcharacterization" xml:lang="ru">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Калибровка и профилирование являются двумя совершенно разными процессами.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>В чём разница между калибровкой и профилированием?</title>
  <p>Многие поначалу путают два этих принципиально разных, хотя и взаимосвязанных процесса. Калибровкой является изменение цветового поведения устройства. Это как правило достигается двумя способами:</p>
  <list>
    <item><p>Изменением внутренних настроек устройства</p></item>
    <item><p>Применением кривых к цветовым каналам устройства</p></item>
  </list>
  <p>Идея калибровки состоит в приведении устройства к некоторому точно известному состоянию применительно к цветопередаче. Часто она используется как повседневное средство поддержки воспроизводимого поведения. Как правило, калибровка хранится в специфичном для устройства или системы формате файлов, куда записаны либо его настройки, либо поканальные корректирующие кривые.</p>
  <p>Профилированием (иначе, характеризацией) называется <em>запись</em> того, как устройство воспроизводит цвет или откликается на него. Результат профилирования как правило сохраняется в файле профиля ICC для этого устройства. Такой профиль сам ничего не меняет, а лишь позволяет системному модулю управления цветом или отдельному приложению изменить цвета при условии использования профиля другого устройства. Достоверно перенести цвет с одного устройства на другое можно лишь обладая актуальным цветовым профилем для каждого из них.</p>
  <note>
    <p>Внимание: профиль действителен для устройства лишь в том случае, если оно находится в том же состоянии калибровки, при котором этот профиль был создан.</p>
  </note>
  <p>В случае с профилями мониторов возникает дополнительная путаница, связанная с тем, что информация о калибровке для удобства часто хранится в самом профиле. Согласно принятому стандарту она хранится в теге <em>vcgt</em>. Несмотря на то, что она всё же есть в профиле, обычные приложения с этими данными никак не работают. Точно так же нормальные приложения для калибровки никак не используют информацию, полученную при профилировании.</p>

</page>
