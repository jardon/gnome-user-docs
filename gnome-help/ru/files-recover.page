<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-recover" xml:lang="ru">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="seealso" xref="files-lost"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Удалённые файлы обычно отправляются в корзину, но могут быть восстановлены.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Восстановление файла из корзины</title>

  <p>При удалении файла в менеджере файлов, файл обычно помещается в <gui>корзину</gui>, откуда его можно восстановить.</p>

  <steps>
    <title>Чтобы восстановить файл из корзины:</title>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить вводить: <gui>Файлы</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Файлы</gui>, чтобы открыть файловый менеджер.</p>
    </item>
    <item>
      <p>Нажмите <gui>Корзина</gui> на боковой панели. Если вы не видите боковую панель, нажмите кнопку меню в правом верхнем углу окна и выберите <gui>Показать боковую панель</gui>.</p>
    </item>
    <item>
      <p>Если удалённый файл находится там, нажмите на него и выберите <gui>Восстановить из корзины</gui>. Он будет восстановлен в ту папку, из которой был удалён.</p>
    </item>
  </steps>

  <p>Если вы удалили файл нажатием <keyseq><key>Shift</key><key>Delete </key></keyseq> или из командной строки, то файл был удалён безвозвратно. Безвозвратно удалённые файлы невозможно восстановить из <gui>корзины</gui>.</p>

  <p>Существуют утилиты для восстановления информации, с помощью которых иногда удаётся восстановить безвозвратно удалённые файлы. Как правило, они не очень просты в использовании. Если вы случайно безвозвратно удалили файл, возможно, лучше попросить совета о том, как его восстановить, на форуме поддержки.</p>

</page>
