<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-resize" xml:lang="ru">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.25.90" date="2017-08-17" status="review"/>

    <desc>Уменьшить или увеличить размер файловой системы и раздела.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Настройка размера файловой системы</title>

  <p>Размер файловой системы может быть увеличен, чтобы использовать свободное пространство после её раздела. Часто это возможно даже при смонтированной файловой системе.</p>
  <p>Чтобы освободить место для другого раздела после файловой системы, её можно уменьшить с учётом свободного места внутри.</p>
  <p>Не все файловые системы поддерживают изменение размера.</p>
  <p>Размер раздела будет изменен вместе с размером файловой системы. Таким же образом можно изменить размер раздела без файловой системы.</p>

<steps>
  <title>Изменение размера файловой системы/раздела</title>
  <item>
    <p>Откройте <gui>Обзор</gui> и откройте приложение <app>Диски</app>.</p>
  </item>
  <item>
    <p>Выберите диск, содержащий необходимую файловую систему, из списка устройств слева. Если на диске более одного тома, выберите том, который содержит файловую систему.</p>
  </item>
  <item>
    <p>На панели инструментов под разделом <gui>Тома</gui> нажмите кнопку меню. Затем нажмите <gui>Изменить размер файловой системы…</gui> или <gui>Изменить размер…</gui>, если файловая система отсутствует.</p>
  </item>
  <item>
    <p>Откроется диалоговое окно, в котором можно выбрать новый размер. Файловая система будет смонтирована для расчёта минимального размера по объёму имеющихся данных. Если сжатие файловой системы не поддерживается, минимальным размером будет текущий размер. При сжатии оставьте достаточно места в файловой системе, чтобы она могла работать быстро и надежно.</p>
    <p>В зависимости от того, сколько данных необходимо переместить из уменьшенной части, изменение размера файловой системы может занять продолжительное время.</p>
    <note style="warning">
      <p>Изменение размера файловой системы автоматически включает <link xref="disk-repair">восстановление</link> файловой системы. Поэтому перед запуском рекомендуется сделать резервную копию важных данных. Действие нельзя останавливать, иначе оно приведет к повреждению файловой системы.</p>
    </note>
  </item>
  <item>
      <p>Подтвердите действие нажатием кнопки <gui style="button">Изменить размер</gui>.</p>
   <p>Если изменение размера смонтированной файловой системы не поддерживается. она будет отмонтирована. Будьте терпеливы во время изменения размера файловой системы.</p>
  </item>
  <item>
    <p>После завершения необходимых действий по изменению размера и восстановлению файловая система снова готова к использованию.</p>
  </item>
</steps>

</page>
