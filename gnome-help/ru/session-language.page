<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Андрэ Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Переключение пользовательского интерфейса и текста справочной системы на другой язык.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Изменение языка</title>

  <p>Среда рабочего стола и приложения поддерживают множество различных языков. Нужно лишь установить на компьютер подходящий языковой пакет.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Язык и регион</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Язык и регион</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите <gui>Язык</gui>.</p>
    </item>
    <item>
      <p>Выберите нужный вам регион и язык. Если вашего региона и языка нет в списке, нажмите <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> в нижней части списка, чтобы выбрать регион и язык из всех доступных регионов и языков.</p>
    </item>
    <item>
      <p>Нажмите кнопку <gui style="button">Выбрать</gui>, чтобы сохранить.</p>
    </item>
    <item>
      <p>Необходимо перезапустить сеанс, чтобы изменения вступили в силу. Для этого нажмите кнопку <gui style="button">Перезапуск…</gui>, или перезапустите сеанс вручную позже.</p>
    </item>
  </steps>

  <p>Некоторые переводы могут быть неполными, некоторые приложения могут вообще не иметь поддержки для вашего языка. Все непереведённые текстовые сообщения будут показаны на языке, который использовали разработчики приложения, обычно это американский вариант английского языка.</p>

  <p>В вашей домашней папке есть несколько специальных папок, в которых приложения могут сохранять музыку, изображения, документы и прочее. Эти папки используют стандартные имена, зависящие от выбранного языка. При следующем входе в систему вам будет предложено изменить названия этих папок на стандартные названия выбранного вами языка. Если вы планируете использовать новый язык постоянно, следует обновить названия папок.</p>

  <note style="tip">
    <p>Если в вашей системе несколько учётных записей пользователей, на экране входа в систему для каждой будет отдельный экземпляр панели <gui>Регион и язык</gui>. Нажмите кнопку <gui>Экран входа</gui> вверху справа, чтобы переключиться между двумя экземплярами.</p>
  </note>

</page>
