<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Юлита Инка (Julita Inca)</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Хуанхо Марин (Juanjo Marín)</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Добавление раскладок клавиатуры и переключение между ними.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Использование других раскладок клавиатуры</title>

  <p>Существуют сотни различных раскладок клавиатуры для разных языков. Часто даже для одного языка есть несколько раскладок, таких как раскладка Dvorak для английского языка. Можно сделать так, чтобы ваша клавиатура действовала как клавиатура с другой раскладкой, независимо от того, какие буквы и символы нанесены на клавиши. Это удобно, если вы часто переключаетесь между различными языками.</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите на <gui>Настройки</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Клавиатура</gui> на боковой панели, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите кнопку <gui>+</gui> в разделе <gui>Источники ввода</gui>, выберите язык, связанный с раскладкой, затем выберите раскладку и нажмите <gui>Добавить</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Если в вашей системе несколько учётных записей пользователей, на экране входа в систему для каждой будет отдельный экземпляр панели <gui>Регион и язык</gui>. Нажмите кнопку <gui>Экран входа</gui> вверху справа, чтобы переключиться между двумя экземплярами.</p>

    <p>Некоторые редко используемые варианты раскладки клавиатуры по умолчанию недоступны при нажатии кнопки <gui>+</gui>. Чтобы сделать доступными и эти источники ввода, вы можете открыть окно терминала, нажав <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>, и запустить эту команду:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Получить предварительный просмотр любой раскладки с помощью изображения можно, выбрав её в списке <gui>Источников ввода</gui> и нажав <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">предпросмотр</span></media></gui></p>
  </note>

  <p>Некоторые языки предлагают дополнительные параметры настройки. Отличить такие языки можно по дополнительному значку <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">предпросмотр</span></media></gui>. Для доступа к этим дополнительным параметрам выберите язык из списка в <gui>Источниках ввода</gui>, и появившаяся кнопка <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">параметры</span></media></gui> даст доступ к этим настройкам.</p>

  <p>При использовании нескольких раскладок можно выбрать, должны ли все окна использовать одну и ту же раскладку, или для каждого окна можно выбирать отдельную раскладку. Использование отдельных раскладок для каждого окна удобно, например, если вы пишете статью на другом языке в окне текстового редактора. При переключении между окнами выбранная вами раскладка будет запоминаться для каждого окна. Нажмите кнопку <gui style="button">Параметры</gui> чтобы указать, как именно обрабатывать несколько раскладок.</p>

  <p>В верхней панели показывается краткий идентификатор текущей раскладки, например, <gui>en</gui> для стандартной английской раскладки. Нажмите на индикатор раскладки и выберите в меню нужную раскладку. Если у выбранного языка есть дополнительные параметры, они будут показаны под списком доступных раскладок. Здесь даётся короткий обзор параметров. Для дополнительной справки можно также открыть изображение текущей раскладки клавиатуры.</p>

  <p>Проще всего сменить раскладку можно с помощью <gui>Источник ввода</gui> <gui>Комбинации клавиш</gui>. Эти комбинации клавиш открывают выбор<gui>Источника ввода</gui>, по которому можно передвигаться назад и вперёд. По умолчанию, переключиться на следующий источник ввода можно с помощью <keyseq><key xref="keyboard-key-super">Super</key><key>Пробел</key> </keyseq>, а на предыдущий — с помощью <keyseq><key>Shift</key><key>Super</key> <key>Пробел</key></keyseq>. Вы можете изменить эти комбинации клавиш в параметрах <gui>Клавиатура</gui> в разделе <guiseq><gui>Комбинации клавиш</gui><gui>Настроить комбинации клавиш</gui><gui>Ввод</gui></guiseq>.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
