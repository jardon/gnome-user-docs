<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="ru">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Устранение проблем с устройствами чтения карт памяти.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Проблемы с устройствами для чтения карт памяти</title>

<p>Многие компьютеры оснащены устройствами чтения SD, MMC, SM, MS, CF и других мультимедийных карт памяти. Они должны обнаруживаться и <link xref="disk-partitions">подключаться</link> автоматически. Вот несколько советов по устранению неполадок, на случай, если этого не происходит:</p>

<steps>
<item>
<p>Убедитесь в том, что карта памяти вставлена правильно. Многие карты памяти, когда они вставлены правильно, выглядят перевёрнутыми. Проверьте также, надёжно ли карта вставлена в отверстие. Некоторые карты памяти, особенно CF, для вставки требуют приложения небольшого усилия. (Будьте осторожны и не нажимайте слишком сильно! Если карта наткнулась на что-то твёрдое, не проталкивайте её.)</p>
</item>

<item>
  <p>Откройте приложение <app>Файлы</app> из меню <gui xref="shell-introduction#activities">Обзор</gui>. Отображается ли подключенная карта на левой боковой панели? Иногда карта появляется в этом списке, , но её файловая система не смонтирована; нажмите на название карты в списке, чтобы подключить карту. (Если боковая панель не видна, нажмите <key>F9</key> или нажмите в <gui style="menu">Файлах</gui> на кнопку меню в верхней панели и выберите <gui style="menuitem">Показать боковую панель</gui>.)</p>
</item>

<item>
  <p>Если карта памяти не отображается в боковой панели, нажмите <keyseq><key>Ctrl</key><key>L</key></keyseq>, затем затем введите <input>computer:///</input> и нажмите <key>Enter</key>. Если устройство чтения карт памяти настроено правильно, оно должно отображаться как диск, если карта памяти не вставлена, или как сама карта, если карта вставлена.</p>
</item>

<item>
<p>Если вы видите устройство чтения, но не карту памяти, проблема может быть в самой карте. Попробуйте вставить другую карту памяти или проверьте эту карту памяти на другом устройстве чтения, если есть такая возможность.</p>
</item>
</steps>

<p>Если карты или диски в локации <gui>Компьютер</gui> не отображаются, возможно, что ваше устройство чтения не работает в Linux из-за проблем с драйверами. Это более вероятно, если устройство чтения внутреннее (расположено внутри, а не снаружи компьютера). Лучшим решением будет напрямую подключить ваше устройство (фотокамеру, сотовый телефон и т. д.) к порту USB на компьютере. Существуют также внешние, подключаемые через USB устройства чтения карт памяти, которые поддерживаются в Linux намного лучше.</p>

</page>
