<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="ru">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Проверка уровня чернил или тонера в картриджах принтера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Как проверить уровень чернил или тонера в принтере?</title>

  <p>Способ проверки уровня чернил или тонера зависит от модели и производителя принтера, а также драйверов и приложений, установленных на компьютере.</p>

  <p>Некоторые принтеры снабжены встроенным экраном для отображения уровня чернил и другой информации.</p>

  <p>Некоторые принтеры сообщают компьютеру об уровне тонера или чернил, который можно найти на панели <gui>Принтеры</gui> в разделе <app>Настройки</app>. Уровень чернил будет показан вместе с информацией о принтере, если она доступна.</p>

  <p>Драйверы и инструменты контроля состояния для большинства принтеров HP предоставляются проектом HP Linux Imaging and Printing (HPLIP). Другие производители могут предоставлять проприетарные драйверы с аналогичными возможностями.</p>

  <p>В качестве альтернативы можно установить приложение для проверки уровня чернил Приложение <app>Inkblot</app> показывает уровень чернил для многих принтеров HP, Epson и Canon. Посмотрите, есть ли ваш принтер в <link href="http://libinklevel.sourceforge.net./#supported">списке поддерживаемых моделей</link>. Ещё одно приложение для контроля уровня чернил в принтерах Epson и некоторых других — <app>mtink</app>.</p>

  <p>Некоторые принтеры пока не до конца поддерживаются в Linux, а для некоторых индикация уровня чернил не предусмотрена конструкцией.</p>

</page>
