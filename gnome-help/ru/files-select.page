<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="ru">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Нажмите <keyseq><key>Ctrl</key><key>S</key></keyseq>, чтобы выделить несколько файлов с похожими именами.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Выделение файлов по шаблону</title>

  <p>Можно выделить в папке файлы, имена которых соответствуют заданному шаблону. Нажмите <keyseq><key>Ctrl</key><key>S</key></keyseq>, чтобы открыть окно <gui>Выделить объекты по шаблону</gui>. Наберите шаблон, используя части имён файлов и символы подстановки. Доступно два символа подстановки:</p>

  <list style="compact">
    <item><p><file>*</file> соответствует любому количеству любых символов (даже если символы отсутствуют).</p></item>
    <item><p><file>?</file> соответствует одному любому символу.</p></item>
  </list>

  <p>Например:</p>

  <list>
    <item><p>If you have an OpenDocument Text file, a PDF file, and an image that all have the same base name <file>Invoice</file>, select all three with the pattern</p>
    <example><p><file>Invoice.*</file></p></example></item>

    <item><p>Если имеется несколько фотографий с именами типа <file>Vacation-001.jpg</file>, <file>Vacation-002.jpg</file> и <file>Vacation-003.jpg</file>, выбрать их все можно с помощью шаблона</p>
    <example><p><file>Vacation-???.jpg</file></p></example></item>

    <item><p>Если вы отредактировали некоторые из этих фотографий и добавили в конце имён их файлов <file>-edited</file>, то выбрать только отредактированные фотографии можно с помощью шаблона</p>
    <example><p><file>Vacation-???-edited.jpg</file></p></example></item>
  </list>

</page>
