<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="ru">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Сигнал может быть слабым, или настройки сети не позволяют установить корректное соединение.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Почему беспроводное соединение постоянно прерывается?</title>

<p>Вы можете обнаружить, что беспроводное соединение было разорвано, даже если вы этого не планировали. Как правило, компьютер попытается восстановить соединение сразу после его разрыва (в такие моменты на значке сети в верхней панели будут показаны три точки), но всё равно такие моменты неприятны, особенно если в это время вы работали с интернетом.</p>

<section id="signal">
 <title>Слабый сигнал беспроводной сети</title>

 <p>Типичная причина прерывания беспроводного соединения — это слабый сигнал сети. Беспроводные сети имеют ограниченный радиус действия, и если компьютер находится далеко от беспроводной базовой станции, то сигнал может оказаться недостаточно сильным для поддержания соединения. Стены и другие объекты, расположенные между компьютером и базовой станцией, также могут ослаблять сигнал.</p>

 <p>Значок сети в верхней панели показывает интенсивность сигнала беспроводной сети. Если сигнал слишком слабый, попробуйте переместиться ближе к базовой станции.</p>

</section>

<section id="network">
 <title>Сетевое соединение установлено неправильно</title>

 <p>Иногда при подключении к беспроводной сети может сначала показаться, что соединение установлено успешно, но вскоре после этого связь прерывается. Обычно это случается из-за того, что компьютеру удалось подключиться к сети лишь частично — ему удаётся установить соединение, но по каким-то причинам он не может довести эту процедуру до конца, и соединение прерывается.</p>

 <p>Возможной причиной может быть ввод неправильного пароля для беспроводной сети или отсутствие доступа в эту сеть у вашего компьютера (например, потому что для входа в сеть требуется указать имя пользователя).</p>

</section>

<section id="hardware">
 <title>Ненадёжное беспроводное оборудование или драйверы</title>

 <p>Некоторые устройства для беспроводных сетей могут работать не вполне стабильно. Беспроводные сети довольно сложны, поэтому иногда у беспроводных карт и базовых станций возникают небольшие сбои и соединение прерывается. Это досаждает, но такие проблемы регулярно возникают у многих устройств. Если ваше соединение с беспроводной сетью время от времени прерывается, то причина может быть в этом. Если такие прерывания происходят достаточно часто, возможно, вам стоит подумать о приобретении другого устройства.</p>

</section>

<section id="busy">
 <title>Перегрузка в беспроводной сети</title>

 <p>К беспроводным сетям в общественных местах (например, университетах или кафе) часто пытаются подключиться много компьютеров одновременно. Иногда в сети возникает перегрузка, она не может обслужить все пытающиеся подключиться компьютеры, и некоторые из них отключаются.</p>

</section>

</page>
