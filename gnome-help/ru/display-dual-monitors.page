<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="ru">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Настройка дополнительного монитора.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

<title>Подключение дополнительного монитора к компьютеру</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Настройка дополнительного монитора</title>
  <p>Чтобы настроить дополнительный монитор, подключите монитор к компьютеру. Если система не распознала его сразу, или если вы хотите изменить его параметры:</p>

  <steps>
    <item>
      <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Мониторы</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Мониторы</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>На схеме расположения мониторов мышью перетащите мониторы в желаемые для вас позиции.</p>
      <note style="tip">
        <p>Когда активна панель <gui>Мониторы</gui>, в верхнем левом углу каждой миниатюры монитора располагаются числа.</p>
      </note>
    </item>
    <item>
      <p>Нажмите <gui>Основной дисплей</gui>, чтобы выбрать основной монитор.</p>

      <note>
        <p>Основной дисплей – это монитор, на котором отображается <link xref="shell-introduction">верхняя панель</link> и доступен <gui>Обзор</gui>.</p>
      </note>
    </item>
    <item>
      <p>Щёлкните на каждый монитор в списке и выберите ориентацию, разрешение, масштаб и частоту обновления.</p>
    </item>
    <item>
      <p>Нажмите <gui>Применить</gui>. Новые настройки сначала применяются в течение 20 секунд, и если эффект применения новых настроек вас не устроит, откат к старым настройкам произойдёт автоматически. Если же всё хорошо, нажмите <gui>Оставить эту конфигурацию</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Режимы отображения</title>
    <p>Для двух экранов доступны следующие режимы отображения:</p>
    <list>
      <item><p><gui>Объединить дисплеи</gui> - края экрана соединяются, чтобы объекты могли переходить с одного экрана на другой.</p></item>
      <item><p><gui>Зеркальное отображение</gui> - одна и та же информация отображается на двух экранах с одинаковым разрешением и ориентацией.</p></item>
    </list>
    <p>Когда требуется только один дисплей, например, внешний монитор, подключенный к ноутбуку с закрытой крышкой, другой монитор можно отключить. В списке выберите монитор, который вы хотите отключить, и выключите его.</p>
      
</section>

<section id="multiple">

  <title>Добавление нескольких мониторов</title>
    <p>При наличии более двух экранов единственным доступным режимом является - <gui>Объединить дисплеи</gui>.</p>
    <list>
      <item>
        <p>Нажмите кнопку дисплея, который вы хотите настроить.</p>
      </item>
      <item>
        <p>Перетащите экраны в необходимые позиции.</p>
      </item>
    </list>

</section>
</page>
