<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="parental-controls" xml:lang="ru">

  <info>
    <link type="guide" xref="user-accounts#manage"/>

    <revision version="gnome:43" status="final" date="2022-12-13"/>

    <credit type="author">
      <name>Anthony McGlone</name>
      <email>anthonymcglone2022@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Настройка родительского контроля для пользователей системы.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ser82-png</mal:name>
      <mal:email>asvmail.as@gmail.com</mal:email>
      <mal:years>2022-2023</mal:years>
    </mal:credit>
  </info>

  <title>Добавление родительского контроля</title>

  <p>Родители могут использовать приложение <gui>Родительский контроль</gui>, чтобы предотвратить доступ детей к вредоносному контенту.</p>

  <p>Администратор может использовать это приложение для следующих целей:</p>

  <list>
    <item>
      <p>Ограничения доступа пользователей к веб-браузерам и приложениям.</p>
    </item>
    <item>
      <p>Запрета пользователю устанавливать приложения.</p>
    </item>
    <item>
      <p>Разрешения пользователю доступ только к приложениям, соответствующим его возрасту.</p>
    </item>
  </list>

  <note>
    <p><gui>Родительский контроль</gui> требует установки приложений через Flatpak или Flathub.</p>
  </note> 

  <section id="restrictwebbrowsers">
    <title>Ограничение доступа к веб-браузерам</title>
    <p><gui>Родительский контроль</gui> позволяет администратору отключить доступ пользователя к браузеру.</p>
    <steps>
      <item>
        <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Пользователи</gui>.</p>
      </item>
      <item>
        <p>Выберите <gui>Пользователи</gui>, чтобы открыть этот раздел настроек.</p>
      </item>
      <item>
         <p>В разделе <gui>Другие пользователи</gui> выберите пользователя, к которому вы хотите применить элементы управления.</p>
      </item>
      <item>
         <p>Выберите вкладку <gui>Родительский контроль</gui>.</p>
         <note>
           <p>Эта вкладка появляется только в том случае, если <gui>Родительский контроль</gui> установлен и включён.</p>
         </note>
      </item>
      <item>
        <p>Нажмите <gui>Разблокировать</gui> в диалоговом окне <gui>Требуется разрешение</gui>.</p>
      </item>
       <item>
        <p>Введите пароль и выполните аутентификацию, чтобы разблокировать диалоговое окно <gui>Родительский контроль</gui>.</p>
      </item>
      <item>
        <p>Переведите переключатель <gui>Огранить доступ к веб-браузерам</gui> во включённое положение.</p>
      </item>
    </steps>
  </section>
  <section id="restrictapplications">
    <title>Ограничение доступа к приложениям</title>
    <p><gui>Родительский контроль</gui> предоставляет список приложений, доступ к которым может отключить администратор.</p>
    <note>
      <p>В этом списке не будут отображаться существующие приложения, если они не установлены через Flatpak.</p>
    </note>
    <steps>
      <item>
        <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Пользователи</gui>.</p>
      </item>
      <item>
        <p>Выберите <gui>Пользователи</gui>, чтобы открыть этот раздел настроек.</p>
      </item>
      <item>
         <p>В разделе <gui>Другие пользователи</gui> выберите пользователя, к которому вы хотите применить элементы управления.</p>
      </item>
      <item>
         <p>Выберите вкладку <gui>Родительский контроль</gui>.</p>
         <note>
           <p>Эта вкладка появляется только в том случае, если <gui>Родительский контроль</gui> установлен и включён.</p>
         </note>
      </item>
      <item>
        <p>Нажмите <gui>Разблокировать</gui> в диалоговом окне <gui>Требуется разрешение</gui>.</p>
      </item>
       <item>
        <p>Введите пароль и выполните аутентификацию, чтобы разблокировать диалоговое окно <gui>Родительский контроль</gui>.</p>
      </item>
      <item>
        <p>Нажмите <gui>Ограничить доступ к приложениям</gui>.</p>
      </item>
      <item>
        <p>Переведите переключатель рядом с приложением(ями) во включённое положение.</p>
      </item>
    </steps>
  </section>
  <section id="restrictapplicationinstallation">
    <title>Ограничение установки приложений</title>
    <p><gui>Родительский контроль</gui> позволяет администратору запрещать пользователю установку приложений.</p>
    <steps>
      <item>
        <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Пользователи</gui>.</p>
      </item>
      <item>
        <p>Выберите <gui>Пользователи</gui>, чтобы открыть этот раздел настроек.</p>
      </item>
      <item>
         <p>В разделе <gui>Другие пользователи</gui> выберите пользователя, к которому вы хотите применить элементы управления.</p>
      </item>
      <item>
         <p>Выберите вкладку <gui>Родительский контроль</gui>.</p>
         <note>
           <p>Эта вкладка появляется только в том случае, если <gui>Родительский контроль</gui> установлен и включён.</p>
         </note>
      </item>
      <item>
        <p>Нажмите <gui>Разблокировать</gui> в диалоговом окне <gui>Требуется разрешение</gui>.</p>
      </item>
       <item>
        <p>Введите пароль и выполните аутентификацию, чтобы разблокировать диалоговое окно <gui>Родительский контроль</gui>.</p>
      </item>
      <item>
        <p>Переведите переключатель <gui>Ограничить установку приложений</gui> во включённое положение.</p>
      </item>
    </steps>
  </section>
  <section id="applicationsuitability">
    <title>Ограничение доступа к приложениям на основании их соответствия возрастной группе</title>
    <p>Ограничение видимости приложений на основании их соответствия возрастной группе.</p>  
    <steps>
      <item>
        <p>Откройте <gui xref="shell-introduction#activities">Обзор</gui> и начните вводить: <gui>Пользователи</gui>.</p>
      </item>
      <item>
        <p>Выберите <gui>Пользователи</gui>, чтобы открыть этот раздел настроек.</p>
      </item>
      <item>
         <p>В разделе <gui>Другие пользователи</gui> выберите пользователя, к которому вы хотите применить элементы управления.</p>
      </item>
      <item>
         <p>Выберите вкладку <gui>Родительский контроль</gui>.</p>
         <note>
           <p>Эта вкладка появляется только в том случае, если <gui>Родительский контроль</gui> установлен и включён.</p>
         </note>
      </item>
      <item>
        <p>Нажмите <gui>Разблокировать</gui> в диалоговом окне <gui>Требуется разрешение</gui>.</p>
      </item>
       <item>
        <p>Введите пароль и выполните аутентификацию, чтобы разблокировать диалоговое окно <gui>Родительский контроль</gui>.</p>
      </item>
      <item>
        <p>Выберите возрастную группу в раскрывающемся списке <gui>Пригодность приложений</gui>.</p>
      </item>
    </steps>
  </section>
</page>
