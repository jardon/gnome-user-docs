<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="hu">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>Phil Bull</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Az egér hibás működésének ellenőrzése.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>Az egérmutató nem mozog</title>

<links type="section"/>

<section id="plugged-in">
 <title>Ellenőrizze, hogy az egér be van-e dugva</title>
 <p>Ha az egér kábellel csatlakozik, akkor ellenőrizze, hogy teljesen be van-e dugva a számítógépbe.</p>
 <p>Ha USB egér (téglalap alakú csatlakozóval), akkor próbálja meg másik USB-portba csatlakoztatni. Ha PS/2-es egér (kis kerek, hat tűs csatlakozóval), akkor győződjön meg róla, hogy a zöld egérportba van csatlakoztatva, és nem a lila billentyűzetportba. Ha nem volt bedugva, akkor lehet, hogy újra kell indítani a számítógépet.</p>
</section>

<section id="broken">
 <title>Ellenőrizze, hogy az egér valóban működik-e</title>
 <p>Csatlakoztassa az egeret egy másik számítógéphez, és ellenőrizze, hogy működik-e.</p>

 <p>Ha az egér optikai és be van kapcsolva, akkor az aljának világítania kell. Ha nem világít, akkor ellenőrizze, be van-e kapcsolva. Ha igen és így sem világít, akkor az egér hibás lehet.</p>
</section>

<section id="wireless-mice">
 <title>Vezeték nélküli egér ellenőrzése</title>

  <list>
    <item>
      <p>Győződjön meg róla, hogy az egér be van kapcsolva. Az egér alján gyakran található egy kapcsoló az egér teljes kikapcsolásához, hogy szállítás közben ne ébredjen fel folyton.</p>
    </item>
   <item><p>Ha Bluetooth egeret használ, akkor győződjön meg róla, hogy párosította egerét a számítógéppel. Lásd: <link xref="bluetooth-connect-device"/>.</p></item>
  <item>
   <p>Kattintson egy gombbal, és ellenőrizze, hogy az egérmutató megmozdul-e. Egyes vezeték nélküli egerek energiatakarékossági okból kikapcsolnak, és egy gomb megnyomásáig nem reagálnak. Lásd: <link xref="mouse-wakeup"/>.</p>
  </item>
  <item>
   <p>Ellenőrizze, hogy az egér akkumulátora fel van-e töltve.</p>
  </item>
  <item>
   <p>Győződjön meg róla, hogy a vevő teljesen be van-e dugva a számítógépbe.</p>
  </item>
  <item>
   <p>Ha az egér és a vevő eltérő rádiócsatornán működnek, akkor állítsa mindkettőt azonos csatornára.</p>
  </item>
  <item>
   <p>A kapcsolat létrehozásához szükség lehet egy gomb megnyomására az egéren, a vevőn vagy mindkettőn. Az egér használati utasításának részletesebb utasításokat kell tartalmaznia ilyen esetekre.</p>
  </item>
 </list>

 <p>A legtöbb rádiós egérnek automatikusan működnie kell a számítógéphez csatlakoztatásakor. Ha Bluetooth vagy infravörös vezeték nélküli egere van, akkor szükség lehet további lépésekre a beüzemeléséhez. A lépések az egér gyártójától vagy típusától függhetnek.</p>
</section>

</page>
