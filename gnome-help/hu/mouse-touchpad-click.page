<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="hu">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kattintás, húzás vagy görgetés koppintásokkal és kézmozdulatokkal az érintőtáblán.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Kattintás, húzás vagy görgetés az érintőtáblával</title>

  <p>Pusztán az érintőtáblával, külön hardvergombok használata nélkül is kattinthat egyszeresen vagy duplán, elemeket húzhat és görgetheti a képernyőtartalmat.</p>

  <note>
    <p>Az <link xref="touchscreen-gestures">érintőtábla kézmozdulataival</link> külön rész foglalkozik.</p>
  </note>

<section id="tap">
  <title>Koppintás a kattintáshoz</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Az érintőtáblára koppintva gombnyomás nélkül is kattinthat.</p>

  <list>
    <item>
      <p>A kattintáshoz koppintson az érintőtáblára.</p>
    </item>
    <item>
      <p>A dupla kattintáshoz koppintson duplán.</p>
    </item>
    <item>
      <p>Egy elem húzásához koppintson duplán, de ne emelje fel az ujját a második koppintás után. Húzza az elemet a célra, majd engedje fel az ujját az ejtéshez.</p>
    </item>
    <item>
      <p>Ha érintőtáblája támogatja a többujjas koppintást, akkor a jobb kattintást két ujjal egyszer koppintással is elvégezheti. Ellenkező esetben a jobb kattintáshoz hardvergombokat kell használnia. A második egérgomb nélküli jobb kattintással kapcsolatos további információkért lásd: <link xref="a11y-right-click"/>.</p>
    </item>
    <item>
      <p>Ha az érintőtábla támogatja a több ujjas koppintást, akkor három ujjal egyszerre koppintva <link xref="mouse-middleclick">középső kattintást</link> végezhet.</p>
    </item>
  </list>

  <note>
    <p>Több ujjal koppintva vagy húzva győződjön meg róla, hogy ujjai elegendő távolságban vannak egymástól. Ha ujjai túl közel vannak, akkor számítógépe egyetlen ujjnak tekintheti azokat.</p>
  </note>

  <steps>
    <title>Koppintás a kattintáshoz engedélyezése</title>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Egér és érintőtábla</gui> szavakat.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Egér és érintőtábla</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Az <gui>Érintőtábla</gui> szakaszban győződjön meg arról, hogy az <gui>Érintőtábla</gui> kapcsoló be van-e kapcsolva.</p>
      <note>
        <p>Az <gui>Érintőtábla</gui> szakasz csak akkor jelenik meg, ha rendszere rendelkezik érintőtáblával.</p>
      </note>
    </item>
   <item>
      <p>Kapcsolja be a <gui>Koppintás a kattintáshoz</gui> kapcsolót.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Kétujjas görgetés</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Az érintőtáblán két ujjának használatával görgethet.</p>

  <p>Ha ez ki van választva, akkor az egy ujjas koppintás és húzás a normál módon fog működni, de ha két ujjal végez húzást az érintőtábla bármely részén, az görgetést fog végezni. Mozgassa ujjait az érintőtábla teteje és alja között a függőleges görgetéshez, vagy vízszintesen az oldalirányú görgetéshez. Ilyenkor ujjait egy kicsit távolítsa el egymástól, nehogy az érintőtábla a közeli két ujját egyetlen nagy ujjnak érzékelje.</p>

  <note>
    <p>A kétujjas görgetés nem minden érintőtáblán működik.</p>
  </note>

  <steps>
    <title>Kétujjas görgetés engedélyezése</title>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Egér és érintőtábla</gui> szavakat.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Egér és érintőtábla</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Az <gui>Érintőtábla</gui> szakaszban győződjön meg arról, hogy az <gui>Érintőtábla</gui> kapcsoló be van-e kapcsolva.</p>
    </item>
    <item>
      <p>Kapcsolja be a <gui>Kétujjas görgetés</gui> kapcsolót.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Széleken történő görgetés</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Használja a széleken történő görgetést, ha csak egy ujjal szeretne görgetni.</p>

  <p>Az érintőtábla specifikációinak meg kell adnia a széleken történő görgetéshez szükséges érzékelők pontos helyét. Általában a függőleges görgetés érzékelője az érintőtábla jobb oldalán található. A vízszintes érzékelő az érintőtábla alsó szélén található.</p>

  <p>Függőleges görgetéshez húzza az ujját felfelé és lefelé az érintőtábla jobb szélén. Vízszintes görgetéshez húzza az ujját az érintőtábla alsó széle mentén.</p>

  <note>
    <p>A széleken történő görgetés nem minden érintőtáblán működik.</p>
  </note>

  <steps>
    <title>Széleken történő görgetés engedélyezése</title>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Egér és érintőtábla</gui> szavakat.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Egér és érintőtábla</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Az <gui>Érintőtábla</gui> szakaszban győződjön meg arról, hogy az <gui>Érintőtábla</gui> kapcsoló be van-e kapcsolva.</p>
    </item>
    <item>
      <p>Kapcsolja be a <gui>Széleken történő görgetés</gui> kapcsolót.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Természetes görgetés</title>

  <p>Az érintőtáblával úgy húzhatja a tartalmat, mintha egy valódi papírdarabot húzna.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni az <gui>Egér és érintőtábla</gui> szavakat.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Egér és érintőtábla</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Az <gui>Érintőtábla</gui> szakaszban győződjön meg arról, hogy az <gui>Érintőtábla</gui> kapcsoló be van-e kapcsolva.</p>
    </item>
    <item>
      <p>Kapcsolja be a <gui>Természetes görgetés</gui> kapcsolót.</p>
    </item>
  </steps>

  <note>
    <p>Ez a szolgáltatás <em>fordított görgetés</em> néven is ismert.</p>
  </note>

</section>

</page>
