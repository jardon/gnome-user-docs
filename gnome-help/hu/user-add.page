<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="hu">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Új felhasználó hozzáadása, hogy mások is használhassák a számítógépet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Új felhasználói fiók hozzáadása</title>

  <p>Több felhasználói fiókot is létrehozhat a számítógépén. Készítsen mindenkinek saját felhasználói fiókot családjában vagy munkahelyén. Minden felhasználónak lesz saját mappája, dokumentumai és beállításai.</p>

  <p>A felhasználói fiókok hozzáadásához <link xref="user-admin-explain">rendszergazdai jogosultság</link> szükséges.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Felhasználók</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Felhasználók</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Nyomja meg a jobb felső sarokban lévő <gui style="button">Feloldás</gui> gombot, és adja meg jelszavát.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui style="button">+ Felhasználó hozzáadása…</gui> gombot az <gui>Egyéb felhasználók</gui> alatt egy új felhasználói fiók hozzáadásához.</p>
    </item>
    <item>
      <p>Ha azt szeretné, hogy az új felhasználónak legyen <link xref="user-admin-explain">rendszergazda hozzáférése</link>, akkor válassza a <gui>Rendszergazda</gui> fióktípust a legördülő menüből.</p>
      <p>A rendszergazdák mindent megtehetnek, például hozzáadhatnak vagy törölhetnek felhasználókat, telepíthetnek szoftvereket és illesztőprogramokat, és megváltoztathatják a dátumot és az időt.</p>
    </item>
    <item>
      <p>Írja be az új felhasználó teljes nevét. A felhasználónév automatikusan kitöltésre kerül a teljes név alapján. Ha a javasolt felhasználónév nem tetszik, megváltoztathatja.</p>
    </item>
    <item>
      <p>Választhat, hogy beállít egy jelszót az új felhasználónak, vagy hagyja, hogy az első bejelentkezéskor ő maga állítsa be. Ha a jelszó azonnali beállítását választja, akkor a <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">jelszó előállítása</span></media></gui> ikont megnyomva automatikusan előállíthat egy véletlen jelszót.</p>
      <p>A felhasználó hálózati tartományhoz való csatlakoztatásához kattintson a <gui>Vállalati bejelentkezés</gui> gombra.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Hozzáadás</gui> gombra. Ha a felhasználó hozzáadásra került, akkor a <gui>Szülői felügyelet</gui> és a <gui>Nyelv</gui> beállítások is beállíthatók.</p>
    </item>
  </steps>

  <p>Ha a jelszót a fiók létrehozása után szeretné megváltoztatni, akkor válassza ki a fiókot, kattintson a <gui style="button">Feloldás</gui> gombra a jobb felső sarokban, és kattintson a jelenlegi jelszó állapotára.</p>

  <note>
    <p>A <gui>Felhasználók</gui> ablakban a fiók képének megváltoztatásához rákattinthat a képre a felhasználó neve mellett. Ez a kép fog megjelenni a bejelentkezési ablakban. A rendszer biztosít néhány alapértelmezett képet, illetve választhat a saját képei közül, vagy készíthet egyet a webkamerájával.</p>
  </note>

</page>
