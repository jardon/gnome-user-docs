<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="hu">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Az éjszakai fény a napszak szerint változtatja a kijelzők színét.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>A képernyő színhőmérsékletének beállítása</title>

  <p>A számítógép monitora kék fényt bocsájt ki, amely sötétedés után álmatlansághoz és a szemek erőltetéséhez vezet. Az <gui>Éjszakai fény</gui> a napszaknak megfelelően változtatja a kijelzők színhőmérsékletét, este melegebbé téve a színt. Az <gui>Éjszakai fény</gui> engedélyezéséhez:</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Kijelzők</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Kijelzők</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Kattintson az <gui>Éjszakai fény</gui> elemre a beállítások megnyitásához.</p>
    </item>
    <item>
      <p>Győződjön meg róla, hogy az <gui>Éjszakai fény</gui> kapcsoló be van-e kapcsolva.</p>
    </item>
    <item>
      <p>Az <gui>Ütemezés</gui> alatt válassza a <gui>Napnyugtától napkeltéig</gui> lehetőséget, hogy a képernyő színe a tartózkodási helye szerinti napnyugta és napkelte idejét kövesse. Válassza a <gui>Kézi ütemezés</gui> lehetőséget az <gui>Idők</gui> egyéni ütemezés szerinti beállításához.</p>
    </item>
    <item>
      <p>Használja a csúszkát a <gui>Színhőmérséklet</gui> beállításához, hogy melegebb vagy kevésbé meleg legyen.</p>
    </item>
  </steps>
      <note>
        <p>A <link xref="shell-introduction">felső sáv</link> megjeleníti, hogy az <gui>Éjszakai fény</gui> aktív-e. Ez átmenetileg letiltható a rendszer menüből.</p>
      </note>



</page>
