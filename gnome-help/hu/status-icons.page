<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="hu">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="shell-overview#apps"/>

    <!--
    Recommended statuses: stub incomplete draft outdated review candidate final
    -->
    <revision version="gnome:40" date="2021-03-12" status="candidate"/>
    <!--
    FIXME: I'm tentatively marking this final for GNOME 40, because it's at
    least no longer incorrect. But here's a lot to improve:

    * I'm not super happy with the "Other" catchall section at the end, but I also
      don't want to add lots of singleton sections. Tweak the presentation.

    * gnome-shell references network-wireless-disconnected but it doesn't exist:
      https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3827

    * The icons for disconnected states might change:
      https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/102

    * topbar-audio-volume-overamplified: Write docs on overamplification:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/117

    * Write docs on setting mic sensitivity, and link in a learn more item:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/116

    * topbar-network-wireless-connected: We're super handwavy about when this is
      used. We could use some docs on ad hoc networks.

    * topbar-screen-shared: We have no docs on the screen share portal:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/118

    * topbar-thunderbolt-acquiring: We have no docs on Thunderbolt:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/119
    -->

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A felső sáv jobb szélén található ikonok jelentésének magyarázata.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Mit jelentenek az ikonok a felső sávon?</title>

<p>Ez az oldal a képernyő jobb felső sarkában található ikonok jelentését magyarázza el. Pontosabban a rendszer által biztosított ikonok különböző változatait ismerteti.</p>

<links type="section"/>


<section id="universalicons">
<title>Akadálymentesítési ikonok</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-accessibility.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-accessibility.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Lehetővé teszi a különféle akadálymentesítési beállítások gyors be- és kikapcsolását.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-pointer.svg"/>
        </if:when>
        <media src="figures/topbar-pointer.svg"/>
      </if:choose>
    </td>
    <td><p>A kattintás típusát jelzi, amely a rámutatási kattintás használatakor fog történni.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="a11y">Tudjon meg többet az akadálymentesítésről.</link></p></item>
  <item><p><link xref="a11y-dwellclick">Tudjon meg többet a rámutatási kattintásról.</link></p></item>
</list>
</section>


<section id="audioicons">
<title>Hangikonok</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume.svg"/>
        </if:when>
        <media src="figures/topbar-audio-volume.svg"/>
      </if:choose>
    </td>
    <td><p>A hangszórók vagy fejhallgatók hangerejét jelzi.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-audio-volume-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>A hangszórók vagy fejhallgatók el vannak némítva.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity.svg"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity.svg"/>
      </if:choose>
    </td>
    <td><p>A mikrofon érzékenységét jelzi.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>A mikrofon el van némítva.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="sound-volume">Tudjon meg többet a hangerőről.</link></p></item>
</list>
</section>


<section id="batteryicons">
<title>Akkumulátorikonok</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-charging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-charging.svg"/>
      </if:choose>
    </td>
    <td><p>Az akkumulátor töltöttségi szintjét jelzi, miközben az akkumulátor töltődik.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100-charged.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100-charged.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Az akkumulátor teljesen feltöltve és töltődik.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-discharging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-discharging.svg"/>
      </if:choose>
    </td>
    <td><p>Az akkumulátor töltöttségi szintjét jelzi, miközben az akkumulátor nem töltődik.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Az akkumulátor teljesen feltöltve és nem töltődik.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-system-shutdown.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-system-shutdown.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Akkumulátor nélküli rendszereken megjelenő betáplálás ikon.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="power-status">Tudjon meg többet az akkumulátorállapotról.</link></p></item>
</list>
</section>


<section id="bluetoothicons">
<title>Bluetooth-ikonok</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg"/>
      </if:choose>
    </td>
    <td><p>A repülési üzemmód be van kapcsolva. A Bluetooth le van tiltva, ha a repülési üzemmód be van kapcsolva.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-bluetooth-active.svg"/>
        </if:when>
        <media src="figures/topbar-bluetooth-active.svg"/>
      </if:choose>
    </td>
    <td><p>Egy Bluetooth-eszköz párosítva van és használatban van. Ez az ikon csak akkor jelenik meg, ha van aktív eszköz, nem csak akkor, ha a Bluetooth engedélyezve van.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Tudjon meg többet a repülési üzemmódról.</link></p></item>
  <item><p><link xref="bluetooth">Tudjon meg többet a Bluetooth-ról.</link></p></item>
</list>
</section>


<section id="networkicons">
<info>
<!--
FIXME: I don't want a bare desc, because it ends up in the section links above.
But this section also gets a seealso from net-wireless.page, and we'd like a
desc for that. In Mallard 1.2, we can use role on desc. It works in Yelp, but
it's not in a schema yet, so it will cause validation errors in CI.
  <desc type="link" role="seealso">Explains the meanings of the networking icons in the top bar.</desc>
-->
</info>
<title>Hálózatkezelési ikonok</title>

<table shade="rows">
  <title>Vezeték nélküli (Wi-Fi) kapcsolatok</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>A repülési üzemmód be van kapcsolva. A vezeték nélküli hálózatkezelés le van tiltva, ha a repülési üzemmód be van kapcsolva.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódás vezeték nélküli hálózatra.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength.svg"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength.svg"/>
      </if:choose>
    </td>
    <td><p>Egy vezeték nélküli hálózati kapcsolat erősségét jelzi.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy vezeték nélküli hálózatra, de nincs jel.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy vezeték nélküli hálózatra. Ez az ikon csak akkor jelenik meg, ha a jelerősség nem határozható meg, például eseti hálózatokhoz való kapcsolódáskor.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-no-route.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-no-route.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy vezeték nélküli hálózatra, de nincs útvonal az internetre. Ez a hálózat hibás beállítása, vagy az internetszolgáltatójának üzemszünete miatt lehet.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Tudjon meg többet a repülési üzemmódról.</link></p></item>
  <item><p><link xref="net-wireless-connect">Tudjon meg többet a vezeték nélküli hálózatkezelésről.</link></p></item>
</list>

<table shade="rows">
  <title>Mobilhálózat-kezelés (mobil széles sáv)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>A repülési üzemmód be van kapcsolva. A mobilhálózat-kezelés le van tiltva, ha a repülési üzemmód be van kapcsolva.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódás mobilhálózatra.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal.svg"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal.svg"/>
      </if:choose>
    </td>
    <td><p>Egy mobilhálózati kapcsolat erősségét jelzi.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy mobilhálózatra, de nincs jel.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy mobilhálózathoz. Ez az ikon csak akkor jelenik meg, ha a jelerősség nem határozható meg, például Bluetooth-on keresztüli kapcsolódáskor. Ha a jelerősség meghatározható, akkor a jelerősség ikon jelenik meg helyette.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Tudjon meg többet a repülési üzemmódról.</link></p></item>
  <item><p><link xref="net-mobile">Tudjon meg többet a mobilhálózat-kezelésről.</link></p></item>
</list>

<table shade="rows">
  <title>Vezetékes kapcsolatok</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódás vezetékes kapcsolatra.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired.svg"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy vezetékes hálózatra.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-disconnected.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-disconnected.svg"/>
      </if:choose>
    </td>
    <td><p>A hálózati kapcsolat bontva.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-no-route.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-no-route.svg"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy vezetékes hálózatra, de nincs útvonal az internetre. Ez a hálózat hibás beállítása, vagy az internetszolgáltatójának üzemszünete miatt lehet.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wired-connect">Tudjon meg többet a vezetékes hálózatkezelésről.</link></p></item>
</list>

<table shade="rows">
  <title>VPN (virtuális magánhálózat)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódás virtuális magánhálózatra.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn.svg"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódva egy virtuális magánhálózatra.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-vpn-connect">Tudjon meg többet a virtuális magánhálózatokról.</link></p></item>
</list>

</section>


<section id="othericons">
<title>Egyéb ikonok</title>
<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-input-method.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-input-method.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>A jelenleg használt billentyűzetkiosztást vagy beviteli módot jelzi. Kattintson az ikonra egy másik kiosztás kiválasztásához. A billentyűzetkiosztás menü csak akkor jelenik meg, ha több beviteli módot állított be.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-find-location.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-find-location.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Egy alkalmazás jelenleg hozzáfér az Ön tartózkodási helyéhez. A helymeghatározáshoz való hozzáférést letilthatja a menüben.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-night-light.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-night-light.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Az éjszakai fény megváltoztatta a kijelző színhőmérsékletét a szem erőltetésének csökkentéséhez. Az éjszakai fényt átmenetileg letilthatja a menüben.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-media-record.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-media-record.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Jelenleg képernyőfelvételt rögzít a teljes képernyőjéről.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-screen-shared.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-screen-shared.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Egy alkalmazás éppen megosztja a képernyőt vagy egy másik ablakot.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-thunderbolt-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-thunderbolt-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Kapcsolódás egy Thunderbolt-eszközhöz, például egy dokkolóhoz.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="keyboard-layouts">Tudjon meg többet a billentyűzetkiosztásokról.</link></p></item>
  <item><p><link xref="privacy-location">Tudjon meg többet az adatvédelemről és a helymeghatározó szolgáltatásokról.</link></p></item>
  <item><p><link xref="display-night-light">Tudjon meg többet az éjszaki fényről és a színhőmérsékletről.</link></p></item>
  <item><p><link xref="screen-shot-record">Tudjon meg többet a képernyőképekről és a képernyővideókról.</link></p></item>
</list>

</section>

</page>
