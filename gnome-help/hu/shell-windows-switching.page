<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="hu">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nyomja meg a <keyseq><key>Super</key><key>Tab</key></keyseq> billentyűkombinációt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>Váltás ablakok között</title>

  <p>Az összes grafikus felhasználói felülettel rendelkező futó alkalmazás látható az <em>ablakváltóban</em>. Ez egyetlen lépésbe sűríti a feladatok közti váltást, és teljes képet ad az éppen futó alkalmazásokról.</p>

  <p>Egy munkaterületről:</p>

  <steps>
    <item>
      <p>Nyomja meg a <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> billentyűkombinációt az <gui>ablakváltó</gui> megjelenítéséhez.</p>
    </item>
    <item>
      <p>Engedje fel a <key xref="keyboard-key-super">Super</key> billentyűt a következő (kiemelt) ablak kiválasztásához az ablakváltóban.</p>
    </item>
    <item>
      <p>Egyébként a <key xref="keyboard-key-super">Super</key> lenyomva tartása mellett a <key>Tab</key> ismételt megnyomásával végiglépkedhet a megnyitott ablakok listáján, a <keyseq><key>Shift</key><key>Tab</key></keyseq> megnyomásával pedig visszafelé lépkedhet.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Használhatja az alsó sávon lévő ablaklistát is a nyitott ablakok eléréséhez és a köztük való váltáshoz.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>Az ablakok az ablakváltóban alkalmazás szerint vannak csoportosítva. A több ablakkal rendelkező alkalmazások előnézetei a végiglépkedés során egy listában jelennek meg. A listán való végiglépkedéshez nyomja le a <key xref="keyboard-key-super">Super</key> billentyűt, és nyomja meg a <key>`</key> (magyar billentyűzeteken: a <key>Tab</key> fölötti <key>0</key>) billentyűt.</p>
  </note>

  <p>Az ablakváltóban az alkalmazásikonok között a <key>→</key> vagy <key>←</key> billentyűkkel lépkedhet, vagy az egérrel kattintva is kiválaszthat egyet.</p>

  <p>Az egyetlen ablakkal rendelkező alkalmazások előnézetei a <key>↓</key> billentyű megnyomásakor jelennek meg.</p>

  <p>A <gui>Tevékenységek</gui> áttekintésben kattintson egy <link xref="shell-windows">ablakra</link> az arra való átváltáshoz és az áttekintés elhagyásához. Ha több <link xref="shell-windows#working-with-workspaces">munkaterületet</link> használ, akkor kattintson az egyes munkaterületekre az azokon lévő nyitott ablakok megjelenítéséhez.</p>

</page>
