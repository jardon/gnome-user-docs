<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="hu">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>GNOME dokumentációs projekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Rendszergazdai jogosultság adásával engedélyezheti felhasználóknak rendszerszintű változtatások végrehajtását.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bojtos Péter</mal:name>
      <mal:email>ptr at ulx dot hu</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Griechisch Erika</mal:name>
      <mal:email>griechisch.erika at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kucsebár Dávid</mal:name>
      <mal:email>kucsdavid at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lakatos 'Whisperity' Richárd</mal:name>
      <mal:email>whisperity at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lukács Bence</mal:name>
      <mal:email>lukacs.bence1 at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Meskó Balázs</mal:name>
      <mal:email>mesko dot balazs at fsf dot hu</mal:email>
      <mal:years>2021, 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nagy Zoltán</mal:name>
      <mal:email>dzodzie at gmail dot com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur dot balazs at fsf dot hu</mal:email>
      <mal:years>2016, 2019, 2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>Rendszergazdai jogosultság adása</title>

  <p>A rendszergazdai jogosultság adásával eldöntheti ki hajthat végre változtatásokat a rendszer fontos részein is. Eldöntheti, melyik felhasználónak legyen rendszergazdai jogosultsága, és melyiknek ne. Ezzel hatékonyan teheti biztonságossá rendszerét, és megakadályozhatja az esetleg káros, felhatalmazás nélküli változtatásokat.</p>

  <p>A fióktípusok megváltoztatásához <link xref="user-admin-explain">rendszergazdai jogosultság</link> szükséges.</p>

  <steps>
    <item>
      <p>Nyissa meg a <gui xref="shell-introduction#activities">Tevékenységek</gui> áttekintést, és kezdje el begépelni a <gui>Felhasználók</gui> szót.</p>
    </item>
    <item>
      <p>Kattintson a <gui>Felhasználók</gui> elemre a panel megnyitásához.</p>
    </item>
    <item>
      <p>Nyomja meg a jobb felső sarokban lévő <gui style="button">Feloldás</gui> gombot, és adja meg jelszavát.</p>
    </item>
    <item>
      <p>Az <gui>Egyéb felhasználók</gui> alatt válassza ki azt a felhasználót, akinek meg szeretné változtatni a jogosultságait.</p>
    </item>
    <item>
      <p>Kapcsolja be a <gui>Rendszergazda</gui> kapcsolót.</p>
    </item>
    <item>
      <p>A felhasználó jogosultságai a következő bejelentkezésekor fognak megváltozni.</p>
    </item>
  </steps>

  <note>
    <p>A rendszer első felhasználói fiókja általában a rendszergazdai jogosultsággal rendelkező. Ez a felhasználói fiók a rendszer telepítésekor készült.</p>
    <p>Nem bölcs dolog túl sok <gui>rendszergazdai</gui> jogosultsággal rendelkező felhasználót létrehozni egy rendszeren.</p>
  </note>

</page>
