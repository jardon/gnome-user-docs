<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="power-whydim" xml:lang="ca">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="41" date="2021-09-08" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Quan l'ordinador estigui inactiu la pantalla s'enfosquirà per a estalviar energia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Per què s'enfosqueix la pantalla després d'una estona?</title>

  <p>Si és possible configurar la brillantor de la vostra pantalla, s'enfosquirà quan l'ordinador estigui inactiu per a estalviar energia. Quan es torni a utilitzar l'ordinador, la pantalla il·luminarà.</p>

  <p>Per a aturar que la pantalla s'enfosqueixi sola:</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Energia</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Canvieu <gui>Atenua la pantalla</gui> a desactivada a la secció <gui>Estalvi d'energia</gui>.</p>
    </item>
  </steps>

  <p>The screen will always dim, and dim more aggressively when choosing
  the “Power Saver” power mode. If you donʼt want the screen to dim at all,
  select the “Balanced” power mode instead.</p>

</page>
