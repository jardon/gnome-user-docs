<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="ca">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cancel·leu un treball d'impressió pendent i elimineu-lo de la cua.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Cancel·lar, fer pausa o alliberar un treball d'impressió</title>

  <p>Podeu cancel·lar un treball d'impressió pendent i eliminar-lo de la cua a la configuració de la impressora.</p>

  <section id="cancel-print-job">
    <title>Cancel·lar un treball d'impressió</title>

  <p>Si accidentalment heu començat a imprimir un document, podeu cancel·lar la impressió de manera que no calgui gastar tinta ni paper.</p>

  <steps>
    <title>Com cancel·lar un treball d'impressió:</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Impressores</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Cliqueu al botó <gui>Mostra treballs</gui> a la banda dreta del diàleg <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Cancel·leu el treball d'impressió clicant al botó de parada.</p>
    </item>
  </steps>

  <p>Si això no cancel·la el treball d'impressió com esperàveu, proveu de prémer el botó <em>cancel·la</em> de la vostra impressora.</p>

  <p>Com a últim recurs, especialment si teniu un treball d'impressió molt gran amb moltes pàgines que no es cancel·laran, retireu el paper de la safata d'entrada de la impressora. La impressora s'adonarà que no té paper i aturarà la impressió. A continuació, podeu provar de cancel·lar de nou el treball d'impressió o provar d'apagar la impressora i tornar-la a encendre.</p>

  <note style="warning">
    <p>Aneu amb compte amb no malmetre la impressora quan retireu el paper. Si heu de tibar amb força per a enretirar-lo, probablement és millor deixar-lo on és.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Fer una pausa i alliberar un treball d'impressió</title>

  <p>Si voleu fer una pausa o alliberar un treball d'impressió, podeu fer-ho, anant al diàleg de treballs a la configuració de la impressora i clicant al botó corresponent.</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Impressores</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Impressores</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Cliqueu al botó <gui>Mostra treballs</gui> a la dreta del diàleg d'<gui>Impressores</gui> i poseu en pausa o allibereu el treball d'impressió segons les vostres necessitats.</p>
    </item>
  </steps>

  </section>

</page>
