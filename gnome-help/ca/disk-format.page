<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-format" xml:lang="ca">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>
    <revision pkgversion="3.37.2" date="2020-08-13" status="review"/>

    <desc>Elimineu tots els fitxers i carpetes d'un disc dur extern o una unitat flaix USB mitjançant la formatació.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Suprimir-ho tot d'un disc extraïble</title>

  <p>Si teniu un disc extraïble, com un llapis de memòria USB o un disc dur extern, de vegades voldreu eliminar completament tots els seus fitxers i carpetes. Podeu fer-ho <em>formatant</em> el disc — això elimina tots els fitxers del disc i el deixa buit.</p>

<steps>
  <title>Formatar el disc extraïble</title>
  <item>
    <p>Obrir <app>Discs</app> des de la vista d'<gui>Activitats</gui>.</p>
  </item>
  <item>
    <p>Seleccioneu el disc que voleu esborrar de la llista de dispositius d'emmagatzematge de l'esquerra.</p>

    <note style="warning">
      <p>Assegureu-vos que heu seleccionat el disc correcte! Si seleccioneu el disc incorrecte, se suprimiran tots els fitxers de l'altre disc!</p>
    </note>
  </item>
  <item>
    <p>A la barra d'eines sota la secció <gui>Volums</gui>, cliqueu al botó de menú i, seguidament, cliqueu a <gui>Formata la partició…</gui>.</p>
  </item>
  <item>
    <p>A la finestra que es desplega, trieu un <gui>Tipus</gui> de sistema de fitxers pel disc.</p>
   <p>Si utilitzeu el disc en ordinadors amb Linux i també en ordinadors amb Windows i Mac OS, trieu <gui>FAT</gui>. Si tan sols l'utilitzeu amb Windows, l'opció <gui>NTFS</gui> pot ser més adient. Teniu una breu descripció del tipus de sistema de fitxers a cada etiqueta.</p>
  </item>
  <item>
    <p>Doneu un nom al disc i cliqueu a <gui>Següent</gui> per a continuar i mostrar una finestra de confirmació. Comproveu els detalls amb cura i cliqueu a <gui>Formata</gui> per a esborrar el disc.</p>
  </item>
  <item>
    <p>Un cop formatat, cliqueu a la icona d'expulsió per a eliminar el disc de forma segura. Ara hauria d'estar buit i a punt per a tornar a utilitzar.</p>
  </item>
</steps>

<note style="warning">
 <title>La formatació d'un disc no suprimeix els fitxers de forma segura</title>
  <p>La formatació d'un disc no és una forma completament segura d'esborrar totes les seves dades. Un disc formatat no sembla que tingui fitxers, però és possible que un programari de recuperació especial pugui recuperar-los. Si necessiteu eliminar els fitxers de forma segura, haureu d'utilitzar una utilitat de la línia d'ordres, com és <app>shred</app>.</p>
</note>

</page>
