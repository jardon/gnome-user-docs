<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="ca">

  <info>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-12"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definiu les funcions del botó i la sensació de pressió del llapis Wacom.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Configurar el llapis</title>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Tauleta Wacom</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Tauleta Wacom</gui> per a obrir el quadre.</p>
      <note style="tip"><p>Si no es detecta cap tauleta, se us demanarà que <gui>connecteu o activeu la tauleta Wacom</gui>. Cliqueu a <gui>Bluetooth</gui> a la barra lateral per a connectar una tauleta sense fil.</p></note>
    </item>
    <item>
      <p>Hi ha una secció que conté paràmetres específics per a cada llapis, amb el nom del dispositiu (la classe llapis) i el diagrama a la part superior.</p>
      <note style="tip"><p>Si no es detecta cap llapis, se us demanarà que <gui>Moveu el llapis a prop de la tauleta per a configurar-la</gui>.</p></note>
      <p>Aquests paràmetres es poden ajustar:</p>
      <list>
        <item><p><gui>Sensació de pressió de la punta:</gui> utilitzeu el control lliscant per a ajustar la «sensació» entre <gui>Suau</gui> i <gui>Ferma</gui>.</p></item>
        <item><p>Configuració del botó / roda de desplaçament (aquests canvien per a reflectir el llapis). Cliqueu al menú que hi ha al costat de cada etiqueta per a seleccionar una d'aquestes funcions: Clic del botó del mig del ratolí, Clic amb el botó dret del ratolí, Enrere o Endavant.</p></item>
     </list>
    </item>
    <item>
      <p>Cliqueu a <gui>Prova la configuració</gui> a la barra de capçalera per a mostrar un sketchpad on podeu provar l'arranjament del llapis.</p>
    </item>
  </steps>

</page>
