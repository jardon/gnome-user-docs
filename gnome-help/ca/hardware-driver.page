<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="ca">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Un controlador de maquinari / dispositiu permet a l'ordinador utilitzar els dispositius que s'hi adjunten.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Què és un disc?</title>

<p>Els dispositius són les parts «físiques» del vostre ordinador. Poden ser <em>externs</em> com impressores o el monitor, o <em>interns</em> com les targetes de gràfics i d'àudio.</p>

<p>Per tal que l'ordinador pugui utilitzar aquests dispositius, necessita saber com comunicar-s'hi. Això es fa mitjançant una peça de programari anomenada a <em>controlador del dispositiu</em>.</p>

<p>Quan inseriu un dispositiu a l'ordinador, haureu de tenir instal·lat el controlador correcte per a aquest dispositiu. Per exemple, si connecteu una impressora però el controlador correcte no està disponible, no podreu utilitzar-la. Normalment, cada model de dispositiu utilitza un controlador que no és compatible amb cap altre model.</p>

<p>A Linux, els controladors de la majoria dels dispositius estan instal·lats de manera predeterminada, de manera que tot hauria de funcionar correctament quan el connecteu. Tanmateix, és possible que els controladors necessitin instal·lar-se manualment o no estiguin disponibles.</p>

<p>A més, alguns controladors existents són incomplets o parcialment no funcionals. Per exemple, podeu trobar que la vostra impressora no pot fer impressions a doble cara, però que és completament funcional.</p>

</page>
