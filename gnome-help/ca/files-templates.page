<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="ca">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Creï ràpidament documents nous a partir de plantilles personalitzades.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Plantilles per als tipus de documents utilitzats habitualment</title>

  <p>Si crea sovint documents basats en el mateix contingut, pot beneficiar-se de l'ús de plantilles. Una plantilla de fitxer pot ser un document de qualsevol tipus amb el format o el contingut que vulgui reutilitzar. Per exemple, pot crear un document de plantilla amb la seva capçalera.</p>

  <steps>
    <title>Fer una nova plantilla</title>
    <item>
      <p>Creï un document que utilitzarà com a plantilla. Per exemple, pot fer la seva capçalera amb una aplicació de processament de text.</p>
    </item>
    <item>
      <p>Desi el fitxer amb el contingut de la plantilla a la carpeta <file>Plantilles</file> a la seva carpeta <file>Inici</file>. Si la carpeta <file>Plantilles</file> no existeix, primer l'heu de crear.</p>
    </item>
  </steps>

  <steps>
    <title>Utilitzi una plantilla per a crear un document</title>
    <item>
      <p>Obrir la carpeta on col·locar el document nou.</p>
    </item>
    <item>
      <p>Cliqueu amb el botó dret a qualsevol lloc de l'espai buit de la carpeta i, a continuació, trieu <gui style="menuitem">Document nou</gui>. Els noms de les plantilles disponibles es mostraran al submenú.</p>
    </item>
    <item>
      <p>Trieu la plantilla que desitgi de la llista.</p>
    </item>
    <item>
      <p>Feu doble clic al fitxer per a obrir-lo i començar a editar-lo. Podeu voler <link xref="files-rename">reanomenar-lo</link> quan acabi.</p>
    </item>
  </steps>

</page>
