<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="ca">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canviar el nom del fitxer o la carpeta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Canviar el nom d'un fitxer o carpeta</title>

  <p>Igual que amb altres gestors de fitxers, podeu utilitzar <app>Fitxers</app> per a canviar el nom d'un fitxer o una carpeta.</p>

  <steps>
    <title>Per a canviar el nom d'un fitxer o carpeta:</title>
    <item><p>Cliqueu amb el botó dret sobre l'element i seleccioneu <gui>Canvia el nom</gui>, o seleccioneu el fitxer i premeu <key>F2</key>.</p></item>
    <item><p>Escriviu el nou nom i premeu <key>Retorn</key> o cliqueu a <gui>Canvia el nom</gui>.</p></item>
  </steps>

  <p>També podeu canviar el nom d'un fitxer des de la finestra <link xref="nautilus-file-properties-basic">propietats</link>.</p>

  <p>Quan canvieu el nom d'un fitxer, només se selecciona la primera part del nom del fitxer, no l'extensió (la part després de l'últim <file>.</file>). L'extensió normalment indica quin tipus de fitxer és (per exemple, <file>fitxer.pdf</file> és un document PDF), i normalment no voldreu canviar-la. Si també heu de canviar l'extensió, seleccioneu el nom complet i canvieu-lo.</p>

  <note style="tip">
    <p>Si us heu equivocat de fitxer en canviar de nom, o heu assignat un nom incorrecte al vostre fitxer, podeu desfer el canvi de nom. Per a desfer l'acció, immediatament cliqueu al botó de menú de la barra d'eines i seleccioneu <gui>Desfés el canvi de nom</gui>, o premeu <keyseq><key>Ctrl</key><key>Z</key></keyseq>, per a restaurar el nom anterior.</p>
  </note>

  <section id="valid-chars">
    <title>Caràcters vàlids per als noms dels fitxers</title>

    <p>Podeu utilitzar qualsevol caràcter excepte el caràcter <file>/</file> (barra) en els noms dels fitxers. Tanmateix, alguns dispositius utilitzen un <em>sistema de fitxers</em> amb més restriccions a l'hora de posar els noms. Per tant, és una bona pràctica evitar els següents caràcters en els noms dels fitxers: <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>Si anomeneu un fitxer amb <file>.</file> com a primer caràcter, el fitxer restarà <link xref="files-hidden">ocult</link> quan intenteu visualitzar-lo al gestor de fitxers.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Problemes comuns</title>

    <terms>
      <item>
        <title>El nom del fitxer ja està en ús</title>
        <p>No podeu tenir dos fitxers o carpetes amb el mateix nom dins de la mateixa carpeta. Si intenteu canviar el nom d'un fitxer a un nom que ja existeix a la carpeta on esteu treballant, el gestor de fitxers no ho permetrà.</p>
        <p>Els noms dels fitxers i carpetes distingeixen entre majúscules i minúscules, així que el nom del fitxer <file>Fitxer.txt</file> no és el mateix que <file>FITXER.txt</file>. L'ús de diferents noms de fitxers com aquests està permès, encara que no és recomanable.</p>
      </item>
      <item>
        <title>El nom del fitxer és massa llarg</title>
        <p>En alguns sistemes de fitxers, els noms dels fitxers no poden tenir de més de 255 caràcters. Aquest límit de 255 caràcters inclou tant el nom del fitxer com el camí d'accés al fitxer (per exemple, <file>/home/wanda/Documents/work/business-proposals/…</file>), de manera que haureu d'evitar noms de fitxers i carpetes llargs sempre que sigui possible.</p>
      </item>
      <item>
        <title>L'opció per a canviar el nom és grisa</title>
        <p>Si <gui>Canvia el nom</gui> està en gris, no teniu permís per a canviar el nom del fitxer. Heu de tenir precaució al canviar el nom d'aquests fitxers, ja que reanomenar fitxers protegits pot fer que el vostre sistema quedi inestable. Consultar <link xref="nautilus-file-properties-permissions"/> per a més informació.</p>
      </item>
    </terms>

  </section>

</page>
