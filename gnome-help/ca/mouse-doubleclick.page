<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="ca">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controleu la rapidesa amb què s'ha de prémer el botó del ratolí una segona vegada per a fer doble clic.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Ajustar la velocitat del doble de clic</title>

  <p>El doble clic tan sols succeeix quan premeu el botó del ratolí dues vegades ràpidament. Si la segona vegada es fa massa tard després de la primera, només obtindreu dos clics diferents, no un doble clic. Si teniu dificultats per a prémer el botó del ratolí ràpidament, heu d'augmentar el temps d'espera.</p>

  <steps>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Accés universal</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Accés universal</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>Sota <gui>Apuntar i clicar</gui>, ajusteu el lliscador de <gui>Retard del doble clic</gui> a un valor que trobeu còmode.</p>
    </item>
  </steps>

  <p>Si el vostre ratolí fa doble clic quan voleu fer un únic clic fins i tot si heu augmentat el temps d'espera del doble clic, el vostre ratolí podria estar defectuós. Intenteu connectar un ratolí diferent a l'ordinador i valideu si funciona correctament. Alternativament, connecteu el ratolí a un altre ordinador i mireu si encara es manté el mateix problema.</p>

  <note>
    <p>Aquesta configuració afectarà tant el ratolí com el touchpad, així com qualsevol altre dispositiu apuntador.</p>
  </note>

</page>
