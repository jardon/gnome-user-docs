<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-setup" xml:lang="ca">

  <info>
    <link type="guide" xref="contacts" group="#first"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="draft"/>
    <revision pkgversion="3.12" date="2014-02-26" status="final"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.22" date="2017-03-19" status="draft"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <credit type="author copyright">
      <name>Paul Cutler</name>
      <email>pcutler@gnome.org</email>
      <years>2017</years>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Guardeu els vostres contactes en una llibreta d'adreces local o en un compte en línia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Iniciar Contactes per primer cop</title>

  <p>Quan executeu <app>Contactes</app> per primer cop, s'obre la finestra <gui>Selecciona la llibreta d'adreces</gui>.</p>
   
  <p>Si teniu configurats <link xref="accounts">comptes en línia</link> es llisten amb la <gui>Llibreta d'adreces local</gui>. Seleccioneu un element de la llista i cliqueu a <gui style="button">Fet</gui>.</p>
  
  <p>Tots els contactes nous que creeu es desaran a la llibreta d'adreces que trieu. També podeu veure i editar contactes a altres llibretes d'adreces.</p>
  
  <p>Si no teniu comptes en línia configurats, cliqueu a <gui style="button">Comptes en línia</gui> per a començar a configurar-los. Si no voleu configurar comptes en línia en aquest moment, cliqueu a <gui style="button">Llibreta d'adreces local</gui>.</p>

</page>
