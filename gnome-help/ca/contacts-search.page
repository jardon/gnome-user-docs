<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-search" xml:lang="ca">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cercar un contacte.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Cercar un contacte</title>

  <p>Podeu cercar un contacte en línia de dues maneres:</p>

  <steps>
    <item>
      <p>A la descripció general d'<gui>Activitats</gui>, comenceu a escriure el nom del contacte.</p>
    </item>
    <item>
      <p>Els contactes coincidents apareixeran a la vista general en comptes de la llista habitual d'aplicacions.</p>
    </item>
    <item>
      <p>Premeu <key>Retorn</key> per a seleccionar el contacte a la part superior de la llista o cliqueu al contacte que vulgueu seleccionar si no es troba a la part superior.</p>
    </item>
  </steps>

  <p>Per a cercar des de dins <app>Contactes</app>:</p>

  <steps>
    <item>
      <p>Cliqueu dins del camp de cerca.</p>
    </item>
    <item>
      <p>Comenceu a escriure el nom del contacte.</p>
    </item>
  </steps>

</page>
