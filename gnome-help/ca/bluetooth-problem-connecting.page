<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="ca">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Potser l'adaptador està apagat o no disposa dels controladors necessaris, o el Bluetooth està desactivat o blocat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>No puc connectar el meu dispositiu Bluetooth</title>

  <p>Hi ha diversos motius pels quals no podeu connectar-vos a un dispositiu Bluetooth, com un telèfon o uns auriculars.</p>

  <terms>
    <item>
      <title>La connexió està blocada o no és de confiança</title>
      <p>Alguns dispositius Bluetooth bloquen per defecte les connexions, o necessiten que modifiqueu algun paràmetre de configuració perquè es puguin efectuar. Assegureu-vos que el dispositiu està configurat de tal manera que les permeti.</p>
    </item>
    <item>
      <title>No s'ha reconegut el maquinari Bluetooth</title>
      <p>És possible que l'ordinador no hagi reconegut el vostre adaptador Bluetooth o el dispositiu. Això pot passar perquè els <link xref="hardware-driver">controladors</link> per l'adaptador no s'hagin instal·lat. Alguns adaptadors Bluetooth no són compatibles amb Linux, de manera que és possible que no pugueu aconseguir els controladors adequats. En aquest cas, és probable que haureu d'obtenir un adaptador Bluetooth diferent.</p>
    </item>
    <item>
      <title>L'adaptador no està activat</title>
        <p>Assegureu-vos que l'adaptador de Bluetooth estigui activat. Obriu el quadre de Bluetooth i comproveu que no estigui <link xref="bluetooth-turn-on-off">desactivat</link>.</p>
    </item>
    <item>
      <title>La connexió del dispositiu Bluetooth està desactivada</title>
      <p>Comproveu que el Bluetooth estigui activat al dispositiu al qual us intenteu connectar, i que està <link xref="bluetooth-visibility">detectable o visible</link>. Per exemple, si intenteu connectar-vos a un telèfon, assegureu-vos que no estigui en mode d'avió.</p>
    </item>
    <item>
      <title>No s'ha trobat cap adaptador Bluetooth al vostre ordinador</title>
      <p>Molts ordinadors no tenen adaptadors Bluetooth. Podeu comprar un adaptador si voleu utilitzar Bluetooth.</p>
    </item>
  </terms>

</page>
