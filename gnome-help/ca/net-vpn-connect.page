<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="ca">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Configureu una connexió VPN a una xarxa local a través d'Internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Connectar a una VPN</title>

<p>Una VPN (o <em>Xarxa Virtual Privada</em>) és una forma de connectar-se a una xarxa local a través d'Internet. Per exemple, diguem que voleu connectar-vos a la xarxa local al lloc de treball mentre feu un viatge de negocis. Trobeu una connexió a Internet en algun lloc (com en un hotel) i després connecteu-vos a la vostra VPN del vostre lloc de treball. Seria com si estiguéssiu directament connectat a la xarxa en el treball, però la connexió de xarxa real seria a través de la connexió a Internet de l'hotel. Aquestes connexions usualment estan <em>xifrades</em> per a evitar que la gent accedeixi a la xarxa local a la qual us esteu connectant sense iniciar sessió.</p>

<p>Hi ha diversos tipus de VPN. És possible que hàgiu d'instal·lar un programari addicional en funció del tipus de VPN a on us connecteu. Coneixeu els detalls de la connexió de qui estigui a càrrec de la VPN i consulteu quin <em>client VPN</em> necessiteu utilitzar. A continuació, aneu a l'aplicació d'instal·lació de programari i cerqueu el paquet <app>NetworkManager</app> que funciona amb la vostra VPN (si existeix) i instal·leu-lo.</p>

<note>
 <p>Si no hi ha cap paquet NetworkManager per al vostre tipus de VPN, probablement haureu de descarregar i instal·lar algun programari de client de la companyia que proporciona la VPN. Probablement haureu de seguir unes instruccions diferents per a aconseguir que funcioni.</p>
</note>

<p>Per a configurar la connexió VPN:</p>

  <steps>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Xarxa</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Xarxa</gui> per a obrir el quadre.</p>
    </item>
    <item>
      <p>A la part inferior de la llista de l'esquerra, cliqueu al botó <gui>+</gui> per a afegir una nova connexió.</p>
    </item>
    <item>
      <p>Trieu <gui>VPN</gui> a la llista d'interfícies.</p>
    </item>
    <item>
      <p>Trieu quin tipus de connexió VPN té.</p>
    </item>
    <item>
      <p>Ompliu els detalls de la connexió VPN i, a continuació, quan hàgiu acabat, premeu <gui>Afegeix</gui>.</p>
    </item>
    <item>
      <p>Quan hàgiu acabat de configurar la VPN, obriu el <gui xref="shell-introduction#yourname">menú del sistema</gui> des del costat dret de la barra superior, cliqueu a <gui>VPN OFF</gui> i seleccioneu <gui>Connecta</gui>. Haureu d'introduir una contrasenya per a la connexió abans d'establir-la. Un cop realitzada la connexió, veureu una icona amb forma de cadenat a la barra superior.</p>
    </item>
    <item>
      <p>Esperem que us connecteu correctament a la VPN. En cas contrari, és possible que hàgiu de marcar dues vegades la configuració de VPN que heu introduït. Podeu fer això des del quadre <gui>Xarxa</gui> que heu utilitzat per a crear la connexió. Seleccioneu la connexió VPN de la llista i, a continuació, premeu el botó <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configuració</span></media> per a revisar la configuració.</p>
    </item>
    <item>
      <p>Per a desconnectar-se de la VPN, cliqueu al menú del sistema a la barra superior i cliqueu a <gui>Apaga</gui> sota el nom de la vostra connexió VPN.</p>
    </item>
  </steps>

</page>
