<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="ca">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aquests són fitxers de còpia de seguretat. Estan amagats per defecte.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Què significa un fitxer amb a <file>~</file> al final del nom?</title>

  <p>Els fitxers amb un <file>~</file> al final del nom (per exemple, <file>exemple.txt~</file>) es creen automàticament com a còpies de seguretat dels documents editats amb l'editor de textos <app>gedit</app> o altres aplicacions. És segur eliminar-los, però no hi ha cap problema per a deixar-los a l'ordinador.</p>

  <p>Aquests fitxers estan amagats de manera predeterminada. Si els veieu, és perquè ja heu seleccionat <gui>Mostra els fitxers ocults</gui> (a les opcions de vista de la barra d'eines <app>Fitxers</app> o premut <keyseq><key>Ctrl</key><key>H</key></keyseq>. Podeu amagar-los de nou repetint aquests passos.</p>

  <p>Aquests fitxers es tracten de la mateixa manera que els fitxers ocults normals. Consultar <link xref="files-hidden"/> per a obtenir consells sobre com tractar fitxers ocults.</p>

</page>
