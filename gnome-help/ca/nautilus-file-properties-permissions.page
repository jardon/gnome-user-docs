<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="ca">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>Controleu qui pot veure i editar els fitxers i carpetes.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Establir els permisos pel fitxer</title>

  <p>Podeu utilitzar els permisos de fitxer per a controlar qui pot veure i editar fitxers de la vostra propietat. Per a visualitzar i establir els permisos d'un fitxer, cliqueu amb el botó dret i seleccioneu <gui>Propietats</gui>, aleshores seleccioneu la pestanya <gui>Permisos</gui>.</p>

  <p>Veure <link xref="#files"/> i <link xref="#folders"/> a continuació es detallen els tipus de permisos que podeu establir.</p>

  <section id="files">
    <title>Fitxers</title>

    <p>Podeu establir els permisos per al propietari del fitxer, el propietari del grup i tots els altres usuaris del sistema. Per als vostres fitxers, sou el propietari, i podeu donar-vos permís de només lectura o lectura i escriptura. Establiu un fitxer a només lectura si no voleu canviar-lo accidentalment.</p>

    <p>Tot usuari de l'ordinador pertany a un grup. En els ordinadors domèstics és comú que cada usuari tingui el seu propi grup, i els permisos de grup no s'utilitzen sovint. En entorns corporatius, els grups s'utilitzen a vegades per a departaments o projectes. A més de tenir un propietari, cada fitxer pertany a un grup. Es pot configurar el grup del fitxer i controlar els permisos per a tots els usuaris d'aquest grup. Només es pot configurar el grup del fitxer a un grup al qual pertanyeu.</p>

    <p>També podeu establir els permisos per als usuaris que no siguin el propietari i els del grup del fitxer.</p>

    <p>Si el fitxer és un programa, com un script, heu de seleccionar <gui>Permet executar el fitxer com a programa</gui> per a executar-lo. Fins i tot amb aquesta opció seleccionada, el gestor de fitxers encara obrirà el fitxer en una aplicació. Vegeu <link xref="nautilus-behavior#executable"/> per a més informació.</p>
  </section>

  <section id="folders">
    <title>Carpetes</title>
    <p>Podeu establir permisos a les carpetes pel propietari, el grup i altres usuaris. Consulteu a dalt els detalls dels permisos de fitxers per a obtenir una explicació dels propietaris, grups i altres usuaris.</p>
    <p>Els permisos que podeu establir per a una carpeta són diferents dels que podeu establir per a un fitxer.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">Cap</gui></title>
        <p>L'usuari ni tan sols podrà veure quins fitxers hi ha a la carpeta.</p>
      </item>
      <item>
        <title><gui>Mostra només fitxers</gui></title>
        <p>L'usuari podrà veure quins fitxers es troben a la carpeta, però no podrà obrir-los, crear-ne o eliminar-los.</p>
      </item>
      <item>
        <title><gui>Accedir als fitxers</gui></title>
        <p>L'usuari podrà obrir fitxers a la carpeta (sempre que tingui permís per a fer-ho en el fitxer en particular), però no podrà crear fitxers nous o eliminar-ne.</p>
      </item>
      <item>
        <title><gui>Crear i eliminar fitxers</gui></title>
        <p>L'usuari tindrà accés complet a la carpeta, inclosa l'obertura, creació i eliminació de fitxers.</p>
      </item>
    </terms>

    <p>També podeu establir ràpidament els permisos de fitxers de tots els fitxers de la carpeta clicant a <gui>Canvia els permisos als fitxers inclosos</gui>. Utilitzeu les llistes desplegables per a ajustar els permisos dels fitxers o carpetes continguts i cliqueu a <gui>Canvia</gui>. Els permisos també s'apliquen als fitxers i carpetes en subcarpetes, a qualsevol profunditat.</p>
  </section>

</page>
