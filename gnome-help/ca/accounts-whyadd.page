<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="accounts-whyadd" xml:lang="ca">

  <info>
    <link type="guide" xref="accounts" group="#first"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-02-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email>ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Per què afegir el vostre compte de correu electrònic o de xarxes socials a l'escriptori?</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Per què afegir un compte?</title>

  <p>Afegir els vostres comptes en línia, us ofereix serveis com ara el calendari, xat i correu electrònic directament al vostre escriptori, i fa que la informació dels serveis sigui transparent a la vostra experiència com a usuaris. Si afegiu comptes, podeu mantenir-vos al mateix temps fàcilment en contacte amb serveis de diferents comptes, com ara xats. Tan sols configureu els vostres comptes en línia una vegada i cada vegada que inicieu l'ordinador, tots els comptes i serveis que hàgiu afegit estaran preparats per a utilitzar-se.</p>

  <p>Veure <link xref="accounts-which-application"/> per a obtenir informació sobre quines aplicacions poden accedir a quins serveis en línia.</p>
</page>
