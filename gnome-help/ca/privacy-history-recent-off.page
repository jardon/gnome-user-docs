<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="ca">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Atureu o limiteu el vostre ordinador de fer un seguiment dels fitxers utilitzats recentment.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar o limitar el seguiment de l'historial de fitxers</title>
  
  <p>El seguiment dels fitxers i carpetes usats recentment fa que sigui més fàcil trobar els elements amb els quals s'ha estat treballant en el gestor de fitxers i als diàlegs de fitxers a les aplicacions. És possible que vulgueu mantenir l'historial d'ús dels vostres fitxers de forma privada, o només fer un seguiment de l'historial més recent.</p>

  <steps>
    <title>Desactivar el seguiment de l'historial de fitxers</title>
    <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Privacitat</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Historial dels fitxers i paperera</gui> per a obrir el quadre.</p>
    </item>
    <item>
     <p>Canvieu a OFF l'<gui>Historial dels fitxers</gui>.</p>
     <p>Per a tornar a activar aquesta característica, canvieu l'<gui>Historial dels fitxers</gui> a actiu.</p>
    </item>
    <item>
      <p>Utilitzeu el botó <gui>Neteja l'historial…</gui> per a purgar l'historial immediatament.</p>
    </item>
  </steps>
  
  <note><p>Aquesta configuració no afectarà la manera com el vostre navegador emmagatzema informació sobre els llocs web que visiteu.</p></note>

  <steps>
    <title>Restringiu el temps durant el qual es fa seguiment del vostre historial de fitxers</title>
    <item>
      <p>Obriu la vista general <gui xref="shell-introduction#activities">Activitats</gui> i comenceu a escriure <gui>Historial de fitxers i paperera</gui>.</p>
    </item>
    <item>
      <p>Cliqueu a <gui>Historial dels fitxers i paperera</gui> per a obrir el quadre.</p>
    </item>
    <item>
     <p>Assegureu-vos que l'interruptor <gui>Historial dels fitxers</gui> estigui activat.</p>
    </item>
    <item>
     <p>A <gui>Durada de l'historial de fitxers</gui>, seleccioneu quant de temps voleu conservar l'historial de fitxers. Trieu entre les opcions <gui>1 dia</gui>, <gui>7 dies</gui>, <gui>30 dies</gui>, o <gui>Per sempre</gui>.</p>
    </item>
    <item>
      <p>Utilitzeu el botó <gui>Neteja l'historial…</gui> per a purgar l'historial immediatament.</p>
    </item>
  </steps>

</page>
