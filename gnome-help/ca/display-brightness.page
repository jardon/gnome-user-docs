<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="ca">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>
    <revision version="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Canvieu la brillantor de la pantalla per a fer-la més llegible amb llum brillant.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Establir la brillantor de la pantalla</title>

  <p>Depenent del maquinari, podeu canviar la brillantor de la pantalla per a estalviar energia o fer que la pantalla sigui més llegible amb llum brillant.</p>

  <p>Per a canviar la brillantor de la pantalla, cliqueu al <gui xref="shell-introduction#systemmenu">menú del sistema</gui> al costat dret de la barra superior i ajusteu el control lliscant de la brillantor al valor que vulgueu utilitzar. El canvi té efecte immediatament.</p>

  <note style="tip">
    <p>Molts teclats portàtils tenen tecles especials per a ajustar la brillantor. Sovint tenen una imatge que sembla el sol. Mantingueu premuda la tecla <key>Fn</key> per a utilitzar aquestes tecles.</p>
  </note>

  <note style="tip">
    <p>Si l'ordinador compta amb un sensor de llum integrat, la brillantor de la pantalla s'ajustarà automàticament. Per a més informació, vegeu <link xref="power-autobrightness"/>.</p>
  </note>

  <p>Si és possible configurar la brillantor de la vostra pantalla, també podeu enfosquir la pantalla de forma automàtica per a estalviar energia. Per a obtenir més informació, consulteu <link xref="power-whydim"/>.</p>

</page>
