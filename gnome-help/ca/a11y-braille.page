<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task a11y" version="1.0 if/1.0" id="a11y-braille" xml:lang="ca">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>
    <link type="seealso" xref="a11y-screen-reader"/>

    <revision pkgversion="3.9.92" date="2013-09-18" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Com utilitzar el lector de pantalla <app>Orca</app> amb una pantalla braille actualitzable.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Llegir la pantalla en braille</title>

  <p>El lector de pantalla <app>Orca</app> pot mostrar la interfície d'usuari en una pantalla braille actualitzable. Depenent de la instal·lació del sistema, és possible que l'Orca no estigui instal·lat. Si és així, instal·leu-lo primer.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Instal·la Orca</link></p>

  <p>Consulteu l'<link href="help:orca">ajuda d'Orca</link> per a més informació.</p>

</page>
