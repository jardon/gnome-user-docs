<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-notspecifiededid" xml:lang="ca">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="guide" xref="color-gettingprofiles"/>
    <link type="guide" xref="color-why-calibrate"/>
    <desc>Els perfils predeterminats pel monitor per defecte no tenen una data de calibratge.</desc>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Per què els perfils del monitor per defecte no tenen una data de caducitat del calibratge?</title>
  <p>El perfil de color predeterminat usat per a cada monitor es genera automàticament en funció de la pantalla<link href="https://en.wikipedia.org/wiki/Extended_Display_Identification_Data"> EDID</link> que s'emmagatzema en un xip de memòria dins del monitor. L'EDID només ens proporciona una instantània dels colors disponibles que era capaç de mostrar quan es va fabricar, i no conté massa informació addicional per a la correcció del color.</p>

  <note style="tip">
    <p>Obtenir un perfil del proveïdor del monitor o crear un perfil propi portaria a una correcció de color més precisa.</p>
  </note>

</page>
