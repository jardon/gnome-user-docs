<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="ca">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Els perfils de color els proporcionen els proveïdors i els podeu generar vosaltres mateixos.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>On puc obtenir perfils de color?</title>

  <p>La millor manera d'obtenir perfils és generar-los un mateix, encara que això requereixi una despesa inicial.</p>
  <p>Molts fabricants intenten proporcionar perfils de color per a dispositius, tot i que de vegades es troben dins de <em>paquets de controladors</em> que haureu de descarregar, extreure i, a continuació, cercar els perfils de color.</p>
  <p>Alguns fabricants no proporcionen perfils precisos per al maquinari i és millor evitar els seus perfils. Una bona pista és descarregar-se el perfil i si la data de creació és més d'un any anterior a la data en què vau comprar el dispositiu, és probable que s'hagi generat amb dades simulades inútils.</p>

  <p>Veure <link xref="color-why-calibrate"/> per a obtenir informació sobre per què els perfils subministrats per proveïdors solen ser pitjor que inútils.</p>

</page>
