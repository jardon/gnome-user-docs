<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="ca">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Escriviu caràcters que no es trobin al teclat, inclosos alfabets estrangers, els símbols matemàtics, emojis i dibuixos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Introduir caràcters especials</title>

  <p>Podeu introduir i veure milers de caràcters de la majoria dels sistemes d'escriptura del món, fins i tot aquells que no es troben al vostre teclat. Aquesta pàgina mostra algunes maneres diferents d'introduir caràcters especials.</p>

  <links type="section">
    <title>Mètodes per a introduir caràcters</title>
  </links>

  <section id="characters">
    <title>Caràcters</title>
    <p>L'aplicació de mapes de caràcters us permet trobar i inserir caràcters poc freqüents, emoji inclosos, navegant per categories de caràcters o cercant paraules clau.</p>

    <p>Podeu iniciar <app>Caràcters</app> des de la vista general d'Activitats.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Inserir un emoji</title>
      <item>
        <p>Premeu <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Exploreu les categories a la part inferior del diàleg o comenceu a escriure una descripció al camp de cerca.</p>
      </item>
      <item>
        <p>Seleccioneu un emoji a inserir.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Tecla de composició</title>
    <p>Una tecla de composició és una tecla especial que us permet prémer diverses tecles seguides per a obtenir un caràcter especial. Per exemple, per a escriure la lletra accentuada <em>é</em>, podeu prémer <key>composició</key> aleshores <key>'</key> i aleshores <key>e</key>.</p>
    <p>Alguns teclats no tenen tecles de composició específiques. Al seu lloc, podeu definir una de les tecles existents al teclat com a tecla de composició.</p>

    <steps>
      <title>Definir una tecla de composició</title>
      <item>
      <p>Obriu la vista general d'<gui xref="shell-introduction#activities">Activitats</gui> i comenceu a teclejar <gui>Paràmetres</gui>.</p>
      </item>
      <item>
        <p>Cliqueu a <gui>Paràmetres</gui>.</p>
      </item>
      <item>
        <p>Cliqueu a <gui>Teclat</gui> a la barra lateral per a obrir el quadre.</p>
      </item>
      <item>
        <p>A la secció <gui>Escriu caràcters especials</gui>, cliqueu a <gui>Tecla de composició</gui>.</p>
      </item>
      <item>
        <p>Activeu la <gui>Tecla de composició</gui>.</p>
      </item>
      <item>
        <p>Marqueu la casella de selecció de la tecla que voleu establir com a tecla de composició.</p>
      </item>
      <item>
        <p>Tanqueu el diàleg.</p>
      </item>
    </steps>

    <p>Podeu escriure molts caràcters comuns utilitzant la tecla de composició, per exemple:</p>

    <list>
      <item><p>Premeu <key>composició</key> aleshores <key>'</key> llavors la lletra on posar-hi l'accent tancat, com ara <em>é</em>.</p></item>
      <item><p>Premeu <key>composició</key> aleshores <key>`</key> (accent endarrere) i una lletra on col·locar-hi l'accent greu, com ara <em>è</em>.</p></item>
      <item><p>Premeu <key>composició</key> aleshores <key>'</key> la lletra on posar-hi dièresi, com ara <em>ü</em>.</p></item>
      <item><p>Premeu <key>composició</key> aleshores <key>'</key> la lletra on posar-hi l'accent circumflex, com ara <em>ê</em>.</p></item>
    </list>
    <p>Per a més seqüències de tecles de composició, vegeu <link href="https://en.wikipedia.org/wiki/Composekey#Commoncomposecombinations">la pàgina de la tecla de composició a la Viquipèdia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Punts de codi</title>

  <p>Podeu introduir qualsevol caràcter Unicode utilitzant només el vostre teclat amb el punt de codi numèric del caràcter. Cada caràcter s'identifica amb un punt de codi de quatre caràcters. Per a trobar el punt de codi d'un caràcter, busqueu-lo a l'aplicació <app>Characters</app>. El punt del codi són els darrers quatre caràcters <gui>U+</gui>.</p>

  <p>Per a introduir un caràcter pel seu punt de codi, premeu <keyseq><key>Ctrl</key><key>Maj</key><key>U</key></keyseq>, escriviu el codi de quatre caràcters i premeu <key>Espai</key> o <key>Retorn</key>. Si utilitzeu sovint caràcters als quals no es pot accedir fàcilment per altres mètodes, pot resultar-vos útil memoritzar el punt de codi d'aquests caràcters per a poder introduir-los ràpidament.</p>

</section>

  <section id="layout">
    <title>Distribucions de teclat</title>
    <p>Podeu fer que el vostre teclat es comporti com el teclat d'un altre idioma, independentment de les lletres impreses a les tecles. Fins i tot podeu canviar fàcilment entre diferents dissenys de teclat usant una icona a la barra superior. Per a saber com, vegeu <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Mètodes d'entrada</title>

  <p>Un mètode d'entrada amplia els mètodes anteriors permetent introduir caràcters no només amb el teclat, sinó també amb qualsevol dispositiu d'entrada. Per exemple, podeu introduir caràcters amb un ratolí mitjançant un mètode gestual o introduir caràcters japonesos utilitzant un teclat llatí.</p>

  <p>Per a triar un mètode d'entrada, cliqueu amb el botó dret sobre un giny de text i al menú <gui>Mètode d'entrada</gui>, trieu el mètode d'entrada que voleu utilitzar. No es proporciona cap mètode d'entrada predeterminat, així que consulteu-ne la documentació per a veure com utilitzar-los.</p>

</section>

</page>
