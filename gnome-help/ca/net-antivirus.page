<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-antivirus" xml:lang="ca">

  <info>
    <link type="guide" xref="net-security"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projecte de documentació del GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hi ha pocs virus de Linux, de manera que probablement no necessiteu programari antivirus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jaume Jorba</mal:name>
      <mal:email>jaume.jorba@gmail.com</mal:email>
      <mal:years>2018, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jordi Mas i Hernàndez</mal:name>
      <mal:email>jmas@softcatala.org</mal:email>
      <mal:years>2018, 2020-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miquel-Àngel Burgos i Fradeja</mal:name>
      <mal:email>miquel.angel.burgos@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Maite Guix i Ribé</mal:name>
      <mal:email>maite.guix@me.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Necessito un antivirus?</title>

  <p>Si esteu acostumat a Windows o a Mac OS, probablement també esteu acostumats a tenir programari antivirus que funcioni en segon pla. El programari antivirus s'executa en segon pla, comprovant constantment si hi ha virus informàtics que puguin trobar una via d'entrada a l'ordinador i causar problemes.</p>

  <p>El programari antivirus existeix per a Linux, però probablement no el necessitareu. Els virus que afecten Linux encara són molt rars. Alguns argumenten que això és perquè Linux no és tan àmpliament utilitzat com altres sistemes operatius, de manera que ningú hi escriu virus. Altres argumenten que Linux és intrínsecament més segur, i que els problemes de seguretat que els virus poden utilitzar, se solucionen molt ràpidament.</p>

  <p>Qualsevol sigui el motiu, els virus de Linux són tan rars que, en aquests moments, no necessiteu preocupar-vos-en.</p>

  <p>Si voleu estar encara més segur, o si voleu cercar virus en fitxers que us esteu passant amb usuaris de Windows i Mac OS, podeu instal·lar un programari antivirus. Consulteu l'instal·lador de programari o cerqueu en línia; hi ha diverses aplicacions disponibles.</p>

</page>
