<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="sl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zoom in on your screen so that it is easier to see things.</desc>
  </info>

  <title>Magnify a screen area</title>

  <p>Povečanje zaslona je drugačno kot povečanje <link xref="a11y-font-size">velikosti besedila</link>. Ta zmožnost je kot povečevalno steklo, ki vam omogoča premikanje in približanje delov zaslona.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press on <gui>Zoom</gui> in the <gui>Seeing</gui> section.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Sedaj se lahko premikate po področju zaslona. S premikom svoje miške na rob zaslona boste povečano področje premaknili v različne smeri, kar vam omogoča ogled področja po izbiri.</p>

  <note style="tip">
    <p>Povečavo lahko hitro vklopite in izklopite s klikom na <link xref="a11y-icon">ikono splošnega dostopa</link> v vrhnji vrstici in izbiro <gui>Povečava</gui>.</p>
  </note>

  <p>You can change the magnification factor, the mouse tracking, and the
  position of the magnified view on the screen. Adjust these in the
  <gui>Magnifier</gui> tab of the <gui>Zoom Options</gui> window.</p>

  <p>You can activate crosshairs to help you find the mouse or touchpad
  pointer. Switch them on and adjust their length, color, and thickness in the
  <gui>Crosshairs</gui> tab of the <gui>Zoom</gui> settings window.</p>

  <p>You can switch to inverse video or <gui>White on black</gui>, and adjust
  brightness, contrast and greyscale options for the magnifier. The combination
  of these options is useful for people with low-vision, any degree of
  photophobia, or just for using the computer under adverse lighting
  conditions. Select the <gui>Color Effects</gui> tab in the <gui>Zoom</gui>
  settings window to enable and change these options.</p>

</page>
