<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-select" xml:lang="sl">

  <info>
    <link type="guide" xref="files#faq"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pritisnite <keyseq><key>Ctrl</key><key>S</key></keyseq> za izbiro več datotek s podobnimi imeni.</desc>
  </info>

  <title>Izbor datotek z vzorcem</title>

  <p>Datoteke v mapi lahko izberete z vzorcem imena datotek. Pritisnite <keyseq><key>Ctrl</key><key>S</key></keyseq> za priklic okna <gui>Izbor ujemanja predmetov</gui>. Vpišite vzorec z uporabo skupnih delov imen datotek in nadomestnih znakov. Na voljo sta dva nadomestna znaka:</p>

  <list style="compact">
    <item><p><file>*</file> se ujema s katerikoli številom znakov tudi z nič znaki.</p></item>
    <item><p><file>?</file> se ujema natanko z enim znakom.</p></item>
  </list>

  <p>Na primer:</p>

  <list>
    <item><p>V primeru da imate datoteko OpenDocument Text, datoteko PDF in sliko, ki imajo enako osnovno ime <file>Račun</file>, vse tri izberite z vzorcem</p>
    <example><p><file>Račun.*</file></p></example></item>

    <item><p>V primeru da imate fotografije z imeni <file>Počitnice-001.jpg</file>, <file>Počitnice-002.jpg</file>, <file>Počitnice-003.jpg</file>, lahko vse izberete z vzorcem</p>
    <example><p><file>Počitnice-???.jpg</file></p></example></item>

    <item><p>If you have photos as before, but you have edited some of them and
    added <file>-edited</file> to the end of the file name of the photos you
    have edited, select the edited photos with</p>
    <example><p><file>Počitnice-???-urejeno.jpg</file></p></example></item>
  </list>

</page>
