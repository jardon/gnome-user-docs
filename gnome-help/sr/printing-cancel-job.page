<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="sr">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Џим Кембел</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Јана Сварова</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Откажите заказани посао штампања и уклоните га из реда.</desc>
  </info>

  <title>Откажите, паузирајте или отпустите посао штампања</title>

  <p>Можете да откажете заказани посао штампања и да га уклоните из реда у подешавањима штампача.</p>

  <section id="cancel-print-job">
    <title>Откажите посао штампања</title>

  <p>Ако сте случајно започели штампање документа, можете да откажете штампање тако да не морате да трошите мастило или папир.</p>

  <steps>
    <title>Како да откажете посао штампања:</title>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Штампачи</gui>.</p>
    </item>
    <item>
      <p>Притисните на <gui>Штампачи</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Кликните на дугме <gui>Прикажи послове</gui> на десном крају прозорчета <gui>Штампачи</gui>.</p>
    </item>
    <item>
      <p>Откажите посао штампања притиском на дугме за заустављање.</p>
    </item>
  </steps>

  <p>Ако ово не откаже посао штампања као што сте очекивали, покушајте да држите притиснутим дугме <em>Cancel</em> (Откажи) на вашем штампачу.</p>

  <p>Као последњи покушај, нарочито ако имате повећи посао штампања са много страница које не желите да откажете, уклоните папир из улазне фиоке штампача. Штампач ће закључити да нема више папира и зауставиће штампање. Можете затим поново да покушате да откажете посао штампања, или да угасите штампач и да га упалите поново.</p>

  <note style="warning">
    <p>Будите пажљиви да не оштетите штампач када уклањате папир. Ако будете морали снажно да повучете папир да бисте га уклонили, најбоље ће бити да га оставите тамо где јесте.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Откажите и отпустите посао штампања</title>

  <p>Ако желите да паузирате или да отпустите посао штампања, можете да урадите ово тако што ћете отићи на прозорче послова у подешавањима штампача и притиснути одговарајуће дугме.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Штампачи</gui>.</p>
    </item>
    <item>
      <p>Притисните на <gui>Штампачи</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Притисните дугме <gui>Прикажи послове</gui> на десној страни прозорчета <gui>Штампачи</gui> и паузирајте или отпустите посао штампања у зависности од ваших потреба.</p>
    </item>
  </steps>

  </section>

</page>
