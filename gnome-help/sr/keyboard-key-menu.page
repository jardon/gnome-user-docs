<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-menu" xml:lang="sr">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>
    <link type="seealso" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="new"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Хуанхо Марин</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Тастер <key>Изборника</key> покреће приручни изборник тастатуром радије него ли десним кликом.</desc>
  </info>

  <title>Шта је то тастер <key>Изборник</key>?</title>

  <p>Тастер <key>Изборник</key>, такође познат као тастер <em>Програма</em>, је тастер који се налази неким тастатурама окренутих Виндоузу. Овај тастер се на обично налази доле десно на тастатури, поред тастера <key>Ктрл</key>, али га произвођачи тастатура могу поставити и на другим местима. На њему је обично нацртан курсор који лебди изнад изборника: <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-menu.svg">
  <key>Menu</key> key icon</media>.</p>

  <p>Првенствена функција овог тастера је покретање приручног изборника тастатуром уместо на клик десним тастером миша: ово је корисно ако није доступан миш или сличан уређај, или ако не постоји десни тастер миша.</p>

  <p>Тастер <key>Изборник</key> је понекад изостављен због простора, нарочито на преносивим тастатурама и на тастатурама преносивих рачунара. У том случају, неке тастатуре укључују функцијски тастер <key>Изборник</key> који може бити покренут у комбинацији са функцијским тастером (<key>Fn</key>).</p>

  <p><em>Приручни изборник</em> је изборник који искочи када извршите десни клик. Изборник који видите, ако га има, је зависан од садржаја и функције области на којој сте извршили десни клик. Када користите тастер <key>Изборник</key>, приручни изборник се приказује за област екрана над којом се налази ваш курсор над тачком када је притиснут тастер.</p>

</page>
