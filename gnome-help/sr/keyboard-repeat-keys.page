<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="sr">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Наталија Руз Лејва</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Јулита Инка</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шоба Тјаги</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Учините да тастатура не понавља слова када држите притиснутим тастер, или измените застој и брзину понављања тастера.</desc>
  </info>

  <title>Управљајте поновљеним притисцима тастера</title>

  <p>По основи, када држите притиснут тастер на тастатури, слово или симбол ће бити понављано све док не отпустите тастер. Ако имате потешкоћа да довољно брзо повучете ваш прст назад, можете да искључите ову функцију, или да измените након колико времена или колико брзо ће се поновити притисак на тастер.</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Подешавања</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Подешавања</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Приступачност</gui> у бочној траци да отворите панел.</p>
    </item>
    <item>
     <p>Притисните <gui>Понављање тастера</gui> у одељку <gui>Куцање</gui>.</p>
    </item>
    <item>
      <p>Пребаците прекидач <gui>Понављање тастера</gui> на искључено.</p>
      <p>Другачије, дотерајте клизач <gui>Застој</gui> да одредите колико дуго ћете морати да држите притиснут тастер да би започело понављање, и дотерајте клизач <gui>Брзина</gui> да одредите колико брзо ће се понављати притисци тастера.</p>
    </item>
  </steps>

</page>
