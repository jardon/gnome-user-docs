<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="sr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Џим Кембел</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">Тражите</title>
    <desc>Пронађите датотеке на основу назива и врсте.</desc>
  </info>

  <title>Тражите датотеке</title>

  <p>Можете да тражите датотеке на основу њиховог назива или према врсти управно из управника датотека.</p>

  <links type="topic" style="linklist">
    <title>Други програми за претрагу</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>Тражите</title>
    <item>
      <p>Отворите програм <app>Датотеке</app> из прегледа <gui xref="shell-introduction#activities">активности</gui>.</p>
    </item>
    <item>
      <p>Ако знате да су датотеке које желите под одређеном фасциклом, идите до те фасцикле.</p>
    </item>
    <item>
      <p>Упишите реч или речи за које знате да се јављају у називу датотеке, и оне ће бити приказане у пољу претраге. На пример, ако сте све ваше фактуре именовали речју „Фактура“, упишите <input>фактура</input>. Речи су упоређене без обзира на величину слова.</p>
      <note>
        <p>Уместо да речи куцате непосредно да бисте призвали поље за претрагу, можете да кликнете <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media> на траци алата, или притисните <keyseq><key>Ктрл </key><key>Ф</key></keyseq>.</p>
      </note>
    </item>
    <item>
      <p>Можете да смањите ваше резултате према датуму, врсти датотеке, и према времену да тражите читав текст датотеке, или да тражите само називе датотека.</p>
      <p>Да примените филтере, изаберите дугме падајућег изборника на левој страни <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media> иконице управника датотека, и изаберите из доступних филтера:</p>
      <list>
        <item>
          <p><gui>Када</gui>: Колико уназад желите да тражите?</p>
        </item>
        <item>
          <p><gui>Шта</gui>: Шта је врста ставке?</p>
        </item>
        <item>
          <p>Да ли треба ваша претрага да укључи претрагу читавог текста, или да претражује само називе датотека?</p>
        </item>
      </list>
    </item>
    <item>
      <p>Да уклоните филтер, изаберите <gui>X</gui> поред ознаке филтера који желите да уклоните.</p>
    </item>
    <item>
      <p>Можете да отворите, умножите, обришете, или да урадите било шта друго са вашим датотекама из резултата претраге, баш као што бисте урадили из било које фасцикле у управнику датотека.</p>
    </item>
    <item>
      <p>Поново кликните на <media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media> на траци алата да напустите претрагу и да се вратите у фасциклу.</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>Прилагодите претрагу датотека</title>

<p>Можете пожелети да одређени директоријуми буду укључени или искључени из претраживања у програму <app>Датотеке</app>. Да прилагодите који директоријуми се претражују:</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Претрага</gui>.</p>
    </item>
    <item>
      <p>Изаберите <guiseq><gui>Поставке</gui><gui>Претрага</gui></guiseq> из резултата. Ово ће отворити панел <gui>Поставке претраге</gui>.</p>
    </item>
    <item>
      <p>Кликните на дугме <gui>Места претраге</gui> у траци заглавља.</p>
    </item>
  </steps>

<p>Ово ће отворити засебан панел поставки који вам омогућава да укључите или искључите претраге директоријума. Можете да окинете претраге на сваком од три језичка:</p>

  <list>
    <item>
      <p><gui>Места</gui>: Исписује општа места личне фасцикле</p>
    </item>
    <item>
      <p><gui>Обележивачи</gui>: Исписује места фасцикли које сте обележили у програму <app>Датотеке</app></p>
    </item>
    <item>
      <p><gui>Остало</gui>: Исписује места фасцикли кооје сте укључили дугметом <gui>+</gui>.</p>
    </item>
  </list>

</section>

</page>
