<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-turn-on-off" xml:lang="sr">

  <info>
    <link type="guide" xref="bluetooth" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-21" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Џим Кембел</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Укључите или искључите блутут уређај на вашем рачунару.</desc>
  </info>

<title>Укључите или искључите блутут</title>

  <p>Можете да укључите блутут да се повежете на друге блутут уређаје, или да га искључите да сачувате енергију. Да укључите блутут:</p>

  <steps>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Блутут</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Блутут</gui> да отворите панел.</p>
    </item>
    <item>
      <p>Пребаците прекидач на врху на укључено.</p>
    </item>
  </steps>

  <p>Многи преносни рачунари имају физички прекидач или комбинацију тастера за укључивање и искључивање блутута. Потражите прекидач на вашем рачунару или тастер на тастатури. Тастеру на тастатури се обично приступа уз помоћ <key>Fn</key> тастера.</p>

  <p>Да искључите блутут:</p>
  <steps>
    <item>
      <p>Отворите <gui xref="shell-introduction#systemmenu">изборник система</gui> на десној страни горње траке.</p>
    </item>
    <item>
      <p>Изаберите <gui><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/> Блутут</gui>.</p>
    </item>
  </steps>

  <note><p>Ваш рачунар је <link xref="bluetooth-visibility">видљив</link> све док је отворен панел <gui>блутута</gui>.</p></note>

</page>
