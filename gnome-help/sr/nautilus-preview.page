<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="sr">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дејвид Кинг</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Одредите када ће се користити минијатуре за датотеке.</desc>
  </info>

<title>Поставке прегледа управника датотека</title>

<p>Управник датотека ствара минијатуре за преглед слика, симака, и текстуалних датотека. Минијатурни прегледи могу бити спори за велике датотеке или путем мреже, тако да можете одредити када ће бити коришћени претпрегледи. Кликните на дугме изборника у горњем десном углу прозора, изаберите <gui>Поставке</gui> а затим идите на одељак <gui>Учинковитост</gui>.</p>

<terms>
  <item>
    <title><gui>Приказ сличица</gui></title>
    <p>По основи, сви прегледи се приказују за <gui>Само на овом рачунару</gui>, на вашем рачунару или на прикљученим спољним уређајима. Можете да поставите ову функцију на <gui>Све датотеке</gui> или <gui>Никада</gui>. Управник датотека може да <link xref="nautilus-connect">разгледа датотеке на другим рачунарима</link> преко локалне мреже или на интернету. Ако често разгледате датотеке на локалној мрежи, а мрежа има велики пропусни опсег, можда ћете пожелети да подесите опцију прегледа на <gui>Све датотеке</gui>.</p>
  </item>
  <item>
    <title><gui>Број датотека у фасциклама</gui></title>
    <p>Ако прикажете величине датотека у <link xref="nautilus-list">колонама прегледа списком</link> или у <link xref="nautilus-display#icon-captions">натписима иконица</link>, фасцикле ће бити приказане са бројем садржаних датотека и фасцикли. Бројање ставки у фасцикли може бити споро, нарочито за велике фасцикле, или преко мреже.</p>
    <p>Можете да укључите или да искључите ову могућност, или да је укључите само за датотеке на вашем рачунару и локалним спољним уређајима.</p>
  </item>
</terms>
</page>
