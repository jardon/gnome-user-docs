<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-select" xml:lang="sr">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Фил Бул</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Штампајте само одређене странице, или само опсег страница.</desc>
  </info>

  <title>Штампајте само одређене странице</title>

  <p>Да одштампате само одређене странице документа:</p>

  <steps>
    <item>
      <p>Отворите прозорче штампања тако што ћете притиснути <keyseq><key>Ктрл</key><key>П</key></keyseq>.</p>
    </item>
    <item>
      <p>У језичку <gui>Опште</gui>, у прозору штампања изаберите <gui>Странице</gui> у одељку <gui>Опсег</gui>.</p>
    </item>
    <item><p>У поље за текст упишите бројеве страница које желите да одштампате, раздвојених зарезима. Користите цртицу да одредите опсег страница.</p></item>
  </steps>

  <note>
    <p>На пример, ако у поље <gui>Странице</gui> упишете „1,3,5-7“, биће одштампане странице 1,3,5,6 и 7.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
