<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 if/1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="sr">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="seealso" xref="keyboard-key-super"/>

    <revision pkgversion="3.29" date="2018-08-27" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Шон Мек Кенс</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Крећите се по радној површи користећи тастатуру.</desc>
  </info>

<title>Корисне пречице тастатуре</title>

<p>Ова страница обезбеђује преглед пречица тастатуре које могу да вам помогну да успешније користите вашу радну површ и програме. Ако уопште не можете да користите миша или показивачки уређај, погледајте <link xref="keyboard-nav"/> за више података о кретању по корисничком сучељу само тастатуром.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Кретање по радној површи</title>
  <tr xml:id="alt-f1">
    <td><p><keyseq><key>Алт</key><key>Ф1</key></keyseq> или</p>
      <p>тастер <key xref="keyboard-key-super">Супер</key></p></td>
    <td><p>Пребацујте се између прегледа <gui>Активности</gui> и радне површи. У прегледу, почните да куцате да одмах потражите ваше програме, контакте и документа.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Алт</key><key>Ф2</key></keyseq></p></td>
    <td><p>Прикажите облачић прозора (за брзо покретање наредби).</p>
    <p>ористите тастере стрелица да брзо приступите претходно извршеним наредбама.</p></td>
  </tr>
  <tr xml:id="super-tab">
    <td><p><keyseq><key>Супер</key><key>Таб</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Брзо се пребацујте између прозора</link>. Држите притиснутим <key>Шифт</key> за преокренути поредак.</p></td>
  </tr>
  <tr xml:id="super-tick">
    <td><p><keyseq><key>Супер</key><key>`</key></keyseq></p></td>
    <td>
      <p>Пребацујте се између прозора из истог програма, или из изабраног програма после <keyseq><key>Супер</key><key>Таб</key></keyseq>.</p>
      <p>Ова пречица користи <key>`</key> на САД тастатурама, на којима се тастер <key>`</key> налази изнад тастера <key>Таб</key>. На свим осталим тастатурама, пречица је <key>Супер</key> плус тастер изнад тастера <key>Таб</key>.</p>
    </td>
  </tr>
  <tr xml:id="alt-escape">
    <td><p><keyseq><key>Алт</key><key>Изађи</key></keyseq></p></td>
    <td>
      <p>Пребацујте се између прозора у тренутном радном простоу. Држите притиснутим <key>Шифт</key> за преокренути поредак.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-tab">
    <!-- To be updated to <key>Tab</key> in the future. -->
    <td><p><keyseq><key>Ктрл</key><key>Алт</key><key>Таб</key></keyseq></p></td>
    <td>
      <p>Поставља први план тастатуре на горњу траку. У прегледу <gui>Активности</gui>, пребацује први план тастатуре између горње траке, полетника, прегледа прозора, списка програма, и поља за претрагу. Користите тастере стрелица за кретање.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-alt-t">
    <td><p><keyseq><key>Ктрл</key><key>Алт</key><key>Т</key></keyseq></p></td>
    <td>
      <p>Отворите Терминал.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-t">
    <td><p><keyseq><key>Ктрл</key><key>Шифт</key><key>T</key></keyseq></p></td>
    <td>
      <p>Отворите нови језичак терминала у истом прозору.</p>
    </td>
  </tr>
  <tr xml:id="ctrl-shift-n">
    <td><p><keyseq><key>Ктрл</key><key>Шифт</key><key>N</key></keyseq></p></td>
    <td>
      <p>Отворите нови прозор терминала. Да користите ову пречицу, треба да сте већ у прозору терминала.</p>
    </td>
  </tr>
  <tr xml:id="super-a">
    <td><p><keyseq><key>Супер</key><key>А</key></keyseq></p></td>
    <td><p>Прикажите списак програма.</p></td>
  </tr>
  <tr xml:id="super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Супер</key><key>Страница горе</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ктрл</key><key>Алт</key><key>→</key></keyseq></p>
      <p>и</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Супер</key><key>Страница доле</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Ктрл</key><key>Алт</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-switch">Пребацујте се између радних простора</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-updown">
    <td>
      <p if:test="!platform:gnome-classic"><keyseq><key>Шифт</key><key>Супер</key><key>Страница доле</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Шифт</key><key>Ктрл</key><key>Алт</key><key>→</key></keyseq></p>
      <p>и</p>
      <p if:test="!platform:gnome-classic"><keyseq><key>Шифт</key><key>Супер</key><key>Страница доле</key></keyseq></p>
      <p if:test="platform:gnome-classic"><keyseq><key>Шифт</key><key>Ктрл</key><key>Алт</key><key>←</key></keyseq></p>
    </td>
    <td><p><link xref="shell-workspaces-movewindow">Преместите тренутни прозор на други радни простор</link>.</p></td>
  </tr>
  <tr xml:id="shift-super-left">
    <td><p><keyseq><key>Шифт</key><key>Супер</key><key>←</key></keyseq></p></td>
    <td><p>Премешта текући прозор један монитор на лево.</p></td>
  </tr>
  <tr xml:id="shift-super-right">
    <td><p><keyseq><key>Шифт</key><key>Супер</key><key>→</key></keyseq></p></td>
    <td><p>Премешта текући прозор један монитор на десно.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ктрл</key><key>Алт</key><key>Обриши</key></keyseq></p></td>
    <td><p><link xref="shell-exit#logout">Прикажите прозорче гашења</link>.</p></td>
  </tr>
  <tr xml:id="super-l">
    <td><p><keyseq><key>Супер</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Закључајте екран.</link></p></td>
  </tr>
  <tr xml:id="super-v">
    <td><p><keyseq><key>Супер</key><key>V</key></keyseq></p></td>
    <td><p>Прикажите <link xref="shell-notifications#notificationlist">списак обавештења</link>. Притисните <keyseq><key>Супер</key><key>V</key></keyseq> поново или <key>Esc</key> да затворите.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Опште пречице за уређивање</title>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>А</key></keyseq></p></td>
    <td><p>Изаберите сав текст или ставке на списку.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>Х</key></keyseq></p></td>
    <td><p>Исеците (уклоните) изабрани текст или ставке и поставите их у оставу.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>C</key></keyseq></p></td>
    <td><p>Умножите изабрани текст или ставке у оставу.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>V</key></keyseq></p></td>
    <td><p>Уметните садржај оставе.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>Z</key></keyseq></p></td>
    <td><p>Опозовите последњу радњу.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>Шифт</key><key>C</key></keyseq></p></td>
    <td><p>Умножите истакнути текст или наредбе у оставу у Терминалу.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ктрл</key><key>Шифт</key><key>V</key></keyseq></p></td>
    <td><p>Уметните садржај оставе у терминал.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Снимање екрана</title>
  <tr>
    <td><p><key>Штампај</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Покрените алат за снимање екрана.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Алт</key><key>Штампај</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Направите слику прозора.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Шифт</key><key>Штампај</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Направите снимак целог екрана.</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Шифт</key><key>Ктрл</key><key>Алт</key><key>R</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screencast">Покрените и зауставите записивање снимка екрана.</link></p></td>
  </tr>
</table>

</page>
