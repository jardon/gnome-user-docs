<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="sr">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Уобичајено је за рачунаре да се загревају, али ако се загреју превише могли би да се прегреју, што би могло да буде штетно.</desc>

    <credit type="author">
      <name>Гномов пројекат документације</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Мој рачунар постаје стварно врућ</title>

<p>Већина рачунара се загреје након неког времена, а неки могу да постану веома врући. То је нормално: то је једноставно начин на који се рачунар хлади. Међутим, ако ваш рачунар постане веома врућ то би могао да буде знак да се прегрева, што би могло довести до оштећења.</p>

<p>Већина преносивих рачунара се с разлогом загреје када их користите извесно време. Уопштено немате разлог за бригу — рачунари производе много топлоте и преносни рачунари су веома сабијени, тако да морају брзо да уклоне своју топлоту и као резултат имамо да се њихово спољно кућиште загрева. Неки преносни рачунари постају превише врући, међутим, и могу бити непријатни за употребу. То је обично резултат лоше дизајнираног расхладног система. Понекад можете добити додатну опрему за хлађење која се уклапа на доњу страну преносног рачунара и обезбеђује делотворније хлађење.</p>

<p>Ако имате стони рачунар који је врућ на додир, онда му недостаје хлађење. Ако вас ово забрињава, можете да купите додатне вентилаторе за хлађење или да проверите да на вентилаторима за хлађење и лопатицама нема прашине и других сметњи. Можете такође размотрити постављање рачунара на боље проветрено место — ако га држите у затвореном простору (на пример у орману), расхладни систем рачунара неће бити у могућности да довољно брзо уклони топлоту и да обезбеди кружење хладног ваздуха.</p>

<p>Неки људи се брину о здравственим ризицима коришћења врућих преносних рачунара. Постоје запажања да дуготрајно коришћење врућег преносног рачунара на крилу може вероватно да умањи плодност, а постоје и извештаји о задобијеним мањим опекотинама (у крајњим случајевима). Ако сте забринути због ових потенцијалних проблема, можда ћете желети да се посаветујете са лекаром за савет. Наравно, можете једноставно да изаберете да не држите преносни рачунар на крилу.</p>

<p>Већина савремених рачунара ће се сами угасити ако се превише загреју, како би спречили оштећење. Ако ваш рачунар наставља да се гаси, ово може бити разлог. Ако се ваш рачунар прегрева, вероватно ћете морати да га поправите.</p>

</page>
