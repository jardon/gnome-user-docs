<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="sr">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>

    <credit type="author">
      <name>Џим Кембел</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Мајкл Хил</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зауставите или ограничите рачунар у праћењу ваших недавно коришћених датотека.</desc>
  </info>

  <title>Искључите или ограничите праћење историјата датотека</title>
  
  <p>Праћење недавно коришћених датотека и фасцикли олакшава проналажење ставки на којима сте радили у управнику датотека и у прозорчићима датотека у програмима. Можете пожелети уместо тога да ваш историјат коришћења датотека буде лични, или да прати само ваш најскорији историјат.</p>

  <steps>
    <title>Искључите праћење историјата датотека</title>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Приватност</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Историјат датотека и смеће</gui> да отворите панел.</p>
    </item>
    <item>
     <p>Поставите прекидач <gui>Историјат датотека</gui> на искључено.</p>
     <p>Да поново укључите ову функцију, поставите прекидач <gui>Историјат датотека</gui> на укључено.</p>
    </item>
    <item>
      <p>Користите дугме <gui>Очисти историјат…</gui> да одмах прочистите историјат.</p>
    </item>
  </steps>
  
  <note><p>Ово подешавање неће утицати на то како ваш веб прегледник складишти податке о веб сајтовима које посетите.</p></note>

  <steps>
    <title>Ограничите количину времена за праћење историјата датотека</title>
    <item>
      <p>Отворите преглед <gui xref="shell-introduction#activities">Активности</gui> и почните да куцате <gui>Историјат датотека и смеће</gui>.</p>
    </item>
    <item>
      <p>Кликните на <gui>Историјат датотека и смеће</gui> да отворите панел.</p>
    </item>
    <item>
     <p>Уверите се да је прекидач <gui>Историјат датотека</gui> постављен на искључено.</p>
    </item>
    <item>
     <p>Под <gui>трајањем историјата датотеке</gui>, изаберите колико дуго ће се чувати историјат датотека. Изаберите опцију од <gui>1 дана</gui>, <gui>7 дана</gui>, <gui>30 дана</gui>, или <gui>Заувек</gui>.</p>
    </item>
    <item>
      <p>Користите дугме <gui>Очисти историјат…</gui> да одмах прочистите историјат.</p>
    </item>
  </steps>

</page>
