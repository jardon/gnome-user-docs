<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-alert" xml:lang="sv">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Välj ett ljud som spelar för meddelanden, ställa in larmljudet eller inaktivera larmljud.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Välj eller inaktivera larmljudet</title>

  <p>Din dator kommer att spela ett enkelt larmljud för vissa typer av meddelanden och händelser. Du kan välja olika ljudklipp för larm, sätta larmvolymen oberoende av din systemvolym eller inaktivera larmljud helt.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Ljud</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Ljud</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>I avsnittet <gui>Larmljud</gui>, välj ett larmljud. Varje ljud kommer att spelas när du klickar på det så att du kan höra hur det låter.</p>
    </item>
  </steps>

  <p>Använd volymskjutreglaget för <gui>Systemljud</gui> i avsnittet <gui>Volymnivåer</gui> för att ställa in volymen för larmljudet. Klicka på högtalarknappen i slutet på skjutreglaget för att tysta eller aktivera larmljudet. Detta kommer inte att påverka volymen för din musik, filmer eller andra ljudfiler.</p>

</page>
