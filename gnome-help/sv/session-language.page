<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="sv">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Växla till ett annat språk för användargränssnittet och hjälptext.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ändra vilket språk du använder</title>

  <p>Du kan använda ditt skrivbord och program vilket som helst av dussintals språk, givet att du har de lämpliga språkpaketen installerade på din dator.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Region &amp; språk</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Region &amp; språk</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Klicka på <gui>Språk</gui>.</p>
    </item>
    <item>
      <p>Välj din önskade region och språk. Om din region och språk inte finns listade, klicka på <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> längst ner i listan för att välja bland alla tillgängliga regioner och språk.</p>
    </item>
    <item>
      <p>Klicka på <gui style="button">Välj</gui> för att spara.</p>
    </item>
    <item>
      <p>Din session behöver startas om för att ändringar ska genomföras. Klicka antingen på <gui style="button">Starta om…</gui> eller logga manuellt in igen senare.</p>
    </item>
  </steps>

  <p>Vissa översättningar kan vara inkompletta och vissa program kanske inte stöder ditt språk alls. Oöversatt text kommer att visas på det språk som programmet ursprungligen utvecklades på, vanligtvis amerikansk engelska.</p>

  <p>De finns vissa specialmappar i din hemkatalog där program kan spara saker som musik, bilder och dokument. Dessa mappar använder standardnamn på ditt språk. När du loggar in igen kommer du att blir tillfrågade om du vill byta namn på dessa mappar till standardnamnen på ditt valda språk. Om du planerar att använda det nya språket hela tiden bör du uppdatera mappnamnen.</p>

  <note style="tip">
    <p>Om det finns flera användarkonton på ditt system finns det en separat instans av panelen <gui>Region &amp; språk</gui> för inloggningsskärmen. Klicka på <gui>Inloggningsskärm</gui>-knappen längst upp till höger för att växla mellan de två instanserna.</p>
  </note>

</page>
