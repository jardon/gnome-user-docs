<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-notifications" xml:lang="sv">

  <info>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="40" date="2021-09-10" status="review"/>

    <credit type="author">
      <name>Marina Zhurakhinskaya</name>
      <email>marinaz@redhat.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Meddelanden visas längst upp på skärmen för att berätta för dig när vissa händelser inträffar.</desc> 
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Aviseringar och aviseringslistan</title>

<section id="what">
  <title>Vad är en avisering?</title>

  <p>Om ett program eller en systemkomponent vill få tag i din uppmärksamhet, kommer en avisering att visas längst upp på skärmen, eller på din låsskärm.</p>

  <p>Om du exempelvis får ett nytt chattmeddelande eller ett nytt e-postmeddelande så kommer du att få en avisering som informerar dig om detta. Chattaviseringar ges specialbehandling och representeras av de individuella kontakterna som skickade chattmeddelandena till dig.</p>

<!--  <p>To minimize distraction, some notifications first appear as a single line.
  You can move your mouse over them to see their full content.</p>-->

  <p>Andra aviseringar har valbara knappar. För att stänga en avisering av denna typ utan att välja en av dess alternativ, klicka på stängknappen.</p>

  <p>Att klicka på stängknappen på vissa aviseringar förkastar dem. Andra, som Rhythmbox eller ditt chattprogram kommer att förbli dolda i aviseringslistan.</p>

</section>

<section id="notificationlist">

  <title>Aviseringslistan</title>

  <p>Aviseringslistan ger dig ett sätt att få tillbaka dina aviseringar när det är bekvämt för dig. Den visas när du klickar på klockan eller trycker <keyseq><key xref="keyboard-key-super">Super</key><key>V</key></keyseq>. Aviseringslistan innehåller alla aviseringar som du inte har agerat på eller som finns där permanent.</p>

  <p>Du kan visa en avisering genom att klicka på den i listan. Du kan stänga aviseringslistan genom att trycka <keyseq><key>Super</key><key>V</key></keyseq> igen eller <key>Esc</key>.</p>

  <p>Klicka på knappen <gui>Töm lista</gui> för att tömma aviseringslistan.</p>

</section>

<section id="hidenotifications">

  <title>Dölja aviseringar</title>

  <p>Om du arbetar med något och inte vill bli störd kan du stänga av aviseringar.</p>

  <p>Du kan dölja alla aviseringar genom att öppna aviseringslistan och slå på <gui>Stör ej</gui> längst ner. Alternativt kan du:</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Aviseringar</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Slå av <gui>Stör ej</gui>.</p>
    </item>
  </steps>

  <p>När de är avstängda kommer de flesta aviseringar inte att poppa upp längst upp på skärmen. Aviseringar kommer fortfarande att finnas tillgängliga i aviseringslistan när du visar den (genom att klicka på klockan, eller trycka på <keyseq><key>Super</key><key>V</key></keyseq>) och de kommer att börja poppa upp igen när du slår på inställningen igen.</p>

  <p>Du kan också inaktivera eller återaktivera aviseringar för individuella program från panelen <gui>Aviseringar</gui>.</p>

</section>

<section id="lock-screen-notifications">

  <title>Dölja aviseringar på låsskärmen</title>

  <p>När skärmen är låst kommer aviseringar att visas på låsskärmen. Du kan av säkerhetsskäl konfigurera så att låsskärmen döljer dessa aviseringar.</p>

  <steps>
    <title>För att slå av aviseringar när skärmen är låst:</title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Aviseringar</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item><p>Slå av <gui>Aviseringar på låsskärm</gui>.</p></item>
  </steps>

</section>

</page>
