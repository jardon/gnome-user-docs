<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Använd längre och mer komplicerade lösenord.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Välj ett säkert lösenord</title>

  <note style="important">
    <p>Gör dina lösenord enkla nog för att själv komma ihåg dem, men svåra för andra (inklusive datorprogram) att gissa.</p>
  </note>

  <p>Att välja ett bra lösenord kommer att hjälpa till att hålla din dator säker. Om lösenordet är lätt att gissa kan någon komma på det och få tillgång till din personliga information.</p>

  <p>Personer kan till och med använda datorer för att systematiskt försöka gissa ditt lösenord, så även ett som skulle vara svårt för en människa att gissa kan vara extremt enkelt för ett datorprogram att knäcka. Här kommer några tips om hur du väljer ett bra lösenord:</p>
  
  <list>
    <item>
      <p>Använd en blandning av stora och små bokstäver, siffror symboler och mellanslag i lösenordet. Detta gör det svårare att gissa; det finns fler symboler att välja mellan vilket innebär fler möjliga lösenord som någon måste kontrollera när de försöka gissa ditt.</p>
      <note>
        <p>Ett bra sätt för att välja ett lösenord är att ta den första bokstaven i varje ord i en fras som du kan komma ihåg. Frasen kan vara namnet på en film, en bok, en låt eller ett album. Till exempel skulle ”Flatland: A Romance of Many Dimensions” bli F:ARoMD eller faromd eller f: aromd.</p>
      </note>
    </item>
    <item>
      <p>Gör dina lösenord så långa som möjligt. Ju fler tecken det innehåller, desto längre bör det ta för en person eller dator att gissa det.</p>
    </item>
    <item>
      <p>Använd inte några ord som dyker upp i ett standarduppslagsverk på något språk. Lösenordsknäckare kommer att försöka med dessa först. Det vanligaste lösenordet är ”password” — personer kan gissa lösenord som detta väldigt snabbt!</p>
    </item>
    <item>
      <p>Använd inte någon personlig information såsom datum, registreringsnummer eller familjemedlemmars namn.</p>
    </item>
    <item>
      <p>Använd inte några substantiv.</p>
    </item>
    <item>
      <p>Välj ett lösenord som kan skriva snabbt, för att minska chanserna att någon kan lista ut vad du skrivit om de råkar se dig.</p>
      <note style="tip">
        <p>Skriv aldrig ner dina lösenord någonstans. De kan hittas lätt!</p>
      </note>
    </item>
    <item>
      <p>Använd olika lösenord för olika saker.</p>
    </item>
    <item>
      <p>Använd olika lösenord för olika konton.</p>
      <p>Om du använder samma lösenord för alla dina konton, kan den som listar ut det omedelbart få tillgång till alla dina konton.</p>
      <p>Det kan dock vara svårt att komma ihåg många lösenord. Även om det inte är lika säkert som att välja olika lösenord för allting så kan det vara lättare att använda samma för saker som inte spelar någon roll (som webbplatser), och olika för viktiga saker (som din internetbank och din e-post).</p>
   </item>
   <item>
     <p>Byt dina lösenord regelbundet.</p>
   </item>
  </list>

</page>
