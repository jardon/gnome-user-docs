<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="sv">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Lägg till eller ta bort en kontakt i den lokala adressboken.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Lägg till eller ta bort en kontakt</title>

  <p>För att lägga till en kontakt:</p>

  <steps>
    <item>
      <p>Tryck på <gui style="button">+</gui>-knappen.</p>
    </item>
    <item>
      <p>I dialogrutan <gui>Ny kontakt</gui>, mata in kontaktnamnet och deras information. Klicka på rullgardinsmenyn intill varje fält för att välja typen som fältet innehåller.</p>
    </item>
    <item>
      <p>För att lägga till fler detaljer, tryck på alternativet <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Visa mer</span></media>.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Lägg till</gui> för att spara kontakten.</p>
    </item>
  </steps>

  <p>För att ta bort en kontakt:</p>

  <steps>
    <item>
      <p>Välj kontakten från din kontaktlista.</p>
    </item>
    <item>
      <p>Tryck på knappen <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Visa mer</span></media> i det övre högra hörnet på rubrikraden.</p>
    </item>
    <item>
      <p>Tryck på alternativet <gui style="menu item">Ta bort</gui> för att ta bort kontakten.</p>
    </item>
   </steps>
    <p>För att ta bort en eller flera kontakter, kryssa i rutorna intill kontakterna som du vill ta bort och tryck på <gui style="button">Ta bort</gui>.</p>
</page>
