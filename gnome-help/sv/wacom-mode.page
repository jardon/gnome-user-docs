<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="sv">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Växla plattan mellan plattläge och musläge.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ställ in Wacom-plattans spårningsläge</title>

<p><gui>Ritplatteläge</gui> avgör hur styluspennan mappas till skärmen.</p>

<steps>
  <item>
    <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Wacom-platta</gui>.</p>
  </item>
  <item>
    <p>Klicka på <gui>Wacom-ritplatta</gui> för att öppna panelen.</p>
    <note style="tip"><p>Om ingen ritplatta detekteras kommer du att bli ombedd att <gui>Anslut eller aktivera din Wacom-ritplatta</gui>. Klicka på <gui>Bluetooth</gui> i sidopanelen för att ansluta en trådlös ritplatta.</p></note>
  </item>
  <item>
    <p>Välj mellan ritplatteläge (absolut) och styrplatteläge (relativt). Slå på <gui>Ritplatteläge</gui> för att få ritplatteläge.</p>
  </item>
</steps>

  <note style="info"><p>I <em>absolut</em> läge kommer varje punkt på plattan att mappas till en punkt på skärmen. Övre vänstra hörnet av skärmen kommer till exempel alltid att motsvara samma punkt på plattan.</p>
  <p>I <em>relativt</em> läge så kommer markören på skärmen inte att flytta sig om du lyfter upp styluspennan från plattan och sätter ner den på en annan position. Det är så en mus fungerar.</p>
  </note>

</page>
