<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="sv">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ta bort användare som inte längre använder din dator.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Ta bort ett användarkonto</title>

  <p>Du kan <link xref="user-add">lägga till flera användarkonton på din dator</link>. Om någon inte längre använder din dator kan du ta bort den användarens konto.</p>

  <p>Du behöver <link xref="user-admin-explain">administratörsbehörighet</link> för att ta bort användarkonton.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Användare</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Användare</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Tryck på <gui style="button">Lås upp</gui> i övre högra hörnet och skriv in ditt lösenord när du blir tillfrågad.</p>
    </item>
    <item>
      <p>Klicka på användarkontot som du vill ta bort under <gui>Andra användare</gui>.</p>
    </item>
    <item>
      <p>Tryck på knappen <gui style="button">Ta bort användare…</gui> för att ta bort det användarkontot.</p>
    </item>
    <item>
      <p>Varje användare har sin egen hemmapp för sina filer och inställningar. Du kan välja att behålla eller ta bort användarens hemmapp. Klicka på <gui>Ta bort filer</gui> om du är säker på att de inte kommer att användas längre och du behöver frigöra diskutrymme. Dessa filer tas bort permanent. De kan inte återställas. Det kan vara bra att göra en säkerhetskopia av filerna till en extern lagringsenhet innan du tar bort dem.</p>
    </item>
  </steps>

</page>
