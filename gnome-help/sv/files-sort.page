<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="sv">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sortera filer efter namn, storlek, typ, eller när de ändrades.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Sortera filer och mappar</title>

<p>Du kan sortera filer på olika sätt i en mapp, till exempel genom att sortera dem efter datum eller filstorlek. Se <link xref="#ways"/> nedan för en lista över vanliga sätt att sortera filer. Se <link xref="nautilus-views"/> för information om hur du ändrar standardsorteringen.</p>

<p>Sättet du sorterar filer beror på <em>mappvyn</em> som du använder. Du kan ändra den aktuella vyn via list- eller ikonknapparna i verktygsfältet.</p>

<section id="icon-view">
  <title>Ikonvy</title>

  <p>För att sortera filer i en annan ordning, klicka på knappen visningsalternativ i verktygsfältet och välj <gui>Efter namn</gui>, <gui>Efter storlek</gui>, <gui>Efter typ</gui>, <gui>Efter ändringsdatum</gui> eller <gui>Efter åtkomstdatum</gui>.</p>

  <p>Som ett exempel, om du väljer <gui>Efter namn</gui> kommer filerna att sorteras efter deras namn i alfabetisk ordning. Se <link xref="#ways"/> för ytterligare alternativ.</p>

  <p>Du kan sortera i omvänd ordning genom att välja <gui>Omvänd ordning</gui> från menyn.</p>

</section>

<section id="list-view">
  <title>Listvy</title>

  <p>För att sortera filer i en annan ordning, klicka på en av kolumnrubrikerna i filhanteraren. Till exempel, klicka på <gui>Typ</gui> för att sortera efter filtyp. Klicka på kolumnrubriken igen för att sortera i omvänd ordning.</p>
  <p>I listvyn kan du visa kolumner med fler attribut och sortera efter dessa kolumner. Klicka på knappen visningsalternativ i verktygsfältet, välj <gui>Synliga kolumner…</gui> och välj de kolumner som du vill ska vara synliga. Du kommer sedan kunna sortera efter dessa kolumner. Se <link xref="nautilus-list"/> för beskrivningar av tillgängliga kolumner.</p>

</section>

<section id="ways">
  <title>Olika sätt att sortera filer</title>

  <terms>
    <item>
      <title>Namn</title>
      <p>Sorterar alfabetiskt efter namnet på filen.</p>
    </item>
    <item>
      <title>Storlek</title>
      <p>Sorterar efter filens storlek (hur mycket diskutrymme den tar upp). Sorterar från minsta till största som standard.</p>
    </item>
    <item>
      <title>Typ</title>
      <p>Sorterar alfabetiskt efter filtyp. Filer av samma typ grupperas tillsammans och sorteras sedan efter namn.</p>
    </item>
    <item>
      <title>Senast ändrad</title>
      <p>Sorterar efter tid och datum när en fil senast ändrades. Sorterar som standard från äldst till nyast.</p>
    </item>
  </terms>

</section>

</page>
