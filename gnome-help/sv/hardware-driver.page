<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="hardware-driver" xml:lang="sv">

  <info>
    <link type="guide" xref="hardware" group="more"/>

    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Dokumentationsprojekt för GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>En hårdvaru-/enhetsdrivrutin låter din dator använda enheter som ansluts till den.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

<title>Vad är en drivrutin?</title>

<p>Enheter är de ”fysiska” delarna av din dator. De kan vara <em>externa</em> som skrivare och skärm eller <em>interna</em> som grafik- och ljudkort.</p>

<p>För att din dator ska kunna använda dessa enheter måste den veta hur den ska kommunicera med dem. Detta utförs med en liten programvara som kallas <em>enhetsdrivrutin</em>.</p>

<p>När du ansluter en enhet till din dator måste du ha den korrekta drivrutinen installerad för att den enheten ska fungera. Om du till exempel ansluter en skrivare men den korrekta drivrutinen inte är tillgänglig kommer du inte att kunna använda skrivaren. Normalt använder varje modell av en enhet en drivrutin som inte är kompatibel med någon annan modell.</p>

<p>I Linux är drivrutiner för de flesta enheter installerade som standard, så allting borde fungera när du ansluter det. Det kan dock finnas drivrutiner som behöver installeras manuellt eller som inte finns tillgängliga överhuvudtaget.</p>

<p>Dessutom finns det drivrutiner som inte är kompletta eller delvis inte fungerar. Du kan till exempel upptäcka att din skrivare inte kan skriva ut dubbelsidigt men annars fungerar bra.</p>

</page>
