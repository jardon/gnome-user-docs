<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="sv">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Att känna till din IP-adress kan hjälpa dig upptäcka nätverksproblem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hitta din IP-adress</title>

  <p>Att veta din IP-adress kan hjälpa dig att felsöka problem med din internetanslutning. Kanske blir du förvånad över att veta att du har <em>två</em> IP-adresser: en IP-adress för din dator på det interna nätverket och en IP-adress för din dator på internet.</p>
  
  <section id="wired">
    <title>Hitta din trådbundna anslutnings interna IP-adress (på nätverket)</title>
  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Nätverk</gui> i sidopanelen för att öppna panelen.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Om mer än en typ av trådbunden anslutning är tillgänglig kan du se namn som <gui>PCI Ethernet</gui> eller <gui>USB Ethernet</gui> i stället för <gui>Trådbundet</gui>.</p>
      </note>
    </item>
    <item>
      <p>Klicka på <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">inställningar</span></media>-knappen intill den aktiva anslutningen för IP-adressen och andra detaljer.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Hitta din trådlösa anslutnings interna IP-adress (på nätverket)</title>
  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Trådlöst</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Klicka på <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">inställningar</span></media>-knappen intill den aktiva anslutningen för IP-adressen och andra detaljer.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Hitta din externa IP-adress (internet)</title>
  <steps>
    <item>
      <p>Besök <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Webbsidan kommer visa dig din externa IP-adress.</p>
    </item>
  </steps>
  <p>Beroende på hur din dator ansluter till internet, kan den interna och den externa adressen vara densamma.</p>  
  </section>

</page>
