<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hardware" xml:lang="sv">

  <info>
    <link type="guide" xref="about" group="hardware"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hitta information om hårdvaran som är installerad på ditt system.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hitta information om systemets hårdvara</title>

  <p>Att veta vilken hårdvara som är installerad kan hjälpa dig förstå om ny programvara eller hårdvara kommer vara kompatibel med systemet.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Om</gui>.</p>
    </item>
    <item>
      <p>Tryck på <gui>Om</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Titta på informationen som listas under <gui>Hårdvarumodell</gui>, <gui>Minne</gui>, <gui>Processor</gui> och så vidare.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Information kan kopieras från varje objekt genom att välja det och sedan kopiera till urklipp. Detta gör det lättare att dela information om systemet med andra.</p>
  </note>

</page>
