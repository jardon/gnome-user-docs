<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="sv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Förhindra andra personer från att använda ditt skrivbord när du går iväg från datorn.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Lås automatiskt skärmen</title>
  
  <p>När du lämnar datorn, bör du <link xref="shell-exit#lock-screen">låsa skärmen</link> för att förhindra andra personer från att använda ditt skrivbord och komma åt dina filer. Om du ibland glömmer att låsa skärmen, kan det vara bra att låta din dators skärm låsas automatiskt efter en inställd tidsperiod. Detta kommer att hjälpa att säkra din dator när du inte använder den.</p>

  <note><p>När skärmen är låst kommer dina program och systemprocesser att fortsätta köra, men du måste mata in ditt lösenord för att börja använda dem igen.</p></note>
  
  <steps>
    <title>För att ställa in tidsintervallet innan skärmen låses automatiskt:</title>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Skärmlås</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Skärmlås</gui> för att öppna panelen.</p>
    </item>
    <item>
      <p>Säkerställ att <gui>Automatiskt skärmlås</gui> är påslagen, välj sedan en tidsperiod från rullgardinsmenyn <gui>Fördröjning för automatiskt skärmlås</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Program kan presentera aviseringar för dig som även visas på din låsskärm. Detta är behändigt för att t.ex. se om du har någon e-post utan att låsa upp skärmen. Om du är bekymrad om att andra kan se dessa aviseringar, slå av <gui>Aviseringar på låsskärm</gui>. Se <link xref="shell-notifications"/> för fler aviseringsinställningar.</p>
  </note>

  <p>När skärmen är låst och du vill låsa upp den, tryck på <key>Esc</key> eller svep uppåt från botten av skärmen med musen. Mata sedan in ditt lösenord och tryck <key>Retur</key> eller klicka på <gui>Lås upp</gui>. Alternativt kan du bara börja skriva ditt lösenord så kommer låsgardinen automatiskt att höjas medan du skriver.</p>

</page>
