<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="sv">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Färgprofiler kan importeras genom att öppna dem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2014, 2015, 2016, 2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2016, 2017</mal:years>
    </mal:credit>
  </info>

  <title>Hur importerar jag färgprofiler?</title>

  <p>Du kan importera en färgprofil genom att dubbelklicka på en <file>.ICC</file>- eller <file>.ICM</file>-fil i filhanteraren.</p>

  <p>Alternativt kan du hantera dina färgprofiler via panelen <gui>Färg</gui>.</p>

  <steps>
    <item>
      <p>Öppna översiktsvyn <gui xref="shell-introduction#activities">Aktiviteter</gui> och börja skriv <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Inställningar</gui>.</p>
    </item>
    <item>
      <p>Klicka på <gui>Färg</gui> i sidopanelen för att öppna panelen.</p>
    </item>
    <item>
      <p>Välj din enhet.</p>
    </item>
    <item>
      <p>Klicka på <gui>Lägg till profil</gui> för att välja en befintlig profil eller importera en ny profil.</p>
    </item>
    <item>
      <p>Tryck på <gui>Lägg till</gui> för att bekräfta ditt val.</p>
    </item>
  </steps>

  <p>Din skärms tillverkare kan tillhandahålla en profil som du kan använda. Dessa profiler är vanligtvis gjorda för den genomsnittliga skärmen, så den är kanske inte perfekt för din specifika skärm. För den bästa kalibreringen bör du <link xref="color-calibrate-screen">skapa din egen profil</link> genom att använda en färgkalibrator eller spektrofotometer.</p>

</page>
