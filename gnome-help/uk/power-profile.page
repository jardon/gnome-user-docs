<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-profile" xml:lang="uk">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Балансування швидкодії і споживання енергії.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Вибір профілю живлення</title>

  <p>Керувати живленням комп'ютера можна за допомогою вибору <gui>Режиму живлення</gui>. Передбачено три можливих профілі:</p>
  
  <list>
    <item>
      <p><gui>Збалансоване</gui>: стандартна швидкодія та споживання енергії. Це типовий варіант.</p>
    </item>
    <item>
      <p><gui>Заощадження енергії</gui>: знижена швидкодія і споживання енергії. Використання цього варіанта робить довшим період роботи від акумуляторів. Вмикає деякі параметри заощадження енергії, зокрема агресивне притлумлення екрана, і запобігає їхньому вимиканню.</p>
    </item>
    <item>
      <p><gui>Швидкодія</gui>: висока швидкодія та споживання енергії. Цей пункт режиму буде показано, лише якщо його підтримку передбачено обладнанням комп'ютера. Його можна буде вибрати, якщо ваш комп'ютер працює від електричної мережі. Якщо на пристрої передбачено виявлення його розташування на колінах, ви не зможете скористатися цим режимом, доки комп'ютер перебуває на ваших колінах.</p>
    </item>
  </list>

  <steps>
    <title>Щоб вибрати профіль живлення, виконайте такі дії:</title>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Живлення</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Живлення</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>У розділі <gui>Режим живлення</gui> виберіть один з профілів.</p>
    </item>
    
  </steps>

</page>
