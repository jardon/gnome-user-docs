<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="uk">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="shell-overview#apps"/>

    <!--
    Recommended statuses: stub incomplete draft outdated review candidate final
    -->
    <revision version="gnome:40" date="2021-03-12" status="candidate"/>
    <!--
    FIXME: I'm tentatively marking this final for GNOME 40, because it's at
    least no longer incorrect. But here's a lot to improve:

    * I'm not super happy with the "Other" catchall section at the end, but I also
      don't want to add lots of singleton sections. Tweak the presentation.

    * gnome-shell references network-wireless-disconnected but it doesn't exist:
      https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3827

    * The icons for disconnected states might change:
      https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/102

    * topbar-audio-volume-overamplified: Write docs on overamplification:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/117

    * Write docs on setting mic sensitivity, and link in a learn more item:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/116

    * topbar-network-wireless-connected: We're super handwavy about when this is
      used. We could use some docs on ad hoc networks.

    * topbar-screen-shared: We have no docs on the screen share portal:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/118

    * topbar-thunderbolt-acquiring: We have no docs on Thunderbolt:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/119
    -->

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Пояснення призначення піктограм, які розташовано у правій частині верхньої панелі.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Для чого призначено піктограми на верхній панелі?</title>

<p>На цій сторінці наведено пояснення щодо піктограм, які розташовано у правому куті екрана. Точніше, тут описано різні варіанти піктограм, які може бути показано системою.</p>

<links type="section"/>


<section id="universalicons">
<title>Піктограми доступності</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-accessibility.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-accessibility.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Надає вам змогу швидко перемикати різноманітні параметри доступності.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-pointer.svg"/>
        </if:when>
        <media src="figures/topbar-pointer.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором типу клацання, яке буде виконано при використанні клацання наведенням.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="a11y">Дізнатися більше про доступність.</link></p></item>
  <item><p><link xref="a11y-dwellclick">Дізнатися більше про клацання наведенням.</link></p></item>
</list>
</section>


<section id="audioicons">
<title>Звукові піктограми</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume.svg"/>
        </if:when>
        <media src="figures/topbar-audio-volume.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором гучності гучномовців або навушників.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-audio-volume-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Звук на гучномовцях або навушниках вимкнено.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity.svg"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором чутливості мікрофона.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Звук на мікрофоні вимкнено.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="sound-volume">Дізнатися більше про гучність звуку.</link></p></item>
</list>
</section>


<section id="batteryicons">
<title>Піктограми акумулятора</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-charging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-charging.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором рівня заряду акумулятора при заряджанні акумулятора.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100-charged.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100-charged.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Акумулятор повністю заряджено і він заряджається.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-discharging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-discharging.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором рівня заряду акумулятора, коли акумулятор не заряджається.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Акумулятор повністю заряджено і він не заряджається.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-system-shutdown.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-system-shutdown.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Піктограма живлення, яку буде показано у системах без акумулятора.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="power-status">Дізнатися більше про стан акумуляторів.</link></p></item>
</list>
</section>


<section id="bluetoothicons">
<title>Піктограми Bluetooth</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg"/>
      </if:choose>
    </td>
    <td><p>Режим польоту увімкнено. Bluetooth вимикається, якщо увімкнено режим польоту.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-bluetooth-active.svg"/>
        </if:when>
        <media src="figures/topbar-bluetooth-active.svg"/>
      </if:choose>
    </td>
    <td><p>Пристрій Bluetooth пов'язано, він перебуває у користуванні. Цю піктограму буде показано, лише якщо є активний пристрій, а не просто тоді, коли Bluetooth увімкнено.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Дізнатися більше про режим польоту.</link></p></item>
  <item><p><link xref="bluetooth">Дізнатися більше про Bluetooth.</link></p></item>
</list>
</section>


<section id="networkicons">
<info>
<!--
FIXME: I don't want a bare desc, because it ends up in the section links above.
But this section also gets a seealso from net-wireless.page, and we'd like a
desc for that. In Mallard 1.2, we can use role on desc. It works in Yelp, but
it's not in a schema yet, so it will cause validation errors in CI.
  <desc type="link" role="seealso">Explains the meanings of the networking icons in the top bar.</desc>
-->
</info>
<title>Піктограми мережі</title>

<table shade="rows">
  <title>Бездротові з'єднання (Wi-Fi)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Режим польоту увімкнено. Бездротова мережа вимикається, якщо увімкнено режим польоту.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Встановлюється з'єднання із бездротовою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength.svg"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором потужності з'єднання із бездротовою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднано із бездротовою мережею, але сигналу немає.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднано із бездротовою мережею. Цю піктограму буде показано, лише якщо потужність сигналу не вдалося визначити, зокрема при встановленні з'єднання із спеціальними мережами.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-no-route.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-no-route.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднано із бездротовою мережею, але немає маршруту до інтернету. Причиною цього можуть бути помилкові налаштування мережі або відмова на сервері надавача послуг інтернету.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Дізнатися більше про режим польоту.</link></p></item>
  <item><p><link xref="net-wireless-connect">Дізнатися більше про роботу із бездротовою мережею.</link></p></item>
</list>

<table shade="rows">
  <title>Стільникова мережа (мобільна широкосмугова мережа)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Режим польоту увімкнено. Стільникова мережа вимикається, якщо увімкнено режим польоту.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Встановлення з'єднання зі стільниковою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal.svg"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal.svg"/>
      </if:choose>
    </td>
    <td><p>Є індикатором потужності з'єднання зі стільниковою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднано зі стільниковою мережею, але сигналу немає.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднано зі стільниковою мережею. Цю піктограму буде показано, лише якщо потужність сигналу не вдасться визначити, зокрема при з'єднанні за допомогою Bluetooth. Якщо потужність сигналу вдасться визначити, буде показано піктограму потужності сигналу.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Дізнатися більше про режим польоту.</link></p></item>
  <item><p><link xref="net-mobile">Дізнатися більше про роботу зі стільниковою мережею.</link></p></item>
</list>

<table shade="rows">
  <title>Дротові з'єднання</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Встановлення з'єднання із дротовою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired.svg"/>
      </if:choose>
    </td>
    <td><p>З'єднано із дротовою мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-disconnected.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-disconnected.svg"/>
      </if:choose>
    </td>
    <td><p>Від'єднано від мережі.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-no-route.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-no-route.svg"/>
      </if:choose>
    </td>
    <td><p>З'єднано із дротовою мережею, але немає маршруту до інтернету. Причиною цього можуть бути помилкові налаштування мережі або відмова на сервері надавача послуг інтернету.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wired-connect">Дізнатися більше про роботу із дротовою мережею.</link></p></item>
</list>

<table shade="rows">
  <title>VPN (віртуальна приватна мережа)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Встановлення з'єднання із віртуальною приватною мережею.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn.svg"/>
      </if:choose>
    </td>
    <td><p>З'єднано із віртуальною приватною мережею.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-vpn-connect">Дізнатися більше про віртуальні приватні мережі.</link></p></item>
</list>

</section>


<section id="othericons">
<title>Інші піктограми</title>
<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-input-method.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-input-method.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Є індикатором розкладки клавіатури або способу введення, який використовується. Клацніть, щоб вибрати іншу розкладку. Меню розкладки клавіатури буде показано, лише якщо у вашій системі налаштовано декілька способів введення.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-find-location.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-find-location.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Доступ до даних щодо вашого місця перебування надано програми. Ви можете вимкнути доступ до даних щодо місця перебування за допомогою меню.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-night-light.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-night-light.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>«Нічне освітлення» змінило температуру кольорів дисплея, щоб зменшити навантаження на очі. Ви можете тимчасово вимкнути «Нічне освітлення» за допомогою меню.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-media-record.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-media-record.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Ведеться запис відео з усього екрана.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-screen-shared.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-screen-shared.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Зараз програма транслює зображення з екрана або іншого вікна.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-thunderbolt-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-thunderbolt-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>З'єднання із пристроєм Thunderbolt, зокрема док-станцією.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="keyboard-layouts">Дізнатися більше про розкладки клавіатури</link></p></item>
  <item><p><link xref="privacy-location">Дізнатися більше про конфіденційність ат служби визначення місця перебування.</link></p></item>
  <item><p><link xref="display-night-light">Дізнатися більше про нічне освітлення та температуру кольорів.</link></p></item>
  <item><p><link xref="screen-shot-record">Дізнатися більше про знімки екрана та записи з екрана.</link></p></item>
</list>

</section>

</page>
