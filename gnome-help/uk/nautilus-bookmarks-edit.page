<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="uk">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Додавання, вилучення і перейменування закладок у програмі для керування файлами.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Редагування закладок на теки</title>

  <p>Список ваших закладок буде показано на бічній панелі вікна програми для керування файлами.</p>

  <steps>
    <title>Додавання закладки:</title>
    <item>
      <p>Відкрийте теку (або місце), закладку для якого ви хочете створити.</p>
    </item>
    <item>
      <p>Клацніть на пункті поточної теки на панелі шляху і виберіть у контекстному меню пункт <gui style="menuitem">Додати до закладок</gui>.</p>
      <note>
        <p>Крім того, ви можете перетягнути теку на бічну панель і скинути її на пункт <gui>Створити закладку</gui>, який з'явиться на панелі у динамічному режимі.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Вилучення закладки:</title>
    <item>
      <p>Клацніть правою кнопкою миші на пункті закладки на бічній панелі і виберіть у контекстному меню пункт <gui>Вилучити</gui>.</p>
    </item>
  </steps>

  <steps>
    <title>Перейменовування закладки:</title>
    <item>
      <p>Клацніть правою кнопкою миші на пункті закладки на бічній панелі і виберіть у контекстному меню пункт <gui>Перейменувати…</gui>.</p>
    </item>
    <item>
      <p>У полі <gui>Назва</gui> введіть нову назву закладки.</p>
      <note>
        <p>Перейменування закладки не призводить до перейменування теки. Якщо у вас є закладки на дві різні теки у різних місцях, але самі теки мають однакові назви, назви закладок будуть однаковими і ви не зможете їх розрізнити. У таких випадках корисно надати закладці назву, відмінну від назви теки, на яку вона вказує.</p>
      </note>
    </item>
  </steps>

</page>
