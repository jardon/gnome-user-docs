<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="uk">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Налаштовування принтера, який з'єднано з вашим комп'ютером безпосередньо або за допомогою локальної мережі.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовування локального принтера</title>

  <p>Ваша система може автоматично розпізнавати принтери багатьох типів, щойно їх буде з'єднано із комп'ютером. З'єднання більшості принтерів відбувається за допомогою USB-кабелю, але деякі принтери з'єднуються із вашою мережею дротового або бездротового зв'язку.</p>

  <note style="tip">
    <p>Якщо ваш принтер з'єднується із комп'ютером мережею, його не буде налаштовано автоматично — вам доведеться додати його за допомогою панелі <gui>Принтери</gui> програми <gui>Параметри</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Переконайтеся, що принтер увімкнено.</p>
    </item>
    <item>
      <p>З'єднайте ваш принтер із комп'ютером за допомогою відповідного кабелю. На екрані ви можете побачити повідомлення про те, що система шукає драйвери. Вас можуть попросити пройти розпізнавання, щоб встановити знайдені драйвери.</p>
    </item>
    <item>
      <p>Після того, як система завершить встановлення драйверів, ви побачите відповідне повідомлення. Виберіть пункт <gui>Надрукувати тестову сторінку</gui> для друку тестової сторінки або пункт <gui>Параметри</gui> для внесення додаткових змін у налаштування принтера.</p>
    </item>
  </steps>

  <p>Якщо принтер не було налаштовано автоматично, ви можете додати його у параметрах принтерів:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Принтери</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Принтери</gui>.</p>
    </item>
    <item>
      <p>Залежно від вашої системи, вам, можливо, доведеться натиснути кнопку <gui style="button">Розблокувати</gui> у верхньому правому куті і ввести ваш пароль, коли вас про це попросять.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">Додати…</gui>.</p>
    </item>
    <item>
      <p>У контекстному вікні виберіть новий принтер і натисніть кнопку <gui style="button">Додати</gui>.</p>
      <note style="tip">
        <p>Якщо ваш принтер не було виявлено автоматично, але вам відома його адреса у мережі, введіть її до поля для введення тексту у нижній частині діалогового вікна, а потім натисніть кнопку <gui style="button">Додати</gui></p>
      </note>
    </item>
  </steps>

  <p>Якщо вашого пункту принтера немає у вікні <gui>Додати принтер</gui>, вам, можливо, слід встановити драйвери до принтера.</p>

  <p>Після встановлення принтера, ви можете <link xref="printing-setup-default-printer">змінити ваш типовий принтер</link>.</p>

</page>
