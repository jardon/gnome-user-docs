<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="uk">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Рафаель Фонтенелле (Rafael Fontenelle)</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Дані щодо вашої IP-адреси можуть допомогти у розв'язанні проблем із мережею.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Визначення вашої IP-адреси</title>

  <p>Дані щодо вашої IP-адреси можуть допомогти у розв'язанні проблем із інтернет-з'єднанням. Можливо, для вас буде несподіванкою дізнатися, що у вас є <em>дві</em> IP-адреси: IP-адреса вашого комп'ютера у внутрішній мережі і IP-адреса вашого комп'ютера у інтернеті.</p>
  
  <section id="wired">
    <title>Визначення вашої внутрішньої (мережевої) IP-адреси дротового з'єднання</title>
  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Мережа</gui> на бічній панелі, щоб відкрити панель.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Якщо доступними є дротові з'єднання декількох типів, ви зможете побачити назви, подібні до <gui>PCI Ethernet</gui> або <gui>USB Ethernet</gui>, замість простого запису <gui>Дротова</gui>.</p>
      </note>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">параметри</span></media> поряд з пунктом активного з'єднання, щоб ознайомитися із даними щодо IP-адреси та інших параметрів з'єднання.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Визначення вашої внутрішньої (мережевої) IP-адреси бездротового з'єднання</title>
  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Wi-Fi</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">параметри</span></media> поряд з пунктом активного з'єднання, щоб ознайомитися із даними щодо IP-адреси та інших параметрів з'єднання.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Визначення вашої зовнішньої IP-адреси (адреси в інтернеті)</title>
  <steps>
    <item>
      <p>Відвідайте <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>Сайт покаже вам зовнішню IP-адресу.</p>
    </item>
  </steps>
  <p>Залежно від способу з'єднання комп'ютера із інтернетом, внутрішня і зовнішня адреси можуть збігатися.</p>  
  </section>

</page>
