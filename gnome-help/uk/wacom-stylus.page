<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="uk">

  <info>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-12"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Визначення прив'язки кнопок до дій та реакції на тиск стилом Wacom.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовування стила</title>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Планшет Wacom</gui>.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Планшет Wacom</gui>, щоб відкрити панель.</p>
      <note style="tip"><p>Якщо планшет не виявлено, програма надішле вам запит: <gui>Під'єднайте або ввімкніть планшет Wacom</gui>. Натисніть пункт <gui>Bluetooth</gui> на бічній панелі, щоб з'єднати з комп'ютером бездротовий планшет.</p></note>
    </item>
    <item>
      <p>У цьому розділі буде показано параметри і налаштування, які є специфічними для стила, назву пристрою (класу стила) та діаграму вгорі.</p>
      <note style="tip"><p>Якщо стила виявлено не буде, програма попросить вас: <gui>Будь ласка, перемістіть ваше стило ближче до планшету, що його налаштувати</gui>.</p></note>
      <p>Можна скоригувати такі параметри:</p>
      <list>
        <item><p><gui>Чутливість дотику:</gui> скористайтеся повзунком для коригування «чутливості» від <gui>М'яка</gui> до <gui>Тверда</gui>.</p></item>
        <item><p>Налаштування кнопки/коліщатка гортання (ці параметри є різними для різних типів стил). Натисніть пункт меню поряд із кожною з міток, щоб вибрати одну з таких функцій: клацання середньою кнопкою миші, клацання правою кнопкою миші, назад або вперед.</p></item>
     </list>
    </item>
    <item>
      <p>Натисніть <gui>Випробувати ваші параметри</gui> на смужці заголовка, щоб відкрити панель для ескізів, де ви зможете перевірити вибрані параметри стила.</p>
    </item>
  </steps>

</page>
