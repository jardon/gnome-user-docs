<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="uk">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Керування тим, наскільки швидко слід натиснути кнопку миші удруге, щоб система зареєструвала подвійне клацання.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Коригування швидкості подвійного клацання</title>

  <p>Подвійне клацання буде зареєстровано, лише якщо ви натиснете кнопку миші двічі достатньо швидко. Якщо друге клацання буде здійснено за надто довгий період після першого, система зареєструє два окремих клацання, а не подвійне клацання. Якщо у вас виникають проблеми із швидким натисканням клавіш, вам слід збільшити час очікування.</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Доступність</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Доступність</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>У розділі <gui>Вказування та натискання</gui> скоригуйте повзунок <gui>Затримка подвійного клацання</gui> до комфортного значення.</p>
    </item>
  </steps>

  <p>Якщо на вашій миші реєструються подвійні клацання, коли ви хочете клацнути один раз, хоча ви вже збільшили час очікування на подвійне клацання, можливо, проблему пов'язано із апаратною частиною миші. Спробуйте скористатися іншою мишею, переконавшись, що вона працює належним чином. Крім того, можете спробувати з'єднати вашу мишу із іншим комп'ютером і подивитися, чи виникають проблеми і на ньому.</p>

  <note>
    <p>Значення цього параметра впливає на роботу як миші, так і клавіатури, а також будь-якого координатного пристрою.</p>
  </note>

</page>
