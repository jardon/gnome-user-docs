<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="uk">

  <info>
    <link type="guide" xref="prefs-display" group="#first"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Наталія Рус Лейва (Natalia Ruz Leiva)</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зміна роздільної здатності екрана та його орієнтації (обертання).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Зміна роздільної здатності або орієнтації екрана</title>

  <p>Ви можете змінити масштаб (або деталізацію) показу на екрані шляхом зміни <em>роздільної здатності</em>. Змінити напрямок показу зображення (наприклад, якщо ви повернули дисплей) можна за допомогою зміни <em>обертання</em>.</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Дисплеї</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Дисплеї</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Якщо ви працюєте із декількома дисплеями, і ці дисплеї не синхронізовано за зображенням, на кожному з дисплеїв можна встановлювати власні параметри зображення. Виберіть дисплей в області попереднього перегляду.</p>
    </item>
    <item>
      <p>Виберіть орієнтацію, роздільну здатність або масштабування і частоту оновлення зображення.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Застосувати</gui>. Нові параметри буде застосовано на 20 секунд, після чого система повернеться до попередніх параметрів. Так зроблено для випадків, коли ви не зможете працювати із новими параметрами, — попередні параметри буде автоматично відновлено. Якщо нові параметри працюють добре, натисніть кнопку <gui>Зберегти зміни</gui>.</p>
    </item>
  </steps>

<section id="orientation">
  <title>Орієнтація</title>

  <p>На деяких пристроях ви можете фізично обертати екран у декількох напрямках. Натисніть пункт <gui>Орієнтація</gui> на панелі і виберіть один з варіантів — <gui>Альбомна</gui>, <gui>Портретна праворуч</gui>, <gui>Портретна ліворуч</gui> або <gui>Альбомна перевернута</gui>.</p>

  <note style="tip">
    <p>Якщо ваш пристрій виконує обертання екрана автоматично, ви можете зафіксувати поточне обертання за допомогою кнопки <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">обертання заблоковано</span></media> у нижній частині <gui xref="shell-introduction#systemmenu">меню системи</gui>. Щоб розблокувати обертання, натисніть кнопку <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">обертання розблоковано</span></media>.</p>
  </note>

</section>

<section id="resolution">
  <title>Роздільність</title>

  <p>Роздільна здатність — кількість пікселів (крапок на екрані) у кожному з напрямків показу. У кожної роздільної здатності є <em>співвідношення розмірів</em>, відношення ширини до висоти. Для широкоекранних дисплеїв використовують співвідношення розмірів 16∶9, а для традиційних — 4∶3. Якщо ви виберете роздільну здатність, яка не відповідає співвідношенню розмірів вашого дисплея, на екрані з'являться чорні смуги, для уникнення викривлення — ви побачите чорні прямокутники у верхній і нижній або лівій і правій частинах екрана.</p>

  <p>Ви можете вибрати бажану роздільну здатність зі спадного списку <gui>Роздільна здатність</gui>. Якщо ви виберете неправильну роздільну здатність для вашого екрана, зображення може <link xref="look-display-fuzzy">стати розмитим або піксельованим</link>.</p>

</section>

<section id="native">
  <title>Природна роздільна здатність</title>

  <p><em>Природною роздільною здатністю</em> екрана ноутбука або рідкокристалічного екрана є та роздільна здатність, яка працює на ньому найкраще: пікселі у відеосигналі точно відповідають пікселям на екрані. Якщо екрану наказати показувати зображення із іншою роздільною здатністю, для показу пікселів потрібна буде інтерполяція, що спричинить втрату якості зображення.</p>

</section>

<section id="refresh">
  <title>Частота оновлення</title>

  <p>Частота оновлення є кількістю дій з малювання або оновлення зображення на екрані протягом секунди.</p>
</section>

<section id="scale">
  <title>Масштаб</title>

  <p>Параметр масштабу збільшує розмір показаних на екрані об'єктів для відповідності щільності пікселів вашого дисплея і спрощення читання тексту на зображенні. Виберіть <gui>100%</gui> або <gui>200%</gui>.</p>

</section>

</page>
