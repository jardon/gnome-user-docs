<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="uk">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Змініть тривалість очікування до блокування екрана у параметрах <gui>Блокування екрана</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Екран блокується надто швидко</title>

  <p>Якщо ви полишите ваш комп'ютер на декілька хвилин, екран буде автоматично заблоковано: вам доведеться ввести пароль, щоб мати змогу почати працювати знову. Так зроблено з міркувань безпеки (щоб ніхто не міг втрутитися у вашу роботу, якщо ви полишите комп'ютер), але блокування може бути надокучливим, якщо відбувається надто швидко.</p>

  <p>Щоб система чекала довше, перш ніж автоматично блокувати екран, виконайте такі дії:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слів <gui>Блокування екрана</gui>.</p>
    </item>

    <item>
      <p>Натисніть пункт <gui>Блокування екрана</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Якщо увімкнено <gui>Автоматичне блокування екрана</gui>, ви можете змінити значення у спадному списку <gui>Затримка автоматичного блокування екрана</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Якщо ви хочете, щоб система ніколи не блокувала екран автоматично, переведіть перемикач <gui>Автоматичне блокування екрана</gui> у стан «вимкнено».</p>
  </note>

</page>
