<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>Александре Франке (Alexandre Franke)</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Давид Фаур (David Faour)</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ознайомтеся із тим, як вийти з вашого облікового запису користувача шляхом виходу з облікового запису, перемикання користувачів тощо.</desc>
    <!-- Should this be a guide which links to other topics? -->
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Вихід, вимикання та перемикання користувачів</title>

  <p>Після завершення користування вашим комп'ютером ви можете вимкнути комп'ютер, призупинити його роботу (для заощадження енергії) або лишити його увімкненим і вийти з системи.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>Вихід або перемикання користувачів</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Меню користувача</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Меню користувача</p>
      </media>
    </if:when>
  </if:choose>

  <p>Щоб передати комп'ютер у користування іншим користувачам, ви можете або вийти з власного облікового запису, або лишити ваш сеанс працювати і просто перемкнути користувача. Якщо ви перемкнете користувача, усі ваші програми продовжать працювати, і усе у сеансі лишатиметься таким, яким ви його мали у момент виходу з облікового запису.</p>

  <p>Щоб <gui>Вийти</gui> або <gui>Перемкнути користувача</gui>, натисніть кнопку <link xref="shell-introduction#systemmenu">меню системи</link> у правій частині верхньої панелі, натисніть кнопку <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media> і виберіть відповідний пункт.</p>

  <note if:test="!platform:gnome-classic">
    <p>Пункти <gui>Вийти</gui> і <gui>Перемкнути користувача</gui> буде показано у меню, лише якщо у вашій системі декілька облікових записів користувачів.</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p>Пункт <gui>Змінити користувача</gui> буде показано, лише якщо у вашій системі є декілька облікових записів користувачів.</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>Блокування екрана</title>

  <p>Якщо ви полишаєте комп'ютер на короткий проміжок часу, вам слід заблокувати ваш екран, щоб запобігти доступу сторонніх осіб до ваших файлів і запуску програм. Коли ви повернетесь до комп'ютера, вам буде показано <link xref="shell-lockscreen">екран блокування</link>. Введіть ваш пароль, щоб знову увійти до облікового запису. Якщо ви не заблокуєте ваш екран власноруч, система автоматично виконає цю дію, щойно мине визначений період бездіяльності.</p>

  <p>Щоб заблокувати екран, натисніть кнопку меню системи у правій частині верхньої панелі і натисніть кнопку <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>.</p>

  <p>Якщо екран заблоковано, інші користувачі можуть входити до власних облікових записів. Для цього достатньо натиснути кнопку <gui>Увійти як інший користувач</gui> у нижній правій частині екрана входу до системи. Ви можете перемкнутися на вашу стільницю, коли інші користувачі завершать роботу.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>Призупинити</title>

  <p>Щоб заощадити енергію, призупиніть роботу вашого комп'ютера, якщо ви ним не користуєтеся. Якщо ви користуєтеся ноутбуком, система, типово, призупиняє роботу вашого комп'ютера автоматично, коли ви закриваєте кришку. Стан пам'яті вашого комп'ютера буде збережено, а більшість функцій комп'ютера буде вимкнено. Під час присипляння комп'ютер споживатиме дуже мало енергії.</p>

  <p>Щоб призупинити роботу вашого комп'ютера вручну, натисніть кнопку меню системи у правій частині верхньої панелі, натисніть кнопку <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media> і виберіть пункт <gui>Призупинити</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>Вимикання або перезапуск</title>

  <p>Якщо ви хочете вимкнути ваш комп'ютер повністю або виконати повний перезапуск системи, натисніть кнопку меню системи у правій частині верхньої панелі, натисніть кнопку <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media> і виберіть пункт <gui>Перезапустити…</gui> або <gui>Вимкнути…</gui>.</p>

  <p>Якщо до системи увійшли інші користувачі, вам може бути заборонено вимикати або перезапускати комп'ютер, оскільки ці дії призведуть до завершенні їхніх сеансів роботи. Якщо ви є користувачем-адміністратором, система може попросити вас ввести пароль для вимикання комп'ютера.</p>

  <note style="tip">
    <p>Потреба у вимиканні комп'ютера може виникнути, якщо вам потрібно його кудись перенести, а живлення від акумулятора є недоступним, або якщо заряд акумулятора є низьким чи акумулятор не тримає заряду. Вимкнений комп'ютер також споживає <link xref="power-batterylife">менше енергії</link>, ніж приспаний комп'ютер.</p>
  </note>

</section>

</page>
