<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-tiled" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Максимізація двох вікон, які розташовано поруч.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>


<title>Мозаїчні вікна</title>

  <p>Ви можете максимізувати вікно лише на лівому або правому краю екрана, що уможливить для вас розташування двох вікон поруч одне із одним для швидкого перемикання між ними.</p>

  <p>Щоб максимізувати (розгорнути) вікно уздовж краю екрана, захопіть вказівником смужку заголовка і перетягніть її ліворуч або праворуч, аж доки не буде підсвічено половину екрана. За допомогою клавіатури те саме можна зробити утримуючи натиснутою клавішу <key xref="keyboard-key-super">Super</key> і натискаючи клавіші <key>←</key> і <key>→</key>.</p>

  <p>Щоб відновити початковий розмір вікна, перетягніть його від краю екрана або скористайтеся тим самим клавіатурним скороченням, яким ви скористалися для максимізації вікна.</p>

  <note style="tip">
    <p>Натисніть і утримуйте клавішу <key>Super</key> і перетягніть вікно за будь-яку точку, щоб пересунути його.</p>
  </note>

</page>
