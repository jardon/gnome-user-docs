<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Робочі простори — спосіб групування вікон на вашій стільниці.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Що таке робочий простір і як він допоможе мені?</title>

  <p if:test="!platform:gnome-classic">Робочими просторами називають групування вікон у вашому стільничному середовищі. Ви можете створити декілька робочих просторів, які працюватимуть як віртуальні стільниці. Робочі простори мають зменшити захаращеність стільниці із зробити навігацію нею простішою.</p>

  <p if:test="platform:gnome-classic">Робочими просторами називають групування вікон у вашому стільничному середовищі. Ви можете користуватися декількома робочими просторами, які працюватимуть як віртуальні стільниці. Робочі простори мають зменшити захаращеність стільниці із зробити навігацію нею простішою.</p>

  <p>Робочими просторами можна скористатися для упорядкування вашої роботи. Наприклад, ви можете зібрати усі вікна, пов'язані із спілкуванням, зокрема вікна програм для роботи з електронною поштою або миттєвого обміну повідомленнями, на одному робочому просторі, а вікна робочих програм — на іншому. Вікно програми для керування фонотекою може розташовуватися на третьому робочому просторі.</p>

<p>Користування робочими просторами:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">На панелі <gui xref="shell-introduction#activities">Діяльностей</gui> ви можете здійснювати горизонтальну навігацію між робочими просторами.</p>
    <p if:test="platform:gnome-classic">Клацніть на кнопці у нижній лівій частині екрана у списку вікон або натисніть клавішу <key xref="keyboard-key-super">Super</key>, щоб відкрити огляд <gui>Діяльності</gui>.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Якщо вже використовується декілька робочих просторів, панель <em>вибору робочого простору</em> буде показано між полем для пошуку і списком вікон. На ній буде показано пункти поточних використаних робочих просторів і додатковий пункт порожнього робочого простору.</p>
    <p if:test="platform:gnome-classic">У нижньому правому куті ви побачите чотири прямокутники. Це засіб вибору робочого простору.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">Щоб додати робочий простір, перетягніть і скиньте вікно з наявного робочого простору на пункт порожнього робочого простору на панелі вибору робочого простору. У створеному рбочому просторі буде розташовано скинуте вами вікно, а поряд із його пунктом з'явиться новий пункт порожнього робочого простору.</p>
    <p if:test="platform:gnome-classic">Перетягніть і скиньте вікно з вашого поточного робочого простору на пункт порожнього робочого простору на панелі вибору робочого простору. У створеному рбочому просторі буде розташовано скинуте вами вікно.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Щоб вилучити робочий простір, просто закрийте усі його вікна або пересуньте їх до інших робочих просторів.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">У системі завжди є принаймні один робочий простір.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>Засіб вибору робочого простору</p>
    </media>

</page>
