<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="bluetooth-send-file" xml:lang="uk">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="files-share"/>

    <revision pkgversion="3.8" date="2013-05-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Надання файлів у спільне користування для пристроїв Bluetooth, зокрема для вашого телефону.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Надсилання файлів на пристрій Bluetooth</title>

  <p>Ви можете надсилати файли на з'єднані пристрої Bluetooth, зокрема на мобільні телефони або інші комп'ютери. Пристрої деяких типів не дозволяють передавання файлів або передавання файлів певних типів. Ви можете надсилати файли за допомогою вікна параметрів Bluetooth.</p>

  <note style="important">
    <p><gui>Надіслати файли</gui> не працює для непідтримуваних пристроїв, зокрема телефонів iPhone.</p>
  </note>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Bluetooth</gui></p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Bluetooth</gui> для відкриття панелі.</p>
    </item>
    <item>
      <p>Переконайтеся, що Bluetooth увімкнено: перемикач на смужці заголовка має бути встановлено у стан «увімкнено».</p>
    </item>
    <item>
      <p>У списку <gui>Пристрої</gui> виберіть пристрій, на який слід надіслати файли. Якщо бажаний пристрій не показано як <gui>З'єднаний</gui> у списку, вам слід <link xref="bluetooth-connect-device">з'єднатися</link> із ним.</p>
      <p>З'явиться панель, яка є специфічною для зовнішнього пристрою.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Надіслати файли…</gui> — у відповідь буде відкрито вікно вибору файлів.</p>
    </item>
    <item>
      <p>Виберіть файл, який ви хочете надіслати, і натисніть кнопку <gui>Вибрати</gui>.</p>
      <p>Щоб надіслати декілька файлів у теці, утримуйте натиснутою клавішу <key>Ctrl</key> під час вибору кожного з файлів.</p>
    </item>
    <item>
      <p>Власний пристрою-отримувача зазвичай має натиснути кнопку для прийняття файла. У вікні <gui>Передавання файлів Bluetooth</gui> буде показано смужку поступу. Натисніть кнопку <gui>Закрити</gui>, коли передавання файлів буде завершено.</p>
    </item>
  </steps>

</page>
