<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="uk">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Натисніть <keyseq><key>Super</key><key>Tab</key></keyseq>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Перемикання між вікнами</title>

  <p>Переглянути список усіх запущених програм, які мають графічний інтерфейс, можна за допомогою <em>засобу перемикання вікон</em>. Він робить перемикання між завданнями однокроковим процесом і надає повну картину щодо запущених програм.</p>

  <p>З робочого простору:</p>

  <steps>
    <item>
      <p>Натисніть комбінацію клавіш <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>, щоб система показала <gui>перемикач вікон</gui>.</p>
    </item>
    <item>
      <p>Відпустіть клавішу <key xref="keyboard-key-super">Super</key>, щоб вибрати наступне (підсвічене) вікно у списку перемикача вікон.</p>
    </item>
    <item>
      <p>Або, утримуючи натиснутою клавішу <key xref="keyboard-key-super">Super</key>, натискайте клавішу <key>Tab</key>, щоб циклічно переходити списком відкритих вікон, або комбінацію клавіш <keyseq><key>Shift</key><key>Tab</key></keyseq> для циклічного переходу у зворотному напрямку.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">Для доступу до усіх відкритих вікон та перемикання між ними ви також можете скористатися списком вікон на нижній панелі.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>Вікна у засобі перемикання вікон згруповано за програмами. При клацання програма розгортатиме зображення попереднього перегляду вікон програм, якщо у програми відкрито декілька вікон. Натисніть і утримуйте клавішу <key xref="keyboard-key-super">Super</key> і натискайте клавішу <key>`</key> (або клавішу під клавішею <key>Tab</key>) для покрокового переходу списком.</p>
  </note>

  <p>Крім того, ви можете пересуватися між піктограмами програм у засобі перемикання вікон за допомогою клавіш <key>→</key> і <key>←</key> або вибрати програму клацанням на її піктограми.</p>

  <p>Зображення попереднього перегляду вікон програм у одному вікні можна переглянути за допомогою натискання клавіші <key>↓</key>.</p>

  <p>На панелі огляду <gui>Діяльності</gui> клацніть на пункті <link xref="shell-windows">вікна</link>, щоб перейти до нього і полишити панель огляду. Якщо відкрито декілька <link xref="shell-windows#working-with-workspaces">робочих просторів</link>, ви можете клацнути на будь-якому з пунктів робочих просторів, щоб переглянути відкриті у ньому вікна.</p>

</page>
