<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="uk">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Налаштовування додаткового монітора.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>З'єднання ще одного монітора із вашим комп'ютером</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Налаштовування додаткового монітора</title>
  <p>Щоб налаштувати додатковий монітор, з'єднайте цей монітор із вашим комп'ютером. Якщо ваша система не розпізнає його негайно, вам варто скоригувати його параметри:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Дисплеї</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Дисплеї</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>На діаграмі упорядковування дисплеїв перетягніть дисплеї у бажані для вас відносні позиції.</p>
      <note style="tip">
        <p>Коли активною є панель <gui>Дисплеї</gui>, числа на діаграмі показано у верхній лівій частині кожного з дисплеїв.</p>
      </note>
    </item>
    <item>
      <p>Натисніть пункт <gui>Основний дисплей</gui> для вибору основного дисплея.</p>

      <note>
        <p>Основним дисплеєм є дисплей, на якому показано <link xref="shell-introduction">верхню панель</link> і де показано огляд <gui>Діяльності</gui>.</p>
      </note>
    </item>
    <item>
      <p>Натисніть пункт кожного з моніторів у списку і виберіть орієнтацію, роздільну здатність або масштабування і частоту оновлення зображення.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui>Застосувати</gui>. Нові параметри буде застосовано на 20 секунд, після чого система повернеться до попередніх параметрів. Так зроблено для випадків, коли ви не зможете працювати із новими параметрами, — попередні параметри буде автоматично відновлено. Якщо нові параметри працюють добре, натисніть кнопку <gui>Зберегти зміни</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Режими показу</title>
    <p>Для двох екранів доступні такі режими показу:</p>
    <list>
      <item><p><gui>З'єднання дисплеїв:</gui> краї екранів з'єднуються, щоб об'єкти можна було пересувати з одного дисплея на інший.</p></item>
      <item><p><gui>Віддзеркалля:</gui> ті самі дані буде показано до двох дисплеях із однаковою роздільною здатністю і орієнтацією на обох.</p></item>
    </list>
    <p>Якщо потрібен лише один дисплей, наприклад, зовнішній монітор, який з'єднано із ноутбуком на підставці із закритою кришкою, зайвий монітор можна вимкнути. Натисніть у списку пункт монітора, який слід вимкнути, і переведіть перемикач у стан «вимкнено».</p>
      
</section>

<section id="multiple">

  <title>Додавання декількох моніторів</title>
    <p>Якщо з комп'ютером з'єднано понад два екрани, єдиним режимом є <gui>З'єднання дисплеїв</gui>.</p>
    <list>
      <item>
        <p>Натисніть кнопку для дисплея, який ви хотіли б налаштувати.</p>
      </item>
      <item>
        <p>Перетягніть екрани на бажані відносні позиції.</p>
      </item>
    </list>

</section>
</page>
