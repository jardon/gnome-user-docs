<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="uk">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Філ Булл (Phil Bull)</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Вам слід зняти позначення з пункту <gui>Доступне для усіх користувачів</gui> у параметрах з'єднання мережею.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Інші користувачі не зможуть редагувати з'єднання мережею</title>

  <p>Якщо ви можете редагувати з'єднання мережею, але інші користувачі вашого комп'ютера не можуть цього робити, можливо, ви можете налаштувати з'єднання як <em>доступне для усіх користувачів</em>. У результаті будь-хто на комп'ютері може <em>з'єднуватися</em> за допомогою цього з'єднання.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Мережа</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Мережа</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Виберіть <gui>Wi-Fi</gui> зі списку ліворуч.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">параметри</span></media>, щоб відкрити панель параметрів з'єднання.</p>
    </item>
    <item>
      <p>Виберіть <gui>Засвідчення</gui> на панелі ліворуч.</p>
    </item>
    <item>
      <p>У нижній частині панелі <gui>Засвідчення</gui> позначте пункт <gui>Зробити доступним для інших користувачів</gui>, щоб дозволити користувачам користуватися з'єднанням мережею.</p>
    </item>
    <item>
      <p>Натисніть кнопку <gui style="button">Застосувати</gui>, щоб зберегти зміни.</p>
    </item>
  </steps>

</page>
