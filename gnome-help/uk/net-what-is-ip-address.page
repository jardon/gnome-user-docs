<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-what-is-ip-address" xml:lang="uk">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>IP-адреса подібна до телефонного номера вашого комп'ютера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Що таке IP-адреса?</title>

  <p>«IP-адреса» — скорочення від <em>адреса інтернет-протоколу</em>, і таку адресу має будь-який пристрій, який з'єднано із мережею (зокрема інтернетом).</p>

  <p>IP-адреса подібна до вашого номеру телефону. Ваш номер телефону є унікальним набором цифр, який ідентифікує ваш телефон так, щоб інші люди могли вам зателефонувати. Так само, IP-адреса є унікальним набором цифр, який ідентифікує ваш комп'ютер так, щоб він надсилати дані на інші комп'ютери і отримувати дані з інших комп'ютерів.</p>

  <p>На сьогодні, більшість IP-адрес складаються з чотирьох наборів чисел, які відокремлено крапками. Прикладом IP-адреси є <code>192.168.1.42</code>.</p>

  <note style="tip">
    <p>IP-адреса може бути <em>динамічною</em> або <em>статичною</em>. Динамічні IP-адреси тимчасово призначаються кожного разу, коли комп'ютер з'єднується з мережею. Статичні IP-адреси є фіксованими і не змінюються. Динамічні IP-адреси є поширенішими за статичні — типово, статичні адреси використовуються, лише якщо у них є особлива потреба, зокрема при адмініструванні сервера.</p>
  </note>

</page>
