<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="uk">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Налаштовування комп'ютера на автоматичне присипляння.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Налаштовування автоматичного присипляння</title>

  <p>Ви можете налаштувати комп'ютер на автоматичне присипляння при бездіяльності. Можна визначити різні проміжки бездіяльності для випадків роботи від акумуляторів та від мережі живлення.</p>

  <steps>

    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Живлення</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Живлення</gui>, щоб відкрити панель.</p>
    </item>
    <item>
      <p>У розділі <gui>Параметри заощадження енергії</gui> натисніть пункт <gui>Автоматично призупиняти</gui>.</p>
    </item>
    <item>
      <p>Виберіть <gui>Живлення від батареї</gui> або <gui>Під'єднано</gui>, перемкніть пункт до стану «увімкнено», і виберіть <gui>Затримку</gui>. Налаштувати затримку можна для обох варіантів.</p>

      <note style="tip">
        <p>На стаціонарних комп'ютерах передбачено варіант <gui>Коли бездіяльний</gui>.</p>
      </note>
    </item>

  </steps>

</page>
