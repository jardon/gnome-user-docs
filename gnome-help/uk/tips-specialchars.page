<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="uk">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Андре Клаппер (Andre Klapper)</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Введення символів, яких немає на клавіатурі, зокрема символів інших абеток, математичних символів, емодзі та декоративних символів.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Введення спеціальних символів</title>

  <p>Ви можете вводити і переглядати тисячі символів з більшості систем писемності з усього світу, навіть тих символів, клавіш для яких немає на вашій клавіатурі. На цій сторінці довідки наведено настанови, за допомогою яких ви зможете вводити спеціальні символи.</p>

  <links type="section">
    <title>Способи введення символів</title>
  </links>

  <section id="characters">
    <title>Символи</title>
    <p>За допомогою програми мапи символів можна знаходити і вставляти спеціальні символи, зокрема емодзі (емоційки), безпосереднім переглядом категорій або пошуком за ключовими словами.</p>

    <p>Ви можете запустити <app>Символи</app> з огляду «Діяльності».</p>

  </section>

  <section id="emoji">
    <title>Емодзі</title>
    <steps>
      <title>Вставити емодзі</title>
      <item>
        <p>Натисніть комбінацію клавіш <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Виберіть категорію у нижній частині вікна або почніть вводити опису у поле пошуку.</p>
      </item>
      <item>
        <p>Виберіть емодзі, який слід вставити.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Клавіша компонування</title>
    <p>Клавіша компонування — спеціальна клавіша, після натискання на яку можна натиснути декілька інших клавіш для отримання спеціального символу. Наприклад, щоб ввести літеру з акцентом <em>é</em>, ви можете натиснути клавішу <key>компонування</key>, потім <key>'</key>, потім <key>e</key>.</p>
    <p>На клавіатурах немає спеціалізованих клавіш компонування. Замість стандартних клавіш, ви можете визначити як клавішу компонування якусь з наявних клавіш на клавіатурі.</p>

    <steps>
      <title>Визначення клавіші компонування</title>
      <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
      </item>
      <item>
        <p>Натисніть пункт <gui>Параметри</gui>.</p>
      </item>
      <item>
        <p>Натисніть пункт <gui>Клавіатура</gui> на бічній панелі, щоб відкрити панель.</p>
      </item>
      <item>
        <p>У розділі <gui>Введення спеціальних символів</gui> виберіть пункт <gui>Скомпонувати клавіші</gui>.</p>
      </item>
      <item>
        <p>Увімкніть перемикач для пункту <gui>Скомпонувати клавіші</gui>.</p>
      </item>
      <item>
        <p>Позначте пункт клавіші, яку ви хочете зробити клавішею компонування.</p>
      </item>
      <item>
        <p>Закрийте вікно.</p>
      </item>
    </steps>

    <p>За допомогою клавіші компонування можна вводити багато типових символів. Приклад:</p>

    <list>
      <item><p>Натисніть клавішу <key>компонування</key>, потім клавішу <key>'</key>, потім клавішу літери, над якою слід додати акцент, щоб отримати, наприклад, <em>é</em>.</p></item>
      <item><p>Натисніть клавішу <key>компонування</key>, потім клавішу <key>`</key> (зворотний акцент), потім клавішу літери, до якої слід додати тупий наголос, щоб отримати, наприклад, <em>è</em>.</p></item>
      <item><p>Натисніть клавішу <key>компонування</key>, потім клавішу <key>"</key>, потім клавішу літери, над якою слід додати умляут, щоб отримати, наприклад, <em>ë</em>.</p></item>
      <item><p>Натисніть клавішу <key>компонування</key>, потім клавішу <key>-</key>, потім клавішу літери, над якою слід додати значок довготи, щоб отримати, наприклад, <em>ē</em>.</p></item>
    </list>
    <p>Докладніший список послідовностей для компонування символів наведено на <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">сторінці щодо клавіші компонування у Вікіпедії</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Кодові позиції</title>

  <p>Ви можете ввести будь-який символ Unicode за допомогою лише вашої клавіатури, вказавши числову кодову позицій символу. Будь-який символ ідентифікується чотирицифровою кодовою позицію. Щоб визначити кодову позицію для символу, знайдіть його у програмі <app>Символи</app>. Кодова позиція — чотири символи після <gui>U+</gui>.</p>

  <p>Щоб ввести символ за його кодовою позицією, натисніть комбінацію клавіш <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, потім введіть чотирисимвольний код символу і натисніть клавішу <key>Пробіл</key> або <key>Enter</key>. Якщо ви часто користуєтеся символами, доступ до яких за допомогою інших способів неможливий, вам буде корисним запам'ятовування символьного коду, щоб згодом вводити його швидше.</p>

</section>

  <section id="layout">
    <title>Розкладки клавіатури</title>
    <p>Ви можете наказати вашій клавіатурі поводитися так, як поводиться клавіатура для введення символів абетки іншої мови, незалежно від тих символів, які показано на фізичній клавіатурі. Ви навіть можете швидко перемикатися між різними розкладками клавіатури за допомогою піктограми на верхній панелі. Щоб дізнатися більше про те, як це зробити, ознайомтеся із розділом <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Способи введення</title>

  <p>Спосіб введення розширює попередні способи введення спеціальних символів уможливленням введення символів не лише з клавіатури, а і з будь-яких пристрої для введення. Наприклад, ви можете вводити символи за допомогою жестів вказівником миші або вводити японські ієрогліфи за допомогою клавіатури з символами латинки.</p>

  <p>Щоб вибрати спосіб введення, клацніть правою кнопкою миші на текстовому віджеті і у під меню <gui>Спосіб введення</gui> виберіть бажаний для вас спосіб введення. Типового способу введення не визначено, тому вам слід звернутися до документації із способів введення, щоб ознайомитися із прийомами користування ними.</p>

</section>

</page>
