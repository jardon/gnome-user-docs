<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0 ui/1.0" id="screen-shot-record" xml:lang="uk">
      
  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Девід Кінг (David King)</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Зробіть знімок екрана або створіть запис відео того, що на ньому відбувається.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Знімки екрана та відеозаписи з екрана</title>

  <list>
    <item><p>Створення знімка частини екрана у форматі зображення</p></item>
    <item><p>Надсилання його як файла або вставлення його з буфера обміну даними</p></item>
    <item><p>Зберігання відео ваших дій на екрані</p></item>
  </list>

  <media type="image" src="figures/screenshot-tool.png" width="500"/>

  <p>Ви можете зробити знімок екрана (<em>знімок вікна</em>) або записати відео того, що відбувається на екрані (<em>скрінкаст</em>). Це корисно, якщо ви, наприклад, хочете продемонструвати комусь, як зробити щось на комп'ютері. Знімки вікон та скрінкасти є простими зображеннями і відео — ви можете надіслати їх електронною поштою або оприлюднити їх в інтернеті.</p>

<section id="screenshot">
  <title>Створення знімка екрана</title>

  <steps>
    <item>
      <p>Натисніть клавішу <key>Print</key> або запустіть програму <app>Зробити знімок вікна</app> з панелі огляду <gui xref="shell-introduction#activities">Діяльності</gui>.</p>
    </item>
    <item>
      <p>На накладці створення знімка буде показано елементи керування для вибору ділянки для захоплення, а <media its:translate="no" type="image" src="figures/camera-photo-symbolic.svg"/> вказуватиме на режим знімка екрана (стоп-кадру).</p>
       <note>
         <p>Натисніть кнопку вказівника, щоб включити вказівник до знімка.</p>
       </note>
    </item>
    <item>
      <p>Натисніть ліву кнопку миші і перетягніть вказівник, щоб намалювати область, знімок якої вам потрібен. Скористайтеся елементами керування або вказівником-перехрестям.</p>
    </item>
    <item>
       <p>Щоб захопити позначену ділянку, натисніть велику червону кнопку.</p>
    </item>
    <item>
       <p>Щоб захопити увесь екран, натисніть <gui>Екран</gui>, а потім натисніть велику червону кнопку.</p>
    </item>
    <item>
       <p>Щоб зробити знімок вікна, натисніть <gui>Вікно</gui>. Буде показано огляд усіх відкритих вікон із позначеним активним вікном. Натисніть на потрібному вікні, а потім натисніть велику круглу кнопку.</p>
    </item>
  </steps>

  </section>
  
  <section id="locations">
  <title>Де буде збережено дані?</title>

  <list>
    <item>
      <p>Зображення знімка буде автоматично збережено у теці <file>Зображення/Знімки екрана</file> у домашній теці вашого користувача. Назва файла починатиметься з <file>Знімок екрана</file> і включатиме дату і час, коли знімок було створено.</p>
    </item>
    <item>
      <p>Зображення також буде збережено у буфері обміну даними. Отже, ви зможете одразу вставити його до програми для редагування зображень або поділитися ним у соціальних мережах.</p>
    </item>
    <item>
      <p>Відео з екрана буде автоматично збережено до вашої теки <file>Відео/Трансляції з екрана</file> у домашній теці вашого користувача. Назва файла починатиметься з <file>Запис</file> і включатиме дату і час, коли знімок було створено.</p>
    </item>
  </list>
    
  </section>

<section id="screencast">
  <title>Створення відеозапису з екрана</title>

  <p>Ви можете створити відеозапис того, що відбувається на екрані:</p>

  <steps>
    <item>
      <p>Натисніть клавішу <key>Print</key> або запустіть програму <app>Зробити знімок вікна</app> з панелі огляду <gui xref="shell-introduction#activities">Діяльності</gui>.</p>
    </item>
    <item>
      <p>Натисніть кнопку <media its:translate="no" type="image" src="figures/camera-video-symbolic.svg"/>, щоб перемкнути програму у режим запису відео з екрана.</p>
       <note>
         <p>Натисніть кнопку вказівника, щоб включити вказівник до відео з екрана.</p>
       </note>
    </item>
    <item>
      <p>Виберіть <gui>Вибір</gui> або <gui>Екран</gui>. Якщо вибрали <gui>Вибір</gui>, натисніть ліву кнопку миші і перетягніть вказівник, щоб намалювати область, відео з якої вам потрібне. Скористайтеся елементами керування або вказівником-перехрестям.</p>
    </item>
    <item>
      <p>Натисніть велику червону кнопку, щоб розпочати запис того, що відбувається на екрані.</p>
      <p>Під час записування у верхньому правому куті екрана буде показано червоний індикатор із відліком у секундах.</p>
    </item>
    <item>
      <p>Коли запис має бути завершено, натисніть червоний індикатор або комбінацію <keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq>, щоб зупинити записування.</p>
    </item>
  </steps>

</section>
  
  <section id="keyboard-shortcuts">
    <title>Клавіатурні скорочення</title>

    <p>У режимі створення знімків ви можете скористатися такими клавіатурними скороченнями:</p>

<table rules="rows" frame="top bottom">

  <tr>
    <td><p><key>S</key></p></td>
    <td><p>Вибрати ділянку</p></td>
  </tr>
  <tr>
    <td><p><key>C</key></p></td>
    <td><p>Захопити екран</p></td>
  </tr>
  <tr>
    <td><p><key>W</key></p></td>
    <td><p>Створити знімок вікна</p></td>
  </tr>
  <tr>
    <td><p><key>P</key></p></td>
    <td><p>Перемикання між режимами показу і приховування вказівника миші</p></td>
  </tr>
  <tr>
    <td><p><key>V</key></p></td>
    <td><p>Перемикання між режимом знімка вікна і режимом запису відео</p></td>
  </tr>
  <tr>
    <td><p><key>Enter</key></p></td>
    <td><p>Захоплення, також можна задіяти за допомогою клавіші <key>Пробіл</key> або комбінації клавіш <keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
  </tr>
</table>

    <p>У режимі запису відео з екрана ви можете скористатися такими клавіатурними скороченнями:</p>

<table rules="rows" frame="top bottom">
  <tr>
    <td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
    <td><p>Створити знімок вікна, яке зараз перебуває у фокусі</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
    <td><p>Захоплення усього зображення з екрана</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
    <td><p>Розпочати і зупинити запис відео з екрана</p></td>
  </tr>
</table>

  </section>

</page>
