<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="uk">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Перемикання планшета між режимом планшета і режимом миші.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Встановлення режиму стеження для планшета Wacom</title>

<p><gui>Режим планшета</gui> визначає спосіб прив'язки стила до екрана.</p>

<steps>
  <item>
    <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть введення слова <gui>Планшет Wacom</gui>.</p>
  </item>
  <item>
    <p>Натисніть кнопку <gui>Планшет Wacom</gui>, щоб відкрити панель.</p>
    <note style="tip"><p>Якщо планшет не виявлено, програма надішле вам запит: <gui>Під'єднайте або ввімкніть планшет Wacom</gui>. Натисніть пункт <gui>Bluetooth</gui> на бічній панелі, щоб з'єднати з комп'ютером бездротовий планшет.</p></note>
  </item>
  <item>
    <p>Виберіть між режимом планшета (або абсолютним режимом) та режимом сенсорної панелі (або відносним режимом). Для режиму планшета перемкніть <gui>Режим планшета</gui> у стан «увімкнено».</p>
  </item>
</steps>

  <note style="info"><p>У <em>абсолютному</em> режимі кожну точку на планшеті буде пов'язано із точкою на екрані. Наприклад, верхній лівий кут екрана завжди відповідатиме такій самій точці на планшеті.</p>
  <p>У <em>відносному</em> режимі, якщо ви приймете стило з поверхні планшета, а потім торкнетеся його поверхні стилом у іншій точці, вказівник на екрані залишиться нерухомим. У подібний спосіб працює миша.</p>
  </note>

</page>
