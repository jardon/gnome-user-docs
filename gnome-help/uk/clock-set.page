<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-set" xml:lang="uk">

  <info>
    <link type="guide" xref="clock" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>

    <credit type="author">
      <name>Проєкт з документування GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Гілл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Скористайтеся пунктом <gui>Параметри дати та часу</gui>, щоб змінити дату або час.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Зміна дати та часу</title>

  <p>Якщо дата і час, які показано на верхній панелі є неправильними або записаними у помилковому форматі, ви можете їх змінити:</p>

  <steps>
    <item>
      <p>Відкрийте огляд <gui xref="shell-introduction#activities">Діяльності</gui> і почніть вводити слово <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Параметри</gui>.</p>
    </item>
    <item>
      <p>Натисніть пункт <gui>Дата і час</gui> на бічній панелі, щоб відкрити панель.</p>
    </item>
    <item>
      <p>Якщо перемикач <gui>Автоматичні дата та час</gui> перебуває у стані «увімкнено», дата і час на вашому комп'ютері оновлюватиметься автоматично, якщо комп'ютер з'єднано із інтернетом. Для оновлення дати і часу вручну встановіть для перемикача значення «вимкнено».</p>
    </item> 
    <item>
      <p>Натисніть пункт <gui>Дата і час</gui> і скоригуйте час і дату.</p>
    </item>
    <item>
      <p>Ви можете змінити спосіб показу годин, вибравши варіант <gui>24-ох годинний</gui> або <gui>До / після полудня</gui> для параметра <gui>Формат часу</gui>.</p>
    </item>
  </steps>

  <p>Крім того, у вас може виникнути потреба у <link xref="clock-timezone">встановленні часового поясу вручну</link>.</p>

</page>
