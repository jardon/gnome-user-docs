<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="fr">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ajouter votre photo aux écrans de connexion et d’utilisateur.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Modification de l’image de votre compte</title>

  <p>À la connexion, ou lors d’un changement d’utilisateur, vous verrez s’afficher une liste d’utilisateurs avec leurs images de connexion. Vous pouvez choisir une des images proposées ou une de vos propres images. Vous pouvez même prendre une nouvelle photo de connexion avec votre Webcam.</p>

  <p><link xref="user-admin-explain">Les privilèges administrateur</link> sont requis pour modifier des comptes utilisateurs autres que le votre.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Utilisateurs</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Utilisateurs</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Si vous voulez modifier l’image d’un utilisateur autre que vous, appuyez sur <gui style="button">Déverrouiller</gui> en haut à droite et saisissez votre mot de passe. Choisissez l’utilisateur dans <gui>Autres utilisateurs</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur l’icône représentant un crayon à côté de votre nom. Une galerie vous proposera une sélection d’images de connexion. Si vous aimez l’une d’elles, cliquez dessus pour l’utiliser.</p>
      <list>
        <item>
          <p>Si vous préférez utiliser une image qui se trouve déjà dans l’ordinateur, cliquez sur <gui>Sélectionner un fichier</gui>.</p>
        </item>
        <item>
          <p>Si vous possédez une webcam, vous pouvez prendre une nouvelle photo de connexion immédiatement en cliquant sur <gui>Prendre une photo</gui>. Prenez la photo, puis déplacez et redimensionnez le cadre pour couper ce que vous ne voulez pas conserver. Si vous n’aimez pas la photo que vous avez prise, cliquez sur <gui style="button">Prendre une autre photo</gui> pour recommencer, ou sur <gui>Annuler</gui> pour abandonner.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
