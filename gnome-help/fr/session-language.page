<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-language" xml:lang="fr">

  <info>
    <link type="guide" xref="prefs-language"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-11" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Modifier la langue pour l’interface utilisateur et le texte d’aide.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Modification de la langue</title>

  <p>Vous pouvez utiliser votre bureau et vos applications dans des dizaines de langues différentes, à condition que les paquets de langue correspondants soient installés sur votre ordinateur.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Pays et langue</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Pays et langue</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Langue</gui>.</p>
    </item>
    <item>
      <p>Sélectionnez votre région et votre langue. S’ils ne sont pas dans la liste, cliquez sur <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">…</span></media></gui> au bas de la liste pour les trouver parmi toutes les régions et langues disponibles.</p>
    </item>
    <item>
      <p>Cliquez sur <gui style="button">Sélectionner</gui> pour enregistrer.</p>
    </item>
    <item>
      <p>Vous devez redémarrer la session pour que les changements soient pris en compte. Cliquez sur <gui style="button">Redémarrer</gui>, ou reconnectez-vous manuellement plus tard.</p>
    </item>
  </steps>

  <p>Certaines traductions peuvent être incomplètes et certaines applications peuvent ne pas prendre du tout en charge votre langue. Tout texte non traduit apparaît dans la langue où il a été développé, généralement l’anglais américanisé.</p>

  <p>Votre répertoire personnel contient des dossiers prédéfinis où les applications peuvent stocker entre autres de la musique, des images ou des documents. Ces dossiers ont des noms standard en fonction de la langue sélectionnée. Quand vous vous reconnecterez, il vous sera demandé si vous souhaitez renommer ces dossiers avec les noms standard de la langue sélectionnée. Si vous pensez utiliser la nouvelle langue sélectionnée en permanence, il est conseillé de mettre à jour les noms de dossier.</p>

  <note style="tip">
    <p>S’il existe plusieurs comptes utilisateur sur votre système, il existe une instance distincte du panneau <gui>Pays et langue</gui> pour l’écran de connexion. Cliquez sur le bouton de l’<gui>écran de connexion</gui> situé en haut à droite pour basculer entre les deux instances.</p>
  </note>

</page>
