<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-vpn-connect" xml:lang="fr">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-wired"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Configurer une connexion VPN à un réseau local via Internet.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Connexion à un VPN</title>

<p>Un VPN (<em>Virtual Private Network</em> ou <em>Réseau Privé Virtuel</em>) est une façon de se connecter à un réseau local en passant par Internet. Voici un exemple : admettons que vous vouliez vous connecter au réseau local de votre entreprise alors que vous êtes en déplacement professionnel. Recherchez d’abord une connexion internet disponible (comme à votre hôtel), et connectez-vous ensuite au VPN de votre entreprise. La connexion est identique à celle que vous avez à votre réseau d’entreprise quand vous êtes sur place, sauf qu’elle passe par le réseau Internet de votre hôtel. Les connexions VPN sont normalement <em>chiffrées</em> pour empêcher les personnes ne possédant pas les identifiants de votre réseau local d’y accéder.</p>

<p>Il existe différents types de VPN. Il vous faudra peut-être installer un logiciel supplémentaire selon le type de VPN auquel vous voulez vous connecter. Demandez les détails de la connexion à la personne en charge du VPN et quel <em>client VPN</em> vous devez utiliser. Ouvrez ensuite votre gestionnaire de paquets, recherchez le paquet <app>NetworkManager</app> qui prend en charge votre VPN et installez-le.</p>

<note>
 <p>S’il n’existe pas de paquet « NetworkManager » pour votre type de VPN, vous devrez sans doute télécharger et installer un logiciel client chez le fournisseur du logiciel VPN. Il y a certainement aussi une procédure à suivre pour le rendre opérationnel.</p>
</note>

<p>Pour configurer la connexion VPN :</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Réseau</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Réseau</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Au bas de la liste de gauche, cliquez sur le bouton <gui>+</gui> pour ajouter une nouvelle connexion.</p>
    </item>
    <item>
      <p>Choisissez <gui>VPN</gui> dans la liste de l’interface.</p>
    </item>
    <item>
      <p>Choisissez votre type de connexion VPN.</p>
    </item>
    <item>
      <p>Renseignez les détails de la connexion VPN, et cliquez sur <gui>Ajouter</gui> quand vous avez terminé.</p>
    </item>
    <item>
      <p>Une fois la configuration du VPN terminée, ouvrez le <gui xref="shell-introduction#systemmenu">menu système</gui> dans la partie droite de la barre supérieure, cliquez sur <gui>VPN désactivé</gui> et sélectionnez <gui>Se connecter</gui>. Vous devez saisir votre mot de passe pour établir la connexion. Une fois connecté, une icône en forme de cadenas s’affiche dans la barre supérieure.</p>
    </item>
    <item>
      <p>Si tout se passe bien, vous êtes connecté avec succès au VPN. Sinon, vous devez vérifier les paramètres VPN que vous avez saisi. Pour cela, allez dans le panneau <gui>Réseau</gui> qui vous a servi à créer la connexion, sélectionnez votre connexion VPN dans la liste et appuyez sur le bouton <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">paramètres</span></media> pour vérifier les paramètres.</p>
    </item>
    <item>
      <p>Pour vous déconnecter du VPN, cliquez sur le menu système dans la barre supérieure et cliquez sur <gui>Éteindre</gui> sous le nom de votre connexion VPN.</p>
    </item>
  </steps>

</page>
