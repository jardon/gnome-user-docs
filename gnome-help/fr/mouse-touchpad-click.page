<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="fr">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cliquer, glisser et faire défiler en utilisant des tapotements et des mouvements sur le pavé tactile.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Clic, glissement et défilement avec le pavé tactile</title>

  <p>Vous pouvez cliquer, double-cliquer, glisser et faire défiler en utilisant uniquement le pavé tactile, sans l’aide de boutons accessoires.</p>

  <note>
    <p><link xref="touchscreen-gestures">Les mouvements sur le pavé tactile</link> sont traités séparément.</p>
  </note>

<section id="tap">
  <title>Tapotement pour le clic</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Vous pouvez tapoter le pavé tactile pour cliquer au lieu d’utiliser un bouton.</p>

  <list>
    <item>
      <p>Pour cliquer, tapotez sur le pavé tactile.</p>
    </item>
    <item>
      <p>Pour double-cliquer, tapotez deux fois.</p>
    </item>
    <item>
      <p>Pour faire glisser un objet, tapotez deux fois sans lever le doigt après le second tapotement. Faites glisser l’objet à l’endroit souhaité, puis levez votre doigt pour le déposer.</p>
    </item>
    <item>
      <p>Si le pavé tactile prend en charge les tapotements à plusieurs doigts, vous pouvez faire un clic droit en tapant simultanément à deux doigts. Sinon, il est quand même nécessaire de faire appel à un bouton matériel pour le clic droit. Consultez <link xref="a11y-right-click"/> pour une méthode de clic droit sans deuxième bouton de souris.</p>
    </item>
    <item>
      <p>Si le pavé tactile prend en charge les tapotements à plusieurs doigts, vous pouvez effectuer un <link xref="mouse-middleclick">clic milieu</link> en tapant simultanément avec trois doigts.</p>
    </item>
  </list>

  <note>
    <p>Lorsque vous tapotez ou glissez avec plusieurs doigts, prenez soin d’éloigner suffisamment les doigts. S’ils sont trop proches les uns des autres, l’ordinateur ne détecte pas toujours qu’il y a plusieurs doigts.</p>
  </note>

  <steps>
    <title>Activer le tapotement pour le clic</title>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Souris et pavé tactile</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris et pavé tactile</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>À la rubrique <gui>Pavé tactile</gui>, assurez-vous que l’option <gui>Pavé tactile</gui> est activée.</p>
      <note>
        <p>La rubrique <gui>Pavé tactile</gui> n’est présente que si le système dispose d’un pavé tactile.</p>
      </note>
    </item>
   <item>
      <p>Activez l’option <gui>Taper pour cliquer</gui> en basculant l’interrupteur.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Défilement à deux doigts</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Il est possible de faire défiler avec le pavé tactile en utilisant deux doigts.</p>

  <p>Avec cette option, le tapotement et le glissement à un doigt fonctionnent comme normalement, mais si vous faites glisser deux doigts à n’importe quel endroit du pavé tactile, l’ordinateur effectue un défilement. Déplacez vos doigts entre le haut et le bas de votre pavé tactile pour faire défiler vers le haut et vers le bas, ou déplacez vos doigts sur le pavé tactile pour faire défiler latéralement. Prenez soin d’espacer suffisamment vos doigts. S’ils sont trop proches, le pavé tactile considère qu’il ne s’agit que d’un unique gros doigt.</p>

  <note>
    <p>Certains pavés tactiles ne prennent pas en charge le défilement à deux doigts.</p>
  </note>

  <steps>
    <title>Activer le défilement à deux doigts</title>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Souris et pavé tactile</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris et pavé tactile</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>À la rubrique <gui>Pavé tactile</gui>, assurez-vous que l’option <gui>Pavé tactile</gui> est activée.</p>
    </item>
    <item>
      <p>Activez l’option <gui>Défilement à deux doigts</gui> en basculant l’interrupteur.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Edge scroll</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Use edge scroll if you want to scroll with only one finger.</p>

  <p>Your touchpad's specifications should give the exact
  location of the sensors for edge scrolling. Typically, the vertical
  scroll sensor is on a touchpad's right-hand side. The horizontal
  sensor is on the touchpad's bottom edge.</p>

  <p>To scroll vertically, drag your finger up and down the right-hand
  edge of the touchpad. To scroll horizontally, drag your finger across
  the bottom edge of the touchpad.</p>

  <note>
    <p>Edge scrolling may not work on all touchpads.</p>
  </note>

  <steps>
    <title>Enable Edge Scrolling</title>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Souris et pavé tactile</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris et pavé tactile</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>À la rubrique <gui>Pavé tactile</gui>, assurez-vous que l’option <gui>Pavé tactile</gui> est activée.</p>
    </item>
    <item>
      <p>Switch the <gui>Edge Scrolling</gui> switch to on.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Défilement naturel</title>

  <p>Vous pouvez glisser un contenu, en utilisant le pavé tactile, comme si vous faisiez glisser un morceau de papier.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Souris et pavé tactile</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Souris et pavé tactile</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>À la rubrique <gui>Pavé tactile</gui>, assurez-vous que l’option <gui>Pavé tactile</gui> est activée.</p>
    </item>
    <item>
      <p>Activez l’option <gui>Défilement naturel</gui> en basculant l’interrupteur.</p>
    </item>
  </steps>

  <note>
    <p>Cette fonction est aussi appelée <em>Défilement inverse</em>.</p>
  </note>

</section>

</page>
