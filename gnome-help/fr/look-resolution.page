<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="fr">

  <info>
    <link type="guide" xref="prefs-display" group="#first"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Modifier la définition de l’écran et son orientation (rotation).</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Modification de la définition ou de l’orientation de l’écran</title>

  <p>Vous pouvez modifier la taille (ou la précision) des éléments qui apparaissent à l’écran en modifiant sa <em>définition</em>. Vous pouvez changer la façon dont les éléments apparaissent (par exemple, si votre écran est pivoté) en modifiant la <em>rotation</em>.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Écrans</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Écrans</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Si vous avez plusieurs écrans et qu’il n’y a pas la même image sur tous les écrans, vous pouvez avoir des réglages différents sur chacun d’eux. Sélectionnez un écran dans la zone d’aperçu.</p>
    </item>
    <item>
      <p>Sélectionnez l’orientation, la définition ou l’échelle, et la fréquence de rafraîchissement.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Appliquer</gui>. Les nouveaux paramètres sont appliqués pendant 20 secondes avant de revenir à l’affichage précédent. De cette façon, si vous ne voyez plus rien avec les nouveaux paramètres, vos anciens paramètres sont automatiquement restaurés. Si vous êtes satisfait des nouveaux paramètres, cliquez sur <gui>Conserver les modifications</gui>.</p>
    </item>
  </steps>

<section id="orientation">
  <title>Orientation</title>

  <p>Sur certains appareils, vous pouvez faire pivoter physiquement l’écran dans de nombreuses directions. Cliquez sur <gui>Orientation</gui> dans le panneau puis choisissez entre <gui>Paysage</gui>, <gui>Portrait sur la droite</gui>, <gui>Portrait sur la gauche</gui>, ou <gui>Paysage (retourné)</gui>.</p>

  <note style="tip">
    <p>Si votre appareil fait pivoter l’écran automatiquement, vous pouvez verrouiller l’orientation actuelle en utilisant le bouton <media its:translate="no" type="image" src="figures/rotation-locked-symbolic.svg"><span its:translate="yes">verrouillage d’orientation</span></media> en bas du <gui xref="shell-introduction#systemmenu">menu système</gui>. Pour déverrouiller, cliquez sur le bouton <media its:translate="no" type="image" src="figures/rotation-allowed-symbolic.svg"><span its:translate="yes">déverrouillage d’orientation</span></media>.</p>
  </note>

</section>

<section id="resolution">
  <title>Définition</title>

  <p>La définition est le nombre de pixels (points sur l’écran) qui peuvent être affichés dans chaque direction. Chaque définition possède un <em>rapport d’affichage</em> largeur sur hauteur. Les écrans larges utilisent un format 16∶9, alors que les écrans traditionnels utilisent un format 4∶3. Si vous choisissez une résolution qui ne correspond pas au rapport d’affichage de votre écran, l’image est entourée de bandes noires pour éviter la distorsion.</p>

  <p>Vous pouvez choisir votre <gui>Définition</gui> dans la liste déroulante. Si votre choix ne convient pas à votre écran, l’image peut paraître <link xref="look-display-fuzzy">brouillée ou pixelisée</link>.</p>

</section>

<section id="native">
  <title>Définition native</title>

  <p>La <em>définition native</em> d’un écran d’ordinateur portable ou d’un moniteur LCD est celle qui fonctionne le mieux : les pixels du signal vidéo s’alignent précisément avec les pixels de l’écran. Lorsque l’écran doit afficher d’autres définitions, une interpolation est nécessaire pour représenter les pixels, entraînant une perte de qualité d’image.</p>

</section>

<section id="refresh">
  <title>Fréquence de rafraîchissement</title>

  <p>La fréquence de rafraîchissement est le nombre de fois par seconde que l’image de l’écran est affichée ou actualisée.</p>
</section>

<section id="scale">
  <title>Échelle</title>

  <p>Le réglage de l’échelle augmente la taille des objets affichés à l’écran pour correspondre à la densité de votre écran, les rendant ainsi plus facile à lire. Choisissez <gui>100 %</gui> ou <gui>200 %</gui>.</p>

</section>

</page>
