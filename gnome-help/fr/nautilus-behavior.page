<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="fr">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Ouvrir des fichiers d’un seul clic, lancer ou afficher des fichiers texte exécutables et définir le comportement de la corbeille.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Préférences du gestionnaire de fichiers</title>
<p>Il est possible de choisir d’ouvrir les fichiers d’un simple ou d’un double-clic, la manière d’exécuter les fichiers texte et le comportement de la corbeille. Cliquez sur le bouton de menu situé dans le coin supérieur droit de la fenêtre, choisissez <gui>Préférences</gui> puis allez à la rubrique <gui>Général</gui>.</p>

<section id="behavior">
<title>Comportement</title>
<terms>
 <item>
  <title><gui>Action pour ouvrir les éléments</gui></title>
  <p>Par défaut, un simple clic sélectionne les fichiers et un double-clic les ouvre. Il est possible de décider d’ouvrir fichiers et dossiers d’un simple clic. Dans ce mode, il est possible de sélectionner un ou plusieurs fichiers en maintenant la touche <key>Ctrl</key> enfoncée.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Fichiers texte exécutables</title>
 <p>Un fichier texte exécutable est un fichier qui contient un programme que vous pouvez lancer (exécuter). Les <link xref="nautilus-file-properties-permissions">permissions du fichier</link> doivent aussi autoriser l’exécution de ce fichier. Les fichiers les plus courants sont les scripts <sys>Shell</sys>, <sys>Python</sys> et <sys>Perl</sys>. Ils se terminent respectivement par les extensions <file>.sh</file>, <file>.py</file> et <file>.pl</file>.</p>

 <p>Les fichiers texte exécutables sont aussi appelés des <em>scripts</em>. Tous les scripts contenus dans le dossier <file>~/.local/share/nautilus/scripts</file> s’affichent dans le menu contextuel du fichier concerné et dans le sous-menu <gui style="menuitem">Scripts</gui>. Lorsqu’un script est lancé depuis un dossier local, tous les fichiers sélectionnés sont collés au script en tant que paramètres. Pour lancer un script sur un fichier :</p>

<steps>
  <item>
    <p>Naviguez vers le dossier désiré.</p>
  </item>
  <item>
    <p>Sélectionnez le fichier souhaité.</p>
  </item>
  <item>
    <p>Faites un clic droit sur le fichier pour ouvrir le menu contextuel et sélectionnez le script à exécuter depuis le menu <gui style="menuitem">Scripts</gui>.</p>
  </item>
</steps>

 <note style="important">
  <p>Un script ne recevra aucun paramètre s’il est exécuté depuis un dossier distant tel qu’un dossier affichant un contenu web ou <sys>ftp</sys>.</p>
 </note>

</section>

</page>
