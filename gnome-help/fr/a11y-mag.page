<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="fr">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Zoom avant sur l’écran pour rendre les choses plus visibles.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Agrandissement d’une zone de l’écran</title>

  <p>L’agrandissement de l’écran est différent de la simple augmentation de la <link xref="a11y-font-size">taille du texte</link>. Cette option agit ici comme une loupe en vous permettant de vous déplacer en effectuant un zoom sur différentes parties de l’écran.</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Accessibilité</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Accessibilité</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Zoom</gui> à la rubrique <gui>Vision</gui>.</p>
    </item>
    <item>
      <p>Activez le <gui>Zoom</gui> en basculant l’interrupteur situé dans le coin supérieur droit de la fenêtre <gui>Options du zoom</gui>.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Vous pouvez maintenant vous déplacer sur l’écran. En déplaçant la souris vers les bords de l’écran, vous déplacez la zone agrandie dans différentes directions, ce qui vous permet de visualiser l’emplacement de votre choix.</p>

  <note style="tip">
    <p>Pour activer rapidement le zoom, cliquez sur l’<link xref="a11y-icon">icône d’accessibilité</link> dans la barre supérieure et sélectionnez <gui>Zoom</gui>.</p>
  </note>

  <p>Vous pouvez modifier l’agrandissement de la loupe, le suivi du curseur de la souris et la position de la partie d’écran agrandie, grâce à l’onglet <gui>loupe</gui> de la fenêtre <gui>Options du zoom</gui>.</p>

  <p>Il est possible d’activer les pointeurs en croix pour vous aider à trouver le pointeur de la souris ou du pavé tactile. Pour les activer et régler leur largeur, leur couleur et leur épaisseur, cliquez sur <gui>Pointeurs en croix</gui> dans l’onglet <gui>Pointeurs en croix</gui> du panneau des paramètres de <gui>Zoom</gui>.</p>

  <p>Vous pouvez passer en vidéo inverse ou <gui>Blanc sur noir</gui>, et régler la luminosité, le contraste et le niveau de couleur de la loupe. La combinaison de ces options est utile aux personnes ayant une mauvaise vue ou un certain degré de photophobie, mais aussi si vous avez affaire à de mauvaises conditions d’éclairage. Sélectionnez l’onglet <gui>Effets de couleurs</gui> dans le panneau des paramètres de <gui>Zoom</gui> pour modifier ces options.</p>

</page>
