<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="fr">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Modifier le nom et l’emplacement d’une imprimante dans les paramètres de l’imprimante.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hydroxyp</mal:name>
      <mal:email>hydroxyp@gmail.com</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Charles Monzat</mal:name>
      <mal:email>charles.monzat@free.fr</mal:email>
      <mal:years>2020-2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Guillaume Bernard</mal:name>
      <mal:email>associations@guillaume-bernard.fr</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>
  
  <title>Modification du nom et de l’emplacement d’une imprimante</title>

  <p>Vous pouvez modifier le nom et l’emplacement d’une imprimante dans les paramètres de l’imprimante.</p>

  <note>
    <p>Vous devez posséder les <link xref="user-admin-explain">privilèges administrateur</link> du système pour modifier le nom et l’emplacement d’une imprimante.</p>
  </note>

  <section id="printer-name-change">
    <title>Modification du nom d’une imprimante</title>

  <p>Si vous souhaitez modifier le nom d’une imprimante, conformez-vous aux étapes suivantes :</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Imprimantes</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Imprimantes</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Selon votre système, vous devrez peut-être cliquer sur le bouton <gui style="button">Déverrouiller</gui> situé dans le coin supérieur droit et saisir votre mot de passe.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">paramètres</span></media> situé à côté de l’imprimante.</p>
    </item>
    <item>
      <p>Sélectionnez <gui style="menuitem">Informations sur l’imprimante</gui>.</p>
    </item>
    <item>
      <p>Saisissez le nouveau nom de l’imprimante dans le champ <gui>Nom</gui>.</p>
    </item>
    <item>
      <p>Fermez la boîte de dialogue.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>Changement de l’emplacement d’une imprimante</title>

  <p>Pour modifier l’emplacement d’une imprimante :</p>

  <steps>
    <item>
      <p>Ouvrez la vue d’ensemble des <gui xref="shell-introduction#activities">Activités</gui> et commencez à saisir <gui>Imprimantes</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Imprimantes</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Selon votre système, vous devrez peut-être cliquer sur le bouton <gui style="button">Déverrouiller</gui> situé dans le coin supérieur droit et saisir votre mot de passe.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">paramètres</span></media> situé à côté de l’imprimante.</p>
    </item>
    <item>
      <p>Sélectionnez <gui style="menuitem">Informations sur l’imprimante</gui>.</p>
    </item>
    <item>
      <p>Saisissez le nouvel emplacement de l’imprimante dans le champ <gui>Emplacement</gui>.</p>
    </item>
    <item>
      <p>Fermez la boîte de dialogue.</p>
    </item>
  </steps>

  </section>

</page>
