<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="pt">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Notebooks dormem quando fecha a tampa, para economizar energia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Por que meu computador desliga quando fecho a tampa?</title>

  <p>Quando fecha a tampa de seu notebook, seu computador será <link xref="power-suspend"><em>suspenso</em></link> de forma a economizar energia. Isso significa que o computador não está exatamente desligado — ele está apenas dormindo. Pode resumi-lo (“acordá-lo”) abrindo a tampa. Se ele não resumir, tente clicar com o rato ou pressionar uma tecla. Se isso ainda não funcionar, pressione o botão de liga/desliga.</p>

  <p>Alguns computadores não conseguem serem suspensos adequadamente, normalmente porque seu hardware não haver suporte completo no sistema operacional (por exemplo, os drivers do Linux estão incompletos). Neste caso, pode descobrir que não consegue resumir seu computador após ter fechado a tampa. Pode tentar <link xref="power-suspendfail">corrigir o problema da suspensão</link> ou pode fazer com que o computador não tente suspender quando fecha a tampa.</p>

<section id="nosuspend">
  <title>Impedir o computador de suspender quando a tampa é fechada</title>

  <note style="important">
    <p>Essas instruções vão apenas funcionar se está usando <app>systemd</app>. Contate sua distribuição para mais informação.</p>
  </note>

  <note style="important">
    <p>Você precisa ter <app>Ajustes</app> instalado em seu computador para alterar essa configuração.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Instalar <app>Ajustes</app></link></p>
    </if:if>
  </note>

  <p>Se não deseja que o computador seja suspendido ao fechar a tampa, pode alterar a configuração para este comportamento.</p>

  <note style="warning">
    <p>Tenha muito cuidado se alterar essa configuração. Alguns notebooks podem sobreaquecer se estiverem ligados com tampa fechada, especialmente se estiverem confinados em lugares fechados, como uma mochila.</p>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Ajustes</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Ajustes</gui> para abrir o aplicativo.</p>
    </item>
    <item>
      <p>Selecione a aba <gui>Geral</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Suspender quando a tampa do notebook é fechada</gui> para desligado.</p>
    </item>
    <item>
      <p>Feche a janela do <gui>Ajustes</gui>.</p>
    </item>
  </steps>

</section>

</page>
