<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="a11y task" version="1.0 if/1.0" id="a11y-screen-reader" xml:lang="pt">

  <info>
    <link type="guide" xref="a11y#vision" group="blind"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Heves</name>
      <email>jsvarova@gnome.org</email>
    </credit>

    <desc>Use o leitor de ecrã <app>Orca</app> para falar a interface com o utilizador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Lendo o ecrã em voz alta</title>

  <p>O leitor de ecrã <app>Orca</app> pode narrar a interface para o utilizador. Dependendo de como instalou seu sistema, pode não ter o Orca instalado. Se não tiver, instale o Orca primeiro.</p>

  <p if:test="action:install"><link style="button" action="install:orca">Instalar Orca</link></p>
  
  <p>Para iniciar o <app>Orca</app> usando o teclado:</p>
  
  <steps>
    <item>
    <p>Pressione <key>Super</key>+<key>Alt</key>+<key>S</key>.</p>
    </item>
  </steps>
  
  <p>Ou para iniciar o <app>Orca</app> usando um rato e teclado:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Acessibilidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Clique em <gui>Leitor de ecrã</gui> na seção <gui>Visão</gui> e, então, ative <gui>Leitor de ecrã</gui> no diálogo.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Ative e desative rapidamente o leitor de ecrã</title>
    <p>Pode ativar e desativar rapidamente o leitor de ecrã clicando no <link xref="a11y-icon">ícone de acessibilidade</link> na barra superior e selecionando <gui>Leitor de ecrã</gui>.</p>
  </note>

  <p>Consulte a <link href="help:orca">Ajuda do Orca</link> para mais informações.</p>
</page>
