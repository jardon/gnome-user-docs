<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="pt">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controla quando as miniatures são usadas para ficheiros.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Preferências de visualização do gestor de ficheiros</title>

<p>The file manager creates thumbnails to preview image, video, and text
files. Thumbnail previews can be slow for large files or over networks, so
you can control when previews are made. Click the menu button in the top-right
of the window, select <gui>Preferences</gui>, then go to the
<gui>Performance</gui> section.</p>

<terms>
  <item>
    <title><gui>Show Thumbnails</gui></title>
    <p>By default, all previews are done for
    <gui>On this computer only</gui>, those on your computer or connected
    external drives. You can set this feature to <gui>All files</gui> or
    <gui>Never</gui>. The file manager can
    <link xref="nautilus-connect">browse files on other computers</link> over
    a local area network or the internet. If you often browse files over a local
    area network, and the network has high bandwidth, you may want to set the
    preview option to <gui>All files</gui>.</p>
  </item>
  <item>
    <title><gui>Count Number of Files in Folders</gui></title>
    <p>Se mostra tamanhos de ficheiros em <link xref="nautilus-list">Colunas de visualização de lista</link> ou <link xref="nautilus-display#icon-captions">legendas de ícones</link>, pastas serão mostradas com uma contagem de quantos ficheiros e pastas elas contém. A contagem de itens em uma pasta pode ser lenta, especialmente para pastas muito grandes ou em uma rede.</p>
    <p>Pode desabilitar ou habilitar este recurso ou habilitá-la somente para ficheiros em seu computador e em unidades externas locais.</p>
  </item>
</terms>
</page>
