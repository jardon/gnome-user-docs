<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-constantfan" xml:lang="pt">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Alguns softwares de controle de ventoinha podem estar faltando, ou seu notebook pode estar quente.</desc>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>A ventoinha do notebook está sempre quente</title>

<p>Se a ventoinha de resfriamento (vulgarmente chamada de “cooler”) está sempre fazendo barulho por estar rápida demais, pode ser que os controles do sistema de resfriamento do seu notebook não possuam suporte muito bom no Linux. Alguns notebooks precisam de software extra para controlar suas ventoinhas de resfriamento de forma eficiente, mas esse software pode não estar instalado (ou não estar disponível no Linux) e, portanto, as ventoinhas ficam funcionando em alta velocidade todo o tempo.</p>

<p>Se este for o seu caso, pode ter que alterar algumas definições ou instalar softwares extras que permitem controle completo da ventoinha. Por exemplo, <link href="http://vaio-utils.org/fan/">vaiofand</link> pode ser instalado para controlar as ventoinhas de alguns notebooks Sony VAIO. Instalar esse software é um processo bem técnico, que é altamente dependente da marca e modelo do notebook, de forma que pode preferir buscar informações específicas sobre como fazê-lo para seu computador.</p>

<p>Também é possível que seu notebook simplesmente produza muito calor. Isso não necessariamente significa que ele está sobreaquecendo; ele pode apenas precisar que a ventoinha funcione em alta velocidade todo o tempo para deixá-lo na temperatura adequada. Se esse é o caso, tem poucas opções senão deixar a ventoinha funcionar em alta velocidade todo o tempo. Em alguns casos, pode comprar acessórios adicionais para resfriamento de seu notebook.</p>

</page>
