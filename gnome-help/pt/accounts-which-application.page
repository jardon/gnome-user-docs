<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="pt">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aplicativos podem usar as contas criadas no <app>Contas on-line</app> e os serviços que eles exploram.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Aplicativos e serviços on-line</title>

  <p>Assim que tiver adicionado uma conta on-line, qualquer aplicativo pode usar aquela conta para qualquer dos serviços disponíveis que não <link xref="accounts-disable-service">desabilitou</link>. Provedores diferentes fornecem serviços diferentes. Essa página lista os serviços diferentes e alguns aplicações que os usam.</p>

  <terms>
    <item>
      <title>Agenda</title>
      <p>O serviço Agenda permite que veja, adicione e edite eventos na agenda on-line. Ele é usado por aplicações como <app>Calendário</app>, <app>Evolution</app> e <app>California</app>.</p>
    </item>

    <item>
      <title>Bate-papo</title>
      <p>O serviço Bate-papo permite que bata-papo com seus contactos em plataformas populares de mensagem instantânea. Ele é usado pelo <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contatos</title>
      <p>O serviço Contatos permite que veja os detalhes publicados de seus contactos em vários serviços. Ele é usado por aplicações como <app>Contatos</app> e <app>Evolution</app>.</p>
    </item>

<!-- See https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/commit/32f35cc07fcbee839978e3630972b67ed55f0da6
    <item>
      <title>Documents</title>
      <p>The Documents service allows you to view your online documents
      such as those in Google docs. You can view your documents using the
      <app>Documents</app> application.</p>
    </item>
-->
    <item>
      <title>Ficheiros</title>
      <p>O serviço Ficheiros adiciona uma localização de ficheiros remota, como se tivesse adicionado uma usando a funcionalidade <link xref="nautilus-connect">Ligar ao servidor</link> no gestor de ficheiros. Pode aceder aos ficheiros remotos usando o gestor de ficheiros, assim como por diálogos de abrir e guardar ficheiros em qualquer aplicação.</p>
    </item>

    <item>
      <title>Correio</title>
      <p>O serviço Correio permite que envie e receba e-mail por um provedor de e-mail como o Google. Ele é usado pelo <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->
<!-- TODO: Not sure what this does in which app. Seems to support e.g. VKontakte.
    <item>
      <title>Music</title>
    </item>
-->
    <item>
      <title>Fotos</title>
      <p>O serviço Fotos permite que veja suas fotos on-line como, por exemplo, as publicadas no Facebook. Pode ver suas fotos usando o aplicativo <app>Fotos</app>.</p>
    </item>

    <item>
      <title>Impressoras</title>
      <p>O serviço Impressoras permite que envie a cópia de um PDF para um provedor por meio de um diálogo de impressão de qualquer aplicações. O provedor pode fornecer serviços de impressão, ou ele pode apenas servir como armazenamento para o PDF, o qual pode ser baixado e impresso posteriormente.</p>
    </item>

  </terms>

</page>
