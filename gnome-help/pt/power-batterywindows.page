<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="pt">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Ajustes do fabricante e diferentes estimativas de tempo da bateria podem ser a causa deste problema.</desc>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Por que eu tenho menos tempo de bateria do que eu tinha no Windows/Mac OS?</title>

<p>Alguns computadores parecem ter um menor tempo de bateria ao usar o Linux do que tinham usando Windows ou Mac OS. Um motivo para isso é que os fabricantes de computador instalam softwares específicos para Windows/Mac OS para otimizar várias definições de hardware/software para um dado modelo de computador. Esses ajustes são geralmente muito específicos, e podem não estar documentados, de forma que inclui-los no Linux é difícil.</p>

<p>Infelizmente, não há forma fácil para aplicar esses ajustes mesmo sem saber exatamente o que eles são. Todavia, pode descobrir que usar alguns <link xref="power-batterylife">métodos de economia de energia</link> ajudam. Se seu computador possui um <link xref="power-batteryslow">processador de velocidade variável</link>, pode descobrir que alterar suas definições também é útil.</p>

<p>Um outro possível motivo para a discrepância é que o método de estimativa de tempo da bateria no Windows/Mac OS é diferente do Linux. O tempo de bateria real poderia ser exatamente o mesmo, mas os diferentes métodos fornecem estimativas de bateria.</p>
	
</page>
