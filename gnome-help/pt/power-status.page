<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="pt">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Exiba o status da bateria e dispositivos conectados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Verificando o status da bateria</title>

  <steps>

    <title>Exiba o status da bateria e dispositivos conectados</title>

    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Energia</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel. The status of
      <gui>Batteries</gui> and known <gui>Devices</gui> is displayed.</p>
    </item>

  </steps>

    <p>Se uma bateria interna for detectada, a seção <gui>Bateria</gui> exibe o status de uma ou mais baterias de notebook. A barra indicadora mostra a percentagem da carga, assim como o tempo até estar completamente carregada se estiver conectado, e o tempo restante quando se está usando a energia da bateria.</p>

    <p>A seção <gui>Dispositivos</gui> exibe o status dos dispositivos conectados.</p>
    
    <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. It can also display the charge as a
    <link xref="power-percentage">percentage</link>.</p>

</page>
