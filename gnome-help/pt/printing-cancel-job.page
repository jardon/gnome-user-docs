<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="pt">

  <info>
    <link type="guide" xref="printing#problems"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cancele um trabalho de impressão ou remova-o da fila.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Cancelando, pausando ou liberando um trabalho de impressão</title>

  <p>Pode cancelar um trabalho de impressão pendente e removê-lo da fila nas definições de impressão.</p>

  <section id="cancel-print-job">
    <title>Cancelando um trabalho de impressão</title>

  <p>Se, acidentalmente, iniciou uma impressão de um documento, pode cancelar a impressão de forma que não precisa gastar qualquer tinta ou papel.</p>

  <steps>
    <title>Como cancelar um trabalho de impressão:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Impressoras</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Clique no botão <gui>Mostrar trabalhos</gui> no canto direito da janela <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Cancele o trabalho de impressão clicando no botão de parar.</p>
    </item>
  </steps>

  <p>Se isso não cancelar o trabalho de impressão como esperava, tente manter pressionado o botão <em>cancelar</em> de sua impressora.</p>

  <p>Como um último recurso, especialmente se tiver um trabalho de impressão grande com muitas páginas que não estão sendo canceladas, remova a bandeja de papel da impressora. A impressora deve perceber que não há papel e vai parar de imprimir. Pode, então, tentar cancelar o trabalho de impressão novamente, ou tentar desligar a impressora e ligar novamente.</p>

  <note style="warning">
    <p>Porém, tenha cuidado para não danificar a impressora ao remover o papel. Se tiver que usar de muita força para remover o papel, provavelmente terá que deixá-lo onde ele está.</p>
  </note>

  </section>

  <section id="pause-release-print-job">
    <title>Pausando e liberando um trabalho de impressão</title>

  <p>Se deseja pausar ou liberar um trabalho de impressão, pode fazê-lo no diálogo de trabalhos nas definições da impressora e clique no botão apropriado.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Impressoras</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Clique no botão <gui>Mostrar trabalhos</gui> na lado direito do diálogo <gui>Impressoras</gui> e pause ou libere o trabalho de impressão conforme sua necessidade.</p>
    </item>
  </steps>

  </section>

</page>
