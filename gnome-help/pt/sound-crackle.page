<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="pt">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifique seus cabos de áudio e os drivers de placa de som.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Escuto estalos e zumbidos quando sons são reproduzidos</title>

  <p>Se escuta estalos ou zumbidos quando há som sendo reproduzido no seu computador, pode estar com problemas com os cabos e conectores de áudio ou um problema com os drivers da placa de som.</p>

<list>
 <item>
  <p>Verifique se os alto-falantes estão conectados corretamente.</p>
  <p>Se os alto-falantes não estiverem totalmente conectados, ou se os encaixou no soquete errado, pode escutar um zumbido.</p>
 </item>

 <item>
  <p>Certifique-se de que o cabo de alto-falante/fone de ouvido não está danificado.</p>
  <p>Cabos e conectores de áudio podem se desgastar gradualmente com o uso. Experimente conectar o cabo ou os fones de ouvido em outro dispositivo de áudio (como um reprodutor de MP3 ou de CD) para verificar se ainda haverá uns sons de estalo. Se houver, pode precisar substituir o cabo ou os fones.</p>
 </item>

 <item>
  <p>Verifique se os drivers de som não estão bons.</p>
  <p>Algumas placas de som não funcionam muito bem no Linux porque elas não têm drivers muito bons. Esse problema é mais difícil de identificar. Tente pesquisar pela marca e modelo da sua placa de som pela Internet, juntamente com o termo “Linux”, para ver se outras pessoas estão tendo o mesmo problema.</p>
  <p>Pode usar o comando <cmd>lspci</cmd> para obter mais informações sobre sua placa de som.</p>
 </item>
</list>

</page>
