<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="pt">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>O recurso de <gui>Clique flutuante</gui> (Clique de permanência) permite que clique ao manter o rato parado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Simulando cliques por flutuação</title>

  <p>Pode clicar ou arrastar simplesmente flutuando o ponteiro do seu rato sobre um controle ou objeto no ecrã. Isso é útil se tem dificuldade em mover o rato e clicar ao mesmo tempo. Este recurso é chamado de <gui>Clique flutuante</gui> ou clique de permanência.</p>

  <p>Quando o <gui>Clique flutuante</gui> está habilitado, pode mover o ponteiro do seu rato sobre um controle, soltar o rato e, então, esperar um momento até que o botão ser clicado por.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Acessibilidade</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Acessibilidade</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui>Assistência de clique</gui> na seção <gui>Apontando e clicando</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Clique flutuante</gui> para ligado.</p>
    </item>
  </steps>

  <p>A janela do <gui>Clique flutuante</gui> abrirá e ficará acima de todas outras janelas. Pode usar ela para escolher qual tipo de clique deveria acontecer quando flutuar. Por exemplo, se selecionar <gui>Clique secundário</gui>, fará o clique com botão direito quando flutuar. Depois fizer clique duplo, clique com botão da direita ou arrastar, retornará automaticamente para cliques.</p>

  <p>Quando flutuar o ponteiro do seu rato sobre um botão, e não movê-lo mais, o ponteiro vai gradualmente mudar de cor. Quando estiver com a cor completamente alterada, o botão será clicado.</p>

  <p>Ajuste a configuração do <gui>Intervalo</gui> para alterar por quanto tempo tem que manter parado o ponteiro do rato até que o clique seja realizado.</p>

  <p>Você não precisa segurar o rato perfeitamente parado, quando estiver flutuando para clicar. O ponteiro pode mover um pouco e mesmo assim clicar após um tempo. Porém, se mover demais, o clique não será realizado.</p>

  <p>Ajuste o <gui>Limiar de movimento</gui>, definindo-o para alterar o quanto o ponteiro pode mover e mesmo assim ser considerado como flutuante.</p>

</page>
