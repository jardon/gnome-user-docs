<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-security-tips" xml:lang="pt">

  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dicas gerais para ter em mente ao usar a Internet.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Mantendo-se seguro na Internet</title>

  <p>Um possível motivo para estar usando Linux é a segurança robusta pela qual é conhecido. Um motivo para o Linux ser relativamente seguro contra malware e vírus decorre do baixo número de pessoas que o usa. Vírus são direcionados para sistemas operacionais populares, como o Windows, que possuem uma base de utilizadores muito grande. O Linux também é muito seguro por causa da sua natureza de código aberto, o que permite a “experts” modificar e melhorar os recursos de segurança incluídos em cada distribuição.</p>

  <p>Apesar das medidas tomadas para assegurar que sua instalação seja segura, há sempre vulnerabilidades. Como um utilizador médio, pode ainda pode estar suscetível a:</p>

  <list>
    <item>
      <p>Phishing (sites ou e-mails que tentam obter informações sensíveis por meio de fraude)</p>
    </item>
    <item>
      <p><link xref="net-email-virus">Encaminhamento de e-mails maliciosos</link></p>
    </item>
    <item>
      <p><link xref="net-antivirus">Aplicativos com intenção maliciosa (vírus)</link></p>
    </item>
    <item>
      <p><link xref="net-wireless-wepwpa">Acesso não autorizado a rede remota/local</link></p>
    </item>
  </list>

  <p>Para se manter seguro on-line, tenha em mente as seguintes dicas:</p>

  <list>
    <item>
      <p>Seja cauteloso com e-mails, anexos ou links que foram enviados por pessoas que não conhece.</p>
    </item>
    <item>
      <p>Se uma oferta de um site é boa demais para ser verdade, ou solicita informações sensíveis que parecem desnecessárias, então pensem duas vezes sobre que informação está enviando e as potenciais consequências se tal informações for comprometida por ladrões de identidade ou outros criminosos.</p>
    </item>
    <item>
      <p>Tenha cuidado ao fornecer <link xref="user-admin-explain">permissões com nível de administrador</link> a quaisquer aplicações, especialmente aqueles que nunca usou antes ou que não são muito conhecidos. Fornecer permissões com nível de administrador para qualquer um ou qualquer coisa coloca seu computador em alto risco a exploração.</p>
    </item>
    <item>
      <p>Make sure you are only running necessary remote-access services.
      Having SSH or RDP running can be useful, but also leaves your computer
      open to intrusion if not secured properly. Consider using a
      <link xref="net-firewall-on-off">firewall</link> to help protect your
      computer from intrusion.</p>
    </item>
  </list>

</page>
