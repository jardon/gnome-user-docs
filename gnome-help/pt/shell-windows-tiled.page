<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-tiled" xml:lang="pt">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Maximize duas janelas lado a lado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>


<title>Janelas lado a lado</title>

  <p>Pode maximizar uma janela no lado esquerdo ou direito do ecrã apenas, permitindo que posicione duas janelas lado a lado para rapidamente alternar entre eles.</p>

  <p>Para maximizar uma janela ao longo de um lado do ecrã, segure a barra de título e arraste-a para o lado esquerdo ou direito até metade do ecrã ser realçada. Usando o teclado, mantenha pressionada a tecla <key xref="keyboard-key-super">Super</key> e pressione a tecla <key>Esquerda</key> ou <key>Direita</key>.</p>

  <p>Para restaurar uma janela para seu tamanho original, arraste-a para longe do lado do ecrã, ou use o mesmo atalho que usou para maximizá-la.</p>

  <note style="tip">
    <p>Mantenha pressionada a tecla <key>Super</key> e arraste-a em qualquer lugar em uma janela para movê-la.</p>
  </note>

</page>
