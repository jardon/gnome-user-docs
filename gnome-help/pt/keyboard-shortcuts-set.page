<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="pt">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Defina e altere atalhos de teclado nas definições de <gui>Teclado</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Configurando atalhos de teclado</title>

<p>Para alterar a tecla ou as teclas a serem pressionadas para um atalho de teclado:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a escrever <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Definições</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>Clique na linha para a ação desejada. A janela <gui>Definir atalho</gui> será mostrada.</p>
    </item>
    <item>
      <p>Mantenha pressionada a combinação de teclas desejada ou pressione <key>Backspace</key> para restaurar, ou ainda pressione <key>Esc</key> para cancelar.</p>
    </item>
  </steps>


<section id="defined">
<title>Atalhos de teclado pré-definidos</title>
  <p>Há uma grande quantidade de atalhos pré-configurados que podem ser alterados, agrupados nestas categorias:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Acessibilidade</title>
  <tr>
	<td><p>Diminuir o tamanho do texto</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Ativar ou desativar alto contraste</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Aumentar o tamanho do texto</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Ativar ou desativar teclado virtual</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Ativar ou desativar o leitor de ecrã</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ativar ou desativar a ampliação</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ampliar</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Reduzir</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Lançadores</title>
  <tr>
	<td><p>Pasta pessoal</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media>, <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> ou <key>Explorador</key></p></td>
  </tr>
  <tr>
	<td><p>Lançar calculadora</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> ou <key>Calculadora</key></p></td>
  </tr>
  <tr>
	<td><p>Lançar cliente de correio</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> ou <key>Correio</key></p></td>
  </tr>
  <tr>
	<td><p>Lançar ajuda do navegador</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Lançar navegador web</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media>, <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> ou <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Pesquisar</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> ou <key>Pesquisar</key></p></td>
  </tr>
  <tr>
	<td><p>Configurações</p></td>
	<td><p><key>Ferramentas</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navegação</title>
  <tr>
	<td><p>Ocultar todas as janelas normais</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the left</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the right</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um monitor abaixo</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um monitor à esquerda</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um monitor à direita</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um monitor acima</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um espaço de trabalho à esquerda</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key> <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela um espaço de trabalho à direita</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela para o último espaço de trabalho</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela para o espaço de trabalho 1</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a janela para o espaço de trabalho 2</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Mover a janela para o espaço de trabalho 3</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Mover a janela para o espaço de trabalho 4</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Alternar aplicações</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar controles do sistema</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar controles do sistema diretamente</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar para o último espaço de trabalho</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar para o espaço de trabalho 1</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar para o espaço de trabalho 2</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Trocar para o espaço de trabalho 3</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Trocar para o espaço de trabalho 4</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
        <td><p>Alternar entre janelas</p></td>
        <td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Alternar janelas diretamente</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar janelas de um aplicativo diretamente</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar janelas de um aplicativo</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Capturas de ecrã</title>
  <tr>
	<td><p>Save a screenshot of a window</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of the entire screen</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Launch the screenshot tool</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
  <tr>
        <td><p>Fazer uma pequena gravação de ecrã</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Som e mídia</title>
  <tr>
	<td><p>Ejetar</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Ejetar)</p></td>
  </tr>
  <tr>
	<td><p>Lançar reprodutor de multimídia</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Mídia do áudio)</p></td>
  </tr>
  <tr>
	<td><p>Microphone mute/unmute</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Próxima faixa</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Próximo áudio)</p></td>
  </tr>
  <tr>
	<td><p>Pausar reprodução</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Pausar o áudio)</p></td>
  </tr>
  <tr>
	<td><p>Reproduzir (ou reproduzir/pausar)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Reproduzir o áudio)</p></td>
  </tr>
  <tr>
	<td><p>Faixa anterior</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Áudio anterior)</p></td>
  </tr>
  <tr>
	<td><p>Parar reprodução</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Parar o áudio)</p></td>
  </tr>
  <tr>
	<td><p>Diminuir volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Abaixar volume do áudio)</p></td>
  </tr>
  <tr>
	<td><p>Mudo</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Sem áudio)</p></td>
  </tr>
  <tr>
	<td><p>Aumentar volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Aumentar volume do áudio)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sistema</title>
  <tr>
        <td><p>Dar foco à notificação ativa</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Bloquear ecrã</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar a janela do Desligar</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Abrir o menu do aplicativo</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restaurar os atalhos de teclado</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Mostrar todos as aplicações</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar a visual geral das atividades</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar a lista de notificação</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar o panorama</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar o prompt de executar comando</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Digitação</title>
  <tr>
  <td><p>Alternar para próxima fonte de entrada</p></td>
  <td><p><keyseq><key>Super</key><key>Espaço</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Alternar para fonte de entrada anterior</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Espaço</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Janelas</title>
  <tr>
	<td><p>Ativar o menu da janela</p></td>
	<td><p><keyseq><key>Alt</key><key>Espaço</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fecha a janela</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Ocultar janela</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Colocar a janela atrás das outras</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Maximizar a janela</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximizar a janela horizontalmente</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Maximizar a janela verticalmente</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Mover a janela</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Elevar a janela para frente das outras</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Elevar janela se ela estiver coberta por outra; caso contrário, baixe-a</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Redimensionar a janela</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaurar janela</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar modo de ecrã inteira</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
	<td><p>Alternar estado de maximização</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alternar janela em todos os espaços de trabalho ou em um</p></td>
	<td><p>Desabilitado</p></td>
  </tr>
  <tr>
        <td><p>Visualizar divisão à esquerda</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Visualizar divisão à direita</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Atalhos personalizados</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p>Selecione <gui>Personalizar atalhos</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Digite um <gui>Nome</gui> para identificar o atalho e um <gui>Comando</gui> para executar um aplicativo. Por exemplo, se desejasse o atalho para abrir o <app>Rhythmbox</app>, poderia chamá-lo de <input>Músicas</input> e usar o comando <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p>Clique em <gui>Adicionar</gui>.</p>
    </item>
  </steps>

  <p>O nome do comando que digitar deveria ser um comando válido do sistemas. Pode verificar que o comando funciona abrindo um Terminal e digitando-o lá. O comando que abre um aplicativo não pode possuir o mesmo nome que o aplicativo.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
