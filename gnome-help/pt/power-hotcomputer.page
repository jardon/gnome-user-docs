<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="pt">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Computadores geralmente ficam quente, mas se eles ficarem quentes demais eles podem sobreaquecer, o que pode ser danoso.</desc>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

<title>Meu computador fica quente demais</title>

<p>A maioria dos computadores ficam quentes após um tempo e alguns podem ficar muito quentes. Isso é normal: é simplesmente parte da forma como o computador se mantém resfriado. Porém, se seu computador fica quente demais, isto pode ser um sinal de que ele está sobreaquecendo, o que potencialmente pode causar danos.</p>

<p>A maioria dos notebooks ficam razoavelmente quente após serem usados por algum tempo. Geralmente não há com que se preocupar — computadores produzem muito calor e notebooks são muito compactos, então eles precisam remover seu calor rapidamente e, consequentemente, sua parte exterior pode esquentar. Porém, alguns notebooks realmente ficam muito quentes, ficando desconfortáveis para uso. Isso normalmente é o resultado de um sistema de resfriamento mal projetado. Em alguns casos, pode obter acessórios adicionais para resfriamento que se encaixam em baixo de seu notebook para fornecer um resfriamento mais eficiente.</p>

<p>Se possui um computador de mesa que está quente ao toque, seu resfriamento pode ser insuficiente. Se isso lhe preocupa, pode comprar ventoinhas extras de resfriamento ou verificar se as ventoinhas de resfriamento e as saídas de ar não estão obstruídas com poeira ou outros materiais. Talvez queira instalar o computador em uma área melhor ventilada — se mantida em espaços confinados (por exemplo, em uma armário), o sistema de resfriamento no computador pode não ser capaz de remover o calor e circular ar frio rápido o suficiente.</p>

<p>Algumas pessoas se preocupam com os riscos à saúde pelo uso de notebooks quentes. Há quem diga que o uso prolongado de um notebook quente em seu colo pode possivelmente reduzir a fertilidade, assim como há relatórios de queimaduras leves (em casos extremos). Se está preocupado com esses problemas em potencial, pode querer consultar um médico. É claro, pode simplesmente não usar o notebook em seu colo.</p>

<p>A maioria dos computadores modernos vão autodesligar se ficarem quentes demais, para evitar que causar dano neles mesmos. Se seu computador fica desligando toda hora, este pode ser o motivo. Se seu computador está sobreaquecendo, ele provavelmente precisa de uma manutenção.</p>

</page>
