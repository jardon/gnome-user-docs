<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="pt">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Luz noturna altera a cor de suas ecrãs de acordo com o hora do dia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tiago Santos</mal:name>
      <mal:email>tiagofsantos81@sapo.pt</mal:email>
      <mal:years>2014, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hugo Carvalho</mal:name>
      <mal:email>hugokarvalho@hotmail.com</mal:email>
      <mal:years>2020, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Ajuste a temperatura de cor de suo ecrã</title>

  <p>Um monitor de computador emite luz azul que contribui para insônia e cansaço visual depois de escurecer. <gui>Luz noturna</gui> muda a cor de suas ecrãs de acordo com a hora do dia, tornando a cor mais quente à noite. Para ativar o <gui>Luz noturna</gui>:</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Telas</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Telas</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Click <gui>Night Light</gui> to open the settings.</p>
    </item>
    <item>
      <p>Ensure the <gui>Night Light</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Under <gui>Schedule</gui>, select <gui>Sunset to Sunrise</gui> to
      make the screen color follow the sunset and sunrise times for your location.
      Select <gui>Manual Schedule</gui> to set the <gui>Times</gui> to a custom schedule.</p>
    </item>
    <item>
      <p>Use o controle deslizante para ajustar <gui>Temperatura de cor</gui> para ser mais ou menos quente.</p>
    </item>
  </steps>
      <note>
        <p>A <link xref="shell-introduction">barra superior</link> mostra quando a <gui>Luz noturna</gui> está ativa. Ela pode ser desabilitada temporariamente a partir do menu do sistema.</p>
      </note>



</page>
