<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="fa">

  <info>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-12"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
      <years>۲۰۱۲</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>عملکرد و فشار فشار قلم وکوم را مشخص کنید.</desc>
  </info>

  <title>پیکربندی قلم</title>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>رایانک وکوم</gui> کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو، بر روی <gui>رایانک وکوم</gui> کلیک کنید.</p>
      <note style="tip"><p>اگر رایانک تشخیص داده نشود، خواسته می‌شود <gui> لطفاً رایانک وکومتان را وصل یا روشن کنید</gui>. برای وصل شدن به رایانکی بی‌سیم، <gui>بلوتوث</gui> را در نوار کناری بزنید.</p></note>
    </item>
    <item>
      <p>There is a section containing settings specific to each stylus,
      with the device name (the stylus class) and diagram at the top.</p>
      <note style="tip"><p>اگر قلم تشخیص داده نشود، از شما خواسته می شود <gui> لطفاً قلم خود را به نزدیکی رایانک ببرید تا پیکربندی شود </gui>.</p></note>
      <p>These settings can be adjusted:</p>
      <list>
        <item><p><gui>Tip Pressure Feel:</gui> use the slider to adjust the
        “feel” between <gui>Soft</gui> and <gui>Firm</gui>.</p></item>
        <item><p>Button/Scroll Wheel configuration (these change to
        reflect the stylus). Click the menu next to each label to select one of
        these functions: Middle Mouse Button Click, Right Mouse Button Click,
        Back, or Forward.</p></item>
     </list>
    </item>
    <item>
      <p>Click <gui>Test Your Settings</gui> in the header bar to pop down a
      sketchpad where you can test your stylus settings.</p>
    </item>
  </steps>

</page>
