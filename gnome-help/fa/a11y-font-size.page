<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-font-size" xml:lang="fa">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-08-04" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>شون مک‌کین</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>مایکل هیل</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>برای سهولت در خواندن متن از قلم‌های بزرگتر استفاده کنید.</desc>
  </info>

  <title>تغییر اندازهٔ متن روی صفحه</title>

  <p>اگر در خواندن متن روی صفحه‌تان دشواری دارید، می‌توانید اندازهٔ قلم را تغییر دهید.</p>

  <steps>
    <item>
      <p>نمای کلی <gui xref="shell-introduction#activities">فعّالیت‌ها</gui> را گشوده و شروع به نوشتن <gui>دسترسی‌پذیری</gui> کنید.</p>
    </item>
    <item>
      <p>برای گشودن تابلو، بر روی <gui> دسترسی‌پذیری</gui> کلیک کنید.</p>
    </item>
    <item>
      <p>در بخش <gui>مشاهده</gui>، کلید <gui>متن بزرگ</gui> را روشن کنید.</p>
    </item>
  </steps>

  <p>هم‌چنین می‌توانید با کلیک روی <link xref="a11y-icon">نقشک دسترسی‌پذیری</link> روی نوار بالایی و گزینش <gui>متن بزرگ</gui>، اندازهٔ متن را تغییر دهید.</p>

  <note style="tip">
    <p>در بسیاری از برنامه‌ها، می‌توانید در هر زمان، اندازهٔ متن را با فردن <keyseq><key>مهار</key><key>+</key></keyseq> افزایش دهید. برای کاهش اندازهٔ متن، <keyseq><key>مهار</key><key>-</key></keyseq> را بزنید.</p>
  </note>

  <p><gui>متن بزرگ</gui> متن را ۱٫۲ بار مقیاس خواهد کرد. برای بزرگ‌تر یا کوچک‌تر کردن اندازهٔ متن، می‌توانید از <app>سیخونک‌ها</app> استفاده‌کنید.</p>

</page>
