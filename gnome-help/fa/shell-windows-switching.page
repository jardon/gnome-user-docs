<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="fa">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>شوبها تیاگی</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>کلیدهای <keyseq><key>سوپر</key><key>جهش</key></keyseq> را بزنید.</desc>
  </info>

<title>تعویض بین پنجره‌ها</title>

  <p>می‌توانید تمامی برنامه‌های در حال اجرایی که یک واسط کاربری گرافیکی دارند را در <em>تعویضگر پنجره</em> ببینید. این کار تعویض بین وظایف را فرایندی تک‌گامه کرده و عکسی کامل از برنامه‌های در حال اجرا فراهم می‌کند.</p>

  <p>از یک فضای کاری:</p>

  <steps>
    <item>
      <p>برای بالا آوردن <em>تعویضگر پنجره</em>، <keyseq><key xref="keyboard-key-super">سوپر</key><key>جهش</key></keyseq> را بزنید.</p>
    </item>
    <item>
      <p>برای گزینش پنجرهٔ بعدی (پررنگ شده) در تعویضگر، <key xref="keyboard-key-super">سوپر</key> را رها کنید.</p>
    </item>
    <item>
      <p>یا این که در حال که کلید <key xref="keyboard-key-super">سوپر</key> را نگه داشته‌اید، برای چرخش میان سیاههٔ پنجره‌های باز، <key>جهش</key> یا برای چرخش معکوس، <keyseq><key>تبدیل</key><key>جهش</key></keyseq> را بزنید.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">هم‌چنین می‌توانید از سیاههٔ پنجره‌ها در نوار پایینی برای دسترسی به تمامی پنجره‌های بازتان و تعویض میانشان استفاده کنید.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>پنجره‌ها در تعویضگر پنجره بر اساس برنامه گروه شده‌اند. پیش‌نماهای برنامه‌هایی با چندین پنجرهٔ هنگام کلیک کردن نشان داده خواهند شد. برای حرکت میان سیاهه، <key xref="keyboard-key-super">سوپر</key> را نگه داشته و <key>`</key> (یا کلید بالای <key>جهش</key>) را بزنید.</p>
  </note>

  <p>You can also move between the application icons in the window
  switcher with the <key>→</key> or <key>←</key> keys, or
  select one by clicking it with the mouse.</p>

  <p>پیش‌نمایش برنامه‌هایی با یک پنجره می‌تواند با کلید <key>↓</key> نشان داده شود.</p>

  <p>از نمای کلی <gui>فعّالیت‌ها</gui> روی یک <link xref="shell-windows">پنجره</link>کلیک کنید تا به آن تعویض کرده و از نمای کلی خارج شوید. اگر چندین <link xref="shell-windows#working-with-workspaces">فضای کاری</link> باز دارید می‌توانید برای دیدن پنجره‌های باز روی هر فضای کاری، رویش کلیک کنید.</p>

</page>
