<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="fa">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>تعلیق، رایانه‌تان را خوابانده تا توان کم‌تری مصرف کند.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>پروژهٔ مستندسازی گنوم</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>اکاترینا گراسیموفا</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>وقتی رایانه‌ام را معلّق می‌کنم، چه می‌شود؟</title>

<p>When you <em>suspend</em> the computer, you send it to sleep. All of your
applications and documents remain open, but the screen and other parts of the
computer switch off to save power. The computer is still switched on though,
and it will still be using a small amount of power. You can wake it up by
pressing a key or clicking the mouse. If that does not work, try pressing the
power button.</p>

<p>Some computers have problems with hardware support which mean that they
<link xref="power-suspendfail">may not be able to suspend properly</link>. It is
a good idea to test suspend on your computer to see if it does work before
relying on it.</p>

<note style="important">
  <title>همواره پیش از تعلیق، کارتان را ذخیره کنید</title>
  <p>You should save all of your work before suspending the computer, just in
  case something goes wrong and your open applications and documents cannot be
  recovered when you resume the computer again.</p>
</note>

</page>
