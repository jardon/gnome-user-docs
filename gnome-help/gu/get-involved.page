<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="gu">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>આ મદદ વિષયો સાથે કેવી રીતે અને ક્યાં સમસ્યાઓને અહેવાલ કરવાનો છે.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>પેટર કોવર</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>આ માર્ગદર્શિકાને સુધારવા માટે ભાગ લો</title>

  <section id="submit-issue">

   <title>Submit an issue</title>

   <p>This help documentation is created by a volunteer community. You are
   welcome to participate. If you notice a problem with these help pages
   (like typos, incorrect instructions or topics that should be covered but
   are not), you can submit a <em>new issue</em>. To submit a new issue, go to
   the <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">issue
   tracker</link>.</p>

   <p>You need to register, so you can submit an issue and receive updates by
   email about its status. If you do not already have an account, click the
   <gui><link href="https://gitlab.gnome.org/users/sign_in">Sign in / Register</link></gui>
   button to create one.</p>

   <p>Once you have an account, make sure you are logged in, then go back to the
   <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">documentation
   issue tracker</link> and click
   <gui><link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues/new">New
   issue</link></gui>. Before reporting a new issue, please
   <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">browse</link>
   for the issue to see if something similar already exists.</p>

   <p>Before submitting your issue, choose the appropriate label in the
   <gui>Labels</gui> menu. If you are filing an issue against this
   documentation, you should choose the <gui>gnome-help</gui> label. If you are
   not sure which component your issue pertains to, do not choose any.</p>

   <p>If you are requesting help about a topic that you feel is not covered,
   choose <gui>Feature</gui> as the label. Fill in the Title and Description
   sections and click <gui>Submit issue</gui>.</p>

   <p>Your issue will be given an ID number, and its status will be updated as
   it is being dealt with. Thanks for helping make the GNOME Help better!</p>

   </section>

   <section id="contact-us">
   <title>અમારો સંપર્ક કરો</title>

   <p>કેવી રીતે દસ્તાવેજીકરણ ટુકડી સાથે સંડોવાવુ તે વિશે વધારે શીખવા માટે GNOME દસ્તાવેજીકરણ મેઇલીંગ યાદીમાં <link href="mailto:gnome-doc-list@gnome.org">ઇમેઇલ</link> ને તમે મોકલી શકો છો.</p>

   </section>
</page>
