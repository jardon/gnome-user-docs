<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="gu">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>જાના સ્વરોવા</name>
      <email>jana.svarova@gmail.com</email>
      <years>૨૦૧૩</years>
    </credit>
    <credit type="editor">
      <name>જીમ કેમ્પબેલ</name>
      <email>jcampbell@gnome.org</email>
      <years>૨૦૧૩</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>પ્રિન્ટર સુયોજનોમાં પ્રિન્ટરનાં નામ અથવા સ્થાનને બદલો.</desc>
  </info>
  
  <title>પ્રિન્ટરનાં નામ અથવા સ્થાનને બદલો</title>

  <p>પ્રિન્ટર સુયોજનોમાં પ્રિન્ટરનાં સ્થાન અથવા નામને તમે બદલી શકો છો.</p>

  <note>
    <p>You need <link xref="user-admin-explain">administrative privileges</link>
    on the system to change the name or location of a printer.</p>
  </note>

  <section id="printer-name-change">
    <title>પ્રિન્ટર નામને બદલો</title>

  <p>જો તમે પ્રિન્ટરનાં નામને બદલવા માંગો તો, નીચેનાં પગલાં લો:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the printer.</p>
    </item>
    <item>
      <p>Select <gui style="menuitem">Printer Details</gui>.</p>
    </item>
    <item>
      <p>Enter a new name for the printer in the <gui>Name</gui> field.</p>
    </item>
    <item>
      <p>Close the dialog.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>પ્રિન્ટર સ્થાનને બદલો</title>

  <p>તમારાં પ્રિન્ટર સ્થાનને બદલવા માટે:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Printers</gui> to open the panel.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the printer.</p>
    </item>
    <item>
      <p>Select <gui style="menuitem">Printer Details</gui>.</p>
    </item>
    <item>
      <p>Enter a new location for the printer in the <gui>Location</gui> field.</p>
    </item>
    <item>
      <p>Close the dialog.</p>
    </item>
  </steps>

  </section>

</page>
