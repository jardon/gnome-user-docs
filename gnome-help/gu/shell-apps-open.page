<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="gu">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>શોભા ત્યાગી</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Launch apps from the <gui>Activities</gui> overview.</desc>
  </info>

  <title>કાર્યક્રમોને શરૂ કરો</title>

  <p if:test="!platform:gnome-classic">Move your mouse pointer to the
  <gui>Activities</gui> corner at the top left of the screen to show the
  <gui xref="shell-introduction#activities">Activities</gui> overview. This is where you
  can find all of your applications. You can also open the overview by pressing
  the <key xref="keyboard-key-super">Super</key> key.</p>
  
  <p if:test="platform:gnome-classic">You can start applications from the
  <gui xref="shell-introduction#activities">Applications</gui> menu at the top
  left of the screen, or you can use the <gui>Activities</gui> overview by
  pressing the <key xref="keyboard-key-super">Super</key> key.</p>

  <p>There are several ways of opening an application once you’re in the
  <gui>Activities</gui> overview:</p>

  <list>
    <item>
      <p>Start typing the name of an application — searching begins instantly.
      (If this doesn’t happen, click the search bar at the top of the screen
      and start typing.) If you don’t know the exact name of an application, try
      to type an related term. Click the application’s icon to start it.</p>
    </item>
    <item>
      <p>Some applications have icons in the <em>dash</em>, the horizontal strip
      of icons at the bottom of the <gui>Activities</gui> overview.
      Click one of these to start the corresponding application.</p>
      <p>જો તમારી પાસે કાર્યક્રમો હોય તો કે જે તમે વારંવાર વાપરો તો, તમે તમારી જાતે <link xref="shell-apps-favorites">ડૅશમાં તેઓને ઉમેરી</link> શકો છો.</p>
    </item>
    <item>
      <p>Click the grid button (which has nine dots) in the dash.
      You will see the first page of all installed applications. To see more
      applications, press the dots at the bottom, above the dash, to view other
      applications. Press on the application to start it.</p>
    </item>
    <item>
      <p>You can launch an application in a separate
      <link xref="shell-workspaces">workspace</link> by dragging its icon from
      the dash, and dropping it onto one of the workspaces. The application will
      open in the chosen workspace.</p>
      <p>You can launch an application in a <em>new</em> workspace by dragging its
      icon to an empty workspace, or to the small gap between two workspaces.</p>
    </item>
  </list>

  <note style="tip">
    <title>ઝડપથી આદેશને ચલાવી રહ્યા છે</title>
    <p>કાર્યક્રમને શરૂ કરવાનો બીજો રસ્તો એ <keyseq><key>Alt</key><key>F2</key></keyseq> ને દબાવવાનો છે, તેનાં <em>આદેશ નામ</em> ને દાખલ કરો, અને પછી <key>Enter</key> કીને દબાવો.</p>
    <p>For example, to launch <app>Rhythmbox</app>, press
    <keyseq><key>Alt</key><key>F2</key></keyseq> and type
    ‘<cmd>rhythmbox</cmd>’ (without the single-quotes). The name of the app is
    the command to launch the program.</p>
    <p>Use the arrow keys to quickly access previously run commands.</p>
  </note>

</page>
