<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="gu">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Look in <guiseq><gui>Settings</gui><gui>Color</gui></guiseq> to add a color profile for your screen.</desc>
  </info>

  <title>કેવી રીતે હું રૂપરેખાઓને ઉપકરણોમાં સોંપુ?</title>

  <p>You may want to assign a color profile for your screen or printer so that
  the colors which it shows are more accurate.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select the device for which you want to add a profile.</p>
    </item>
    <item>
      <p>Click <gui>Add profile</gui> to select an existing profile or import
      a new profile.</p>
    </item>
    <item>
      <p>Press <gui>Add</gui> to confirm your selection.</p>
    </item>
    <item>
      <p>To change the used profile, select the profile you would like to use
      and press <gui>Enable</gui> to confirm your selection.</p>
    </item>
  </steps>

  <p>દરેક ઉપકરણ પાસે તેમાં સોંપેલ ઘણી રૂપરેખાઓ હોઇ શકે છે, પરંતુ ફક્ત એક જ રૂપરેખા <em>મૂળભૂત</em> રૂપરેખા બની શકે છે. મૂળભૂત રૂપરેખા એ વપરાશમાં છે જ્યારે ત્યાં આપમેળે પસંદ કરવા માટે રૂપરેખાને પરવાનગી આપવા વધારાની જાણકારી ન હોય. આ આપોઆપ પસંદગીનું ઉદાહરણ એ હશે જો એક રૂપરેખા એ લીસા પેપર માટે અને બીજા સાદા પેપર માટે બનાવેલ હોય.</p>

  <p>If calibration hardware is connected, the <gui>Calibrate…</gui> button
  will create a new profile.</p>

</page>
