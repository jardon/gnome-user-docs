<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="gu">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>ફીલ બુલ</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>જીમ કેમ્પબેલ</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set up a printer that is connected to your computer, or your local
    network.</desc>
  </info>

  <title>સ્થાનિક પ્રિન્ટરને સુયોજિત કરો</title>

  <p>Your system can recognize many types of printers automatically once they
  are connected. Most printers are connected with a USB cable that attaches to
  your computer, but some printers connect to your wired or wireless
  network.</p>

  <note style="tip">
    <p>If your printer is connected to the network, it will not be set up
    automatically – you should add it from the <gui>Printers</gui> panel in
    <gui>Settings</gui>.</p>
  </note>

  <steps>
    <item>
      <p>ખાતરી કરો કે પ્રિન્ટર ચાલુ છે.</p>
    </item>
    <item>
      <p>અનૂકુળ કેબલ મારફતે તમારી સિસ્ટમમાં પ્રિન્ટરને જોડો. તમે ડ્રાઇવરો માટે સિસ્ટમ શોધ તરીકે પ્રવૃત્તિ જોઇ શકો છો, અને તમે તેઓને સ્થાપિત કરવા માટે સત્તાધિકરણ કરવા માટે પૂછી શકો છો.</p>
    </item>
    <item>
      <p>સંદેશો દેખાશે જ્યારે સિસ્ટમ પ્રિન્ટરને સ્થાપિત કરવાનું સમાપ્ત કરે. ચકાસણી પાનાંને છાપવા માટે <gui>ચકાસણી પાનાંને છાપો</gui> ને પસંદ કરો અથવા પ્રિન્ટર સુયોજનમાં વધારાનાં ફેરફારો કરવા માટે <gui>વિકલ્પો</gui>.</p>
    </item>
  </steps>

  <p>If your printer was not set up automatically, you can add it in the
  printer settings:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p><gui>પ્રિન્ટરો</gui> પર ક્લિક કરો.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add…</gui> button.</p>
    </item>
    <item>
      <p>In the pop-up window, select your new printer and press
      <gui style="button">Add</gui>.</p>
      <note style="tip">
        <p>If your printer is not discovered automatically, but you know its
        network address, enter it into the text field at the bottom of the
        dialog and then press <gui style="button">Add</gui></p>
      </note>
    </item>
  </steps>

  <p>If your printer does not appear in the <gui>Add Printer</gui> window, you
  may need to install print drivers.</p>

  <p>તમે પ્રિન્ટરને સ્થાપિત કરો પછી, તમે <link xref="printing-setup-default-printer">તમારાં મૂળભૂત પ્રિન્ટર બદલવાની</link> ઇચ્છા રાખી શકો છો.</p>

</page>
