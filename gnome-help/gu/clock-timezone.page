<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="clock-timezone" xml:lang="gu">

  <info>
    <link type="guide" xref="clock"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.16" date="2015-01-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-04-09" status="review"/>
    <revision pkgversion="3.37.3" date="2020-08-05" status="review"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>જીમ કેમ્પબેલ</name>
       <email>jcampbell@gnome.org</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Update your time zone to your current location so that your time is correct.</desc>
  </info>

  <title>તમારો ટાઇમઝોન બદલો</title>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Date &amp; Time</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>If you have the <gui>Automatic Time Zone</gui> switch set to on, your
      time zone should update automatically if you have an internet
      connection and the <link xref="privacy-location">location services
      feature</link> is enabled. To update your time zone manually, set this to
      off.</p>
    </item>
    <item>
      <p>Click <gui>Time Zone</gui>, then select your location on the map or
      search for your current city.</p>
    </item>
  </steps>

  <p>The time will be updated automatically when you select a different
  location. You may also wish to <link xref="clock-set">set the clock
  manually</link>.</p>

</page>
