<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="gu">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>ટિફની એન્ટોપોલસ્કી</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>ઍકાટેરીના ગેરાસીમોવા</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>વપરાશકર્તાઓને દૂર કરો કે જે તમારા કમ્પ્યૂટરને લાંબો સમય વાપરતા નથી.</desc>
  </info>

  <title>વપરાશકર્તા ખાતાને કાઢી નાંખો</title>

  <p>You can <link xref="user-add">add multiple user accounts to your
  computer</link>. If somebody is no longer using your computer, you can delete
  that user’s account.</p>

  <p>તમારે વપરાશકર્તા ખાતાને કાઢવા માટે <link xref="user-admin-explain">સંચાલક અધિકારો</link> ની જરૂર છે.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Click on the user account that you want to delete under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Remove User...</gui> button to delete
      that user account.</p>
    </item>
    <item>
      <p>Each user has their own home folder for their files and settings. You
      can choose to keep or delete the user’s home folder. Click <gui>Delete
      Files</gui> if you are sure they will not be used anymore and you need to
      free up disk space. These files are permanently deleted. They cannot be
      recovered. You may want to back up the files to an external storage device
      before deleting them.</p>
    </item>
  </steps>

</page>
