<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="keyboard-nav" xml:lang="gu">
  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.7.5" version="0.2" date="2013-02-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.20" date="2016-08-13" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
       <name>જુલીટા ઇન્કા</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>૨૦૧૨</years>
    </credit>
    <credit type="editor">
       <name>ઍકાટેરીના ગેરાસીમોવા</name>
       <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>માઉસ વગર કાર્યક્રમો અને ડેસ્કટોપને વાપરો.</desc>
  </info>

  <title>કિબોર્ડ શોધખોળ</title>

  <p>આ પાનું લોકો માટે કિબોર્ડ સંશોધક હોય તો કે જે માઉસ અથવા બીજા નિર્દેશક ઉપકરણને વાપરી શકાતુ નથી, અથવા કે જે શક્ય હોય તેટલું કિબોર્ડને વાપરવા માંગો છો. કિબોર્ડ ટૂંકાણો માટે કે જે બધા વપરાશકર્તાઓ માટે ઉપયોગી છે, તેને બદલે <link xref="shell-keyboard-shortcuts"/> જુઓ.</p>

  <note style="tip">
    <p>જો તમે નિર્દેશક ઉપકરણને વાપરી શકતા નથી, તમે તમારાં કિબોર્ડ પર આંકડાકીય કિપેડની મદદથી માઉસ નિયંત્રણ કરી શકો છો. વધારે વિગતો માટે <link xref="mouse-mousekeys"/> જુઓ.</p>
  </note>

<table frame="top bottom" rules="rows">
  <title>વપરાશકર્તા ઇન્ટરફેસને શોધો</title>
  <tr>
    <td><p><key>Tab</key> and</p>
    <p><keyseq><key>Ctrl</key><key>Tab</key></keyseq></p>
    </td>
    <td>
      <p>વિવિધ નિયંત્રણો વચ્ચે કિબોર્ડ પ્રકાશને ખસેડો. <keyseq><key>Ctrl</key> <key>Tab</key></keyseq> એ નિયંત્રણોનાં જૂથો વચ્ચે ખસેડાય છે, જેમ કે મુખ્ય સમાવિષ્ટમાં બાજુપટ્ટીમાંથી. <keyseq><key>Ctrl</key><key>Tab</key></keyseq>  નિયંત્રણને ભંગ કરી શકે છે કે જે પોતાનીજાતે <key>Tab</key> ને વાપરે છે, જેમ કે લખાણ વિસ્તાર.</p>
      <p>વિપરીત ક્રમમાં ફોકસને ખસેડવા માટે <key>Shift</key> ને પકડી રાખો.</p>
    </td>
  </tr>
  <tr>
    <td><p>તીર કી</p></td>
    <td>
      <p>એકજ નિયંત્રણમાં વસ્તુઓ વચ્ચે પસંદગીને ખસેડો, અથવા સંબંધિત સમૂહ વચ્ચે નિયંત્રણ કરો. સાધનપટ્ટીમાં બટનોને પ્રકાશિત કરવા માટે તીર કીને વાપરો, યાદી અથવા ચિહ્ન દૃશ્યમાં વસ્તુઓને પસંદ કરો, અથવા જૂથમાંથી રેડિયો બટનને પસંદ કરો.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key>તીર કી</keyseq></p></td>
    <td><p>યાદી અથવા ચિહ્ન દૃશ્યમાં, જે વસ્તુને પસંદ કરેલ છે તેને બદલ્યા વગર કિબોર્ડ પ્રકાશનને બીજી વસ્તુમાં ખસેડો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key>તીર કી</keyseq></p></td>
    <td><p>યાદી અથવા ચિહ્ન દૃશ્યમાં, નવી પ્રકાશિત થયેલ વસ્તુ માટે હાલમાં પસંદ થયેલ વસ્તુમાંથી બધી વસ્તુઓને પસંદ કરો.</p>
    <p>In a tree view, items that have children can be expanded or collapsed,
    to show or hide their children: expand by pressing
    <keyseq><key>Shift</key><key>→</key></keyseq>, and collapse by
    pressing <keyseq><key>Shift</key><key>←</key></keyseq>.</p></td>
  </tr>
  <tr>
    <td><p><key>Space</key></p></td>
    <td><p>પ્રકાશિત વસ્તુ જેવી કે બટન, ચેક બોક્સ, અથવા વસ્તુ યાદીને સક્રિય કરો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Space</key></keyseq></p></td>
    <td><p>યાદી અથવા ચિહ્ન દૃશ્યમાં, બીજી વસ્તુઓને પસંદ કર્યા વગર પ્રકાશિત થયેલ વસ્તુને નાપસંદ કરો.</p></td>
  </tr>
  <tr>
    <td><p><key>Alt</key></p></td>
    <td><p><key>Alt</key> કી દબાવી રાખો, <em>એસીલરેટરો</em>: મેનુ વસ્તુઓ, બટનો, અને અન્ય નિયંત્રકો પરના નીચે લીટીવાળા અક્ષરો બહાર પાડવા માટે. નિયંત્રકને સક્રિય કરવા માટે <key>Alt</key> વત્તા નીચે લીટીવાળો અક્ષર દબાવો, એવી જ રીતે જેમ તમે તેના પર ક્લિક કર્યું હોય.</p></td>
  </tr>
  <tr>
    <td><p><key>Esc</key></p></td>
    <td><p>મેનુ, પોપઅપ, સ્વીચર, અથવા સંવાદ વિન્ડોમાંથી બહાર નીકળો.</p></td>
  </tr>
  <tr>
    <td><p><key>F10</key></p></td>
    <td><p>વિન્ડોની મેનુપટ્ટી પર પ્રથન મેનુને ખોલો. મેનુને સ્થળાંતર કરવા માટે તીર કીને વાપરો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="keyboard-key-super">Super</key> <key>F10</key></keyseq></p></td>
    <td><p>ટોચની પટ્ટી પર કાર્યક્રમ મેનુને ખોલો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Shift</key><key>F10</key></keyseq> or</p>
    <p><key xref="keyboard-key-menu">Menu</key></p></td>
    <td>
      <p>વર્તમાન પસંદગી માટે સંદર્ભ મેનુ પર પોપઅપ કરો, જો તમે જમણી ક્લિક કરી હોય તો.</p>
    </td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>F10</key></keyseq></p></td>
    <td><p>ફાઇલ સંચાલકમાં, વર્તમાન ફોલ્ડર માટે સંદર્ભ મેનુને પોપ અપ કરો, જો તમે પાશ્ર્વભાગ પર જમણી ક્લિક કરેલ હોય તો અને કોઇપણ વસ્તુ પર નથી.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>PageUp</key></keyseq></p>
    <p>and</p>
    <p><keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
    <td><p>ટેબ થયેલ ઇન્ટરફેસમાં, ડાબે અથવા જમણે ટેબને ખસેડો.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>ડેસ્કટોપને શોધો</title>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="alt-f1"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-tick"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="ctrl-alt-tab"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="super-updown"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
    <td><p>Cycle through windows in the same application. Hold down the
    <key>Alt</key> key and press <key>F6</key> until the window you want is
    highlighted, then release <key>Alt</key>. This is similar to the
    <keyseq><key>Alt</key><key>`</key></keyseq> feature.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
    <td><p>કામ કરવાની જગ્યા પર બધી ખુલ્લી વિન્ડો મારફતે ઘટનાચક્ર કરો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
    <td><p><link xref="shell-notifications#notificationlist">Open the
    notification list.</link> Press <key>Esc</key> to close.</p></td>
  </tr>
</table>

<table frame="top bottom" rules="rows">
  <title>વિન્ડોને શોધો</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>વર્તમાન વિન્ડોને બંધ કરો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F5</key></keyseq> અથવા <keyseq><key>Super</key><key>↓</key></keyseq></p></td>
    <td><p>Restore a maximized window to its original size. Use
    <keyseq><key>Alt</key><key>F10</key></keyseq> to maximize.
    <keyseq><key>Alt</key><key>F10</key></keyseq> both maximizes and
    restores.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
    <td><p>વર્તમાન વિન્ડોમાં ખસેડો, <keyseq><key>Alt</key><key>F7</key></keyseq> દબાવો, પછી વિન્ડોને ખસેડવા માટે તીર કીને વાપરો. વિન્ડોને ખસેડવાનું સમાપ્ત કરવા માટે <key>Enter</key> ને દબાવો, અથવા તેનાં મૂળભૂત સ્થાનમાં તેને પાછુ લાવવા માટે <key>Esc</key> ને દબાવો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
    <td><p>વર્તમાન વિન્ડોનું નામ બદલો. <keyseq><key>Alt</key><key>F8</key></keyseq> ને દબાવો, પછી વિન્ડોનું માપ બદલવા માટે તીર કીને વાપરો. વિન્ડોનું માપ બદલવાનું સમાપ્ત કરવા માટે <key>Enter</key> ને દબાવો, અથવા તેનાં મૂળભૂત માપમાં તેને પાછુ લાવવા માટે <key>Esc</key> ને દબાવો.</p></td>
  </tr>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-updown"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-left"/>
  <include xmlns="http://www.w3.org/2001/XInclude" href="shell-keyboard-shortcuts.page" xpointer="shift-super-right"/>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F10</key></keyseq> અથવા <keyseq><key>Super</key><key>↑</key></keyseq></p>
    </td>
    <td><p>વિન્ડોને <link xref="shell-windows-maximize">મહત્તમ</link> કરો. મહત્તમ વિન્ડોને તેનાં મૂળભૂત માપમાં લાવવા પુન:સંગ્રહવા માટે Press <keyseq><key>Alt</key><key>F10</key></keyseq> or <keyseq><key>Super</key><key>↓</key></keyseq> દબાવો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
    <td><p>વિન્ડોને ન્યૂનત્તમ કરો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
    <td><p>સ્ક્રીનની ડાબી બાજુની સાથે વિન્ડોને ઊભી રીતે મહત્તમ કરો. તેમાં પહેલાંના માપમાં વિન્ડોને પુન:સંગ્રહવા માટે ફરી દબાવો. બાજુઓને બદલવા માટે <keyseq><key>Super</key><key>→</key></keyseq> દબાવો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
    <td><p>સ્ક્રીનની જમણી બાજુની સાથે વિન્ડોને ઊભી રીતે મહત્તમ કરો. તેમાં પહેલાંના માપમાં વિન્ડોને પુન:સંગ્રહવા માટે ફરી દબાવો. બાજુઓને બદલવા માટે <keyseq><key>Super</key><key>←</key></keyseq> દબાવો.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Space</key></keyseq></p></td>
    <td><p>વિન્ડોમેનુ પર પોપઅપ કરો, જો તમે શીર્ષકપટ્ટી પર જમણી ક્લિક કરેલ હોય.</p></td>
  </tr>
</table>

</page>
