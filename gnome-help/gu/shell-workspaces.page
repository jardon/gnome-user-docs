<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="gu">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>GNOME દસ્તાવેજીકરણ પ્રોજેક્ટ</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>એન્ડ્રે ક્લેપર</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>કામ કરવાની જગ્યા એ તમારાં ડેસ્કટોપ પર વિન્ડોનું જૂથ કરવાની રીત છે.</desc>
  </info>

<title>કામ કરવાની જગ્યા શું છે, અને તે કેવી રીતે મને મદદ કરશે?</title>

  <p if:test="!platform:gnome-classic">કામ કરવાની જગ્યા એ તમારાં ડેસ્કટોપ પર વિન્ડોનાં જૂથનો સંદર્ભ કરે છે. તમે ઘણી કામ કરવાની જગ્યાઓને બનાવી શકો છો, કે જે વર્ચ્યુઅલ ડેસ્કટોપ તરીકે કાર્ય કરે છે. કામ કરવાની જગ્યા ક્લટરને ઘટાડવાનો મતલબ હતો અને ડેસ્કટોપને સ્થળાંતર કરવા માટે સરળ બનાવો.</p>

  <p if:test="platform:gnome-classic">કામ કરવાની જગ્યા એ તમારાં ડેસ્કટોપ પર વિન્ડોનાં જૂથનો સંદર્ભ કરે છે. તમે ઘણી કામ કરવાની જગ્યાઓને વાપરી શકો છો, કે જે વર્ચ્યુઅલ ડેસ્કટોપ તરીકે કાર્ય કરે છે. કામ કરવાની જગ્યા ક્લટરને ઘટાડવાનો મતલબ હતો અને ડેસ્કટોપને સ્થળાંતર કરવા માટે સરળ બનાવો.</p>

  <p>કામ કરવાની જગ્યાઓ તમારાં કામને વ્યવસ્થિત કરવા માટે વાપરી શકાય છે. ઉદાહરણ તરીકે, તમારી પાસે બધી તમારી વાર્તાલાપ વિન્ડો હોઇ શકે છે, જેમ કે ઇમેઇલ અને તમારો વાર્તાલાપ કાર્યક્રમ, એક કાર્ય કરવાની જગ્યા પર, અને કામ કે તમે વિવિધ કાર્ય કરવાની જગ્યા પર કરી રહ્યા છો. તમારું સંગીત સંચાલક ત્રીજી કામ કરવાની જગ્યા હોઇ શકે છે.</p>

<p>કામ કરવાની જગ્યાને વાપરી રહ્યા છે:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, you can
    horizontally navigate between the workspaces.</p>
    <p if:test="platform:gnome-classic">Click the button at the bottom left of
    the screen in the window list, or press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">If more than one workspace is already
    in use, the <em>workspace selector</em> is shown between the search field and
    the window list. It will display currently used workspaces plus an empty workspace.</p>
    <p if:test="platform:gnome-classic">In the bottom right corner, you see four
    boxes. This is the workspace selector.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">To add a workspace, drag and drop a
    window from an existing workspace onto the empty workspace in the workspace
    selector. This workspace now contains the window you have dropped, and a new
    empty workspace will appear next to it.</p>
    <p if:test="platform:gnome-classic">Drag and drop a window from your current
    workspace onto an empty workspace in the workspace selector. This workspace
    now contains the window you have dropped.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>કામ કરવાની જગ્યાને દૂર કરવા માટે, સરળતાથી બધી તેની વિન્ડોને બંધ કરો અથવા બીજી કામ કરવાની જગ્યામાં તેઓને ખસેડો.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">ત્યાં હંમેશા ઓછામાં ઓછી એક કામ કરવાની જગ્યા છે.</p>

    <media its:translate="yes" type="image" src="figures/shell-workspaces.png" width="940" height="291">
        <p>કામ કરવાની જગ્યા પસંદકર્તા</p>
    </media>

</page>
