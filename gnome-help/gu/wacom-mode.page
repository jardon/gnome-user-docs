<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="gu">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>માઇકલ હીલ</name>
      <email>mdhillca@gmail.com</email>
      <years>૨૦૧૨</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ટેબલેટ સ્થિતિ અને માઉસ સ્થિતિ વચ્ચે ટેબલેટને ખસેડો.</desc>
  </info>

  <title>Set the Wacom tablet’s tracking mode</title>

<p><gui>Tablet Mode</gui> determines how the stylus is mapped to the screen.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
    overview and start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>પેનલને ખોલવા માટે <gui>Wacom ટૅબલેટ</gui> પર ક્લિક કરો.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Choose between tablet (or absolute) mode and touchpad (or relative)
    mode. For tablet mode, switch <gui>Tablet Mode</gui> to on.</p>
  </item>
</steps>

  <note style="info"><p><em>ચોક્કસ</em> સ્થિતિમાં, ટેબલેટ પર દરેક બિંદુ એ સ્ક્રીન પર એક બિંદુને માપાંકિત કરે છે. સ્ક્રીનનાં ટોચનાં ડાબે ખૂણે, ઉદાહરણ તરીકે, હંમેશા ટેબલેટ પર એજ બિંદુ સાથે સંકળાયેલ છે.</p>
  <p>In <em>relative</em> mode, if you lift the stylus off the tablet and put it
  down in a different position, the pointer on the screen doesn’t move. This is
  the way a mouse operates.</p>
  </note>

</page>
