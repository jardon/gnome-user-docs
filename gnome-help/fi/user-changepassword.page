<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="fi">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pidä tunnuksesi turvassa vaihtamalla salasanasi säännöllisesti.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Vaihda salasanasi</title>

  <p>Salasanan vaihtaminen säännöllisesti on hyvä idea. Vaihda salasanasi heti, jos luulet toisen henkilön tietävän salasanasi.</p>

  <p>Tarvitset <link xref="user-admin-explain">ylläpitäjän oikeudet</link> muokataksesi muiden käyttäjien tilejä.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Käyttäjät</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Käyttäjät</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>Click the label <gui>·····</gui> next to <gui>Password</gui>. If you
      are changing the password for a different user, you will first need to
      <gui>Unlock</gui> the panel and select the account under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Anna nykyinen salasanasi ja uusi salasanasi. Kirjoita uusi salasana uudelleen <gui>Vahvista uusi salasana</gui> -kenttään.</p>
      <p>You can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
    </item>
    <item>
      <p>Napsauta <gui>Vaihda</gui>.</p>
    </item>
  </steps>

  <p>Varmista, että käytät <link xref="user-goodpassword">hyvää salasanaa</link>. Näin tunnuksesi pysyy varmemmin turvattuna.</p>

  <note>
    <p>When you update your login password, your login keyring password will
    automatically be updated to be the same as your new login password.</p>
  </note>

  <p>Jos unohdat salasanasi, kuka tahansa ylläpito-oikeuksin oleva käyttäjä voi vaihtaa salasanasi.</p>

</page>
