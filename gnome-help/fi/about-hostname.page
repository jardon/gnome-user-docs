<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hostname" xml:lang="fi">

  <info>
    <link type="guide" xref="about" group="hostname"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vaihda nimeä, joka yksilöi järjestelmäsi verkkoyhteyksissä ja Bluetooth-laitteille. </desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Vaihda laitteen nimi</title>

  <p>Järjestelmän yksilöllinen, helposti tunnistettava nimi tekee järjestelmän tunnistamisesta helppoa verkkoyhteyksiä luotaessa tai Bluetooth-laitepareja luotaessa.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Tietoja</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Tietoja</gui> avataksesi paneelin.</p>
    </item>
    <item>
      <p>Valitse <gui>Laitteen nimi</gui> luettelosta.</p>
    </item>
    <item>
      <p>Enter a name for your system, and click
      <gui style="button">Rename</gui>.</p>
    </item>
  </steps>

  <note style="important">
    <p>Although the hostname is changed immediately after the system name has
    been changed, some running programs and services may have to be restarted
    for the change to take effect.</p>
  </note>

</page>
