<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="fi">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Väriprofiileja tarjoavat laitevalmistaja ja lisäksi niitä on mahdollista luoda itse.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Mistä saan väriprofiileja?</title>

  <p>Paras tapa hankkia profiileja on luoda ne itse, vaikkakin tämä tarkoittaa aluksi jonkin verran kustannuksia.</p>
  <p>Monet laitevalmistajat yrittävät tarjota väriprofiileja laitteille, vaikkakin jotkin niistä on laitettu <em>ajuripakettien</em> sekaan joista väriprofiilien irrottaminen voi vaatia hieman vaivannäköä.</p>
  <p>
    Some manufacturers do not provide accurate profiles for the hardware
    and the profiles are best avoided.
    A good clue is to download the profile, and if the creation date is
    more than a year before the date you bought the device then it’s
    likely dummy data generated that is useless.
  </p>

  <p>Lue <link xref="color-why-calibrate"/> ymmärtääksesi, minkä vuoksi valmistajien tarjoamat profiilit ovat yleensä täysin hyödyttömiä.</p>

</page>
