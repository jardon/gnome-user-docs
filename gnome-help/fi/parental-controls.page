<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="parental-controls" xml:lang="fi">

  <info>
    <link type="guide" xref="user-accounts#manage"/>

    <revision version="gnome:43" status="final" date="2022-12-13"/>

    <credit type="author">
      <name>Anthony McGlone</name>
      <email>anthonymcglone2022@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Määritä vanhempien käytettäväksi järjestelmän käytönvalvonta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Lisää käytönvalvonta vanhemmille</title>

  <p>Parents can use the <gui>Parental Controls</gui> application to prevent children from accessing harmful content.</p>

  <p>An administrator can use this application to:</p>

  <list>
    <item>
      <p>Restrict a user's access to web browsers and applications.</p>
    </item>
    <item>
      <p>Estä käyttäjää asentamasta sovelluksia.</p>
    </item>
    <item>
      <p>Allow a user to only access age-appropriate applications.</p>
    </item>
  </list>

  <note>
    <p>
      <gui>Parental Controls</gui> requires applications to be installed via Flatpak or Flathub.
    </p>
  </note> 

  <section id="restrictwebbrowsers">
    <title>Rajoita verkkoselaimia</title>
    <p><gui>Parental Controls</gui> allows an administrator to disable browser access for a user.</p>
    <steps>
      <item>
        <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Käyttäjät</gui>.</p>
      </item>
      <item>
        <p>
          Select <gui>Users</gui> to open the panel.
        </p>
      </item>
      <item>
         <p>Under <gui>Other Users</gui>, select the user to whom you wish to apply the controls.
         </p>
      </item>
      <item>
         <p>Select the <gui>Parental Controls</gui> tab.
         </p>
         <note>
           <p>The tab only appears if <gui>Parental Controls</gui> is installed and enabled.
           </p>
         </note>
      </item>
      <item>
        <p>
          Press <gui>Unlock</gui> in the <gui>Permission Required</gui> dialog.
        </p>
      </item>
       <item>
        <p>
          Enter your password and authenticate to unlock the <gui>Parental Controls</gui> dialog.
        </p>
      </item>
      <item>
        <p>
          Toggle the <gui>Restrict Web Browsers</gui> switch to turn it on.
        </p>
      </item>
    </steps>
  </section>
  <section id="restrictapplications">
    <title>Rajoita sovelluksia</title>
    <p><gui>Parental Controls</gui> provides a list of applications that an administrator can disable.</p>
    <note>
      <p>
        Existing applications (which are not installed via Flatpak) will not appear in this list.
      </p>
    </note>
    <steps>
      <item>
        <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Käyttäjät</gui>.</p>
      </item>
      <item>
        <p>
          Select <gui>Users</gui> to open the panel.
        </p>
      </item>
      <item>
         <p>Under <gui>Other Users</gui>, select the user to whom you wish to apply the controls.
         </p>
      </item>
      <item>
         <p>Select the <gui>Parental Controls</gui> tab.
         </p>
         <note>
           <p>The tab only appears if <gui>Parental Controls</gui> is installed and enabled.
           </p>
         </note>
      </item>
      <item>
        <p>
          Press <gui>Unlock</gui> in the <gui>Permission Required</gui> dialog.
        </p>
      </item>
       <item>
        <p>
          Enter your password and authenticate to unlock the <gui>Parental Controls</gui> dialog.
        </p>
      </item>
      <item>
        <p>
          Click on <gui>Restrict Applications</gui>.
        </p>
      </item>
      <item>
        <p>
          Toggle the switch next to the application(s) you wish to restrict.
        </p>
      </item>
    </steps>
  </section>
  <section id="restrictapplicationinstallation">
    <title>Rajoita sovellusten asennuksia</title>
    <p><gui>Parental Controls</gui> allows an administrator to deny installation privileges to a user.</p>
    <steps>
      <item>
        <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Käyttäjät</gui>.</p>
      </item>
      <item>
        <p>
          Select <gui>Users</gui> to open the panel.
        </p>
      </item>
      <item>
         <p>Under <gui>Other Users</gui>, select the user to whom you wish to apply the controls.
         </p>
      </item>
      <item>
         <p>Select the <gui>Parental Controls</gui> tab.
         </p>
         <note>
           <p>The tab only appears if <gui>Parental Controls</gui> is installed and enabled.
           </p>
         </note>
      </item>
      <item>
        <p>
          Press <gui>Unlock</gui> in the <gui>Permission Required</gui> dialog.
        </p>
      </item>
       <item>
        <p>
          Enter your password and authenticate to unlock the <gui>Parental Controls</gui> dialog.
        </p>
      </item>
      <item>
        <p>
          Toggle the <gui>Restrict Application Installation</gui> switch to turn it on.
        </p>
      </item>
    </steps>
  </section>
  <section id="applicationsuitability">
    <title>Rajoita sovelluksia ikäryhmän perusteella</title>
    <p>
        Restrict what applications are visible based on their suitability for a given age group. 
    </p>  
    <steps>
      <item>
        <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Käyttäjät</gui>.</p>
      </item>
      <item>
        <p>
          Select <gui>Users</gui> to open the panel.
        </p>
      </item>
      <item>
         <p>Under <gui>Other Users</gui>, select the user to whom you wish to apply the controls.
         </p>
      </item>
      <item>
         <p>Select the <gui>Parental Controls</gui> tab.
         </p>
         <note>
           <p>The tab only appears if <gui>Parental Controls</gui> is installed and enabled.
           </p>
         </note>
      </item>
      <item>
        <p>
          Press <gui>Unlock</gui> in the <gui>Permission Required</gui> dialog.
        </p>
      </item>
       <item>
        <p>
          Enter your password and authenticate to unlock the <gui>Parental Controls</gui> dialog.
        </p>
      </item>
      <item>
        <p>
          Select the age group from the dropdown in  <gui>Application Suitability</gui>.
        </p>
      </item>
    </steps>
  </section>
</page>
