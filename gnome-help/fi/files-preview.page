<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="files-preview" xml:lang="fi">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="nautilus-preview"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-10-06" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Esikatsele nopeasti asiakirjoja, kuvia, videoita ja paljon muuta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Esikatsele tiedostoja ja kansioita</title>

<note if:test="platform:ubuntu" style="important">
  <p>Sovelluksen nimeltä <app>Sushi</app> tulee olla asennettu järjestelmään, jotta alla olevat ohjeet pätevät.</p>
  <p its:locNote="Translators: This button is only shown on Ubuntu, where this   link format is preferred over 'install:gnome-sushi'."><link style="button" href="apt:gnome-sushi">Asenna <app>Sushi</app></link></p>
</note>

<p>You can quickly preview files without opening them in a full-blown
application.  Select any file and press the space bar. The file will open in a
simple preview window. Press the space bar again to dismiss the preview.</p>

<p>The built-in preview supports most file formats for documents, images,
video, and audio. In the preview, you can scroll through your documents or seek
through your video and audio.</p>

<p>To view a preview full-screen, press <key>F</key> or <key>F11</key>. Press
<key>F</key> or <key>F11</key> again to leave full-screen, or press the space
bar to exit the preview completely.</p>

</page>
