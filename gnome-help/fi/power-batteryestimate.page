<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="fi">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc>Akun arvioitu kesto <gui>akkuvalikossa</gui> on pelkkä arvio.</desc>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Akun arvioitu kesto on väärin ilmoitettu</title>

<p>Akun arvioitu kesto saattaa erota todellisesta kestoajasta, sillä akun kestoa voidaan vain arvioida. Yleensä arviot tarkentuvat ajan myötä.</p>

<p>In order to estimate the remaining battery life, a number of factors must be
taken into account. One is the amount of power currently being used by the
computer: power consumption varies depending on how many programs you have
open, which devices are plugged in, and whether you are running any intensive
tasks (like watching high-definition video or converting music files, for
example). This changes from moment to moment, and is difficult to predict.</p>

<p>Toinen muuttuja on akun purkautumistapa. Jotkut akut purkautuvat sitä nopeammin mitä tyhjemmiksi ne tulevat. Ilman tarkkaa tietoa akun purkautumisesta akun kestosta voidaan antaa vain suuntaa antavia arvioita.</p>

<p>Kun akku purkautuu, virranhallinta saa selville akun purkautumisominaisuuksia ja oppii antamaan parempia arvioita akun kestosta. Arviot eivät kuitenkaan koskaan ole täydellisiä.</p>

<note>
  <p>Jos virranhallinta ehdottaa jotain täysin naurettavaa arvoa (kuten satoja päiviä), virranhallinta ei mitä luultavimmin saa tarpeeksi tietoa akun tilasta järkevän arvion tekemiseksi.</p>
  <p>Jos irrotat virtajohdon ja annat kannettavan tietokoneen toimia akun avulla hetken aikaa ennen virtajohdon kiinnittämistä ja akun latautumista, virranhallinta hankkii tarvittavia tietoja akun keston arvioimiseen.</p>
</note>

</page>
