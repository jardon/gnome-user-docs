<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-firewall-on-off" xml:lang="fi">

  <info>
    <link type="guide" xref="net-security" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-03" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hallitse mitkä ohjelmat saavat käyttää Internet-yhteyttä.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Salli ja estä ohjelmien yhteydet palomuurin läpi</title>

  <p>GNOME does not come with a firewall, so for support beyond this document
  check with your distribution’s support team or your organization’s IT department.
  Your computer should be equipped with a <em>firewall</em> that allows it to
  block programs from being accessed by other people on the internet or your
  network. This helps to keep your computer secure.</p>

  <p>Monet sovellukset voivat käyttää tietokoneesi verkkoyhteyttä. Voit esimerkiksi jakaa tiedostoja tai sallia etätyöpöytäyhteyden tietokoneeseesi. Riippuen kuinka tietokoneesi asetukset on määritetty, sinun saattaa olla tarpeen muokata palomuurisääntöjä, jotta nämä palvelut toimivat kuten on tarkoitettu.</p>

  <p>Jokainen verkkopalvelun tarjoava ohjelma käyttää tiettyä <em>verkkoporttia</em>. Jotta muut verkossa olevat tietokoneet voivat käyttää kyseistä ohjelman tarjoamaa palvelua, sinun saattaa olla tarpeen "avata" portti palomuurista:</p>


  <steps>
    <item>
      <p>Go to <gui>Activities</gui> in the top left corner of the screen and
      start your firewall application. You may need to install a firewall
      manager yourself if you can’t find one (for example, GUFW).</p>
    </item>
    <item>
      <p>Open or disable the port for your network service, depending on
      whether you want people to be able to access it or not. Which port you
      need to change will <link xref="net-firewall-ports">depend on the
      service</link>.</p>
    </item>
    <item>
      <p>Save or apply the changes, following any additional instructions given
      by the firewall tool.</p>
    </item>
  </steps>

</page>
