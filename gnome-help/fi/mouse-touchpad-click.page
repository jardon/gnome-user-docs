<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="fi">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Napsauta, vedä tai vieritä näpytyksin ja elein kosketuslevyä käyttäen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Napsauta, vedä tai vieritä kosketuslevyä käyttäen</title>

  <p>Voit käyttää kosketuslevyä napsautuksiin, kaksoisnapsautuksiin, raahaamiseen ja vierittämiseen ilman fyysisiä näppäimiä.</p>

  <note>
    <p><link xref="touchscreen-gestures">Kosketuslevyn eleet</link> kuvataan erikseen.</p>
  </note>

<section id="tap">
  <title>Napauta napsauttaaksesi</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Voit napauttaa kosketuslevyä sen sijaan, että napsauttaisit painiketta.</p>

  <list>
    <item>
      <p>Napsautus onnistuu kosketuslevyä napauttamalla.</p>
    </item>
    <item>
      <p>Kaksoisnapsautus onnistuu napauttamalla kosketuslevyä kahdesti.</p>
    </item>
    <item>
      <p>To drag an item, double-tap but don’t lift your finger after the
      second tap. Drag the item where you want it, then lift your finger to
      drop.</p>
    </item>
    <item>
      <p>Jos kosketuslevy tukee usean sormen napautuksia, voit simuloida hiiren oikean painikkeen napsautusta napauttamalla kosketuslevyä kahdella sormella samaan aikaan. Muussa tapauksessa sinun täytyy käyttää fyysisiä painikkeita hiiren oikean näppäimen napsautukseen. Lue toisesta tavasta simuloida hiiren oikeaa napsautusta artikkelista <link xref="a11y-right-click"/> .</p>
    </item>
    <item>
      <p>Jos kosketuslevy tukee usean sormen napautuksia, <link xref="mouse-middleclick">hiiren keskimmäisen painikkeen napsautus</link> onnistuu napauttamalla kosketuslevyä kolmella sormella samanaikaisesti.</p>
    </item>
  </list>

  <note>
    <p>When tapping or dragging with multiple fingers, make sure your fingers
    are spread far enough apart. If your fingers are too close, your computer
    may think they’re a single finger.</p>
  </note>

  <steps>
    <title>Enable Tap to Click</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Hiiri ja kosketuslevy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
      <note>
        <p><gui>Kosketuslevy</gui>-osio näkyy vain, jos tietokoneessa on kosketuslevy.</p>
      </note>
    </item>
   <item>
      <p>Switch the <gui>Tap to Click</gui> switch to on.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Kahden sormen vieritys</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Voit vierittää kosketusalustalla käyttämällä kahta sormea.</p>

  <p>When this is selected, tapping and dragging with one finger will work as
  normal, but if you drag two fingers across any part of the touchpad, it will
  scroll instead. Move your fingers between the top and bottom of your touchpad
  to scroll up and down, or move your fingers across the touchpad to scroll
  sideways. Be careful to space your fingers a bit apart. If your fingers are
  too close together, they just look like one big finger to your touchpad.</p>

  <note>
    <p>Kahden sormen vieritys ei välttämättä toimi kaikilla kosketuslevyillä.</p>
  </note>

  <steps>
    <title>Ota kahden sormen vieritys käyttöön</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Hiiri ja kosketuslevy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Two-finger Scrolling</gui> switch to on.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Reunavieritys</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Use edge scroll if you want to scroll with only one finger.</p>

  <p>Your touchpad's specifications should give the exact
  location of the sensors for edge scrolling. Typically, the vertical
  scroll sensor is on a touchpad's right-hand side. The horizontal
  sensor is on the touchpad's bottom edge.</p>

  <p>To scroll vertically, drag your finger up and down the right-hand
  edge of the touchpad. To scroll horizontally, drag your finger across
  the bottom edge of the touchpad.</p>

  <note>
    <p>Edge scrolling may not work on all touchpads.</p>
  </note>

  <steps>
    <title>Enable Edge Scrolling</title>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Hiiri ja kosketuslevy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure the <gui>Touchpad</gui>
      switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Edge Scrolling</gui> switch to on.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Luonnollinen vieritys</title>

  <p>You can drag content as if sliding a physical piece of paper using the
  touchpad.</p>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Hiiri ja kosketuslevy</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Touchpad</gui> section, make sure that the
     <gui>Touchpad</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Switch the <gui>Natural Scrolling</gui> switch to on.</p>
    </item>
  </steps>

  <note>
    <p>Tätä ominaisuutta kutsutaan myös nimellä <em>käänteinen vieritys</em>.</p>
  </note>

</section>

</page>
