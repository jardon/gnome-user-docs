<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-batterywindows" xml:lang="fi">

  <info>
    <link type="guide" xref="power#faq"/>
    <link type="seealso" xref="power-batteryestimate"/>
    <link type="seealso" xref="power-batterylife"/>
    <link type="seealso" xref="power-batteryslow"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>

    <desc>Valmistajan ohjelmistolisäykset ja akun käyttöajan arvioinnin erot voivat aiheuttaa tämän ongelman.</desc>
    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

<title>Miksi akkua tuntuu olevan vähemmän kuin Windowsissa tai Mac OS X:ssä?</title>

<p>Joidenkin tietokoneiden käyttöaika akulla saattaa lyhetä Linuxia käytettäessä, vaikka entisessä Windowsissa tai Mac OS X:ssä akku kestää pidempään. Tämä johtuu yleensä siitä, että tietokonevalmistaja on todennäköisesti asentanut Windowsiin/Maciin virranhallintaa parantavia ohjelmistoja tai laitteita. Ne ovat yleensä niin käyttöjärjestelmäkohtaisia, että niiden integroiminen myös Linuxiin ei ole järkevää.</p>

<p>Valitettavasti näiden ongelmien korjaaminen ainakaan helposti ei ole mahdollista, ellet tarkalleen tiedä mistä ne johtuvat. <link xref="power-batterylife">Yleiset virransäästövinkit</link> voivat auttaa. Lisäksi jos tietokoneen <link xref="power-batteryslow">suorittimen nopeutta</link> voi muuttaa, sitä kannattaa kokeilla.</p>

<p>Yksi syy akun keston arvion epäjohdonmukaisuuteen on se, että arviointitapa on erilainen Windowsin, Mac OS X:n ja Linuxin välillä. Akun kesto saattaa olla kaikissa sama, mutta eri arviointitavat antavat vaihtelevia arvoja.</p>
	
</page>
