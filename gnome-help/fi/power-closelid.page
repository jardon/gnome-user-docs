<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="fi">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Gnomen dokumentointiprojekti</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kannettavat siirtyvät valmiustilaan sulkiessasi kannen virran säästämiseksi.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Timo Jyrinki</mal:name>
      <mal:email>timo.jyrinki@iki.fi</mal:email>
      <mal:years>2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jiri Grönroos</mal:name>
      <mal:email>jiri.gronroos+l10n@iki.fi</mal:email>
      <mal:years>2012-2023.</mal:years>
    </mal:credit>
  </info>

  <title>Miksi tietokoneeni sammuu, kun suljen kannen?</title>

  <p>When you close the lid of your laptop, your computer will
  <link xref="power-suspend"><em>suspend</em></link> in order to save power.
  This means that the computer is not actually turned off — it has just gone to
  sleep. You can resume it by opening the lid. If it does not resume, try
  clicking the mouse or pressing a key. If that still does not work, press the
  power button.</p>

  <p>Some computers are unable to suspend properly, normally because their
  hardware is not completely supported by the operating system (for example,
  the Linux drivers are incomplete). In this case, you may find that you are
  unable to resume your computer after you have closed the lid. You can try to
  <link xref="power-suspendfail">fix the problem with suspend</link>, or you
  can prevent the computer from trying to suspend when you close the lid.</p>

<section id="nosuspend">
  <title>Estä tietokonetta siirtymästä valmiustilaan, kun kansi suljetaan</title>

  <note style="important">
    <p>These instructions will only work if you are using <app>systemd</app>.
    Contact your distribution for more information.</p>
  </note>

  <note style="important">
    <p>Sovelluksen nimeltä <app>Lisäasetukset</app> tulee olla asennettu järjestelmään, jotta tämä asetus on mahdollista muuttaa.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Asenna <app>Gnomen lisäasetukset</app></link></p>
    </if:if>
  </note>

  <p>If you do not want the computer to suspend when you close the lid, you can
  change the setting for that behavior.</p>

  <note style="warning">
    <p>Ole varovainen, jos muutat tätä asetusta. Jotkut kannettavat voivat ylikuumentua, jos ne jätetään päälle kannen ollessa kiinni. Tämä on todennäköistä eritoten silloin, kun ne ovat suljetussa tilassa kuten laukussa.</p>
  </note>

  <steps>
    <item>
      <p>Avaa <gui xref="shell-introduction#activities">Toiminnot</gui>-yleisnäkymä ja ala kirjoittamaan <gui>Lisäasetukset</gui>.</p>
    </item>
    <item>
      <p>Napsauta <gui>Lisäasetukset</gui> avataksesi sovelluksen.</p>
    </item>
    <item>
      <p>Select the <gui>General</gui> tab.</p>
    </item>
    <item>
      <p>Switch the <gui>Suspend when laptop lid is closed</gui> switch to
      off.</p>
    </item>
    <item>
      <p>Sulje <gui>Lisäasetukset</gui>-ikkuna.</p>
    </item>
  </steps>

</section>

</page>
