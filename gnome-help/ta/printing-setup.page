<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="ta">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>ஃபில் புல்</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஜிம் காம்ப்பெல்</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>பால் W. ஃப்ரியெல்ட்ஸ்</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>ஷான் மெக்கேன்ஸ்</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>மைக்கேல் ஹில்</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Set up a printer that is connected to your computer, or your local
    network.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>உள்ளமை அச்சுப்பொறியை அமைத்தல்</title>

  <p>Your system can recognize many types of printers automatically once they
  are connected. Most printers are connected with a USB cable that attaches to
  your computer, but some printers connect to your wired or wireless
  network.</p>

  <note style="tip">
    <p>If your printer is connected to the network, it will not be set up
    automatically – you should add it from the <gui>Printers</gui> panel in
    <gui>Settings</gui>.</p>
  </note>

  <steps>
    <item>
      <p>உங்கள் அச்சுப்பொறி இயக்கப்பட்டுள்ளதா எனப் பார்த்துக்கொள்ளவும்.</p>
    </item>
    <item>
      <p>உங்கள் அச்சுப்பொறியை சரியான கேபிள் கொண்டு கணினியில் இணைக்கவும். கணினி இயக்கிகளைத் தேடும் போது உங்கள் திரையில் அந்த செயல்பாடுகளைக் காணலாம், அவற்றை நிறுவ உங்களிடம் அங்கீகரிப்பு கேட்கப்படலாம்.</p>
    </item>
    <item>
      <p>கணினி அச்சுப்பொறியை நிறுவுதலை முடித்ததும் ஒரு செய்தி காண்பிக்கப்படும். ஒரு சோதனைப் பக்கத்தை அச்சிட <gui>சோதனைப் பக்கத்தை அச்சிடு</gui> ஐ தேர்ந்தெடுக்கவும் அல்லது அச்சுப்பொறி அமைவில் கூடுதல் மாற்றங்கள் செய்ய <gui>விருப்பங்கள்</gui> ஐ தேர்ந்தெடுக்கவும்.</p>
    </item>
  </steps>

  <p>If your printer was not set up automatically, you can add it in the
  printer settings:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Printers</gui>.</p>
    </item>
    <item>
      <p><gui>அச்சுப்பொறிகள்</gui> ஐ சொடுக்கவும்.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Add…</gui> button.</p>
    </item>
    <item>
      <p>In the pop-up window, select your new printer and press
      <gui style="button">Add</gui>.</p>
      <note style="tip">
        <p>If your printer is not discovered automatically, but you know its
        network address, enter it into the text field at the bottom of the
        dialog and then press <gui style="button">Add</gui></p>
      </note>
    </item>
  </steps>

  <p>If your printer does not appear in the <gui>Add Printer</gui> window, you
  may need to install print drivers.</p>

  <p>நீங்கள் அச்சுப்பொறியை நிறுவிய பிறகு <link xref="printing-setup-default-printer">உங்கள் முன்னிருப்பு அச்சுப்பொறியை மாற்றலாம்</link>.</p>

</page>
