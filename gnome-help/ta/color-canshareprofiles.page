<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="ta">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>நிற தனியமைப்பைப் பகிர்தல் என்பது நல்ல பரிந்துரையல்ல, ஏனெனில் வன்பொருள் காலம் செல்லச் செல்ல மாறக்கூடும்.</desc>

    <credit type="author">
      <name>ரிச்சர்ட் ஹியூகஸ்</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>நான் என் நிற தனியமைப்பைப் பகிரலாமா?</title>

  <p>நீங்கள் உருவாக்கும் நிற தனியமைப்புகள் வன்பொருளுக்கும் நீங்கள் எந்த ஒளியமைப்புக்காக அளவை வகுத்தீர்களோ அந்த ஒளியமைப்புக்கும் பிரத்யேகமானது. சில நூறு மணி நேரங்கள் ஒளியூட்டி இயங்கிக் கொண்டிருக்கும் ஒரு காட்சி சாதனத்தின் நிற தனியமைப்பானது அதற்கு அடுத்த வரிசை எண்ணைக் கொண்ட ஆயிரம் மணி நேரங்கள் ஒளியூட்டி இயங்கிக் கொண்டிருக்கும் மற்றொரு ஒத்த காட்சி சாதனத்தின் நிற தனியமைப்பிலிருந்து மிகவும் வேறுபட்டதாக இருக்கும்.</p>
  <p>
    This means if you share your color profile with somebody, you might
    be getting them <em>closer</em> to calibration, but it’s misleading
    at best to say that their display is calibrated.
  </p>
  <p>
    Similarly, unless everyone has recommended controlled lighting
    (no sunlight from windows, black walls, daylight bulbs etc.) in a
    room where viewing and editing images takes place, sharing a profile
    that you created in your own specific lighting conditions doesn’t make
    a lot of sense.
  </p>

  <note style="warning">
    <p>நீங்கள் நிற தனியமைப்புகளை பதிவிறக்கம் செய்யும் நிறுவன வலைத்தளங்கள் அல்லது உங்கள் சார்பில் தனியமைப்புகள் உருவாக்கப்படும் இடங்கள் ஆகியவற்றின் நிற தனியமைப்புக்கான மறுவிநியோக நிபந்தனைகளை கவனமாக புரிந்து நடந்துகொள்ள வேண்டும்.</p>
  </note>

</page>
