<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="keyboard-key-super" xml:lang="ta">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.7.91" version="0.2" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-27" status="review"/>

    <credit type="author">
      <name>GNOME ஆவணமாக்கத் திட்டப்பணி</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>எக்காட்டெரினா ஜெராசிமோவா</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><key>Super</key> விசையானது <gui>செயல்பாடுகள் மேலோட்டத்தைத்</gui> திறக்கும். இது வழக்கமாக உங்கள் விசைப்பலகையில் <key>Alt</key> க்கு அடுத்ததாக இருக்கும்.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shantha kumar,</mal:name>
      <mal:email>shkumar@redhat.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title><key>Super</key> என்பது என்ன?</title>

  <p>நீங்கள் <key>Super</key> விசையை அழுத்தும் போது, <gui>செயல்பாடுகள் மேலோட்டம்</gui> காட்டப்படும். வழக்கமாக உங்கள் விசைப்பலகையின் கீழ் இடது பக்கத்தில் <key>Alt</key> விசைக்கு அடுத்து இந்த விசையைக் காணலாம், வழக்கமாக இதன் மீது ஒரு Windows சின்னம் இருக்கும். சில சமயம் இதனை <em>Windows</em> விசை அல்லது கணினி விசை என்றும் குறிப்பிடுகிறோம்.</p>

  <note>
    <p>If you have an Apple keyboard, you will have a <key>⌘</key> (Command)
    key instead of the Windows key, while Chromebooks have a magnifying glass
    instead.</p>
  </note>

  <p><gui>செயல்பாடுகள் மேலோட்டத்தைக்</gui> காண்பிக்க எந்த விசை பயன்படுகிறது என்பதை மாற்ற:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the <gui>System</gui> category.</p>
    </item>
    <item>
      <p>Click the row with <gui>Show the activities overview</gui>.</p>
    </item>
    <item>
      <p>விரும்பும் விசை சேர்க்கையை அழுத்திப் பிடிக்கவும்.</p>
    </item>
  </steps>

</page>
