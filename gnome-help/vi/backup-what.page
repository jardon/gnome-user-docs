<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="vi">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up anything that you cannot bear to lose if something goes
    wrong.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Sao lưu cái gì</title>

  <p>Độ ưu tiên sao lưu nên là <link xref="backup-thinkabout">tập tin quan trọng nhất</link> và những tập tin khó tái tạo lại. Ví dụ xếp hạng những tập tin quan trọng nhất đến những tập tin ít quan trọng hơn:</p>

<terms>
 <item>
  <title>Tập tin cá nhân</title>
   <p>Tài liệu, bảng tính, email, cuộc hẹn, dữ liệu tài chính, ảnh gia đình, bất cứ thứ gì quan trọng đối với bạn. Những thứ này rõ ràng quan trọng và không thể thay thế.</p>
 </item>

 <item>
  <title>Thiết lập cá nhân</title>
   <p>Bao gồm thay đổi về màu sắc, ảnh nền, độ phân giải màn hình, thiết lập chuột... Ngoài ra còn có tuỳ chỉnh ứng dụng như thiết lập <app>LibreOffice</app>, trình phát nhạc, trình email. Những thứ này có thể thay thể được, nhưng có thể mất thời gian.</p>
 </item>

 <item>
  <title>Thiết lập hệ thống</title>
   <p>Hầu hết mọi người không bao giờ thay đổi thiết lập sau khi cài đặt. Nếu bạn thay đổi vì lí do nào đó, hoặc bạn dùng máy tính như máy chủ, bạn có thể muốn sao lưu các thiết lập này.</p>
 </item>

 <item>
  <title>Phần mềm đã cài đặt</title>
   <p>Phần mềm thường có thể phục hồi nhanh bằng cách cài đặt lại sau khi máy tính gặp vấn đề nghiêm trọng.</p>
 </item>
</terms>

  <p>Nhìn chung, bạn sẽ muốn sao lưu những tập tin không thể thay thế và những tập tin tốn nhiều thời gian công sức để thay thế nếu không sao lưu. Ngược lại, bạn có thể không muốn sao lưu những thứ dễ thay để đỡ tốn chỗ sao lưu.</p>

</page>
