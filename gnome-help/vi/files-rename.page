<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="vi">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Dự án tài liệu GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Thay đổi tên tập tin hoặc thư mục.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Đổi tên tập tin hoặc thư mục</title>

  <p>As with other file managers, you can use <app>Files</app> to change the
  name of a file or folder.</p>

  <steps>
    <title>Để đổi tên tập tin hoặc thư mục:</title>
    <item><p>Nhấn chuột phải vào tập tin hoặc thư mục và chọn <gui>Đổi tên</gui>, hoặc chọn tập tin và nhấn <key>F2</key>.</p></item>
    <item><p>Type the new name and press <key>Enter</key> or click
    <gui>Rename</gui>.</p></item>
  </steps>

  <p>Bạn cũng có thể đổi tên tập tin từ cửa sổ <link xref="nautilus-file-properties-basic#rename">thuộc tính</link>.</p>

  <p>When you rename a file, only the first part of the name of the file is
  selected, not the file extension (the part after the last <file>.</file>).
  The extension normally denotes what type of file it is (for example,
  <file>file.pdf</file> is a PDF document), and you usually do not want to
  change that. If you need to change the extension as well, select the entire
  file name and change it.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the toolbar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>Ký tự hợp lệ cho tên tập tin</title>

    <p>You can use any character except the <file>/</file> (slash) character in
    file names. Some devices, however, use a <em>file system</em> that has more
    restrictions on file names. Therefore, it is a best practice to avoid the
    following characters in your file names: <file>|</file>, <file>\</file>,
    <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>,
    <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>If you name a file with a <file>.</file> as the first character, the
    file will be <link xref="files-hidden">hidden</link> when you attempt to
    view it in the file manager.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Vấn đề thông thường</title>

    <terms>
      <item>
        <title>Tên tập tin đã dùng rồi</title>
        <p>You cannot have two files or folders with the same name in the same
        folder. If you try to rename a file to a name that already exists in
        the folder you are working in, the file manager will not allow it.</p>
        <p>Tên tập tin và thư mục phân biệt chữ hoa và chữ thường, nên <file>Taptin.txt</file> và <file>taptin.txt</file> là hai tên khác nhau. Đặt tên như vậy là hợp lệ, nhưng không nên.</p>
      </item>
      <item>
        <title>Tên tập tin quá dài</title>
        <p>On some file systems, file names can have no more than 255
        characters in their names.  This 255 character limit includes both the
        file name and the path to the file (for example,
        <file>/home/wanda/Documents/work/business-proposals/…</file>), so you
        should avoid long file and folder names where possible.</p>
      </item>
      <item>
        <title>Tuỳ chọn đổi tên bị tô xám</title>
        <p>Nếu <gui>Đổi tên</gui> bị tô xám, bạn không có quyền đổi tên tập tin đó. Bạn nên cẩn thận nếu muốn đổi tên những tập tin như vậy, vì đổi tên tập tin được bảo vệ có thể làm mất ổn định hệ thống. Xem <link xref="nautilus-file-properties-permissions"/> để biết thêm.</p>
      </item>
    </terms>

  </section>

</page>
