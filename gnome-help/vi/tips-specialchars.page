<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="vi">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Type characters not found on your keyboard, including foreign alphabets, mathematical symbols, emoji, and dingbats.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Nhập ký tự đặc biệt</title>

  <p>You can enter and view thousands of characters from most of the world’s
  writing systems, even those not found on your keyboard. This page lists
  some different ways you can enter special characters.</p>

  <links type="section">
    <title>Cách nhập ký tự</title>
  </links>

  <section id="characters">
    <title>Characters</title>
    <p>The character map application allows you to find and insert unusual
    characters, including emoji, by browsing character categories or searching
    for keywords.</p>

    <p>You can launch <app>Characters</app> from the Activities overview.</p>

  </section>

  <section id="emoji">
    <title>Emoji</title>
    <steps>
      <title>Insert emoji</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Browse the categories at the bottom of the dialog or start typing a
    description in the search field.</p>
      </item>
      <item>
        <p>Select an emoji to insert.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Phím tổng hợp</title>
    <p>Phím tổng hợp (compose) là phím đặc biệt, cho phép bạn nhấn nhiều phím cùng lúc để tạo ra một ký tự đặc biệt. Vídụ, để gõ ký tự <em>é</em>, bạn có thể nhấn <key>tổng hợp</key> và sau đó <key>'</key> rồi <key>e</key>.</p>
    <p>Keyboards don’t have specific compose keys. Instead, you can define
    one of the existing keys on your keyboard as a compose key.</p>

    <steps>
      <title>Định nghĩa phím tổng hợp</title>
      <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click on <gui>Settings</gui>.</p>
      </item>
      <item>
        <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
      </item>
      <item>
        <p>In the <gui>Type Special Characters</gui> section, click <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Turn the switch on for the <gui>Compose Key</gui>.</p>
      </item>
      <item>
        <p>Tick the checkbox of the key that you want to set as the Compose
        key.</p>
      </item>
      <item>
        <p>Close the dialog.</p>
      </item>
    </steps>

    <p>Bạn có thể gõ nhiều ký tự thông thường với phím tổng hợp, ví dụ:</p>

    <list>
      <item><p>Nhấn <key>tổng hợp</key> rồi <key>'</key> và một chữ cái để đặt dấu sắc lên chữ cái đó, ví dụ <em>é</em>.</p></item>
      <item><p>Nhấn <key>tổng hợp</key> rồi <key>`</key> và một chữ cái để đặt dấu huyền lên chữ cái đó, ví dụ <em>è</em>.</p></item>
      <item><p>Nhấn <key>tổng hợp</key> rồi <key>"</key> và một chữ cái để đặt dấu hai chấm ngang (umlaut) lên chữ cái đó, ví dụ <em>ë</em>.</p></item>
      <item><p>Nhấn <key>tổng hợp</key> rồi <key>-</key> và một chữ cái để đặt dấu gạch ngang ở trên (macron) lên chữ cái đó, ví dụ <em>ē</em>.</p></item>
    </list>
    <p>For more compose key sequences, see <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">the
    compose key page on Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Điểm mã</title>

  <p>You can enter any Unicode character using only your keyboard with the
  numeric code point of the character. Every character is identified by a
  four-character code point. To find the code point for a character, look it up
  in the <app>Characters</app> application. The code point is the four characters
  after <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>Bố trí bàn phím</title>
    <p>Bạn có thể biến bàn phím mình thành bàn phím thiết kế cho một ngôn ngữ khác, bỏ qua tên phím in trên mỗi phím. Bạn cũng có thể dễ dành chuyển đổi các bố trí bàn phím khác nhau dùng biểu tượng trên thanh đỉnh. Xem <link xref="keyboard-layouts"/> để biết thêm.</p>
  </section>

<section id="im">
  <title>Input methods</title>

  <p>An Input Method expands the previous methods by allowing to enter
  characters not only with keyboard but also any input devices. For instance
  you could enter characters with a mouse using a gesture method, or enter
  Japanese characters using a Latin keyboard.</p>

  <p>To choose an input method, right-click over a text widget, and in the menu
  <gui>Input Method</gui>, choose an input method you want to use. There is no
  default input method provided, so refer to the input methods documentation to
  see how to use them.</p>

</section>

</page>
