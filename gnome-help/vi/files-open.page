<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="vi">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Open files using an application that isn’t the default one for that
    type of file. You can change the default too.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

<title>Mở tập tin bằng ứng dụng khác</title>

  <p>When you double-click (or middle-click) a file in the file manager, it
  will be opened with the default application for that file type. You can open
  it in a different application, search online for applications, or set the
  default application for all files of the same type.</p>

  <p>To open a file with an application other than the default, right-click
  the file and select the application you want from the top of the menu. If
  you do not see the application you want, select <gui>Open With Other
  Application</gui>. By default, the file manager only shows applications that
  are known to handle the file. To look through all the applications on your
  computer, click <gui>View All Applications</gui>.</p>

<p>If you still cannot find the application you want, you can search for
more applications by clicking <gui>Find New Applications</gui>. The
file manager will search online for packages containing applications
that are known to handle files of that type.</p>

<section id="default">
  <title>Thay đổi ứng dụng mặc định</title>
  <p>Bạn có thể thay đổi ứng dụng mặc định dùng để mở tập tin thuộc một loại cho trước, cho phép bạn mở ứng dụng ưa thích của mình khi nhấp đúp để mở tập tin. Ví dụ, bạn có thể muốn mở ứng dụng nghe nhạc yêu thích khi nhấp đúp tập tin MP3.</p>

  <steps>
    <item><p>Chọn một tập tin thuộc loại bạn muốn thay đổi ứng dụng mặc định. Ví dụ, để thay đổi ứng dụng dùng để mở tập tin MP3, chọn một tập tin <file>.mp3</file>.</p></item>
    <item><p>Nhấn chuột phải và chọn <gui>Thuộc tính</gui>.</p></item>
    <item><p>Chọn thẻ <gui>Mở bằng</gui>.</p></item>
    <item><p>Select the application you want and click
    <gui>Set as default</gui>.</p>
    <p>If <gui>Other Applications</gui> contains an application you sometimes
    want to use, but do not want to make the default, select that application
    and click <gui>Add</gui>. This will add it to <gui>Recommended
    Applications</gui>. You will then be able to use this application by
    right-clicking the file and selecting it from the list.</p></item>
  </steps>

  <p>Hành động này thay đổi ứng dụng mặc định, không chỉ cho tập tin đó, mà cho mọi tập tin cùng loại.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
