<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="vi">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Nhiều thứ có thể đang tải về, có thể bạn có đường truyền kém, hoặc có thể do giờ cao điểm.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nguyễn Thái Ngọc Duy</mal:name>
      <mal:email>pclouds@gmail.com</mal:email>
      <mal:years>2011-2012.</mal:years>
    </mal:credit>
  </info>

  <title>Internet có vẻ chậm</title>

  <p>Nếu bạn đang dùng Internet và cảm thấy chậm, có thể có vài lý do dẫn đến chậm mạng.</p>

  <p>Thử đóng các trình duyệt web và mở lại, ngắt kết nối Internet và kết nối lại. Làm thế có thể sẽ ngắt những thứ gây ra chậm mạng.</p>

  <list>
    <item>
      <p><em style="strong">Giờ cao điểm</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">Tải về quá nhiều thứ cùng lúc</em></p>
      <p>Nếu bạn hoặc ai khác đang dùng kết nối Internet của bạn tải về nhiều tập tin cùng lúc, hoặc xem phim, kết nối có thể sẽ không đủ nhanh để đáp ứng. Bạn sẽ cảm giác chậm hơn.</p>
    </item>
    <item>
      <p><em style="strong">Kết nối không ổn định</em></p>
      <p>Vài kết nối Internet không ổn định, đặc biệt là những kết nối tạm hoặc ở vùng có nhu cầu cao. Nếu bạn ở trong quán cà phê đông khác hoặc một trung tâm hội nghị, kết nối Internet có thể quá bận rộn hoặc không ổn định.</p>
    </item>
    <item>
      <p><em style="strong">Tín hiệu kết nối không dây yếu</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">Dùng kết nối Internet di động chậm hơn</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Duyệt web gặp vấn đề</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
