<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="es">

  <info>
    <link type="guide" xref="shell-overview"/>

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Considere cambiar a GNOME clásico si prefiere una experiencia del escritorio más tradicional.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>¿Qué es GNOME clásico?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME clásico</em> es una característica para aquellos usuarios que prefieren una experiencia del escritorio más tradicional. Aunque <em>GNOME clásico</em> se basa en tecnologías de GNOME modernas, proporciona varios cambios en la interfaz del usuario, como los menús <gui>Aplicaciones</gui> y <gui>Lugares</gui> en la barra superior, y una lista de ventanas en la parte inferior de la pantalla.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME clásico</em> es una característica para aquellos usuarios que prefieren una experiencia del escritorio más tradicional. Aunque <em>GNOME clásico</em> se basa en tecnologías de GNOME modernas, proporciona varios cambios en la interfaz del usuario, como los menús <gui xref="shell-introduction#activities">Aplicaciones</gui> y <gui>Lugares</gui> en la barra superior, y una lista de ventanas en la parte inferior de la pantalla.</p>
  </if:when>
</if:choose>

<p>Puede usar el menú <gui>Aplicaciones</gui> para lanzar aplicaciones.</p>

<section id="gnome-classic-window-list">
<title>Lista de ventanas</title>

<p>La lista de ventanas en la parte inferior de la pantalla proporciona acceso a todas sus ventanas y aplicaciones abiertas y le permite minimizarlas y restaurarlas rápidamente.</p>

<p>La vista de <gui xref="shell-introduction#activities">Actividades</gui> está disponible pulsando el botón a la izquierda de la lista de ventanas de la parte inferior.</p>

<p>Para acceder a la <em>vista de <gui>Actividades</gui></em>, también puede pulsar la tecla <key xref="keyboard-key-super">Super</key>.</p>
 
 <p>A la derecha de la lista de ventanas, GNOME muestra un identificador corto para el área de trabajo actual, como <gui>1</gui> para la primera área de trabajo. Además, el identificador también muestra el número total de áreas de trabajo disponibles. Para cambiar a un área de trabajo diferente, puede pulsar el en identificador y seleccionar el área de trabado que quiere usar en el menú.</p>

</section>

<section id="gnome-classic-switch">
<title>Cambiar a y desde GNOME clásico</title>

<note if:test="!platform:gnome-classic !platform:ubuntu" style="important">
<p>GNOME clásico sólo está disponible en sistemas con determinadas extensiones de GNOME Shell instaladas. Algunas distribuciones de Linux pueden no tener esas extensiones disponibles o instaladas de manera predeterminada.</p>
</note>

<note if:test="!platform:gnome-classic platform:ubuntu" style="important">
<p its:locNote="Translators: Ubuntu only string">You need to have the
 <sys>gnome-shell-extensions</sys> package installed to make GNOME Classic available.</p>
<p its:locNote="Translators: Ubuntu only string"><link style="button" action="install:gnome-shell-extensions">Install <sys>gnome-shell-extensions</sys></link></p>
</note>

  <steps>
    <title>Para cambiar de <em>GNOME</em> a <em>GNOME clásico</em>:</title>
    <item>
      <p>Guarde cualquier trabajo que tenga abierto y cierre la sesión. Pulse en el menú del sistema a la derecha de la barra superior y seleccione la opción apropiada.</p>
    </item>
    <item>
      <p>Aparecerá un mensaje de confirmación. Seleccione <gui>Cerrar la sesión</gui> para confirmar.</p>
    </item>
    <item>
      <p>En la pantalla de inicio de sesión, seleccione su nombre de usuario de la lista.</p>
    </item>
    <item>
      <p>Pulse el botón <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">configuración</span></media> en la esquina inferior derecha.</p>
    </item>
    <item>
      <p>Seleccione <gui>GNOME clásico</gui> en la lista.</p>
    </item>
    <item>
      <p>Introduzca su contraseña en la caja de entrada de la contraseña.</p>
    </item>
    <item>
      <p>Pulse <key>Intro</key>.</p>
    </item>
  </steps>

  <steps>
    <title>Para cambiar de <em>GNOME clásico</em> a <em>GNOME</em>:</title>
    <item>
      <p>Guarde cualquier trabajo que tenga abierto y cierre la sesión. Pulse sobre en el menú del sistema a la derecha de la barra superior, pulse sobre su nombre y seleccione la opción apropiada.</p>
    </item>
    <item>
      <p>Aparecerá un mensaje de confirmación. Seleccione <gui>Cerrar la sesión</gui> para confirmar.</p>
    </item>
    <item>
      <p>En la pantalla de inicio de sesión, seleccione su nombre de usuario de la lista.</p>
    </item>
    <item>
      <p>Pulse el icono de opciones en la esquina inferior derecha.</p>
    </item>
    <item>
      <p>Seleccione <gui>GNOME</gui> en la lista.</p>
    </item>
    <item>
      <p>Introduzca su contraseña en la caja de entrada de la contraseña.</p>
    </item>
    <item>
      <p>Pulse <key>Intro</key>.</p>
    </item>
  </steps>
  
</section>

</page>
