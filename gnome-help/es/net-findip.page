<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="es">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conocer su dirección IP le puede ayudar a resolver problemas de red.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Buscar su dirección IP</title>

  <p>Conocer su dirección IP le puede ayudar a solucionar problemas con su conexión a Internet. Es posible que se sorprenda al saber que tiene <em>dos</em> direcciones IP: una dirección IP de su equipo en la red interna y una dirección IP de su equipo en Internet.</p>
  
  <section id="wired">
    <title>Find your wired connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Red</gui> en la barra lateral para abrir el panel.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">Si hay más de un tipo de red cableada disponible puede ver nombres como <gui>PCI Ethernet</gui> o <gui>USB Ethernet</gui> en lugar de <gui>Cableada</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Find your wireless connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Configuración</gui>.</p>
    </item>
    <item>
      <p>Pulse <gui>Inalámbrica</gui> en la barra lateral para abrir el panel.</p>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>Buscar su dirección IP externa (Internet)</title>
  <steps>
    <item>
      <p>Visite <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>El sitio le mostrará su dirección IP externa.</p>
    </item>
  </steps>
  <p>Depending on how your computer connects to the internet, the internal and
  external addresses may be the same.</p>  
  </section>

</page>
