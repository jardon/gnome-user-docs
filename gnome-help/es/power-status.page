<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="es">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostrar el estado de la batería y de los dispositivos conectados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Compruebe el estado de la batería</title>

  <steps>

    <title>Mostrar el estado de la batería y de los dispositivos conectados</title>

    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Energía</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel. The status of
      <gui>Batteries</gui> and known <gui>Devices</gui> is displayed.</p>
    </item>

  </steps>

    <p>Si se detecta una batería interna, la sección <gui>Batería</gui> muestra el estado de una o más batería de portátil. La barra indicadora muestra el porcentaje de carga, así como el tiempo hasta que se cargue por completo si está enchufada y el tiempo restante si no lo está.</p>

    <p>La sección <gui>Dispositivos</gui> muestra el estado de los dispositivos conectados.</p>
    
    <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. It can also display the charge as a
    <link xref="power-percentage">percentage</link>.</p>

</page>
