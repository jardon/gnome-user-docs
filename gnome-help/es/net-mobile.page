<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="es">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Use su teléfono o su USB de Internet para conectarse a la red de banda ancha móvil.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Conectar a una red de banda ancha móvil</title>

  <p>Puede configurar una conexión a una red móvil (3G) usando su módem 3G integrado, su teléfono móvil o un adaptador de Internet.</p>

  <note style="tip">
  <p>La mayoría de los teléfonos tienen una opción llamada <link xref="net-tethering">USB tethering</link> que no necesita configuración en el equipo y que generalmente es el mejor método para conectar el teléfono móvil.</p>
  </note>

  <steps>
    <item><p>Si no tiene un módem 3G integrado, conecte su teléfono o dispositivo de Internet a un puerto USB de su equipo.</p>
    </item>
    <item>
    <p>Abra el <gui xref="shell-introduction#systemmenu">menú del sistema</gui> en la parte derecha de la barra superior.</p>
  </item>
  <item>
    <p>Select
    <gui>Mobile Broadband Off</gui>. The <gui>Mobile Broadband</gui> section of
      the menu will expand.</p>
      <note>
        <p>Si <gui>Red de banda ancha móvil</gui> no aparece en el menú del sistema, asegúrese de que su dispositivo no está conectado como dispositivo de almacenamiento masivo.</p>
      </note>
    </item>
    <item><p>Seleccione <gui>Conectar</gui>. Si se está conectando por primera vez, se ejecutará el asistente para <gui>Configurar la red de banda ancha móvil</gui>. La ventana que se abre muestra una lista de información requerida. Pulse en <gui style="button">Siguiente</gui>.</p></item>
    <item><p>Elija el país o la región de su proveedor de la lista. Pulse <gui style="button">Siguiente</gui>.</p></item>
    <item><p>Elija su proveedor de la lista. Pulse <gui style="button">Siguiente</gui>.</p></item>
    <item><p>Seleccione un plan de acuerdo al tipo de dispositivo que está conectando. Esto determinará el nombre del punto de acceso. Pulse en <gui style="button">Siguiente</gui>.</p></item>
    <item><p>Confirme la configuración que ha elegido pulsando <gui style="button">Aplicar</gui>. El asistentese cerrará y el panel de <gui>Red</gui> mostrará las propiedades de su conexión.</p></item>
  </steps>

</page>
