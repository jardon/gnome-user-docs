<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="es">

  <info>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-12"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definir las funciones de los botones y el tacto de la presión de la Wacom stylus.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Configurar la tableta</title>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Tableta Wacom</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Tableta Wacom</gui> para abrir el panel.</p>
      <note style="tip"><p>If no tablet is detected, you’ll be asked to
      <gui>Please plug in or turn on your Wacom tablet</gui>. Click
      <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
    </item>
    <item>
      <p>There is a section containing settings specific to each stylus,
      with the device name (the stylus class) and diagram at the top.</p>
      <note style="tip"><p>Si no se detecta ninguna tableta, se le pedirá que <gui>Mueva su «stylus» al borde de la tableta para configurarla</gui>.</p></note>
      <p>These settings can be adjusted:</p>
      <list>
        <item><p><gui>Sensibilidad de presión del lápiz:</gui> use el deslizador para ajustar la sensibilidad entre <gui>Suave</gui> y <gui>Firme</gui>.</p></item>
        <item><p>Button/Scroll Wheel configuration (these change to
        reflect the stylus). Click the menu next to each label to select one of
        these functions: Middle Mouse Button Click, Right Mouse Button Click,
        Back, or Forward.</p></item>
     </list>
    </item>
    <item>
      <p>Click <gui>Test Your Settings</gui> in the header bar to pop down a
      sketchpad where you can test your stylus settings.</p>
    </item>
  </steps>

</page>
