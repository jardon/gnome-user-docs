<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="es">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Cambie el navegador web predeterminado yendo a <gui>Detalles</gui> en la <gui>Configuración</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar qué navegador abre de manera predeterminada los sitios web</title>

  <p>Cuando pulsa en un enlace a una página web, en cualquier aplicación, se abre un navegador web automáticamente en esa página. Sin embargo, si tiene más de un navegador instalado, la página puede no abrirse en el navegador que quería. Para solucionar este problema, cambie el navegador predeterminado:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <input>Default Applications</input>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Aplicaciones predeterminadas</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Elija qué navegador web quiere usar para abrir enlaces, cambiando la opción <gui>Web</gui>.</p>
    </item>
  </steps>

  <p>Al abrir un navegador web diferente, le puede decir que no es el navegador predeterminado. Si esto sucede, pulse en el botón <gui>Cancelar</gui> (o similar) para que no trate de configurarse como el navegador predeterminado otra vez.</p>

</page>
