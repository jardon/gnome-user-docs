<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="es">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Contribuyentes al wiki de documentación de Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Para poder seguir los siguientes pasos de solución de problemas, puede que necesite detalles tales como el número de modelo de su adaptador inalámbrico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Solucionador de problemas de red inalámbrica</title>
  <subtitle>Recopilar información de su hardware de red</subtitle>

  <p>En este paso va a recopilar información sobre su dispositivo de red inalámbrica. La forma de solucionar muchos problemas de la red inalámbrica depende de la marca y el número de modelo del adaptador inalámbrico, por lo que tendrá que tomar nota de estos detalles. También puede ser útil contar con algunos de los elementos que venían con su equipo, tales como los discos de instalación del controlador del dispositivo. Busque los siguientes elementos, si todavía los tiene:</p>

  <list>
    <item>
      <p>La caja y las instrucciones de sus dispositivos inalámbricos (particularmente el manual de usuario de su enrutador)</p>
    </item>
    <item>
      <p>Un disco con el controlador de su adaptador inalámbrico (incluso si solo contiene el controlador para Windows)</p>
    </item>
    <item>
      <p>Los fabricantes y los números de modelo de su equipo, adaptador inalámbrico y enrutador. Generalmente, esta información se puede encontrar en la parte inferior/trasera del dispositivo.</p>
    </item>
    <item>
      <p>Cualquier número de versión que aparezca impreso en sus dispositivos inalámbricos o sus cajas. Pueden ser especialmente útiles, así que mire cuidadosamente.</p>
    </item>
    <item>
      <p>Cualquier cosa que haya en el disco del controlador y que identifique el dispositivo en sí, la versión de su «firmware» o los componentes (chipset) que usa.</p>
    </item>
  </list>

  <p>Si es posible, trate de obtener acceso a una conexión a Internet alternativa que funcione, para que pueda descargar el software y los controladores si es necesario. (Conectar el equipo directamente al enrutador con un cable de red Ethernet es una forma de conseguirlo, pero solo tiene que conectarlo cuando lo necesite.)</p>

  <p>Una vez que tenga tantos detalles de estos como le haya sido posible, pulse <gui>Siguiente</gui>.</p>

</page>
