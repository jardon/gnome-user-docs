<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="es">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Compruebe la cantidad de tinta o tóner en los cartuchos de impresión.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>¿Cómo puedo comprobar los niveles de tinta o tóner de mi impresora?</title>

  <p>Cómo comprobar cuánta tinta o tóner queda en su impresora depende del modelo y fabricante de su impresora y de los controladores y aplicaciones instaladas en su equipo.</p>

  <p>Algunas impresoras tienen una pantalla empotrada para mostrar los niveles de tinta y otra información.</p>

  <p>Algunas impresoras informan de sus niveles de tinta o tóner al equipo, lo que se puede encontrar en el panel <gui>Impresoras</gui> de la <app>Configuración</app>. El nivel de tinta se mostrará con los detalles de la impresoras si está disponible.</p>

  <p>Los controladores y herramientas de estado para la mayoría de las impresoras de HP los proporciona el proyecto imagen e impresióin en Linux de HP («HP Linux Imaging and Printing», HPLIP). Otros fabricantes pueden proporcionar controladores propietarios con características similares.</p>

  <p>Alternativamente, puede instalar una aplicación para comprobar o monitorizar los niveles de tinta. <app>Inkblot</app> muestra el estado de la tinta para la mayoría de las impresoras HP, Epson y Canon. Compruebe si su impresora está en la <link href="http://libinklevel.sourceforge.net/#supported">lista de modelos soportados</link>. Otra aplicación de niveles de tinta para Epson y algunas otras impresoras es <app>mktink</app>.</p>

  <p>Algunas impresoras aún no están muy bien soportadas en GNU/Linux y otras no están diseñadas para informar de sus niveles de tinta.</p>

</page>
