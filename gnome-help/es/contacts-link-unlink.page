<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="es">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Combinar información de varias fuentes para un contacto.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2022</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Enlazar y desenlazar contactos</title>

<section id="link-contacts">
  <title>Enlazar contactos</title>

  <p>Puede combinar contactos duplicados de su libreta de direcciones local y sus cuentas en línea en una entrada de <app>Contactos</app>. Esta característica le ayuda a mantener su libreta de direcciones organizada, con todos los detalles de un contacto en el mismo sitio.</p>

  <steps>
    <item>
      <p>Active el <em>modo de selección</em> pulsando el botón sobre la lista de contactos.</p>
    </item>
    <item>
      <p>Aparecerá una casilla junto a cada contacto. Marque las casillas juntos a los contactos que quiere enlazar.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">enlazar</gui> para enlazar los contactos seleccionados.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Desenlazar contactos</title>

  <p>Puede querer desenlazar contactos si accidentalmente enlazó contactos que no deberían estarlo.</p>

  <steps>
    <item>
      <p>Seleccione el contacto que quiere desenlazar en su lista de contactos.</p>
    </item>
    <item>
      <p>Pulse <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">ver más</span></media> en la esquina superior derecha de <app>Contactos</app>.</p>
    </item>
    <item>
      <p>Pulse <gui style="button">Desenlazar</gui> para desenlazar la entrada del contacto.</p>
    </item>
  </steps>

</section>

</page>
