<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="lv">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neļaut citām personām izmantot jūsu datoru, kad dodaties prom.</desc>
  </info>

  <title>Automātiski bloķēt ekrānu</title>
  
  <p>Kad atstājiet savu datoru, jums vajadzētu <link xref="shell-exit#lock-screen">bloķēt ekrānu</link>, lai neļautu citiem cilvēkiem strādāt ar jūsu datoru un piekļūt jūsu datnēm. Ja dažkārt aizmirstat bloķēt savu ekrānu, jūs varētu vēlēties automātiski bloķēt sava datora ekrānu pēc noteikta laika. Tas palīdz turēt datoru drošībā arī tad, kad to neizmantojat.</p>

  <note><p>Kad ekrāns ir bloķēts, jūsu lietotnes un sistēmas procesi turpinās darboties, bet jums vajadzēs ievadīt paroli, lai to turpinātu izmantot.</p></note>
  
  <steps>
    <title>Lai iestatītu laiku, pirms ekrāns tiek bloķēts:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Screen Lock</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p>Make sure <gui>Automatic Screen Lock</gui> is switched on, then select a
      length of time from the <gui>Automatic Screen Lock Delay</gui> drop-down list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Applications can present notifications to you that are still displayed
    on your lock screen. This is convenient, for example, to see if you have
    any email without unlocking your screen. If you’re concerned about other
    people seeing these notifications, switch <gui>Lock Screen Notifications</gui>
    to off. For further notification settings, refer to <link xref="shell-notifications"/>.</p>
  </note>

  <p>Kad ekrāns ir bloķēts un vēlaties to atbloķēt, spiediet <key>Esc</key>, vai velciet no ekrāna apakšas ar peli. Tad ievadiet savu paroli un spiediet <key>Enter</key> vai <gui>Atbloķēt</gui>. Varat arī sākt rakstīt paroli un bloķēšanas aizkars tiks automātiski pacelts.</p>

</page>
