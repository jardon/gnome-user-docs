<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-select" xml:lang="lv">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Drukāt tikai norādītās lappuses, vai lappuses noteiktā diapazonā.</desc>
  </info>

  <title>Drukāt tikai dažas lappuses</title>

  <p>Lai drukātu tikai dažas dokumenta lapas:</p>

  <steps>
    <item>
      <p>Atveriet drukāšanas dialoglodziņu, spiežot <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p><gui>Vispārīgi</gui> cilnē izvēlieties <gui>Lapas</gui> no <gui>Drukāt</gui> sadaļas.</p>
    </item>
    <item><p>Ievadiet lapas, kuras vēlaties drukāt teksta laukumā, atdalot ar komatiem. Izmantojiet domuzīmi, lai norādītu lapu diapazonu.</p></item>
  </steps>

  <note>
    <p>For example, if you enter “1,3,5-7” in the <gui>Pages</gui> text box,
    pages 1,3,5,6 and 7 will be printed.</p>
    <media type="image" src="figures/printing-select.png"/>
  </note>

</page>
