<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="lv">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Savienoties ar bezvadu tīklu, kas nav attēlots tīklu sarakstā.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Savienojaties ar slēptu bezvadu tīklu.</title>

<p>Ir iespējams izveidot bezvadu tīklu tā, lai tas būtu “paslēpts”. Paslēpts tīkls neuzrādīsies bezvadu tīklu sarakstā, kas ir redzami <gui>Tīklu</gui> iestatījumos. Lai savienotos ar slēptu bezvadu tīklu:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui>Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Spiediet <gui>Wi-Fi iestatījumi</gui>.</p>
  </item>
  <item><p>Press the menu button in the top-right corner of the window and
  select <gui>Connect to Hidden Network…</gui>.</p></item>
 <item>
  <p>Tajā logā, kas atveras, izvēlieties iepriekš savienoto slēpto tīklu, izmantojot izkrītošo sarakstu <gui>Savienojums</gui>, vai <gui>Jauns</gui>, lai izveidotu jaunu.</p>
 </item>
 <item>
  <p>Lai izveidotu jaunu savienojumu, ievadiet tīkla nosaukumu un izvēlieties bezvadu drošības tipu no <gui>Wi-Fi drošības</gui> izkrītošā saraksta.</p>
 </item>
 <item>
  <p>Ievadiet paroli vai citu drošības informāciju.</p>
 </item>
 <item>
  <p>Spiediet <gui>Savienot</gui>.</p>
 </item>
</steps>

  <p>Iespējams, jums varēs noskaidrot bezvadu bāzes stacijas vai maršrutētāja iestatījumus, lai uzzinātu tīkla nosaukumu. Ja nav zināms tīkla nosaukums (SSID), varat izmantot <em>BSSID</em> (Basic Service Set Identifier, piekļuves punkta MAC adrese), un izskatās aptuveni šādi: <gui>02:00:01:02:03:04</gui>, to parasti var atrast piekļuves punkta apakšpusē.</p>

  <p>Jums vajadzētu arī pārbaudīt drošības iestatījums bezvadu piekļuves punktam. Meklējiet tādas frāzes kā WEP un WPA.</p>

<note>
 <p>Jūs varētu domāt, ka slēpjot jūsu bezvadu tīklu jūs uzlabosiet drošību, liedzot tīklam pieslēgties tiem, kas par to neko nezina. Praksē tas tā nav, tīklu ir nedaudz grūtāk atrast, bet to joprojām var atklāt.</p>
</note>

</page>
