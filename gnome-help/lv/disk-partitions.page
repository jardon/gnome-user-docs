<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="disk-partitions" xml:lang="lv">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <desc>Saprotiet, kādi ir apjomi un nodalījumi, un izmantojiet disku utilītprogrammas, lai tos pārvaldītu.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

 <title>Pārvaldiet apjomus un nodalījumus</title>

  <p>Vārds <em>sējums</em> tiek izmantots, lai aprakstītu glabāšanas ierīces, piemēram, cieto disku. Tas var arī attiekties uz <em>daļu</em> no atmiņas šajā ierīcē, jo jūs varat sadalīt atmiņu vairākos gabalos. Dators padara šo atmiņu pieejamu caur jūsu datņu sistēmu, šo procesu sauc par <em>montēšana</em>. Montēts sējums var būt cietais disks, USB ierīce, DVD-RW diski, SD atmiņas kartes un citi datu nesēji. Ja sējums pašlaik ir montēts, jūs tajā varat lasīt (un, iespējams, rakstīt) datnes.</p>

  <p>Bieži montēts sējums tiek saukts par <em>nodalījumu</em>, lai gan tie ne vienmēr ir viens un tas pats. "Nodalījums" attiecas uz <em>fizisko</em> uzglabāšanas platību uz viena diska. Līdzko nodalījums ir uzmontēts, tas var tikt saukts par sējumu, jo caur to var piekļūt datnēm. Jūs varat iedomāties sējumus kā marķētus un pieejamus “veikala letes”, kas slēpj vajadzīgās “noliktavas” — nodalījumus un dziņus. </p>

<section id="manage">
 <title>Apskatiet un pārvaldiet sējumus un nodalījumus, izmantojot disku utilītprogrammas</title>

  <p>Jūs varat pārbaudīt un modificēt datora krātuvju sējumus ar disku utilītprogrammām.</p>

<steps>
  <item>
    <p>Atveriet <gui>Aktivitāšu</gui> apskatu un palaidiet <app>Disku</app> lietotni.</p>
  </item>
  <item>
    <p>Krātuvju ierīču sarakstā pa kreisi jūs atradīsiet cietos diskus, CD/DVD iekārtas un citas fiziskās ierīces. Spiediet uz ierīces, kuru vēlaties pārbaudīt.</p>
  </item>
  <item>
    <p>Labā rūts sniedz vizuālu sadalījumu sējumiem un nodalījumiem, kas atrodas izvēlētajā ierīcē. Tas ietver arī dažādus instrumentus, kurus var izmantot, lai pārvaldītu šos sējumi.</p>
    <p>Esiet uzmanīgs: ar šīm utilītprogrammām ir iespējams pilnībā izdzēst datus no diska.</p>
  </item>
</steps>

  <p>Jūsu datorā ir vismaz viens <em>galvenais</em> nodalījums un viens <em>maiņvietas</em> nodalījums. Maiņvietas nodalījumu izmanto operētājsistēmas atmiņas pārvaldībai un tas tiek reti montēts. Primārais nodalījums satur operētājsistēmu, programmas, iestatījumus un personīgās datnes. Šīs datnes var sadalīt arī starp vairākiem nodalījumiem drošības vai ērtības nolūkos.</p>

  <p>Vienam primārajam nodalījumam ir jāietver informācija, ko dators izmanto, lai uzsāktu savu darbību jeb <em>ielādētos</em>. Šī iemesla dēļ to reizēm sauc par ielādes nodalījumu vai ielādes sējumu. Lai noteiktu, vai sējums ir palaižams, izvēlieties nodalījumu un spiediet izvēlnes pogu rīkjoslā zem nodalījumu saraksta. Tad spiediet <gui>Rediģēt nodalījumu…</gui> un apskatiet tā <gui>Karogus</gui>. Ārējie datu nesēji, piemēram, USB ierīces un CD diski, var saturēt arī ielādējamu sējumu.</p>

</section>

</page>
