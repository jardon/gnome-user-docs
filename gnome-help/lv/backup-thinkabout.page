<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="lv">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.37.1" date="2020-07-30" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mapju saraksts, kur var atrast dokumentus, datnes un iestatījumus, kuriem jūs varētu vēlēties izveidot rezerves kopiju.</desc>
  </info>

  <title>Kur es varu atrast datnes, kurām vēlos veidot rezerves kopiju?</title>

  <p>Izlemt, kurām datnēm veidot rezerves kopiju un kā tos atrast, ir visgrūtākie soļi, kas jāveic, veidojot rezerves kopijas. Tālāk ir uzskatītas biežākās vietas ar svarīgākajām datnēm un iestatījumiem, kuriem jūs varat veidot rezerves kopijas.</p>

<list>
 <item>
  <p>Personīgās datnes (dokumenti, mūzika, fotogrāfijas un video)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs   and need to be translated.  You can find the correct translations for your   language here: http://translationproject.org/domain/xdg-user-dirs.html">These
  are usually stored in your home folder (<file>/home/your_name</file>).
  They could be in subfolders such as <file>Desktop</file>,
  <file>Documents</file>, <file>Pictures</file>, <file>Music</file> and
  <file>Videos</file>.</p>
  <p>Ja iekārtā, kurā veidojat rezerves kopijas, ir pietiekami daudz vietas (piemēram, ja tas ir ārējais cietais disks) apsveriet domu veidot rezerves kopiju visai Mājas mapei. Jūs varat uzzināt, cik daudz vietas aizņem jūsu mājas mape, izmantojot <app>Diska izmantošanas analizatoru</app>.</p>
 </item>

 <item>
  <p>Slēptās datnes</p>
  <p>Any file or folder name that starts with a period (.) is hidden by
  default. To view hidden files, press the menu button in the top-right corner
  of the window of <app>Files</app> and press <gui>Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq>. You can copy these to a
  backup location like any other file.</p>
 </item>

 <item>
  <p>Personīgie iestatījumi (darbvirsmas iestatījumi, motīvi un programmatūras iestatījumi)</p>
  <p>Lielākā daļa lietotņu glabā to iestatījumu datnes slēptajās mapēs jūsu Mājas mapē (skatīt augstāk informāciju par slēptajām datnēm).</p>
  <p>Lielākā daļa no jūsu programmu iestatījumiem tiks uzglabāti slēptās mapēs <file> .config</file> un <file>.local</file>jūsu Mājas mapē.</p>
 </item>

 <item>
  <p>Sistēmas iestatījumi</p>
  <p>Iestatījumi svarīgām sistēmas daļām netiek glabāti jūsu Mājas mapē. Ir vairākas vietas, kurās tie varētu tikt glabāti, taču lielākā daļā no tiem tiek glabāti <file>/etc</file> mapē. Parasti jums nevajadzēs dublēt šīs datnes mājas datoram. Ja darbiniet serveri, tad gan jums vajadzētu veidot rezerves kopijas to servisu datnēm, kuri tiek darbināti.</p>
 </item>
</list>

</page>
