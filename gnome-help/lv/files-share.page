<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="lv">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Viegli pārsūtiet datnes e-pasta kontaktiem no datņu pārvaldnieka.</desc>
  </info>

<title>Dalīties ar datnēm caur e-pastu</title>

<p>Jūs varat viegli koplietot datnes ar saviem kontaktiem ar e-pastu vai no datņu pārvaldnieka.</p>

  <note style="important">
    <p>Pirms sākat, pārliecinieties, ka uz šī datora ir uzinstalēts <app>Evolution</app> vai <app>Geary</app> un e-pasta konts ir nokonfigurēts.</p>
  </note>

<steps>
  <title>Sūtīt datni ar e-pastu:</title>
    <item>
      <p>Atveriet <app>Datnes</app> lietotni no <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskata.</p>
    </item>
  <item><p>Atrodiet datni, kuru vēlaties pārsūtīt.</p></item>
    <item>
      <p>Veiciet labo peles klikšķi uz datnes un izvēlieties <gui>Sūtīt uz…</gui>. Parādīsies e-pasta vēstules rakstīšanas logs ar pievienotu datni.</p>
    </item>
  <item><p>Spiediet <gui>Kam</gui>, lai izvēlētos kontaktu, vai ievadiet e-pasta adresi, uz kuru vēlaties sūtīt datni. Aizpildiet <gui>Temats</gui> un vēstules pamattekstu, un spiediet <gui>Sūtīt</gui>.</p></item>
</steps>

<note style="tip">
  <p>Jūs varat sūtīt vairākas datnes uzreiz. Izvēlieties visas vajadzīgās datnes, pieturot <key>Ctrl</key>, kamēr izvēlaties datnes, un pēc tam ar peles labo pogu veiciet klikšķi uz jebkura no tām.</p>
</note>

</page>
