<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="lv">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jūs varat ierakstīties savā sistēmā, izmantojot atbalstītu pirkstu nospiedumu skeneri, nevis rakstot paroli.</desc>
  </info>

  <title>Ierakstīties ar pirkstu nospiedumu</title>

  <p>Ja jūsu sistēmai ir atbalstīts pirkstu nospiedumu skeneris, jūs varat ierakstīt savus pirkstu nospiedumus un izmantot tos, lai ierakstītos sistēmā.</p>

<section id="record">
  <title>Ierakstīt ar pirkstu nospiedumu</title>

  <p>Pirms varat ierakstīties ar saviem pirkstu nospiedumiem, tos vajag ierakstīt, lai sistēma var jūs atpazīt.</p>

  <note style="tip">
    <p>Ja jūsu āda ir par sausu, var rasties grūtības ar pirkstu nospiedumu ierakstīšanu. Tādā gadījumā mazliet samitriniet pirkstus, noslaukiet tos ar tīru drānu, kas neatstāj pūkas, un mēģiniet vēlreiz.</p>
  </note>

  <p>Jums ir jābūt <link xref="user-admin-explain">administratora tiesībām</link>, lai rediģētu ne savējos kontus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Lietotāji</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Lietotāji</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet <gui>Deaktivēts</gui>, pie <gui>Ierakstīšanās ar pirkstu nospiedumu</gui>, lai pievienotu pirkstu nospiedumu pie izvēlētā konta. Ja pievienojat pirkstu nospiedumu citam lietotājam, jums vajadzēs <gui>Atslēgt</gui> paneli.</p>
    </item>
    <item>
      <p>Izvēlieties pirkstu, kuru vēlaties izmantot autentifikācijai un spiediet <gui style="button">Nākamais</gui>.</p>
    </item>
    <item>
      <p>Seko instrukcijām dialoglodziņā un velc pirkstu <em>vidēji ātri</em> pāri pirkstu nospiedumu lasītājam. Kad datoram būs labs pirkstu nospiedumu ieraksts, redzēsiet paziņojumu <gui>Darīts!</gui></p>
    </item>
    <item>
      <p>Spiediet <gui>Nākamais</gui>. Redzēsiet apstiprinājumu, ka pirksta nospiedums sekmīgi saglabāts. Spiediet <gui>Aizvērt</gui>, lai pabeigtu.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Pārbaudiet vai pirkstu nospiedumi tiešām strādā</title>

  <p>Tagad pārbaudiet, vai jauniestatītā ierakstīšanās ar pirksta nospiedumu strādā. Arī tad, ja saglabājāt pirksta nospiedumu, vēl joprojām varat ierakstīties ar paroli.</p>

  <steps>
    <item>
      <p>Saglabājiet atvērto darbu un <link xref="shell-exit#logout">izrakstieties</link>.</p>
    </item>
    <item>
      <p>Ierakstīšanās ekrānā izvēlieties no saraksta savu vārdu. Parādīsies paroles ievades forma.</p>
    </item>
    <item>
      <p>Tā vietā, lai rakstītu paroli, jūs varat vilkt savu pirkstu pāri pirkstu nospiedumu lasītājam.</p>
    </item>
  </steps>

</section>

</page>
