<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="lv">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>GNOME dokumentācijas projekts</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pievienot jaunus lietotājus, lai cilvēki varētu ierakstīties sistēmā un lietot datoru.</desc>
  </info>

  <title>Pievienot jaunu lietotāja kontu</title>

  <p>Jūs varat pievienot datoram vairākus lietotāja kontus. Izveidojiet vienu kontu katram cilvēkam jūsu mājās vai uzņēmumā. Katram lietotājam ir sava mājas mape, dokumenti un iestatījumi.</p>

  <p>Jums ir jābūt <link xref="user-admin-explain">administratora tiesībām</link>, lai pievienotu lietotāju kontus.</p>

  <steps>
    <item>
      <p>Atveriet <gui xref="shell-introduction#activities">Aktivitāšu</gui> pārskatu un sāciet rakstīt <gui>Lietotāji</gui>.</p>
    </item>
    <item>
      <p>Spiediet <gui>Lietotāji</gui>, lai atvērtu paneli.</p>
    </item>
    <item>
      <p>Spiediet pogu <gui style="button">Atslēgt</gui> un ievadiet savu  paroli.</p>
    </item>
    <item>
      <p>Press the <gui style="button">+ Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>Ja gribat, lai jaunajam lietotājam būtu <link xref="user-admin-explain">administratīva pieeja</link> datoram, izvēlēties <gui>Administratora</gui> konta tipu.</p>
      <p>Administratori var pievienot un dzēst lietotājus, instalēt programmatūru un draiverus, nomainīt datumu un laiku u.tml.</p>
    </item>
    <item>
      <p>Ievadiet jaunā lietotāja pilno vārdu. Automātiski tiks ieteikts atbilstošs lietotājvārds. Ja nepatīk piedāvātais lietotājvārds, varat to nomainīt.</p>
    </item>
    <item>
      <p>You can choose to set a password for the new user, or let them set it
      themselves on their first login. If you choose to set the password now,
      you can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
      <p>To connect the user to a network domain, click
      <gui>Enterprise Login</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Add</gui>. When the user has been added,
      <gui>Parental Controls</gui> and <gui>Language</gui> settings can be
      adjusted.</p>
    </item>
  </steps>

  <p>Ja vēlaties mainīt paroli pēc konta izveidošanas, izvēlieties kontu, <gui style="button">Atslēdziet</gui> paneli un spiediet uz pašreizējās paroles statusa.</p>

  <note>
    <p>In the <gui>Users</gui> panel, you can click the image next to the
    user’s name to the right to set an image for the account. This image will
    be shown in the login window. The system provides some stock photos you can
    use, or you can select your own or take a picture with your webcam.</p>
  </note>

</page>
