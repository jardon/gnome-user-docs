<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="lv">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Tuvināt ekrānu, lai varētu vieglāk saskatīt lietas.</desc>
  </info>

  <title>Palielināt ekrāna laukumu</title>

  <p>Ekrāna palielināšana ir atšķirīga no vienkārša <link xref="a11y-font-size">teksta izmēra</link> palielināšanas. Šī iespēja ir kā skatīties cauri palielināmajam stiklam, atļaujot jums to pārvietot, palielinot konkrēto ekrāna daļu.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Spiediet uz <gui>Tālummaiņa</gui> sadaļā <gui>Redzēšana</gui>.</p>
    </item>
    <item>
      <p>Switch the <gui>Zoom</gui> switch in the top-right corner of the
      <gui>Zoom Options</gui> window to on.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Jūs tagad varat pārvietot ekrāna laukumu. Novietojot savu peles kursoru ekrāna malās, jūs pārvietosiet palielināto laukumu dažādos virzienos, ļaujot jums aplūkot laukumu pēc jūsu izvēles.</p>

  <note style="tip">
    <p>Jūs varat ātri ieslēgt un izslēgt palielinājumu, spiežot uz <link xref="a11y-icon">pieejamības ikonas</link> augšējā joslā un izvēloties <gui>Tālummaiņa</gui>.</p>
  </note>

  <p>Jūs varat mainīt palielinājuma koeficientu, peles izsekošanu un palielinājuma skata novietojumu uz ekrāna. Mainiet šos iestatījumus cilnē <gui>palielinātājs</gui> logā <gui>Tuvināšanas opcijas</gui>.</p>

  <p>Jūs varat aktivēt krustiņu, lai palīdzētu atrast peles vai skārienpaliktņa rādītāju. Lai ieslēgtu to un pielāgotu tā garumu, krāsu un biezumu,  ejiet uz cilni <gui>Krustiņš</gui> iestatījumu logā <gui>Tuvināšana</gui>.</p>

  <p>Jūs varat pārslēgties uz inversu video vai <gui>Balts uz melna</gui>, un pielāgot palielinātāja gaišumu, kontrastu un pelēktoņu opcijas. Šo opciju kombinācija noder cilvēkiem ar vāju redzi, fotofobiju, vai vienkārši datora izmantošanai nelabvēlīgā apgaismojumā. Izvēlieties <gui>Krāsu efektu</gui> cilni iestatījumu logā <gui>Tālummaiņa</gui>, lai aktivētu vai mainītu šīs opcijas.</p>

</page>
