<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-exit" xml:lang="as">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="power"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-09-15" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.24.2" date="2017-06-11" status="candidate"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>আন্দ্ৰে ক্লেপাৰ</name>
      <email>ak-47@gmx.net</email>
    </credit>
    <credit type="author">
      <name>আলেজান্ড্ৰে ফ্ৰাঙ্কে</name>
      <email>afranke@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David Faour</name>
      <email>dfaour.gnome@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপোনাৰ ব্যৱহাৰকাৰী একাওন্ট কেনেকৈ প্ৰস্থান কৰিব শিকক, লগ আউট কৰি, ব্যৱহাৰকাৰী পৰিবৰ্তন কৰি, আৰু ইত্যাদি।</desc>
    <!-- Should this be a guide which links to other topics? -->
  </info>

  <title>Log out, power off or switch users</title>

  <p>When you have finished using your computer, you can turn it off, suspend
  it (to save power), or leave it powered on and log out.</p>

<section id="logout">
  <info>
    <link type="seealso" xref="user-add"/>
  </info>

  <title>লগ আউট কৰক অথবা ব্যৱহাৰকাৰী পৰিবৰ্তন কৰক</title>

  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-exit-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>ব্যৱহাৰকাৰী মেনু</p>
      </media>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-exit-classic-expanded.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>ব্যৱহাৰকাৰী মেনু</p>
      </media>
    </if:when>
  </if:choose>

  <p>অন্য ব্যৱহাৰকাৰীক আপোনা কমপিউটাৰ ব্যৱহাৰ কৰাৰ অনুমতি দিবলে, আপুনি লগ আউট কৰিব পাৰে, অথবা নিজকে লগ্গড্ ইন থৈ ব্যৱহাৰকাৰী সলনি কৰিব পাৰে। যদি আপুনি ব্যৱহাৰকাৰী সলনি কৰে, আপোনাৰ সকলো এপ্লিকেচন চলি থাকিব, আৰু আপুনি পুনৰ লগ কৰোতে আপোনাৰ সকলো কাৰ্য্য আপুনি যেনেকৈ এৰিছিল তেনেকৈ থাকিব।</p>

  <p>To <gui>Log Out</gui> or <gui>Switch User</gui>, click the
  <link xref="shell-introduction#systemmenu">system menu</link> on the right
  side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select the correct option.</p>

  <note if:test="!platform:gnome-classic">
    <p><gui>লগ আউট কৰক</gui> আৰু <gui>ব্যৱহাৰকাৰী পৰিবৰ্তন কৰক</gui> প্ৰৱিষ্টিসমূহ মেনুত তেতিয়াহে উপস্থিত হয় যেতিয়া আপোনাৰ চিস্টেমত এটাৰ অধিক ব্যৱহাৰকাৰী একাওন্ট থাকে।</p>
  </note>

  <note if:test="platform:gnome-classic">
    <p><gui>ব্যৱহাৰকাৰী পৰিবৰ্তন কৰক</gui> প্ৰৱিষ্টি মেনুত কেৱল তেতিয়াহে আহে যেতিয়া আপোনাৰ চিস্টেমত এটাতকৈ অধিক ব্যৱহাৰকাৰী একাওন্ট থাকে।</p>
  </note>

</section>

<section id="lock-screen">
  <info>
    <link type="seealso" xref="session-screenlocks"/>
  </info>

  <title>পৰ্দা লক কৰক</title>

  <p>If you’re leaving your computer for a short time, you should lock your
  screen to prevent other people from accessing your files or running
  applications. When you return, you will see the
  <link xref="shell-lockscreen">lock screen</link>. Enter your
  password to log back in. If you don’t lock your screen, it will lock
  automatically after a certain amount of time.</p>

  <p>To lock your screen, click the system menu on the right side of the top
  bar and click the
    <media type="image" its:translate="no" src="figures/system-lock-screen-symbolic.svg">
      Lock
    </media>
  button.</p>

  <p>When your screen is locked, other users can log in to their own accounts
  by clicking <gui>Log in as another user</gui> at the bottom right of the login
  screen. You can switch back to your desktop when they are finished.</p>

</section>

<section id="suspend">
  <info>
    <link type="seealso" xref="power-suspend"/>
  </info>

  <title>স্থগিত কৰক</title>

  <p>To save power, suspend your computer when you are not using it. If you use
  a laptop, the system, by default, suspends your computer automatically when
  you close the lid.
  This saves your state to your computer’s memory and powers off most of the
  computer’s functions. A very small amount of power is still used during
  suspend.</p>

  <p>To suspend your computer manually, click the system menu on the right side
  of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select <gui>Suspend</gui>.</p>

</section>

<section id="shutdown">
<!--<info>
  <link type="seealso" xref="power-off"/>
</info>-->

  <title>বন্ধ কৰক অথবা পুনাৰম্ভ কৰক</title>

  <p>If you want to power off your computer entirely, or do a full restart,
  click the system menu on the right side of the top bar, click the
    <media type="image" its:translate="no" src="figures/system-shutdown-symbolic.svg">
      Shutdown
    </media>
  button, and select either <gui>Restart…</gui> or <gui>Power Off…</gui>.</p>

  <p>If there are other users logged in, you may not be allowed to power off or
  restart the computer because this will end their sessions.  If you are an
  administrative user, you may be asked for your password to power off.</p>

  <note style="tip">
    <p>আপুনি আপোনাৰ কমপিউটাৰ বন্ধ কৰিব খোজিব পাৰে যদি আপুনি ইয়াক স্থানান্তৰ কৰিব খোজে আৰু আপোনাৰ ওচৰত এটা বেটাৰি নাই, যদি আপোনাৰ বেটাৰি কম আছে অথবা ভালকৈ চাৰ্জ নহয়। এটা বন্ধ কমপিউটাৰে স্থগিত কমপিউটাৰতকৈ <link xref="power-batterylife">কম শক্তি</link> ব্যৱহাৰ কৰে।</p>
  </note>

</section>

</page>
