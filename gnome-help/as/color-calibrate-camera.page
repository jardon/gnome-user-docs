<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="as">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>সঠিক ৰঙবোৰ লবলৈ আপোনাৰ কেমেৰা মানাংকন কৰাটো গুৰুত্বপূৰ্ণ।</desc>

    <credit type="author">
      <name>ৰিচাৰ্ড হিউগ্চ</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>মই মোৰ কেমেৰা কেনেকৈ মানাংকন কৰিম?</title>

  <p>
    Camera devices are calibrated by taking a photograph of a target
    under the desired lighting conditions.
    By converting the RAW file to a TIFF file, it can be used to
    calibrate the camera device in the <gui>Color</gui> panel.
  </p>
  <p>আপুনি TIFF ফাইল ক্ৰপ কৰিব লাগিব যাতে কেৱল লক্ষ্য দৃশ্যমান থাকে। বগা অথবা কলা প্ৰান্তসমূহ দৃশ্যমান থকাটো সুনিশ্চিত কৰক। মানাংকন কাম নকৰিব যদি ছবি উলোটা অথবা অধিক পৰিমাণত বিকৃত।</p>

  <note style="tip">
    <p>
      The resulting profile is only valid under the lighting condition
      that you acquired the original image from.
      This means you might need to profile several times for
      <em>studio</em>, <em>bright sunlight</em> and <em>cloudy</em>
      lighting conditions.
    </p>
  </note>

</page>
