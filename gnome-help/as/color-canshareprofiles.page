<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="as">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>ৰঙৰ আলেখ্যসমূহ অংশীদাৰী কৰাটো কেতিয়াও এটা ভাল বুদ্ধি নহয় যিহেতু হাৰ্ডৱেৰ সময়ৰ সৈতে পৰিবৰ্তন হয়।</desc>

    <credit type="author">
      <name>ৰিচাৰ্ড হিউগ্চ</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>মই মোৰ ৰঙৰ আলেখ্য অংশীদাৰী কৰিব পাৰিম নে?</title>

  <p>
    Color profiles that you have created yourself are specific to the
    hardware and lighting conditions that you calibrated for.
    A display that has been powered for a few hundred hours
    is going to have a very different color profile to a similar display
    with the next serial number that has been lit for a thousand hours.
  </p>
  <p>
    This means if you share your color profile with somebody, you might
    be getting them <em>closer</em> to calibration, but it’s misleading
    at best to say that their display is calibrated.
  </p>
  <p>
    Similarly, unless everyone has recommended controlled lighting
    (no sunlight from windows, black walls, daylight bulbs etc.) in a
    room where viewing and editing images takes place, sharing a profile
    that you created in your own specific lighting conditions doesn’t make
    a lot of sense.
  </p>

  <note style="warning">
    <p>আপুনি বিক্ৰেতা ৱেবছাইটসমূহৰ পৰা ডাউনল'ড কৰা আলেখ্যসমূহ অথবা আপোনাৰ বাবে সৃষ্টি কৰা আলেখ্যসমূহৰ পুনৰ বিতৰন অৱস্থা ভালকৈ নিৰীক্ষণ কৰিব লাগে।</p>
  </note>

</page>
