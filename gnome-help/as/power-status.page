<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-status" xml:lang="as">

  <info>

    <link type="guide" xref="power" group="#first"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-batterylife"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display the status of the battery and connected devices.</desc>
  </info>

  <title>Check the battery status</title>

  <steps>

    <title>Display the status of the battery and connected devices</title>

    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel. The status of
      <gui>Batteries</gui> and known <gui>Devices</gui> is displayed.</p>
    </item>

  </steps>

    <p>If an internal battery is detected, the <gui>Batteries</gui> section
    displays the status of one or more laptop batteries. The indicator bar
    shows the percent charged, as well as time until fully charged if plugged
    in, and time remaining when running on battery power.</p>

    <p>The <gui>Devices</gui> section displays the status of connected
    devices.</p>
    
    <p>The <link xref="status-icons#batteryicons">status icon</link> in the top
    bar shows the charge level of the main internal battery, and whether it is
    currently charging or not. It can also display the charge as a
    <link xref="power-percentage">percentage</link>.</p>

</page>
