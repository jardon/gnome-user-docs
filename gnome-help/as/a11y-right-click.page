<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-right-click" xml:lang="as">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>ৰাইট-ক্লিক কৰিবলৈ বাঁও মাউছ বুটাম টিপক আৰু ধৰি থওক।</desc>
  </info>

  <title>এটা ৰাইট মাউছ ক্লিক চিমুলেইট কৰক</title>

  <p>আপুনি বাঁও মাউছ বুটাম ধৰি ৰাখি ৰাইট-ক্লিক কৰিব পাৰিব। ই উপযোগী যদি আপুনি এটা হাতৰ আঙুলিবোৰ সূকীয়াভাৱে লৰাব অসুবিধা হয়, অথবা যদি আপোনাৰ পইন্টিং ডিভাইচৰ কেৱল এটা বুটাম আছে।</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Click Assist</gui> in the <gui>Pointing &amp; Clicking</gui>
      section.</p>
    </item>
    <item>
      <p>In the <gui>Click Assist</gui> window, switch the <gui>Simulated
      Secondary Click</gui> switch to on.</p>
    </item>
  </steps>

  <p>You can change how long you must hold down the left mouse button before it
  is registered as a right click by changing the <gui>Acceptance
  delay</gui>.</p>

  <p>To right-click with simulated secondary click, hold down the left mouse
  button where you would normally right-click, then release. The pointer fills
  with a different color as you hold down the left mouse button. Once it will
  change this color entirely, release the mouse button to right-click.</p>

  <p>Some special pointers, such as the resize pointers, do not change colors.
  You can still use simulated secondary click as normal, even if you do not get
  visual feedback from the pointer.</p>

  <p>যদি আপুনি <link xref="mouse-mousekeys">মাউছ কি'সমূহ</link> ব্যৱহাৰ কৰে, ই আপোনাক কিপেডত <key>5</key> কি ধৰি থৈ ৰাইট-ক্লিক কৰাৰ অনুমতি দিয়ে।</p>

  <note>
    <p>In the <gui>Activities</gui> overview, you are always able to long-press
    to right-click, even with this feature disabled. Long-press works slightly
    differently in the overview: you do not have to release the button to
    right-click.</p>
  </note>

</page>
