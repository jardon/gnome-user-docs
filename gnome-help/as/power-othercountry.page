<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="as">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>আপোনাৰ কমপিউটাৰ কাম কৰিব, কিন্তু আপোনাক এটা ভিন্ন পাৱাৰ কেবুল অথবা এটা ট্ৰেভেল এডাপ্টাৰৰ প্ৰয়োজন হব।</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>মোৰ কমপিউটাৰ এটা শক্তি যোগানৰ সৈতে অন্য দেশত কাম কৰিব নে?</title>

<p>Different countries use power supplies at different voltages (usually 110V
or 220-240V) and AC frequencies (usually 50 Hz or 60 Hz). Your computer should
work with a power supply in a different country as long as you have
an appropriate power adapter. You may also need to flip a switch.</p>

<p>If you have a laptop, all you should need to do is get the right plug for
your power adapter. Some laptops come packaged with more than one plug for
their adapter, so you may already have the right one. If not, plugging your
existing one into a standard travel adapter will suffice.</p>

<p>If you have a desktop computer, you can also get a cable with a different
plug, or use a travel adapter. In this case, however, you may need to change
the voltage switch on the computer’s power supply, if there is one. Many
computers do not have a switch like this, and will happily work with either
voltage. Look at the back of the computer and find the socket that the power
cable plugs into. Somewhere nearby, there may be a small switch marked “110V”
or “230V” (for example). Switch it if you need to.</p>

<note style="warning">
  <p>পাৱাৰ কেবুল সলনি কৰোতে অথবা ট্ৰেভেল এডাপ্টাৰ ব্যৱহাৰ কৰোতে সাৱধান হব। যদি সম্ভব প্ৰথমতে সকলো বন্ধ কৰিব।</p>
</note>

</page>
