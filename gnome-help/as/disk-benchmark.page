<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="as">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>নাটালিয়া ৰুজ লিভা</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>মাইকেল হিল</name>
     <email>mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>আপোনাৰ হাৰ্ড ডিস্কৰ দ্ৰুততা নিৰীক্ষন কৰিবলৈ বেঞ্চমাৰ্ক চলাওক।</desc>
  </info>

<title>আপোনাৰ হাৰ্ড ডিস্কৰ পৰিৱেশন পৰিক্ষা কৰক</title>

  <p>আপোনাৰ হাৰ্ড ডিস্কৰ গতি পৰিক্ষা কৰিবলৈ:</p>

  <steps>
    <item>
      <p>Open <app>Disks</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Choose the disk from the list in the left pane.</p>
    </item>
    <item>
      <p>Click the menu button and select <gui>Benchmark disk…</gui> from the
      menu.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmark…</gui> and adjust the <gui>Transfer
      Rate</gui> and <gui>Access Time</gui> parameters as desired.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmarking…</gui> to test how fast data can be read
      from the disk. <link xref="user-admin-explain">Administrative
      privileges</link> may be required. Enter your password, or the password
      for the requested administrator account.</p>
      <note>
        <p>If <gui>Perform write-benchmark</gui> is checked, the benchmark
        will test how fast data can be read from and written to the disk. This
        will take longer to complete.</p>
      </note>
    </item>
  </steps>

  <p>When the test is finished, the results will appear on the graph. The green
  points and connecting lines indicate the samples taken; these correspond to
  the right axis, showing access time, plotted against the bottom axis,
  representing percentage time elapsed during the benchmark. The blue line
  represents read rates, while the red line represents write rates; these are
  shown as access data rates on the left axis, plotted against percentage of
  the disk traveled, from the outside to the spindle, along the bottom
  axis.</p>

  <p>গ্ৰাফৰ তলত, নূন্যতম, সৰ্বাধিক আৰু গড় পঢ়া আৰু লিখা হাৰ, গড় অভিগম সময় আৰু সৰ্বশেষ বেঞ্চমাৰ্ক পৰিক্ষাৰ পিছত পাৰ হোৱা সময়ৰ বাবে মান প্ৰদৰ্শন কৰা হয়।</p>

</page>
