<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="as">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="candidate"/>
    <revision pkgversion="3.38.4" date="2021-03-07" status="candidate"/>

    <credit type="author">
      <name>জিম কেম্পবেল</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>শ্বণ মেকেন্স</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>কিমান সঘনে আপোনাৰ আবৰ্জনা আৰু অস্থায়ী ফাইলসমূহ আপোনাৰ কমপিউটাৰৰ পৰা আতৰোৱা হব চাওক।</desc>
  </info>

  <title>আবৰ্জনা &amp; অস্থায়ী ফাইলসমূহ বাদ দিয়ক</title>

  <p>আপোনাৰ আবৰ্জনা আৰু অস্থায়ী ফাইলসমূহ পৰিষ্কাৰ কৰিলে আপোনাৰ কমপিউটাৰৰ পৰা অপ্ৰয়োজনীয় ফাইলসমূহ আতৰ হয়, আৰু আপোনাৰ হাৰ্ড ড্ৰাইভত অধিক স্থান মুক্ত হয়। আপুনি আপোনাৰ আবৰ্জনা হস্তচালিতভাৱে খালি কৰি আপোনাৰ অস্থায়ী ফাইলসমূহ পৰিষ্কাৰ কৰিব পাৰিব, কিন্তু আপুনি আপোনাৰ কমপিউটাৰক আপোনাৰ বাবে ইয়াক স্বচালিতভাৱে কৰিবলৈ সংহতি কৰিব পাৰিব।</p>

  <p>Temporary files are files created automatically by applications in the
  background. They can increase performance by providing a copy of data that
  was downloaded or computed.</p>

  <steps>
    <title>Automatically empty your trash and clear temporary files</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>File History &amp; Trash</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>File History &amp; Trash</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch on one or both of <gui>Automatically Delete Trash Content</gui>
      or <gui>Automatically Delete Temporary Files</gui>.</p>
    </item>
    <item>
      <p>Set how often you would like your <em>Trash</em>  and
      <em>Temporary Files</em> to be purged by changing the
      <gui>Automatically Delete Period</gui> value.</p>
    </item>
    <item>
      <p>Use the <gui>Empty Trash…</gui> or <gui>Delete Temporary Files…</gui>
      buttons to perform these actions immediately.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can delete files immediately and permanently without using the Trash.
    See <link xref="files-delete#permanent"/> for information.</p>
  </note>

</page>
