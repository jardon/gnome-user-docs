<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-dwellclick" xml:lang="as">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>শ্বণ মেকেন্স</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>একাটেৰিনা গেৰাচিমভা</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>The <gui>Hover Click</gui> (Dwell Click) feature allows you to click
    by holding the mouse still.</desc>
  </info>

  <title>হভাৰ কৰি ক্লিকিং চিমুলেইট কৰক</title>

  <p>You can click or drag simply by hovering your mouse pointer over a control
  or object on the screen. This is useful if you find it difficult to move the
  mouse and click at the same time. This feature is called <gui>Hover
  Click</gui> or Dwell Click.</p>

  <p>When <gui>Hover Click</gui> is enabled, you can move your mouse pointer
  over a control, let go of the mouse, and then wait for a while before the
  button will be clicked for you.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Accessibility</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Accessibility</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui>Click Assist</gui> in the <gui>Pointing &amp;
      Clicking</gui> section.</p>
    </item>
    <item>
      <p>Switch <gui>Hover Click</gui> to on.</p>
    </item>
  </steps>

  <p><gui>হভাৰ ক্লিক</gui> উইন্ডো খোলিব, আৰু আপোনাৰ সকলো উইন্ডোৰ ওপৰত থাকিব। আপুনি ইয়াক হভাৰ কৰোতে কি ধৰণৰ ক্লিক হব লাগে বাছইবলৈ ব্যৱহাৰ কৰিব পাৰিব। উদাহৰণস্বৰূপ, যদি আপুনি <gui>দ্বিতীয় ক্লিক</gui> নিৰ্বাচন কৰে, আপুনি হভাৰ কৰোতে ৰাইট-ক্লিক কৰিব। আপুনি দুবাৰ-ক্লিক , ৰাইট-ক্লিক, অথবা ড্ৰেগ কৰাৰ পিছত, স্বচালিতভাৱে ক্লিকিংলৈ ঘুৰি যাব।</p>

  <p>When you hover your mouse pointer over a button and do not move it, it will
  gradually change color. When it has fully changed color, the button will be
  clicked.</p>

  <p>ক্লিক কৰাৰ আগতে মাউছ পোইন্টাৰ কিমান দেৰি ধৰি ৰাখিব লাগিব পৰিবৰ্তন কৰিবলৈ <gui>বিলম্ব</gui> সংহতি সমন্বয় কৰক।</p>

  <p>You do not need to hold the mouse perfectly still when hovering to click.
  The pointer is allowed to move a little bit and will still click after a
  while. If it moves too much, however, the click will not happen.</p>

  <p>পইন্টাৰ কিমান লৰিব পাৰিব আৰু তথপিও তাক হভাৰিং কৰা বুলি ধৰা হব পৰিবৰ্তন কৰিবলৈ <gui>স্পন্দন ডেউৰী</gui> সংহতি সমন্বয় কৰক।</p>

</page>
