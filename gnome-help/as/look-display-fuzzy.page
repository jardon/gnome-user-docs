<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="as">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    	<revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>GNOME তথ্যচিত্ৰ প্ৰকল্প</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>নাটালিয়া ৰুজ লিভা</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>ফিল বুল</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>মাইকেল হিল</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>শোভা ত্যাগী</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>পৰ্দাৰ বিভেদন হয়তো ভুলভাৱে সংহতি কৰা হৈছে।</desc>
  </info>

  <title>বস্তুবোৰ মোৰ পৰ্দাত কিয় ফাজি/পিক্সেলেটেড প্ৰদৰ্শিত হয়?</title>

  <p>The display resolution that is configured may not be the correct one for
  your screen. To solve this:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Try some of the <gui>Resolution</gui> options and select the one that
      makes the screen look better.</p>
    </item>
  </steps>

<section id="multihead">
  <title>কিয় একাধিক প্ৰদৰ্শন সংযোগ কৰা হয়</title>

  <p>If you have two displays connected to the computer (for example, a normal
  monitor and a projector), the displays might have different optimal, or
  <link xref="look-resolution#native">native</link>, resolutions.</p>

  <p>Using <link xref="display-dual-monitors#modes">Mirror</link> mode, you can
  display the same thing on two screens. Both screens use the same resolution,
  which may not match the native resolution of either screen, so the sharpness
  of the image may suffer on both screens.</p>

  <p>Using <link xref="display-dual-monitors#modes">Join Displays</link> mode,
  the resolution of each screen can be set independently, so they can both be
  set to their native resolution.</p>

</section>

</page>
