<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="hr">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Morate isključiti <gui>Učini dostupno ostalim korisnicima</gui> mogućnost u postavkama mrežnog povezivanja.</desc>
  </info>

  <title>Drugi korisnici ne smiju uređivati mrežna povezivanja</title>

  <p>Ako možete uređivati mrežno povezivanje a ostali korisnici vašeg računala ne mogu, morat ćete postaviti da povezivanje bude <em>dostupno svim korisnicima</em>. Time se svi korisnici računala mogu <em>povezati</em> pomoću tog povezivanja.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mreža</gui> za otvaranje panela.</p>
    </item>
    <item>
      <p>Odaberite <gui>Bežična mreža</gui> s popisa na lijevoj strani.</p>
    </item>
    <item>
      <p>Kliknite na <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">postavke</span></media> tipku za otvaranje pojedinosti povezivanja.</p>
    </item>
    <item>
      <p>Odaberie <gui>Pojedinosti</gui> karticu.</p>
    </item>
    <item>
      <p>Na dnu <gui>Pojedinosti</gui> kartice, Uključite <gui>Učini dostupno ostalim korisnicima</gui> mogućnost, kako bi omogućili drugim korisnicima korištenje mrežnih povezivanja.</p>
    </item>
    <item>
      <p>Kliknite na <gui style="button">Primijeni</gui> kako bi spremili promjene.</p>
    </item>
  </steps>

</page>
