<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="contacts-add-remove" xml:lang="hr">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-10-31" status="review"/>

    <credit type="author">
      <name>Lucie Hankey</name>
      <email>ldhankey@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dodavanje ili uklanjanje kontakta u lokalnom adresaru.</desc>

  </info>

<title>Dodavanje ili uklanjanje kontakta</title>

  <p>Kako bi dodali račun:</p>

  <steps>
    <item>
      <p>Pritisnite <gui style="button">+</gui> tipku.</p>
    </item>
    <item>
      <p>U <gui>Novi kontakt</gui> dijalogu, upišite naziv i informacije kontakta. Pritisnite padajući okvir pokraj svakog polja kako bi odabrali vrstu pojedinosti.</p>
    </item>
    <item>
      <p>Kako bi dodali više pojedinosti pritisnite <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Pogledaj više</span></media> mogućnost.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Dodaj</gui> za spremanje kontakta.</p>
    </item>
  </steps>

  <p>Kako bi uklonili kontakt:</p>

  <steps>
    <item>
      <p>Odaberite kontakt iz svojeg popisa kontakata.</p>
    </item>
    <item>
      <p>Pritisnite <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">Pogledaj više</span></media> tipku u traci zaglavlja u gornjem desnom kutu.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="menu item">Obriši</gui> mogućnost za uklanjanje kontakta.</p>
    </item>
   </steps>
    <p>Kako bi uklonili jedan ili više kontakta, odaberite okvire odabira pokraj kontakta kojeg želite obrisati i pritisnite <gui style="button">Ukloni</gui>.</p>
</page>
