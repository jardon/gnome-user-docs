<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-device-specific-pairing" xml:lang="hr">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>

    <credit type="author">
      <name>Bastien Nocera</name>
      <email>hadess@hadess.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uparivanje određenih uređaja s računalom.</desc>
  </info>

  <title>Upute uparivanja za određene uređaje</title>

  <p>Čak i ako uspijete pronaći priručnik za uređaj, možda neće sadržavati dovoljno informacija kako bi uparivanje bilo moguće. Evo pojedinosti za nekoliko uobičajenih uređaja.</p>

  <terms>
    <item>
      <title>PlayStation 5 upravljač za igre</title>
      <p>Držite pritisnutu <media its:translate="no" type="image" mime="image/svg" src="figures/ps-create.svg">Create</media> tipku, zatim pritisnite <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> tipku sve dok svjetlosna traka ne počne treperiti kako bi postao vidljiv i mogli ga uparili kao bilo koji drugi Bluetooth uređaj.</p>
    </item>
    <item>
      <title>PlayStation 4 upravljač za igre</title>
      <p>Korištenje <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> i "Share" kombinacija tipki za uparivanje upravljača igre isto se može koristiti kako bi upravljač igre postao vidljiv i upario se kao bilo koji drugi Bluetooth uređaj.</p>
      <p>Ti uređaji isto koriste "uparivanje kabelom". Priključite upravljače igre putem USB-a s otvorenim <gui>Bluetooth postavkama</gui> i uključenim Bluetoothom. Dobit ćete upit želite li postaviti te upravljače igre bez potrebe pritiska na <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> tipku. Isključite ih i pritisnite <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> tipku za korištenje putem Bluetootha.</p>
    </item>
    <item>
      <title>PlayStation 3 upravljač za igre</title>
      <p>Ti uređaji koriste "uparivanje kabelom". Priključite upravljače igre putem USB-a s otvorenim <gui>Bluetooth postavkama</gui> i uključenim Bluetoothom. Nakon pritiska na <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> tipku, dobit ćete upit želite li postaviti te upravljače igre. Isključite ih i pritisnite <media its:translate="no" type="image" mime="image/svg" src="figures/ps-button.svg">PS</media> tipku za korištenje putem Bluetootha.</p>
    </item>
    <item>
      <title>PlayStation 3 BD daljinski upravljač</title>
      <p>Istovremeno držite "Start" i "Enter" tipke oko 5 sekundi. Zatim možete odabrati daljinski upravljač na popisu uređaja kao i obično.</p>
    </item>
    <item>
      <title>Nintendo Wii and Wii U daljinski upravljači</title>
      <p>Koristite crvenu "Sync" tipku unutar odjeljka baterija za početak postupka uparivanja. Druge kombinacije tipki neće zadržati informacije o uparivanju, stoga bi morali sve ponoviti u kratkom roku. Zapamtite da pojedini softver poput emulatora konzola želi izravan pristup daljinskim upravljačima, i u tim slučajevima, ne bi ih trebali postavljati u Bluetooth panelu. Upute potražite u priručniku aplikacije.</p>
    </item>
    <item>
      <title>ION iCade</title>
      <p>Držite pritisnute donje 4 tipke i gornju bijelu tipku za početak postupka uparivanja. Kada se pojave upute za uparivanje, obavezno koristite samo crvene smjerove za unos kôda, nakon čega slijedi potvrda s bilo kojim od 2 bijele tipke krajnje desno od arkadne palice.</p>
    </item>
  </terms>

</page>
