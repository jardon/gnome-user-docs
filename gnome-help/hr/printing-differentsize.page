<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="hr">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Ispis dokumenta na drugačijoj veličini ili orijentaciji papira.</desc>
  </info>

  <title>Promjena veličine papira pri ispisu</title>

  <p>Ako želite promijeniti veličinu papira dokumenta (primjerice, ispisati PDF u formatu SAD pismo na A4 papiru), možete promijeniti format ispisa dokumenta.</p>

  <steps>
    <item>
      <p>Otvorite dijalog ispisa pritiskom na <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Odaberite karticu <gui>Postavke ispisa</gui>.</p>
    </item>
    <item>
      <p>U odjeljku <gui>Papir</gui> odaberite <gui>Veličina papira</gui> s padajućeg popisa.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Ispis</gui> za ispis dokumenta.</p>
    </item>
  </steps>

  <p>Možete koristiti padajući popis <gui>Orijentacija</gui> za odabir drugačije orijentacije:</p>

  <list>
    <item><p><gui>Uspravno</gui></p></item>
    <item><p><gui>Položeno</gui></p></item>
    <item><p><gui>Obrnuto uspravno</gui></p></item>
    <item><p><gui>Obrnuto položeno</gui></p></item>
  </list>

</page>
