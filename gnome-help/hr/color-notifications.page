<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-notifications" xml:lang="hr">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Može se dobiti obavijest ako je profil boja star i netočan.</desc>
  </info>

  <title>Mogu li se dobiti obavijest kada je profil boje netočan?</title>

  <p>Možete dobiti podsjetnik za ponovno kalibriranje svojeg uređaja nakon određenog vremena. Nažalost, bez ponovne kalibracije nije moguće utvrditi je li profil uređaja točan, stoga je najbolje redovito kalibriranje uređaja.</p>

  <p>Pojedine tvrtke imaju vrlo specifična pravila isteka kalibracije za profile, jer netočan profil boja može značajno utjecati na krajnji proizvod.</p>

  <p>Ako postavite pravilo isteka, a profil je stariji od pravila tada će se crveni trokut upozorenja prikazati u <gui>Boja</gui> panelu pokraj profila. Obavijest upozorenja će se prikazati svaki puta kada se prijavite na svoje računalo.</p>

  <p>Za postavljanje pravila zaslona i pisača, navedite najveće trajanje profila u danima:</p>

<screen its:translate="no">
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 90</input>
</screen>

</page>
