<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="hr">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Koristite analogni ili USB mikrofon i odaberite zadani ulazni uređaj.</desc>
  </info>

  <title>Korištenje vanjskog mikrofona</title>

  <p>Možete koristiti vanjski mikrofon za razgovor s prijateljima, razgovor s kolegama na poslu, snimanje glasa ili korištenje s drugim multimedijskim aplikacijama. Čak i ako vaše računalo ima ugrađen mikrofon ili web kameru s mikrofonom, zasebni mikrofon obično pruža bolju kvalitetu zvuka.</p>

  <p>Ako vaš mikrofon ima okrugli utikač, jednostavno ga priključite u odgovarajuću zvučnu utičnicu na računalu. Većina računala ima dvije utičnice: jednu za mikrofone i jednu za zvučnike. Ova utičnica je obično svjetlocrvene boje ili je popraćena slikom mikrofona. Mikrofoni uključeni u odgovarajuću utičnicu obično se koriste po zadanome. Ako ne, pogledajte upute u nastavku za odabir zadanog ulaznog uređaja.</p>

  <p>Ako imate USB mikrofon, priključite ga u bilo koji USB priključak na računalu. USB mikrofoni djeluju kao zasebni zvučni uređaji i možda ćete morati odrediti koji mikrofon se koristiti po zadanome.</p>

  <steps>
    <title>Odabir zadanog ulaza zvučnog uređaja</title>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite tipkati <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zvuk</gui> kako bi otvorili panel.</p>
    </item>
    <item>
      <p>U odjeljku <gui>Ulaz</gui>, odaberite uređaj koji želite koristiti. Indikator razine glasnoće ulaza trebao bi reagirati kada govorite.</p>
    </item>
  </steps>

  <p>Možete prilagoditi glasnoću zvuka i isključiti mikrofon s ovog panela.</p>

</page>
