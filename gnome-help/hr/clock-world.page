<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="hr">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prikaz vremena u drugim gradovima ispod kalendara u gornjoj traci.</desc>
  </info>

  <title>Dodavanje svjetskih satova</title>

  <p>Koristite aplikaciju <app>Satovi</app> za dodavanje vremena u drugim gradovima.</p>

  <note>
    <p>Ovo zahtijeva instaliranu <app>Satovi</app> aplikaciju.</p>
    <p>Većina distribucija dolazi s aplikacijom <app>Satovi</app> instaliranom po zadanome. Ako je vaša nema, možda ćete ju morati instalirati pomoću upravitelja paketima svoje distribucije.</p>
  </note>

  <p>Za dodavanje svjetskog sata:</p>

  <steps>
    <item>
      <p>Kliknite na sat na gornjoj traci.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Dodaj satove iz svijeta…</gui> ispod kalendara za pokretanje aplikacije <app>Satovi</app>.</p>

    <note>
       <p>Ako već imate jedan ili više svjetskih satova, kliknite na jedan i aplikacija <app>Satovi</app> će se pokrenuti.</p>
    </note>

    </item>
    <item>
      <p>U prozoru <app>Satovi</app> aplikacije, kliknite na <gui style="button">+</gui> tipku ili pritisnite <keyseq><key>Ctrl</key><key>N</key></keyseq> tipke za dodavanje novog grada.</p>
    </item>
    <item>
      <p>Započnite upisivati naziv grada u pretragu.</p>
    </item>
    <item>
      <p>Odaberite odgovarajući grad ili najbližu lokaciju s popisa.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Dodaj</gui> tipku za završetak dodavanja grada.</p>
    </item>
  </steps>

  <p>Više o mogućnostima aplikacije <app>Satovi</app> potražite u <link href="help:gnome-clocks">Satovi priručniku</link>.</p>

</page>
