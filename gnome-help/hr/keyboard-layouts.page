<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="hr">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dodavanje rasporeda tipkovnice i prebacivanje između njih.</desc>
  </info>

  <title>Korištenje alternativnih rasporeda tipkovnice</title>

  <p>Tipkovnice dolaze s tisućama različitih rasporeda za različite jezike. Čak i za pojedinačni jezik, često postoji više rasporeda tipkovnice, poput Dvorak raspored za engleski. Možete postaviti vašu tipkovnicu da koristi drugi raspored, bez obzira na slova i simbole naslikane na tipkama. To je korisno ako često prebacujete između više jezika.</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Postavke</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Postavke</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Tipkovnica</gui> u bočnoj traci za otvaranje panela.</p>
    </item>
    <item>
      <p>Kliknite na <gui>+</gui> tipku u <gui>Ulazni načini unosa</gui> odjeljku, odaberite jezik koji je povezan s rasporedom, zatim odaberite raspored i kliknite na <gui>Dodaj</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Ako postoji više korisničkih računa na vašem sustavu, postoji zasebni primjerak panela <gui>Jezik i regija</gui> za zaslon prijave. Kliknite na tipku <gui>Zaslon prijave</gui> u gornjem desnom kutu za prebacivanje između dva primjerka.</p>

    <p>Određene rijetko korištene varijante rasporeda nisu dostupne po zadanome kada kliknete <gui>+</gui> tipku. Kako bi učinili te ulazne načine dostupnima možete otvorit prozor terminala pritiskom <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq> i pokrenuti ovu naredbu:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Možete pogledati svaku sliku rasporeda odabirom u popisu <gui>Ulazni načini unosa</gui> i klikom na <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">pregled</span></media></gui></p>
  </note>

  <p>Određeni jezici omogućuju neke dodatne mogućnosti podešavanja. Možete identificirati te jezike zato jer imaju <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">pregled</span></media></gui> ikonu pokraj njih. Ako želite pristupiti tim dodatnim parametrima, odaberite jezik iz<gui>Ulazni načini unosa</gui> popisa i novu <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">osobitosti</span></media></gui> tipku dati će vam pristup dodatnim postavkama.</p>

  <p>Kada koristite više rasporeda, možete postaviti da svi prozori koriste isti raspored ili da svaki prozor koristi različiti raspored. Korištenje različitog rasporeda za svaki prozor je korisno, primjerice, ako pišete članak na drugom jeziku u prozoru za obradu teksta. Vaš odabir tipkovnice će biti zapamćen za svaki prozor kada se prebacivate između prozora. Pritisnite <gui style="button">Mogućnosti</gui> tipku za odabir o načinu upravljanja više rasporeda.</p>

  <p>Gornja traka će prikazati kratki identifikator trenutnog rasporeda, poput <gui>hr</gui> za standardni hrvatski raspored. Kliknite na identifikator rasporeda i odaberite raspored koji želite koristiti iz izbornika. Ako odabrani jezik ima dodatne postavke, biti će prikazane na dnu popisa dostupnih rasporeda. To vam daje brz prikaz vaših postavki. Isto tako možete otvoriti sliku s trenutnim rasporedom tipkovnice u informativnu svrhu.</p>

  <p>Najbrži način promjene na drugi raspored je korištenjem <gui>Prečaca tipkovnice</gui> <gui>Ulaznih načina</gui>. Ti prečaci otvaraju odabiratelja <gui>Ulaznih načina</gui> gdje se možete pomicati natrag i naprijed. Po zadanome, možete prebaciti na sljedeći ulazni izvor sa <keyseq><key xref="keyboard-key-super">Super</key><key>Space</key></keyseq> i na prijašnji sa<keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq>. Te prečace možete promijeniti u postavkama <gui>Tipkovnice</gui>u odjeljku <guiseq><gui>Prečaci tipkovnice</gui><gui>Pogledaj i prilagodi prečace</gui>u <gui>Tipkanje</gui></guiseq> postavci.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
