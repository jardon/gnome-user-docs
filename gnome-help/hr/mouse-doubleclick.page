<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task a11y" id="mouse-doubleclick" xml:lang="hr">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9" date="2013-10-16" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="20156-06-15" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Upravljanje koliko brzo treba pritisnuti tipku miša drugi puta za dvostruki klik.</desc>
  </info>

  <title>Prilagodba brzine dvostrukog klika</title>

  <p>Dvostruki klik se događa samo kada dvaput dovoljno brzo pritisnete tipku miša. Ako je drugi pritisak predug nakon prvog, dobit ćete samo dva odvojena klika, a ne dvostruki klik. Ako imate poteškoća s brzim pritiskom tipke miša, trebali biste produljiti odgodu dvostrukog klika.</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Pristupačnost</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Pristupačnost</gui> za otvaranje panela.</p>
    </item>
    <item>
      <p>U odjeljku <gui>Usmjeravanje i klikanje</gui> prilagodite klizač <gui>Odgoda dvostrukog klika</gui> na pogodnu vrijednost.</p>
    </item>
  </steps>

  <p>Ako vaš miš dvostruko klikne kada želite jedan klik iako ste povećali odgodu dvostrukog klika, vaš miš je možda neispravan. Pokušajte priključiti drugi miš u svoje računalo i provjerite radi li ispravno. Alternativno, priključite svoj miš na drugo računalo i provjerite ima li i dalje isti problem.</p>

  <note>
    <p>Ova postavka će utjecati na vaš miš i touchpad, kao i na bilo koji drugi pokazivački uređaj.</p>
  </note>

</page>
