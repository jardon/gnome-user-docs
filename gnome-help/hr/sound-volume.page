<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="hr">

  <info>
    <link type="guide" xref="media#sound" group="#first"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-03-02"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Postavljanje glasnoće zvuka za računalo i upravljanje glasnoćom zvuka pojedine aplikacije.</desc>
  </info>

<title>Promjena glasnoće zvuka</title>

  <p>Kako biste promijenili glasnoću zvuka, otvorite <gui xref="shell-introduction#systemmenu">izbornik sustava</gui> s desne strane gornje trake i pomaknite klizač glasnoće zvuka lijevo ili desno. Zvuk možete potpuno isključiti povlačenjem klizača ulijevo.</p>
 
  <note style="tip">
    <p>Ako zadržite pokazivač iznad ikone glasnoće na gornjoj traci ili klizaču u izborniku sustava, glasnoćom se može upravljati pomicanjem kotačića miša ili touchpada.</p>
  </note>

  <p>Određene tipkovnice imaju tipke koje vam omogućuju upravljanje glasnoćom zvuka. Uobičajeno izgledaju kao stilizirani zvučnici s valovima koji izlaze iz njih. Često se nalaze blizu tipki “F” na vrhu. Na tipkovnicama prijenosnih računala obično se nalaze na tipkama “F”. Držite pritisnutu tipku <key>Fn</key> na tipkovnici kako biste ih koristili.</p>

  <p>Ako imate vanjske zvučnike, možete promijeniti glasnoću zvuka pomoću upravljača glasnoćom zvuka koji se nalazi na zvučniku. Pojedine slušalice isto imaju upravljača glasnoćom zvuka.</p>

<section id="apps">
 <title>Promjena glasnoće zvuka za pojedine aplikacije</title>

  <p>Možete promijeniti glasnoću zvuka za jednu aplikaciju, a za ostale ostaviti nepromijenjenu. Ovo je korisno, primjerice, ako slušate glazbu i pretražujete web. Možda poželite isključiti zvuk u web pregledniku kako zvukovi s web stranica ne bi ometali glazbu.</p>

  <p>Određene aplikacije imaju upravljanje glasnoćom zvuka u svome glavnom prozoru. Ako vaša aplikacija ima vlastito upravljanje glasnoćom zvuka, koristite ga za promjenu glasnoće zvuka. A ako ne:</p>

    <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite tipkati <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zvuk</gui> kako bi otvorili panel.</p>
    </item>
    <item>
      <p>U odjeljku <gui>Razina glasnoće zvuka</gui>, koristite klizač glasnoće zvuka za upravljanje glasnoćom zvuka pojedine aplikacije. Kliknite na tipku zvučnika na kraju klizača kako bi utišali ili uključili glasnoću zvuka aplikacije.</p>

  <note style="tip">
    <p>Navedene su samo aplikacije koje reproduciraju zvukove. Ako aplikacija reproducira zvukove ali nije na popisu, možda neće podržavati značajku koja vam omogućuje upravljanje glasnoćom zvuka na ovaj način. U tome slučaju ne možete mijenjati glasnoću zvuka takve aplikacije.</p>
  </note>
    </item>

  </steps>

</section>

</page>
