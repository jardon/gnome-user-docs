<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-open" xml:lang="hr">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Otvaranje datoteka s aplikacijom koja nije zadana aplikacija za tu vrstu datoteke. Može se promijeniti i zadana aplikacija.</desc>
  </info>

<title>Otvaranje datoteka s drugim aplikacijama</title>

  <p>Kada dvostruko kliknete (ili srednjim klikom) na datoteku u upravitelju datoteka, otvoriti će se sa zadanom aplikacijom za tu vrstu datoteke. Možete ju otvoriti i u drugoj aplikaciji, tražiti aplikacije na mreži ili postaviti zadanu aplikaciju za sve datoteke iste vrste.</p>

  <p>Kako bi otvorili datoteku s aplikacijom koja nije zadana, desno kliknite na datoteku i odaberite željenu aplikaciju s vrha izbornika. Ako ne vidite željenu aplikaciju, odaberite <gui>Otvori s drugom aplikacijom</gui>. Po zadanome, upravitelj datoteka samo prikazuje aplikacije koje mogu rukovati datotekom. Za pregled svih aplikacija na računalu, kliknite <gui>Prikaži sve aplikacije</gui>.</p>

<p>Ako još uvijek ne možete pronaći željenu aplikaciju, možete potražiti više aplikacija klikom na <gui>Pretraži nove aplikacije</gui>. Upravitelj datoteka pretražit će na mreži pakete aplikacija koje mogu rukovati datotekama te vrste.</p>

<section id="default">
  <title>Promjena zadane aplikacije</title>
  <p>Možete promijeniti zadanu aplikaciju koja se koristi za otvaranje datoteka određene vrste. To vam omogućuje otvaranje željene aplikacije kada dvostrukim klikom otvorite datoteku. Primjerice, možda želite otvoriti svoj omiljeni glazbeni reproduktor dvostrukim klikom na MP3 datoteku.</p>

  <steps>
    <item><p>Odaberite vrstu datoteke čiju zadanu aplikaciju želite promijeniti. Primjerice, ako želite promijenili aplikaciju za otvaranje MP3 datoteka, odaberite <file>.mp3</file> datoteku.</p></item>
    <item><p>Desno kliknite na datoteku i odaberite <gui>Svojstva</gui>.</p></item>
    <item><p>Odaberite <gui>Otvori s(a)</gui> karticu.</p></item>
    <item><p>Odaberite željenu aplikaciju i kliknite <gui>Postavi kao zadano</gui>.</p>
    <p>Ako <gui>Druge aplikacije</gui> sadrže aplikaciju koju povremeno koristite, ali ne želite ju postaviti zadanom, odaberite tu aplikaciju i kliknite <gui>Dodaj</gui>. Ovo će ju dodati u <gui>Preporučene aplikacije</gui>. Tada ćete moći koristiti ovu aplikaciju tako da desno kliknete na datoteku i odaberete ju s popisa.</p></item>
  </steps>

  <p>Ovo mijenja zadanu aplikaciju ne samo za odabranu datoteku, već za sve datoteke iste vrste.</p>

<!-- TODO: mention resetting the open with list with the "Reset" button -->

</section>

</page>
