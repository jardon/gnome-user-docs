<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="hr">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Provjerite lozinku i isprobajte druge stvari.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Upisali ste ispravnu lozinku, ali još se ne možete povezati</title>

<p>Ako ste sigurni da ste ispravno upisali <link xref="net-wireless-wepwpa">lozinku bežične mreže</link> ali se još uvijek ne možete uspješno povezati, pokušajte nešto od sljedećeg:</p>

<list>
 <item>
  <p>Još jednom provjerite jeste li upisali ispravnu lozinku</p>
  <p>Lozinke razlikuju velika ili mala slova (bitno je imaju li velika ili mala slova), stoga provjerite niste li pogrešno upisali velika i mala slova.</p>
 </item>

 <item>
  <p>Pokušajte heksadecimalni ili ASCII ključ za pristup</p>
  <p>Lozinka koju upišete može se predstaviti na drugačiji način — kao niz heksadecimalnih znakova (brojevi 0-9 i slova a-f) koji se naziva ključ za pristup. Svaka lozinka ima ekvivalentni ključ za pristup. Ako imate pristup ključu za pristup kao i lozinki, umjesto pokušajte upisati ključ za pristup. Provjerite jeste li odabrali ispravnu mogućnost <gui>bežične sigurnosti</gui> kada se upita za vašu lozinku (primjerice, odaberite <gui>WEP 40/128-bitni ključ</gui> ako upisujete 40-znakovni ključ za pristup za WEP-šifrirano povezivanje).</p>
 </item>

 <item>
  <p>Pokušajte isključiti pa zatim ponovno uključiti vašu bežičnu karticu</p>
  <p>Ponekad se bežične kartice zablokiraju ili imaju manje probleme što znači da se neće povezati. Pokušajte isključiti, a zatim ponovno uključiti karticu kako biste ju vratili na zadane postavke - pogledajte <link xref="net-wireless-troubleshooting"/> za više informacija.</p>
 </item>

 <item>
  <p>Provjerite koristite li ispravnu vrstu bežične sigurnosti</p>
  <p>Kada ste upitani za vašu bežičnu sigurnosnu lozinku, možete odabrati koju vrstu bežične sigurnosti želite koristiti. Provjerite jeste li odabrali onu koju koristi usmjernik ili bežična bazna stanica. Ovo bi trebalo biti odabrano po zadanome, ali ponekad iz nekog razloga se neće odabrati. Ako ne znate koji je to razlog, koristite metodu pokušaja i pogrešaka kako bi isprobali različite mogućnosti.</p>
 </item>

 <item>
  <p>Provjerite je li vaša bežična kartica pravilno podržana</p>
  <p>Pojedine bežične kartice nisu dobro podržane. Pojavljuju se kao bežično povezivanje, ali se ne mogu povezati s mrežom jer njihovi upravljački programi nemaju mogućnost za to. Provjerite možete li nabaviti alternativni upravljački program za bežičnu karticu ili možda treba dodatno podešavanje (poput instalacije drugog <em>firmvera</em>). Pogledajte <link xref="net-wireless-troubleshooting"/> za više informacija.</p>
 </item>

</list>

</page>
