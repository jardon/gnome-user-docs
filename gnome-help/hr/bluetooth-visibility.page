<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="bluetooth-visibility" xml:lang="hr">

  <info>
    <link type="guide" xref="bluetooth" group="#last"/>
    <link type="seealso" xref="bluetooth-device-specific-pairing"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mogućnost drugih uređaja da otkriju vaše računalo.</desc>
  </info>

  <title>Što je Bluetooth vidljivost?</title>

  <p>Bluetooth vidljivost odnosi se na to mogu li drugi uređaji otkriti vaše računalo kada traže Bluetooth uređaje. Kada je Bluetooth uključen i <gui>Bluetooth</gui> panel je otvoren, vaše računalo će se oglasiti svim drugim uređajima u dometu, dopuštajući im pokušaj povezivanja s vašim računalom.</p>

  <note style="tip">
    <p>You can <link xref="about-hostname">change</link> the name your
    computer displays to other devices.</p>
  </note>

  <p>Nakon što ste se <link xref="bluetooth-connect-device">povezali s uređajem</link>, ni vaše računalo ni uređaj ne moraju biti vidljivi za međusobnu komunikaciju.</p>

  <p>Uređaji bez zaslona uobičajeno imaju način uparivanja u koji se može ući pritiskom na tipku, ili kombinacijom tipki na kratko vrijeme, bilo kada su već uključeni, bilo dok se uključuju.</p>

  <p>Najbolji način da saznate kako ući u način uparivanja je da pogledate priručnik uređaja. Za pojedine uređaje, postupak se može <link xref="bluetooth-device-specific-pairing">neznatno razlikovati od uobičajenog</link>.</p>

</page>
