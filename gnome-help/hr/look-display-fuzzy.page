<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="hr">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.28" date="2018-07-28" status="review"/>
    	<revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Razlučivost zaslona možda nije ispravno postavljena.</desc>
  </info>

  <title>Zašto prikaz zaslona izgleda nejasno/pikselizirano?</title>

  <p>Razlučivost prikaza koja je postavljena možda ne odgovara razlučivosti zaslona. Kako bi ovo riješili:</p>

  <steps>
    <item>
      <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Zasloni</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Zasloni</gui> u bočnoj traci za otvaranje panela.</p>
    </item>
    <item>
      <p>Isprobajte neke od mogućnosti <gui>Razlučivost</gui> i odaberite onu koja najbolje izgleda na zaslonu.</p>
    </item>
  </steps>

<section id="multihead">
  <title>Više priključenih zaslona</title>

  <p>Ako imate dva zaslona priključena s računalom (primjerice, normalni monitor i projektor), zasloni mogu imati različitu optimalnu ili <link xref="look-resolution#native">izvornu</link> razlučivosti.</p>

  <p>Korištenjem <link xref="display-dual-monitors#modes">Zrcali</link> načina prikaza isti prikaz je na oba zaslona. Oba zaslona koriste istu razlučivost, koja možda neće odgovarati izvornoj razlučivosti bilo kojeg od oba zaslona, stoga oštrina slike bi mogla biti slabija na oba zaslona.</p>

  <p>Korištenjem načina prikaza <link xref="display-dual-monitors#modes">Spoji zaslone</link>, razlučivost svakog zaslona može se postaviti zasebno, tako da se oba zaslona mogu postaviti na svoju izvornu razlučivost.</p>

</section>

</page>
