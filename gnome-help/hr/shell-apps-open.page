<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="hr">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pokretanje aplikacija iz <gui>Aktivnosti</gui> pregleda.</desc>
  </info>

  <title>Pokretanje aplikacija</title>

  <p if:test="!platform:gnome-classic">Pomaknite svoj pokazivač miša u <gui>Aktivnosti</gui> kut gore lijevo u traci na zaslonu kako bi se prikazao <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled. Ovdje možete potražiti sve svoje aplikacije. Pregled možete otvoriti i pritiskom na <key xref="keyboard-key-super">Super</key> tipku.</p>
  
  <p if:test="platform:gnome-classic">Aplikacije možete pokrenuti iz izbornika <gui xref="shell-introduction#activities">Aplikacija</gui> gore lijevo na zaslonu, ili možete koristiti <gui>Aktivnosti</gui> pregled pritiskom na <key xref="keyboard-key-super">Super</key> tipku.</p>

  <p>Postoji nekoliko načina otvaranja aplikacija iz <gui>Aktivnosti</gui> pregleda:</p>

  <list>
    <item>
      <p>Započnite upisivati naziv aplikacije — pretraživanje započinje trenutačno. (Ako se to ne dogodi, kliknite u traku pretrage na vrhu zaslona i započnite upisivanje.) Ako ne znate točan naziv aplikacije, pokušajte upisati što sličniji naziv. Kliknite na ikonu aplikacije kako bi ju pokrenuli.</p>
    </item>
    <item>
      <p>Određene aplikacije imaju ikone u <em>pokretaču</em>, u okomitoj traci na dnu <gui>Aktivnosti</gui> pregleda. Kliknite na željenu ikonu kako bi pokrenuli aplikaciju.</p>
      <p>Ako imate aplikacije koje često koristite, možete ih <link xref="shell-apps-favorites">dodati u pokretač</link>.</p>
    </item>
    <item>
      <p>Kliknite na tipku mreže (koja sadrži devet točaka) u pokretaču. Vidjet ćete prvu stranicu svih instaliranih aplikacija. Kako bi vidjeli više aplikacija, pritisnite točke na dnu iznad pokretača, zatim kliknite na aplikaciju kako bi ju pokrenuli.</p>
    </item>
    <item>
      <p>Možete pokrenuti aplikaciju u zasebnom <link xref="shell-workspaces">radnom prostoru</link> dovlačenjem ikone aplikacije iz pokretača i ispuštanjem u jedan od radnih prostora. Aplikacija će se otvoriti u odabranom radnom prostoru.</p>
      <p>Možete pokrenuti aplikaciju u <em>novom</em> radnom prostoru povlačenjem ikone aplikacije na prazni radni prostor, ili na mali prostor između dva radna prostora.</p>
    </item>
  </list>

  <note style="tip">
    <title>Brzo pokretanje naredbi</title>
    <p>Još jedan način pokretanja aplikacija je da pritisnete <keyseq><key>Alt</key><key>F2</key></keyseq> tipke, upišete <em>naziv u naredbeni redak</em>, i pritisnete <key>Enter</key> tipku.</p>
    <p>Primjerice, kako bi pokrenuli <app>Rhythmbox</app>, pritisnite <keyseq><key>Alt</key><key>F2</key></keyseq> i upišite ‘<cmd>rhythmbox</cmd>’ (bez navodnika). Naziv aplikacije je naredba za pokretanje programa.</p>
    <p>Koristite tipke strelica za brži pristup prijašnjim naredbama.</p>
  </note>

</page>
