<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="hr">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dekorativni i funkcionalni zaključani zaslon prikazuje korisne informacije.</desc>
  </info>

  <title>Zaključavanje zaslona</title>

  <p>Na zaključanom zaslonu možete vidjeti što se događa kada je vaše računalo zaključano, i omogućuje vam dobivanje sažetka o tome što se događa dok ste bili odsutni. Zaključani zaslon vam pruža korisne informacije:</p>

  <list>
<!--<item><p>the name of the logged-in user</p></item> -->
    <item><p>datum i vrijeme te određene obavijesti</p></item>
    <item><p>stanje baterije i mreže</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Kako bi otključali računalo, kliknite jednom mišem ili touchpadom, ili pritisnite <key>Esc</key> ili <key>Enter</key> tipke. Ovo će otkriti zaslon za prijavu, gdje možete upisati svoju lozinku za otključavanje. Alternativno, samo započnite upisivati svoju lozinku i zaslon za prijavu će se automatski prikazati dok upisujete. Možete promijeniti korisnike u donjem desnom kutu zaslona za prijavu ako je vaš sustav podešen za više od jednog korisnika.</p>

  <p>Kako bi sakrili obavijesti na svome zaslonu zaključavanja, pogledajte <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
