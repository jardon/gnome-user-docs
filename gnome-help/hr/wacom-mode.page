<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="hr">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Prebacivanje tableta između načina rada tableta i načina rada miša.</desc>
  </info>

  <title>Postavljanje načina praćenja Wacom tableta</title>

<p><gui>Tablet način rada</gui> određuje kako se olovka mapira na zaslon.</p>

<steps>
  <item>
    <p>Otvorite <gui xref="shell-introduction#activities">Aktivnosti</gui> pregled i započnite upisivati <gui>Wacom tablet</gui>.</p>
  </item>
  <item>
    <p>Kliknite na <gui>Wacom tablet</gui> u bočnoj traci za otvaranje panela.</p>
    <note style="tip"><p>Ako se tablet ne otkrije, od vas će se zatražiti da <gui>Priključite ili uključite Wacom tablet</gui>. Kliknite na <gui>Bluetooth</gui> u bočnoj traci za povezivanje bežičnog tableta.</p></note>
  </item>
  <item>
    <p>Odaberite između načina rada tableta (apsolutnog) i touchpad načina rada (relativnog). Za način rada tableta, uključite preklopnik <gui>Tablet način rada</gui>.</p>
  </item>
</steps>

  <note style="info"><p>U <em>apsolutnom</em> načinu rada, svaka točka na tabletu preslikava se u točku na zaslonu. Gornji lijevi kut zaslona, primjerice, uvijek odgovara istoj točki na tabletu.</p>
  <p>U <em>relativnom</em> načinu rada, ako podignete olovku s tableta i spustite je u drugi položaj, pokazivač na zaslonu se ne pomiče. Ovo je način na koji radi miš.</p>
  </note>

</page>
