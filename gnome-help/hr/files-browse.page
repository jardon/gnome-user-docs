<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="hr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Upravljanje i organizacija datoteka pomoću upravitelja datoteka.</desc>
  </info>

<title>Pregledavanje datoteka i mapa</title>

<p>Koristite <app>Datoteke</app> upravitelja datoteka kako bi pregledavali i organizirali svoje datoteke na računalu. Možete ga koristiti na uređajima pohrane (poput vanjskih čvrstih diskova), na<link xref="nautilus-connect">poslužiteljima datoteka</link> i na mrežnim dijeljenjima.</p>

<p>Kako bi pokrenuli upravitelja datoteka, otvorite <app>Datoteke</app> u <gui xref="shell-introduction#activities">Aktivnosti</gui> pregledu. Možete pretražiti datoteke putem pregleda na isti način kao da <link xref="shell-apps-open">pretražujte aplikaciju</link>.</p>

<section id="files-view-folder-contents">
  <title>Istraživanje sadržaja mapa</title>

<p>U upravitelju datoteka, dvostruko kliknite na bilo koju mapu kako bi vidjeli njezin sadržaj, dvostruko kliknite ili kliknite <link xref="mouse-middleclick">srednji klik</link> na bilo koju datoteku kako bi ju otvorili sa zadanom aplikacijom za tu datoteku. Srednji klik na mapu otvara ju u novoj kartici. Isto tako možete desnim klikom miša kliknuti mapu kako bi ju otvorili u novoj kartici ili novom prozoru.</p>

<p>Kada pregledavate datoteke u mapi, možete brzo <link xref="files-preview">pregledati svaku datoteku</link> pritiskom na Space tipku kako bi bili sigurni da imate pravu datoteku prije otvaranja, kopiranja ili brisanja.</p>

<p><em>Traka putanje</em> iznad popisa datoteka i mapa prikazuje koju mapu pregledavate, uključujući sadržajne mape trenutne mape. Klikom na sadržajnu mapu u traci putanje idete u tu mapu. Desnom tipkom miša kliknite na bilo koju mapu u traci putanje kako bi je otvorili u novoj kartici ili prozoru ili pristupili njenim svojstvima.</p>

<p>Ako želite brzo <link xref="files-search">pretražiti datoteku</link>, u ili ispod mape koju pregledavate, počnite upisivati njezin naziv. <em>Traka pretrage</em> pojavit će se na vrhu prozora i prikazati će se samo datoteke koje odgovaraju vašoj pretrazi. Pritisnite <key>Esc</key> tipku za prekidanje pretrage.</p>

<p>Možete brzo pristupiti uobičajenim lokacijama s <em>bočne trake</em>. Ako ne vidite bočnu traku, pritisnite tipku izbornika u gornjem desnom kutu prozora, zatim odaberite <gui>Prikaži bočnu traku</gui>. Možete <link xref="nautilus-bookmarks-edit">označiti mape koje često koristite</link> i one će se pojaviti na bočnoj traci u stavci Označeno.</p>

</section>

</page>
