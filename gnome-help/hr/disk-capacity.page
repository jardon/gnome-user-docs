<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="hr">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Projekt GNOME dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Koristite <gui>Analizator iskoristivosti diska</gui>, <gui>Nadgledatelj sustava</gui> ili <gui>Resursi</gui> aplikacije za provjeru prostora i kapaciteta.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Provjera preostalog prostora na disku</title>

  <p>Možete provjeriti koliko je prostora na disku preostalo pomoću <app>Analizatora iskoristivosti diska</app>, <app>Nadgledatelja sustava</app> ili <app>Resursi</app> aplikacija.</p>

<section id="disk-usage-analyzer">
<title>Provjera s Analizatorom iskoristivosti diska</title>

  <p>Za provjeru slobodnog prostora i kapaciteta diska pomoću <app>Analizatora iskoristivosti diska</app>:</p>

  <list>
    <item>
      <p>Otvorite <app>Analizator iskoristivosti diska</app> iz <gui>Aktivnosti</gui> pregleda. Prozor će prikazati popis lokacija datoteka zajedno s upotrebom i kapacitetom pojedine lokacije.</p>
    </item>
    <item>
      <p>Kliknite na jednu od stavki s popisa za pregled opširnijeg sažetka korištenja te stavke. Pritisnite tipku izbornika, a zatim <gui>Pretraži mapu…</gui> za pretragu druge lokacije.</p>
    </item>
  </list>
  <p>Informacije se prikazuju prema <gui>Mapi</gui>, <gui>Veličini</gui>, <gui>Sadržaju</gui> i kada su podaci posljednji put <gui>Promijenjeni</gui>. Pogledajte više pojedinosti u <link href="help:baobab"><app>Analizatoru iskoristivosti diska</app></link>.</p>

</section>

<section id="system-monitor">

<title>Provjera s Nadgledateljem sustava</title>

  <p>Za provjeru slobodnog prostora i kapaciteta diska pomoću <app>Nadgledatelja sustava</app>:</p>

<steps>
 <item>
  <p>Otvorite <app>Nadgledatelj sustava</app> aplikaciju iz <gui>Aktivnosti</gui> pregleda.</p>
 </item>
 <item>
  <p>Odaberite karticu <gui>Datotečni sustavi</gui> za pregled particija sustava i korištenja prostora na disku. Informacije se prikazuju prema <gui>Ukupno</gui>, <gui>Slobodno</gui>, <gui>Dostupno</gui> i <gui>Iskorišteno</gui> stavkama.</p>
 </item>
</steps>
</section>

<section id="usage">
<title>Provjera s Resursima</title>

  <p>Za provjeru slobodnog prostora i kapaciteta diska pomoću <app>Resursa</app>:</p>

<steps>
  <item>
    <p>Otvorite <app>Resursi</app> aplikaciju iz <gui>Aktivnosti</gui> pregleda.</p>
  </item>
  <item>
    <p>Odaberite karticu <gui>Pohrana</gui> za pregled ukupnog <gui>Iskorištenog</gui> i <gui>Dostupnog</gui> diskovnog prostora sustava, kao i prostora koji koristi <gui>Operativni sustav</gui> i uobičajeni direktoriji korisnika.</p>
  </item>
</steps>

<note style="tip">
  <p>Diskovni prostor se može osloboditi iz direktorija korisnika i njihovih poddirektorija odabirom okvira pokraj naziva direktorija.</p>
</note>
</section>

<section id="disk-full">

<title>Što ako je disk prepun?</title>

  <p>Ako je disk prepun trebali bi:</p>

 <list>
  <item>
   <p>Obrisati datoteke koje nisu bitne ili koje više nećete koristiti.</p>
  </item>
  <item>
   <p>Napravite <link xref="backup-why">sigurnosne kopije</link> bitnih datoteka koje vam neko vrijeme neće trebati i obrišite ih s čvrstog diska.</p>
  </item>
 </list>
</section>

</page>
