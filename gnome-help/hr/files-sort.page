<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="hr">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Razvrstavanje datoteka prema nazivu, veličini, vrsti ili vremenu promjene.</desc>
  </info>

<title>Razvrstavanje datoteka i mapa</title>

<p>Datoteke možete razvrstati na razne načine u mapi, primjerice razvrstavanjem prema datumu ili veličini datoteke. Pogledajte <link xref="#ways"/> u nastavku popis uobičajenih načina razvrstavanja datoteka. Pogledajte <link xref="nautilus-views"/> za informacije promjene zadanog redoslijeda razvrstavanja.</p>

<p>Način razvrstavanja datoteke ovisi o <em>prikazu mape</em> koji koristite. Trenutni prikaz možete promijeniti pomoću tipke s popisom ili ikonama na alatnoj traci.</p>

<section id="icon-view">
  <title>Prikaz mreže</title>

  <p>Kako bi razvrstali datoteke drugim redoslijedom, kliknite tipku prikaza na alatnoj traci i odaberite <gui>Po nazivu</gui>, <gui>Po veličini</gui>, <gui>Po vrsti</gui>, <gui>Po datumu promjene</gui> ili <gui>Po datumu pristupa</gui>.</p>

  <p>Primjerice, ako odaberete <gui>Po nazivu</gui>, datoteke će biti razvrstane po nazivima, abecednim redom. Pogledajte <link xref="#ways"/> za ostale mogućnosti.</p>

  <p>Možete razvrstati obrnutim redoslijedom, odabirom <gui>Obrnuti redoslijed</gui> s izbornika.</p>

</section>

<section id="list-view">
  <title>Prikaz popisa</title>

  <p>Kako biste razvrstali datoteke drugim redoslijedom, kliknite jedan od naslova stupaca u upravitelju datoteka. Primjerice, kliknite <gui>Vrsta</gui> kako bi razvrstali prema vrsti datoteke. Ponovno kliknite naslov stupca kako bi razvrstali obrnutim redoslijedom.</p>
  <p>U prikazu popisa možete prikazati stupce s više svojstava i razvrstati po tim stupcima. Kliknite tipku prikaza na alatnoj traci, zatim kliknite na <gui>Vidljivi stupci…</gui> i odaberite dodatne stupce za koje želite da budu vidljivi. Tada ćete moći razvrstati po tim stupcima. Pogledajte <link xref="nautilus-list"/> za opise dostupnih stupaca.</p>

</section>

<section id="ways">
  <title>Načini razvrstavanja datoteka</title>

  <terms>
    <item>
      <title>Naziv</title>
      <p>Razvrstavanje abecedno po nazivu datoteke.</p>
    </item>
    <item>
      <title>Veličina</title>
      <p>Razvrstavanje po veličini datoteke (koliko datoteka zauzima diskovnog prostora). Datoteke se razvrstavaju od najmanje prema najvećoj po zadanome.</p>
    </item>
    <item>
      <title>Vrsta</title>
      <p>Razvrstavanje abecedno po vrsti datoteke. Datoteke iste vrste su grupirane zajedno, zatim su razvrstane po nazivu.</p>
    </item>
    <item>
      <title>Posljednja promjena</title>
      <p>Razvrstavanje po datumu i vremenu posljednje promjene datoteke. Datoteke su razvrstane od najstarije prema najnovijoj po zadanome.</p>
    </item>
  </terms>

</section>

</page>
