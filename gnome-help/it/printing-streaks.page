<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="printing-streaks" xml:lang="it">

  <info>
    <link type="guide" xref="printing#problems"/>
    <link type="seealso" xref="printing-inklevel"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Se i fogli stampati sono striati, sbiaditi o mancano alcuni colori, controllare i livelli dell'inchiostro o pulire la testina di stampa.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Perché ci sono striature, linee o colori sbagliati sui fogli stampati?</title>

  <p>If your print-outs are streaky, faded, have lines on them that should not be
  there, or are otherwise poor in quality, this may be due to a problem with the
  printer or a low ink or toner supply.</p>

  <terms>
     <item>
       <title>Testo o immagini sbiaditi</title>
       <p>You may be running out of ink or toner. Check your ink or toner supply
       and buy a new cartridge if necessary.</p>
     </item>
     <item>
       <title>Striature e linee</title>
       <p>If you have an inkjet printer, the print head may be dirty or
       partially blocked. Try cleaning the print head. See the printer’s manual
       for instructions.</p>
     </item>
     <item>
       <title>Colori errati</title>
       <p>The printer may have run out of one color of ink or toner. Check your
       ink or toner supply and buy a new cartridge if necessary.</p>
     </item>
     <item>
       <title>Jagged lines, or lines are not straight</title>
       <p>If lines on your print-out that should be straight turn out jagged,
       you may need to align the print head. See the printer’s instruction
       manual for details on how to do this.</p>
     </item>
  </terms>

</page>
