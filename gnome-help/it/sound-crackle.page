<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="it">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Controllare i cavi audio e i driver della propria scheda audio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Durante la riproduzione audio si sentono ronzii</title>

  <p>Se durante la riproduzione audio si sentono ronzii, ci potrebbe essere un problema con i cavi o i connettori audio o un problema con i driver della scheda audio.</p>

<list>
 <item>
  <p>Verificare che gli altoparlanti siano collegati correttamente.</p>
  <p>If the speakers are not fully plugged in, or if they are plugged into the
  wrong socket, you might hear a buzzing sound.</p>
 </item>

 <item>
  <p>Make sure the speaker/headphone cable is not damaged.</p>
  <p>Cavi e connettori audio possono sciuparsi con il loro utilizzo. Provare a collegare il cavo o le cuffie in un altro dispositivo audio (come un lettore MP3 o un lettore CD portatile) per verificare la presenza di rumori di fondo o ronzii. Nel caso si sentissero ancora tali rumori, potrebbe essere necessario sostituire il cavo o le cuffie.</p>
 </item>

 <item>
  <p>Check if the sound drivers are not very good.</p>
  <p>Some sound cards do not work very well on Linux because they do not have very
  good drivers. This problem is more difficult to identify. Try searching for
  the make and model of your sound card on the internet, plus the search term
  “Linux”, to see if other people are having the same problem.</p>
  <p>You can use the <cmd>lspci</cmd> command to get more information about your
  sound card.</p>
 </item>
</list>

</page>
