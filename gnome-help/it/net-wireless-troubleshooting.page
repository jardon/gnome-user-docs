<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting" xml:lang="it">

  <info>
    <link type="guide" xref="net-wireless" group="first"/>
    <link type="guide" xref="hardware#problems" group="first"/>
    <link type="next" xref="net-wireless-troubleshooting-initial-check"/>

    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Collaboratori del wiki della documentazione di Ubuntu</name>
    </credit>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Identify and fix problems with wireless connections.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Risoluzione dei problemi relativi alle reti senza fili</title>

  <p>This is a step-by-step troubleshooting guide to help you identify and fix
  wireless problems. If you cannot connect to a wireless network for some
  reason, try following the instructions here.</p>

  <p>Seguire i prossimi passaggi per connettere il computer a Internet:</p>

  <list style="numbered compact">
    <item>
      <p>Eseguire un controllo iniziale</p>
    </item>
    <item>
      <p>Raccogliere informazioni sulle componenti hardware</p>
    </item>
    <item>
      <p>Controllare le componenti hardware</p>
    </item>
    <item>
      <p>Tentare di creare una connessione al router senza fili</p>
    </item>
    <item>
      <p>Eseguire un controllo del modem e del router</p>
    </item>
  </list>

  <p>Fare clic sul collegamento <em>Avanti</em> in alto a destra nella pagina: questo collegamento e altri simili nelle pagine seguenti saranno di aiuto in ogni passaggio della guida.</p>

  <note>
    <title>Usare la riga di comando</title>
    <p>Some of the instructions in this guide ask you to type commands into the
    <em>command line</em> (Terminal). You can find the <app>Terminal</app> application in
    the <gui>Activities</gui> overview.</p>
    <p>If you are not familiar with using a command line, don’t worry — this
    guide will direct you at each step. All you need to remember is that
    commands are case-sensitive (so you must type them <em>exactly</em> as they
    appear here), and to press <key>Enter</key> after typing each command to
    run it.</p>
  </note>

</page>
