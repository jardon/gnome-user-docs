<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-states" xml:lang="it">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.4.0" date="2012-03-24" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Disporre le finestre su uno spazio di lavoro aiuta a lavorare in modo più efficiente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Spostare e ridimensionare le finestre</title>

  <p>You can move and resize windows to help you work more efficiently. In
  addition to the dragging behavior you might expect, the system features
  shortcuts and modifiers to help you arrange windows quickly.</p>

  <list>
    <item>
      <p>Move a window by dragging the titlebar, or hold down
      <key xref="keyboard-key-super">Super</key> and drag anywhere in the
      window. Hold down <key>Shift</key> while moving to snap the window to the
      edges of the screen and other windows.</p>
    </item>
    <item>
      <p>Ridimensionare una finestra trascinando i suoi bordi o l'angolo. Tenere premuto <key>Maiusc</key> durante il ridimensionamento per agganciare la finestra ai bordi dello schermo o di altre finestre.</p>
      <p if:test="platform:gnome-classic">You can also resize a maximized
      window by clicking the maximize button in the titlebar.</p>
    </item>
    <item>
      <p>Muovere o ridimensionare una finestra usando solo la tastiera. Premere <keyseq><key>Alt</key><key>F7</key></keyseq> per spostare una finestra oppure <keyseq><key>Alt</key><key>F8</key></keyseq> per ridimensionarla. Usare i tasti freccia per muoverla o ridimensionarla, quindi premere <key>Invio</key> per completare l'azione, oppure premere <key>Esc</key> per tornare alla posizione e dimensione originale.</p>
    </item>
    <item>
      <p><link xref="shell-windows-maximize">Massimizzare una finestra</link> trascinandola sul bordo superiore dello schermo. Trascinare una finestra su un lato dello schermo per massimizzarla lungo quel lato, consentendo così <link xref="shell-windows-tiled">di affiancare due finestre</link>.</p>
    </item>
  </list>

</page>
