<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="it">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aggiungere nuovi utenti al sistema.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Aggiungere un nuovo account utente</title>

  <p>È possibile aggiungere molteplici account al proprio computer: uno per ogni familiare o, all'interno di un'azienda, uno per ogni dipendente. Ogni utente avrà a disposizione una propria cartella personale per documenti, foto e per le proprie impostazioni.</p>

  <p>You need <link xref="user-admin-explain">administrator privileges</link>
  to add user accounts.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Users</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Users</gui> to open the panel.</p>
    </item>
    <item>
      <p>Press <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Press the <gui style="button">+ Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>If you want the new user to have
      <link xref="user-admin-explain">administrative access</link> to the
      computer, select <gui>Administrator</gui> for the account type.</p>
      <p>Administrators can do things like add and delete users, install software
      and drivers, and change the date and time.</p>
    </item>
    <item>
      <p>Enter the new user’s full name. The username will be filled in
      automatically based on the full name. If you do not like the proposed
      username, you can change it.</p>
    </item>
    <item>
      <p>You can choose to set a password for the new user, or let them set it
      themselves on their first login. If you choose to set the password now,
      you can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
      <p>To connect the user to a network domain, click
      <gui>Enterprise Login</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Add</gui>. When the user has been added,
      <gui>Parental Controls</gui> and <gui>Language</gui> settings can be
      adjusted.</p>
    </item>
  </steps>

  <p>If you want to change the password after creating the account, select the
  account, <gui style="button">Unlock</gui> the panel and press the current
  password status.</p>

  <note>
    <p>In the <gui>Users</gui> panel, you can click the image next to the
    user’s name to the right to set an image for the account. This image will
    be shown in the login window. The system provides some stock photos you can
    use, or you can select your own or take a picture with your webcam.</p>
  </note>

</page>
