<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>从文件管理器轻松地传文件给您的电子邮件联系人。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>通过电子邮件分享文件</title>

<p>您可以直接从文件管理器中通过电子邮件轻松地与联系人分享文件。</p>

  <note style="important">
    <p>在开始之前，请确保您的计算机上安装了 <app>Evolution</app> 或 <app>Geary</app>，并配置了电子邮件帐号。</p>
  </note>

<steps>
  <title>要通过电子邮件分享文件：</title>
    <item>
      <p>从<gui xref="shell-introduction#activities">活动</gui>概览中打开<app>文件</app>应用程序。</p>
    </item>
  <item><p>找到要传送的文件。</p></item>
    <item>
      <p>右键点击该文件并选择<gui>发送至…</gui>。将会出现一个带有文件附件的电子邮件撰写窗口。</p>
    </item>
  <item><p>点击<gui>收件人</gui>选择一个联系人，或输入您要发送文件到的电子邮件地址。根据需要填写<gui>主题</gui>和邮件正文，然后点击<gui>发送</gui>。</p></item>
</steps>

<note style="tip">
  <p>您可以一次发送多个文件。在点击文件时按住 <key>Ctrl</key> 选择多个文件，然后右键点击任何选定的文件即可。</p>
</note>

</page>
