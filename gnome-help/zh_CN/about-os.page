<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-os" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="about" group="os"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>获取有关当前安装的操作系统的信息。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>查找有关您的操作系统的信息</title>

  <p>了解当前安装的操作系统有助于您知晓新软件或硬件是否与您的系统兼容。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>关于</gui>。</p>
    </item>
    <item>
      <p>点击 <gui>关于</gui> 打开面板。</p>
    </item>
    <item>
      <p>查看 <gui>操作系统名称</gui>、<gui>操作系统类型</gui>、<gui>GNOME 版本</gui> 等条目下列出的信息。</p>
    </item>
  </steps>

  <note style="tip">
    <p>可以通过点击各个项目将信息复制到剪贴板。这样可以轻松地与他人共享您的系统信息。</p>
  </note>

</page>
