<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.37.1" date="2020-07-30" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>文件夹列表，您可以在其中找到您可能想要备份的文档、文件和设置。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>我可以在哪里找到想要备份的文件？</title>

  <p>确定要备份哪些文件并找到它们所在的位置是尝试执行备份最困难的一步。以下列出的是您可能要想备份的重要文件的最常见的位置。</p>

<list>
 <item>
  <p>个人文件（文档、音乐、照片和视频）</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs   and need to be translated.  You can find the correct translations for your   language here: http://translationproject.org/domain/xdg-user-dirs.html">它们通常存放在您的主文件夹（<file>/home/您的用户名</file>）。它们可能位于子文件夹中，如<file>桌面</file>、<file>文档</file>、<file>图片</file>、<file>音乐</file>和<file>视频</file>。</p>
  <p>如果备份介质空间足够大（比如是外接硬盘），可以考虑备份整个主文件夹，您可以先用<app>磁盘</app>查看一下它占多大空间。</p>
 </item>

 <item>
  <p>隐藏文件</p>
  <p>以点开头的文件或文件夹默认是隐藏的，要查看隐藏文件，点<app>文件</app>窗口右上角的菜单<gui>显示隐藏文件</gui>选项，或者按组合键 <keyseq><key>Ctrl</key><key>H</key></keyseq>。您可以像复制其他文件一样复制到备份位置。</p>
 </item>

 <item>
  <p>个人设置（桌面首选项、主题和软件设置）</p>
  <p>绝大部分软件的设置信息都以隐藏文件的形式将存储在您的主文件夹中（请参阅上面的隐藏文件信息）。</p>
  <p>绝大部分软件的设置信息都存放在您主文件夹的：<file>.config</file> 和 <file>.local</file> 文件夹中。</p>
 </item>

 <item>
  <p>系统设置</p>
  <p>系统关键部分的设置信息并不存放在主目录中。而可能存放在其他一些地方。大部分情况下，这些信息会被存储在 <file>/etc</file> 文件夹中。一般来说作为桌面用户您并不需要保存这些信息。但如果您使用的是服务器，那么您可能会需要为正在运行服务备份文件。</p>
 </item>
</list>

</page>
