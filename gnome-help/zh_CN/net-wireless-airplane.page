<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-airplane" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>打开网络设置并将飞行模式切换为开启状态。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>关闭无线网络（飞行模式）</title>

<p>如果在飞机上（或其他不允许无线连接的地方）使用计算机，您应该关闭无线网络。您还可能出于其他原因（例如，为了节省电池电量）关闭无线网络。</p>

  <note>
    <p>使用<em>飞行模式</em>将完全关闭包括 Wi-Fi、3G 和蓝牙在内的所有无线连接。</p>
  </note>

  <p>要打开飞行模式：</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入 <gui>Wi-Fi</gui>。</p>
    </item>
    <item>
      <p>点击 <gui>Wi-Fi</gui> 打开面板。</p>
    </item>
    <item>
      <p>将<gui>飞行模式</gui>开关切换为开启状态。这将关闭您的无线连接直到您禁用飞机模式为止。</p>
    </item>
  </steps>

  <note style="tip">
    <p>您可以从<gui xref="shell-introduction#systemmenu">系统菜单</gui>中关闭 Wi-Fi 连接，方法是点击连接名称并选择<gui>关闭</gui>。</p>
  </note>

</page>
