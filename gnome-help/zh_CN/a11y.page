<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="guide" style="a11y task" id="a11y" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="index" group="a11y"/>

    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>使用辅助技术来满足视觉，听觉和行动能力的特殊需求。</desc>
    <uix:thumb role="experimental-gnome-tiles" src="figures/tile-a11y.svg"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>辅助功能</title>

  <p>该系统包括辅助技术，为具有各类障碍和特殊需求的用户提供支持，并与常见的辅助设备进行交互。可将辅助功能菜单添加到顶栏中，从而更轻松地访问多种辅助功能。</p>

  <section id="vision">
    <title>视觉障碍</title>

    <links type="topic" groups="blind" style="linklist">
      <title>失明</title>
    </links>
    <links type="topic" groups="lowvision" style="linklist">
      <title>弱视</title>
    </links>
    <links type="topic" groups="colorblind" style="linklist">
      <title>色盲</title>
    </links>
    <links type="topic" style="linklist">
      <title>其他主题</title>
    </links>
  </section>

  <section id="sound">
    <title>听觉障碍</title>
    <links type="topic" style="linklist"/>
  </section>

  <section id="mobility">
    <title>行动障碍</title>

    <links type="topic" groups="pointing" style="linklist">
      <title>鼠标操作</title>
    </links>
    <links type="topic" groups="clicking" style="linklist">
      <title>点击和拖动</title>
    </links>
    <links type="topic" groups="keyboard" style="linklist">
      <title>键盘使用</title>
    </links>
    <links type="topic" style="linklist">
      <title>其他主题</title>
    </links>
  </section>
</page>
