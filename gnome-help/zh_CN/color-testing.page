<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-testing" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-gettingprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>
    
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>使用提供的测试配置文件来检查您的配置文件是否已正确应用于屏幕。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>如果颜色管理器正确，我应该如何测试？</title>

  <p>颜色配置文件的影响有时是微小的，要看到变化可能有些困难。</p>

  <p>本系统自带了几个测试用的配置文件，在应用这些配置文件时变化清楚可见：</p>

  <terms>
    <item>
      <title>蓝色</title>
      <p>这将使屏幕变蓝，并测试校准曲线是否已发送给显示器。</p>
    </item>
<!--    <item>
      <title>ADOBEGAMMA-test</title>
      <p>This will turn the screen pink and tests different features of a
      screen profile.</p>
    </item>
    <item>
      <title>FakeBRG</title>
      <p>This will not change the screen, but will swap around the RGB channels
      to become BGR. This will make all the colors gradients look mostly
      correct, and there won’t be much difference on the whole screen, but
      images will look very different in applications that support color
      management.</p>
    </item>-->
  </terms>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击侧边栏中的<gui>色彩</gui>打开面板。</p>
    </item>
    <item>
      <p>选择您要添加配置文件的设备。您或许想要记住当前正在使用的配置文件。</p>
    </item>
    <item>
      <p>点击<gui>添加配置</gui>选择一个测试配置文件，它应该在列表的底部。</p>
    </item>
    <item>
      <p>按<gui>添加</gui>确认您的选择。</p>
    </item>
    <item>
      <p>要恢复到之前的配置文件，请在<gui>色彩</gui>面板中选择设备，然后选择您在尝试当前测试配置文件之前使用的配置文件，并按<gui>启用</gui>再次使用它。</p>
    </item>
  </steps>


  <p>使用这些配置文件，您可以清楚地看到一个应用程序是否支持色彩管理。</p>

</page>
