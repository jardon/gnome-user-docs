<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="guide" style="task" id="files-search" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.34" date="2019-07-20" status="draft"/>
    <revision pkgversion="3.36" date="2020-04-18" status="draft"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <title type="link" role="trail">搜索</title>
    <desc>基于文件名和类型定位文件。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>搜索文件</title>

  <p>您可以根据文件名和类型在文件管理器中直接查找文件。</p>

  <links type="topic" style="linklist">
    <title>其他搜索程序</title>
    <!-- This is an extension point where search apps can add
    their own topics. It's empty by default. -->
  </links>

  <steps>
    <title>搜索</title>
    <item>
      <p>从<gui xref="shell-introduction#activities">活动</gui>概览中打开<app>文件</app>应用程序。</p>
    </item>
    <item>
      <p>如果您知道要找的文件存放位置，可以直接转到那个文件夹。</p>
    </item>
    <item>
      <p>只要输入您知道的文件名中的字词，它们就会显示在搜索栏中。例如，如果您将所有发票命名为“Invoice”，输入<input>invoice</input>即可。字母不区分大小写。</p>
      <note>
        <p>除了直接输入文字，还可以点击工具栏中的<media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
        </media>，或者按 <keyseq><key>Ctrl</key><key>F</key></keyseq>。</p>
      </note>
    </item>
    <item>
      <p>您可以按日期、文件类型、是检索文件全文还是仅文件名来搜索以精简结果。</p>
      <p>要使用筛选，请选择文件管理器的<media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol</media>图标左侧的下拉菜单按钮，然后从可用的筛选器中选择：</p>
      <list>
        <item>
          <p><gui>时间范围</gui>：搜索到多远时间前的文件？</p>
        </item>
        <item>
          <p><gui>搜索类型</gui>：搜索的文件的类型是什么？</p>
        </item>
        <item>
          <p>包括全文搜索，还是只搜索文件名？</p>
        </item>
      </list>
    </item>
    <item>
      <p>要删除过滤条件，请选择其标签旁边的 <gui>X</gui>。</p>
    </item>
    <item>
      <p>您可以对搜索出来的文件，进行打开、复制、删除或其他操作，就像是在文件管理器的文件夹里操作一样。</p>
    </item>
    <item>
      <p>再次点击工具栏中的<media its:translate="no" type="image" mime="image/svg" src="figures/edit-find-symbolic.svg"> <key>Search</key> key symbol
      </media>退出搜索返回文件夹。</p>
    </item>
  </steps>

<section id="customize-files-search">
  <title>自定义文件搜索</title>

<p>您可能希望<app>文件</app>应用程序的搜索结果中包含或排除某些目录。要自定义搜索哪些目录：</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>搜索</gui>。</p>
    </item>
    <item>
      <p>从结果中选择<guiseq><gui>设置</gui><gui>搜索</gui></guiseq>。这将打开<gui>搜索设置</gui>面板。</p>
    </item>
    <item>
      <p>点击标题栏中的<gui>搜索位置</gui>按钮。</p>
    </item>
  </steps>

<p>这将打开一个单独的设置面板，它允许您打开或关闭目录搜索。您可以在三个标签上分别切换搜索：</p>

  <list>
    <item>
      <p><gui>位置</gui>：列出常见的主目录位置</p>
    </item>
    <item>
      <p><gui>书签</gui>：列出您在<app>文件</app>应用程序中加为书签的目录位置</p>
    </item>
    <item>
      <p><gui>其他</gui>：列出您通过 <gui>+</gui> 按钮加入的目录位置。</p>
    </item>
  </list>

</section>

</page>
