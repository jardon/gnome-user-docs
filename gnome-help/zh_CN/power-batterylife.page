<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="41" date="2021-09-08" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>计算机节电的技巧。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>节电和延长电池续航</title>

  <p>电脑可以有很大的耗电量，通过一些简便的节电措施，您可以减少电费并且改善环境。</p>

<section id="general">
  <title>常规提示</title>

<list>
  <item>
    <p>在不使用电脑的时候，<link xref="shell-exit#suspend">挂起</link>电脑，这可以减少大量的电能损耗，而且它也可以很方便地唤醒。</p>
  </item>
  <item>
    <p>当您较长时间不使用电脑时，请将其<link xref="shell-exit#shutdown">关闭</link>。有人担心经常关机会导致电脑磨损加快，但事实并非如此。</p>
  </item>
  <item>
    <p>使用<app>设置</app>中的<gui>电源</gui>面板来更改电源设置。有许多选项有助于省电：您可以在一定时间后<link xref="display-blank">自动息屏</link>；在电池电量低时启用<gui>自动<link xref="power-profile">省电</link></gui>模式；如果在一定时间内不使用，则让电脑<link xref="power-autosuspend">自动挂起</link>。</p>
  </item>
  <item>
    <p>降低<link xref="display-brightness">屏幕亮度</link>。</p>
  </item>
  <item>
    <p>不使用外部设备（例如打印机和扫描仪）时，请将其关闭。</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>笔记本、上网本和其他电池供电设备</title>

 <list>
   <item>
     <p>降低<link xref="display-brightness">屏幕亮度</link>。屏幕供电占了笔记本电脑耗电量的很大一部分。</p>
     <p>大多数笔记本键盘上有一个按钮（或快捷键），可以降低屏幕亮度。</p>
   </item>
   <item>
     <p>如果您暂时不需要网络连接，请<link xref="power-wireless">关闭无线或蓝牙卡</link>。这些设备通过广播无线电波工作，会消耗不少电量。</p>
     <p>一些计算机有一个拨动开关可以用来关闭它，另外有一些是用键盘快捷键，在需要的时候再开启它们。</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>更多高级提示</title>

 <list>
   <item>
     <p>减少后台运行的任务，电脑在进行多个任务时耗电也多。</p>
     <p>当不频繁使用时，您所运行的大多数应用程序的能耗非常小，但经常从互联网上抓取数据和播放音乐或电影的应用程序会显著影响耗电量。</p>
   </item>
 </list>

</section>

</page>
