<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-left-handed" xml:lang="zh-CN">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>切换 Wacom 数位板为惯用左手或惯用右手。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>左手或右手使用数位板</title>

  <p>一些数位板在侧边有实体按键。数位板可以旋转180度以定位这些按键。默认情况下，方向适用于惯用右手的人。要切换方向为惯用左手：</p>

<steps>
  <item>
    <p>打开 <gui xref="shell-introduction#activities">活动</gui> 概览，并开始输入 <gui>Wacom 数位板</gui>。</p>
  </item>
  <item>
    <p>点击 <gui>Wacom 数位板</gui>打开面板。</p>
    <note style="tip"><p>如果没有检测到数位板，您会被要求 <gui>请插入并打开您的 Wacom 数位板</gui>。点击侧边栏中的 <gui>蓝牙</gui> 以连接无线数位板。</p></note>
  </item>
  <item>
    <p>设置<gui>惯用左手</gui>开关为开启状态。</p>
  </item>
</steps>

</page>
