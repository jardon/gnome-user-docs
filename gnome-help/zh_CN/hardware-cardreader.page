<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>介质卡读卡器故障排除。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>读卡器问题</title>

<p>许多计算机带有读卡器，像 SD、MMC、SM、MS、CF 和其他设备存储卡的读卡器。它们应该会被自动检测并<link xref="disk-partitions">挂载</link>。如果没有自动挂载，请尝试以下步骤来解决：</p>

<steps>
<item>
<p>确保卡放置正确。许多卡正确插入时反面朝上。同时确保卡牢固地插在卡槽里；某些卡，尤其是 CF 卡，需要略用力来插紧。（小心用力不要过大！如果觉得有很大阻力，不要硬插。）</p>
</item>

<item>
  <p>从<gui xref="shell-introduction#activities">活动</gui>概览中打开<app>文件</app>。看插入的卡是否出现在左方侧边栏中？有时卡出现在此列表中但未挂载，点击它来挂载。如果没有侧边栏，按 <key>F9</key> 或点击顶栏的<gui style="menu">文件</gui>后选择<gui style="menuitem">侧边栏</gui>。</p>
</item>

<item>
  <p>如果您的卡未显示在侧边栏中，按 <keyseq><key>Ctrl</key><key>L</key></keyseq>，输入 <input>computer://</input> 后按 <key>Enter</key>。如果您的读卡器配置正确，其在没插卡时应显示为驱动器，而插卡后显示为卡本身。</p>
</item>

<item>
<p>如果您看到了读卡器但没看到卡，问题可能在卡上。尝试其他卡或试试另一个读卡器。</p>
</item>
</steps>

<p>如果<gui>计算机</gui>文件夹中没有卡或读卡器，可能您的读卡器由于驱动问题无法在 Linux 上使用。如果是内置读卡器（在计算机内部而不是外部），这种情况的可能性更大。最好的解决办法是直接将您的设备（相机、手机等）连接到计算机的 USB 接口上。USB 外接读卡器也行，而且 Linux 对它们的支持要好得多。</p>

</page>
