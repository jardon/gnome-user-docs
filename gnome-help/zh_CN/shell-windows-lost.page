<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>检查<gui>活动</gui>概览或其他工作区。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>找出丢失的窗口</title>

  <p>使用<gui xref="shell-introduction#activities">活动</gui>概览，可以很容易地找到不同工作区的或隐藏在其他窗口后面的窗口：</p>

  <list>
    <item>
      <p>打开<gui>活动</gui>概览。如果丢失的窗口在当前<link xref="shell-windows#working-with-workspaces">工作区</link>中，它将以缩略图形式显示在这里。只需点击缩略图即可重新显示窗口，或者</p>
    </item>
    <item>
      <p>点击<link xref="shell-workspaces">工作区选择器</link>中的不同工作区，尝试找到您的窗口，或者</p>
    </item>
    <item>
      <p>右键点击 Dash 中的应用程序，其打开的窗口将被列出。点击列表中的窗口即可切换到该窗口。</p>
    </item>
  </list>

  <p>使用窗口切换器：</p>

  <list>
    <item>
      <p>按 <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> 显示<link xref="shell-windows-switching">窗口切换器</link>。继续按住 <key>Super</key> 键并按 <key>Tab</key> 循环浏览打开的窗口，或 <keyseq><key>Shift</key><key>Tab</key> </keyseq> 向后循环。</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>如果一个应用程序有多个打开的窗口，请按住 <key>Super</key> 并按 <key>`</key>（或 <key>Tab</key> 上面的键）来浏览它们。</p>
    </item>
  </list>

</page>
