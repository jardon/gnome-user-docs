<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>按快捷键时可以依次按下每一个键，而无需一次按住所有键。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>开启粘滞键</title>

  <p><em>粘滞键</em>使您能够在按键盘快捷键时每次按一个键，而无需一次按住所有键。例如，<keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> 快捷键可以切换窗口。如果未开启粘滞键，您需要同时按下这两个键；而开启粘滞键后，先按 <key>Super</key> 再按 <key>Tab</key> 可以完成相同操作。</p>

  <p>如果您感觉同时按住几个键有困难，可以考虑开启粘滞键。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击<gui>设置</gui>。</p>
    </item>
    <item>
      <p>点击侧边栏中的<gui>辅助功能</gui>打开面板。</p>
    </item>
    <item>
      <p>按<gui>打字</gui>部分的<gui>AccessX 打字助手</gui>。</p>
    </item>
    <item>
      <p>切换<gui>粘滞键</gui>开关为开启状态。</p>
    </item>
  </steps>

  <note style="tip">
    <title>快速开启和关闭粘滞键</title>
    <p>切换<gui>通过键盘启用</gui>开关，可以通过键盘来开启和关闭粘滞键。选中此项，您可以通过按5次<key>Shift</key>键，来启用或关闭粘滞键功能。</p>
    <p>您可以通过点击顶栏上的<link xref="a11y-icon">辅助功能图标</link>并选择<gui>粘滞键</gui>来开启或关闭此功能。辅助功能图标在已启用一项或多项<gui>辅助功能</gui>面板中的设置时可见。</p>
  </note>

  <p>如果您一次按住了两个键，就可以临时关闭粘滞键，像平常那样输入快捷键。</p>

  <p>例如，如果您打开了此选项，并且您开启了粘滞键，但同时按下了 <key>Super</key> 和 <key>Tab</key> 键，粘滞键不会等待您按下另一键。但是，如果您只按了一个键，它<em>将会</em>等待您按下另一个。如果您能够同时按下某些键盘快捷键（例如，紧邻的几个键）但不能按住其他快捷键，此选项将非常有用。</p>

  <p>选中<gui>同时按下两个键时禁用输入</gui>来启用它。</p>

  <p>您可以对计算机进行设置，使其在您开始按键盘快捷键时（粘滞键开启的情况下）发“哔”声。如果您想要知道粘滞键是否认为要输入的是键盘快捷键，即下一个按键是否是快捷键的一部分，这将非常有用。选择<gui>按修饰键时蜂鸣</gui>可启用此功能。</p>

</page>
