<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>检查声音是否静音，线缆是否插好，是否检测到声卡。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>我听不到电脑上的声音</title>

  <p>如果您听不到电脑上的任何声音，例如当您尝试播放音乐时，请按以下故障排除提示操作。</p>

<section id="mute">
  <title>确保声音没有被“静音”</title>

  <p>打开<gui xref="shell-introduction#systemmenu">系统菜单</gui>，并确保声音没有被静音或调低。</p>

  <p>有些笔记本电脑的键盘上有静音开关或键——试着按那个键，看看是否能解除静音。</p>

  <p>您还应该检查您是否将播放声音的应用程序（例如，您的音乐播放器或电影播放器）静音了。应用程序的主窗口中可能有一个静音或音量按钮，请检查一下。</p>

  <p>另外，您也可以在<gui>声音</gui>面板中检查其音量滑块：</p>
  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>声音</gui>。</p>
    </item>
    <item>
      <p>点击<gui>声音</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>音量级别</gui>下，检查您的应用程序是否处于静音状态。音量滑块末端的按钮可以打开和关闭<gui>静音</gui>。</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>检查音箱电源打开，并且连接正确</title>
  <p>如果您的电脑有外接音箱，请确保它们已打开，并且已调高音量。确保音箱线牢固地插入计算机的”output“音频插孔。这个插孔通常是浅绿色的。</p>

  <p>有些声卡用于输出的插孔（到音箱）和用于输入的插孔（例如从麦克风）之间可能会变换。运行在 Linux、Windows 或 MacOS 上时，输出插孔可能不同。请尝试将音箱线接入计算机上的不同音频插孔。</p>

 <p>最后要检查的是，音频线是否牢牢地插在音箱的后面。有些音箱也支持多路输入。</p>
</section>

<section id="device">
  <title>检查是否选择了正确的声音设备</title>

  <p>有些电脑安装了多个“声音设备”。其中有些能够输出声音但另一些不能，所以您应该检查您选择的声音设备是否正确。这也许可以用试错的方法来选择正确的设备。</p>

  <steps>
    <item>
      <p>打开<gui xref="shell-introduction#activities">活动</gui>概览后输入<gui>声音</gui>。</p>
    </item>
    <item>
      <p>点击<gui>声音</gui>打开面板。</p>
    </item>
    <item>
      <p>在<gui>输出</gui>下，选择<gui>输出设备</gui>然后点击<gui>测试</gui>按钮，看看它是否能正常工作。</p>

      <p>您可能需要尝试每一个可用的设备。</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>检查声卡已经正确检测到</title>

  <p>声卡可能因为没有安装声卡驱动而无法被检测到。您可能需要手动安装声卡驱动。如何安装取决于声卡的类型。</p>

  <p>在终端中运行 <cmd>lspci</cmd> 命令来找到您的声卡：</p>
  <steps>
    <item>
      <p>回到<gui>活动</gui>视图，打开一个终端。</p>
    </item>
    <item>
      <p>以<link xref="user-admin-explain">管理员权限</link>运行 <cmd>lspci</cmd>；或者输入 <cmd>sudo lspci</cmd> 并输入密码；再或者输入 <cmd>su</cmd>，输入 <em>root</em>（管理员）密码，然后输入 <cmd>lspci</cmd>。</p>
    </item>
    <item>
      <p>检查是否有列出<em>音频控制器</em>或<em>音频设备</em>：在这种情况下，您应该可以看到声卡的品牌和型号。另外，<cmd>lspci -v</cmd> 会显示一个含更多详细信息的列表。</p>
    </item>
  </steps>

  <p>您也许可以自己寻找并安装声卡驱动。最好在支持（或其他）论坛上询问您的 Linux 发行版的说明。</p>

  <p>如果您找不到声卡驱动，您最好购买一个新的声卡。您可以买能安装在电脑内部的声卡或外接的 USB 声卡。</p>

</section>

</page>
