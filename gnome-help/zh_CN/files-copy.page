<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="43" date="2022-09-10" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>复制或移动项目到一个新文件夹中。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

<title>复制或移动文件和文件夹</title>

 <p>通过鼠标的拖放操作、使用复制粘贴命令，或者使用键盘快捷键，可以将文件或文件夹复制或移动到另一个文件夹。</p>

 <p>例如，您可能想把演示文稿复制到 U 盘上以便随身携带和使用。或者，您也可能修改文档之前进行备份（如果您对修改不满意还能使用旧的副本）。</p>

 <p>这些做法对文件和文件夹都有效，您可以用同样的方式移动和复制文件和文件夹。</p>

<steps ui:expanded="false">
<title>复制和粘贴文件</title>
<item><p>单击选中您想复制的文件。</p></item>
<item><p>右击并选择 <gui>复制</gui>，或按 <keyseq><key>Ctrl</key><key>C</key></keyseq>。</p></item>
<item><p>再打开您想要存放复制后文件的目录。</p></item>
<item><p>右击并选择 <gui>粘贴</gui>，或按 <keyseq><key>Ctrl</key><key>V</key></keyseq> 来完成文件复制。这样原文件夹和另一文件夹中都有同一文件的副本了。</p></item>
</steps>

<steps ui:expanded="false">
<title>通过剪切和粘贴操作来移动文件</title>
<item><p>单击选中您想要移动的文件。</p></item>
<item><p>右击并选择 <gui>剪切</gui>，或按 <keyseq><key>Ctrl</key><key>X</key></keyseq>。</p></item>
<item><p>再打开您想要存放移动后项目的文件夹。</p></item>
<item><p>右击并选择 <gui>粘贴</gui>，或按 <keyseq><key>Ctrl</key><key>V</key></keyseq> 来完成文件移动。该文件将从原文件夹中取出并移动到另一文件夹中。</p></item>
</steps>

<steps ui:expanded="false">
<title>用拖动的方法复制或移动文件</title>
<item><p>打开文件管理器，进入您想要复制项目的文件夹。</p></item>
<item><p>点击窗口右上角的菜单按钮，然后选择 <gui>新建窗口</gui>（或者按组合键 <keyseq><key>Ctrl</key><key>N</key></keyseq>）以打开第二个窗口。在新窗口中，找到您想要移动或复制文件的文件夹。</p></item>
<item>
 <p>单击并将该项目从一个窗口拖动至另一个。默认情况下，如果目的地在<em>同一</em>设备上（即如果两个文件夹都在计算机的同一个分区），拖动项目将<em>移动</em>该项目。如果目标文件夹在<em>不同</em>的设备分区中，这将会<em>复制它</em>。</p>
 <p>例如，如果您将文件从 U 盘拖到用户文件夹，该文件将会被复制，因为这是在不同的设备间拖动。</p>
 <p>您可以在拖动时按住 <key>Ctrl</key> 键来强制要求复制，或者按住 <key>Shift</key> 键来强制要求移动。</p>
 </item>
</steps>

<note>
  <p>您不能将文件复制或移动到<em>只读</em>文件夹。有些文件是只读的以防止您更改其内容。您可以通过<link xref="nautilus-file-properties-permissions">更改文件权限</link>来改变其只读状态。</p>
</note>

</page>
