<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>双击或拖动标题栏以最大化或恢复窗口。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>tuberry</mal:name>
      <mal:email>orzun@foxmail.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Victor_Trista</mal:name>
      <mal:email>ljlzjzm@hotmail.com</mal:email>
      <mal:years>2023</mal:years>
    </mal:credit>
  </info>

  <title>最大化和取消最大化（恢复）窗口</title>

  <p>您可以将窗口最大化，占满整个桌面的空间，或者取消最大化恢复窗口的正常大小。您也可以将窗口从左到右竖直最大化，这样就可以容易的同时看到多个窗口。查看 <link xref="shell-windows-tiled"/> 获取更多细节。</p>

  <p>要使窗口最大化，请抓住标题栏并将其拖动到屏幕顶部，或者直接双击标题栏。要使用键盘来操作，请按住 <key xref="keyboard-key-super">Super</key> 键并按 <key>↑</key>，或按 <keyseq><key>Alt</key><key>F10</key></keyseq>。</p>

  <p if:test="platform:gnome-classic">您也可以通过点击标题栏中的最大化按钮来最大化窗口。</p>

  <p>要恢复窗口的原始大小，点击程序的标题栏，把它拖离屏幕边缘，如果窗口是全屏最大化，您可以双击标题栏来恢复。您也可以再次按最大化窗口的快捷键来恢复。</p>

  <note style="tip">
    <p>按住 <key>Super</key> 键，并拖动窗口中的任意位置来移动窗口。</p>
  </note>

</page>
