<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="el">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Εκτυπώστε και στις δυο όψεις του χαρτιού, ή πολλαπλών σελίδων ανά φύλλο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Εκτύπωση διπλής όψης και διατάξεις πολλαπλών σελίδων</title>

  <p>Για να εκτυπώσετε και στις δύο πλευρές κάθε φύλλου χαρτιού:</p>

  <steps>
    <item>
      <p>Άνοίξτε τον διάλόγο εκτύπωσης πατώντας <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Πηγαίνετε στην καρτέλα <gui>Διαμόρφωση σελίδας</gui> του παραθύρου εκτύπωσης και επιλέξτε μια επιλογή από την αναπτυσσόμενη λίστα <gui>Διπλή όψη</gui>. Εάν η επιλογή είναι ανενεργή, η εκτύπωση διπλής όψης δεν είναι διαθέσιμη για τον εκτυπωτή σας.</p>
      <p>Οι εκτυπωτές χειρίζονται την εκτύπωση διπλής όψης με διαφορετικούς τρόπους. Είναι μια καλή ιδέα να πειραματιστείτε με τον εκτυπωτή σας για να δείτε πώς δουλεύει.</p>
    </item>
    <item>
      <p>Μπορείτε να εκτυπώσετε πάνω από μια σελίδα του εγγράφου ανά <em>πλευρά</em> χαρτιού επίσης. Χρησιμοποιήστε την επιλογή <gui>Σελίδες ανά πλευρά</gui> για να το κάνετε αυτό.</p>
    </item>
  </steps>

  <note>
    <p>Η διαθεσιμότητα αυτών των επιλογών μπορεί να εξαρτάται από τον τύπο του εκτυπωτή που έχετε, καθώς και από την εφαρμογή που χρησιμοποιείτε. Αυτή η επιλογή μπορεί να μην είναι πάντα διαθέσιμη.</p>
  </note>

</page>
