<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="el">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ο προσαρμογέας μπορεί να είναι ανενεργός ή μπορεί να μην έχει οδηγούς, ή το Bluetooth μπορεί να είναι ανενεργό ή φραγμένο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Δεν μπορώ να συνδέσω τη συσκευή Bluetooth</title>

  <p>Υπάρχουν πολλοί λόγοι γιατί δεν μπορείτε να συνδεθείτε με μια συσκευή Bluetooth, όπως ένα τηλέφωνο ή ακουστικά.</p>

  <terms>
    <item>
      <title>Φραγμένη ή αναξιόπιστη σύνδεση</title>
      <p>Μερικές συσκευές Bluetooth φράσσουν τις συνδέσεις από προεπιλογή, ή απαιτούν την αλλαγή μιας ρύθμισης για να μπορέσουν να γίνουν συνδέσεις. Βεβαιωθείτε ότι η συσκευή σας ρυθμίστηκε να επιτρέπει συνδέσεις.</p>
    </item>
    <item>
      <title>Δεν αναγνωρίστηκε το Bluetooth</title>
      <p>Ο προσαρμογέας Bluetooth μπορεί να μην έχει αναγνωριστεί από τον υπολογιστή. Αυτό μπορεί να συνέβη επειδή οι <link xref="hardware-driver">οδηγοί</link> για τον προσαρμογέα δεν είναι εγκατεστημένοι. Μερικοί προσαρμογείς Bluetooth δεν υποστηρίζονται στο Linux, έτσι ίσως να μην μπορείτε να βρείτε τους σωστούς οδηγούς για αυτούς. Σε αυτήν την περίπτωση, θα πρέπει να πάρετε προφανώς έναν διαφορετικό προσαρμογέα Bluetooth.</p>
    </item>
    <item>
      <title>Ο προσαρμογέας δεν ενεργοποιήθηκε</title>
        <p>Βεβαιωθείτε ότι ο προσαρμογέας σας Bluetooth είναι ενεργός. Ανοίξτε τον πίνακα Bluetooth και ελέγξτε ότι δεν είναι <link xref="bluetooth-turn-on-off">ανενεργός</link>.</p>
    </item>
    <item>
      <title>Ανενεργή σύνδεση συσκευής Bluetooth</title>
      <p>Ελέγξετε ότι το Bluetooth είναι ενεργό στη συσκευή στην οποία προσπαθείτε να συνδεθείτε και ότι αυτή είναι <link xref="bluetooth-visibility">αντιληπτή ή ορατή</link>. Για παράδειγμα, εάν προσπαθείτε να συνδεθείτε με ένα τηλέφωνο, βεβαιωθείτε ότι δεν είναι σε λειτουργία αεροπλάνου.</p>
    </item>
    <item>
      <title>Δεν υπάρχει προσαρμογέας Bluetooth στον υπολογιστή σας</title>
      <p>Πολλοί υπολογιστές δεν έχουν προσαρμογείς Bluetooth. Μπορείτε να αγοράσετε έναν προσαρμογέα εάν θέλετε να χρησιμοποιήσετε Bluetooth.</p>
    </item>
  </terms>

</page>
