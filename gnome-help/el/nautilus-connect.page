<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-connect" xml:lang="el">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>

    <revision pkgversion="3.6.0" date="2012-10-06" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Δείτε και επεξεργαστείτε αρχεία σε άλλον υπολογιστή μέσα από FTP, SSH, κοινόχρηστα Windows ή WebDAV.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Περιήγηση αρχείων σε έναν διακομιστή ή σε ένα κοινόχρηστο δίκτυο</title>

<p>Μπορείτε να συνδεθείτε σε έναν διακομιστή ή ένα κοινόχρηστο δίκτυο για να περιηγηθείτε και να προβάλετε τα αρχεία, σαν να ήταν στον δικό σας υπολογιστή. Αυτός είναι ένας βολικός τρόπος να κάνετε λήψη ή να αποστείλετε αρχεία στο διαδίκτυο, ή να μοιραστείτε αρχεία με άλλα άτομα στο τοπικό σας δίκτυο.</p>

<p>To browse files over the network, open the <app>Files</app>
application from the <gui>Activities</gui> overview, and click
<gui>Other Locations</gui> in the sidebar. The file manager
will find any computers on your local area network that advertise
their ability to serve files. If you want to connect to a server
on the internet, or if you do not see the computer you’re looking
for, you can manually connect to a server by typing in its
internet/network address.</p>

<steps>
  <title>Σύνδεση σε έναν διακομιστή αρχείων</title>
  <item><p>Στον διαχειριστή αρχείων, στην πλευρική στήλη κάντε κλικ στο <gui>Άλλες τοποθεσίες</gui>.</p>
  </item>
  <item><p>Στο πεδίο <gui>Σύνδεση σε διακομιστή</gui>, εισάγετε τη διεύθυνση του διακομιστή, με τη μορφή <link xref="#urls">διεύθυνσης</link>. Λεπτομέρειες σχετικά με τους υποστηριζόμενους τύπους διευθύνσεων θα <link xref="#types"> δείτε παρακάτω</link>.</p>
  <note>
    <p>Εάν έχετε συνδεθεί με τον διακομιστή πριν, μπορείτε να κάνετε κλικ σε αυτόν στη λίστα <gui>Πρόσφατοι διακομιστές</gui>.</p>
  </note>
  </item>
  <item>
    <p>Κάντε κλικ στο <gui>Σύνδεση</gui>. Θα εμφανιστούν τα αρχεία του διακομιστή. Μπορείτε να περιηγηθείτε σε αυτά με τον ίδιο τρόπο που βλέπετε τα τοπικά αρχεία. Ο διακομιστής θα προστεθεί επίσης στην πλευρική στήλη ώστε να μπορείτε να τον χρησιμοποιήσετε άμεσα οποιαδήποτε στιγμή.</p>
  </item>
</steps>

<section id="urls">
 <title>Συγγραφή διευθύνσεων</title>

<p>Μία <em>Διεύθυνση</em>, ή <em>ενιαίος εντοπιστής πόρου</em>, είναι μια μορφή διεύθυνσης που αναφέρεται σε μια τοποθεσία ή αρχείο στο δίκτυο. Η διεύθυνση μορφοποιείται όπως αυτή:</p>
  <example>
    <p><sys>scheme://servername.example.com/folder</sys></p>
  </example>
<p>Το <em>σχήμα</em> καθορίζει το πρωτόκολλο ή τύπο του διακομιστή. Το τμήμα <em>example.com</em> της διεύθυνσης λέγεται το <em>όνομα τομέα</em>. Αν ένα όνομα χρήστη απαιτείται, εισάγεται πριν το όνομα του διακομιστή:</p>
  <example>
    <p><sys>scheme://username@servername.example.com/folder</sys></p>
  </example>
<p>Μερικά σχήματα απαιτούν να καθοριστεί ο αριθμός θύρας. Εισάγετε τον μετά το όνομα τομέα:</p>
  <example>
    <p><sys>scheme://servername.example.com:port/folder</sys></p>
  </example>
<p>Παρακάτω υπάρχουν συγκεκριμένα παραδείγματα για τους ποικίλους τύπους διακομιστή που υποστηρίζονται.</p>
</section>

<section id="types">
 <title>Τύποι διακομιστών</title>

<p>Μπορείτε να συνδεθείτε σε διαφορετικούς τύπους διακομιστών. Μερικοί διακομιστές είναι δημόσιοι και επιτρέπουν σε οποιονδήποτε να συνδεθεί. Άλλοι διακομιστές απαιτούν να συνδεθείτε με όνομα χρήστη και κωδικό πρόσβασης.</p>
<p>Μπορεί να μην έχετε δικαιώματα για εκτέλεση συγκεκριμένων ενεργειών σε αρχεία σε έναν διακομιστή. Για παράδειγμα, σε δημόσιους τόπους FTP, δεν θα μπορείτε προφανώς να διαγράψετε αρχεία.</p>
<p>Η διεύθυνση που εισάγετε εξαρτάται από το πρωτόκολλο που χρησιμοποιεί ο διακομιστής για εξαγωγή των κοινόχρηστων αρχείων του.</p>
<terms>
<item>
  <title>SSH</title>
  <p>Εάν έχετε έναν λογαριασμό <em>ασφαλούς κελύφους</em> σε έναν διακομιστή, μπορείτε να συνδεθείτε χρησιμοποιώντας αυτήν τη μέθοδο. Πολλοί κεντρικοί υπολογιστές ιστού παρέχουν λογαριασμούς SSH σε μέλη ώστε να μπορούν με ασφάλεια να αποστείλουν αρχεία. Διακομιστές SSH απαιτούν πάντα να συνδεθείτε.</p>
  <p>Μία τυπική διεύθυνση SSH φαίνεται σαν αυτή:</p>
  <example>
    <p><sys>ssh://username@servername.example.com/folder</sys></p>
  </example>

  <p>When using SSH, all the data you send (including your password)
  is encrypted so that other users on your network can’t see it.</p>
</item>
<item>
  <title>FTP (με σύνδεση)</title>
  <p>FTP είναι ένας δημοφιλής τρόπος ανταλλαγής αρχείων στο διαδίκτυο. Επειδή τα δεδομένα δεν είναι κρυπτογραφημένα στο FTP, πολλοί διακομιστές τώρα παρέχουν πρόσβαση μέσα από SSH. Μερικοί διακομιστές, επιτρέπουν ακόμα ή απαιτούν τη χρήση FTP για τη λήψη ή αποστολή αρχείων. Οι τόποι FTP με συνδέσεις θα σας επιτρέπουν συνήθως να διαγράψετε και να αποστείλετε αρχεία.</p>
  <p>Μία τυπική διεύθυνση FTP φαίνεται σαν αυτή:</p>
  <example>
    <p><sys>ftp://username@ftp.example.com/path/</sys></p>
  </example>
</item>
<item>
  <title>Δημόσιο FTP</title>
  <p>Τόποι που σας επιτρέπουν τη λήψη αρχείων θα παρέχουν μερικές φορές δημόσια ή ανώνυμη πρόσβαση FTP. Αυτοί οι διακομιστές δεν απαιτούν όνομα χρήστη και κωδικό πρόσβασης και δεν θα σας επιτρέπουν συνήθως να διαγράψετε ή να αποστείλετε αρχεία.</p>
  <p>Μία τυπική διεύθυνση FTP φαίνεται σαν αυτή:</p>
  <example>
    <p><sys>ftp://ftp.example.com/path/</sys></p>
  </example>
  <p>Μερικοί ανώνυμοι τόποι FTP απαιτούν να συνδεθείτε με ένα δημόσιο όνομα χρήστη και κωδικό πρόσβασης, ή με ένα δημόσιο όνομα χρήστη χρησιμοποιώντας τη διεύθυνση ηλεκτρονικής αλληλογραφίας σας ως κωδικό πρόσβασης. Για αυτούς τους διακομιστές, χρησιμοποιήστε τη μέθοδο <gui>FTP (με σύνδεση)</gui> και χρησιμοποιήστε τα καθορισμένα διαπιστευτήρια από τον τόπο FTP.</p>
</item>
<item>
  <title>Κοινόχρηστο Windows</title>
  <p>Οι υπολογιστές Windows χρησιμοποιούν ένα ιδιοταγές πρωτόκολλο για μερισμό αρχείων σε ένα τοπικό δίκτυο περιοχής. Οι υπολογιστές σε δίκτυο Windows είναι μερικές φορές ομαδοποιημένοι σε <em>τομείς</em> για οργάνωση και καλύτερο έλεγχο πρόσβασης. Εάν έχετε τις σωστές άδειες στον απομακρυσμένο υπολογιστή, μπορείτε να συνδεθείτε με ένα κοινόχρηστο Windows από τον διαχειριστής αρχείων.</p>
  <p>Μία τυπική κοινόχρηστη διεύθυνση Windows φαίνεται σαν αυτή:</p>
  <example>
    <p><sys>smb://servername/Share</sys></p>
  </example>
</item>
<item>
  <title>WebDAV και ασφαλές WebDAV</title>
  <p>Based on the HTTP protocol used on the web, WebDAV is sometimes used to
  share files on a local network and to store files on the internet. If the
  server you’re connecting to supports secure connections, you should choose
  this option. Secure WebDAV uses strong SSL encryption, so that other users
  can’t see your password.</p>
  <p>Μια διεύθυνση WebDAV μοιάζει με:</p>
  <example>
    <p><sys>dav://example.hostname.com/path</sys></p>
  </example>
</item>
<item>
  <title>Κοινόχρηστο NFS</title>
  <p>Οι υπολογιστές UNIX παραδοσιακά χρησιμοποιούν το πρωτόκολλο NFS για να διαμοιράσουν αρχεία μέσω του τοπικού δικτύου. Με το NFS, η ασφάλεια βασίζεται στο UID του χρήστη που έχει πρόσβαση, έτσι δεν απαιτούνται διαπιστευτήρια πιστοποίησης κατά τη σύνδεση.</p>
  <p>Μια διεύθυνση κοινόχρηστου NFS μοιάζει με:</p>
  <example>
    <p><sys>nfs://servername/path</sys></p>
  </example>
</item>
</terms>
</section>

</page>
