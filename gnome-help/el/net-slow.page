<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-slow" xml:lang="el">

  <info>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-21" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μπορεί να γίνεται λήψη πραγμάτων, αλλά μπορεί να έχετε μια κακή σύνδεση, ή θα μπορούσε να είναι μια πολυάσχολη ώρα της ημέρας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Το διαδίκτυο φαίνεται αργό</title>

  <p>Εάν χρησιμοποιείτε το διαδίκτυο και φαίνεται αργό, υπάρχει ένας αριθμός από πράγματα που μπορεί να προκαλούν την επιβράδυνση.</p>

  <p>Δοκιμάστε το κλείσιμο του περιηγητή ιστού και έπειτα ξανανοίξτε τον, ή αποσυνδεθείτε από το διαδίκτυο και έπειτα ξανασυνδεθείτε. (Κάνοντας αυτό επαναφέρει πολλά πράγματα που μπορεί να προκαλούν την επιβράδυνση του διαδικτύου.)</p>

  <list>
    <item>
      <p><em style="strong">Απασχολημένη ώρα της ημέρας</em></p>
      <p>Internet service providers commonly setup internet connections so that
      they are shared between several households. Even though you connect
      separately, through your own phone line or cable connection, the
      connection to the rest of the internet at the telephone exchange might
      actually be shared. If this is the case and lots of your neighbors are
      using the internet at the same time as you, you might notice a slow-down.
      You’re most likely to experience this at times when your neighbors are
      probably on the internet (in the evenings, for example).</p>
    </item>
    <item>
      <p><em style="strong">Λήψη πολλών πραγμάτων ταυτόχρονα</em></p>
      <p>Εάν εσείς ή κάποιος άλλος που χρησιμοποιεί τη σύνδεση διαδικτύου κάνει λήψη πολλών αρχείων ταυτόχρονα, ή παρακολουθεί βίντεο, η σύνδεση διαδικτύου μπορεί να μην είναι αρκετά γρήγορη για να κρατηθεί με τη ζήτηση. Σε αυτήν την περίπτωση, η σύνδεση θα είναι πιο αργή.</p>
    </item>
    <item>
      <p><em style="strong">Αναξιόπιστη σύνδεση</em></p>
      <p>Μερικές συνδέσεις διαδικτύου είναι απλά αναξιόπιστες, ειδικά οι προσωρινές ή αυτές σε περιοχές με υψηλή ζήτηση. Εάν είσαστε σε ένα πολυάσχολο καφενείο ή σε ένα κέντρο διάσκεψης, η σύνδεση διαδικτύου μπορεί να είναι υπερβολικά απασχολημένη ή απλά αναξιόπιστη.</p>
    </item>
    <item>
      <p><em style="strong">Χαμηλό ασύρματο σήμα σύνδεσης</em></p>
      <p>If you are connected to the internet by wireless (Wi-Fi), check the
      network icon on the top bar to see if you have good wireless signal. If
      not, the internet may be slow because you don’t have a very strong
      signal.</p>
    </item>
    <item>
      <p><em style="strong">Χρησιμοποιώντας μια πιο αργή κινητή σύνδεση διαδικτύου</em></p>
      <p>If you have a mobile internet connection and notice that it is slow,
      you may have moved into an area where signal reception is poor. When this
      happens, the internet connection will automatically switch from a fast
      “mobile broadband” connection like 3G to a more reliable, but slower,
      connection like GPRS.</p>
    </item>
    <item>
      <p><em style="strong">Ο περιηγητής ιστού έχει πρόβλημα</em></p>
      <p>Sometimes web browsers encounter a problem that makes them run slow.
      This could be for any number of reasons — you could have visited a
      website that the browser struggled to load, or you might have had the
      browser open for a long time, for example. Try closing all of the
      browser’s windows and then opening the browser again to see if this makes
      a difference.</p>
    </item>
  </list>

</page>
