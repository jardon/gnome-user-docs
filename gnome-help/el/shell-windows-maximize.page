<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="el">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Κάντε διπλό κλικ ή σύρετε μια γραμμή τίτλου για να μεγιστοποιήσετε ή να επαναφέρετε ένα παράθυρο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Μεγιστοποίηση και απομεγιστοποίηση παραθύρου</title>

  <p>Μπορείτε να μεγιστοποιήσετε ένα παράθυρο για να καταλάβει όλον τον χώρο στην επιφάνεια εργασίας σας και να απομεγιστοποιήσετε ένα παράθυρο για επαναφορά του στο κανονικό του μέγεθος. Μπορείτε επίσης να μεγιστοποιήσετε κάθετα τα παράθυρα στην αριστερή και δεξιά πλευρά της οθόνης, έτσι ώστε να μπορείτε εύκολα να κοιτάτε δύο παράθυρα μονομιάς. Για λεπτομέρειες δείτε <link xref="shell-windows-tiled"/>.</p>

  <p>Για να μεγιστοποιήσετε ένα παράθυρο, πάρτε τη γραμμή τίτλου και σύρετέ την στην κορυφή της οθόνης, ή απλά κάντε διπλό κλικ στη γραμμή του τίτλου. Για μεγιστοποίηση ενός παραθύρου χρησιμοποιώντας το πληκτρολόγιο, κρατήστε πατημένο το πλήκτρο <key xref="keyboard-key-super">Λογότυπο</key> και πατήστε <key>↑</key>, ή πατήστε <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">Μπορείτε επίσης να μεγιστοποιήσετε ένα παράθυρο κάνοντας κλικ στο κουμπί μεγιστοποίησης στη γραμμή τίτλου.</p>

  <p>Για να επαναφέρετε ένα παράθυρο στο κανονικό του μέγεθος, σύρετε το μακριά από τις άκρες της οθόνης. Εάν το παράθυρο είναι πλήρως μεγιστοποιημένο, μπορείτε με διπλό κλικ στη γραμμή τίτλου να το επαναφέρετε. Μπορείτε επίσης να χρησιμοποιήσετε τις ίδιες συντομεύσεις πληκτρολογίου που χρησιμοποιήσατε για τη μεγιστοποίηση του.</p>

  <note style="tip">
    <p>Κρατήστε πατημένο το πλήκτρο <key>Λογότυπο</key> και σύρετε το οπουδήποτε σε ένα παράθυρο για να το μετακινήσετε.</p>
  </note>

</page>
