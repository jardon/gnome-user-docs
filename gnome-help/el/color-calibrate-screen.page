<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="el">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Η βαθμονόμηση της οθόνη σας είναι σημαντική για τη προβολή πιστών χρωμάτων.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Πως βαθμονομώ την οθόνη μου;</title>

  <p>Μπορείτε να βαθμονομήσετε την οθόνη σας έτσι ώστε να εμφανίζει πιο ακριβές χρώμα. Αυτό είναι ιδιαίτερα χρήσιμο αν ασχολείστε με ψηφιακή φωτογραφία, σχεδίαση.</p>

  <p>Θα χρειαστείτε είτε ένα χρωματόμετρο ή ένα φασματοφωτόμετρο για να το κάνετε αυτό. Και οι δυο συσκευές χρησιμοποιούνται για οθόνες προφίλ, αλλά δουλεύουν με ελαφρά διαφορετικούς τρόπους.</p>

  <steps>
    <item>
      <p>Βεβαιωθείτε ότι η συσκευή βαθμονόμησης είναι συνδεμένη στον υπολογιστή σας.</p>
    </item>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select your screen.</p>
    </item>
    <item>
      <p>Πατήστε <gui style="button">Βαθμονόμηση…</gui> για να ξεκινήσετε τη βαθμονόμηση.</p>
    </item>
  </steps>

  <p>Οι οθόνες αλλάζουν συνέχεια - το πίσω φως σε μια οθόνη TFT θα υποδιπλασιάζεται σε φωτεινότητα περίπου κάθε 18 μήνες και θα γίνεται πιο κίτρινη καθώς γερνά. Αυτό σημαίνει ότι θα πρέπει να επαναβαθμονομείτε την οθόνη σας όταν το εικονίδιο [!] εμφανίζεται στον πίνακα <gui>Χρώμα</gui>.</p>

  <p>Οι οθόνες LED επίσης αλλάζουν με τον χρόνο, αλλά πολύ πιο αργά από τις TFTs.</p>

</page>
