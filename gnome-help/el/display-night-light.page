<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="el">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Night Light changes the color of your displays according to the time
    of day.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Adjust the color temperature of your screen</title>

  <p>A computer monitor emits blue light which contributes to sleeplessness and
  eye strain after dark. <gui>Night Light</gui> changes the color of your
  displays according to the time of day, making the color warmer in the
  evening. To enable <gui>Night Light</gui>:</p>

  <steps>
    <item>
      <p>Ανοίξτε την επισκόπηση <gui xref="shell-introduction#activities">Δραστηριότητες</gui> και αρχίστε να πληκτρολογείτε <gui>Οθόνες</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Displays</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Night Light</gui> to open the settings.</p>
    </item>
    <item>
      <p>Ensure the <gui>Night Light</gui> switch is set to on.</p>
    </item>
    <item>
      <p>Under <gui>Schedule</gui>, select <gui>Sunset to Sunrise</gui> to
      make the screen color follow the sunset and sunrise times for your location.
      Select <gui>Manual Schedule</gui> to set the <gui>Times</gui> to a custom schedule.</p>
    </item>
    <item>
      <p>Use the slider to adjust the <gui>Color Temperature</gui> to be more
      warm or less warm.</p>
    </item>
  </steps>
      <note>
        <p>The <link xref="shell-introduction">top bar</link> shows when
        <gui>Night Light</gui> is active. It can be temporarily disabled from
        the system menu.</p>
      </note>



</page>
