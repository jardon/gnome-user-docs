<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-link-unlink" xml:lang="el">

  <info>
    <link type="guide" xref="contacts"/>
    <revision pkgversion="3.5.5" date="2012-02-19" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-27" status="final"/>
    <revision pkgversion="3.15" date="2015-01-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Συνδιάστε πληροφορίες για μια επαφή από πολλαπλές πηγές.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Σύνδεση και αποσύνδεση επαφών</title>

<section id="link-contacts">
  <title>Σύνδεση επαφών</title>

  <p>Μπορείτε να συνδυάσετε διπλές επαφές από το τοπικό βιβλίο διευθύνσεών σας και από τους διαδικτυακούς λογαριασμούς σε μια καταχώριση στις <app>Επαφές</app>. Αυτή η λειτουργία βοηθά στη διατήρηση του βιβλίου διευθύνσεων οργανωμένο, με όλες τις λεπτομέρειες για μια επαφή σε μια θέση.</p>

  <steps>
    <item>
      <p>Ενεργοποιήστε την <em>λειτουργία επιλογής</em> πατώντας το κουμπί επιλογής πάνω από τη λίστα επαφών.</p>
    </item>
    <item>
      <p>Ένα πλαίσιο ελέγχου θα εμφανιστεί δίπλα σε κάθε επαφή. Σημειώστε τα πλαίσια ελέγχου δίπλα στις επαφές που θέλετε να συγχωνεύσετε.</p>
    </item>
    <item>
      <p>Πατήστε <gui style="button">Σύνδεση</gui> για να συνδέσετε τις επιλεγμένες επαφές.</p>
    </item>
  </steps>

</section>

<section id="unlink-contacts">
  <title>Αποσύνδεση επαφών</title>

  <p>Μπορεί να θέλετε να αποσυνδέσετε επαφές, αν συνδέσατε κατά λάθος επαφές που δεν έπρεπε να συνδεθούν.</p>

  <steps>
    <item>
      <p>Select the contact you wish to unlink from your list of contacts.</p>
    </item>
    <item>
      <p>Press <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">view more</span></media> in the top-right corner of
      <app>Contacts</app>.</p>
    </item>
    <item>
      <p>Πατήστε <gui style="button">Αποσύνδεση</gui> για να αποσυνδέσετε την καταχώριση από την επαφή.</p>
    </item>
  </steps>

</section>

</page>
