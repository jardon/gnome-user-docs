<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="el">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Έργο Τεκμηρίωσης GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Μάθετε πόσο συχνά θα πρέπει να αντιγράφετε τα σημαντικά σας αρχεία για να βεβαιωθείτε ότι είναι ασφαλή.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Συχνότητα αντιγράφων ασφαλείας</title>

  <p>Το πόσο συχνά θα κάνετε αντίγραφα ασφαλείας εξαρτάται από τον τύπο των δεδομένων για αντιγραφή. Για παράδειγμα, εάν εκτελείτε ένα περιβάλλον δικτύου με κρίσιμα δεδομένα αποθηκευμένα στους διακομιστές σας, τότε ακόμα και καθημερινά αντίγραφα μπορεί να μην είναι αρκετά.</p>

  <p>Από την άλλη μεριά, εάν αντιγράφετε τα δεδομένα του οικιακού σας υπολογιστή τότε τα ωριαία αντίγραφα ασφαλείας θα είναι πιθανόν περιττά. Μπορείτε να βοηθηθείτε σκεπτόμενοι τα ακόλουθα σημεία όταν σχεδιάζετε το σχεδιασμό των αντιγράφων σας:</p>

<list style="compact">
  <item><p>Η ποσότητα του χρόνου που ξοδεύετε στον υπολογιστή.</p></item>
  <item><p>Πόσο συχνά και κατά πόσο αλλάζουν τα δεδομένα στον υπολογιστή.</p></item>
</list>

  <p>Εάν τα δεδομένα των οποίων θέλετε να δημιουργήσετε αντίγραφα ασφαλείας είναι χαμηλής προτεραιότητας, ή υπόκεινται σε λίγες αλλαγές, όπως μουσική, μηνύματα και οικογενειακές φωτογραφίες, τότε εβδομαδιαία ή ακόμα μηνιαία αντίγραφα ασφαλείας μπορεί να αρκούν. Όμως, εάν είστε στη μέση ενός ελέγχου φόρου, πιο συχνά αντίγραφα ασφαλείας μπορεί να είναι απαραίτητα.</p>

  <p>Ως γενικός κανόνας, ο χρόνος των ενδιάμεσων αντιγράφων ασφαλείας δεν πρέπει να είναι περισσότερος από τον χρόνο που είσαστε πρόθυμοι να ξοδέψετε ξανακάνοντας οποιαδήποτε χαμένη εργασία. Για παράδειγμα, εάν μια βδομάδα επαναγραψίματος των χαμένων εγγράφων είναι υπερβολικά πολύς χρόνος για σας, θα πρέπει να δημιουργείτε αντίγραφα ασφαλείας τουλάχιστον μια φορά την βδομάδα.</p>

</page>
