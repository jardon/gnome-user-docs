<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="el">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ελέγξτε πότε οι μικρογραφίες χρησιμοποιούνται για τα αρχεία.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2009-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Φώτης Τσάμης</mal:name>
      <mal:email>ftsamis@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2009, 2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2012-2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Μαυρίδου</mal:name>
      <mal:email>mavridou@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Προτιμήσεις προεπισκόπησης του διαχειριστή αρχείων</title>

<p>The file manager creates thumbnails to preview image, video, and text
files. Thumbnail previews can be slow for large files or over networks, so
you can control when previews are made. Click the menu button in the top-right
of the window, select <gui>Preferences</gui>, then go to the
<gui>Performance</gui> section.</p>

<terms>
  <item>
    <title><gui>Show Thumbnails</gui></title>
    <p>By default, all previews are done for
    <gui>On this computer only</gui>, those on your computer or connected
    external drives. You can set this feature to <gui>All files</gui> or
    <gui>Never</gui>. The file manager can
    <link xref="nautilus-connect">browse files on other computers</link> over
    a local area network or the internet. If you often browse files over a local
    area network, and the network has high bandwidth, you may want to set the
    preview option to <gui>All files</gui>.</p>
  </item>
  <item>
    <title><gui>Count Number of Files in Folders</gui></title>
    <p>If you show file sizes in <link xref="nautilus-list">list view columns</link>
    or <link xref="nautilus-display#icon-captions">icon captions</link>,
    folders will be shown with a count of how many files and folders they
    contain. Counting items in a folder can be slow, especially for very large
    folders, or over a network.</p>
    <p>You can turn this feature on or off, or turn it on only for files on
    your computer and local external drives.</p>
  </item>
</terms>
</page>
