<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="te">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Back up anything that you cannot bear to lose if something goes
    wrong.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>ఏమి బ్యాక్అప్ చేయాలి</title>

  <p>మీ <link xref="backup-thinkabout">అతిముఖ్యమైన ఫైళ్ళు</link> అదే విధంగా తిరిగిసృష్టించుటకు క్లిష్టమైన ఫైళ్ళను బ్యాకప్ తీయుట మీ ప్రాముఖ్యతగా వుండాలి. ఉదాహరణకు, అతిముఖ్యమైన వాటినుండి అంత ముఖ్యమైనవికాని వాటివరకు:</p>

<terms>
 <item>
  <title>మీ వ్యక్తిగత ఫైళ్ళు</title>
   <p>వీటిలో పత్రములు, స్ప్రెడ్‌షీట్లు, ఈమెయిల్, క్యాలెండర్ నియామకాలు, ఆర్ధిక దత్తాంశం, ఇంట్లో ఫొటోలు, లేదా తిరిగి పొందుటకు సాధ్యంకాని ఏ ఫైళ్ళైనా ఈ కోవలోకే వస్తాయి.</p>
 </item>

 <item>
  <title>మీ వ్యక్తిగత అమరికలు</title>
   <p>మీ డెస్కుటాప్ పైన రంగులకు, బ్యాక్‌గ్రౌండ్లకు, తెర రిజొల్యూషన్ మరియు మౌస్ అమరికలకు చేసిన మార్పులు దీని కిందకే వస్తాయి. అనువర్తన అభీష్టాలు, <app>లిబ్రేఆఫీస్</app>, మీ మ్యూజిక్ ప్లేయర్, మరియు మీ ఈమెయిల్ ప్రోగ్రామ్ వంటివాటి అమరికలు కూడా దీనికిందకే వస్తాయి. ఇవి తిరిగిపొందవచ్చు, అయితే తిరిగిచేయుటకు సమయం పడుతుంది.</p>
 </item>

 <item>
  <title>వ్యవస్థ అమరికలు</title>
   <p>చాలామంది సంస్థాపనా సమయమందు సృష్టించిన వ్యవస్థ అమరికలను మార్చరు. మీరు మీ వ్యవస్థ అమరికలను మార్చివుంటే, లేదా మీరు మీ వ్యవస్థను సేవిక వలె వుపయోగిస్తుంటే, అప్పుడు మీరు ఈ అమరికలను బ్యాకప్ తీయాలని అనుకోవచ్చు.</p>
 </item>

 <item>
  <title>స్థాపించబడిన సాఫ్ట్‍వేర్</title>
   <p>ఏదైనా తీవ్రమైన కంప్యూటర్ సమస్య తరువాత మీరు వుపయోగించే సాఫ్టువేర్ ను సాధారణంగా దానిని తిరిగిసంస్థాపించుట ద్వారా తిరిగివుంచవచ్చు.</p>
 </item>
</terms>

  <p>సాధారణంగా, మీరు తిరిగిపొందుటకు సాధ్యంకాని ఫైళ్ళను మరియు బ్యాకప్ తీయకపోతే పునఃస్థాపించుటకు ఎక్కువ సమయం పట్టే ఫైళ్ళను మీరు బ్యాకప్ తీయాలనుకొంటారు. సుళువుగా పునఃస్థాపించుటకు వీలగు వాటిని బ్యాకప్ తీసి మీరు డిస్కు జాగాను వృధా చేయలనుకోరు.</p>

</page>
