<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-lefthanded" xml:lang="te">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.10" date="2013-10-29" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-21" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <credit type="author">
      <name>ఫిల్ బుల్</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Reverse the left and right mouse buttons in the mouse settings.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Use your mouse left-handed</title>

  <p>You can swap the behavior of the left and right buttons on your
  mouse or touchpad to make it more comfortable for left-handed use.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Mouse &amp; Touchpad</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Mouse &amp; Touchpad</gui> to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> section, click to switch
      <gui>Primary button</gui> to <gui>Right</gui>.</p>
    </item>
  </steps>

  <note>
    <p>This setting will affect both your mouse and touchpad, as well as any
    other pointing device.</p>
  </note>

</page>
