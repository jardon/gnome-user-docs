<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="te">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Learn how often you should backup your important files to make sure
    that they are safe.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>బ్యాకప్స్ తరచుదనం</title>

  <p>ఎంత తరచుగా మీరు బ్యాకప్స్ తీయాలి అనేది మీరు ఏ రకమైన దత్తాంశం బ్యాకప్ తీద్దామని అనుకుంటున్నారో దానిపై ఆదారపడి వుటుంది. ఉదాహరణకు, మీరు ముఖ్యమైన దత్తాంశం కలిగివున్న సేవికలు గల నెట్వర్కు వాతావరణం కలిగివుంటే, రోజుకోసారి బ్యాకప్స్ కూడా సరిపోవు.</p>

  <p>అలాగే, మీరు మీ ఇంటి కంప్యూటర్ నందలి దత్తాంశం బ్యాకప్ తీస్తుంటే అప్పుడు గంటకోసారి బ్యాకప్స్ అనవసరం. మీ బ్యాకప్ ప్రణాళికను ఎలా రూపొందించాలి అనేదానికి కింది సూచనలు సహాయకంగా వుండవచ్చు:</p>

<list style="compact">
  <item><p>మీ కంప్యూటర్ పైన గడిపే సమయం.</p></item>
  <item><p>కంప్యూటర్ పైని దత్తాంశం ఎంత తరచుగా ఎంత మేరకు మారుతుంది.</p></item>
</list>

  <p>మీరు బ్యాకప్ తీద్దామనుకొన్న దత్తాంశం తక్కువ ప్రాముఖ్యతను కలిగివుంటే, లేదా కొద్దిపాటి మార్పులు, సంగీతం, ఈ-మెయిల్స్ మరియు ఇంట్లో ఫొటోలు లాంటివి అయితే, వారంకోసారి లేదా నెలకోసారి బ్యాకప్ సరిపోతుంది. మీరు పన్ను లెక్కల మధ్యలో వుంటే, మరింత తరచుగా బ్యాకప్లు తీయుట అవసరం.</p>

  <p>సాధారణంగా, బ్యాకప్ల మధ్యని సమయం అనునది మీరు కోల్పోయిన పనిని తిరిగిచేయుటకు వెచ్చించాల్సిన సమయం కన్నా ఎక్కువ వుండరాదు. ఉదాహరణకు, పోయిన పత్రములను తిరిగి వ్రాయుటకు మీకు వారం సమయం మరీ ఎక్కువ అయితే, మీరు వారానికోసారి బ్యాకప్ తీసుకోవాలి.</p>

</page>
