<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-findip" xml:lang="te">

  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.37.3" date="2020-08-05" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>షాన్ మెక్‌కేన్స్</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>జిమ్ క్యాంబెల్</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Knowing your IP address can help you troubleshoot network problems.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>మీ ఐపీ చిరునామాను కనుక్కోండి</title>

  <p>Knowing your IP address can help you troubleshoot problems with your
  internet connection. You may be surprised to learn that you have <em>two</em>
  IP addresses: an IP address for your computer on the internal network and an
  IP address for your computer on the internet.</p>
  
  <section id="wired">
    <title>Find your wired connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Network</gui> in the sidebar to open the panel.</p>
      <note style="info">
        <p its:locNote="TRANSLATORS: See NetworkManager for 'PCI', 'USB' and 'Ethernet'">
        If more than one type of wired connected is available, you might see
        names like <gui>PCI Ethernet</gui> or <gui>USB Ethernet</gui> instead
        of <gui>Wired</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>

  </section>
  
  <section id="wireless">
    <title>Find your wireless connection’s internal (network) IP address</title>
  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Click the
      <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button next to the active connection for the IP address and other details.</p>
    </item>
  </steps>
  </section>
  
  <section id="external">
  	<title>మీ బాహ్య (జాలం) ఐపీ చిరునామాను కనుక్కోండి</title>
  <steps>
    <item>
      <p>Visit
      <link href="https://whatismyipaddress.com/">whatismyipaddress.com</link>.</p>
    </item>
    <item>
      <p>The site will display your external IP address for you.</p>
    </item>
  </steps>
  <p>Depending on how your computer connects to the internet, the internal and
  external addresses may be the same.</p>  
  </section>

</page>
