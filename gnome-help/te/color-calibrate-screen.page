<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="te">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>ఖచ్చితమైన రంగులను ప్రదర్శించుటకు మీ తెరను కాలిబ్రేట్ చేయుట ముఖ్యం.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>నేను నా తెరను ఎలా కాలిబ్రేట్ చేస్తాను?</title>

  <p>You can calibrate your screen so that it shows more accurate color. This
  is especially useful if you are involved in digital photography, design or
  artwork.</p>

  <p>You will need either a colorimeter or a spectrophotometer to do this. Both
  devices are used to profile screens, but they work in slightly different
  ways.</p>

  <steps>
    <item>
      <p>Make sure your calibration device is connected to your computer.</p>
    </item>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Select your screen.</p>
    </item>
    <item>
      <p>Press <gui style="button">Calibrate…</gui> to commence the
      calibration.</p>
    </item>
  </steps>

  <p>Screens change all the time: the backlight in a TFT display will halve in
  brightness approximately every 18 months, and will get yellower as it gets
  older. This means you should recalibrate your screen when the [!] icon
  appears in the <gui>Color</gui> panel.</p>

  <p>LED తెరలు కూడా కాలానుగతంగా మారును, అయితే TFTల కన్నా నిదానంగా మారును.</p>

</page>
