<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="te">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>మీ బ్యాకప్ సఫలవంతంగా అయిందేమో నిర్ధారించు.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>మీ బ్యాకప్ సరిచూచుకోండి</title>

  <p>After you have backed up your files, you should make sure that the
 backup was successful. If it didn’t work properly, you could lose important
 data since some files could be missing from the backup.</p>

   <p>When you use <app>Files</app> to copy or move files, the computer checks
   to make sure that all of the data transferred correctly. However, if you are
   transferring data that is very important to you, you may want to perform
   additional checks to confirm that your data has been transferred
   properly.</p>

  <p>గమ్యపు మాధ్యమంకు నకలుతీసిన ఫైళ్ళను మరియు సంచయాలను పరిశీలిస్తూ మీరు మరొక తనిఖీ నిర్వహించవచ్చు. మీరు బదిలీచేసిన ఫైళ్ళు మరియు సంచయాలు యధార్ధంగా బ్యాకప్ నందు వున్నాయి అని పరిశీలించుకొనుట ద్వారా, కార్యక్రమం విజయవంతంగా జరిగిందని మరింత విశ్వాసంతో వుండవచ్చు.</p>

  <note style="tip"><p>మీరు పెద్ద మొత్తంలో రోజువారీ బ్యాకప్‌లు జరుపుతుంటే, బ్యాకప్ కొరకై కేటాయించన ప్రోగ్రామ్‌ను వుపయోగించుట మీకు సులువుగా వుంటుంది, <app>Déjà Dup</app> వంటిది. మామూలుగా ఫైళ్ళను నకలుతీసి అతికించుట కన్నా అటువంటి కార్యక్రమం మరింత నమ్మసక్యంగా వుంటుంది.</p></note>

</page>
