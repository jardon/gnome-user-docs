<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="te">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>గ్నోమ్ పత్రీకరణ పరియోజన</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>మైకేల్ హిల్</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Check the <gui>Activities</gui> overview or other workspaces.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Praveen Illa</mal:name>
      <mal:email>mail2ipn@gmail.com</mal:email>
      <mal:years>2011, 2014. </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>కృష్ణబాబు క్రొత్తపల్లి</mal:name>
      <mal:email>kkrothap@redhat.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Find a lost window</title>

  <p>A window on a different workspace, or hidden behind another window, is
  easily found using the <gui xref="shell-introduction#activities">Activities</gui>
  overview:</p>

  <list>
    <item>
      <p>Open the <gui>Activities</gui> overview. If the missing window is on
      the current
      <link xref="shell-windows#working-with-workspaces">workspace</link>, it
      will be shown here in thumbnail. Simply click the thumbnail to redisplay
      the window, or</p>
    </item>
    <item>
      <p>Click different workspaces in the
      <link xref="shell-workspaces">workspace selector</link>
      to try to find your window, or</p>
    </item>
    <item>
      <p>Right-click the application in the dash and its open windows will be
      listed. Click the window in the list to switch to it.</p>
    </item>
  </list>

  <p>Using the window switcher:</p>

  <list>
    <item>
      <p>Press
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      to display the <link xref="shell-windows-switching">window switcher</link>.
      Continue to hold down the <key>Super</key> key and press <key>Tab</key>
      to cycle through the open windows, or
      <keyseq><key>Shift</key><key>Tab</key> </keyseq> to cycle backwards.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>If an application has multiple open windows, hold down
      <key>Super</key> and press <key>`</key> (or the key above <key>Tab</key>)
      to step through them.</p>
    </item>
  </list>

</page>
