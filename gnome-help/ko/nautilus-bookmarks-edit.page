<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="ko">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>파일 관리자에서 책갈피를 추가, 삭제, 아름 바꾸기를 진행합니다.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>책갈피 폴더 편집</title>

  <p>책갈피는 파일 관리자의 가장자리 창에 나타납니다.</p>

  <steps>
    <title>책갈피 추가:</title>
    <item>
      <p>책갈피를 추가할 폴더(또는 위치)를 엽니다.</p>
    </item>
    <item>
      <p>경로 표시줄의 현재 폴터를 누른 후 <gui style="menuitem">책갈피 추가</gui>를 선택합니다.</p>
      <note>
        <p>가장자리 창으로 폴더를 끌어다 동적으로 나타나는 <gui>새 책갈피</gui>로 놓을 수 있습니다.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>책갈피 삭제:</title>
    <item>
      <p>가장자리 창에서 책갈피에 커서 포인터를 가져다 둔 후 오른쪽 단추를 누르고 메뉴에서 <gui>제거</gui>를 선택합니다.</p>
    </item>
  </steps>

  <steps>
    <title>책갈피 이름 바꾸기:</title>
    <item>
      <p>가장자치 창에서 책갈피에 커서 포인터를 가져다 둔 후 오른쪽 단추를 누르고 <gui>이름 바꾸기…</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><gui>이름</gui> 텍스트 상자에서, 책갈피 새 이름을 입력합니다.</p>
      <note>
        <p>책갈피 이름을 바꾼다고 해서 폴더의 이름이 바뀌진 않습니다. 다른 폴더에 대한 책갈피가 서로 다른 위치에 있고 동일한 이름을 가지고 있다면, 단지 책갈피가 동일한 이름을 가질 뿐이며, 각자가 동일한 폴더를 부를 수는 없습니다. 이 경우 책갈피 이름 보단, 책갈피가 가리키는 폴더 이름을 활용하는게 좋습니다.</p>
      </note>
    </item>
  </steps>

</page>
