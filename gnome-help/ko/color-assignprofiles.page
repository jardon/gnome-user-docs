<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="ko">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>화면의 색 프로파일을 추가하려면 <guiseq><gui>설정</gui><gui>색</gui></guiseq>에서 찾아봅니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>어떻게 장치에 프로파일을 할당하나요?</title>

  <p>화면과 프린터의 색 프로파일을 할당하여 각 장치에서 색을 더 정확하게 출력할 수 있도록 하고 싶을 때가 있습니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>색</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>프로파일을 추가할 장치를 선택합니다.</p>
    </item>
    <item>
      <p><gui>프로파일 추가</gui>를 눌러 기존 프로파일을 선택하거나 새 프로파일을 가져옵니다.</p>
    </item>
    <item>
      <p>선택을 결정했다면 <gui>추가</gui>를 누릅니다.</p>
    </item>
    <item>
      <p>사용하는 프로파일을 바꾸려면 원하는 프로파일을 선택하고 <gui>사용</gui>을 눌러 선택을 결정합니다.</p>
    </item>
  </steps>

  <p>각 장치에서 여러 프로파일을 할당할 수 있지만, 그 중 하나의 프로파일만 <em>기본</em>으로 지정할 수 있습니다. 기본 프로파일은 프로파일을 허용할 때 추가 정보 없이 자동으로 선택할 때 활용합니다. 이 자동 선택을 예로 들자면 광택 용지와 기타 일반 용지용 프로파일을 만들었을 때에 해당할 수 있습니다.</p>

  <p>보정 하드웨어를 연결한 후, <gui>보정…</gui> 단추를 누르면 새 프로파일을 만들어줍니다.</p>

</page>
