<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="ko">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>각 연락처의 정보를 편집합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>연락처 세부 정보 편집</title>

  <p>연락처 세부 정보 편집 기능은 주소록의 정보를 완전히 최신의 상태로 유지할 수 있게 해줍니다.</p>

  <steps>
    <item>
      <p>연락처 목록에서 연락처를 선택합니다.</p>
    </item>
    <item>
      <p>창 우측 상단의 <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">더 보기</span></media> 단추를 누른 후 <gui style="menuitem">편집</gui>을 선택합니다.</p>
    </item>
    <item>
      <p>연락처 세부 정보를 편집합니다.</p>
      <p>새 전화번호 또는 전자메일 주소 같은 <em>세부 정보</em>를 추가하려면, 추가하려는 다음 빈 입력창 형식(전화번호, 전자메일 등)에 세부 내용을 채워나가면 됩니다.</p>
      <note style="tip">
        <p>하단의 <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">더 보기</span></media> 옵션으 눌러 가용 옵션을 확장하여 <gui>웹사이트</gui> 와 <gui>생년월일</gui> 같은 입력 창을 나타냅니다.</p>
      </note>
    </item>
    <item>
      <p>연락처 편집이 끝나면 <gui style="button">완료</gui>를 누릅니다.</p>
    </item>
  </steps>

</page>
