<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-switching" xml:lang="ko">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.12" date="2014-03-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><keyseq><key>Super</key><key>Tab</key></keyseq> 키를 누릅니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>창 전환</title>

  <p><em>창 전환 상자</em>에서 실행 중인 모든 그래픽 사용자 인터페이스 프로그램을 볼 수 있습니다. 여기서는 단일 단계 프로세스 작업을 전환하며 어떤 프로그램을 실행하고 있는지 전체 그림을 보여줍니다.</p>

  <p>작업 공간에서:</p>

  <steps>
    <item>
      <p><keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> 키를 눌러 <gui>창 전환 상자</gui>를 띄웁니다.</p>
    </item>
    <item>
      <p><key xref="keyboard-key-super">Super</key> 키를 데어 전환 상자의 다음(강조) 창을 선택합니다.</p>
    </item>
    <item>
      <p>아니면 <key xref="keyboard-key-super">Super</key> 키를 누른 채로 <key>Tab</key>키를 눌러 열린 창 목록을 전환하거나, <keyseq><key>Shift</key><key>Tab</key></keyseq> 키를 눌러 반대 방향으로 전환합니다.</p>
    </item>
  </steps>

  <p if:test="platform:gnome-classic">열려있는 모든 창을 전환할 때 하단 표시줄의 창 목록을 활용할 수 있습니다.</p>

  <note style="tip" if:test="!platform:gnome-classic">
    <p>창 전환 상자의 창은 프로그램 별로 모입니다. 여러 창에 대한 프로그램 미리 보기는 해당 창을 누르면 창 그림의 아래 방향으로 뜹니다. <key xref="keyboard-key-super">Super</key> 키를 누른 채로 <key>`</key> 키 (또는 <key>Tab</key> 키 위의 키)를 눌러 프로그램안으로 한 단계 들어갑니다.</p>
  </note>

  <p>창 전환 상자에서 <key>→</key> 키 또는 <key>←</key> 키로 프로그램 아이콘 사이를 움직이거나 마우스로 아이콘을 눌러 선택할 수 있습니다.</p>

  <p>단일 창으로 이루어진 프로그램 미리 보기 그림은 <key>↓</key> 키로 나타낼 수 있습니다.</p>

  <p><gui>현재 활동</gui> 개요에서, 창으로 전환하려면 <link xref="shell-windows">창</link>을 눌러 개요 화면을 빠져나갑니다. 여러 <link xref="shell-windows#working-with-workspaces">작업 공간</link>을 열어두었다면, 각 작업 공간을 눌러 작업 공간에 열린 창을 볼 수 있습니다.</p>

</page>
