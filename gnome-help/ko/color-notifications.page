<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-notifications" xml:lang="ko">

  <info>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>색 프로파일이 오래되었거나 정확하지 않으면 알림을 받을 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>색 프로파일이 정확하지 않을 때 알림을 받을 수 있을까요?</title>

  <p>정해진 시간이 지난 후 장치 재보정을 상기할 수 있습니다. 불행하게도 장치 프로파일이 정확한지 여부를 재보정 없이는 알 수 없기 때문에 장치를 주기적으로 재보정하는게 가장 좋습니다.</p>

  <p>일부 업체에서는 색 프로파일의 부정확성이 최종 제품에 엄청난 차이를 보일 수 있기 때문에 프로파일 보정 기한 정책을 둡니다.</p>

  <p>유효기한 정책을 수립하고 프로파일이 정책상 지정한 기간보다 오래됐을 경우 프로파일 옆 <gui>색</gui> 창에 붉은 경고 삼각형 표시가 나타납니다. 컴퓨터에 매번 로그인할 때도 경고 알림이 계속 나타납니다.</p>

  <p>디스플레이 및 프린터 장치의 정책을 설정하려면, 프로파일의 최대 연한을 일자 단위로 지정합니다:</p>

<screen its:translate="no">
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">$ </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 90</input>
</screen>

</page>
