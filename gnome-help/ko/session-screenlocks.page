<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="ko">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>화면 잠금</gui> 설정에서 화면을 잠그기 전 대기 시간을 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>화면이 너무 빨리 잠깁니다</title>

  <p>몇분 동안 컴퓨터 앞에서 자리를 비우면, 화면을 자체적으로 잠근 후 다시 시작할 때 암호를 입력하도록 합니다. 보안상 이유로 동작하긴 하지만(그래서 원하지 않는 상황에 자리를 비울 때 그 어느 누구도 당황하게 할 수 없습니다), 화면이 너무 빨리 잠기면 좀 짜증날 수 있습니다.</p>

  <p>화면을 자동으로 잠기게 하기 전 더 긴 대기 시간동안 기다리게 하려면:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Screen Lock</gui>.</p>
    </item>

    <item>
      <p>Click on <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p><gui>자동 화면 잠금</gui>을 켰다면 <gui>자동 화면 잠금 지연</gui> 드롭다운 목록의 값을 바꿀 수 있습니다.</p>
    </item>
  </steps>

  <note style="tip">
    <p>화면을 더이상 자동으로 잠그기 싫다면 <gui>자동 화면 잠금</gui> 스위치를 끕니다.</p>
  </note>

</page>
