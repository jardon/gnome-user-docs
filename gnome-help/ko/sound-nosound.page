<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="ko">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>소리를 껐는지, 케이블을 적절하게 연결했는지, 소리 카드를 찾았는지 확인합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>컴퓨터의 그 어떤 소리도 들을 수 없어요</title>

  <p>컴퓨터에서 어떤 소리도 들을 수 없다면, 음악을 재생할 경우 다음 문제 해결 방법을 따릅니다.</p>

<section id="mute">
  <title>음소거했는지 확인하기</title>

  <p><gui xref="shell-introduction#systemmenu">시스템 메뉴</gui>를 열어 소리를 껐거나 음량을 줄였는지 확인합니다.</p>

  <p>일부 랩톱은 음소거 스위치 또는 키가 키보드에 있습니다. 해당 키를 눌러 음소거 상태가 꺼지는지 확인합니다.</p>

  <p>소리를 재생할 때(음악 재생 프로그램, 영화 재생 프로글매) 사용하는 프로그램에서 음소거 상태인지를 확인해보아야 합니다. 프로그램에 음소거 단추가 있거나 음량 단추가 메인 창에 있을테니 확인해봅니다.</p>

  <p>또한 <gui>소리</gui> 창에서 음량 슬라이터를 확인할 수 있습니다:</p>
  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>소리</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>소리</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>음량 수준</gui>에서 프로그램의 소리를 껐는지 확인합니다. 음량 슬라이더 끝의 단추로 <gui>음소거</gui> 켬/끔 상태를 바꿉니다.</p>
    </item>
  </steps>

</section>

<section id="speakers">
  <title>스피커를 켜고 제대로 연결했는지 확인하기</title>
  <p>컴퓨터에 외장 스피커가 있다면, 스피커를 켰고 음량을 올렸는지 확인합니다. 스피커 케이블을 컴퓨터의 “출력” 음향 소켓에 제대로 연결했는지 확인합니다. 이 소켓은 보통 밝은 녹색입니다.</p>

  <p>일부 사운드 카드는 출력(스피커)용 소켓과 입력용 소켓(마이크로폰)을 전환할 수 있습니다. 출력 소켓은 리눅스, 윈도우, 맥OS를 실행할 때 다를 수 있습니다. 스피커 케이블을 컴퓨터의 서로 다른 음향 소켓에 연결해봅니다.</p>

 <p>확인 과정의 마지막은, 스피커 후면 부에 음향 케이블을 제대로 연결했는 지 여부 확인입니다. 일부 스피커는 하나 이상의 입력 소켓이 있습니다.</p>
</section>

<section id="device">
  <title>올바른 사운드 장치를 선택했는지 확인하기</title>

  <p>일부 컴퓨터에는 여러 “사운드 장치”를 설치하기도 합니다. 일부 사운드 장치는 소리를 출력하는 기능이 있으며 다른 경우는 그렇지 않기에 올바른 사운드 장치를 선택했는지 확인해야합니다. 이런 구조는 올바른 장치 선택의 시행 착오를 겪게 하기도 합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>소리</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>소리</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>출력</gui>에서 <gui>출력 장치</gui>를 선택한 후 <gui>시험</gui>을 눌러 동작하는지 확인합니다.</p>

      <p>가용 장치 마다 시험해봐야 할 수도 있습니다.</p>
    </item>
  </steps>

</section>

<section id="hardware-detected">

 <title>사운드 카드를 제대로 발견했는지 확인</title>

  <p>아마도 사운드 카드용 드라이버를 제대로 설치하지 않았다면 사운드 카드를 제대로 찾지 못할 수도 있습니다. 이럴때, 사운드 카드 드라이버를 직접 설치해야합니다. 이 과정은 카드 형식에 따라 다릅니다.</p>

  <p>터미널에서 <cmd>lspci</cmd> 명령을 실행하여 어떤 사운드 카드를 설치했는지 찾아봅니다:</p>
  <steps>
    <item>
      <p><gui>현재 활동</gui> 개요로 이동한 후 터미널 창을 엽니다.</p>
    </item>
    <item>
      <p><link xref="user-admin-explain">관리자 권한</link>으로 <cmd>lspci</cmd> 명령을 실행합니다. <cmd>sudo lspci</cmd> 명령을 입력한 후 계정 암호를 입력하거나, <cmd>su</cmd> 명령을 입력하고 <em>root</em> (관리자) 암호를 입력한 다음 <cmd>lspci</cmd> 명령을 입력합니다.</p>
    </item>
    <item>
      <p><em>audio controller</em> 또는 <em>audio device</em>가 목록에 나타나는지 확인합니다. 이 경우 여러 사운드 카드의 제조사와 모델을 볼 수 있습니다. 또한 <cmd>lspci -v</cmd> 명령으로 더 자세한 정보를 볼 수 있습니다.</p>
    </item>
  </steps>

  <p>카드용 드라이버를 찾아 설치할 수 있습니다. 리눅스 배포판 지원 포럼(아니면 다른 곳)으로의 방법 문의가 가장 좋은 방법입니다.</p>

  <p>사운드 카드 드라이버를 확보할 수 없다면 새 사운드 카드를 구매해야 할 지도 모릅니다. 컴퓨터 내부에 장착하거나 외부에 연결하는 USB 사운드 카드를 확보할 수 있습니다.</p>

</section>

</page>
