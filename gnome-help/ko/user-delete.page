<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="ko">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>더이상 컴퓨터를 사용하지 않는 사용자를 제거합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>사용자 계정 삭제</title>

  <p><link xref="user-add">컴퓨터에 여러 사용자를 추가</link>할 수 있습니다. 누군가가 컴퓨터를 더이상 사용하지 않는다면 해당 사용자 계정을 삭제할 수 있습니다.</p>

  <p>사용자 계정을 삭제하려면 <link xref="user-admin-explain">관리자 권한</link>이 필요합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>사용자</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>사용자</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>우측 상단 구석의 <gui style="button">잠금 해제</gui>를 누르고 암호를 물어보면 입력합니다.</p>
    </item>
    <item>
      <p><gui>기타 사용자</gui>에서 삭제하려는 사용자 계정을 누릅니다.</p>
    </item>
    <item>
      <p>사용자 계정을 제거하려면 <gui style="button">사용자 제거...</gui> 단추를 누릅니다.</p>
    </item>
    <item>
      <p>각 사용자에게는 파일과 설정을 지닌 각자의 폴더가 있습니다. 사용자 내 폴더를 유지하거나 삭제할 수 있습니다. 삭제하려는 사용자가 더이상 사용하지 않는다는게 분명하고 디스크 공간 확보가 필요하다면 <gui>파일 삭제</gui>를 누릅니다. 그러면 이 파일을 완전히 삭제하며 복원할 수 없습니다. 삭제하기 전에 외장 저장 장치에 파일을 백업할 수도 있습니다.</p>
    </item>
  </steps>

</page>
