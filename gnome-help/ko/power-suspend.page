<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-suspend" xml:lang="ko">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>대기 모드는 컴퓨터에게 적은 전력을 소모하도록 잠자기 상태로 둡니다.</desc>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>제 컴퓨터를 대기 모드로 설정하면 무슨 일이 일어나나요?</title>

<p>컴퓨터를 <em>대기</em> 상태로 둘 때 잠자기 신호를 보냅니다. 모든 프로그램과 문서는 열린 상태로 두지만, 화면과 다른 부분은 절전을 목적으로 끕니다. 컴퓨터는 여전히 켜저 있으며, 적은 전력을 소모합니다. 키를 누르거나 마우스 단추를 눌러 깨울 수 있습니다. 동작하지 않으면, 전원 단추를 눌러봅니다.</p>

<p>어떤 컴퓨터는 하드웨어 지원 문제가 있는데 <link xref="power-suspendfail">대기 모드에 제대로 진입하지 못합니다</link>. 대기 모드 기능에 의존하기 전에 컴퓨터가 대기 모드로 들어가는지 시험을 진행하시는게 좋습니다.</p>

<note style="important">
  <title>대기 모드 진입 전 항상 모든 작업을 저장</title>
  <p>컴퓨터를 대기모드로 바꾸기 전 모든 작업 내용을 저장해야 합니다. 이 경우 뭔가 잘못될 수 있으며 컴퓨터를 원상 복귀할 때 열린 프로그램과 문서를 복원할 수 없을 수도 있습니다.</p>
</note>

</page>
