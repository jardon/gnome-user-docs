<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="ko">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="41.0" date="2021-10-15" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>목록 보기에서 어떤 열 정보를 보여줄 지 제어합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>파일 목록 항목 기본 설정</title>

  <p><gui>파일</gui> 목록 보기에서 몇가지 내용 정보를 표시할 수 있습니다. 열 제목 표시줄에서 오른쪽 단추를 누른 후 나타낼 열을 선택하거나 선택 취소합니다.</p>

  <terms>
    <item>
      <title><gui>이름</gui></title>
      <p>폴더 및 파일의 이름입니다.</p>
      <note style="tip">
        <p><gui>이름</gui> 열은 숨길 수 없습니다.</p>
      </note>
    </item>
    <item>
      <title><gui>크기</gui></title>
      <p>폴더의 크기는 폴더에 들어있는 항목 갯수로 주어집니다. 파일 크기는 바이트, KB, MB 단위로 주어집니다.</p>
    </item>
    <item>
      <title><gui>형식</gui></title>
      <p>폴더로 나타내거나 PDF 문서, JPEG 그림, MP3 오디오 등의 파일 형식으로 나타냅니다.</p>
    </item>
    <item>
      <title><gui>수정 일시</gui></title>
      <p>파일을 수정한 최종 일시의 날짜를 보여줍니다.</p>
    </item>
    <item>
      <title><gui>소유자</gui></title>
      <p>폴더 또는 파일을 소유한 사용자 이름입니다.</p>
    </item>
    <item>
      <title><gui>그룹</gui></title>
      <p>파일 소유 그룹입니다. 각 사용자는 그룹에 들어있지만, 하나의 글부에 여러 사용자가 들어갈 수 있습니다. 예를 들어, 부서는 작업 환경의 그룹을 소유할 수 있습니다.</p>
    </item>
    <item>
      <title><gui>권한</gui></title>
      <p>파일 접근 권한을 나타냅니다. 예를 들면, <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>첫 문자는 파일 형식입니다. <gui>-</gui>는 일반 파일, <gui>d</gui>는 디렉터리(폴더)를 나타냅니다. 드물게, 다른 문자가 나타나기도 합니다.</p>
        </item>
        <item>
          <p>그 다음 <gui>rwx</gui> 문자 세개는 파일을 보유한 사용자의 권한을 지정합니다.</p>
        </item>
        <item>
          <p>그 다음 <gui>rw-</gui> 문자 세개는 파일을 보유한 그룹의 모든 사용자 권한을 지정합니다.</p>
        </item>
        <item>
          <p>그 다음 <gui>r--</gui> 문자 세개는 시스템의 다른 사용자 권한을 지정합니다.</p>
        </item>
      </list>
      <p>각 권한에는 다음 의미가 있습니다:</p>
      <list>
        <item>
          <p><gui>r</gui>: 읽을 수 있음, 파일 또는 폴더를 열 수 있음</p>
        </item>
        <item>
          <p><gui>w</gui>: 쓸 수 있음, 파일에 바뀐 내용을 저장할 수 있음</p>
        </item>
        <item>
          <p><gui>x</gui>: 실행할 수 있음, 파일에 프로그램 또는 스크립트가 있을 때 실행할 수 있으며, 폴터의 경우 하위 폴더 및 파일에 접근할 수 있음</p>
        </item>
        <item>
          <p><gui>-</gui>: 권한을 설정하지 않음</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>MIME 형식</gui></title>
      <p>항목의 MIME 형식을 표시합니다.</p>
    </item>
    <item>
      <title><gui>위치</gui></title>
      <p>파일 위치 경로입니다.</p>
    </item>
    <item>
      <title><gui>수정일자 — 시각</gui></title>
      <p>파일을 수정한 최종 일시의 날짜 및 시간을 제시합니다.</p>
    </item>
    <item>
      <title><gui>접근 일시</gui></title>
      <p>파일을 수정한 최종 일시의 날짜 또는 시간을 제시합니다.</p>
    </item>
  </terms>

</page>
