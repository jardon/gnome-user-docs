<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="ko">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>자동으로 화면 밝기를 조절하여 배터리의 소모량을 줄입니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>자동 밝기 조절 켜기</title>

  <p>컴퓨터에 통합 광 센서가 있다면 자동으로 화면 밝기를 조절할 때 사용할 수 있습니다. 제각기 다른 주변 조명 상태에 따라 언제든 쉽게 화면을 볼 수 있게 해주며, 배터리 전력 소모 감소에 도움을 줍니다.</p>

  <steps>

    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>전원</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>전원</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>절전 옵션</gui> 섹션에서 <gui>자동 화면 밝기</gui> 스위치를 켰는지 확인합니다.</p>
    </item>

  </steps>

  <p>자동 화면 밝기를 끄려면 스위치를 끕니다.</p>

</page>
