<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-open" xml:lang="ko">

  <info>
    <link type="guide" xref="shell-overview"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>현재 활동</gui> 개요에서 프로그램을 실행합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>프로그램 시작</title>

  <p if:test="!platform:gnome-classic">화면의 좌측 상단 구석에 있는 <gui>현재 활동</gui> 으로 마우스 포인터를 이동하여 <gui xref="shell-introduction#activities">현재 활동</gui> 개요를 표시합니다. 여기가 모든 프로그램을 찾아볼 수 있는 곳입니다. <key xref="keyboard-key-super">Super</key> 키를 눌러 개요를 열어볼 수 있습니다.</p>
  
  <p if:test="platform:gnome-classic">화면 좌측 상단의 <gui xref="shell-introduction#activities">프로그램</gui> 메뉴에서 프로그램을 시작하거나 <key xref="keyboard-key-super">Super</key> 키를 눌러 <gui>현재 활동</gui> 개요를 활용할 수 있습니다.</p>

  <p><gui>현재 활동</gui> 개요에 있을 때 프로그램을 여는 방법은 여러가지가 있습니다:</p>

  <list>
    <item>
      <p>프로그램 이름 입력을 시작하면 검색을 바로 시작합니다. 검색 동작이 일어나지 않으면 화면 상단에서 검색 표시줄을 누른 후 입력을 시작합니다. 프로그램의 정확한 이름을 모른다면, 관련 용어를 입력해봅니다. 프로그램 아이콘을 눌러 프로그램 실행을 시작합니다.</p>
    </item>
    <item>
      <p><gui>현재 활동</gui> 개요 하단의 수평 아이콘 모음인 <em>대시</em>에 일부 프로그램 아이콘이 있습니다. 이 아이콘 중 하나를 누르면 관련 프로그램을 시작합니다.</p>
      <p>매우 자주 사용하는 프로그램이 있다면, <link xref="shell-apps-favorites">대시에 자주 사용하는 프로그램을 직접 추가</link>할 수 있습니다.</p>
    </item>
    <item>
      <p>대시의 격자 단추(9점)를 누릅니다. 설치한 모든 프로그램이 첫 페이지에 나타납니다. 더 많은 프로그램을 보려면, 대시에 여러점이 찍힌 단추를 눌러 다른 프로그램을 봅니다. 프로그램을 누르면 실행을 시작합니다.</p>
    </item>
    <item>
      <p>대시에 아이콘을 끌어다 작업 공간 중 한군데에 넣어 개별 <link xref="shell-workspaces">작업 공간</link> 에서 프로그램을 실행할 수 있습니다. 프로그램은 선택한 작업 공간에서 열립니다.</p>
      <p>빈 작업 공간 또는 두 작업 공간 사이 좁은 틈새에 아이콘을 끌어다 놓아 <em>새</em> 작업 공간에서 프로그램을 실행할 수 있습니다.</p>
    </item>
  </list>

  <note style="tip">
    <title>빠른 명령 실행</title>
    <p>프로그램을 실행하는 다른 방법으로는 <keyseq><key>Alt</key><key>F2</key></keyseq> 키를 누르고 <em>명령 이름</em>을 입력한 다음 <key>Enter</key> 키를 누릅니다.</p>
    <p>예를 들어 <app>리듬박스</app>를 시작하려면, <keyseq><key>Alt</key><key>F2</key></keyseq>키를 누르고 (작은 따옴표 빼고) '<cmd>rhythmbox</cmd>'를 입력합니다. 여기서는 앱 이름이 프로그램 실행 명령입니다.</p>
    <p>화살표 키를 활용하여 이전에 실행한 명령을 빠르게 접근합니다.</p>
  </note>

</page>
