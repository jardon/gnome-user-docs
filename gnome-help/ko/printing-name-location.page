<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-name-location" xml:lang="ko">

  <info>
    <link type="guide" xref="printing#setup"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision version="gnome:40" date="2021-03-05" status="final"/>

    <credit type="author copyright">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>프린터 설정에서 이름 또는 위치 정보를 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>
  
  <title>프린터 이름 또는 위치 바꾸기</title>

  <p>프린터 설정에서 프린터의 이름 또는 위치 정보를 바꿀 수 있습니다.</p>

  <note>
    <p>프린터의 이름 또는 위치 정보를 바꾸려면, 시스템의 <link xref="user-admin-explain">관리자 권한</link>이 필요합니다.</p>
  </note>

  <section id="printer-name-change">
    <title>프린터 이름 바꾸기</title>

  <p>프린터 이름을 바꾸려면 다음 단계를 따릅니다:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>프린터</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>프린터</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>시스템에 따라 우측 상단 구석의 <gui style="button">잠금 해제</gui> 단추를 누른 후 암호를 물어보면 암호를 입력해야합니다.</p>
    </item>
    <item>
      <p>프린터 옆 <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">설정</span></media> 단추를 누릅니다.</p>
    </item>
    <item>
      <p><gui style="menuitem">프린터 자세히 보기</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><gui>이름</gui> 필드에 프린터의 새 이름을 입력합니다.</p>
    </item>
    <item>
      <p>대화상자를 닫습니다.</p>
    </item>
  </steps>

  </section>

  <section id="printer-location-change">
    <title>프린터 위치 바꾸기</title>

  <p>프린터 위치를 바꾸려면:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>프린터</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>프린터</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>시스템에 따라 우측 상단 구석의 <gui style="button">잠금 해제</gui> 단추를 누른 후 암호를 물어보면 암호를 입력해야합니다.</p>
    </item>
    <item>
      <p>프린터 옆 <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">설정</span></media> 단추를 누릅니다.</p>
    </item>
    <item>
      <p><gui style="menuitem">프린터 자세히 보기</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><gui>위치</gui> 입력칸에 프린터 새 위치를 입력합니다.</p>
    </item>
    <item>
      <p>대화상자를 닫습니다.</p>
    </item>
  </steps>

  </section>

</page>
