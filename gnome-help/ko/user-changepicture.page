<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepicture" xml:lang="ko">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.32.0" date="2019-03-16" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>그놈 문서 프로젝트</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>로그인 및 사용자 화면에 사진을 추가합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>로그인 화면 사진 바꾸기</title>

  <p>로그인을 하거나 사용자 계정을 바꿀 때 로그인 사진이 붙은 사용자 목록을 볼 수 있습니다. 그림 모음의 사진 또는 보유한 그림으로 사진을 바꿀 수 있습니다. 또한 웹캠으로 새 로그인 사진을 찍을 수 있습니다.</p>

  <p>여러분의 계정이 아닌 사용자 계정을 편집하려면 <link xref="user-admin-explain">관리자 권한</link>이 필요합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>사용자</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>사용자</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>사용자 자신을 제외한 다른 사용자를 편집하려면, 우측 상단 구석의 <gui style="button">잠금 해제</gui>를 누른 후 암호를 물어볼 때 암호를 입력합니다. 그 다음 <gui>기타 사용자</gui>에서 편집할 다른 사용자를 선택합니다.</p>
    </item>
    <item>
      <p>이름 옆 연필 아이콘을 누릅니다. 몇가지 로그인 사진이 들어있는 드롭-다운 그림 모음이 나타납니다. 이 중 하나가 맘에 든다면, 해당 항목을 눌러 활용합니다.</p>
      <list>
        <item>
          <p>컴퓨터에 있는 다른 사진을 활용하려 한다면, <gui>파일 선택…</gui>을 누릅니다.</p>
        </item>
        <item>
          <p>웹캠이 있다면, <gui>사진 찍기…</gui>를 눌러 바로 새 로그인 사진을 찍을 수 있습니다. 불필요한 부분을 잘라내고자 하는 사각 테두리 범위 내로 움직이고 크기를 조절합니다. 찍은 사진이 맘에 들지 않을 경우 <gui style="button">다른 사진 찍기</gui>를 눌러 다시 찍거나 <gui>취소</gui>를 눌러 포기할 수 있습니다.</p>
        </item>
      </list>
    </item>
  </steps>

</page>
