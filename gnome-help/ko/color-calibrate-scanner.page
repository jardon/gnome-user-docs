<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-scanner" xml:lang="ko">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>스캐너 보정은 정확한 색 감지에 중요합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>스캐너는 어떻게 보정할까요?</title>

  <p>정확한 색으로 스캔을 수행하려면, 보정해야 합니다.</p>

  <steps>
    <item>
      <p>스캐너를 케이블 또는 네트워크로 연결했는지 확인합니다.</p>
    </item>
    <item>
      <p>보정 대상을 스캔하고 비압축 TIFF 파일로 저장합니다.</p>
    </item>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>색</gui>을 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>스캐너를 선택합니다.</p>
    </item>
    <item>
      <p><gui style="button">보정…</gui>을 눌러 보정을 시작합니다.</p>
    </item>
  </steps>

  <note style="tip">
    <p>스캐너 장비는 시간이 갈수록 온도점에 도달할 수록 점차적으로 안정 상태에 도달하기 때문에 보통 재보정할 필요는 없습니다.</p>
  </note>

</page>
