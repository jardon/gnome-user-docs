<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="bluetooth-problem-connecting" xml:lang="ko">

  <info>
    <link type="guide" xref="bluetooth#problems"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>어댑터는 꺼둘 수 있고, 드라이버가 없을 수도 있는데, 블루투스 자체 기능을 사용할 수 없거나 막혀있을 수도 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>블루투스 장치에 연결할 수 없어요</title>

  <p>전화 또는 헤드셋과 같은 블루투스 장치에 연결할 수 없는 이유가 몇가지 있습니다.</p>

  <terms>
    <item>
      <title>차단했거나 신뢰할 수 없는 장치 연결</title>
      <p>일부 블루투스 장치는 기본적으로 연결을 차단하거나 연결하도록 설정을 바꿔야 하는 경우가 있습니다. 연결을 허용하도록 설정했는지 확인합니다.</p>
    </item>
    <item>
      <title>인식하지 않은 블루투스 하드웨어</title>
      <p>블루투스 어댑터 또는 동글을 컴퓨터에서 인식하지 않을 수도 있습니다. 이 경우 어댑터용 <link xref="hardware-driver">드라이버</link>를 설치하지 않았기 때문입니다. 일부 블루투스 어댑터는 리눅스에서 지원하지 않기 때문에 적절한 드라이버를 취할 수 없습니다. 이 경우 다른 블루투스 어댑터를 취해야 합니다.</p>
    </item>
    <item>
      <title>어댑터 스위치가 꺼짐</title>
        <p>블루투스 어댑터 스위치를 켰는지 확인합니다. 블루투스 창을 열고 <link xref="bluetooth-turn-on-off">꺼짐</link> 상태가 아닌지 확인합니다.</p>
    </item>
    <item>
      <title>장치 블루투스 연결 스위치가 꺼짐</title>
      <p>연결하려는 장치의 블루투스를 켰는지, <link xref="bluetooth-visibility">발견할 수 있거나 살펴볼 수 있는지</link> 확인합니다. 예를 들어 폰에 연결한다면, 비행기 모드 상태가 아닌지 확인합니다.</p>
    </item>
    <item>
      <title>컴퓨터에 블루투스 어댑터가 없는 경우</title>
      <p>대부분의 컴퓨터에는 블루투스 어댑터가 없습니다. 블루투스를 사용하려면 어댑터를 구매할 수 있습니다.</p>
    </item>
  </terms>

</page>
