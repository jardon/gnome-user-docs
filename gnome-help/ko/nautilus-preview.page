<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-preview" xml:lang="ko">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-preview"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>
    <revision pkgversion="40.2" date="2021-08-25" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>파일에서 미리 보기 그림을 사용할 때를 제어합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>파일 관리자 미리 보기 기본 설정</title>

<p>파일 관리자에서는 그림, 영상, 텍스트 파일을 미리 보여주는 미리 보기 그림을 만듭니다. 미리 보기 그림은 큰 파일 또는 네트워크 파일에 대해 느리게 동작할 수 잇으니 언제 미리보기 그림을 만들지 제어할 수 있습니다. 창 우측 상단의 메뉴 단추를 누른 후 <gui>기본 설정</gui>을 선택한 다음 <gui>성능</gui> 섹션에 들어갑니다.</p>

<terms>
  <item>
    <title><gui>미리 보기 그림 표시</gui></title>
    <p>기본적으로 모든 미리 보기 화면은 여러분의 컴퓨터 또는 연결한 외장 드라이브에 대해 <gui>이 컴퓨터에서만</gui> 가능합니다. 이 기능을 <gui>모든 파일</gui> 또는 <gui>안함</gui>으로 설정할 수 있습니다. 파일 관리자에서는 로컬 영역 네트워크 또는 인터넷으로 연결한 <link xref="nautilus-connect">다른 컴퓨터의 파일 탐색</link>을 할 수 있습니다. 로컬 영역 네트워크의 파일을 자주 탐색하며 네트워크 대역폭이 크다면, 미리 보기 옵션을 <gui>모든 파일</gui>로 설정할 수도 있습니다.</p>
  </item>
  <item>
    <title><gui>폴더의 파일 개수</gui></title>
    <p><link xref="nautilus-list">목록 보기 열</link> 또는 <link xref="nautilus-display#icon-captions">아이콘 설명</link>에 파일 크기를 나타낸다면, 폴더에서는 들어있는 파일 및 폴더 갯수를 나타냅니다. 폴더가 매우 크거나, 네트워크에 연결한 폴더일 경우 폴더의 항목 계수가 느릴 수 있습니다.</p>
    <p>이 기능을 켜고 끄거나 컴퓨터와 로컬 외장 드라이브의 파일에 대해서만 켤 수 있습니다.</p>
  </item>
</terms>
</page>
