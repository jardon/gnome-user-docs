<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-delete" xml:lang="ko">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-recover"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.16" date="2015-02-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>더이상 필요하지 않은 파일 또는 폴더를 제거합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>파일 및 폴더 삭제</title>

  <p>더이상 필요하지 않은 파일 또는 폴더가 있다면 삭제할 수 있습니다. 항목을 삭제하면 해당 항목은 휴지통을 비우기 전에 항목을 계속 두는<gui>휴지통</gui>으로 이동합니다. 삭제한 항목이 필요하다고 생각할 경우, 또는 실수로 삭제했다고 생각할 경우 <gui>휴지통</gui>에서 <link xref="files-recover">항목 복원</link>을 하여 원본 위치로 되돌릴 수 있습니다.</p>

  <steps>
    <title>파일을 휴지통으로 보내려면:</title>
    <item><p>휴지통에 두고자 하는 항목을 한 번 둘러 선택합니다.</p></item>
    <item><p>키보드의 <key>Delete</key> 키를 누릅니다. 대신 가장자리 창의 <gui>휴지통</gui>으로 항목을 끌어다 놓습니다.</p></item>
  </steps>

  <p>파일은 휴지통에 이동하며 삭제를 <gui>실행 취소</gui>할 선택지가 나타남을 볼 수 있습니다. <gui>실행 취소</gui> 단추는 몇 초간 나타납니다. <gui>실행 취소</gui>를 선택하면, 원본 위치로 파일을 복원합니다.</p>

  <p>파일을 완전히 삭제하여 컴퓨터의 디스크 여분 공간을 확보하려면 휴지통을 비워야합니다. 휴지통을 비우려면, 가장자리 창에서 <gui>휴지통</gui>에 포인터 커서를 두고 오른쪽 단추를 누른 후, <gui>휴지통 비우기</gui>를 선택합니다.</p>

  <section id="permanent">
    <title>완전한 파일 삭제</title>
    <p>휴지통으로 먼저 보내지 않고 파일을 완전히 즉시 삭제할 수 있습니다.</p>

  <steps>
    <title>파일을 완전히 삭제하려면:</title>
    <item><p>삭제할 항목을 선택합니다.</p></item>
    <item><p><key>Shift</key> 키를 누른 상태에서 키보드의 <key>Delete</key> 키를 누릅니다.</p></item>
    <item><p>실행 취소할 수 없는 동작이기 때문에 파일 또는 폴더를 정말로 삭제할 지 확인 질문을 받습니다.</p></item>
  </steps>

  <note><p><link xref="files#removable">이동식 장치</link>에서 삭제한 파일은 윈도우 또는 맥 OS와 같은 기타 운영체제에서 나타나지 않습니다. 여전히 해당 미디어에 있겠지만, 컴퓨터에 해당 장치를 다시 연결하기 전까지는 볼 수 없습니다.</p></note>

  </section>

</page>
