<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="ko">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>키보드</gui> 설정에서 키보드 바로 가기를 정의하거나 바꿉니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>키보드 바로 가기 설정</title>

<p>키보드 바로 가기 키로 누를 키 또는 여러 키의 설정을 바꾸려면:</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>설정</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>설정</gui>을 누릅니다.</p>
    </item>
    <item>
      <p>가장자리 창에서 <gui>키보드</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p><gui>키보드 바로 가기</gui> 섹션에서, <gui>바로 가기 사용자 설정</gui>을 선택합니다.</p>
    </item>
    <item>
      <p>원하는 분류를 선택하거나 검색 단어를 입력합니다.</p>
    </item>
    <item>
      <p>원하는 동작 행을 누릅니다. <gui>바로 가기 설정</gui>창이 나타납니다.</p>
    </item>
    <item>
      <p>원하는 키 조합을 누른채로 기다리거나 <key>Backspace</key>를 눌러 원 상태로 되돌리든지, <key>Esc</key>키를 눌러 취소합니다.</p>
    </item>
  </steps>


<section id="defined">
<title>미리 지정한 바로 가기 키</title>
  <p>아래 분류로 묶어 설정을 바꿀 수 있는 여러가지 기 지정 바로 가기 키가 있습니다:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>접근성</title>
  <tr>
	<td><p>글자 크기 작게</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>고대비 켬/끔</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>글자 크기 크게</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>화면 키보드 켬/끔</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>화면 읽기 프로그램을 켬/끔</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>확대 기능을 켬/끔</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>확대</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>축소</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>실행 프로그램</title>
  <tr>
	<td><p>홈 폴더</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media>, <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media>, <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>계산기 실행</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> 또는 <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>전자메일 클라이언트 실행</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> 또는 <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>도움말 탐색기 실행</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>웹 브라우저 실행</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media>, <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media>, <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>검색</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> 또는 <key>검색</key></p></td>
  </tr>
  <tr>
	<td><p>설정</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>탐색</title>
  <tr>
	<td><p>모든 일반 창 숨기기</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>좌측 작업 공간으로 이동</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>우측 작업 공간으로 이동</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 아래 모니터로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 좌측 모니터로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 우측 모니터로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 위 모니터로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 좌측 작업 공간으로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key> <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 우측 작업 공간으로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 마지막 작업 공간으로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 작업 공간 1로 이동</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창을 작업 공간 2로 이동</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창을 작업 공간 3으로 이동</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창을 작업 공간 4로 이동</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>프로그램 전환</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>시스템 조작 전환</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>직접 시스템 조작 전환</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>마지막 작업 공간으로 전환</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>작업 공간 1로 전환</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>작업 공간 2로 전환</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>작업 공간 3으로 전환</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>작업 공간 4로 전환</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
        <td><p>창 전환</p></td>
        <td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창 바로 전환</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>앱 창으로 직접 전환</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>프로그램 창 전환</p></td>
	<td><p>끔</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>스크린샷</title>
  <tr>
	<td><p>창 화면을 그림으로 저장</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>전체 화면 영역을 그림으로 저장</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>스크린샷 도구 실행</p></td>
	<td><p><key>Print</key></p></td>
  </tr>
  <tr>
        <td><p>짧은 스크린 캐스트 녹화</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>소리 및 미디어</title>
  <tr>
	<td><p>꺼내기</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (꺼냄)</p></td>
  </tr>
  <tr>
	<td><p>미디어 재생 프로그램 실행</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (오디오 미디어)</p></td>
  </tr>
  <tr>
	<td><p>마이크로폰 음소거 켬/끔</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>다음 트랙</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (오디오 다음 부분)</p></td>
  </tr>
  <tr>
	<td><p>일시정지</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (오디오 일시정지)</p></td>
  </tr>
  <tr>
	<td><p>재생 (또는 재생/일시정지)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (오디오 재생)</p></td>
  </tr>
  <tr>
	<td><p>이전 트랙</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (오디오 이전 부분)</p></td>
  </tr>
  <tr>
	<td><p>재생 정지</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (오디오 정지)</p></td>
  </tr>
  <tr>
	<td><p>음량 감소</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (오디오 음량 감소)</p></td>
  </tr>
  <tr>
	<td><p>소리 끄기</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (오디오 음소거)</p></td>
  </tr>
  <tr>
	<td><p>음량 증가</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (오디오 음량 증가)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>시스템</title>
  <tr>
        <td><p>활성 알림 포커스</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>화면 잠금</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>전원 끄기 대화상자 표시</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>프로그램 메뉴 열기</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>키보드 바로 가기 복원</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>모든 프로그램 표시</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>현재 활동 개요 표시</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>알림 목록 표시</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>개요 표시</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>명령 실행 프롬프트 표시</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>입력</title>
  <tr>
  <td><p>다음 입력기로 전환</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>이전 입력기로 전환</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>창</title>
  <tr>
	<td><p>창 메뉴 활성</p></td>
	<td><p><keyseq><key>Alt</key><key>Space</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창 닫기</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>창 숨김</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>다른 창 뒤로 창 이동</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창 최대화</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>수평으로 창 최대화</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>수직으로 창 최대화</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창 이동</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>다른 창 앞으로 창 이동</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창이 덮여있으면 앞으로, 그렇지 않으면 뒤로</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>창 크기 조절</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>창 복원</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>전체 화면 모드 전환</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
	<td><p>최대화 상태 전환</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>모든 또는 하나의 작업 공간에 창 두기 전환</p></td>
	<td><p>끔</p></td>
  </tr>
  <tr>
        <td><p>왼쪽 나눔 창 보기</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>오른쪽 나눔 창 보기</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>사용자 정의 바로 가기</title>

  <p><gui>키보드</gui> 설정에서 프로그램 키보드 바로 가기를 만들려면:</p>

  <steps>
    <item>
      <p><gui>사용자 정의 바로 가기</gui>를 선택합니다.</p>
    </item>
    <item>
      <p>사용자 정의 바로 가기를 아직 설정하지 않았다면 <gui style="button">바로 가기 추가</gui> 단추를 누릅니다. 아니면 <gui style="button">+</gui> 단추를 누릅니다. <gui>사용자 정의 바로 가기 추가</gui> 창이 뜹니다.</p>
    </item>
    <item>
      <p>바로 가기를 식별할 <gui>이름</gui>과 프로그램을 실행할 <gui>명령</gui>을 입력합니다. 이를테면, <app>리듬박스</app>를 여는 바로 가기를 만들려면, <input>음악</input>이라는 이름을 넣고 <input>rhythmbox</input> 명령을 활용합니다.</p>
    </item>
    <item>
      <p><gui style="button">바로 가기 추가…</gui> 단추를 누릅니다. <gui>사용자 정의 바로 가기 추가</gui> 창에서 원하는 바로 가기 키 조합을 누르고 기다립니다.</p>
    </item>
    <item>
      <p><gui>추가</gui>를 누릅니다.</p>
    </item>
  </steps>

  <p>입력한 명령 이름은 적절한 시스템 명령이어야 합니다. 터미널을 열고 여기에 명령을 입력하여 확인할 수 있습니다. 프로그램을 여는 명령은 프로그램 자체 이름과 동일할 수 없습니다.</p>

  <p>사용자 정의 키보드 바로 가기 키와 관련있는 명령을 바꾸려면 바로 가기 키 줄을 누릅니다. <gui>사용자 정의 바로 가기 설정</gui>창이 나타나며, 명령을 편집할 수 있습니다.</p>

</section>

</page>
