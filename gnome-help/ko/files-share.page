<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="ko">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>파일 관리자에서 전자메일 연락처로 쉽게 파일을 내보냅니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

<title>전자메일로 파일 공유하기</title>

<p>파일 관리자에서 연락처의 지인에게 전자메일을 직접 보내어 쉽게 파일을 공유할 수 있습니다.</p>

  <note style="important">
    <p>시작하기 전에 <app>에볼루션</app> 또는 <app>기어리</app>를 컴퓨터에 설치했고, 전자메일 계정을 설정했는지 확인합니다.</p>
  </note>

<steps>
  <title>전자메일로 파일을 공유하려면:</title>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요에서 <app>파일</app> 프로그램을 엽니다.</p>
    </item>
  <item><p>보내려는 파일을 찾습니다.</p></item>
    <item>
      <p>파일에 오른쪽 단추를 누른 후 <gui>다른 위치로 보내기…</gui>를 선택합니다. 전자메일 작성 창에 파일이 들어있는 채로 나타납니다.</p>
    </item>
  <item><p><gui>받는 사람</gui>을 눌러 연락처를 선택하거나 파일을 보내고자 하는 전자메일 주소를 입력합니다. <gui>제목</gui>과 메시지 본문을 필요한대로 채우고 <gui>보내기</gui>를 누릅니다.</p></item>
</steps>

<note style="tip">
  <p>여러 파일을 한번에 보낼 수도 있습니다. 파일을 누르는 동안 <key>Ctrl</key> 키를 눌러 여러 파일을 선택하고, 선택한 파일에 오른쪽 단추를 누릅니다.</p>
</note>

</page>
