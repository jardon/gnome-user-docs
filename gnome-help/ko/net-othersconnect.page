<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersconnect" xml:lang="ko">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>(암호와 같은) 네트워크 연결 설정을 저장하여 모든 컴퓨터 사용자가 이 연결로 연결하게 할 수 있습니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2021, 2022, 2023</mal:years>
    </mal:credit>
  </info>

  <title>다른 사용자가 인터넷에 연결할 수 없어요</title>

  <p>네트워크 연결을 설정할 때, 보통 컴퓨터의 다른 사용자도 해당 연결을 활용할 수 있습니다. 연결 정보를 공유하지 않으면, 연결 설정을 확인해야합니다.</p>

  <steps>
    <item>
      <p><gui xref="shell-introduction#activities">현재 활동</gui> 개요를 열고 <gui>네트워크</gui> 입력을 시작합니다.</p>
    </item>
    <item>
      <p><gui>네트워크</gui>를 눌러 창을 엽니다.</p>
    </item>
    <item>
      <p>좌측의 목록에서 <gui>와이파이</gui>를 선택합니다.</p>
    </item>
    <item>
      <p><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">설정</span></media> 단추를 눌러 연결 세부 설정 창을 엽니다.</p>
    </item>
    <item>
      <p>좌측 창에서 <gui>신원</gui>을 선택합니다.</p>
    </item>
    <item>
      <p><gui>신원</gui>창 하단에서 <gui>다른 사용자가 사용할 수 있게 허용</gui> 옵션을 표시하여 다른 사용자가 네트워크 연결을 사용할 수 있게 합니다.</p>
    </item>
    <item>
      <p><gui style="button">적용</gui>을 눌러 바뀐 내용을 저장합니다.</p>
    </item>
  </steps>

  <p>컴퓨터의 다른 사용자는 이제부터 세부 설정을 열지 않고도 이 연결을 활용할 수 있습니다.</p>

  <note>
    <p>누구든지 이 설정을 바꿀 수 있습니다.</p>
  </note>

</page>
