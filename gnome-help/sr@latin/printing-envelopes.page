<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Džim Kembel</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uverite se da ste kovertu postavili kako treba, i da ste izabrali odgovarajuću veličinu papira.</desc>
  </info>

  <title>Štampajte koverte</title>

  <p>Većina štampača će vam omogućiti da štampate direktno na koverti. Ovo je naročito korisno ako imate mnogo pisama za slanje, na primer.</p>

  <section id="envelope">
    <title>Štampajte na koverti</title>

  <p>Postoje dve stvari koje trebate da proverite kada pokušate da štampate na koverti.</p>
  <p>Prva je da li štampač zna koja je veličina koverte. Pritisnite <keyseq><key>Ktrl</key><key>P</key></keyseq> da otvorite prozorče štampanja, idite na jezičak <gui>Podešavanje strane</gui> i ako možete izaberite „Koverta“ za <gui>veličinu papira</gui>. Ako ovo ne možete da uradite, vidite da li možete da izmenite <gui>Veličinu papira</gui> na veličinu koverte (npr. „C5“). Paket koverti će reći koje su veličine; većina koverti je u uobičajenoj veličini.</p>

  <p>Kao drugo, morate da se uverite da su koverte ubačene u fioku štampača sa odgovarajućom stranom na gore. Proverite u uputstvu štampača za ovo, ili pokušajte da odštampate jednu kovertu i proverite koja je strana odštampana da vidite kako treba da ih postavite.</p>

  <note style="warning">
    <p>Neki štampači nisu napravljeni sa mogućnošću štampanja na kovertama, naročito neki laserski štampači. Proverite uputstvo vašeg štampača da vidite da li prihvata koverte. U suprotnom, možete da oštetite štampač ako u njega ubacite kovertu.</p>
  </note>

  </section>

<!--
TODO: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
