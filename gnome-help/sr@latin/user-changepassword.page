<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-changepassword" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="user-accounts#passwords"/>
    <link type="seealso" xref="user-goodpassword"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Držite vaš nalog bezbednim tako što ćete često menjati vašu lozinku u vašim podešavanjima naloga.</desc>
  </info>

  <title>Promenite vašu lozinku</title>

  <p>Dobra zamisao je da promenite vašu lozinku s vremena na vreme, naročito ako mislite da neko drugi zna vašu lozinku.</p>

  <p>Potrebna su vam <link xref="user-admin-explain">Administratorska prava</link> za uređivanje korisničkih naloga koji nisu vaši.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Korisnici</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Korisnici</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Click the label <gui>·····</gui> next to <gui>Password</gui>. If you
      are changing the password for a different user, you will first need to
      <gui>Unlock</gui> the panel and select the account under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>U odgovarajuća polja prvo unesite vašu trenutnu lozinku a zatim novu. Unesite još jednom vašu novu lozinku u polje <gui>Potvrdi novu lozinku</gui>.</p>
      <p>Možete da pritisnete ikonicu za <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">stvorite lozinku</span></media></gui> da sami stvorite nasumičnu lozinku.</p>
    </item>
    <item>
      <p>Kliknite <gui>Promeni</gui>.</p>
    </item>
  </steps>

  <p>Uverite se da ste <link xref="user-goodpassword">izabrali dobru lozinku</link>. Ovo će pomoći u očuvanju bezbednosti vašeg korisničkog naloga.</p>

  <note>
    <p>Kada osvežite vašu lozinku prijavljivanja, vaša lozinka priveska prijavljivanja će samostalno biti osvežena da bi bila ista kao vaša nova lozinka prijavljivanja.</p>
  </note>

  <p>Ako zaboravite vašu lozinku, bilo koji korisnik sa administratorskim ovlašćenjiam može da vam je izmeni.</p>

</page>
