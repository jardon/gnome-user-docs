<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-map-buttons" xml:lang="sr-Latn">
      
  <info>
    <revision pkgversion="3.7.1" version="0.1" date="2012-11-16" status="stub"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Assign functions to the hardware buttons on the graphics tablet.</desc>
  </info>

  <title>Map the tablet buttons</title>

  <p>The hardware buttons on a tablet can be configured for various functions.</p>

<steps>
  <item>
    <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Vakom tablica</gui>.</p>
  </item>
  <item>
    <p>Kliknite na <gui>Vakom tablica</gui> da otvorite panel.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Click <gui>Map Buttons</gui>.</p>
  </item>
  <item>
    <p>An on screen display shows the layout of the tablet's buttons. Press each
    button on the tablet and choose one of these functions:</p>
    <list>
      <item><p><gui>Application defined</gui></p></item>
      <item><p><gui>Show on-screen help</gui></p></item>
      <item><p><gui>Switch monitor</gui></p></item>
      <item><p><gui>Send keystroke</gui></p></item>
   </list>
  </item>
  <item>
    <p>Click <gui>Done</gui> when each button is configured, and press
    <key>Esc</key> to exit.</p>
  </item>
</steps>


</page>
