<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-connect" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33" date="2019-07-17" status="candidate"/>
    <revision pkgversion="43" date="2022-09-10" status="candidate"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Pristupite internetu — bežično.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Povezivanje na bežičnu mrežu</title>

<p>Ako imate računar sa bežičnom karticom, možete da se povežete na bežičnu mrežu kojoj ste u dometu da biste dobili pristup internetu, videli deljene datoteke na mreži, i tako dalje.</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#systemmenu">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-disabled-symbolic.svg"/>
    Wi-Fi</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click the name of the network you want.</p>
    <p>If the name of the network is not shown, scroll down the list. If you
    still do not see the network, you may be out of range, or the network
    <link xref="net-wireless-hidden">might be hidden</link>.</p>
  </item>
  <item>
    <p>Ako je mreža zaštićena lozinkom (<link xref="net-wireless-wepwpa">ključem šifrovanja</link>), unesite je kada vam bude bila zatražena i kliknite na <gui>Uspostavi vezu</gui>.</p>
    <p>Ako ne znate ključ, može biti da je zapisan na poleđini bežičnog usmerivača ili osnovne postave ili u priručniku uputstva, ili ćete morati da pitate osobu koja administrira bežičnu mrežu.</p>
  </item>
  <item>
    <p>Ikonica mreže će promeniti izgled kako računar bude pokušavao da se poveže na mrežu.</p>
  </item>
  <item>
    <if:choose>
    <if:when test="platform:gnome-classic">
    <p>Ako je povezivanje uspešno obavljeno, ikonica će se promeniti u tačkice sa nekoliko uspravnih štapića iznad (<media its:translate="no" type="image" mime="image/svg" src="figures/classic-topbar-network-wireless-strength-excellent.svg" width="28" height="28"/>). Više štapića označavaju jaču vezu sa mrežom. Manje štapića znači da je veza slaba i neće biti mnogo pouzdana.</p>
    </if:when>
    <p>Ako je povezivanje uspešno obavljeno, ikonica će se promeniti u tačkice sa nekoliko uspravnih štapića iznad (<media its:translate="no" type="image" mime="image/svg" src="figures/topbar-network-wireless-strength-excellent.svg" width="28" height="28"/>). Više štapića označavaju jaču vezu sa mrežom. Manje štapića znači da je veza slaba i neće biti mnogo pouzdana.</p>
    </if:choose>
  </item>
</steps>

  <p>Ako povezivanje na mrežu nije uspešno, od vas može biti zatraženo da ponovo unesete vašu lozinku ili vas može obavestiti da je veza sa mrežom prekinuta. Uzrok ovome može biti veliki broj stvari. Možda ste uneli pogrešnu lozinku, bežični signal može biti previše slab, ili možda bežična kartica vašeg računara nije ispravna. Za više pomoći pogledajte <link xref="net-wireless-troubleshooting"/>.</p>

  <p>Jača veza na bežičnu mrežu ne mora obavezno da znači da ćete imati bržu vezu na internet, ili da ćete imati veće brzine preuzimanja. Bežična veza povezuje vaš računar na <em>uređaj koji obezbeđuje vezu na internet</em> (na usmerivač ili modem), ali ove dve veze su zapravo različite, tako da će raditi pri drugačijim brzinama.</p>

</page>
