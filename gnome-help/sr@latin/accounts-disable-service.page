<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="accounts"/>

    <revision pkgversion="3.5.5" date="2012-08-14" status="review"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
<credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Neki nalozi na mreži mogu biti korišćeni za pristup nekoliko usluga (kao kalendar i e-pošta). Možete da odredite koje od ovih usluga mogu da koriste programi.</desc>
  </info>

  <title>Odredite kojim uslugama na mreži može da pristupi nalog</title>

  <p>Some types of online account providers allow you to access several services
  with the same user account. For example, Google accounts provide access to
  calendar, email, and contacts. You may want to use your account for some
  services, but not others. For example, you may want to use your Google account
  for email but not calendar if you have a different online account that you use
  for calendar.</p>

  <p>Možete da isključite neke usluge koje su dostavljene svakim nalogom na mreži:</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-terminology">Aktivnosti</gui> i počnite da kucate <gui>Nalozi na mreži</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>naloge na mreži</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Select the account which you want to change from the list on the
      right.</p>
    </item>
    <item>
      <p>Spisak usluga koje su dostupne sa ovim nalogom biće prikazane pod <gui>Koristi za</gui>. Pogledajte <link xref="accounts-which-application"/> da vidite koji programi pristupaju kojim uslugama.</p>
    </item>
    <item>
      <p>Isključite sve one usluge koje ne želite da koristite.</p>
    </item>
  </steps>

  <p>Kada isključite uslugu za nalog, programi na vašem računaru neće više biti u mogućnosti da koriste nalog da se povežu na tu uslugu.</p>

  <p>To turn on a service that you disabled, just go back to the <gui>Online
  Accounts</gui> panel and switch it on.</p>

</page>
