<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-admin-change" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Možete da dozvolite korisnicima da vrše izmene na sistemu dajući im administratorska ovlašćenja.</desc>
  </info>

  <title>Izmenite ko ima administratorska ovlašćenja</title>

  <p>Administratorska ovlašćenja predstavljaju način odlučivanja o tome ko može da unosi izmene na važnim delovima sistema. Možete da izmenite koji korisnici imaju administratorska ovlašćenja a koji ne. Ona predstavljaju dobar način održavanja bezbednosti sistema i zaštitu od mogućih oštećenja neovlašćenim izmenama.</p>

  <p>Potrebna su vam <link xref="user-admin-explain">administratorska prava</link> za menjanje vrsta naloga.</p>

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Korisnici</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Korisnici</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Otključaj</gui> u gornjem desnom uglu i unesite vašu lozinku.</p>
    </item>
    <item>
      <p>Under <gui>Other Users</gui>, select the user whose privileges you want
      to change.</p>
    </item>
    <item>
      <p>Set the <gui>Administrator</gui> switch to on.</p>
    </item>
    <item>
      <p>Ovlašćenja korisnika će se izmeniti prilikom sledećeg prijavljivanja.</p>
    </item>
  </steps>

  <note>
    <p>Prvi korisnički nalog na sistemu je obično onaj koji ima administratorska ovlašćenja. To je korisnički nalog koji je napravljen prilikom instalacije sistema.</p>
    <p>Nije poželjno imati previše korisnika sa <gui>Administratorskim</gui> ovlašćenjima na sistemu.</p>
  </note>

</page>
