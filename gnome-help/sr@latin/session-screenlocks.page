<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="session-screenlocks" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="prefs-display#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>

    <revision pkgversion="3.38.4" date="2021-03-07" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change how long to wait before locking the screen in the
    <gui>Screen Lock</gui> settings.</desc>
  </info>

  <title>Ekran se previše brzo zaključava</title>

  <p>Ako ostavite vaš računar na nekoliko minuta, ekran će se sam zaključati tako da ćete morati da unesete lozinku da biste počeli ponovo da ga koristite. Ovo se radi iz bezbednosnih razloga (tako da niko ne može da poremeti vaš rad ako ostavite računar bez nadzora), ali može biti zamarajuće ako se ekran prebrzo zaključava.</p>

  <p>Da bi se ekran zaključavao nakon dužeg vremena:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Screen Lock</gui>.</p>
    </item>

    <item>
      <p>Click on <gui>Screen Lock</gui> to open the panel.</p>
    </item>
    <item>
      <p>If <gui>Automatic Screen Lock</gui> is on, you can change the value
      in the <gui>Automatic Screen Lock Delay</gui> drop-down list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If you don’t ever want the screen to lock itself automatically, switch
    the <gui>Automatic Screen Lock</gui> switch to off.</p>
  </note>

</page>
