<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Izmenite vreme zatamnjivanja ekrana da uštedite energiju.</desc>
  </info>

  <title>Podesite vreme zatamnjivanja ekrana</title>

  <p>Da uštedite napajanje, možete da podesite vreme pre zatamnjivanja ekrana kada je u mirovanju. Možete takođe u potpunosti da isključite zatamnjivanje.</p>

  <steps>
    <title>Da podesite vreme zatamnjivanja ekrana:</title>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Napajanje</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Napajanje</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Use the <gui>Screen Blank</gui> drop-down list under <gui>Power Saving
      Options</gui> to set the time until the screen blanks, or disable the blanking
      completely.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Kada ostavite računar da miruje, ekran će se sam zaključati iz bezbednosnih razloga. Da izmenite ovo ponašanje, pogledajte <link xref="session-screenlocks"/>.</p>
  </note>

</page>
