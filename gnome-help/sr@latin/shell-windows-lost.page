<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Proverite pregled <gui>Aktivnosti</gui> ili ostale radne prostore.</desc>
  </info>

  <title>Pronađite izgubljeni prozor</title>

  <p>Prozor na drugom radnom prostoru, ili skriven iza drugog prozora, se može lako naći ako koristite pregled <gui xref="shell-introduction#activities">aktivnosti</gui>:</p>

  <list>
    <item>
      <p>Otvorite pregled <gui>aktivnosti</gui>. Ako se nedostajući prozor nalazi na tekućem <link xref="shell-windows#working-with-workspaces">radnom prostoru</link>, ovde će biti prikazan u minijaturi. Jednostavno kliknite na minijaturu da ponovo prikažete prozor, ili</p>
    </item>
    <item>
      <p>Click different workspaces in the
      <link xref="shell-workspaces">workspace selector</link>
      to try to find your window, or</p>
    </item>
    <item>
      <p>Kliknite desnim tasterom miša na program u poletniku i njegovi otvoreni prozori će biti prikazani. Kliknite na prozor na spisku da se prebacite na njega.</p>
    </item>
  </list>

  <p>Korišćenje prebacivača prozora:</p>

  <list>
    <item>
      <p>Pritisnite <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> da prikažete <link xref="shell-windows-switching">prebacivač prozora</link>. Nastavite da držite taster <key>Super</key> i pritisnite <key>Tab</key> da kružite po otvorenim prozorima, ili <keyseq><key>Pomak</key><key>Tab</key></keyseq> da kružite unazad.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>Ako neki program ima više otvorenih prozora, držite pritisnutim <key>Super</key> i pritisnite <key>`</key> (ili taster iznad tastera <key>Tab</key>) da se krećete po njima.</p>
    </item>
  </list>

</page>
