<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-troubleshooting-hardware-info" xml:lang="sr-Latn">

  <info>
    <link type="next" xref="net-wireless-troubleshooting-hardware-check"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>

    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Priložnici vikija Ubuntuove dokumentacije</name>
    </credit>
    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Trebaće vam detalji kao što je broj modela vašeg bežičnog prilagođivača u narednim koracima rešavanja problema.</desc>
  </info>

  <title>Rešavanje problema bežične mreže</title>
  <subtitle>Prikupite podatke o vašim mrežnim komponentama</subtitle>

  <p>U ovom koraku, prikupite podatke o vašem bežičnom mrežnom uređaju. Način rešavanja mnogih bežičnih problema zavisi od marke i broja modela bežičnog prilagođivača, tako da ćete morati da pribeležite te detalje. Takođe može biti od pomoći da imate neke od stvari koje idu uz vaš računar, kao što su diskovi za instalaciju upravljačkih programa diska. Potražite sledeće stvari, ako ih još uvek imate:</p>

  <list>
    <item>
      <p>Pakovanje i uputstva za vaše bežične uređaje (naročito korisničko uputstvo vašeg usmerivača)</p>
    </item>
    <item>
      <p>Disk koji sadrži upravljačke programe za vaš bežični prilagođivač (čak i ako sadrži samo Vindouzove upravljačke programe)</p>
    </item>
    <item>
      <p>Brojeve proizvođača i modela vašeg računara, bežičnog prilagođivača i usmerivača. Ovaj podatak se obično nalazi na donjoj strani ili na pozadini uređaja.</p>
    </item>
    <item>
      <p>Bilo koji broj izdanja ili revizije koji može biti odštampan na vašim bežičnim mrežnim uređajima ili njihovim pakovanjima. Ovo naročito može biti korisno, zato tražite pažljivo.</p>
    </item>
    <item>
      <p>Bilo šta na disku upravljačkog programa što određuje sam uređaj, izdanje njegovog „ugnježdenog programa“, ili komponente (čipset) koje koristi.</p>
    </item>
  </list>

  <p>Ako je moguće, pokušajte da omogućite pristup nekoj drugoj radnoj internet vezi kako biste mogli da preuzmete softver i upravljačke programe ako je potrebno. (Priključivanje vašeg računara direktno u usmerivač pomoću mrežnog kabla je jedan način postizanja ovoga, ali ga priključite samo kada morate.)</p>

  <p>Kada budete imali što je više moguće ovih stvari, kliknite <gui>Sledeće</gui>.</p>

</page>
