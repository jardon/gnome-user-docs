<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-multi-monitor" xml:lang="sr-Latn">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mapirajte Vakom tablicu prema posebnom monitoru.</desc>
  </info>

  <title>Izaberite monitor</title>

<steps>
  <item>
    <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Vakom tablica</gui>.</p>
  </item>
  <item>
    <p>Kliknite na <gui>Vakom tablica</gui> da otvorite panel.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Next to <gui>Map to Monitor</gui>, select the monitor you wish to
    receive input from your graphics tablet, or choose
    <gui>All Displays</gui>.</p>
    <note style="tip"><p>Samo monitori koji su podešeni moći će da se biraju.</p></note>
  </item>
  <item>
    <p>Switch <gui>Keep aspect ratio</gui> to on to match the drawing
    area of the tablet to the proportions of the monitor. This setting, also
    called <em>force proportions</em>, “letterboxes” the drawing area on a
    tablet to correspond more directly to a display. For example, a 4∶3 tablet
    would be mapped so that the drawing area would correspond to a widescreen
    display.</p>
  </item>
</steps>

</page>
