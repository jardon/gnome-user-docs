<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-suspendfail" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Neke komponente računara stvaraju probleme prilikom obustave.</desc>

    <credit type="author">
      <name>Gnomov projekat dokumentacije</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Zašto moj računar neće da se povrati nakon što sam ga uspavao?</title>

<p>Ako <link xref="power-suspend">obustavite</link> rad računara, i zatim pokušate da ga povratite, možete uvideti da ne radi onako kako ste očekivali. Ovo može biti zato što vaše komponente ne podržavaju ispravno uspavljivanje.</p>

<section id="resume">
  <title>Moj računar je obustavljen i neće da se povrati</title>
  <p>Ako obustavite vaš računar i nakon toga pritisnete taster ili kliknete mišem, trebao bi da se probudi i da prikaže ekran na kome traži vašu lozinku. Ako se ovo ne dogodi, pokušajte da pritisnete dugme za napajanje (nemojte ga držati, samo ga pritisnite jednom).</p>
  <p>Ako i ovo ne pomogne, uverite se da je monitor vašeg računara upaljen i pokušajte još jednom da pritisnete taster na tastaturi.</p>
  <p>Kao poslednje sredstvo, ugasite računar pritiskom na dugme za napajanje 5-10 sekundi. Izgubićete sav nesačuvani rad na ovaj način. Tada ćete biti u mogućnosti da ponovo uključite računar.</p>
  <p>Ako se ovo dešava svaki put kada obustavite računar, funkcija obustavljanja ne radi sa vašim komponentama.</p>
  <note style="warning">
    <p>Ako vaš računar ostane bez napajanja, a nema rezervni izvor napajanja (kao što je radna baterija), ugasiće se.</p>
  </note>
</section>

<section id="hardware">
  <title>Moja bežična veza (ili druga komponenta) ne radi kada probudim računar</title>
  <p>Ako obustavite rad računara i nakon toga ga povratite, možda ćete uvideti da vaša internet veza, miš, ili neki drugi uređaj ne radi ispravno. Ovo može biti zato što upravljački program za uređaj ne podržava pravilno obustavljanje. To je <link xref="hardware-driver">problem sa upravljačkim programom</link> a ne sa samim uređajem.</p>
  <p>Ako uređaj ima prekidač, pokušajte da ga ugasite, i da ga zatim ponovo upalite. U većini slučajeva, uređaj će početi da radi ponovo. Ako se povezuje preko USB kabla ili slično, isključite uređaj i ponovo ga priključite i vidite da li radi.</p>
  <p>Ako ne možete da ugasite ili da isključite uređaj, ili ako ovo ne radi, možda ćete morati ponovo da pokrenete računar da bi uređaj ponovo počeo da radi.</p>
</section>

</page>
