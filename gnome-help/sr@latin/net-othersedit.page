<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-othersedit" xml:lang="sr-Latn">

  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-31" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Fil Bul</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Majkl Hil</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Morate da poništite izbor opcije <gui>Dostupno svim korisnicima</gui> u podešavanjima mrežne veze.</desc>
  </info>

  <title>Ostali korisnici ne mogu da uređuju mrežne veze</title>

  <p>Ako vi možete da uređujete mrežnu vezu ali ostali korisnici na vašem računaru ne mogu, morate da podesite vezu na <gui>dostupno svim korisnicima</gui>. Ovo čini da svako na računaru može da se <em>poveže</em> koristeći tu vezu.</p>

<!--
  <p>The reason for this is that, since everyone is affected if the settings
  are changed, only highly-trusted (administrator) users should be allowed to
  modify the connection.</p>

  <p>If other users really need to be able to change the connection themselves,
  make it so the connection is <em>not</em> set to be available to everyone on
  the computer. This way, everyone will be able to manage their own connection
  settings rather than relying on one set of shared, system-wide settings for
  the connection.</p>
-->

  <steps>
    <item>
      <p>Otvorite pregled <gui xref="shell-introduction#activities">Aktivnosti</gui> i počnite da kucate <gui>Mreža</gui>.</p>
    </item>
    <item>
      <p>Kliknite na <gui>Mreža</gui> da otvorite panel.</p>
    </item>
    <item>
      <p>Izaberite <gui>Bežična</gui> sa spiska na levoj strani.</p>
    </item>
    <item>
      <p>Pritisnite na dugme <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">podešavanja</span></media> da otvorite pojedinosti veze.</p>
    </item>
    <item>
      <p>Izaberite <gui>Identitet</gui> u oknu na levoj strani.</p>
    </item>
    <item>
      <p>Na dnu panela <gui>Identitet</gui>, izaberite opciju <gui>Učini dostupno ostalim korisnicima</gui> da omogućite ostalim korisnicima da koriste mrežnu vezu.</p>
    </item>
    <item>
      <p>Pritisnite <gui style="button">Primeni</gui> da sačuvate izmene.</p>
    </item>
  </steps>

</page>
