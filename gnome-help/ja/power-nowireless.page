<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="power-nowireless" xml:lang="ja">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="hardware-driver"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>一部の無線デバイスは、コンピューターのサスペンドをうまく処理できず、正常に再開できないことがあります。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>コンピューターを再開すると無線ネットワークの接続が切れます</title>

  <p>If you have suspended your computer, you may find that your wireless
  internet connection does not work when you resume it again. This happens when
  the <link xref="hardware-driver">driver</link> for the wireless device does
  not fully support certain power saving features.
  Typically, the wireless connection fails to turn on properly when the
  computer is resumed.</p>

  <p>この問題が発生した場合は、無線接続を一度オフにしてからオンにしてみてください。</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Wi-Fi</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Wi-Fi</gui> to open the panel.</p>
    </item>
    <item>
      <p>Switch the <gui>Wi-Fi</gui> switch at the top-right of the window to
      off and then on again.</p>
    </item>
    <item>
      <p>If the wireless still does not work, switch the <gui>Airplane
      Mode</gui> switch to on and then switch it off again.</p>
    </item>
  </steps>

  <p>これでもまだ動作しない場合は、コンピューターを再起動させれば再び無線が機能するようになるでしょう。再起動後も問題が解決されない場合には、Ethernet ケーブルを使ってインターネットに接続してコンピューターを更新してみてください。</p>

</page>
