<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="ja">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>有線ネットワーク接続を設定する際、ほとんどの場合がネットワークケーブルを差し込むだけです。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>有線 (Ethernet) ネットワークへの接続</title>

  <!-- TODO: create icon manually because it is one overlaid on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>)
  is displayed on the top bar with three dots while the connection is being
  established. The dots disappear when you are connected.</p>
  <media its:translate="no" type="image" src="figures/net-wired-ethernet-diagram.svg" mime="image/svg" width="51" height="38" style="floatend floatright">
    <p its:translate="yes">Diagram of an Ethernet cable</p>
  </media>

  <p>If this does not happen, you should first of all make sure that your
  network cable is plugged in. One end of the cable should be plugged into the
  rectangular Ethernet (network) port on your computer, and the other end
  should be plugged into a switch, router, network wall socket or similar
  (depending on the network setup you have). The Ethernet port is often on the
  side of a laptop, or near the top of the back of a desktop computer. Sometimes, a
  light beside the Ethernet port will indicate that it is plugged in and active.</p>

  <note>
    <p>You cannot plug one computer directly into another one with a network
    cable (at least, not without some extra setting-up). To connect two
    computers, you should plug them both into a network hub, router or
    switch.</p>
  </note>

  <p>それでもなお接続できない場合、自動設定 (DHCP) に対応していない可能性があります。このような場合、<link xref="net-manual">手作業による設定</link>が必要になります。</p>

</page>
