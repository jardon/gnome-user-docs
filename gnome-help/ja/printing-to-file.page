<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="ja">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Save a document as a PDF, PostScript or SVG file instead of sending
    it to a printer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>ファイルに出力する</title>

  <p>You can choose to print a document to a file instead of sending it to
  print from a printer. Printing to file will create a <sys>PDF</sys>, a
  <sys>PostScript</sys> or a <sys>SVG</sys> file that contains the document.
  This can be useful if you want to transfer the document to another machine
  or to share it with someone.</p>

  <steps>
    <title>To print to file:</title>
    <item>
      <p>Open the print dialog by pressing
      <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p><gui style="tab">全般</gui>タブの<gui>プリンター</gui>オプションで<gui>ファイルに出力する</gui>を選択します。</p>
    </item>
    <item>
      <p>デフォルトのファイル名および保存先を変更するには、プリンター一覧の下のファイル名をクリックします。ファイル名と保存先を指定して、<gui style="button">選択</gui>をクリックします。</p>
    </item>
    <item>
      <p><sys>PDF</sys> is the default file type for the document. If you want
      to use a different <gui>Output format</gui>, select either
      <sys>PostScript</sys> or <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>必要に応じて他のページ設定を行います。</p>
    </item>
    <item>
      <p><gui style="button">印刷</gui>を押してファイルを保存します。</p>
    </item>
  </steps>

</page>
