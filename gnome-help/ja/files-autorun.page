<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="ja">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>CDやDVD、カメラ、オーディオプレイヤー、その他のデバイスおよびメディアにあわせてアプリケーションを自動起動します。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>デバイスやディスクにあわせてアプリケーションを起動する</title>

  <p>デバイスを接続したときや、ディスクやメディアカードを挿入したときにアプリケーションを自動起動させることができます。たとえば、デジタルカメラをつないだら写真管理ソフトが起動するようにしておくと便利でしょう。この機能をオフにして、デバイスを接続しても何も起動しないように設定することもできます。</p>

  <p>さまざまなデバイスを接続した場合に起動するアプリケーションを設定する方法は次のとおりです。</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Removable Media</gui>.</p>
  </item>
  <item>
    <p><gui>リムーバブルメディア</gui>をクリックします。</p>
  </item>
  <item>
    <p>設定したいデバイスやメディアの種類を探し、それ用のアプリケーションや動作を選択します。各種のデバイスやメディアについては以下の説明を参照してください。</p>
    <p>Instead of starting an application, you can also set it so that the
    device will be shown in the file manager, with the <gui>Open folder</gui>
    option. When that happens, you will be asked what to do, or nothing will
    happen automatically.</p>
  </item>
  <item>
    <p>If you do not see the device or media type that you want to change in
    the list (such as Blu-ray discs or E-book readers), click <gui>Other
    Media…</gui> to see a more detailed list of devices. Select the type of
    device or media from the <gui>Type</gui> drop-down and the application or
    action from the <gui>Action</gui> drop-down.</p>
  </item>
</steps>

  <note style="tip">
    <p>If you do not want any applications to be opened automatically, whatever
    you plug in, select <gui>Never prompt or start programs on media
    insertion</gui> at the bottom of the <gui>Removable Media</gui> window.</p>
  </note>

<section id="files-types-of-devices">
  <title>デバイスやメディアの種類</title>
<terms>
  <item>
    <title>オーディオディスク</title>
    <p>Choose your favorite music application or CD audio extractor to handle
    audio CDs. If you use audio DVDs (DVD-A), select how to open them under
    <gui>Other Media…</gui>. If you open an audio disc with the file manager,
    the tracks will appear as WAV files that you can play in any audio player
    application.</p>
  </item>
  <item>
    <title>ビデオディスク</title>
    <p>Choose your favorite video application to handle video DVDs. Use the
    <gui>Other Media…</gui> button to set an application for Blu-ray, HD DVD,
    video CD (VCD), and super video CD (SVCD). If DVDs or other video discs
    do not work correctly when you insert them, see <link xref="video-dvd"/>.
    </p>
  </item>
  <item>
    <title>空のディスク</title>
    <p>Use the <gui>Other Media…</gui> button to select a disc-writing
    application for blank CDs, blank DVDs, blank Blu-ray discs, and blank HD
    DVDs.</p>
  </item>
  <item>
    <title>カメラと写真</title>
    <p><gui>写真</gui>のドロップダウンリストから、デジタルカメラを接続したときや、カメラ用の CF、SD、MMC、MS カードなどのメディアカードを挿入したときに起動する写真管理アプリケーションを選択します。ファイルマネージャーを使って単に写真を見ることもできます。</p>
    <p>Under <gui>Other Media…</gui>, you can select an application to open
    Kodak picture CDs, such as those you might have made in a store. These are
    regular data CDs with JPEG images in a folder called
    <file>Pictures</file>.</p>
  </item>
  <item>
    <title>音楽プレイヤー</title>
    <p>お使いのポータブル音楽プレイヤーの音楽ライブラリを管理するアプリケーションを選択するか、あるいは、ファイルマネージャーを使って自分でファイルを管理します。</p>
    </item>
    <item>
      <title>電子書籍リーダー</title>
      <p>Use the <gui>Other Media…</gui> button to choose an application to
      manage the books on your e-book reader, or manage the files yourself
      using the file manager.</p>
    </item>
    <item>
      <title>ソフトウェア</title>
      <p>ディスクやリムーバブルメディアによっては、メディア挿入時に自動実行可能なソフトウェアが含まれているものもあります。<gui>ソフトウェア</gui>のオプションで、自動実行ソフトウェアが含まれるメディアが挿入された場合にどうするか制御します。ソフトウェアが実行される前に、どう処理するかの確認が必ず行われます。</p>
      <note style="warning">
        <p>Never run software from media you don’t trust.</p>
      </note>
   </item>
</terms>

</section>

</page>
