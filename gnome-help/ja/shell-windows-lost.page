<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-lost" xml:lang="ja">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision version="gnome:40" date="2021-02-24" status="review"/>

    <credit type="author">
      <name>GNOME ドキュメンテーションプロジェクト</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Check the <gui>Activities</gui> overview or other workspaces.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>松澤 二郎</mal:name>
      <mal:email>jmatsuzawa@gnome.org</mal:email>
      <mal:years>2011, 2012, 2013, 2014, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>赤星 柔充</mal:name>
      <mal:email>yasumichi@vinelinux.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kentaro KAZUHAMA</mal:name>
      <mal:email>kazken3@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Shushi Kurose</mal:name>
      <mal:email>md81bird@hitaki.net</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Noriko Mizumoto</mal:name>
      <mal:email>noriko@fedoraproject.org</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>坂本 貴史</mal:name>
      <mal:email>o-takashi@sakamocchi.jp</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>日本GNOMEユーザー会</mal:name>
      <mal:email>http://www.gnome.gr.jp/</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>見失ったウィンドウを探しだす</title>

  <p>A window on a different workspace, or hidden behind another window, is
  easily found using the <gui xref="shell-introduction#activities">Activities</gui>
  overview:</p>

  <list>
    <item>
      <p>Open the <gui>Activities</gui> overview. If the missing window is on
      the current
      <link xref="shell-windows#working-with-workspaces">workspace</link>, it
      will be shown here in thumbnail. Simply click the thumbnail to redisplay
      the window, or</p>
    </item>
    <item>
      <p>Click different workspaces in the
      <link xref="shell-workspaces">workspace selector</link>
      to try to find your window, or</p>
    </item>
    <item>
      <p>ダッシュボードのアプリケーションを右クリックすると、そのアプリケーションが開いているウィンドウの一覧が表示されます。その一覧からお探しのウィンドウをクリックすると、そのウィンドウに切り替わります。</p>
    </item>
  </list>

  <p>ウィンドウスイッチャーを使う方法は次のとおりです。</p>

  <list>
    <item>
      <p>Press
      <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>
      to display the <link xref="shell-windows-switching">window switcher</link>.
      Continue to hold down the <key>Super</key> key and press <key>Tab</key>
      to cycle through the open windows, or
      <keyseq><key>Shift</key><key>Tab</key> </keyseq> to cycle backwards.</p>
    </item>
    <item if:test="!platform:gnome-classic">
      <p>アプリケーションが複数のウィンドウを開いている場合、<key>Super</key> を押さえながら <key>`</key> キー (<key>Tab</key> の上のキー) を押すと、そのアプリケーション内でウィンドウが切り替わります。</p>
    </item>
  </list>

</page>
