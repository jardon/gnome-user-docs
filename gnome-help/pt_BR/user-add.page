<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-add" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#manage" group="#first"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-03-17"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione novos usuários para que outros possam iniciar sessão no computador.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Adicionando uma nova conta de usuário</title>

  <p>Você pode adicionar múltiplas contas de usuários a seu computador. Forneça uma conta para cada pessoa em sua residência ou empresa. Cada usuário tem sua própria pasta pessoal, documentos e configurações.</p>

  <p>Você precisa de <link xref="user-admin-explain">privilégios administrativos</link> para adicionar contas de usuários.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Usuários</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Usuários</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Desbloquear</gui> no canto superior direito e insira sua senha quando solicitada.</p>
    </item>
    <item>
      <p>Press the <gui style="button">+ Add User...</gui> button under
      <gui>Other Users</gui> to add a new user account.</p>
    </item>
    <item>
      <p>Se você quiser que o novo usuário tenha <link xref="user-admin-explain">acesso administrativo</link> ao computador, selecione <gui>Administrador</gui> para o tipo de conta.</p>
      <p>Administradores podem fazer coisas como adicionar e excluir usuários, instalar softwares e drivers, e alterar data e hora.</p>
    </item>
    <item>
      <p>Informe o nome completo do novo usuário. O nome de usuário será preenchido automaticamente com base no nome completo. Se você não gostar do proposto como nome de usuário, você pode alterá-lo.</p>
    </item>
    <item>
      <p>You can choose to set a password for the new user, or let them set it
      themselves on their first login. If you choose to set the password now,
      you can press the <gui style="button"><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16">
      <span its:translate="yes">generate password</span></media></gui> icon to
      automatically generate a random password.</p>
      <p>To connect the user to a network domain, click
      <gui>Enterprise Login</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Add</gui>. When the user has been added,
      <gui>Parental Controls</gui> and <gui>Language</gui> settings can be
      adjusted.</p>
    </item>
  </steps>

  <p>Se você deseja alterar a senha após criar a conta, selecione a conta, deslize para <gui style="button">Desbloquear</gui> o painel e pressione o atual status da senha atual.</p>

  <note>
    <p>No painel de <gui>Usuários</gui>, você pode clicar na imagem à direita do nome do usuário para definir uma imagem para a conta. Esta imagem será mostrada na janela de início de sessão. O sistema fornece algumas imagens predefinidas que você pode usar ou você pode selecionar uma foto sua ou tirar uma com sua webcam.</p>
  </note>

</page>
