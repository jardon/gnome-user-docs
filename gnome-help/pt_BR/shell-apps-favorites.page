<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="pt-BR">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione (ou remova) ícones de programas frequentemente usados ao dash.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Fixe seus aplicativos favoritos ao dash</title>

  <p>Para adicionar um aplicativo ao <link xref="shell-introduction#activities">dash</link> para acesso rápido:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> clicando em <gui>Atividades</gui> no canto superior esquerdo da tela</p>
      <p if:test="platform:gnome-classic">Clique no menu <gui xref="shell-introduction#activities">Aplicativos</gui> no canto superior esquerdo da tela e escolha o item do <gui>Panorama de atividades</gui> do menu.</p></item>
    <item>
      <p>Clique no botão de grade no dash e localize o aplicativo que você deseja adicionar.</p>
    </item>
    <item>
      <p>Clique com o botão direito no ícone do aplicativo e selecione <gui>Adicionar aos favoritos</gui>.</p>
      <p>Alternativamente, você pode clicar e arrastar o ícone ao dash.</p>
    </item>
  </steps>

  <p>Para remover um ícone de aplicativo do dash, clique com botão direito no ícone do aplicativo e selecione <gui>Remover dos favoritos</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Aplicativos favoritos também aparecem na seção <gui>Favoritos</gui> do menu <gui xref="shell-introduction#activities">Aplicativos</gui>.</p>
  </note>

</page>
