<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>Dicas como “Não deixe a carga da bateria ficar baixa demais”.</desc>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

<title>Fazendo melhor uso da bateria de seu notebook</title>

<p>Na medida em que baterias de notebooks envelhecem, elas pioram em termos de armazenamento de carga, e sua capacidade decresce gradualmente. Há algumas poucas técnicas que você pode usar para prolongar o tempo de vida útil da bateria, apesar de que você não deve esperar grandes diferenças.</p>

<list>
  <item>
    <p>Não deixe a bateria acabar por completo. Sempre recarregue-a <em>antes</em> de ficar muito baixa, apesar de que a maioria das baterias possuem “safeguards” embutidos para evitar que a ela atinja um nível muito baixo. Recarregar quando ela estiver apenas parcialmente descarregada é mais eficiente, mas recarregar quando ela está um pouco descarregada é pior para a bateria.</p>
  </item>
  <item>
    <p>O calor tem um efeito prejudicial na eficiência da carga da bateria. Não deixe a bateria ficar mais quente do que ela tem.</p>
  </item>
  <item>
    <p>Baterias envelhecem mesmo se você deixá-las guardadas. Há pouca vantagem em comprar uma bateria reserva ao mesmo tempo que você obtém a original — sempre compre reservas quando você precisa delas.</p>
  </item>
</list>

<note>
  <p>Essa sugestão se aplica especificamente a baterias de íon de lítio (Li-Ion), que é o tipo mais comum. Outros tipos de bateria podem se beneficiar de um tratamento diferente.</p>
</note>

</page>
