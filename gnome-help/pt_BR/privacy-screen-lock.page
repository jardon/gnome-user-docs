<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>
    <revision pkgversion="3.38.1" date="2020-11-22" status="final"/>
    <revision pkgversion="3.38.4" date="2020-03-07" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Evite que outras pessoas usem seu computador quanto você estiver distante dele.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Bloqueando automaticamente sua tela</title>
  
  <p>Ao se afastar do seu computador, você deveria <link xref="shell-exit#lock-screen">bloquear sua tela</link> para evitar que outras pessoas usem o seu computador e acessem seus arquivos. Se você algumas vezes se esquece de bloquear sua tela, você querer o bloqueio automática do seu computador após um período de tempo. Isso vai ajudar a manter segurança do seu computador, se você não está usando-o.</p>

  <note><p>Quando sua janela estiver bloqueada, os processos dos seus aplicativos e sistema vão continuar a executar, mas você precisará digitar sua senha para começar a usá-los novamente.</p></note>
  
  <steps>
    <title>Para definir um período de tempo até sua tela ser bloqueada automaticamente:</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Tela de bloqueio</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Tela de bloqueio</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Certifique-se de que <gui>Bloqueio de tela automático</gui> está ligado e selecione um período de tempo na lista suspensa <gui>Atraso para bloquear a tela automaticamente</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Applications can present notifications to you that are still displayed
    on your lock screen. This is convenient, for example, to see if you have
    any email without unlocking your screen. If you’re concerned about other
    people seeing these notifications, switch <gui>Lock Screen Notifications</gui>
    to off. For further notification settings, refer to <link xref="shell-notifications"/>.</p>
  </note>

  <p>Quando sua tela estiver bloqueada, e você quiser desbloqueá-la, pressione <key>Esc</key> ou arraste a tela de baixo para cima com o seu mouse. Então, digite sua senha e pressione <key>Enter</key> ou clique em <gui>Desbloquear</gui>. Alternativamente, comece a digitar sua senha e a cortina de bloqueio será realizado automaticamente na medida em que você digita.</p>

</page>
