<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="43" date="2022-09-10" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Copie ou mova itens para uma nova pasta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

<title>Copiando ou movendo arquivos e pastas</title>

 <p>Arquivos e pastas podem ser copiados ou movidos para um novo local arrastando e soltando com o mouse, usando os comandos copiar e colar, ou usando atalhos de teclado.</p>

 <p>Por exemplo, você pode querer copiar uma apresentação para um pendrive, para poder levá-la ao trabalho com você. Ou você poderia fazer um backup de um documento antes de fazer alterações nele (e, assim, usar a cópia antiga se não gostar das suas mudanças).</p>

 <p>Estas instruções se aplicam tanto a arquivos como a pastas. Você copia e move arquivos e pastas exatamente da mesma forma.</p>

<steps ui:expanded="false">
<title>Copiar e colar arquivos</title>
<item><p>Selecione o arquivo que queira copiar clicando nele uma vez.</p></item>
<item><p>Right-click and select <gui>Copy</gui>, or press
 <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Navegue para outra pasta, onde que você queira colocar a cópia do arquivo.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Recortar e colar arquivos para movê-los</title>
<item><p>Selecione o arquivo que queira mover, clicando nele uma vez.</p></item>
<item><p>Right-click and select <gui>Cut</gui>, or press
 <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Navegue para outra pasta, para onde você queira mover o arquivo.</p></item>
<item><p>Right-click and select <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Arrastar arquivos para copiar ou mover</title>
<item><p>Abra o gerenciador de arquivos e vá para a pasta que contenha o arquivo que queira copiar.</p></item>
<item><p>Press the menu button in the top-right corner of the window and select
 <gui style="menuitem">New Window</gui> (or
 press <keyseq><key>Ctrl</key><key>N</key></keyseq>) to open a second window. In
 the new window, navigate to the folder where you want to move or copy the file.
 </p></item>
<item>
 <p>Clique e arraste o arquivo de uma janela para outra. Isso irá <em>movê-lo</em> se o destino estiver no <em>mesmo</em> dispositivo, ou irá <em>copiá-lo</em> se o destino está em um dispositivo <em>diferente</em>.</p>
 <p>Por exemplo, se você arrastar um arquivo de um pendrive USB para sua pasta pessoal, ele será copiado, porque você está arrastando de um dispositivo para outro.</p>
 <p>Você pode forçar o arquivo a ser copiado se mantiver pressionada a tecla <key>Ctrl</key> enquanto arrasta o arquivo, ou forçá-lo a ser movido se mantiver pressionada a tecla <key>Shift</key> enquanto arrasta.</p>
 </item>
</steps>

<note>
  <p>Você não pode copiar ou mover um arquivo para uma pasta que seja de <em>somente-leitura</em>. Algumas pastas são assim para impedir que você faça alterações em seu conteúdo. Você pode alterar as coisas para deixarem de ser de somente-leitura <link xref="nautilus-file-properties-permissions">alterando as permissões de arquivo</link>.</p>
</note>

</page>
