<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Use seu telefone ou adaptador de Internet para se conectar a uma rede de banda larga móvel.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Conectando a uma banda larga móvel</title>

  <p>Você pode configurar uma conexão com uma rede de celular (3G) com o modem 3G embarcado do seu computador, seu telefone móvel ou um adaptador para computador.</p>

  <note style="tip">
  <p>A maioria dos telefones possui uma configuração chamada <link xref="net-tethering">ancoragem USB</link> que não requer configuração no computador e geralmente é o melhor método para se conectar à rede celular.</p>
  </note>

  <steps>
    <item><p>Se você não possui um modem 3G embarcado, conecte seu telefone ou o adaptador a uma porta USB em seu computador.</p>
    </item>
    <item>
    <p>Abra o <gui xref="shell-introduction#systemmenu">menu do sistema</gui> no lado direito da barra superior.</p>
  </item>
  <item>
    <p>Selecione <gui>Banda larga móvel desligada</gui>. A seção <gui>Banda larga</gui> do menu se expandirá.</p>
      <note>
        <p>Se <gui>Banda larga</gui> não aparecer no menu do sistema, certifique-se de que seu dispositivo não esteja definido como armazenamento de dados (em inglês, “mass storage”).</p>
      </note>
    </item>
    <item><p>Selecione <gui>Conectar</gui>. Se você está conectando pela primeira vez, o assistente <gui>Configurar uma conexão de banda larga móvel</gui> é iniciado. A tela de abertura exibe uma lista de informação necessária. Clique em <gui style="button">Próximo</gui>.</p></item>
    <item><p>Selecione o país ou região do seu provedor a partir da lista. Clique em <gui style="button">Próximo</gui>.</p></item>
    <item><p>Selecione o seu provedor a partir da lista. Clique em <gui style="button">Próximo</gui>.</p></item>
    <item><p>Selecione um plano de acordo com o tipo de dispositivo que você está conectando. Isso vai determinar o nome do ponto de acesso (APN). Clique em <gui style="button">Próximo</gui>.</p></item>
    <item><p>Confirme as configurações que você selecionou clicando em <gui style="button">Aplicar</gui>. O assistente vai fechar o painel de <gui>Rede</gui> vai exibir as propriedades da sua conexão.</p></item>
  </steps>

</page>
