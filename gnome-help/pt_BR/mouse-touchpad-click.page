<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>
    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Clique, arraste ou role usando toques e gestos em seu touchpad.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Clicando, arrastando ou rolagem com o touchpad</title>

  <p>Você pode clicar, dar um duplo clique, arrastar e rolar somente com seu touchpad, sem a necessidade de botões explícitos no hardware.</p>

  <note>
    <p><link xref="touchscreen-gestures">Gestos de touchscreen</link> são abordados separadamente.</p>
  </note>

<section id="tap">
  <title>Tocar para clicar</title>

  <media src="figures/touch-tap.svg" its:translate="no" style="floatend"/>

  <p>Você pode tocar no seu touchpad para clicar, ao invés de usar um botão.</p>

  <list>
    <item>
      <p>Para clicar, dê um toque no touchpad.</p>
    </item>
    <item>
      <p>Para duplo clique, dê dois toques.</p>
    </item>
    <item>
      <p>Para arrastar um item, dê dois toques sem levantar o seu dedo após o segundo toque. Arraste o item para onde você quiser e, então, levante o seu dedo para soltar o item.</p>
    </item>
    <item>
      <p>Se o seu mouse tiver suporte a multitoque, execute o clique com botão direito tocando com dois dedos de uma só vez. Do contrário, você ainda precisará usar os botões de hardware para fazer o clique com botão direito. Veja <link xref="a11y-right-click"/> por um método sem uso de um segundo botão de mouse.</p>
    </item>
    <item>
      <p>Se o seu touchpad tem suporte a multitoque, execute o <link xref="mouse-middleclick">clique com botão do meio</link> tocando com três dedos de uma só vez.</p>
    </item>
  </list>

  <note>
    <p>Ao tocar ou arrastar com múltiplos dedos, certifique-se de que seus dedos estão afastados o suficiente. Se eles estiverem muito perto, o seu computador pode achar que eles são um só dedo.</p>
  </note>

  <steps>
    <title>Habilitar Tocar para clicar</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Mouse &amp; touchpad</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Mouse &amp; touchpad</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Touchpad</gui>, certifique-se de que <gui>Touchpad</gui> está definido para ligado.</p>
      <note>
        <p>A seção <gui>Touchpad</gui> aparece apenas se seu sistema tiver um touchpad.</p>
      </note>
    </item>
   <item>
      <p>Ative <gui>Tocar para clicar</gui>.</p>
    </item>
  </steps>
</section>

<section id="twofingerscroll">
  <title>Rolagem com dois dedos</title>

  <media src="figures/touch-scroll.svg" its:translate="no" style="floatend"/>

  <p>Você pode usar rolagem no seu touchpad usando dois dedos.</p>

  <p>Quando essa opção está selecionada, tocar e arrastar com um dedo vai funcionar normalmente, mas se você arrastar dois dedos por qualquer parte do touchpad, estará executando rolagem. Mova seus dedos entre a parte superior e a inferior do seus touchpad para rolar para cima e para baixo, ou mova seus dedos horizontalmente para realizar rolagem horizontalmente. Tenha cuidado com o espaço entre os dedos. Se os seus dedos estiverem próximos demais, eles podem parecer simplesmente como um grande dedo para o seu touchpad.</p>

  <note>
    <p>Rolagem com dois dedo pode não funcionar em todos touchpads.</p>
  </note>

  <steps>
    <title>Habilitar Rolagem com dois dedos</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Mouse &amp; touchpad</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Mouse &amp; touchpad</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Touchpad</gui>, certifique-se de que <gui>Touchpad</gui> está definido para ligado.</p>
    </item>
    <item>
      <p>Defina <gui>Rolagem com dois dedos</gui> para ligado.</p>
    </item>
  </steps>
</section>

<section id="edgescroll">
  <title>Edge scroll</title>

  <media src="figures/touch-edge-scroll.svg" its:translate="no" style="floatend"/>

  <p>Use edge scroll if you want to scroll with only one finger.</p>

  <p>Your touchpad's specifications should give the exact
  location of the sensors for edge scrolling. Typically, the vertical
  scroll sensor is on a touchpad's right-hand side. The horizontal
  sensor is on the touchpad's bottom edge.</p>

  <p>To scroll vertically, drag your finger up and down the right-hand
  edge of the touchpad. To scroll horizontally, drag your finger across
  the bottom edge of the touchpad.</p>

  <note>
    <p>Edge scrolling may not work on all touchpads.</p>
  </note>

  <steps>
    <title>Enable Edge Scrolling</title>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Mouse &amp; touchpad</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Mouse &amp; touchpad</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Touchpad</gui>, certifique-se de que <gui>Touchpad</gui> está definido para ligado.</p>
    </item>
    <item>
      <p>Switch the <gui>Edge Scrolling</gui> switch to on.</p>
    </item>
  </steps>
</section>
 
<section id="contentsticks">
  <title>Rolagem natural</title>

  <p>Você pode arrastar um conteúdo como se deslizasse um pedaço de papel usando o touchpad.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Mouse &amp; touchpad</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Mouse &amp; touchpad</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Na seção <gui>Touchpad</gui>, certifique-se de que <gui>Touchpad</gui> está definido para ligado.</p>
    </item>
    <item>
      <p>Defina <gui>Rolagem natural</gui> para ligado.</p>
    </item>
  </steps>

  <note>
    <p>Este recurso também é conhecido como <em>Rolagem invertida</em>.</p>
  </note>

</section>

</page>
