<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-layouts" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="guide" xref="keyboard" group="i18n"/>

    <revision pkgversion="3.8" version="0.3" date="2013-04-30" status="review"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-08-20" status="review"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="author">
       <name>Julita Inca</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione disposições de teclado e alterne entre eles.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Usando disposições alternativas de teclado</title>

  <p>Teclados vêm em centenas de diferentes disposições para diferentes idiomas. Mesmo em um único idioma, frequentemente existem mais de uma disposição de teclado, como a Dvorak para o inglês ou BR-Nativo para o português do Brasil. Você pode fazer seu teclado se comportar como um com uma disposição de teclas diferente, independentemente das letras e símbolos impressas nas teclas. Isso é útil se você costumar alternar entre idiomas.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Configurações</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Teclado</gui> na barra lateral para abrir o painel.</p>
    </item>
    <item>
      <p>Clique no botão <gui>+</gui> na seção <gui>Fontes de entrada</gui>, selecione um idioma que esteja associado com a disposição e, então, selecione uma disposição e pressione <gui>Adicionar</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Se houver várias contas de usuário em seu sistema, há uma instância separada da região e do painel <gui>Região &amp; idioma</gui> para a tela de início de sessão. Clique no botão <gui>Tela de início de sessão</gui> no canto superior direito para alternar entre as duas instâncias.</p>

    <p>Algumas variantes de layout de teclado raramente usadas não estão disponíveis por padrão quando você clica no botão <gui>+</gui>. Para disponibilizar também essas fontes de entrada, você pode abrir uma janela de terminal pressionando <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq> e executar este comando:</p>
    <p><cmd its:translate="no">gsettings set org.gnome.desktop.input-sources
    show-all-sources true</cmd></p>
  </note>

  <note style="sidebar">
    <p>Você pode visualizar uma imagem de qualquer disposição selecionando-a na lista de <gui>Fontes de entrada</gui> e clicando em <gui><media its:translate="no" type="image" src="figures/input-keyboard-symbolic.png" width="16" height="16"><span its:translate="yes">visualizar</span></media></gui></p>
  </note>

  <p>Alguns idiomas oferecem algumas opções extras de configuração. Você pode identificar esses idiomas porque eles têm uma <gui><media its:translate="no" type="image" src="figures/system-run-symbolic.svg" width="16" height="16"><span its:translate="yes">visualizar</span></media></gui> de um ícone retratado. Se você deseja acessar alguns desses parâmetros extras, selecione o idioma na lista de <gui>Fontes de entrada</gui> e um novo botão de <gui style="button"><media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg" width="16" height="16"><span its:translate="yes">preferências</span></media></gui> vai dar a você acesso às configurações extras.</p>

  <p>Quando se usa mais de uma disposição de teclado, você pode escolher ter todas as janelas usando a mesma disposição ou definir uma diferente para cada janela. O uso de disposições de teclado diferentes para cada janela é útil para, por exemplo, escrever um artigo em um outro idioma em uma janela de um processador de textos. Sua seleção de teclado será lembrada para cada janela à medida em que você alternar entre janelas. Pressione o botão <gui style="button">Opções</gui> para selecione como você deseja lidar com múltiplas disposições.</p>

  <p>A barra superior vai exibir um pequeno identificador para a disposição atual, como <gui>en</gui> para a disposição em inglês padrão. Clique no indicador de disposição e selecione a disposição que você desejar usar do menu. Se o idioma selecionado possui algumas definições extras, estas serão mostradas embaixo da lista de disposições disponíveis. Isso permite que você tenha uma visão geral das suas definições. Você também pode abrir uma imagem com a disposição de teclado atual como referência.</p>

  <p>A forma mais rápida de alterar para outra disposição é usar os <gui>Atalhos de teclado</gui> de <gui>Fontes de entrada</gui>. Estes atalhos abrem o seletor de <gui>Fontes de entrada</gui>, no qual você pode mover para cima ou para baixo. Por padrão, você pode alternar para a próxima fonte de entrada com <keyseq><key xref="keyboard-key-super">Super</key><key>Espaço</key></keyseq> e para o anterior com <keyseq><key>Shift</key><key>Super</key> <key>Espaço</key></keyseq>. Você pode alterar esses atalhos nas configurações de <gui>Teclado</gui> em <guiseq><gui>Atalhos de teclado</gui><gui>Personalizar atalhos</gui><gui>Digitação</gui></guiseq>.</p>

  <p><media type="image" src="figures/input-methods-switcher.png"/></p>

</page>
