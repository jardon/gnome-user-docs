<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-personal" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.1" date="2013-09-23" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permita que outras pessoas acessem arquivos em sua pasta <file>Público</file>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Compartilhando seus arquivos pessoais</title>

  <p>Você pode permitir acesso a sua pasta <file>Público</file> no seu diretório <file>Home</file> (pasta pessoal) a outros computadores da rede. Configure <gui>Compartilhamento de arquivos pessoais</gui> para permitir que outras pessoas acessem o conteúdo da pasta.</p>

  <note style="info package">
    <p>Você deve ter o pacote <app>gnome-user-share</app> instalado para que <gui>Compartilhamento de arquivos pessoais</gui> esteja visível.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-user-share" style="button">Instalar gnome-user-share</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Compartilhamento</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Compartilhamento</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Se o alternador <gui>Compartilhamento</gui> no canto superior direito da janela estiver desligada, ligue-a.</p>

      <note style="info"><p>If the text below <gui>Device Name</gui> allows
      you to edit it, you can <link xref="about-hostname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Selecione <gui>Compartilhamento de arquivos pessoais</gui>.</p>
    </item>
    <item>
      <p>Alterne <gui>Compartilhamento de arquivos pessoais</gui> para ligada. Isso significa que outras pessoas poderão tentar se conectar a seu computador e acessar arquivos em sua pasta <file>Público</file>.</p>
      <note style="info">
        <p>Uma <em>URI</em> é exibida pela qual sua pasta <file>Público</file> pode ser acessada a partir de outros computadores na rede.</p>
      </note>
    </item>
  </steps>

  <section id="security">
  <title>Segurança</title>

  <terms>
    <item>
      <title>Exigir senha</title>
      <p>Para exigir que outras pessoas usem uma senha ao acessar sua pasta <file>Público</file>, alterne <gui>Exigir senha</gui> para ligado. Se você não usar essa opção, qualquer um pode tentar ver sua pasta <file>Público</file>.</p>
      <note style="tip">
        <p>Essa opção é desabilitada por padrão, mas você deveria habilitá-la e definir uma senha segura.</p>
      </note>
    </item>
  </terms>
  </section>

  <section id="networks">
  <title>Redes</title>

  <p>A seção <gui>Redes</gui> lista as redes às quais você está atualmente conectado. Use o alternador próximo a cada um para escolher onde seus arquivo pessoais podem ser compartilhados.</p>

  </section>

</page>
