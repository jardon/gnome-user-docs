<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hostname" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="about" group="hostname"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the name used to identify your system on the network and to
    Bluetooth devices.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Change the device name</title>

  <p>Having a name that is uniquely recognisable makes it easier to identify
  your system on the network and when pairing Bluetooth devices.</p>

  <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Sobre</gui>.</p>
    </item>
    <item>
      <p>Press <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Select <gui>Device Name</gui> from the list.</p>
    </item>
    <item>
      <p>Enter a name for your system, and click
      <gui style="button">Rename</gui>.</p>
    </item>
  </steps>

  <note style="important">
    <p>Although the hostname is changed immediately after the system name has
    been changed, some running programs and services may have to be restarted
    for the change to take effect.</p>
  </note>

</page>
