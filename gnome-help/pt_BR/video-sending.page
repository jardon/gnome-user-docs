<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="video-sending" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#music"/>

    <revision pkgversion="3.17.90" date="2015-08-30" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifique se eles têm os codecs corretos de vídeo instalados.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Outras pessoas não conseguem reproduzir os vídeos que eu fiz</title>

  <p>Se você fez um vídeo no seu computador Linux e o enviou para alguém usando Windows ou Mac OS, você pode acabar descobrindo que essa pessoa tem problemas em reproduzir o vídeo.</p>

  <p>Para ser capaz de ver seu vídeo, a pessoa a quem você o enviou deve ter os <em>codecs</em> corretos instalados. Um codec é um programa que sabe como pegar o vídeo e exibi-lo na tela. Existem muitos formatos diferentes de vídeo, e cada um exige um codec diferente para reproduzi-lo. Você pode conferir qual é o formato do seu vídeo assim:</p>

  <list>
    <item>
      <p>Abra <app>Arquivos</app> no panorama de <gui xref="shell-introduction#activities">Atividades</gui>.</p>
    </item>
    <item>
      <p>Clique com o botão direito no arquivo de vídeo e selecione <gui>Propriedades</gui>.</p>
    </item>
    <item>
      <p>Vá para a aba <gui>Áudio/vídeo</gui> ou <gui>Vídeo</gui> e veja qual <gui>Codec</gui> está listado em <gui>Vídeo</gui> e <gui>Áudio</gui> (se o vídeo também possuir áudio).</p>
    </item>
  </list>

  <p>Pergunte à pessoa com problemas na reprodução se ela tem o codec correto instalado. Ela pode achar útil buscar na Internet pelo nome do codec mais o nome do aplicativo de vídeo dele. Por exemplo, se seu vídeo usa o formato <em>Theora</em>, e você tem um amigo que usa Windows Media Player para tentar assisti-lo, busque por “theora windows media player”. É bem comum você ser capaz de baixar o codec correto de graça se ele não estiver instalado.</p>

  <p>Se não conseguir encontrar o codec correto, experimente o <link href="http://www.videolan.org/vlc/">Reprodutor de Mídias VLC</link>. Ele funciona no Windows, Mac OS e Linux, além de oferecer suporte a muitos formatos de vídeo. Não obtendo sucesso, tente converter seu vídeo para um formato diferente. A maioria dos editores de vídeo é capaz disso, e aplicativos específicos para conversão de vídeo estão disponíveis. Verifique o aplicativo instalador de programas para ver o que há disponível.</p>

  <note>
    <p>Existem alguns poucos problemas que poderiam impedir alguém de reproduzir seu vídeo. O vídeo pode ter sido danificado no envio para eles (às vezes, arquivos grandes não são copiados perfeitamente), eles poderiam ter problemas com o aplicativo deles ou o vídeo pode não ter sido criado apropriadamente (teriam ocorrido alguns erros quando você o salvou).</p>
  </note>

</page>
