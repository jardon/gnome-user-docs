<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sound-volume" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#sound" group="#first"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-03-02"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Configure o volume sonoro para o computador e controle a sonoridade de cada aplicativo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

<title>Alterando o volume do som</title>

  <p>Para alterar o volume do som, abra o <gui xref="shell-introduction#systemmenu">menu do sistema</gui> no lado direito na barra superior e mova o controle deslizante de volume para esquerda ou direita. Você pode desligar completamente arrastando-o completamente para a esquerda.</p>
 
  <note style="tip">
    <p>If you hover over the volume icon in the top bar or the slider in the
    system menu, the volume can be controlled by scrolling the mouse wheel or
    touchpad.</p>
  </note>

  <p>Alguns teclados têm teclas que lhe permitem controlar o volume. Normalmente elas se parecem com alto-falantes estilizados com ondas saindo deles. Elas costumam ficar próximas às teclas “F” (teclas de função) na parte superior. Em teclados de notebooks, ficam frequentemente nas teclas “F”. Mantenha pressionada a tecla <key>Fn</key> do teclado para usá-las.</p>

  <p>Se você tem alto-falantes externos, você também pode alterar o volume usando o controle de volume dos alto-falantes. Alguns fones de ouvido têm um controle de volume próprio também.</p>

<section id="apps">
 <title>Alterando o volume sonoro para aplicativos individuais</title>

  <p>Você pode alterar o volume de um aplicativo e deixar o volume inalterado para outros. Isso é útil se você estiver ouvindo música e navegando pela web, por exemplo. Você pode desligar o áudio no navegador de forma que os sons de sites não interrompam a música.</p>

  <p>Alguns aplicativos possuem controles de volume nas janelas principais. Se seu aplicativo possui seu controle de volume, use aquele para alterar o volume. Se não:</p>

    <steps>
    <item>
      <p>Abra o panorama de <gui xref="shell-introduction#activities">Atividades</gui> e comece a digitar <gui>Som</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Som</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Under <gui>Volume Levels</gui>, use the volume slider for each
      application to set its volume. Click the speaker button at the end of the
      slider to mute or unmute the application.</p>

  <note style="tip">
    <p>Only applications that are playing sounds are listed. If an
    application is playing sounds but is not listed, it might not support the
    feature that lets you control its volume in this way. In such a case, you
    cannot change its volume.</p>
  </note>
    </item>

  </steps>

</section>

</page>
