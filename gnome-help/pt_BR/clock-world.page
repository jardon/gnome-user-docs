<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index"/>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.28" date="2018-07-30" status="review"/>
    <revision pkgversion="3.37" date="2020-08-06" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Exibe horas em outras cidades sob a agenda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Adicionando um relógio mundial</title>

  <p>Use <app>Relógios</app> para adicionar horas em outras cidades.</p>

  <note>
    <p>Isso exige que o aplicativo <app>Relógios</app> esteja instalado no seu computador.</p>
    <p>A maioria das distribuições Linux vem com <app>Relógios</app> instalado por padrão. Se este não for o caso da sua, você pode precisar instalá-lo usando o gerenciador de pacotes da sua distribuição.</p>
  </note>

  <p>Para adicionar um relógio mundial:</p>

  <steps>
    <item>
      <p>Clique no relógio na barra superior.</p>
    </item>
    <item>
      <p>Clique no botão <gui>Adicionar relógios mundiais…</gui> sob a agenda para iniciar <app>Relógios</app>.</p>

    <note>
       <p>Se você já possui um ou mais relógios mundiais, clique em um e <app>Relógios</app> será iniciado.</p>
    </note>

    </item>
    <item>
      <p>Na janela <app>Relógios</app>, clique em <gui style="button">+</gui> ou pressione <keyseq><key>Ctrl</key><key>N</key></keyseq> para adicionar uma nova cidade.</p>
    </item>
    <item>
      <p>Comece digitando o nome da cidade na pesquisa.</p>
    </item>
    <item>
      <p>Selecione a cidade correta ou a localização mais próxima a você na lista.</p>
    </item>
    <item>
      <p>Pressione <gui style="button">Adicionar</gui> para finalizar a adição da cidade.</p>
    </item>
  </steps>

  <p>Consulte <link href="help:gnome-clocks">Ajuda do Relógios</link> para mais informação sobre as capacidades do <app>Relógios</app>.</p>

</page>
