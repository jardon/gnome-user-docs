<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione, exclua ou renomeie marcadores no gerenciador de arquivos.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2021.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2022.</mal:years>
    </mal:credit>
  </info>

  <title>Editando marcadores de pastas</title>

  <p>Seus marcadores estão listados na barra lateral do gerenciador de arquivos.</p>

  <steps>
    <title>Adicionar um marcador:</title>
    <item>
      <p>Abra a pasta (ou local) que você deseja marcar.</p>
    </item>
    <item>
      <p>Clique na pasta atual na barra de caminho e selecione <gui style="menuitem">Adicionar marcador</gui>.</p>
      <note>
        <p>Você também pode arrastar uma pasta para a barra lateral e soltá-la sobre <gui>Novo marcador</gui>, o que aparece dinamicamente.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Excluir um marcador:</title>
    <item>
      <p>Clique com o botão direito no marcador na barra lateral e selecione <gui>Remover</gui> do menu.</p>
    </item>
  </steps>

  <steps>
    <title>Renomear um marcador:</title>
    <item>
      <p>Clique com o botão direito no marcador na barra lateral e selecione <gui>Renomear…</gui>.</p>
    </item>
    <item>
      <p>Na caixa de texto <gui>Nome</gui>, digite o novo nome do marcador.</p>
      <note>
        <p>Renomear um marcador não significa renomear a pasta. Se você tem marcadores para duas pastas diferentes em dois locais diferentes, mas ambas têm o mesmo nome, os marcadores terão o mesmo nome e você não poderá distinguir um do outro. Nestes casos, é útil dar ao marcador um nome diferente do nome da pasta para a qual ele aponta.</p>
      </note>
    </item>
  </steps>

</page>
