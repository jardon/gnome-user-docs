<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-noconnection" xml:lang="gl">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" version="0.2" date="2013-11-11" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Comprobe dúas veces o contrasinal, e outras cousas que probar.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

<title>Escribín o contrasinal correcto, pero sigo sen poder conectarme</title>

<p>If you’re sure that you entered the correct <link xref="net-wireless-wepwpa">wireless password</link> but you still can’t successfully connect to a wireless network, try some of the following:</p>

<list>
 <item>
  <p>Comprobe dúas veces que introduciu correctamente o contrasinal</p>
  <p>Os contrasinais son sensíbeis a capitalización (quere dicir que distinguen entre maiúsculas e minúsculas), de tal forma que se non usou unha maiúscula onde debería facelo, unha das letras é incorrecta.</p>
 </item>

 <item>
  <p>Tente a chave hexadecimal ou ASCII</p>
  <p>O contrasinal que escribiu tamén pode representarse dunha forma distinta — como unha cadea de caracteres en hexadecimal (números 0-9 e letras a-f) chamado a chave de paso. Cada contrasinal ten unha frase de paso equivalente. Se ten acceso á súa frase de paso así como do contrasinal, tente escribir a frase de paso no lugar. Asegúrese de que selecciona a correcta <gui>seguranza sen fíos</gui> cando se lle pregunte polo seu contrasinal (por exemplo, seleccione <gui>chave WEP 40/128-bit</gui> se está escribindo unha chave de paso de 40 caracteres para unha conexión cifrada por WEP).</p>
 </item>

 <item>
  <p>Tente apagar a súa tarxeta sen fíos e volvela a acender</p>
  <p>Algunhas tarxetas de rede bloquéanse ou experimentan un problema menor o que significa que non se conectan. Tente apagar a tarxeta e logo acéndaa de novo para reiniciala — vexa <link xref="net-wireless-troubleshooting"/> para obter máis información.</p>
 </item>

 <item>
  <p>Comprobe que está usando o tipo correcto de seguranza sen fíos</p>
  <p>Cando se lle pregunte polo contrasinal de seguranza da rede, pode escoller o tipo de seguranza sen fíos a usar. Asegúrese que seleccionou o que usa o enrutador ou a estación base sen fíos. Isto debería seleccionarse por omisión, pero a veces pode que non sexa así por calquera razón. Se non sabe cal escoller, use a proba e erro con todas as opcións.</p>
 </item>

 <item>
  <p>Comprobe que a súa tarxeta sen fíos é completamente compatíbel</p>
  <p>Algunhas tarxetas de rede sen fíos non son compatíbeis de todo. Mostran unha conexión sen fíos, pero non poden conectarse á rede porque os seus controladores non poden facelo. Vexa se pode obter un controlador sen fíos alternativo, ou se precisa levar a cabo algunha configuración adicional (como instalar un <em>firmware</em> diferente). Vexa <link xref="net-wireless-troubleshooting"/> para obter máis información.</p>
 </item>

</list>

</page>
