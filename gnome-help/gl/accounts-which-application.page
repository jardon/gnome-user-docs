<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-which-application" xml:lang="gl">

  <info>
    <link type="guide" xref="accounts"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.8.2" date="2013-05-22" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="incomplete"/>

    <credit type="author copyright">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
      <years>2012, 2013</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>As aplicacións que poden usar as contas creadas en <app>Contas en liña</app> e os servizos que poden usar.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Servizos en liña e aplicacións</title>

  <p>En canto engada unha conta en liña, calquera aplicación poderá usar a conta para calquera dos servizos dispoñíbeis que non <link xref="accounts-disable-service">desactivara</link>. Diferentes fornecedores fornecen distintos servizos. Esta páxina mostra os distintos servizos e algúns das aplicacións que os usan.</p>

  <terms>
    <item>
      <title>Calendario</title>
      <p>O servizo de Calendario permítelle ver, engadir e editar eventos nun calendario en liña. É usado por aplicacións como <app>Calendario</app>, <app>Evolution</app> e <app>California</app>.</p>
    </item>

    <item>
      <title>Conversas</title>
      <p>O servizo de chat permítelle falar cos seus contactos en plataformas de mensaxaría instantánea populares. É usado pola aplicación <app>Empathy</app>.</p>
    </item>

    <item>
      <title>Contactos</title>
      <p>O servizo de Contactos permítelle ver os detalles publicados dos seus contactos en varios servizos. É usado por aplicacións como <app>Contactos</app> e <app>Evolution</app>.</p>
    </item>

<!-- See https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/commit/32f35cc07fcbee839978e3630972b67ed55f0da6
    <item>
      <title>Documents</title>
      <p>The Documents service allows you to view your online documents
      such as those in Google docs. You can view your documents using the
      <app>Documents</app> application.</p>
    </item>
-->
    <item>
      <title>Ficheiros</title>
      <p>O servizo de Ficheiros engade unha localización de ficheiro remota, como se ten que engadir unha usando a funcionalidade <link xref="nautilus-connect">Conectarse ao servidor</link> nun xestor de ficheiros. Pode acceder a ficheiros remotos usndo o xestor de ficheiros, así como mediante os diálogos de abrir e gardar ficheiros nunha aplicación.</p>
    </item>

    <item>
      <title>Correo electrónico</title>
      <p>O servizo de Correo permítelle enviar e recibir correo electrónico mediante un fornecedor de correo como Google. É usado por <app>Evolution</app>.</p>
    </item>

<!-- TODO: Not sure what this does. Doesn't seem to do anything in Maps app.
    <item>
      <title>Maps</title>
    </item>
-->
<!-- TODO: Not sure what this does in which app. Seems to support e.g. VKontakte.
    <item>
      <title>Music</title>
    </item>
-->
    <item>
      <title>Fotos</title>
      <p>O servizo de Fotos permítelle ver as súas fotos en liña como as enviadas a Facebook. Pode ver as súas fotos usando a aplicación <app>Fotos</app>.</p>
    </item>

    <item>
      <title>Impresoras</title>
      <p>O servizo de Impresoras permítelle enviar unha copia dun PDF a un fornecedor desde o diálogo de impresión de moitas aplicacións. O fornecedor pode fornecer servizos de impresión, ou pode servir como almacenamento do PDF, que pode descargalo e imprimilo máis tarde.</p>
    </item>

  </terms>

</page>
