<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="tips-specialchars" xml:lang="gl">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.26" date="2017-11-27" status="review"/>
    <revision version="gnome:40" date="2021-03-02" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Escriba os caracteres que non se encontran no teclado, incluíndo alfabetos estranxeiros, símbolos matemáticos e «dingbats».</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Inserir caracteres especiais</title>

  <p>Pode introducir e ver miles de caracteres da maioría dos sistemas de escritura do mundo, incluso aqueles que non se atopan no seu teclado. Esta páxina enumera algunhas formas diferentes de introducir caracteres especiais.</p>

  <links type="section">
    <title>Métodos para inserir caracteres</title>
  </links>

  <section id="characters">
    <title>Mapa de caracteres</title>
    <p>A aplicación de mapas de personaxes permítelle atopar e inserir caracteres pouco comúns, incluídos os emojis, navegando por categorías de personaxes ou buscando palabras clave.</p>

    <p>Inicie <app>Caracteres</app> desde a vista de Actividades.</p>

  </section>

  <section id="emoji">
    <title>Emoticona</title>
    <steps>
      <title>Inserir emoticona</title>
      <item>
        <p>Prema <keyseq><key>Ctrl</key><key>;</key></keyseq>.</p>
      </item>
      <item>
        <p>Explore as categorías na parte inferior do diálogo e comece a escribir unha descrición no campo de busca.</p>
      </item>
      <item>
        <p>Seleccionar unha emoticona para inserila.</p>
      </item>
    </steps>
  </section>

  <section id="compose">
    <title>Tecla Compose</title>
    <p>Unha chave composta é unha chave especial que lle permite premer varias teclas seguidas para obter un caracter especial. Por exemplo para usar a letra con tilde <em>é</em> pode premer <key>compoñer</key> despois <key>'</key> e despois <key>e</key>.</p>
    <p>Os teclados non teñen teclas de composición específicas. No seu lugar, pode definir unha das teclas existentes no seu teclado, como tecla para compoñer.</p>

    <steps>
      <title>Definir unha tecla composta</title>
      <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Preferencias</gui>.</p>
      </item>
      <item>
        <p>Prema en <gui>Preferencias</gui>.</p>
      </item>
      <item>
        <p>Prema <gui>Teclado</gui> na barra lateral para abrir o panel.</p>
      </item>
      <item>
        <p>Na sección <gui>Escriba caracteres especiais</gui>, faga clic en <gui>Tecla Compose</gui>.</p>
      </item>
      <item>
        <p>Active o trocado da <gui>Tecla Compose</gui>.</p>
      </item>
      <item>
        <p>Marque a caixa de verificación da tecla que quere usar como tecla de Composición.</p>
      </item>
      <item>
        <p>Pechar a xanela de configuración da rede.</p>
      </item>
    </steps>

    <p>Pode teclear moitos caracteres comúns usando a tecla composta, por exemplo:</p>

    <list>
      <item><p>Prema <key>compoñer</key> e logo <key>'</key> e logo a tecla para estabelecer a tilde en tal letra, tal como é o <em>é</em>.</p></item>
      <item><p>Prema <key>compoñer</key> despois <key>`</key> despois unha letra para estabelecer un acento grave sobre tal letra, tal como <em>è</em>.</p></item>
      <item><p>Prema <key>compoñer</key> despois <key>"</key> despois unha letra para estabelecer unha diérese en tal letra, tal como o <em>ë</em>.</p></item>
      <item><p>Prema <key>compoñer</key> despois <key>-</key> despois unha letra para estabelecer un macrón sobre a letra, tal como o <em>ē</em>.</p></item>
    </list>
    <p>Para obter máis secuencias de teclas de composición, consulte a <link href="https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations">a páxina de teclas de composición na Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Puntos de código</title>

  <p>Pode introducir calquera carácter Unicode usando só o teclado co punto de código numérico do carácter. Cada carácter identifícase cun punto de código de catro caracteres. Para atopar o punto de código dun carácter, busque o carácter na aplicación <app>Carácteres</app>. O punto de código son os catro caracteres que hai despois de <gui>U+</gui>.</p>

  <p>To enter a character by its code point, press
  <keyseq><key>Ctrl</key><key>Shift</key><key>U</key></keyseq>, then type the
  four-character code and press <key>Space</key> or <key>Enter</key>. If you often
  use characters that you can’t easily access with other methods, you might find
  it useful to memorize the code point for those characters so you can enter
  them quickly.</p>

</section>

  <section id="layout">
    <title>Disposicións de teclado</title>
    <p>Pode facer que o teclado se comporte como o teclado doutro idioma, independentemente das teclas impresas nas teclas. Incluso pode cambiar facilmente entre diferentes distribucións de teclado cunha icona na barra superior. Para saber como facelo, consulte a <link xref="keyboard-layouts"/>.</p>
  </section>

<section id="im">
  <title>Métodos de entrada</title>

  <p>An Input Method expands the previous methods by allowing to enter
  characters not only with keyboard but also any input devices. For instance
  you could enter characters with a mouse using a gesture method, or enter
  Japanese characters using a Latin keyboard.</p>

  <p>Para escoller un método de entrada, prema co botón dereito sobre un widget de texto e, no menú <gui>Método de entrada</gui>, seleccione o método de entrada que quere usar. Non hai ningún predeterminado, polo que pode consultar a documentación dos métodos de entrada para saber como usalos.</p>

</section>

</page>
