<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-night-light" xml:lang="gl">
  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="40.1" date="2021-06-09" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2018</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A luz nocturna cambia a cor das súas pantallas segundo a hora do día.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Axuste a temperatura da cor da súa pantalla</title>

  <p>O monitor dun computador emite luz azul que contribúe á perda de sono e fatiga visual cando escurece. <gui>Luz nocturna</gui> cambia de cor das súas pantallas segundo a hora do día, facendo o cor máis quente contra a noite. Para activar <gui>Luz ncturna</gui>:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Pantallas</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Pantallas</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>Faga clic en <gui>Luz nocturna</gui> para abrir as preferencias.</p>
    </item>
    <item>
      <p>Asegure que está activo <gui>Luz nocturna</gui>.</p>
    </item>
    <item>
      <p>En <gui>Programar</gui>, selecciona <gui>Puesta do sol ao amencer</gui> para que a cor da pantalla siga as horas do solpor e do amencer da túa localización. Seleccione <gui>Programación manual</gui> para configurar os <gui>Horas</gui> nunha programación personalizada.</p>
    </item>
    <item>
      <p>Use o deslizador para axustar a <gui>Temperatura da cor</gui> para que sexa máis ou menos quente.</p>
    </item>
  </steps>
      <note>
        <p>A <link xref="shell-introduction">barra superior</link> mostra cando <gui>Luz nocturna</gui> está activo. Pode desactivarse temporalmente desde o menú do sistema.</p>
      </note>



</page>
