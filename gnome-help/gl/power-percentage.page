<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-percentage" xml:lang="gl">

  <info>

    <link type="guide" xref="power"/>
    <link type="guide" xref="status-icons"/>
    <link type="seealso" xref="power-status"/>

    <revision version="gnome:40" date="2021-03-21" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mostrar o porcentaxe da batería na barra superior.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>
  
  <title>Mostrar o estado da batería como un porcentaxe</title>

  <p>A <link xref="status-icons#batteryicons">icona de estado</link> na barra superior mostra o nivel de carga da batería interna principal e se está cargando ou non. Tamén pode mostrar a carga como unha <link xref="power-percentage">porcentaxe</link>.</p>

  <steps>

    <title>Mostrar o porcentaxe de batería na barra superior</title>

    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Enerxía</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Enerxía</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>In the <gui>Suspend &amp; Power Button</gui> section, set <gui>Show
      Battery Percentage</gui> to on.</p>
    </item>

  </steps>

</page>
