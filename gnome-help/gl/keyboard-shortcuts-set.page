<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="gl">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Definir ou cambiar atallos de teclado na configuración do <gui>Teclado</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Atallos de teclado útiles</title>

<p>Para cambiar as teclas que premer para un atallo de teclado:</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Preferencias</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Preferencias</gui>.</p>
    </item>
    <item>
      <p>Prema <gui>Teclado</gui> na barra lateral para abrir o panel.</p>
    </item>
    <item>
      <p>Na sección <gui>Atallos de teclado</gui>, seleccione <gui>Personalizar atallos</gui>.</p>
    </item>
    <item>
      <p>Seleccione a categoría desexada ou escriba un termo de busca.</p>
    </item>
    <item>
      <p>Prema a fila da acción desexada. Mostrarase a xanela <gui>Estabelecer atallo</gui>.</p>
    </item>
    <item>
      <p>Manteña premida a combinación de teclas desexada ou prema <key>Retroceso</key> para restablecer ou prema <key>Esc</key> para cancelar.</p>
    </item>
  </steps>


<section id="defined">
<title>Atallos de edición comúns</title>
  <p>Hai unha serie de atallos preconfigurados que se poden cambiar, agrupados nestas categorías:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accesibilidade</title>
  <tr>
	<td><p>Incrementar o tamaño do texto</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Contraste alto activado ou desactivado</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Incrementar o tamaño do texto</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Activar ou desactivar o teclado en pantalla</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Apagar ou acender o Bluetooth</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Apagar ou acender o Bluetooth</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ampliar</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Reducir</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Iniciadores</title>
  <tr>
	<td><p>Cartafol persoal</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> ou <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> ou <key>Explorador</key></p></td>
  </tr>
  <tr>
	<td><p>Iniciar a calculadora</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> ou <key>Calculadora</key></p></td>
  </tr>
  <tr>
	<td><p>Iniciar o cliente de correo-e</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> ou <key>Correo electrónico</key></p></td>
  </tr>
  <tr>
	<td><p>Iniciar o visor de axuda</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Iniciar o navegador web</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> ou <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> ou <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>Buscar</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> ou <key>Busca</key></p></td>
  </tr>
  <tr>
	<td><p>Preferencias</p></td>
	<td><p><key>Ferramentas</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Calibración</title>
  <tr>
	<td><p>Buscar unha xanela perdida</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Mover ao espazo de traballo da esquerda</p></td>
	<td><p><keyseq><key>Super</key><key>RePáx</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover ao espazo de traballo da dereita</p></td>
	<td><p><keyseq><key>Super</key><key>Av Páx</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mova a xanela ao monitor de abaixo</p></td>
	<td><p><keyseq><key>Maiús</key><key xref="keyboard-key-super">Super</key> <key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mova a xanela ao monitor da esquerda</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mova a xanela ao monitor da dereita</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mova a xanela ao monitor de arriba</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a xanela ao espazo de traballo da esquerda</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key> <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover xanela un espazo de traballo á dereita</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>Av Páx</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover xanela ao último espazo de traballo</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>Fin</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a xanela ao espazo de traballo 1</p></td>
	<td><p><keyseq><key>Maiús</key><key>Super</key><key>Inicio</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mover a xanela ao espazo de traballo 2</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Mover a xanela ao espazo de traballo 3</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Mover a xanela ao espazo de traballo 4</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Outras aplicacións</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar os controis do sistema</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar entre controis do sistema directamente</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar ao último espazo de traballo</p></td>
	<td><p><keyseq><key>Super</key><key>Fin</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar ao espazo de traballo 1</p></td>
	<td><p><keyseq><key>Super</key><key>Inicio</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar ao espazo de traballo 2</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Cambiar ao espazo de traballo 3</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Cambiar ao espazo de traballo 4</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
        <td><p>Trocar entre as xanelas</p></td>
        <td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Trocar entre as xanelas</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Cambiar entre xanelas dunha aplicación directametne</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar xanelas dunha aplicación</p></td>
	<td><p>Desactivado</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Facer unha captura de pantalla</title>
  <tr>
	<td><p>Garda unha captura de pantalla dunha xanela</p></td>
	<td><p><keyseq><key>Alt</key><key>Imp</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Garda unha captura de pantalla de toda a pantalla</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Inicie a ferramenta de captura de pantalla</p></td>
	<td><p><key>Impr</key></p></td>
  </tr>
  <tr>
        <td><p>Gravar un «screencast» curto</p></td>
        <td><p><keyseq><key>Maíus</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Son e medios</title>
  <tr>
	<td><p>Expulsar</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Expulsar)</p></td>
  </tr>
  <tr>
	<td><p>Música e reprodutores</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Multimedia son)</p></td>
  </tr>
  <tr>
	<td><p>Micrófono enmudecido/desenmudecido</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>Seguinte pista</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Anterior son)</p></td>
  </tr>
  <tr>
	<td><p>Pausar a reprodución</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Pausar audio)</p></td>
  </tr>
  <tr>
	<td><p>Reproducir (ou reproducir/pausar)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Reproducir son)</p></td>
  </tr>
  <tr>
	<td><p>Pista anterior</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Seguinte son)</p></td>
  </tr>
  <tr>
	<td><p>Deter a reprodución</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Deter son)</p></td>
  </tr>
  <tr>
	<td><p>Volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Baixar o volume)</p></td>
  </tr>
  <tr>
	<td><p>Volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Silenciar son)</p></td>
  </tr>
  <tr>
	<td><p>Volume</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Subir o volume)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Sistema</title>
  <tr>
        <td><p>Enfocar a notificación activa</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Bloquear a pantalla</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar o diálogo de Apagar</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Supr</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Abrir o menú de aplicación</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restaurar os atallos de teclado</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Mostrar todas as aplicacións</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vista previa de actividades</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar a lista de notificacións</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar na vista xeral</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Mostrar o diálogo de executar orde</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Escritura</title>
  <tr>
  <td><p>Cambiar á seguinte orixe de entrada</p></td>
  <td><p><keyseq><key>Super</key><key>Espacio</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Cambiar ao orixe de entrada anterior</p></td>
  <td><p><keyseq><key>Maiús</key><key>Super</key><key>Espacio</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Compartido por Windows</title>
  <tr>
	<td><p>Activar o menú da xanela</p></td>
	<td><p><keyseq><key>Alt</key><key>Espacio</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Redimensionar unha xanela</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Ocultar xanela</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Traballando coas xanelas</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Redimensionar unha xanela</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximizar a xanela horizontalmente</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Maximizar a xanela verticalmente</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Redimensionar unha xanela</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Buscar unha xanela perdida</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Elevar a xanela se está cuberta, do contrario, baixala</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Redimensionar unha xanela</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaurar xanela</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar o modo a pantalla completa</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
	<td><p>Trocar o estado de maximización</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Trocar a xanela en todos os espazos de traballo ou en un</p></td>
	<td><p>Desactivado</p></td>
  </tr>
  <tr>
        <td><p>Ver división á esquerda</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Ver división á dereita</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Opcións personalizadas</title>

  <p>Para crear o seu propio atallo de teclado da aplicación nas preferencias de <gui>Teclado</gui>:</p>

  <steps>
    <item>
      <p>Seleccione <gui>Atallos de teclado personalizados</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Escriba un <gui>Nome</gui> para identificar o atallo, <gui>Orde</gui> para executar unha aplicación. Por exemplo, se quere que o atallo abra <app>Rhythmbox</app>, pode chamalo <input>Música</input> e usar a orde <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Faga clic no botón <gui style="button">Engadir atallo…</gui>. Na xanela <gui>Engadir atallo personalizado</gui>, manteña premida a combinación de teclas de atallo desexada.</p>
    </item>
    <item>
      <p>Prema <gui>Engadir</gui>.</p>
    </item>
  </steps>

  <p>O nome da orde que escriba debe ser unha orde do sistema válido. Pode comprobar que a orde funciona abrindo un terminal e escribíndoo nela. A orde que abre unha aplicación pode non ter o mesmo nome que a propia aplicación.</p>

  <p>Se queres cambiar o comando que está asociado a un atallo de teclado personalizado, faga clic na fila do atallo. Aparecerá a xanela <gui>Establecer atallo personalizado</gui> e pode editar o comando.</p>

</section>

</page>
