<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="user-admin-problems" xml:lang="gl">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>
    <link type="seealso" xref="user-admin-explain"/>
    <link type="seealso" xref="user-admin-change"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <!-- TODO: review that this is actually correct -->
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="authohar">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tamén pode facer algunhas cousas, como instalar aplicacións se ten privilexios de administración.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Problemas causados polas restricións administrativas</title>

  <p>Hai algúns problemas que pode experimentar debido a que non teña <link xref="user-admin-explain">privilexios de administración</link>. Algunhas tarefas requiren que teña privilexios de administración para que funcionen, como por exemplo:</p>

  <list>
    <item>
      <p>conectarse a redes con/sen fíos,</p>
    </item>
    <item>
      <p>ver os contidos de particións de disco diferentes (p.ex. se ten unha partición de Windows), ou</p>
    </item>
    <item>
      <p>instalar novas aplicacións.</p>
    </item>
  </list>

  <p>Pode <link xref="user-admin-change">cambiar que ten privilexios de administración</link>.</p>

</page>
