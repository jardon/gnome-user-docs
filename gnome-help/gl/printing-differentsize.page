<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-differentsize" xml:lang="gl">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <desc>Imprimir un documento nun tamaño de papel ou orientación diferente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Cambiar o tamaño do papel ao imprimir</title>

  <p>Se quere cambiar o tamaño do papel do seu documento (por exemplo, imprimir un documento PDF de papel A4 nun papel tipo US Letter), pode cambiar o formato de impresión para o documento.</p>

  <steps>
    <item>
      <p>Abra o diálogo de impresión premendo <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>Seleccione a lapela <gui>Configuración de páxina</gui>.</p>
    </item>
    <item>
      <p>Baixo a columna <gui>Papel</gui>, escolla o seu <gui>Tamaño de papel</gui> na lista despregábel.</p>
    </item>
    <item>
      <p>Prema <gui>Imprimir</gui> e o seu documento debería imprimirse.</p>
    </item>
  </steps>

  <p>You can also use the <gui>Orientation</gui> drop-down list to choose a
  different orientation:</p>

  <list>
    <item><p><gui>Vertical</gui></p></item>
    <item><p><gui>Apaisado</gui></p></item>
    <item><p><gui>Vertical inverso</gui></p></item>
    <item><p><gui>Apaisado inverso</gui></p></item>
  </list>

</page>
