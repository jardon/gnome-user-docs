<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-alert" xml:lang="gl">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Elixa o son para reproducir as mensaxes, estabeleza o volume de alerta, ou desactive os sons de alerta.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Seleccione ou desactive as alertas de son</title>

  <p>O equipo reproducirá un son de alerta sinxelo para certos tipos de mensaxes e eventos. Pode elixir diferentes sons para as alertas, estabelecer o volume da alerta con independencia do volume do seu sistema, ou desactivar os sons de alerta por completo.</p>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Son</gui>.</p>
    </item>
    <item>
      <p>Prema en <gui>Son</gui> para abrir o panel.</p>
    </item>
    <item>
      <p>In the <gui>Alert Sound</gui> section, select an alert sound. Each
      sound will play when you click on it so you can hear how it sounds.</p>
    </item>
  </steps>

  <p>Use the volume slider for <gui>System Sounds</gui> in the <gui>Volume
  Levels</gui> section to set the volume of the alert sound. Click the speaker
  button at the end of the slider to mute or unmute the alert sound. This will
  not affect the volume of your music, movies, or other sound files.</p>

</page>
