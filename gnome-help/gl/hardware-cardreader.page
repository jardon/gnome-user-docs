<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="gl">

  <info>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Resolución de problemas en lectores de tarxetas multimedia.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

<title>Problemas no lector de tarxetas multimedia</title>

<p>Moitos computadores conteñen lectores para tarxetas de almacenamento SD, MMC, MS, CF e outras. Deberíanse detectar automaticamente e <link xref="disk-partitions">montar</link>. Aquí pode ler algunha resolución de erro se non se detectan e montan automaticamente:</p>

<steps>
<item>
<p>Asegúrese de que a tarxeta está inserida correctamente. Moitas tarxetas parecen que están ao revés cando se insiren correctamente. Asegúrese tamén de que a tarxeta está ben colocada na ranura; algunhas tarxetas, especialmente as CF requiren que se faga un pouco de forza para inserilas correctamente. (Teña coidado de non empurrar moi forte. Se choca contra algo sólido, non o force)</p>
</item>

<item>
  <p>Open <app>Files</app> from the
  <gui xref="shell-introduction#activities">Activities</gui> overview. Does the inserted
  card appear in the left sidebar? Sometimes the
  card appears in this list but is not mounted; click it once to mount. (If the
  sidebar is not visible, press <key>F9</key> or click <gui style="menu">Files</gui> in
  the top bar and select the <gui style="menuitem">Sidebar</gui>.)</p>
</item>

<item>
  <p>If your card does not show up in the sidebar, press
  <keyseq><key>Ctrl</key><key>L</key></keyseq>, then type
  <input>computer:///</input> and press <key>Enter</key>. If your card reader
  is correctly configured, the reader should come up as a drive when no card is
  present, and the card itself when the card has been mounted.</p>
</item>

<item>
<p>Se ve o lector de tarxetas pero non a tarxeta, o problema pode estar na tarxeta en si. Ténteo cunha tarxeta diferente ou probe a tarxeta nun lector de tarxetas se lles é posíbel.</p>
</item>
</steps>

<p>If no cards or drives are shown when browsing the <gui>Computer</gui>
location, it is possible that your card reader does not work with Linux due to
driver issues. If your card reader is internal (inside the computer instead of
sitting outside) this is more likely. The best solution is to directly connect
your device (camera, cell phone, etc.) to a USB port on the computer. USB
external card readers are also available, and are far better supported by
Linux.</p>

</page>
