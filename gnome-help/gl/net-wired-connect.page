<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="gl">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.28" date="2018-03-28" status="review"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Para configurar a maioría das conexións a redes con fíos, todo o que precisa é conectar un cabo de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Conectar a unha rede cableada (Ethernet)</title>

  <!-- TODO: create icon manually because it is one overlaid on top of another
       in real life. -->
  <p>Para configurar a maioría das conexións a redes cableadas, todo o que precisa é conectar un cabo de rede. A icona de rede (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">preferencias</span></media>) na barra superior con tres puntos mentres se estabelece a conexión. Os puntos desaparecerán cando estea conectado.</p>
  <media its:translate="no" type="image" src="figures/net-wired-ethernet-diagram.svg" mime="image/svg" width="51" height="38" style="floatend floatright">
    <p its:translate="yes">Diagrama dun cable Ethernet</p>
  </media>

  <p>If this does not happen, you should first of all make sure that your
  network cable is plugged in. One end of the cable should be plugged into the
  rectangular Ethernet (network) port on your computer, and the other end
  should be plugged into a switch, router, network wall socket or similar
  (depending on the network setup you have). The Ethernet port is often on the
  side of a laptop, or near the top of the back of a desktop computer. Sometimes, a
  light beside the Ethernet port will indicate that it is plugged in and active.</p>

  <note>
    <p>You cannot plug one computer directly into another one with a network
    cable (at least, not without some extra setting-up). To connect two
    computers, you should plug them both into a network hub, router or
    switch.</p>
  </note>

  <p>Se aínda non está conectado, pode que a súa rede non admita a configuración automática (DHCP). Neste caso que terá que <link xref="net-manual">configurala manualmente</link>.</p>

</page>
