<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="gl">

  <info>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-12"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Define as funcións de botón e sensación de presión para os «stylus» Wacom.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Configure o «stylus»</title>

  <steps>
    <item>
      <p>Abra a vista de <gui xref="shell-introduction#activities">Actividades</gui> e comece a escribir <gui>Tableta Wacom</gui>.</p>
    </item>
    <item>
      <p>Prema na <gui>Táboa de Wacom</gui> para abrir o panel.</p>
      <note style="tip"><p>Se non se detecta ningunha tableta, pediráselle que <gui>Conecte ou acenda a súa tableta Wacom</gui>. Faga clic en <gui>Bluetooth</gui> na barra lateral para conectar unha tableta sen fíos.</p></note>
    </item>
    <item>
      <p>Hai unha sección que contén preferencias específicas para cada estilete, co nome do dispositivo (a clase do estilete) e o diagrama na parte superior.</p>
      <note style="tip"><p>Se non se detecta ningunha tableta, pediráselle que <gui>Mova o lapis cerca da súa tableta Wacom para configurala</gui>.</p></note>
      <p>Estes axustes pódense axustar:</p>
      <list>
        <item><p><gui>Sensación de presión:</gui> use este desprazador para axustar a «sensación» a <gui>Débil</gui> e <gui>Firme</gui>.</p></item>
        <item><p>Configuración do botón/roda de desprazamento (estas cambian para reflectir o estilete). Faga clic no menú ao lado de cada etiqueta para seleccionar unha destas funcións: Clic no botón central do rato, Clic co botón dereito do rato, Atrás ou Avanzar.</p></item>
     </list>
    </item>
    <item>
      <p>Faga clic en <gui>Probar a súa configuración</gui> na barra de cabeceira para abrir un bloc de debuxos onde podes probar a configuración do teu estilete.</p>
    </item>
  </steps>

</page>
