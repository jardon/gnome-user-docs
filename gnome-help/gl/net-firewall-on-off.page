<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-firewall-on-off" xml:lang="gl">

  <info>
    <link type="guide" xref="net-security" group="#first"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.10" date="2013-11-03" status="incomplete"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pode controlar que programas poden acceder á rede. Isto axúdalle a manter o seu computador seguro.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Activar ou bloquear o acceso á devasa</title>

  <p>GNOME non inclúe unha devasa, polo que para obter asistencia máis aló deste documento consulte co equipo de asistencia da súa distribución ou co departamento de TI da súa organización. O seu ordenador debe estar equipado cunha <em>devasa</em> que lle permita bloquear o acceso a programas doutras persoas en Internet ou na súa rede. Isto axuda a manter o seu ordenador seguro.</p>

  <p>Algunhas aplicacións poden usar a súa conexión de rede. Por exemplo, pode compartir ficheiros ou deixarlle a outras persoas ver o seu escritorio de forma remota ao conectarse a unha rede. Dependendo de como estea configurado o seu computador, podería necesitar axustar o firewall para permitir a ditos servizos funcionar como deben.</p>

  <p>Each program that provides network services uses a specific <em>network
  port</em>. To enable other computers on the network to access a service, you
  may need to “open” its assigned port on the firewall:</p>


  <steps>
    <item>
      <p>Vaia a <gui>Actividades</gui> na esquina superior esquerda da pantalla e inicie a aplicación da devasa. Pode querer instalar un xestor de devasa por si mesmo se non atopa un (por exemplo, GUFW).</p>
    </item>
    <item>
      <p>Abra ou desactive un porto para un servizo de rede, dependendo de se quere que as persoas poidan acceder a el ou non. O porto que debe cambiar <link xref="net-firewall-ports">dependerá do servizo</link>.</p>
    </item>
    <item>
      <p>Garde ou aplique os cambios, seguindo as instrucións adicionais fornecidas pola ferramenta da devasa.</p>
    </item>
  </steps>

</page>
