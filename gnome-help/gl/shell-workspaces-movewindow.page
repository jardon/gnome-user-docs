<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-workspaces-movewindow" xml:lang="gl">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <revision pkgversion="3.8" version="0.3" date="2013-05-10" status="review"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.35.91" date="2020-02-27" status="candidate"/>

    <credit type="author">
      <name>Proxecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vaia á vista de <gui>Actividades</gui> e arrastre unha xanela a un espazo de traballo distinto.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2011-2023</mal:years>
    </mal:credit>
  </info>

  <title>Mova unha xanela a un espazo de traballo diferente</title>

 <if:choose>
 <if:when test="platform:gnome-classic">
  <steps>
    <title>Usando o rato:</title>
    <item>
      <p>Prema o botón na parte inferior esquerda da pantalla na lista de xanelas.</p>
    </item>
    <item>
      <p>Prema e arrastre a xanela cara a parte inferior dereita da pantalla.</p>
    </item>
    <item>
      <p>Drop the window onto one of the workspaces in the <em>workspace
      selector</em> at the right-hand side of the window list. This workspace
      now contains the window you have dropped.</p>
    </item>
  </steps>
 </if:when>
 <if:when test="!platform:gnome-classic">
  <steps>
    <title>Usando o rato:</title>
    <item>
      <p>Abra a vista xeral de <link xref="shell-introduction#activities">Actividades</link>.</p>
    </item>
    <item>
      <p>Prema e arrastre a xanela ao espazo de traballo mostrado parcialmente ao lado da pantalla.</p>
    </item>
    <item>
      <p>If more than one workspace is already in use, you can also drop the window
      onto one of the non-adjacent workspaces in the <em xref="shell-workspaces">workspace
      selector</em> between the search field and the window list. This workspace
      now contains the window you have dropped, and a new empty workspace appears
      at the end of the <em>workspace selector</em>.</p>
    </item>
  </steps>
 </if:when>
 </if:choose>

  <steps>
    <title>Usando o teclado:</title>
    <item>
      <p>Seleccione a xanela que quere mover (p.ex. usando o <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq> ou o <link xref="shell-windows-switching">trocador de xanelas</link>).</p>
    </item>
    <item>
      <p if:test="!platform:gnome-classic">Press <keyseq><key>Super</key><key>Shift</key><key>Page
      Up</key></keyseq> to move the window to a workspace left of the
      current workspace on the <em>workspace selector</em>.</p>
      <p if:test="!platform:gnome-classic">Press <keyseq><key>Super</key><key>Shift</key><key>Page
      Down</key></keyseq> to move the window to a workspace right of the
      current workspace on the <em>workspace selector</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>→</key></keyseq> to move the window to a workspace which
      is left of the current workspace on the <em>workspace selector</em>.</p>
      <p if:test="platform:gnome-classic">Press <keyseq><key>Shift</key><key>Ctrl</key>
      <key>Alt</key><key>←</key></keyseq> to move the window to a workspace which
      is right of the current workspace on the <em>workspace selector</em>.</p>
    </item>
  </steps>

</page>
