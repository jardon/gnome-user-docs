<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="da">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Den dekorative og funktionelle låseskærm formidler nyttig information.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Låseskærmen</title>

  <p>Med låseskærmen kan du se hvad der sker mens din computer er låst og den giver dig mulighed for at få en oversigt over, hvad der er sket, mens du har været væk. Låseskærmen giver nyttige informationer:</p>

  <list>
<!--<item><p>the name of the logged-in user</p></item> -->
    <item><p>dato og klokkeslæt, samt bestemte påmindelser</p></item>
    <item><p>batteri- og netværksstatus</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Klik én gang med din mus eller pegeplade, eller tryk på <key>Esc</key> eller <key>Enter</key> for at låse din computer op. Så vises loginskærmen, hvor du kan indtaste din adgangskode for at låse op. Eller du kan blot begynde at skrive din adgangskode, og loginskærmen vises automatisk når du skriver. Du kan også skifte bruger, nederst til højre på loginskærmen, hvis dit system er konfigureret til mere end én bruger.</p>

  <p>Se <link xref="shell-notifications#lock-screen-notifications"/> for at skjule påmindelser på låseskærmen.</p>

</page>
