<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="keyboard-osk" xml:lang="da">

  <info>
    <link type="guide" xref="keyboard" group="a11y"/>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Brug et skærmtastatur til at indtaste tekst ved at klikke på knapper med musen eller en berøringsfølsom skærm.</desc>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Brug et skærmtastatur</title>

  <p>Hvis du ikke har et tastatur tilsluttet din computer eller foretrækker ikke at bruge det, så kan du tænde for <em>skærmtastaturet</em> for at indtaste tekst.</p>

  <note>
    <p>Skærmtastaturet aktiveres automatisk hvis du bruger en berøringsfølsom skærm</p>
  </note>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilgængelighed</gui> i sidebjælken for at åbne panelet.</p>
    </item>
    <item>
      <p>Tænd for <gui>Skærmtastatur</gui> i afsnittet <gui>Indtastning</gui>.</p>
    </item>
  </steps>

  <p>Næste gang du har muligheden for at skrive åbnes skærmtastaturet nederst på skærmen.</p>

  <p>Tryk på knappen <gui style="button">?123</gui> for at indtaste tal og symboler. Der er flere symboler hvis du herefter trykker på <gui style="button">=/&lt;</gui>-tasten. Vend tilbage til det alfabetiske tastatur ved at trykke på knappen <gui style="button">ABC</gui>.</p>

  <p>You can press the
  <gui style="button"><media its:translate="no" type="image" src="figures/go-down-symbolic.svg" width="16" height="16"><span its:translate="yes">down</span></media></gui>
  button to hide the keyboard temporarily. The keyboard will show again
  automatically when you next press on something where you can use it.
  On a touchscreen, you can also pull up the keyboard by
  <link xref="touchscreen-gestures">dragging up from the bottom edge</link>.</p>
  <p>Tryk på knappen <gui style="button"><media its:translate="no" type="image" src="figures/emoji-flags-symbolic.svg" width="16" height="16"><span its:translate="yes">flag</span></media></gui> for at ændre dine indstillinger for <link xref="session-language">Sprog</link> eller <link xref="keyboard-layouts">Indtastningskilder</link>.</p>

</page>
