<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-browse" xml:lang="da">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>
    <link type="seealso" xref="files-copy"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Håndter og organiser filer med filhåndteringen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

<title>Gennemse filer eller mapper</title>

<p>Brug filhåndteringen <app>Filer</app> til at gennemse og organisere filerne på din computer. Du kan også bruge den til at håndtere filer på lagerenheder (såsom eksterne harddiske), på <link xref="nautilus-connect">filservere</link> og andre netværkdsdelinger.</p>

<p>To start the file manager, open <app>Files</app> in the
<gui xref="shell-introduction#activities">Activities</gui> overview. You can also search
for files and folders through the overview in the same way you would
<link xref="shell-apps-open">search for applications</link>.
</p>

<section id="files-view-folder-contents">
  <title>Udforsk indholdet i mapper</title>

<p>In the file manager, double-click any folder to view its contents, and
double-click or <link xref="mouse-middleclick">middle-click</link> any file to
open it with the default application for that file. Middle-click a folder to
open it in a new tab. You can also right-click a folder to open it in a new tab
or new window.</p>

<p>When looking through the files in a folder, you can quickly <link xref="files-preview">preview each file</link> by pressing the space bar
to be sure you have the right file before opening it, copying it, or
deleting it.</p>

<p>The <em>path bar</em> above the list of files and folders shows you which
folder you’re viewing, including the parent folders of the current folder.
Click a parent folder in the path bar to go to that folder. Right-click any
folder in the path bar to open it in a new tab or window, or access its
properties.</p>

<p>If you want to quickly <link xref="files-search">search for a file</link>,
in or below the folder you are viewing, start typing its name. A <em>search
bar</em> will appear at the top of the window and only files which match your
search will be shown. Press <key>Esc</key> to cancel the search.</p>

<p>You can quickly access common places from the <em>sidebar</em>. If you do
not see the sidebar, press the menu button in the top-right corner of the window
and then select <gui>Sidebar</gui>. You can <link xref="nautilus-bookmarks-edit">add
bookmarks to folders that you use often</link> and they will appear in the sidebar.</p>

</section>

</page>
