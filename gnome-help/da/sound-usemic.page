<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="da">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Brug en analog- eller USB-mikrofon og vælg en standardinputenhed.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Brug en anden mikrofon</title>

  <p>Du kan bruge en ekstern mikrofon til at chatte med venner, snakke med kollegaer på arbejde, foretage stemmeoptagelser eller bruge andre multimedieprogrammer. Selv hvis din computer har en indbygget mikrofon eller et webcam med en mikrofon, så giver en separat mikrofon gerne bedre lydkvalitet.</p>

  <p>Hvis din mikrofon har et rundt stik, så sæt det blot i det rette lydstik på din computer. De fleste computere har to stik: et til mikrofoner og et til højttalere. Stikkets farve er gerne lyserødt eller der er et billede af en mikrofon. Mikrofoner der sættes i det rette stik bruges gerne som standard. Hvis det ikke er tilfældet, så se instruktionerne nedenfor for at vælge en standardinputenhed.</p>

  <p>Hvis du har en USB-mikrofon, så sæt den i en vilkårlig USB-port på din computer. USB-mikrofoner fungerer som separate lydenheder og det kan være nødvendigt at angive hvilken mikrofon der skal bruges som standard.</p>

  <steps>
    <title>Vælg en standardenhed til lydinput</title>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Lyd</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Lyd</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> section, select the device that you want to
      use. The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>Du kan justere lydstyrken og slukke for mikrofonen i panelet.</p>

</page>
