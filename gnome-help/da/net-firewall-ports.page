<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="reference" id="net-firewall-ports" xml:lang="da">

  <info>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-firewall-on-off"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Du skal angive den rette netværksport for at aktivere/deaktivere netværksadgang til et program med din firewall.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Populære netværksporte</title>

  <p>This is a list of network ports commonly used by applications that provide
  network services, like file sharing or remote desktop viewing. You can change
  your system’s firewall to <link xref="net-firewall-on-off">block or allow
  access</link> to these applications. There are thousands of ports in use, so
  this table isn’t complete.</p>

  <table shade="rows" frame="top">
    <thead>
      <tr>
	<td>
	  <p>Port</p>
	</td>
	<td>
	  <p>Navn</p>
	</td>
	<td>
	  <p>Beskrivelse</p>
	</td>
      </tr>
    </thead>
    <tbody>
      <tr>
	<td>
	  <p>5353/udp</p>
	</td>
	<td>
	  <p>mDNS, Avahi</p>
	</td>
	<td>
	  <p>Allows systems to find each other, and describe which services
          they offer, without you having to specify the details manually.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/udp</p>
	</td>
	<td>
	  <p>Udskrivning</p>
	</td>
	<td>
	  <p>Giver dig mulighed for at sende udskriftsjob til en printer over netværket.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>631/tcp</p>
	</td>
	<td>
	  <p>Udskrivning</p>
	</td>
	<td>
	  <p>Giver dig mulighed for at dele din printer med andre personer over netværket.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5298/tcp</p>
	</td>
	<td>
	  <p>Presence</p>
	</td>
	<td>
	  <p>Allows you to advertise your instant messaging status to other
          people on the network, such as “online” or “busy”.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>5900/tcp</p>
	</td>
	<td>
	  <p>Remote desktop</p>
	</td>
	<td>
	  <p>Allows you to share your desktop so other people can view it or
          provide remote assistance.</p>
	</td>
      </tr>
      <tr>
	<td>
	  <p>3689/tcp</p>
	</td>
	<td>
	  <p>Musikdeling (DAAP)</p>
	</td>
	<td>
	  <p>Giver dig mulighed for at dele dit musikbibliotek med andre på dit netværk.</p>
	</td>
      </tr>
    </tbody>
  </table>

</page>
