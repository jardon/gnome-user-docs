<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="da">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skift tegnepladen mellem tegneplade- og musetilstand.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Indstil Wacom-tegnepladens sporingstilstand</title>

<p><gui>Tablet Mode</gui> determines how the stylus is mapped to the screen.</p>

<steps>
  <item>
    <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Wacom-tegneplade</gui>.</p>
  </item>
  <item>
    <p>Klik på <gui>Wacom-tegneplade</gui> for at åbne panelet.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Choose between tablet (or absolute) mode and touchpad (or relative)
    mode. For tablet mode, switch <gui>Tablet Mode</gui> to on.</p>
  </item>
</steps>

  <note style="info"><p>In <em>absolute</em> mode, each point on the tablet maps
  to a point on the screen. The top left corner of the screen, for instance,
  always corresponds to the same point on the tablet.</p>
  <p>In <em>relative</em> mode, if you lift the stylus off the tablet and put it
  down in a different position, the pointer on the screen doesn’t move. This is
  the way a mouse operates.</p>
  </note>

</page>
