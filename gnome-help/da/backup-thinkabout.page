<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="da">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.37.1" date="2020-07-30" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>En liste over mapper hvor du kan finde dokumenter, filer og indstillinger som du måske vil sikkerhedskopiere.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Hvor finder jeg de filer, jeg vil sikkerhedskopiere?</title>

  <p>At beslutte hvilke filer der skal sikkerhedskopieres og at finde dem er det sværeste trin når der skal udføres en sikkerhedskopiering. Nedenfor er de mest almindelige placeringer af vigtige filer og indstillinger som du måske vil sikkerhedskopiere.</p>

<list>
 <item>
  <p>Personlige filer (dokumenter, musik, billeder og videoer)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs   and need to be translated.  You can find the correct translations for your   language here: http://translationproject.org/domain/xdg-user-dirs.html">De er gerne gemt i din hjemmemappe (<file>/home/dit_navn</file>). De kan være i mapper såsom <file>Skrivebord</file>, <file>Dokumenter</file>, <file>Billeder</file>, <file>Musik</file> og <file>Videoer</file>.</p>
  <p>Hvis dit sikkerhedskopieringsmedie har tilstrækkelig plads (f.eks. hvis det er en ekstern harddisk), så overvej at sikkerhedskopiere hele hjemmemappen. Du kan finde ud af hvor meget plads din hjemmemappe bruger med <app>Diskforbrugsanalyse</app>.</p>
 </item>

 <item>
  <p>Skjulte filer</p>
  <p>Fil- og mappenavne der begynder med et punktum (.) er skjulte som standard. Vis skjulte filer ved at trykke på menuknappen i øverste højre hjørne af vinduet i <app>Filer</app> og trykke på <gui>Vis skjulte filer</gui>, eller ved at trykke på <keyseq><key>Ctrl</key><key>H</key></keyseq>. Du kan kopiere dem til en sikkerhedskopieringsplacering som enhver anden fil.</p>
 </item>

 <item>
  <p>Personlige indstillinger (skrivebordspræferencer, temaer og softwareindstillinger)</p>
  <p>De fleste programmer gemmer deres indstillinger i skjulte mapper i din hjemmemappe (se information om skjulte filer oven for).</p>
  <p>De fleste af dine programindstillinger gemmes i de skjulte mapper <file>.config</file> og <file>.local</file> i din hjemmemappe.</p>
 </item>

 <item>
  <p>Indstillinger der gælder for hele systemet</p>
  <p>Indstillinger til vigtige dele af systemet gemmes ikke i din hjemmemappe. Der findes flere placeringer hvor de kan være gemt, men de fleste gemmes i mappen <file>/etc</file>. Generelt behøver du ikke sikkerhedskopiere disse filer på en hjemmecomputer. Hvis du kører en server skal du dog sikkerhedskopiere filerne for de tjenester den kører.</p>
 </item>
</list>

</page>
