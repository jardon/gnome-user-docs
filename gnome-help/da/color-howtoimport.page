<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-howtoimport" xml:lang="da">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-assignprofiles"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Farveprofiler kan importeres ved at åbne dem.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Hvordan importerer jeg farveprofiler?</title>

  <p>You can import a color profile by double clicking on a <file>.ICC</file>
  or <file>.ICM</file> file in the file browser.</p>

  <p>Alternatively you can manage your color profiles through the
  <gui>Color</gui> panel.</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Indstillinger</gui>.</p>
    </item>
    <item>
      <p>Klik på <gui>Farve</gui> i sidebjælken for at åbne panelet.</p>
    </item>
    <item>
      <p>Select your device.</p>
    </item>
    <item>
      <p>Klik på <gui>Tilføj profil</gui> for at vælge en eksisterende profil eller importere en ny profil.</p>
    </item>
    <item>
      <p>Tryk på <gui>Tilføj</gui> for at bekræfte dit valg.</p>
    </item>
  </steps>

  <p>The manufacturer of your display may supply a profile that you can use.
  These profiles are usually made for the average display, so may not be perfect
  for your specific one. For the best calibration, you should
  <link xref="color-calibrate-screen">create your own profile</link> using a
  colorimeter or a spectrophotometer.</p>

</page>
