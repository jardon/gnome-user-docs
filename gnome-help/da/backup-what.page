<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="da">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sikkerhedskopiér alt det du ikke kan holde ud af miste hvis noget går galt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Hvad der skal sikkerhedskopieres</title>

  <p>Din prioritet bør være at sikkerhedskopiere dine <link xref="backup-thinkabout">mest vigtige filer</link> samt dem der er svære at genskabe. F.eks. fra mest vigtige til mindst vigtige:</p>

<terms>
 <item>
  <title>Dine personlige filer</title>
   <p>Det kan omfatte dokumenter, regneark, e-mail, kalenderaftaler, finansielle data, familiebilleder eller andre personlige filer som du betragter som uerstattelige.</p>
 </item>

 <item>
  <title>Dine personlige indstillinger</title>
   <p>Det omfatter ændringer som du har foretaget til farver, baggrunde, skærmopløsning og museindstillinger på dit skrivebord. Det omfatter programindstillinger såsom indstillinger til <app>LibreOffice</app>, din musikafspiller og dit e-mailprogram. De er til at erstatte men det kan tage noget tid af genskabe.</p>
 </item>

 <item>
  <title>Systemindstillinger</title>
   <p>De fleste personer ændrer aldrig de systemindstillinger som blev oprettet under installation. Hvis du af en eller anden grund tilpasser dine systemindstillinger eller hvis du bruger din computer som en server, så kan det være du vil sikkerhedskopiere disse indstillinger.</p>
 </item>

 <item>
  <title>Installeret software</title>
   <p>Den software du bruger kan normalt genoprettes ret hurtigt efter et seriøst problem med computeren ved at geninstallere den.</p>
 </item>
</terms>

  <p>Generelt skal du gå efter at sikkerhedskopiere filer som er uerstattelige og filer som kræver lang tid at erstatte uden en sikkerhedskopi. Modsat, hvis ting er lette at erstatte, så kan det være du ikke vil bruge diskplads på at have sikkerhedskopier af dem.</p>

</page>
