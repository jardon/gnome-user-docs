<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="clock-calendar" xml:lang="da">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Vis dine aftaler i kalenderområdet øverst på skærmen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

 <title>Kalenderaftaler</title>

  <note>
    <p>Det kræver at du bruger din kalender fra <app>Evolution</app> eller <app>Kalender</app>, eller at du har opsat en onlinekonto der understøttes af <gui>Kalender</gui>.</p>
    <p>De fleste distributioner har mindst et af programmerne installeret som standard. Hvis din ikke har, så skal du først installere det med din distributions pakkehåndtering.</p>
 </note>

  <p>Vis dine aftaler:</p>
  <steps>
    <item>
      <p>Klik på uret på toplinjen.</p>
    </item>
    <item>
      <p>Klik på den dato hvor du vil se dine aftaler i kalenderen.</p>

    <note>
       <p>Datoer som har en aftale vises med en prik.</p>
    </note>

      <p>Eksisterende aftaler vises til venstre i kalenderen. Efterhånden som aftaler tilføjes til din <app>Evolution</app>-kalender eller til <app>Kalender</app>, vil de blive vist i urets aftaleliste.</p>
    </item>
  </steps>

 <if:choose>
 <if:when test="!platform:gnome-classic">
 <media type="image" src="figures/shell-appts.png" width="500">
  <p>Ur, kalender og aftaler</p>
 </media>
 </if:when>
 <if:when test="platform:gnome-classic">
 <media type="image" src="figures/shell-appts-classic.png" width="373" height="250">
  <p>Ur, kalender og aftaler</p>
 </media>
 </if:when>
 </if:choose>

</page>
