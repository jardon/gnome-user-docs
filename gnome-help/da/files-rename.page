<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="da">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOMEs dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skift navn på fil eller mappe.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Omdøb en fil eller mappe</title>

  <p>Ligesom i andre filhåndteringer kan du bruge <app>Filer</app> til at ændre navnet på en fil eller mappe.</p>

  <steps>
    <title>Omdøb en fil eller mappe:</title>
    <item><p>Højreklik på elementet og vælg <gui>Omdøb</gui>, eller vælg filen og tryk på <key>F2</key>.</p></item>
    <item><p>Skriv det nye navn og tryk på <key>Enter</key> eller klik på <gui>Omdøb</gui>.</p></item>
  </steps>

  <p>Du kan også omdøbe en fil i vinduet <link xref="nautilus-file-properties-basic">egenskaber</link>.</p>

  <p>Når du omdøber en fil, så er det kun den første del af filen som er valgt, ikke filendelsen (den del der er efter det sidste <file>.</file>). Endelsen angiver normalt hvilken type fil det er (f.eks. er <file>fil.pdf</file> et PDF-dokument), og du behøver normalt ikke ændre det. Hvis du også har brug for at ændre endelsen, så vælg hele filnavnet og ændr det.</p>

  <note style="tip">
    <p>Hvis du omdøbte den forkerte fil eller gav din fil det forkerte navn, så kan du fortryde omdøbningen. Tilbagefør handlingen med det samme ved at klikke på menuknappen i værktøjslinjen og vælge <gui>Fortryd Omdøb</gui>, eller tryk på <keyseq><key>Ctrl</key><key>Z</key></keyseq> for at gendanne det tidligere navn.</p>
  </note>

  <section id="valid-chars">
    <title>Gyldige tegn til filnavne</title>

    <p>Du kan bruge alle tegn i filnavne undtagen tegnet <file>/</file> (skråstreg). Nogle enheder bruger dog et <em>filsystem</em> med flere begrænsninger når det kommer til filnavne. Det er derfor bedst at undgå følgende tegn i dine filnavne: <file>|</file>, <file>\</file>, <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>, <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>Hvis du navngiver en fil med et <file>.</file> som det første tegn, så <link xref="files-hidden">skjules</link> filen når du forsøger at vise den i filhåndteringen.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>Almindelige problemer</title>

    <terms>
      <item>
        <title>Filnavnet er allerede i brug</title>
        <p>Du kan ikke have to filer eller mapper med det samme navn i den samme mappe. Hvis du prøver at omdøbe en fil til et navn der allerede findes i den mappe du arbejder i, så vil filhåndteringen ikke tillade det.</p>
        <p>Fil- og mappenavne skelner mellem store/små bogstaver, så filnavnet <file>Fil.txt</file> er ikke det samme som <file>FIL.txt</file>. Det er tilladt at bruge forskellige filnavne på denne måde, men det anbefales ikke.</p>
      </item>
      <item>
        <title>Filnavnet er for langt</title>
        <p>På nogle filsystemer må filnavne ikke have mere end 255 tegn i deres navne. Grænsen på 255 tegn inkluderer både filnavnet og stien til filen (f.eks. <file>/home/ronnie/Dokumenter/arbejde/forretningsforslag/…</file>), så du bør undgå lange fil- og mappenavne når der er mulighed for det.</p>
      </item>
      <item>
        <title>Valgmuligheden til at omdøbe er gjort grå</title>
        <p>Hvis <gui>Omdøb</gui> er gjort grå, så har du ikke tilladelse til at omdøbe filen. Du bør være forsigtig med at omdøbe sådanne filer da omdøbning af visse beskyttede filer kan få dit system til at blive ustabilt. Se <link xref="nautilus-file-properties-permissions"/> for mere information.</p>
      </item>
    </terms>

  </section>

</page>
