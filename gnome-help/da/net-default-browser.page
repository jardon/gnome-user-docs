<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-default-browser" xml:lang="da">

  <info>
    <link type="guide" xref="net-browser"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.36" date="2020-06-24" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Skift standardwebbrowseren ved at gå til <gui>Detaljer</gui> i <gui>Indstillinger</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2015-2016, 2019-2021</mal:years>
    </mal:credit>
  </info>

  <title>Vælg webbrowser der åbner websteder som standard</title>

  <p>When you click a link to a web page in any application, a web browser will
  automatically open up to that page. However, if you have more than one
  browser installed, the page may not open up in the browser you wanted it to
  open in. To fix this, change the default web browser:</p>

  <steps>
    <item>
      <p>Åbn <gui xref="shell-introduction#activities">Aktivitetsoversigten</gui> og begynd at skrive <input>Standardprogrammer</input>.</p>
    </item>
    <item>
      <p>Klik på <gui>Standardprogrammer</gui> for at åbne panelet.</p>
    </item>
    <item>
      <p>Choose which web browser you would like to open links by
      changing the <gui>Web</gui> option.</p>
    </item>
  </steps>

  <p>When you open up a different web browser, it might tell you that it’s not
  the default browser any more. If this happens, click the <gui>Cancel</gui>
  button (or similar) so that it does not try to set itself as the default
  browser again.</p>

</page>
