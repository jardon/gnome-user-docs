<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hostname" xml:lang="id">

  <info>
    <link type="guide" xref="about" group="hostname"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengubah nama yang digunakan untuk mengidentifikasi sistem Anda di jaringan dan ke perangkat Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Mengubah nama perangkat</title>

  <p>Memiliki nama yang dapat dikenali secara unik membuatnya lebih mudah untuk mengidentifikasi sistem Anda di jaringan dan saat memperpasangkan perangkat Bluetooth.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Tentang</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Tentang</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Pilih <gui>Nama Perangkat</gui> dari daftar.</p>
    </item>
    <item>
      <p>Masukkan nama untuk sistem Anda, dan klik <gui style="button">Ganti Nama</gui>.</p>
    </item>
  </steps>

  <note style="important">
    <p>Meskipun nama host berubah seketika setelah nama sistem diubah, beberapa program dan layanan yang sedang berjalan mungkin harus dimulai ulang agar perubahan berdampak.</p>
  </note>

</page>
