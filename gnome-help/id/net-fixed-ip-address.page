<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-fixed-ip-address" xml:lang="id">

  <info>
    <link type="guide" xref="net-wired"/>
    <link type="seealso" xref="net-findip"/>

    <revision pkgversion="3.4.0" date="2012-03-13" status="final"/>
    <revision pkgversion="3.15" date="2014-12-04" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menggunakan alamat IP statis dapat mempermudah penyediaan beberapa layanan jaringan dari komputer Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Buat suatu koneksi dengan suatu alamat IP yang tetap</title>

  <p>Sebagian besar jaringan akan secara otomatis menetapkan <link xref="net-what-is-ip-address">alamat IP</link> dan rincian lainnya ke komputer Anda ketika Anda terhubung ke jaringan. Rincian ini dapat berubah secara berkala, tetapi Anda mungkin ingin memiliki alamat IP tetap untuk komputer sehingga Anda selalu tahu apa alamatnya (misalnya, jika itu adalah server berkas).</p>

  <!-- TODO Update for Network/Wi-Fi split -->
  <steps>
    <title>Untuk memberi alamat IP tetap (statik) ke komputer Anda:</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Jaringan</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Jaringan</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Temukan sambungan jaringan yang Anda ingin memiliki alamat tetap. Klik tombol <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">pengaturan</span></media> di samping koneksi jaringan. Untuk koneksi <gui>Wi-Fi</gui>, tombol <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">pengaturan</span></media> akan terletak di sebelah jaringan aktif.</p>
    </item>
    <item>
      <p>Pilih tab <gui>IPv4</gui> atau <gui>IPv6</gui> dan ubah <gui>Metode</gui> menjadi <gui>Manual</gui>.</p>
    </item>
    <item>
      <p>Ketikkan <gui xref="net-what-is-ip-address">Alamat IP</gui> dan <gui>Gateway</gui>, serta <gui>Netmask</gui>yang sesuai.</p>
    </item>
    <item>
      <p>Di bagian <gui>DNS</gui>, matikan <gui>Otomatis</gui>. Masukkan alamat IP server DNS yang ingin Anda gunakan. Masukkan alamat server DNS tambahan menggunakan tombol <gui>+</gui> .</p>
    </item>
    <item>
      <p>Di bagian <gui>Rute</gui>, matikan <gui>Otomatis</gui>. Masukkan <gui>Alamat</gui>, <gui>Netmask</gui>, <gui>Gateway</gui>, dan <gui>Metrik</gui> untuk rute yang ingin Anda gunakan. Masukkan rute tambahan menggunakan tombol <gui>+</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Terapkan</gui>. Koneksi jaringan sekarang harus memiliki alamat IP tetap.</p>
    </item>
  </steps>

</page>
