<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-how" xml:lang="id">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit>
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Memakai Déjà Dup (atau aplikasi pencadangan lain) untuk membuat salinan berkas-berkas berharga dan pengaturan sebagai perlindungan atas kehilangan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Bagaimana melakukan back up</title>

  <p>Cara termudah membackup berkas dan pengaturan Anda adalah dengan membiarkan aplikasi backup mengelola proses backup bagi Anda. Tersedia sejumlah aplikasi backup yang berbeda, misalnya <app>Déjà Dup</app>.</p>

  <p>Bantuan bagi aplikasi backup yang Anda pilih akan membimbing Anda langkah demi langkah dalam mengatur preferensi Anda bagi backup, serta bagaimana memulihkan data Anda.</p>

  <p>Sebuah pilihan alternatif adalah untuk <link xref="files-copy">menyalin berkas Anda</link> ke lokasi yang aman, seperti hard drive eksternal, komputer lain pada jaringan, atau USB drive. <link xref="backup-thinkabout">Berkas pribadi</link> dan pengaturan Anda biasanya ada dalam folder Rumah Anda, sehingga Anda dapat menyalinnya dari sana.</p>

  <p>Banyaknya data yang dapat Anda backup dibatasi oleh ukuran media penyimpanan. Bila Anda memiliki ruang pada perangkat backup Anda, hal terbaik adalah untuk membackup seluruh folder Rumah Anda dengan pengecualian berikut:</p>

<list>
 <item><p>Berkas yang telah di-backup di tempat lain, seperti ke drive USB, atau media lepas pasang lain.</p></item>
 <item><p>Berkas yang dapat dengan mudah Anda ciptakan ulang. Misalnya, bila Anda programmer, Anda tak perlu membackup berkas yang dihasilkan ketika Anda mengkompail program Anda. Pastikan saja bahwa Anda membackup berkas sumber aslinya.</p></item>
 <item><p>Sebarang berkas dalam folder Tong Sampah. Folder Tong Sampah Anda dapat ditemukan di <file>~/.local/share/Trash</file>.</p></item>
</list>

</page>
