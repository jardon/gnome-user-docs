<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="id">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>Klik tunggal untuk membuka berkas, menjalankan, atau melihat berkas teks yang dapat dieksekusi, dan menentukan perilaku sampah.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Preferensi perilaku pengelola berkas</title>
<p>You can control whether you single-click or double-click files, how
executable text files are handled, and the trash behavior. Click the menu
button in the top-right corner of the window, select <gui>Preferences</gui>,
then go to the <gui>General</gui> section.</p>

<section id="behavior">
<title>Perilaku</title>
<terms>
 <item>
  <title><gui>Aksi untuk Membuka Butir</gui></title>
  <p>Secara default, mengklik memilih berkas dan mengklik ganda membuka mereka. Anda malah dapat memilih agar berkas dan folder terbuka ketika Anda klik pada mereka sekali. Ketika Anda menggunakan mode klik-tunggal, Anda dapat menekan terus tombol <key>Ctrl</key> sambil mengklik untuk memilih satu atau lebih berkas.</p>
 </item>
</terms>

</section>
<section id="executable">
<title>Berkas teks executable</title>
 <p>Sebuah berkas teks executable adalah berkas yang berisi program yang dapat dijalankan (execute). <link xref="nautilus-file-properties-permissions">Hak akses berkas</link> juga harus memungkinkan berkas untuk dijalankan sebagai sebuah program. Yang paling umum adalah skrip <sys>Shell</sys>, <sys>Python</sys>, dan <sys>Perl</sys>. Ini masing-masing memiliki ekstensi <file>.sh</file>, <file>.py</file>, dan <file>.pl</file>.</p>

 <p>Berkas teks executable juga disebut <em>skrip</em>. Semua skrip di folder <file>~/.local/share/nautilus/scripts</file> akan muncul dalam menu konteks untuk berkas di bawah sub menu <gui style="menuitem">Skrip</gui>. Ketika skrip dieksekusi dari folder lokal, semua berkas yang dipilih akan disisipkan ke skrip sebagai parameter. Untuk menjalankan skrip pada berkas:</p>

<steps>
  <item>
    <p>Navigasikan ke folder yang dikehendaki.</p>
  </item>
  <item>
    <p>Pilih berkas yang diinginkan.</p>
  </item>
  <item>
    <p>Klik kanan pada berkas untuk membuka menu konteks dan pilih skrip yang diinginkan untuk mengeksekusi dari menu <gui style="menuitem">Skrip</gui>.</p>
  </item>
</steps>

 <note style="important">
  <p>Skrip tidak akan diberi parameter apa pun saat dijalankan dari folder jarak jauh seperti folder yang menampilkan konten web atau <sys>ftp</sys>.</p>
 </note>

</section>

</page>
