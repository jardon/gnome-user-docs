<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="user-admin-explain" xml:lang="id">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-02-26"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
        <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Anda memerlukan hak akses administratif untuk mengubah bagian penting dari sistem Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Bagaimana privilese administratif bekerja?</title>

  <p>Selain berkas yang <em>Anda</em> buat, komputer Anda memiliki beberapa berkas yang dibutuhkan oleh sistem untuk itu berfungsi dengan benar. Jika <em>berkas-berkas sistem</em> penting ini diubah secara tidak benar mereka dapat menyebabkan berbagai hal rusak, sehingga mereka terlindung dari perubahan secara default. Aplikasi tertentu juga memodifikasi bagian-bagian penting dari sistem, dan begitu juga dilindungi.</p>

  <p>Cara mereka dilindungi adalah dengan hanya mengizinkan pengguna dengan <em>priviliese administratif</em> untuk mengubah berkas atau memakai aplikasi. Dalam pemakaian sehari-hari, Anda tak akan perlu mengubah sebarang berkas sistem atau memakai aplikasi-aplikasi ini, sehingga secara baku Anda tak punya privilese administratif.</p>

  <p>Terkadang Anda perlu memakai aplikasi-aplikasi ini, sehingga Anda dapat memperoleh privilese administratif secara sementara untuk mengizinkan Anda membuat perubahan. Bila sebuah aplikasi memerlukan privilese administratif, itu akan meminta sandi Anda. Sebagai contoh, bila Anda ingin memasang beberapa perangkat lunak baru, pemasang perangkat lunak (manajer paket) akan meminta sandi administrator Anda sehingga mereka dapat menambahkan aplikasi baru ke sistem. Setelah mereka selesai, privilese administratif Anda akan dicabut lagi.</p>

  <p>Hak administratif dikaitkan dengan akun pengguna Anda. Pengguna <gui>Administrator</gui> diperbolehkan memiliki hak istimewa ini sementara pengguna <gui>Standar</gui> tidak. Tanpa hak administratif, Anda tidak akan bisa menginstal piranti lunak. Beberapa akun pengguna (misalnya, akun "root" memiliki hak administratif permanen. Anda tidak boleh menggunakan hak administratif sepanjang waktu, karena Anda mungkin secara tidak sengaja mengubah sesuatu yang tidak Anda maksudkan (seperti menghapus berkas sistem yang diperlukan, misalnya).</p>

  <p>Ringkasnya, privilese administratif mengizinkan Anda mengubah bagian-bagian penting dari sistem Anda ketika diperlukan, tapi mencegah Anda melakukannya secara tak sengaja.</p>

  <note>
    <title>Apa arti "super user" (pengguna super)?</title>
    <p>Seorang pengguna dengan hak akses administratif kadang disebut <em>super user</em>. Hal ini hanya karena pengguna yang memiliki hak lebih dari pengguna biasa. Anda mungkin melihat orang-orang membicarakan hal-hal seperti <cmd>su</cmd> dan <cmd>sudo</cmd>; ini adalah program yang untuk sementara memberikan Anda hak "super user" (administratif).</p>
  </note>

<section id="advantages">
  <title>Mengapa privilese adminisitratif itu berguna?</title>

  <p>Mempersyaratkan pengguna untuk memiliki privilese administratif sebelum perubahan-perubahan sistem yang penting dilakukan berguna karena itu membantu mencegah sistem Anda rusak, secara disengaja maupun tidak.</p>

  <p>Jika Anda memiliki hak akses administratif sepanjang waktu, Anda mungkin secara tidak sengaja mengubah berkas penting, atau menjalankan aplikasi yang mengubah sesuatu yang penting secara tidak sengaja. Hanya mendapatkan hak akses administratif sementara, ketika Anda membutuhkannya, mengurangi risiko kesalahan ini terjadi.</p>

  <p>Hanya pengguna terpercaya tertentu harus diizinkan untuk memiliki hak administratif. Ini mencegah pengguna lain bermain-main dengan komputer dan melakukan hal seperti mencopot aplikasi yang Anda perlukan, menginstal aplikasi yang tidak diinginkan, atau mengubah berkas penting. Hal ini berguna dari sudut pandang keamanan.</p>

</section>

</page>
