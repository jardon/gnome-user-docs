<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-crackle" xml:lang="id">

  <info>
    <link type="guide" xref="sound-broken"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Periksa kabel audio dan driver kartu suara Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Saya mendengar suara gemerisik atau mendengung ketika memutar</title>

  <p>Jika Anda mendengar gemerisik atau berdengung ketika suara diputar di komputer Anda, Anda mungkin memiliki masalah dengan kabel audio atau konektor, atau masalah dengan driver untuk kartu suara.</p>

<list>
 <item>
  <p>Periksa apakah speaker ditancapkan dengan benar.</p>
  <p>Jika speaker tidak dicolokkan sepenuhnya, atau jika mereka dicolokkan ke soket yang salah, Anda mungkin mendengar suara berdengung.</p>
 </item>

 <item>
  <p>Pastikan kabel speaker/headphone tidak rusak.</p>
  <p>Kabel audio dan konektor secara bertahap memburuk ketika dipakai. Coba colokkan kabel atau headphone ke perangkat audio lain (seperti pemutar MP3 atau pemutar CD) untuk memeriksa apakah masih ada bunyi gemerisik. Jika ada, Anda mungkin perlu mengganti kabel atau headphone.</p>
 </item>

 <item>
  <p>Periksa apakah driver suara tidak sangat baik.</p>
  <p>Beberapa kartu suara tidak bekerja sangat baik di Linux karena mereka tidak memiliki driver yang sangat baik. Masalah ini lebih sulit untuk diidentifikasi. Cobalah mencari pembuat dan model kartu suara Anda di internet, ditambah istilah pencarian "Linux", untuk melihat apakah orang lain mengalami masalah yang sama.</p>
  <p>Anda dapat menggunakan perintah <cmd>lspci</cmd> untuk mendapatkan informasi lebih lanjut tentang kartu suara Anda.</p>
 </item>
</list>

</page>
