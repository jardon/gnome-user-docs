<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="id">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Abaikan penekanan tombol yang sama yang diulang dengan cepat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Nyalakan tombol pantul</title>

  <p>Nyalakan <em>tombol pantul</em> untuk mengabaikan penekanan tombol yang diulang secara cepat. Sebagai contoh, bila Anda mengalami tremor yang menyebabkan Anda menekan suatu tombol secara berulang ketika Anda hanya ingin menekannya sekali, Anda sebaiknya menyalakan tombol pantul.</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pengaturan</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pengaturan</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Aksesibilitas</gui> dalam bilah sisi untuk membuka panel.</p>
    </item>
    <item>
      <p>Tekan <gui>Bantuan Mengetik (AccessX)</gui> dalam bagian <gui>Pengetikan</gui>.</p>
    </item>
    <item>
      <p>Nyalakan <gui>Tombol Pantul</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Nyalakan dan matikan tombol pantul secara cepat</title>
    <p>Anda dapat menyalakan dan mematikan tombol pantul dengan mengklik <link xref="a11y-icon">ikon aksesibilitas</link> pada bilah puncak dan memilih <gui>Tombol Pantul</gui>. Ikon aksesibilitas nampak ketika satu atau lebih pengaturan difungsikan dari panel <gui>Aksesibilitas</gui>.</p>
  </note>

  <p>Memakai penggeser <gui>Tundaan penerimaan</gui> untuk mengubah berapa lama tombol pantul menunggu sebelum dia meloloskan penekanan tombol berikutnya setelah Anda menekan suatu tombol pertama kali. Pilih <gui>Bip ketika suatu tombol ditolak</gui> bila Anda ingin agar komputer bersuara setiap kali dia mengabaikan suatu penekanan tombol karena terjadi terlalu cepat setelah penekanan tombol sebelumnya.</p>

</page>
