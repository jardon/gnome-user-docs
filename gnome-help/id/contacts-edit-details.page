<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-edit-details" xml:lang="id">

  <info>
    <link type="guide" xref="contacts"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="review"/>
    <revision pkgversion="3.8" date="2013-04-27" status="review"/>
    <revision pkgversion="3.12" date="2014-02-26" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.36.2" date="2020-08-11" status="review"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sunting informasi bagi setiap kontak.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Sunting rincian kontak</title>

  <p>Menyunting rincian kontak membantu Anda menjaga informasi dalam buku alamat Anda mutakhir dan lengkap.</p>

  <steps>
    <item>
      <p>Pilih kontak dari daftar kontak Anda.</p>
    </item>
    <item>
      <p>Tekan tombol <media its:translate="no" type="image" src="figures/view-more-symbolic.svg">
      <span its:translate="yes">lihat lainnya</span></media> di pojok kanan atas jendela dan pilih <gui style="menuitem">Sunting</gui>.</p>
    </item>
    <item>
      <p>Sunting rincian kontak.</p>
      <p>Untuk menambahkan <em>rincian</em> seperti misalnya nomor telepon baru atau alamat surel, isi saja rincian pada ruas kosong selanjutkan dengan tipe (nomor telepon, surel, dsb.) yang ingin Anda tambahkan.</p>
      <note style="tip">
        <p>Tekan opsi <media its:translate="no" type="image" src="figures/view-more-symbolic.svg"><span its:translate="yes">lihat lainnya</span></media> di paling bawah untuk memperluas opsi yang tersedia, mengungkapkan bidang seperti <gui>Situs Web</gui> dan <gui>Ulang Tahun</gui>.</p>
      </note>
    </item>
    <item>
      <p>Klik <gui style="button">Selesai</gui> untuk mengakhiri penyuntingan kontak.</p>
    </item>
  </steps>

</page>
