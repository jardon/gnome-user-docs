<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="display-dual-monitors" xml:lang="id">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.34" date="2019-11-12" status="review"/>
    <revision version="gnome:42" status="final" date="2022-02-27"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menata monitor tambahan.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Sambungkan monitor lain ke komputer Anda</title>

<!-- TODO: update video
<section id="video-demo">
  <title>Video Demo</title>
  <media its:translate="no" type="video" width="500" mime="video/webm" src="figures/display-dual-monitors.webm">
    <p its:translate="yes">Demo</p>
    <tt:tt its:translate="yes" xmlns:tt="http://www.w3.org/ns/ttml">
      <tt:body>
        <tt:div begin="1s" end="3s">
          <tt:p>Type <input>displays</input> in the <gui>Activities</gui>
          overview to open the <gui>Displays</gui> settings.</tt:p>
        </tt:div>
        <tt:div begin="3s" end="9s">
          <tt:p>Click on the image of the monitor you would like to activate or
          deactivate, then switch it <gui>ON/OFF</gui>.</tt:p>
        </tt:div>
        <tt:div begin="9s" end="16s">
          <tt:p>The monitor with the top bar is the main monitor. To change
          which monitor is “main”, click on the top bar and drag it over to
          the monitor you want to set as the “main” monitor.</tt:p>
        </tt:div>
        <tt:div begin="16s" end="25s">
          <tt:p>To change the “position” of a monitor, click on it and drag it
          to the desired position.</tt:p>
        </tt:div>
        <tt:div begin="25s" end="29s">
          <tt:p>If you would like both monitors to display the same content,
          check the <gui>Mirror displays</gui> box.</tt:p>
        </tt:div>
        <tt:div begin="29s" end="33s">
          <tt:p>When you are happy with your settings, click <gui>Apply</gui>
          and then click <gui>Keep This Configuration</gui>.</tt:p>
        </tt:div>
        <tt:div begin="33s" end="37s">
          <tt:p>To close the <gui>Displays Settings</gui> click on the
          <gui>×</gui> in the top corner.</tt:p>
        </tt:div>
      </tt:body>
    </tt:tt>
  </media>
</section>
-->

<section id="steps">
  <title>Menata monitor tambahan</title>
  <p>Untuk menyiapkan sebuah monitor tambahan, sambungkan monitor ke komputer Anda. Bila sistem Anda tak mengenalinya seketika, atau Anda ingin menyetel pengaturan:</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Tampilan</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Layar</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Pada diagram pengaturan tampilan, seret layar ke posisi relatif yang Anda inginkan.</p>
      <note style="tip">
        <p>Angka pada diagram ditampilkan di kiri atas setiap layar saat panel <gui>Tampilan</gui> aktif.</p>
      </note>
    </item>
    <item>
      <p>Klik <gui>Tampilan Primer</gui> untuk memilih tampilan primer Anda.</p>

      <note>
        <p>Tampilan primer adalah yang dengan <link xref="shell-introduction">bilah puncak</link>, dan tempat ringkasan <gui>Aktivitas</gui> ditampilkan.</p>
      </note>
    </item>
    <item>
      <p>Klik pada setiap monitor dalam daftar dana pilih orientasi, resolusi atau skala, dan laju penyegaran.</p>
    </item>
    <item>
      <p>Klik <gui>Terapkan</gui>. Pengaturan baru akan diterapkan selama 20 detik sebelum dipulihkan. Dengan demikian, jika Anda tidak dapat melihat apa pun dengan setelan baru, setelan lama Anda akan dipulihkan secara otomatis. Jika Anda senang dengan pengaturan baru, klik <gui>Simpan Perubahan</gui>.</p>
    </item>
  </steps>

</section>

<section id="modes">

  <title>Mode tampilan</title>
    <p>Dengan dua layar, mode tampilan ini tersedia:</p>
    <list>
      <item><p><gui>Tampilan Gabungan:</gui> tepi layar yang digabung sehingga berbagai hal dapat lewat dari satu tampilan ke yang lain.</p></item>
      <item><p><gui>Cermin:</gui> konten yang sama ditampilkan pada dua layar, dengan resolusi dan orientasi yang sama untuk keduanya.</p></item>
    </list>
    <p>Ketika hanya satu tampilan yang diperlukan, misalnya, sebuah monitor eksternal yang tersambung ke laptop yang tertancap dengan lid ditutup, monitor lain dapat dimatikan. Dalam daftar, klik pada monitor yang ingin Anda nonaktifkan, dan matikan itu.</p>
      
</section>

<section id="multiple">

  <title>Menambahkan lebih dari satu monitor</title>
    <p>Dengan lebih dari dua layar, <gui>Tampilan Gabungan</gui> adalah satu-satunya modus yang tersedia.</p>
    <list>
      <item>
        <p>Tekan tombol untuk tampilan yang ingin Anda konfigurasikan.</p>
      </item>
      <item>
        <p>Seret layar ke posisi relatif yang diinginkan.</p>
      </item>
    </list>

</section>
</page>
