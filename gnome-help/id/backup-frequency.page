<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="id">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pelajari seberapa sering Anda mesti membackup berkas penting Anda untuk memastikan agar mereka aman.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

<title>Frekuensi pembuatan cadangan</title>

  <p>Seberapa sering Anda membuat cadangan bergantung kepada tipe data yang akan dibackup. Sebagai contoh, bila Anda menjalankan suatu lingkungan jaringan dengan data kritis yang disimpan dalam server Anda, maka bahkan backup setiap malampun mungkin tidak cukup.</p>

  <p>Di sisi lain, bila Anda membackup data pada komputer rumah Anda maka backup per jam sepertinya tak perlu. Anda mungkin akan terbantu bila mempertimbangkan poin-poin berikut ketika merencanakan jadwal backup Anda:</p>

<list style="compact">
  <item><p>Banyaknya waktu yang Anda habiskan pada komputer.</p></item>
  <item><p>Seberapa sering dan berapa banyak data pada komputer yang berubah.</p></item>
</list>

  <p>Jika data yang Anda ingin buat cadangannya berprioritas lebih rendah, atau hanya akan sedikit berubah, seperti musik, surel, dan foto keluarga, maka backup mingguan atau bahkan bulanan mungkin sudah cukup. Namun, jika Anda kebetulan berada di tengah-tengah pemeriksaan pajak, backup yang lebih sering mungkin diperlukan.</p>

  <p>Sebagai aturan umum, selang waktu di antara backup sebaiknya tidak lebih dari rentang waktu yang Anda rela keluarkan kembali untuk melakukan ulang pekerjaan yang hilang. Misalnya, jika menghabiskan seminggu menulis ulang dokumen hilang terlalu panjang untuk Anda, Anda harus membuat cadangan minimal sekali per minggu.</p>

</page>
