<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup" xml:lang="id">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>
    <link type="seealso" xref="printing-setup-default-printer"/>

    <revision pkgversion="3.10.2" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menyiapkan pencetak yang terhubung ke komputer Anda, atau jaringan lokal Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Mempersiapkan pencetak lokal</title>

  <p>Sistem Anda dapat mengenali banyak tipe pencetak secara otomatis ketika mereka tersambung. Kebanyakan pencetak disambungkan dengan kabel USB yang mencantol ke komputer Anda, tapi beberapa pencetak menyambung ke jaringan kabel atau nirkabel Anda.</p>

  <note style="tip">
    <p>Jika pencetak Anda terhubung ke jaringan, maka tidak akan disiapkan secara otomatis - Anda harus menambahkannya dari panel <gui>Pencetak</gui> dalam <gui>Pengaturan</gui>.</p>
  </note>

  <steps>
    <item>
      <p>Pastikan pencetak dinyalakan.</p>
    </item>
    <item>
      <p>Hubungkan pencetak ke sistem Anda melalui kabel yang sesuai. Anda mungkin melihat aktivitas di layar ketika sistem mencari driver, dan Anda mungkin diminta untuk mengotentikasi untuk menginstalnya.</p>
    </item>
    <item>
      <p>Pesan akan muncul ketika sistem selesai memasang pencetak. Pilih <gui>Cetak Halaman Uji</gui> untuk mencetak halaman uji, atau <gui>Opsi</gui> untuk melakukan perubahan tambahan dalam penyiapan pencetak.</p>
    </item>
  </steps>

  <p>Jika pencetak Anda tidak disiapkan secara otomatis, Anda dapat menambahkannya dalam pengaturan pencetak:</p>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Pencetak</gui>.</p>
    </item>
    <item>
      <p>Klik <gui>Pencetak</gui>.</p>
    </item>
    <item>
      <p>Depending on your system, you may have to press 
      <gui style="button">Unlock</gui> in the top right corner and
      type in your password when prompted.</p>
    </item>
    <item>
      <p>Tekan tombol <gui style="button">Tambah…</gui>.</p>
    </item>
    <item>
      <p>Di jendela pop-up, pilih pencetak baru Anda dan tekan <gui style="button">Tambah</gui>.</p>
      <note style="tip">
        <p>Jika pencetak Anda tidak ditemukan secara otomatis, tetapi Anda tahu alamat jaringannya, masukkan ke dalam ruas teks di bagian bawah dialog dan kemudian tekan <gui style="button">Tambah</gui></p>
      </note>
    </item>
  </steps>

  <p>Jika pencetak Anda tidak muncul di jendela <gui>Tambah Pencetak</gui>, Anda mungkin perlu menginstal pengandar cetak.</p>

  <p>Setelah Anda memasang pencetak, Anda mungkin ingin <link xref="printing-setup-default-printer">mengubah pencetak baku Anda</link>.</p>

</page>
