<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-blank" xml:lang="id">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="session-screenlocks"/>

    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="3.28" date="2018-07-22" status="review"/>
    <revision pkgversion="3.34" date="2019-10-28" status="review"/>
    <revision pkgversion="41" date="2021-09-08" status="review"/>

    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>


    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengubah waktu mengosongkan layar untuk menghemat daya.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Atur waktu pengosongan layar</title>

  <p>Untuk menyimpan daya, Anda dapat mengatur waktu sebelum layar menjadi kosong saat ditinggalkan. Anda juga dapat menonaktifkan pengosongan secara sepenuhnya.</p>

  <steps>
    <title>Untuk mengatur waktu pengosongan layar:</title>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Daya</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Daya</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Pakai menu tarik-turun <gui>Mengosongkan Layar</gui> di bawah <gui>Opsi Penghematan Daya</gui> untuk mengatur waktu sampai layar menjadi kosong, atau menonaktifkan pengosongan secara menyeluruh.</p>
    </item>
  </steps>
  
  <note style="tip">
    <p>Bila komputer Anda ditinggalkan saat kondisi menyala, layar secara otomatis akan mengunci dengan alasan keamanan. Untuk mengubah hal tersebut, lihat <link xref="session-screenlocks"/>.</p>
  </note>

</page>
