<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="backup-thinkabout" xml:lang="id">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.37.1" date="2020-07-30" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Proyek Dokumentasi GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Daftar folder dimana Anda dapat menemukan dokumen, berkas, dan pengaturan yang mungkin ingin Anda backup.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Dimana saya dapat menemukan berkas yang ingin saya back up?</title>

  <p>Menentukan berkas mana yang akan dibackup, dan mencari lokasinya, adalah langkah paling sulit ketika mencoba melakukan backup. Di bawah ini adalah lokasi paling umum dari berkas-berkas penting dan pengaturan yang mungkin ingin Anda backup.</p>

<list>
 <item>
  <p>Berkas-berkas pribadi (dokumen, musik, foto, dan video)</p>
  <p its:locNote="translators: xdg dirs are localised by package xdg-user-dirs   and need to be translated.  You can find the correct translations for your   language here: http://translationproject.org/domain/xdg-user-dirs.html">Ini biasanya disimpan di folder rumah Anda (<file>/home/nama_anda</file>). Mereka bisa berada dalam sub folder seperti misalnya <file>Desktop</file>, <file>Dokumen</file>, <file>Gambar</file>, <file>Musik</file>, dan <file>Video</file>.</p>
  <p>Bila medium backup Anda punya cukup ruang (bila itu adalah hard disk eksternal, misalnya), pertimbangkan untuk membackup seluruh folder Rumah. Anda dapat mengetahui berapa banyak ruang yang dipakai oleh folder Rumah dengan memakai <app>Penganalisa Penggunaan Disk</app>.</p>
 </item>

 <item>
  <p>Berkas tersembunyi</p>
  <p>Sebarang nama berkas atau folder yang diawali dengan titik (.) secara baku tersembunyi. Untuk melihat berkas tersembunyi, klik tombol menu di pojok kanan atas jendela <app>Berkas</app> dan tekan <gui>Tampilkan Berkas Tersembunyi</gui>, atau tekan <keyseq><key>Ctrl</key><key>H</key></keyseq>. Anda dapat menyalin ini ke lokasi cadangan seperti berkas lain.</p>
 </item>

 <item>
  <p>Pengaturan pribadi (preferensi dekstop, tema, dan pengaturan perangkat lunak)</p>
  <p>Kebanyakan aplikasi menyimpan pengaturan mereka dalam folder tersembunyi di dalam folder Rumah Anda (lihat di atas untuk informasi tentang berkas tersembunyi).</p>
  <p>Kebanyakan dari pengaturan aplikasi Anda akan disimpan di dalam folder tersembunyi <file>.config</file> dan <file>.local</file> di dalam folder Rumah Anda.</p>
 </item>

 <item>
  <p>Pengaturan seluruh-sistem</p>
  <p>Pengaturan untuk bagian penting dari sistem tidak disimpan dalam folder Rumah Anda. Ada sejumlah lokasi tempat mereka dapat disimpan, namun sebagian besar disimpan dalam folder <file>/etc</file>. Secara umum, Anda tidak perlu membuat cadangan berkas-berkas tersebut pada sebuah komputer rumah. Namun jika Anda menjalankan server, Anda harus membuat cadangan berkas-berkas bagi layanan yang sedang berjalan.</p>
 </item>
</list>

</page>
