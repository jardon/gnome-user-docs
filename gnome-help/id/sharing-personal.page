<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-personal" xml:lang="id">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.1" date="2013-09-23" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Mengizinkan orang lain mengakses berkas di folder <file>Public</file> Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2011-2014, 2017, 2020-2023.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ahmad Haris</mal:name>
      <mal:email>ahmadharis1982@gmail.com</mal:email>
      <mal:years>2017.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rofiquzzaki</mal:name>
      <mal:email>babisayabundar@gmail.com</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Berbagi pakai berkas pribadi Anda</title>

  <p>Anda dapat mengizinkan akses ke folder <file>Public</file> di folder <file>Home</file> dari komputer lain di jaringan. Konfigurasikan <gui>Berbagi Berkas Pribadi</gui> untuk mengizinkan orang lain mengakses konten folder.</p>

  <note style="info package">
    <p>Anda harus memasang paket <app>gnome-user-share</app> agar <gui>Berbagi Berkas Pribadi</gui> terlihat.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-user-share" style="button">Pasang gnome-user-share</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Buka ringkasan <gui xref="shell-introduction#activities">Aktivitas</gui> dan mulai mengetik <gui>Berbagi</gui>.</p>
    </item>
    <item>
      <p>Klik pada <gui>Berbagi</gui> untuk membuka panel.</p>
    </item>
    <item>
      <p>Jika saklar <gui>Berbagi</gui> di kanan atas jendela disetel ke mati, alihkan ke nyala.</p>

      <note style="info"><p>If the text below <gui>Device Name</gui> allows
      you to edit it, you can <link xref="about-hostname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Pilih <gui>Berbagi Berkas Pribadi</gui>.</p>
    </item>
    <item>
      <p>Nyalakan saklar <gui>Berbagi Berkas Pribadi</gui>. Ini berarti bahwa orang lain pada jaringan Anda saat ini akan bisa coba menyambung ke komputer Anda dan mengakses berkas di dalam folder <file>Public</file> Anda.</p>
      <note style="info">
        <p><em>URI</em> ditampilkan yang dengannya folder <file>Public</file> Anda dapat diakses dari komputer lain di jaringan.</p>
      </note>
    </item>
  </steps>

  <section id="security">
  <title>Keamanan</title>

  <terms>
    <item>
      <title>Memerlukan Sandi</title>
      <p>Untuk meminta orang lain menggunakan kata sandi saat mengakses folder <file>Public</file> Anda, nyalakan tombol <gui>Wajibkan Kata Sandi</gui>. Jika Anda tidak menggunakan opsi ini, siapa pun dapat mencoba melihat folder <file>Public</file> Anda.</p>
      <note style="tip">
        <p>Opsi ini dinonaktifkan secara baku, tetapi Anda harus memfungsikannya dan menetapkan sandi yang aman.</p>
      </note>
    </item>
  </terms>
  </section>

  <section id="networks">
  <title>Jaringan</title>

  <p>Bagian <gui>Jaringan</gui> mencantumkan jaringan yang saat ini Anda terhubung. Gunakan saklar di samping masing-masing untuk memilih di mana berkas pribadi Anda dapat dibagikan.</p>

  </section>

</page>
