<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="pl">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Sygnał może być słaby lub sieć nie pozwala poprawnie się połączyć.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

<title>Dlaczego sieć bezprzewodowa ciągle się rozłącza?</title>

<p>Może się zdarzyć, że sieć bezprzewodowa jest rozłączana bez powodu. Komputer zwykle stara się połączyć ponownie jak najszybciej (ikona sieci na górnym pasku wyświetli trzy kropki, tak jak przy łączeniu), ale może to być irytujące, zwłaszcza podczas korzystania z Internetu.</p>

<section id="signal">
 <title>Słaby sygnał bezprzewodowy</title>

 <p>Częstą przyczyną rozłączeń z sieci bezprzewodowej jest słaby sygnał. Sieci bezprzewodowe mają ograniczony zasięg, więc jeśli komputer jest za daleko od stacji bazowej, to sygnał może być za słaby, aby utrzymać połączenie. Ściany i inne przeszkody między komputerem a stacją bazową także mogą osłabić sygnał.</p>

 <p>Ikona sieci na górnym pasku pokazuje siłę sygnału bezprzewodowego. Jeśli sygnał jest słaby, spróbuj przenieść komputer bliżej stacji bazowej (lub stację bliżej komputera).</p>

</section>

<section id="network">
 <title>Połączenie sieciowe nie jest poprawnie nawiązywane</title>

 <p>Czasami podczas łączenia się z siecią bezprzewodową może wydawać się, że połączono pomyślnie, ale zaraz następuje rozłączenie. Zwykle zdarza się to, jeśli komputer tylko częściowo połączył się z siecią — nawiązał połączenie, ale z jakiegoś powodu go nie sfinalizował, co spowodowało rozłączenie.</p>

 <p>Możliwą przyczyną jest wpisanie błędnego hasła sieci bezprzewodowej, albo fakt, że komputer nie ma pozwolenia na używanie sieci (ponieważ sieć wymaga nazwy użytkownika do zalogowania, na przykład).</p>

</section>

<section id="hardware">
 <title>Zawodny sprzęt/sterowniki bezprzewodowe</title>

 <p>Niektóre urządzenia bezprzewodowe mogą być nieco zawodne. Sieci bezprzewodowe są skomplikowane, więc karty i stacje bazowe sporadycznie sprawiają problemy i mogą się rozłączać. Jest to irytujące, ale przy niektórych urządzeniach zdarza się całkiem często. Jeśli połączenia bezprzewodowe są od czasu do czasu rozłączane, to może być jedyną przyczyną. Jeśli zdarza się to bardzo regularnie, to można zastanowić się nad wymianą sprzętu.</p>

</section>

<section id="busy">
 <title>Sieci bezprzewodowe z wieloma użytkownikami</title>

 <p>Do sieci bezprzewodowych w ruchliwych miejscach (np. na uniwersytetach i w kawiarniach) często próbuje połączyć się wiele komputerów jednocześnie. Czasami takie sieci są zbyt zajęte i mogą nie móc obsłużyć wszystkich komputerów próbujących się połączyć, co powoduje rozłączenie części z nich.</p>

</section>

</page>
