<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-closelid" xml:lang="pl">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>
    <link type="seealso" xref="power-suspend"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-11-08" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.26" date="2017-09-30" status="candidate"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="candidate"/>

    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Laptopy przechodzą do stanu uśpienia po zamknięciu pokrywy, aby oszczędzać prąd.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Dlaczego komputer wyłącza się po zamknięciu pokrywy?</title>

  <p>Po zamknięciu pokrywy laptopa przechodzi on do stanu <link xref="power-suspend"><em>uśpienia</em></link>, aby oszczędzać prąd. Oznacza to, że komputer nie jest tak naprawdę wyłączany — jest tylko usypiany. Można go przebudzić otwierając pokrywę. Jeśli się nie przebudzi, to spróbuj kliknąć myszą lub nacisnąć klawisz. Jeśli laptop nadal nie chce się przebudzić, to naciśnij przycisk zasilania.</p>

  <p>Niektóre komputery nie potrafią się poprawnie uśpić, zwykle ponieważ ich sprzęt nie jest całkowicie obsługiwany przez system operacyjny (np. sterowniki dla systemu Linux są niekompletne). W takim przypadku przebudzenie komputera po zamknięciu pokrywy może się nie udać. Można spróbować <link xref="power-suspendfail">naprawić problem z usypianiem</link> lub wyłączyć usypianie komputera po zamknięciu pokrywy.</p>

<section id="nosuspend">
  <title>Wyłączanie usypiania komputera po zamknięciu pokrywy</title>

  <note style="important">
    <p>Poniższe instrukcje działają tylko, jeśli używany jest <app>systemd</app>. Dokumentacja używanej dystrybucji zawiera więcej informacji.</p>
  </note>

  <note style="important">
    <p>Aby zmienić to ustawienie, na komputerze musi być zainstalowany program <app>Dostrajanie</app>.</p>
    <if:if xmlns:if="http://projectmallard.org/if/1.0/" test="action:install">
      <p><link style="button" action="install:gnome-tweaks">Zainstaluj program <app>Dostrajanie</app></link></p>
    </if:if>
  </note>

  <p>Jeśli komputer nie ma być usypiany po zamknięciu pokrywy, to można zmienić ustawienie tego zachowania.</p>

  <note style="warning">
    <p>Należy zachować ostrożność podczas zmiany tego ustawienia. Niektóre laptopy mogą ulec przegrzaniu, jeśli są włączone z zamkniętą pokrywą, zwłaszcza jeśli są w zamkniętym miejscu, takim jak plecak.</p>
  </note>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Dostrajanie</gui>.</p>
    </item>
    <item>
      <p>Kliknij ikonę <gui>Dostrajanie</gui>, aby otworzyć program.</p>
    </item>
    <item>
      <p>Wybierz kartę <gui>Ogólne</gui>.</p>
    </item>
    <item>
      <p>Przełącz <gui>Uśpienie po zamknięciu pokrywy laptopa</gui> na <gui>◯</gui> (wyłączone).</p>
    </item>
    <item>
      <p>Zamknij okno programu <gui>Dostrajanie</gui>.</p>
    </item>
  </steps>

</section>

</page>
