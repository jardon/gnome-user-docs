<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="pl">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="intro"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="review"/>
    <revision pkgversion="3.35.91" date="2020-07-19" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wizualny przegląd pulpitu, górnego paska i <gui>ekranu podglądu</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Wizualny przegląd środowiska GNOME</title>

  <p>GNOME zawiera interfejs użytkownika zaprojektowany tak, aby nie przeszkadzał, nie rozpraszał i pomagał w pracy. Po pierwszym zalogowaniu zostanie wyświetlony <gui>ekran podglądu</gui> i górny pasek.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Górny pasek powłoki GNOME</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" if:test="!target:mobile">
      <p>Górny pasek powłoki GNOME</p>
    </media>
  </if:when>
</if:choose>

  <p>Górny pasek daje dostęp do okien i programów, kalendarza i spotkań oraz <link xref="status-icons">właściwości systemu</link>, takich jak dźwięk, sieć i zasilanie. W menu systemowym na górnym pasku można zmienić głośność i jasność ekranu, zmienić informacje o połączeniu <gui>Wi-Fi</gui>, sprawdzić stan naładowania akumulatora, wylogować się lub przełączyć użytkownika oraz wyłączyć komputer.</p>

<links type="section"/>

<!-- TODO: Replace "Activities overview" title for classic mode with something
like "Application windows" by using if:when and if:else ? -->
<section id="activities">
  <title><gui>Ekran podglądu</gui></title>

  <p if:test="!platform:gnome-classic">Po uruchomieniu GNOME automatycznie wyświetlony zostanie <gui>ekran podglądu</gui>. Umożliwia on dostęp do okien i programów. Na ekranie podglądu można także zacząć pisać, aby wyszukiwać programy, pliki, katalogi i w Internecie.</p>

  <p if:test="!platform:gnome-classic">Aby otworzyć ekran podglądu w dowolnej chwili, kliknij przycisk <gui>Podgląd</gui> lub przenieś kursor myszy do górnego lewego rogu ekranu. Można także nacisnąć klawisz <key xref="keyboard-key-super">Super</key>.</p>

  <p if:test="platform:gnome-classic">Aby używać okien i programów, kliknij przycisk w dolnym lewym rogu ekranu na liście okien. Można także nacisnąć klawisz <key xref="keyboard-key-super">Super</key>, aby wyświetlić ekran podglądu z miniaturami wszystkich okien na obecnym obszarze roboczym.</p>

  <media type="image" its:translate="no" src="figures/shell-activities-dash.png" height="65" style="floatend floatright" if:test="!target:mobile, !platform:gnome-classic">
    <p>Activities button and Dash</p>
  </media>
  <p if:test="!platform:gnome-classic">Po dole ekranu podglądu znajduje się <em>pasek ulubionych</em>. Wyświetla on ulubione i uruchomione programy. Kliknij ikonę na pasku, aby otworzyć dany program. Jeśli jest on już uruchomiony, to pod jego ikoną będzie widoczna mała kropka. Kliknięcie ikony programu podniesie jego ostatnio używane okno. Można także przeciągnąć ikonę na obszar roboczy.</p>

  <p if:test="!platform:gnome-classic">Kliknięcie ikony prawym przyciskiem myszy wyświetli menu umożliwiające wybranie okna uruchomionego programu lub otwarcie nowego. Można także kliknąć ikonę przytrzymując klawisz <key>Ctrl</key>, aby otworzyć nowe okno.</p>

  <p if:test="!platform:gnome-classic">Przejście do ekranu podglądu początkowo wyświetli przegląd otwartych okien. Wyświetlane będą miniatury wszystkich okien na obecnym obszarze roboczym.</p>

  <p if:test="!platform:gnome-classic">Kliknij ostatni przycisk (ten z dziewięcioma kropkami) na pasku ulubionych, aby wyświetlić przegląd programów. Wyświetlane będą wszystkie programy zainstalowane na komputerze. Kliknij program, aby go uruchomić lub przeciągnij go na obszar roboczy wyświetlany nad zainstalowanymi programami. Można także przeciągnąć program na pasek ulubionych, aby go do niego dodać. Ulubione programy zostają na pasku ulubionych nawet wtedy, kiedy nie są uruchomione, aby można je było szybko włączać.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Więcej informacji o uruchamianiu programów.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Więcej informacji o oknach i obszarach roboczych.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Menu programu</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu programu <app>Terminal</app></p>
      </media>
      <p>Menu programu, położone obok przycisku <gui>Podgląd</gui>, wyświetla nazwę aktywnego programu razem z jego ikoną oraz dostarcza szybki dostęp do okien i informacji o programie, a także możliwość jego wyłączenia.</p>
    </if:when>
    <!-- TODO: check how the app menu removal affects classic mode -->
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>Menu programu <app>Terminal</app></p>
      </media>
      <p>Menu programu, położone obok menu <gui>Programy</gui> i <gui>Miejsca</gui>, wyświetla nazwę aktywnego programu razem z jego ikoną i dostarcza szybki dostęp do preferencji lub pomocy programu. Elementy dostępne w menu programu różnią się w zależności od programu.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Zegar, kalendarz i spotkania</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Zegar, kalendarz, spotkania i powiadomienia</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Zegar, kalendarz i spotkania</p>
    </media>
  </if:when>
</if:choose>

  <p>Kliknij zegar na górnym pasku, aby wyświetlić obecną datę, miesięczny kalendarz, listę nadchodzących spotkań i nowych powiadomień. Można także otworzyć kalendarz naciskając klawisze <keyseq><key>Super</key><key>V</key></keyseq>. Można otworzyć ustawienia daty i czasu oraz pełny program kalendarza bezpośrednio z menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Więcej informacji o kalendarzu i spotkaniach.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Więcej informacji o powiadomieniach i liście powiadomień.</link></p>
    </item>
  </list>

</section>


<section id="systemmenu">
  <title>Menu systemowe</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu użytkownika</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menu użytkownika</p>
    </media>
  </if:when>
</if:choose>

  <p>Kliknij menu systemowe w górnym prawym rogu, aby zarządzać ustawieniami systemu i komputerem.</p>

  <p>Jeśli komputer nie będzie używany, to można zablokować ekran, aby uniemożliwić innym osobom jego używanie. Można także szybko przełączyć użytkownika bez wylogowania się, aby dać komuś dostęp do komputera, albo uśpić go lub wyłączyć z menu. Jeśli ekran obsługuje obrót w pionie lub poziomie, to można szybko go obrócić z menu systemowego. Jeśli ekran nie obsługuje obrotu, to ten przycisk nie będzie wyświetlany.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Więcej informacji o przełączaniu użytkowników, wylogowywaniu i wyłączaniu komputera.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Blokada ekranu</title>

  <p>Po zablokowaniu ekranu, lub jeśli ekran zablokował się automatycznie, wyświetlana jest blokada ekranu. Oprócz zabezpieczania komputera, kiedy użytkownik go nie używa, blokada wyświetla datę i czas. Wyświetla także informacje o stanie naładowania akumulatora i sieci.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Więcej informacji o blokadzie ekranu.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Lista okien</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME zawiera inne podejście do przełączania okien niż zawsze widoczna lista okien, jaką można znaleźć w innych środowiskach. Umożliwia to skupienie się na wykonywanym zadaniu bez rozpraszania się.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Więcej informacji o przełączaniu między oknami.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="800" if:test="!target:mobile">
      <p>Lista okien</p>
    </media>
    <p>Lista okien na dole ekranu zapewnia dostęp do wszystkich otwartych okien i programów, umożliwiając ich szybkie minimalizowanie i przywracanie.</p>
    <p>Po prawej stronie listy okien GNOME wyświetla cztery obszary robocze. Aby przełączyć na inny obszar, kliknij jeden z nich.</p>
  </if:when>
</if:choose>

</section>

</page>
