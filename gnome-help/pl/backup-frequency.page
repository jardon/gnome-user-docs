<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-frequency" xml:lang="pl">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projekt dokumentacji GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak często wykonywać kopię zapasową ważnych plików, aby zapewnić ich bezpieczeństwo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

<title>Częstość kopii zapasowych</title>

  <p>Jak często wykonywać kopie zapasowe zależy od typu danych. Na przykład w przypadku środowiska sieciowego z danymi krytycznymi przechowywanymi na serwerach nawet kopia co noc może nie wystarczać.</p>

  <p>Z drugiej strony, w przypadku danych na domowym komputerze kopia zapasowa co godzinę może być niepotrzebna. Podczas planowania harmonogramu kopii zapasowych można rozważyć te kwestie:</p>

<list style="compact">
  <item><p>Czas spędzany przy komputerze.</p></item>
  <item><p>Jak często i jak bardzo zmieniają się dane na komputerze.</p></item>
</list>

  <p>Jeśli dane w kopii zapasowej mają niższy priorytet, albo mało się zmieniają, tak jak muzyka, wiadomości e-mail i rodzinne zdjęcia, to kopia co tydzień lub nawet co miesiąc może wystarczyć. Ale jeśli akurat jest przeprowadzana kontrola podatkowa, to częstsze kopie mogą być niezbędne.</p>

  <p>Generalnie, czas między kopiami zapasowymi nie powinien być dłuższy od czasu, jaki zajęłoby odtworzenie utraconej pracy. Na przykład, jeśli tydzień ponownego pisania straconych dokumentów to za dużo, to należy wykonywać kopię zapasową co najmniej raz na tydzień.</p>

</page>
