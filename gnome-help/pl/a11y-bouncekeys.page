<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-bouncekeys" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.29" date="2018-09-05" status="review"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Ignorowanie szybko powtórzonych naciśnięć tego samego klawisza.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Włączenie odskakujących klawiszy</title>

  <p>Włącz <em>odskakujące klawisze</em>, aby ignorować szybko powtórzone naciśnięcia klawisza. Jest to przydatne na przykład w przypadku drżenia dłoni, które powoduje wiele naciśnięć klawiszy zamiast jednego.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ustawienia</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui> na panelu bocznym, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Naciśnij <gui>Wspomaganie pisania (AccessX)</gui> w sekcji <gui>Pisanie</gui>.</p>
    </item>
    <item>
      <p>Przełącz <gui>Odskakujące klawisze</gui> na <gui>|</gui> (włączone).</p>
    </item>
  </steps>

  <note style="tip">
    <title>Szybkie przełączanie odskakujących klawiszy</title>
    <p>Można włączać i wyłączać odskakujące klawisze klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Odskakujące klawisze</gui>. Ikona ułatwień dostępu jest widoczna, jeśli włączono co najmniej jedno ustawienie w panelu <gui>Ułatwienia dostępu</gui>.</p>
  </note>

  <p>Użyj suwaka <gui>Opóźnienie akceptacji</gui>, aby zmienić czas po pierwszym naciśnięciu, zanim odskakujące klawisze zarejestrują następne naciśnięcie klawisza. Wybierz <gui>Sygnał dźwiękowy po odrzuceniu klawisza</gui>, aby komputer odtwarzał dźwięk po każdym zignorowaniu klawisza z powodu zbyt szybkiego naciśnięcia.</p>

</page>
