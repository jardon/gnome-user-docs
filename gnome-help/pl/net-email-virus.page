<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="net-email-virus" xml:lang="pl">

  <info>
    <link type="guide" xref="net-email"/>
    <link type="guide" xref="net-security"/>
    <link type="seealso" xref="net-antivirus"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wirusy raczej nie zarażą tego komputera, ale mogą zarazić komputery odbiorców wiadomości.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Czy muszę skanować moje wiadomości e-mail pod kątem wirusów?</title>

  <p>Wirusy to małe programy, które mogą powodować problemy, jeśli znajdą się na komputerze. Często dostają się do komputera przez wiadomości e-mail.</p>

  <p>Wirusy na komputery z systemem Linux są bardzo rzadkie, więc <link xref="net-antivirus">jest mała szansa na zarażenie się przez pocztę e-mail lub każdą inną drogę</link>. Odebranie wiadomości z ukrytym w niej wirusem nie wpłynie na komputer. Dlatego raczej nie ma potrzeby skanowania wiadomości pod kątem wirusów.</p>

  <p>Można jednak skanować wiadomości e-mail pod kątem wirusów, aby uniknąć przekazywania wirusów od jednej osoby do drugiej. Na przykład, jeśli jeden ze znajomych ma komputer z systemem Windows zarażony wirusem i wysłał wirus w wiadomości, to inny znajomy także może go dostać. Można zainstalować program antywirusowy do skanowania wiadomości e-mail, aby tego uniknąć, ale taka sytuacja zdarza się bardzo rzadko, a większość komputerów z systemami Windows i macOS już ma swoje oprogramowanie antywirusowe.</p>

</page>
