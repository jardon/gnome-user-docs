<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-contrast" xml:lang="pl">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Bardziej (lub mniej) wyraźne okna i przyciski na ekranie.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Dostosowanie kontrastu</title>

  <p>Można dostosować kontrast okien i przycisków, aby były lepiej widoczne. To inna funkcja niż zmiana jasności całego ekranu — zostaną zmienione tylko części <em>interfejsu użytkownika</em>.</p>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Ułatwienia dostępu</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ułatwienia dostępu</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Przełącz <gui>Wysoki kontrast</gui> na <gui>|</gui> (włączone) w sekcji <gui>Wzrok</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Szybkie przełączanie wysokiego kontrastu</title>
    <p>Można włączać i wyłączać wysoki kontrast klikając <link xref="a11y-icon">ikonę ułatwień dostępu</link> na górnym pasku i wybierając <gui>Wysoki kontrast</gui>.</p>
  </note>

</page>
