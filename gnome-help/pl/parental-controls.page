<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="parental-controls" xml:lang="pl">

  <info>
    <link type="guide" xref="user-accounts#manage"/>

    <revision version="gnome:43" status="final" date="2022-12-13"/>

    <credit type="author">
      <name>Anthony McGlone</name>
      <email>anthonymcglone2022@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Konfiguracja kontroli rodzicielskiej dla użytkowników na komputerze.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Dodanie kontroli rodzicielskiej</title>

  <p>Rodzice mogą używać programu <gui>Kontrola rodzicielska</gui>, aby uniemożliwić dzieciom dostęp do szkodliwych treści.</p>

  <p>Administrator może używać tego programu do:</p>

  <list>
    <item>
      <p>Ograniczania dostępu użytkownika do przeglądarek WWW i programów.</p>
    </item>
    <item>
      <p>Uniemożliwiania użytkownikowi instalowania programów.</p>
    </item>
    <item>
      <p>Zezwalania użytkownikowi na dostęp do programów wyłącznie odpowiednich do wieku.</p>
    </item>
  </list>

  <note>
    <p><gui>Kontrola rodzicielska</gui> wymaga, aby programy były zainstalowane przez pakiety Flatpak lub z serwisu Flathub.</p>
  </note> 

  <section id="restrictwebbrowsers">
    <title>Ograniczanie przeglądarek WWW</title>
    <p><gui>Kontrola rodzicielska</gui> umożliwia administratorowi wyłączanie użytkownikowi dostępu do przeglądarki.</p>
    <steps>
      <item>
        <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
      </item>
      <item>
        <p>Wybierz <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
      </item>
      <item>
         <p>Pod <gui>Inni użytkownicy</gui> wybierz użytkownika, dla którego zastosować kontrolę rodzicielską.</p>
      </item>
      <item>
         <p>Wybierz kartę <gui>Kontrola rodzicielska</gui>.</p>
         <note>
           <p>Karta jest widoczna tylko, jeśli <gui>Kontrola rodzicielska</gui> jest zainstalowana i włączona.</p>
         </note>
      </item>
      <item>
        <p>Kliknij <gui>Odblokuj</gui> w oknie <gui>Wymagane jest uprawnienie</gui>.</p>
      </item>
       <item>
        <p>Wpisz hasło i uwierzytelnij się, aby odblokować okno <gui>Kontrola rodzicielska</gui>.</p>
      </item>
      <item>
        <p>Przełącz <gui>Ograniczanie przeglądarek internetowych</gui> na <gui>|</gui> (włączone).</p>
      </item>
    </steps>
  </section>
  <section id="restrictapplications">
    <title>Ograniczanie programów</title>
    <p><gui>Kontrola rodzicielska</gui> zawiera listę programów, które administrator może wyłączyć.</p>
    <note>
      <p>Istniejące programy (które nie są zainstalowane przez pakiety Flatpak) nie będą wyświetlane na liście.</p>
    </note>
    <steps>
      <item>
        <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
      </item>
      <item>
        <p>Wybierz <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
      </item>
      <item>
         <p>Pod <gui>Inni użytkownicy</gui> wybierz użytkownika, dla którego zastosować kontrolę rodzicielską.</p>
      </item>
      <item>
         <p>Wybierz kartę <gui>Kontrola rodzicielska</gui>.</p>
         <note>
           <p>Karta jest widoczna tylko, jeśli <gui>Kontrola rodzicielska</gui> jest zainstalowana i włączona.</p>
         </note>
      </item>
      <item>
        <p>Kliknij <gui>Odblokuj</gui> w oknie <gui>Wymagane jest uprawnienie</gui>.</p>
      </item>
       <item>
        <p>Wpisz hasło i uwierzytelnij się, aby odblokować okno <gui>Kontrola rodzicielska</gui>.</p>
      </item>
      <item>
        <p>Kliknij <gui>Ograniczanie programów</gui>.</p>
      </item>
      <item>
        <p>Przełącz przełącznik obok programu do ograniczenia.</p>
      </item>
    </steps>
  </section>
  <section id="restrictapplicationinstallation">
    <title>Ograniczanie instalacji programów</title>
    <p><gui>Kontrola rodzicielska</gui> umożliwia administratorowi odmawianie użytkownikowi uprawnień do instalacji.</p>
    <steps>
      <item>
        <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
      </item>
      <item>
        <p>Wybierz <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
      </item>
      <item>
         <p>Pod <gui>Inni użytkownicy</gui> wybierz użytkownika, dla którego zastosować kontrolę rodzicielską.</p>
      </item>
      <item>
         <p>Wybierz kartę <gui>Kontrola rodzicielska</gui>.</p>
         <note>
           <p>Karta jest widoczna tylko, jeśli <gui>Kontrola rodzicielska</gui> jest zainstalowana i włączona.</p>
         </note>
      </item>
      <item>
        <p>Kliknij <gui>Odblokuj</gui> w oknie <gui>Wymagane jest uprawnienie</gui>.</p>
      </item>
       <item>
        <p>Wpisz hasło i uwierzytelnij się, aby odblokować okno <gui>Kontrola rodzicielska</gui>.</p>
      </item>
      <item>
        <p>Przełącz <gui>Ograniczanie instalacji programów</gui> na <gui>|</gui> (włączone).</p>
      </item>
    </steps>
  </section>
  <section id="applicationsuitability">
    <title>Ograniczanie programów według grup wiekowych</title>
    <p>Ogranicz, które programy są widoczne na podstawie ich stosowności dla danej grupy wiekowej.</p>  
    <steps>
      <item>
        <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Użytkownicy</gui>.</p>
      </item>
      <item>
        <p>Wybierz <gui>Użytkownicy</gui>, aby otworzyć panel.</p>
      </item>
      <item>
         <p>Pod <gui>Inni użytkownicy</gui> wybierz użytkownika, dla którego zastosować kontrolę rodzicielską.</p>
      </item>
      <item>
         <p>Wybierz kartę <gui>Kontrola rodzicielska</gui>.</p>
         <note>
           <p>Karta jest widoczna tylko, jeśli <gui>Kontrola rodzicielska</gui> jest zainstalowana i włączona.</p>
         </note>
      </item>
      <item>
        <p>Kliknij <gui>Odblokuj</gui> w oknie <gui>Wymagane jest uprawnienie</gui>.</p>
      </item>
       <item>
        <p>Wpisz hasło i uwierzytelnij się, aby odblokować okno <gui>Kontrola rodzicielska</gui>.</p>
      </item>
      <item>
        <p>Wybierz grupę wiekową z listy w sekcji <gui>Odpowiedniość programów</gui>.</p>
      </item>
    </steps>
  </section>
</page>
