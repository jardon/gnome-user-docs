<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="pl">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.33.3" date="2019-07-21" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-09"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Umożliwienie innym osobom wyświetlania i działania na komputerze za pomocą RDP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Udostępnianie ekranu</title>

  <p>Można umożliwić innym osobom wyświetlanie i sterowanie komputerem z innego komputera za pomocą specjalnego programu. Skonfiguruj <gui>Zdalny pulpit</gui>, aby umożliwić innym dostęp do komputera i ustaw preferencje zabezpieczeń.</p>

  <note style="info package">
    <p><gui>Zdalny pulpit</gui> jest wyświetlany tylko, jeśli pakiet <app>gnome-remote-desktop</app> jest zainstalowany.</p>

    <if:choose>
      <if:when test="action:install">
        <p><link action="install:gnome-remote-desktop" style="button">Zainstaluj gnome-remote-desktop</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Otwórz <gui xref="shell-introduction#activities">ekran podglądu</gui> i zacznij pisać <gui>Udostępnianie</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Udostępnianie</gui>, aby otworzyć panel.</p>
    </item>
    <item>
      <p>Jeśli <gui>Udostępnianie</gui> w górnym prawym rogu okna jest przełączone na <gui>◯</gui> (wyłączone), to przełącz je na <gui>|</gui> (włączone).</p>

      <note style="info"><p>Jeśli tekst pod napisem <gui>Nazwa urządzenia</gui> umożliwia jego zmianę, to można <link xref="about-hostname">zmienić</link> nazwę wyświetlaną przez komputer w sieci.</p></note>
    </item>
    <item>
      <p>Kliknij <gui>Zdalny pulpit</gui>.</p>
    </item>
    <item>
      <p>Aby umożliwić innym wyświetlanie komputera, przełącz <gui>Zdalny pulpit</gui> na <gui>|</gui> (włączone). Oznacza to, że inne osoby mogą łączyć się z komputerem i widzieć to, co jest na ekranie.</p>
    </item>
    <item>
      <p>Aby inni mogli używać komputera, przełącz <gui>Zdalne sterowanie</gui> na <gui>|</gui> (włączone). Może to umożliwić innej osobie przesuwać kursor, uruchamiać programy i przeglądać pliki na komputerze, w zależności od obecnie używanych ustawień zabezpieczeń.</p>
    </item>
  </steps>

  <section id="security">
  <title>Bezpieczeństwo</title>

  <p>Sekcja <gui>Uwierzytelnianie</gui> wyświetla dane uwierzytelniania logowania do użycia w oprogramowaniu klienckim na łączącym się urządzeniu.</p>
  <terms>
    <item>
      <title>Nazwa użytkownika</title>
      <p>Użyj proponowanej wartości lub wpisz własną.</p>
    </item>
    <item>
      <title>Hasło</title>
      <p>Użyj proponowanej wartości lub wpisz własną.</p>
    <note style="tip">
      <p>Kliknij przycisk obok pola, aby umieścić jego zawartość w schowku.</p>
    </note>
    </item>
    <item>
      <title>Sprawdzenie poprawności szyfrowania</title>
      <p>Kliknij przycisk <gui>Sprawdź poprawność szyfrowania</gui>, aby wyświetlić odcisk szyfrowania. Porównaj go z wartością wyświetlaną w kliencie podczas łączenia: powinny być takie same.</p>
    </item>

<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->

  </terms>
  </section>

  <section id="connecting">
  <title>Łączenie</title>

  <p>Sekcja <gui>Jak się połączyć</gui> wyświetla <gui>Nazwę komputera</gui> i <gui>Adres zdalnego pulpitu</gui>, których można używać do łączenia się. Kliknij przycisk obok danego pola, aby umieścić jego zawartość w schowku. Można także połączyć się za pomocą <link xref="net-findip">adresu IP</link> komputera.</p>
  <p>Kiedy drugi komputer pomyślnie się połączy, w obszarze stanu systemu wyświetlana jest ikona <gui>Ekran jest udostępniany</gui>, <media its:translate="no" type="image" src="figures/topbar-screen-shared.svg" style="floatend"/>.</p>
  </section>

  <section id="clients">
  <title>Klienci</title>

  <p>Działanie tych klientów zostało potwierdzone.</p>
  <terms>
    <item>
      <title>W systemie Linux:</title>
      <list>
        <item><p><app>Remmina</app>, klient używający biblioteki GTK, jest dostępny w większości dystrybucji, a także jako pakiet <link href="https://flathub.org/apps/details/org.remmina.Remmina">Flatpak</link>. Użyj domyślnych ustawień, zwłaszcza ustawienia <gui>Głębi kolorów</gui> na „Automatyczna” w ustawieniach profilu połączenia.</p>
        </item>
        <item><p><app>xfreerdp</app> to klient wiersza poleceń dostępny w większości dystrybucji. Należy przekazać mu opcję <cmd>/network:auto</cmd> w wierszu poleceń.</p>
        </item>
      </list>
    </item>
    <item>
      <title>W systemie Microsoft Windows:</title>
      <list>
        <item><p><app>mstsc</app> to klient wbudowany w system Windows. Zalecane są domyślne ustawienia.</p>
        </item>
      </list>
    </item>
    <item>
      <title>W systemach Linux, Windows lub macOS:</title>
      <list>
        <item><p><app>Thincast</app> to własnościowy klient. Wersja dla systemu Linux jest dostępna jako pakiet <link href="https://flathub.org/apps/details/com.thincast.client">Flatpak</link>. Zalecane są domyślne ustawienia.</p>
        </item>
      </list>
    </item>
  </terms>
  </section>

  <section id="disconnect">
  <title>Zatrzymywanie udostępniania ekranu</title>

  <p>Aby rozłączyć kogoś widzącego pulpit:</p>

  <steps>
    <item>
      <p>Kliknij menu systemowe po prawej stronie górnego paska.</p>
    </item>
    <item>
      <p>Kliknij <gui>Ekran jest udostępniany</gui>.</p>
    </item>
    <item>
      <p>Kliknij <gui>Wyłącz</gui>.</p>
    </item>
  </steps>

  </section>
  
  <section id="advanced" ui:expanded="false">
  <title>Zaawansowane</title>

  <terms>
    <item>
      <title>Konfiguracja w wierszu poleceń</title>
      <p>Narzędzie <cmd>grdctl</cmd> umożliwia skonfigurowanie ustawień komputera w oknie konsoli. Wpisanie <cmd>grdctl --help</cmd> wyświetli informacje o tym, jak je używać.</p>
    </item>
    <item>
      <title>Kodek H.264</title>
      <p>Kodowanie wideo H.264 znacznie zmniejsza zużycie danych. <app>Zdalny pulpit GNOME</app> użyje kodeku H.264, jeśli: używany jest potok graficzny (wymaganie protokołu), obsługuje go klient i NVENC (koder firmy NVIDIA) jest dostępny.</p>
    </item>
  </terms>
  </section>

</page>
