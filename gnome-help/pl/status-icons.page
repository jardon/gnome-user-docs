<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="pl">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="shell-overview#apps"/>

    <!--
    Recommended statuses: stub incomplete draft outdated review candidate final
    -->
    <revision version="gnome:40" date="2021-03-12" status="candidate"/>
    <!--
    FIXME: I'm tentatively marking this final for GNOME 40, because it's at
    least no longer incorrect. But here's a lot to improve:

    * I'm not super happy with the "Other" catchall section at the end, but I also
      don't want to add lots of singleton sections. Tweak the presentation.

    * gnome-shell references network-wireless-disconnected but it doesn't exist:
      https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3827

    * The icons for disconnected states might change:
      https://gitlab.gnome.org/GNOME/adwaita-icon-theme/-/issues/102

    * topbar-audio-volume-overamplified: Write docs on overamplification:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/117

    * Write docs on setting mic sensitivity, and link in a learn more item:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/116

    * topbar-network-wireless-connected: We're super handwavy about when this is
      used. We could use some docs on ad hoc networks.

    * topbar-screen-shared: We have no docs on the screen share portal:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/118

    * topbar-thunderbolt-acquiring: We have no docs on Thunderbolt:
      https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/119
    -->

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2021</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Wyjaśnia znaczenie ikon położonych po prawej stronie górnego paska.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Piotr Drąg</mal:name>
      <mal:email>piotrdrag@gmail.com</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aviary.pl</mal:name>
      <mal:email>community-poland@mozilla.org</mal:email>
      <mal:years>2017-2023</mal:years>
    </mal:credit>
  </info>

  <title>Co oznaczają ikony na górnym pasku?</title>

<p>Ta strona wyjaśnia znaczenie ikon położonych w górnym prawym rogu ekranu. Mówiąc dokładniej, opisane są różne rodzaje ikon wyświetlanych przez system.</p>

<links type="section"/>


<section id="universalicons">
<title>Ikony ułatwień dostępu</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-accessibility.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-accessibility.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Umożliwia szybkie przełączanie różnych ustawień ułatwień dostępu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-pointer.svg"/>
        </if:when>
        <media src="figures/topbar-pointer.svg"/>
      </if:choose>
    </td>
    <td><p>Wskazuje typ kliknięcia podczas używania klikania po najechaniu.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="a11y">Więcej informacji o ułatwieniach dostępu.</link></p></item>
  <item><p><link xref="a11y-dwellclick">Więcej informacji o klikaniu po najechaniu.</link></p></item>
</list>
</section>


<section id="audioicons">
<title>Ikony dźwięku</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume.svg"/>
        </if:when>
        <media src="figures/topbar-audio-volume.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje głośność głośników lub słuchawek.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-audio-volume-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-audio-volume-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Głośniki lub słuchawki są wyciszone.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity.svg"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje czułość mikrofonu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-microphone-sensitivity-muted.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-microphone-sensitivity-muted.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Mikrofon jest wyciszony.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="sound-volume">Więcej informacji o głośności dźwięku.</link></p></item>
</list>
</section>


<section id="batteryicons">
<title>Ikony akumulatorów</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-charging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-charging.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje poziom naładowania akumulatora podczas ładowania.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100-charged.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100-charged.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Akumulator jest w pełni naładowany, ale jest w trakcie ładowania.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-discharging.svg"/>
        </if:when>
        <media src="figures/topbar-battery-discharging.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje poziom naładowania akumulatora, kiedy nie jest w trakcie ładowania.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-battery-level-100.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-battery-level-100.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Akumulator jest w pełni naładowany i nie jest w trakcie ładowania.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-system-shutdown.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-system-shutdown.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Ikona zasilania wyświetlana na komputerach bez akumulatora.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="power-status">Więcej informacji o stanie akumulatora.</link></p></item>
</list>
</section>


<section id="bluetoothicons">
<title>Ikony Bluetooth</title>

<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg"/>
      </if:choose>
    </td>
    <td><p>Tryb samolotowy jest włączony. Bluetooth jest wyłączony, kiedy tryb samolotowy jest włączony.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-bluetooth-active.svg"/>
        </if:when>
        <media src="figures/topbar-bluetooth-active.svg"/>
      </if:choose>
    </td>
    <td><p>Urządzenie Bluetooth jest połączone i używane. Ta ikona jest wyświetlana tylko wtedy, kiedy jest aktywne urządzenie, nie zawsze kiedy Bluetooth jest włączony.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Więcej informacji o trybie samolotowym.</link></p></item>
  <item><p><link xref="bluetooth">Więcej informacji o Bluetooth.</link></p></item>
</list>
</section>


<section id="networkicons">
<info>
<!--
FIXME: I don't want a bare desc, because it ends up in the section links above.
But this section also gets a seealso from net-wireless.page, and we'd like a
desc for that. In Mallard 1.2, we can use role on desc. It works in Yelp, but
it's not in a schema yet, so it will cause validation errors in CI.
  <desc type="link" role="seealso">Explains the meanings of the networking icons in the top bar.</desc>
-->
</info>
<title>Ikony sieci</title>

<table shade="rows">
  <title>Połączenia bezprzewodowe (Wi-Fi)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Tryb samolotowy jest włączony. Sieci bezprzewodowe są wyłączone, kiedy tryb samolotowy jest włączony.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Łączenie z siecią bezprzewodową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength.svg"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje siłę połączenia z siecią bezprzewodową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-strength-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-strength-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią bezprzewodową, ale nie ma zasięgu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią bezprzewodową. Ta ikona jest wyświetlana tylko, jeśli nie można ustalić siły sygnału, na przykład podczas łączenia z sieciami typu ad hoc.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wireless-no-route.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-wireless-no-route.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią bezprzewodową, ale nie ma połączenia z Internetem. Może to być spowodowane błędną konfiguracją sieci po stronie użytkownika lub awarią dostawcy Internetu.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Więcej informacji o trybie samolotowym.</link></p></item>
  <item><p><link xref="net-wireless-connect">Więcej informacji o sieciach bezprzewodowych.</link></p></item>
</list>

<table shade="rows">
  <title>Sieć komórkowa</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-airplane-mode.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-airplane-mode.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Tryb samolotowy jest włączony. Sieci komórkowe są wyłączone, kiedy tryb samolotowy jest włączony.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Łączenie z siecią komórkową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal.svg"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal.svg"/>
      </if:choose>
    </td>
    <td><p>Pokazuje siłę połączenia z siecią komórkową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-signal-none.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-signal-none.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią komórkową, ale nie ma zasięgu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-cellular-connected.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-network-cellular-connected.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią komórkową. Ta ikona jest wyświetlana tylko, jeśli nie można ustalić siły sygnału, na przykład podczas łączenia przez Bluetooth. Jeśli można ustalić siłę sygnału, to zamiast niej jest wyświetlana ikona z siłą.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wireless-airplane">Więcej informacji o trybie samolotowym.</link></p></item>
  <item><p><link xref="net-mobile">Więcej informacji o sieciach komórkowych.</link></p></item>
</list>

<table shade="rows">
  <title>Połączenia przewodowe</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Łączenie z siecią przewodową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired.svg"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią przewodową.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-disconnected.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-disconnected.svg"/>
      </if:choose>
    </td>
    <td><p>Rozłączono z sieci.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-wired-no-route.svg"/>
        </if:when>
        <media src="figures/topbar-network-wired-no-route.svg"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią przewodową, ale nie ma połączenia z Internetem. Może to być spowodowane błędną konfiguracją sieci po stronie użytkownika lub awarią dostawcy Internetu.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-wired-connect">Więcej informacji o sieciach przewodowych.</link></p></item>
</list>

<table shade="rows">
  <title>VPN (wirtualna sieć prywatna)</title>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn-acquiring.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn-acquiring.svg"/>
      </if:choose>
    </td>
    <td><p>Łączenie z siecią VPN.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-network-vpn.svg"/>
        </if:when>
        <media src="figures/topbar-network-vpn.svg"/>
      </if:choose>
    </td>
    <td><p>Połączono z siecią VPN.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="net-vpn-connect">Więcej informacji o sieciach VPN.</link></p></item>
</list>

</section>


<section id="othericons">
<title>Pozostałe ikony</title>
<table shade="rows">
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-input-method.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-input-method.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Pokazuje obecnie używany układ klawiatury lub metodę wprowadzania. Kliknij, aby wybrać inny układ. To menu jest wyświetlane tylko, jeśli skonfigurowano więcej niż jedną metodę wprowadzania.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-find-location.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-find-location.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Program obecnie korzysta z informacji o położeniu komputera. Można to wyłączyć w tym menu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-night-light.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-night-light.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Tryb nocnego światła zmienił temperaturę kolorów ekranu, aby zmniejszyć przemęczenie wzroku. Można to tymczasowo wyłączyć w tym menu.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-media-record.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-media-record.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Obecnie nagrywany jest cały ekran.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-screen-shared.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-screen-shared.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Program obecnie udostępnia ekran lub inne okno.</p></td>
  </tr>
  <tr>
    <td its:translate="no">
      <if:choose>
        <if:when test="platform:gnome-classic">
          <media src="figures/classic-topbar-thunderbolt-acquiring.svg" style="floatend"/>
        </if:when>
        <media src="figures/topbar-thunderbolt-acquiring.svg" style="floatend"/>
      </if:choose>
    </td>
    <td><p>Łączenie z urządzeniem Thunderbolt, takim jak stacja dokująca.</p></td>
  </tr>
</table>

<list style="compact">
  <item><p><link xref="keyboard-layouts">Więcej informacji o układach klawiatury.</link></p></item>
  <item><p><link xref="privacy-location">Więcej informacji o prywatności i usługach położenia.</link></p></item>
  <item><p><link xref="display-night-light">Więcej informacji o nocnym świetle i temperaturze kolorów.</link></p></item>
  <item><p><link xref="screen-shot-record">Więcej informacji o zrzutach i nagraniach ekranu.</link></p></item>
</list>

</section>

</page>
