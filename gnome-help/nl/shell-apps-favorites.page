<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="nl">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Veelgebruikte programmapictogrammen toevoegen aan (of verwijderen uit) de Starter.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Zet uw favoriete toepassingen op de Starter</title>

  <p>Om een toepassing toe te voegen aan de <link xref="shell-introduction#activities">Starter</link> voor snelle toegang:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht door te klikken op <gui>Activiteiten</gui> in de linkerbovenhoek van het scherm</p>
      <p if:test="platform:gnome-classic">Klik op het <gui xref="shell-introduction#activities">Toepassingen</gui>-menu in de linkerbovenhoek van het scherm en kies het onderdeel <gui>Activiteitenoverzicht</gui> uit het menu.</p></item>
    <item>
      <p>Klik op de rasterknop in de starter en zoek de toepassing die u wilt toevoegen.</p>
    </item>
    <item>
      <p>Klik met de rechtermuisknop op het pictogram van de toepassing en kies <gui>Toevoegen aan favorieten</gui>.</p>
      <p>U kunt ook het pictogram naar de starter slepen.</p>
    </item>
  </steps>

  <p>Om een toepassingspictogram uit de Starter te verwijderen, klik met rechts op het toepassingspictogram en selecteer <gui>Uit favorieten verwijderen</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Favoriete toepassingen verschijnen ook in de sectie <gui>Favorieten</gui> van het <gui xref="shell-introduction#activities">Toepassingen</gui>-menu.</p>
  </note>

</page>
