<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="nl">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision pkgversion="41" date="2021-09-08" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tips om het energieverbruik van uw computer te verminderen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Gebruik minder stroom en verbeter de levensduur van de accu</title>

  <p>Computers kunnen veel stroom verbruiken. Door gebruik te maken van een aantal eenvoudige tips kunt u een lagere energierekening krijgen en het milieu helpen.</p>

<section id="general">
  <title>Algemene tips</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Zet uw computer in de pauzestand</link> wanneer u deze niet gebruikt. Dit vermindert het stroomverbruik aanmerkelijk en en de computer kan snel weer geactiveerd worden.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Schakel de computer uit</link> als u het voor een langere tijd niet zal gebruiken. Sommige mensen vrezen dat het regelmatig uitschakelen van een computer ervoor zal zorgen dat de computer sneller verslijt. Dit is echter niet het geval.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="display-blank">automatically blank the screen</link>
    after a certain time, enable the <gui>Automatic <link xref="power-profile">Power
    Saver</link></gui> mode when the battery is low, and have the computer
    <link xref="power-autosuspend">automatically suspend</link> if you have not
    used it for a certain period of time.</p>
  </item>
  <item>
    <p>Reduce the <link xref="display-brightness">screen brightness</link>.</p>
  </item>
  <item>
    <p>Schakel alle externe apparaten uit (zoals printers en scanners) uit als u ze niet gebruikt.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Laptops, netbooks en andere apparaten met accu's</title>

 <list>
   <item>
     <p><link xref="display-dimscreen">De helderheid van het scherm</link> verlagen; het beeldscherm neemt een aanzienlijk deel van het stroomverbruik van een laptop voor zijn rekening.</p>
     <p>De meeste laptops hebben toetsen op het toetsenbord (of sneltoetsen) waarmee u de helderheid kunt verlagen.</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, <link xref="power-wireless">turn off
     the wireless or Bluetooth cards</link>. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>Sommige computers hebben een fysieke schakelaar waarmee de kaart uitgeschakeld kan worden, terwijl andere sneltoetsen hebben die daarvoor gebruikt kunnen worden. U kunt de kaart weer inschakelen wanneer u hem nodig heeft.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Geavanceerde tips</title>

 <list>
   <item>
     <p>Beperk het aantal taken dat op de achtergrond draait. Computers gebruiken meer energie wanneer ze meer werk te doen hebben.</p>
     <p>De meeste van uw openstaande toepassingen doen zeer weinig als u ze niet actief gebruikt. Maar toepassingen die regelmatig gegevens van het internet ophalen, muziek of video's afspelen, of berekeningen maken kunnen van invloed zijn op het energieverbruik.</p>
   </item>
 </list>

</section>

</page>
