<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-fingerprint" xml:lang="nl">

  <info>
    <link type="guide" xref="hardware-auth"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-03" status="review"/>
    <revision pkgversion="3.12" date="2014-06-16" status="final"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="final"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="editor">
      <name>Jim Campbell</name>
      <email>jcampbell@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>U kunt zich op uw systeem aanmelden via een ondersteunde vingerafdrukscanner in plaats van dat u uw wachtwoord intypt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Aanmelden met een vingerafdruk</title>

  <p>Als uw systeem een ondersteunde vingerafdrukscanner heeft, dan kunt u uw vingerafdruk opnemen de deze gebruiken om aan te melden.</p>

<section id="record">
  <title>Een vingerafdruk vastleggen</title>

  <p>Voordat u kunt aanmelden met uw vingerafdruk dient u deze op te nemen, zodat het systeem deze kan gebruiken om u de identificeren.</p>

  <note style="tip">
    <p>Als uw vinger te droog is, dan kan het moeilijk zijn een vingerafdruk te verkrijgen. Als dit het geval is, maak uw vinger dan enigszins vochtig, droog hem met een schone, pluisvrije doek en probeer het opnieuw.</p>
  </note>

  <p>Om andere gebruikersaccounts dan de uwe te bewerken dient u <link xref="user-admin-explain">beheerder</link> te zijn.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Gebruikers</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Gebruikers</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Druk op <gui>Uitgeschakeld</gui>, naast <gui>Aanmelden met vingerafdruk</gui>, om een vingerafdruk toe te voegen voor het geselecteerde account. Als u de vingerafdruk voor een andere gebruiker toevoegt, dan dient u eerst het paneel te <gui>Ontgrendelen</gui>.</p>
    </item>
    <item>
      <p>Kies de vinger die u voor de vingerafdruk wilt gebruiken, en daarna <gui style="button">Volgende</gui>.</p>
    </item>
    <item>
      <p>Volg de instructies in het dialoogvenster en veeg met een <em>gematigde snelheid</em> met uw vinger over uw vingerafdruklezer. Zodra er een goede vingerafdruk is opgenomen, zult u de melding <gui>Klaar!</gui> te zien krijgen.</p>
    </item>
    <item>
      <p>Kies <gui>Volgende</gui>. U zult een bevestigingsmelding krijgen dat uw vingerafdruk met succes opgeslagen is. Kies <gui>Afsluiten</gui> om het af te ronden.</p>
    </item>
  </steps>

</section>

<section id="verify">
  <title>Controleren of uw vingerafdruk werkt</title>

  <p>Controleer nu of het aanmelden met uw nieuwe vingerafdruk werkt. Als u een vingerafdruk registreert, heeft u nog altijd de optie om u met uw wachtwoord aan te melden.</p>

  <steps>
    <item>
      <p>Sla al uw werk op, en kies dan <link xref="shell-exit#logout">afmelden</link>.</p>
    </item>
    <item>
      <p>Kies in het aanmeldscherm uw naam in de lijst. Het wachtwoordinvoerveld zal verschijnen.</p>
    </item>
    <item>
      <p>In plaats van uw wachtwoord in te typen, kunt u met uw vinger over de vingerafdruklezer vegen.</p>
    </item>
  </steps>

</section>

</page>
