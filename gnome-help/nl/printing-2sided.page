<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-2sided" xml:lang="nl">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aan beide kanten van het papier afdrukken, of meerdere pagina's per vel.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Tweezijdig afdrukken en multipagina-opmaak</title>

  <p>Om beide kanten van het papier af te drukken:</p>

  <steps>
    <item>
      <p>Druk op <keyseq><key>Ctrl</key><key>P</key></keyseq> om het afdrukdialoogvenster te openen.</p>
    </item>
    <item>
      <p>Ga naar het tabblad <gui>Papierinstellingen</gui> in het venster Afdrukken en maak een keuze uit de keuzelijst <gui>Tweezijdig</gui>. Als de optie niet beschikbaar is, dan is tweezijdig afdrukken niet mogelijk voor uw printer.</p>
      <p>Printers behandelen tweezijdig afdrukken op verschillende manieren. Het is goed om te experimenteren met uw printer om te zien hoe het werkt.</p>
    </item>
    <item>
      <p>U kunt ook meer dan één pagina van het document per <em>bladzijde</em> afdrukken. Gebruik de optie <gui>Pagina's per bladzijde</gui>  om dit te doen.</p>
    </item>
  </steps>

  <note>
    <p>De beschikbaarheid van deze opties hangt af van het type printer dat u heeft en van de toepassing die u gebruikt. Deze optie is mogelijk niet altijd beschikbaar.</p>
  </note>

</page>
