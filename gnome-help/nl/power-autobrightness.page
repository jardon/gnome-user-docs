<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autobrightness" xml:lang="nl">

  <info>
    <link type="guide" xref="power#saving"/>
    <link type="seealso" xref="display-brightness"/>

    <revision pkgversion="3.20" date="2016-06-15" status="candidate"/>
    <revision pkgversion="41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Schermhelderheid automatisch regelen om accuverbruik te verminderen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Automatische helderheid instellen</title>

  <p>Als uw computer beschikt over een geïntegreerde lichtsensor, dan kan dit gebruikt worden om automatisch de schermhelderheid te regelen. Dit zorgt ervoor dat het scherm altijd goed te zien is bij verschillende lichtomstandigheden van de omgeving en het helpt het accuverbruik te verminderen.</p>

  <steps>

    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Energie</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Energie</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>In the <gui>Power Saving Options</gui> section, ensure that the
      <gui>Automatic Screen Brightness</gui> switch is set to on.</p>
    </item>

  </steps>

  <p>To disable automatic screen brightness, switch it to off.</p>

</page>
