<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" version="1.0 if/1.0" id="gnome-classic" xml:lang="nl">

  <info>
    <link type="guide" xref="shell-overview"/>

    <revision pkgversion="3.10.1" date="2013-10-28" status="review"/>
    <revision pkgversion="3.29" date="2018-08-28" status="review"/>
    <revision version="gnome:40" date="2021-06-16" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Petr Kovar</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Overweeg over te stappen naar Gnome Klassiek als u liever een traditioneler bureaublad heeft.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Wat is Gnome Klassiek?</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
  <p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on modern GNOME
  technologies, it provides a number of changes to the user interface, such as
  the <gui>Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
  <if:when test="platform:gnome-classic">
<p><em>GNOME Classic</em> is a feature for users who prefer a more traditional
  desktop experience. While <em>GNOME Classic</em> is based on modern GNOME
  technologies, it provides a number of changes to the user interface, such as
  the <gui xref="shell-introduction#activities">Applications</gui> and
  <gui>Places</gui> menus on the top bar, and a window
  list at the bottom of the screen.</p>
  </if:when>
</if:choose>

<p>You can use the <gui>Applications</gui> menu on the top bar to launch
 applications.</p>

<section id="gnome-classic-window-list">
<title>Vensterlijst</title>

<p>De vensterlijst onderaan het scherm geeft toegang tot alle geopende vensters en toepassingen en u kunt ze snel minimaliseren en herstellen.</p>

<p>The <gui xref="shell-introduction#activities">Activities</gui> overview is available
 by clicking the button at the left-hand side of the window list at the bottom.</p>

<p>Om toegang te krijgen tot het <em><gui>Activiteiten</gui> overzicht</em> kunt u ook drukken op de <key xref="keyboard-key-super">Super</key>-toets.</p>
 
 <p>Rechts van de vensterlijst toont Gnome een korte identifier voor de huidige werkruimte, zoals <gui>1</gui> voor de eerste (bovenste) werkruimte. Daarnaast toont de identifier ook het totale aantal beschikbare werkruimtes. Om over te schakelen naar een andere werkruimte kunt u klikken op de identifier en uit het menu de werkruimte te kiezen die u wilt gebruiken.</p>

</section>

<section id="gnome-classic-switch">
<title>Van en naar Gnome Klassiek overschakelen</title>

<note if:test="!platform:gnome-classic !platform:ubuntu" style="important">
<p>Gnome Klassiek is alleen beschikbaar op systemen waar bepaalde Gnome Shell-uitbreidingen zijn geïnstalleerd. Sommige GNU/Linux-distributies hebben deze uitbreidingen wellicht niet beschikbaar of niet standaard geïnstalleerd.</p>
</note>

<note if:test="!platform:gnome-classic platform:ubuntu" style="important">
<p its:locNote="Translators: Ubuntu only string">You need to have the
 <sys>gnome-shell-extensions</sys> package installed to make GNOME Classic available.</p>
<p its:locNote="Translators: Ubuntu only string"><link style="button" action="install:gnome-shell-extensions">Install <sys>gnome-shell-extensions</sys></link></p>
</note>

  <steps>
    <title>Om van <em>Gnome</em> naar <em>Gnome Klassiek</em> over te schakelen:</title>
    <item>
      <p>Save any open work, and then log out. Click the system menu on the right
      side of the top bar and then choose the right option.</p>
    </item>
    <item>
      <p>Een bevestigingsbericht zal verschijnen. Selecteer <gui>Afmelden</gui> om te bevestigen.</p>
    </item>
    <item>
      <p>Kies in het aanmeldscherm uw naam uit de lijst.</p>
    </item>
    <item>
      <p>Click the <media its:translate="no" type="image" src="figures/emblem-system-symbolic.svg"><span its:translate="yes">settings</span></media>
      button in the bottom right corner.</p>
    </item>
    <item>
      <p>Select <gui>GNOME Classic</gui> from the list.</p>
    </item>
    <item>
      <p>Vul uw wachtwoord in in het wachtwoordinvoerveld.</p>
    </item>
    <item>
      <p>Druk op <key>Enter</key>.</p>
    </item>
  </steps>

  <steps>
    <title>Om van <em>Gnome Klassiek</em> naar <em>Gnome</em> over te schakelen:</title>
    <item>
      <p>Sla al uw geopende werk op, en meld dan af. Klik op het systeemmenu aan de rechterkant van de bovenbalk, klik op uw naam en kies dan de juiste optie.</p>
    </item>
    <item>
      <p>Een bevestigingsbericht zal verschijnen. Selecteer <gui>Afmelden</gui> om te bevestigen.</p>
    </item>
    <item>
      <p>Kies in het aanmeldscherm uw naam uit de lijst.</p>
    </item>
    <item>
      <p>Click the options icon in the bottom right corner.</p>
    </item>
    <item>
      <p>Select <gui>GNOME</gui> from the list.</p>
    </item>
    <item>
      <p>Vul uw wachtwoord in in het wachtwoordinvoerveld.</p>
    </item>
    <item>
      <p>Druk op <key>Enter</key>.</p>
    </item>
  </steps>
  
</section>

</page>
