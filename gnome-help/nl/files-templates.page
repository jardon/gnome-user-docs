<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-templates" xml:lang="nl">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Snel documenten aanmaken op basis van aangepaste bestandssjablonen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Sjablonen voor veelgebruikte documenttypes</title>

  <p>Als u vaak documenten aanmaakt gebaseerd op dezelfde inhoud, dan kunt u baat hebben bij het gebruiken van bestandssjablonen. Een bestandssjabloon kan een document zijn van welk type dan ook met de opmaak of inhoud die u opnieuw wilt gebruiken. U kunt bijvoorbeeld een documentsjabloon maken met uw briefhoofd.</p>

  <steps>
    <title>Een nieuw sjabloon aanmaken</title>
    <item>
      <p>Een document aanmaken dat u als sjabloon wilt gebruiken. U kunt bijvoorbeeld uw briefhoofd maken in een tekstverwerker.</p>
    </item>
    <item>
      <p>Sla het bestand met de sjablooninhoud op in de map <file>Sjablonen</file> in uw <file>Persoonlijke map</file>. Als de map <file>Sjablonen</file> niet bestaat, dan dient u deze eerst te maken.</p>
    </item>
  </steps>

  <steps>
    <title>Gebruik een sjabloon om een document aan te maken</title>
    <item>
      <p>Open de map waar u het nieuwe document wilt plaatsen.</p>
    </item>
    <item>
      <p>Rechtsklik in de folder op een lege plek en kies <gui style="menuitem">Nieuw document</gui>. De namen van de beschikbare sjablonen zullen in een submenu worden weergegeven.</p>
    </item>
    <item>
      <p>Kies uit de lijst het gewenste sjabloon.</p>
    </item>
    <item>
      <p>Dubbelklik op het bestand om het te openen en begin met het te bewerken. Als u klaar bent, wilt u mogelijk <link xref="files-rename">het bestand hernoemen</link>.</p>
    </item>
  </steps>

</page>
