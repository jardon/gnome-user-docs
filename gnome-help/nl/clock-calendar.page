<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="clock-calendar" xml:lang="nl">

  <info>
    <link type="guide" xref="clock"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.8.0" date="2013-03-09" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="review"/>
    <revision pkgversion="3.16" date="2015-03-02" status="outdated"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Uw afspraken weergeven in de agenda aan de bovenkant van het scherm.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

 <title>Agenda-afspraken</title>

  <note>
    <p>This requires you to use your calendar from <app>Evolution</app> or
    the <app>Calendar</app>, or for you to have an online account set up
    which <gui>Calendar</gui> supports.</p>
    <p>Most distributions come with at least one of these programs installed
    by default. If yours does not, you may need to install it using your
    distribution’s package manager.</p>
 </note>

  <p>Om uw afspraken te bekijken:</p>
  <steps>
    <item>
      <p>Klik op de klok in de bovenbalk.</p>
    </item>
    <item>
      <p>Klik op de datum waarvoor u uw afspraken in de agenda wilt zien.</p>

    <note>
       <p>Er staat een punt onder elke datum waar een afspraak gepland is.</p>
    </note>

      <p>Existing appointments will be displayed to the left of the calendar.
      As appointments are added to your <app>Evolution</app> calendar or to
      <app>Calendar</app>, they will appear in the clock’s appointment list.</p>
    </item>
  </steps>

 <if:choose>
 <if:when test="!platform:gnome-classic">
 <media type="image" src="figures/shell-appts.png" width="500">
  <p>Klok, agenda en afspraken</p>
 </media>
 </if:when>
 <if:when test="platform:gnome-classic">
 <media type="image" src="figures/shell-appts-classic.png" width="373" height="250">
  <p>Klok, agenda en afspraken</p>
 </media>
 </if:when>
 </if:choose>

</page>
