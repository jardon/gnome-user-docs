<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-bookmarks-edit" xml:lang="nl">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-30" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="review"/>
    <revision pkgversion="3.38" date="2020-10-16" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Bladwijzers aan bestandsbeheer toevoegen, verwijderen en hernoemen.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Bladwijzers van mappen toevoegen</title>

  <p>Uw bladwijzers worden in de zijbalk of in de bestandsbeheerder weergegeven.</p>

  <steps>
    <title>Een bladwijzer toevoegen:</title>
    <item>
      <p>Open de map (of locatie) waarvoor u een bladwijzer wilt maken.</p>
    </item>
    <item>
      <p>Click the current folder in the path bar and then select
      <gui style="menuitem">Add to Bookmarks</gui>.</p>
      <note>
        <p>You can also drag a folder to the sidebar, and drop it
        over <gui>New bookmark</gui>, which appears dynamically.</p>
      </note>
    </item>
  </steps>

  <steps>
    <title>Een bladwijzer verwijderen:</title>
    <item>
      <p>Klik met rechts op de bladwijzer in de zijbalk en kies <gui>Verwijderen</gui> uit het menu.</p>
    </item>
  </steps>

  <steps>
    <title>Een bladwijzer hernoemen:</title>
    <item>
      <p>Klik met rechts op de bladwijzer in de zijbalk en kies <gui>Hernoemen…</gui>.</p>
    </item>
    <item>
      <p>Voer een nieuwe naam voor de bladwijzer in het <gui>Naam</gui> tekstveld in.</p>
      <note>
        <p>Het hernoemen van een bladwijzer hernoemt niet de naam van de map. Als u bladwijzers naar twee verschillende mappen op twee verschillende locaties heeft, maar ze hebben dezelfde naam, dan zal de bladwijzer dezelfde naam hebben en kunt u ze niet onderscheiden. In deze gevallen is het nuttig een bladwijzer een andere naam te geven dan de naam van de map waarnaar deze verwijst.</p>
      </note>
    </item>
  </steps>

</page>
