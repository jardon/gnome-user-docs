<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-remove" xml:lang="nl">

  <info>
    <link type="guide" xref="accounts" group="remove"/>
    <link type="seealso" xref="accounts-disable-service"/>

    <revision pkgversion="3.5.5" date="2012-08-15" status="draft"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="draft"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Klein Kravis</name>
      <email>kleinkravis44@outlook.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>De toegang van uw toepassingen tot een dienstenprovider verwijderen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Een account verwijderen</title>

  <p>You can remove an online account which you no longer want to use.</p>

  <note style="tip">
    <p>Many online services provide an authorization token which your desktop stores
    instead of your password. If you remove an account, you should also revoke
    that certificate in the online service. This will ensure that no other
    application or website can connect to that service using the authorization
    for your desktop.</p>

    <p>Hoe u de autorisatie intrekt hangt af van de dienstenprovider. Ga naar uw instellingen op de website van de provider en zoek naar geautoriseerde of verbonden toepassingen of websites. Zoek naar een toepassing genaamd ‘Gnome’ en verwijder die.</p>
  </note>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Online-accounts</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Online-accounts</gui> om het paneel te openen.</p>
    </item>
    <item>
      <p>Selecteer het account dat u wilt verwijderen.</p>
    </item>
    <item>
      <p>Klik op de <gui>-</gui> knop linksonder in het venster.</p>
    </item>
    <item>
      <p>Klik op <gui>Verwijderen</gui> in het bevestigingsvenster.</p>
    </item>
  </steps>

  <note style="tip">
    <p>In plaats van het volledig verwijderen van het account kunt u kiezen voor het <link xref="accounts-disable-service">beperken van de diensten</link> zodat uw computer geen toegang heeft tot uitgeschakelde diensten.</p>
  </note>
</page>
