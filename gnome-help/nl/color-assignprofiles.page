<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-assignprofiles" xml:lang="nl">

  <info>
    <link type="guide" xref="color"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-why-calibrate"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-04" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ga naar <guiseq><gui>Instellingen</gui><gui>Kleur</gui></guiseq> om een kleurprofiel voor uw scherm toe te voegen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Hoe kan ik profielen toewijzen aan apparaten?</title>

  <p>Misschien wilt u een kleurprofiel toewijzen voor uw scherm of printer, zodat de kleuren die ermee getoond worden nauwkeuriger zijn.</p>

  <steps>
    <item>
      <p>Open het <gui xref="shell-introduction#activities">Activiteiten</gui>-overzicht en typ <gui>Instellingen</gui>.</p>
    </item>
    <item>
      <p>Klik op <gui>Instellingen</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Color</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>Selecteer het apparaat waarvoor u een profiel wilt toevoegen.</p>
    </item>
    <item>
      <p>Klik op <gui>Profiel toevoegen</gui> om een bestaand profiel te selecteren of een nieuw profiel te importeren.</p>
    </item>
    <item>
      <p>Druk op <gui>Toevoegen</gui> om uw selectie te bevestigen.</p>
    </item>
    <item>
      <p>To change the used profile, select the profile you would like to use
      and press <gui>Enable</gui> to confirm your selection.</p>
    </item>
  </steps>

  <p>Elk apparaat kan meerdere profielen toegewezen krijgen, maar slechts één profiel kan het <em>standaard</em>profiel zijn. Het standaardprofiel wordt gebruikt wanneer er geen extra informatie is om het profiel automatisch te laten kiezen. Een voorbeeld van deze automatische selectie is als er één profiel aangemaakt is voor glanspapier en een ander voor gewoon papier.</p>

  <p>Als er kalibratiehardware is aangesloten zal de knop <gui>Kalibreren…</gui> een nieuw profiel aanmaken.</p>

</page>
