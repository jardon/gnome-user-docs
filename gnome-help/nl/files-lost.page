<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-lost" xml:lang="nl">

  <info>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Gnome-documentatieproject</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Volg deze tips als u een bestand dat u heeft gemaakt of gedownload niet kunt vinden.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Justin van Steijn</mal:name>
      <mal:email>jvs@fsfe.org</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hannie Dumoleyn</mal:name>
      <mal:email>hannie@ubuntu-nl.org</mal:email>
      <mal:years>2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

<title>Zoeken naar een verloren bestand</title>

<p>Als u een bestand heeft aangemaakt of gedownload, maar het bestand niet meer kunt terug vinden, kunt u deze tips volgen.</p>

<list>
  <item><p>Als u niet meer weet waar u het bestand opgeslagen heeft, maar u herinnert zich nog wel de naam, dan kunt u <link xref="files-search">naar het bestand zoeken op naam</link>.</p></item>

  <item><p>Als u net een bestand heeft gedownload, kan uw webbrowser het bestand automatisch in een standaardmap hebben opgeslagen. Controleer de mappen Check the <file>Bureaublad</file> en <file>Downloads</file> in uw persoonlijke map.</p></item>

  <item><p>U zou per ongeluk een bestand kunnen verwijderen. Wanneer u een bestand verwijdert, wordt het verplaatst naar de prullenbak, waar het blijft tot u handmatig de prullenbak leegt. Zie <link xref="files-recover"/> om te leren hoe u een verwijderd bestand kunt terugzetten.</p></item>

  <item><p>Het is mogelijk dat u het bestand heeft hernoemd op zo'n manier dat het verborgen wordt. Bestanden die met <file>.</file> beginnen of die met <file>~</file> eindigen worden in bestandsbeheer verborgen. Klik in de taakbalk van <app>Bestandsbeheer</app> op de knop Weergaveopties en kies <gui>Verborgen bestanden weergeven</gui> om ze weer te geven. Zie <link xref="files-hidden"/> voor meer informatie.</p></item>
</list>

</page>
