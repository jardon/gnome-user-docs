<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:itst="http://itstool.org/extensions/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="mr">

  <info>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>फाइल्स आणि फोल्डर्स कोण पाहू आणि संपादित करू शकतात ते नियंत्रीत करा.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>
  <title>फाइल अनुमती ठरवा</title>

  <p>मालकीच्या फाइल्स कोण पाहू आणि संपादित करू शकतात त्याकरिता फाइल परवानगीचा वापर करा. फाइलकरिता परवानगीचे अवलोकन आणि सेट करण्यासाठी, उजवी क्लिक द्या आणि <gui>गुणधर्म</gui> पसंत करा, आणि त्यानंतर <gui>परवानगी</gui> टॅब निवडा.</p>

  <p>सेट करण्याजोगी परवानगी प्रकारच्या तपशीलकरिता खालील <link xref="#files"/> आणि <link xref="#folders"/> पहा.</p>

  <section id="files">
    <title>फाइल्स</title>

    <p>You can set the permissions for the file owner, the group owner,
    and all other users of the system. For your files, you are the owner,
    and you can give yourself read-only or read-and-write permission.
    Set a file to read-only if you don’t want to accidentally change it.</p>

    <p>Every user on your computer belongs to a group. On home computers,
    it is common for each user to have their own group, and group permissions
    are not often used. In corporate environments, groups are sometimes used
    for departments or projects. As well as having an owner, each file belongs
    to a group. You can set the file’s group and control the permissions for
    all users in that group. You can only set the file’s group to a group you
    belong to.</p>

    <p>You can also set the permissions for users other than the owner and
    those in the file’s group.</p>

    <p>If the file is a program, such as a script, you must select <gui>Allow
    executing file as program</gui> to run it. Even with this option selected,
    the file manager will still open the file in an application. See
    <link xref="nautilus-behavior#executable"/> for more information.</p>
  </section>

  <section id="folders">
    <title>फोल्डर्स</title>
    <p>मालक, गट, आणि इतर वापरकर्त्यांकरिता तुम्ही फोल्डर्सकरिता परवानगी सेट करू शकता. मालकी, गट, आणि इतर वापरकर्त्यांच्या स्पष्टीकरणकरिता फाइल परवानगीचे तपशील पहा.</p>
    <p>फोल्डरकरिता सेट करण्याजोगी परवानगी फाइलकरिता सेट करण्याजोगी परवानगीपेक्षा वेगळी आहे.</p>
    <terms>
      <item>
        <title><gui itst:context="permission">None</gui></title>
        <p>वापरकर्ता फोल्डरमधील फाइल्स देखील पाहू शकणार नाही.</p>
      </item>
      <item>
        <title><gui>फाइल्सचीच यादी करा</gui></title>
        <p>वापरकर्ता फोल्डर्समधील फाइल्स पाहू शकेल, परंतु फाइल्स उघडू, निर्माण, किंवा नष्ट करू शकणार नाही.</p>
      </item>
      <item>
        <title><gui>फाइल वापरा</gui></title>
        <p>वापरकर्ता फोल्डर्समधील फाइल्स उघडू शकेल (ठराविक फाइलवरील परवानगी असल्यास), परंतु नविन फाइल्स निर्माण, किंवा नष्ट करू शकणार नाही.</p>
      </item>
      <item>
        <title><gui>फाइल्स बनवा आणि काढा</gui></title>
        <p>वापरकर्ता फोल्डरकरिता संपूर्ण प्रवेश राहील, फाइल्स उघडणे, निर्माण करणे आणि नष्ट करणे समाविष्टीत.</p>
      </item>
    </terms>

    <p><gui>एंक्लोज्ड फाइल्सकरिता परवानगी बदला</gui> क्लिक करून तुम्ही फोल्डरमधील सर्व फाइल्सकरिता फाइल परवानगी सेट करू शकता. समाविष्टीत फाइल्स किंवा फोल्डर्सची परवानगी सुस्थीत करण्यासाठी ड्रॉप-डाउन सूचीचा वापर करा, आणि <gui>बदला</gui> क्लिक करा. उपफोल्डर्समधील फाइल्स आणि फोल्डर्सकरिता परवानगी लागू केली जाते, कोणत्याही खोलतापर्यंत.</p>
  </section>

</page>
