<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="mr">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>हे बॅकअप फाइल्स आहेत. ते पूर्वनिर्धारितपणे छुपे असतात.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>What is a file with a <file>~</file> at the end of its name?</title>

  <p>Files with a <file>~</file> at the end of their names (for example,
  <file>example.txt~</file>) are automatically created backup copies of
  documents edited in the <app>gedit</app> text editor or other applications.
  It is safe to delete them, but there is no harm to leave them on your
  computer.</p>

  <p>These files are hidden by default. If you are seeing them, that is because
  you either selected <gui>Show Hidden Files</gui> (in the view options menu
  <!-- FIXME: Get a tooltip added for "View options" -->
  of the <app>Files</app> toolbar) or pressed
  <keyseq><key>Ctrl</key><key>H</key></keyseq>. You can hide them again by
  repeating either of these steps.</p>

  <p>ह्या फाइल्सला सामान्य छुप्या फाइल्सप्रमाणेच उपचार केले जाते. छुप्या फाइल्स हाताळण्याकरिता सूचनाकरिता <link xref="files-hidden"/> पहा.</p>

</page>
