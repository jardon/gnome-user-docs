<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="contacts-setup" xml:lang="mr">

  <info>
    <link type="guide" xref="contacts" group="#first"/>

    <revision pkgversion="3.5.5" date="2012-08-13" status="draft"/>
    <revision pkgversion="3.12" date="2014-02-26" status="final"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>
    <revision pkgversion="3.22" date="2017-03-19" status="draft"/>
    <revision pkgversion="3.38.0" date="2020-11-02" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>२०१२</years>
    </credit>
    
    <credit type="author copyright">
      <name>Paul Cutler</name>
      <email>pcutler@gnome.org</email>
      <years>2017</years>
    </credit>

    <credit type="editor">
      <name>Pranali Deshmukh</name>
      <email>pranali21293@gmail.com</email>
      <years>2020</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>तुमच्या संपर्कांना स्थानीय पत्ता पुस्तिका किंवा एक ऑनलाइन खात्यामध्ये साठवा.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>संपर्क सुरु करण्याची ही पहिली वेळ</title>

  <p>When you run <app>Contacts</app> for the first time, the <gui>Select
  Address Book</gui> window opens.</p>
   
  <p>If you have <link xref="accounts">online accounts</link> configured, they
  are listed with <gui>Local Address Book</gui>. Select an item from the
  list and press <gui style="button">Done</gui>.</p>
  
  <p>All new contacts you create will be saved to the address book you choose. 
  You are also able to view, edit and delete contacts in other address books.</p>
  
  <p>If you have no online accounts configured, press
  <gui style="button">Online Accounts</gui> to begin the setup. If you do not
  wish to set up online accounts at this time, press <gui style="button">Local
  Address Book</gui>.</p>

</page>
