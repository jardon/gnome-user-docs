<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-check" xml:lang="mr">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Test your hard disk for problems to make sure that it’s healthy.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>अडचणींकरिता हार्ड डिस्कची चाचणी करा </title>

<section id="disk-status">
 <title>हार्ड डिस्क तपासत आहे</title>
  <p>हार्ड डिस्क्समध्ये बिल्ट-इन हेल्थ-चेक साधन ज्यास <app>SMART</app> (सेल्फ-मॉनिटरिंग, अनॅलसिस, आणि रिपोर्टिंग टेक्नॉलजि) असे म्हटले जाते, जे ठराविक अडचणींकरिता वारंवार डिस्कची चाचणी करते. डिस्क खंडीत होत असल्यास SMART तुम्हाला सावध करते, ज्यामुळे महत्वाचा डाटा गमवण्यापासून मदत प्राप्त होते.</p>

  <p>Although SMART runs automatically, you can also check your disk’s
 health by running the <app>Disks</app> application:</p>

<steps>
 <title>Check your disk’s health using the Disks application</title>

  <item>
    <p>Open <app>Disks</app> from the <gui>Activities</gui> overview.</p>
  </item>
  <item>
    <p>Select the disk you want to check from the list of storage devices on
    the left. Information and status of the disk will be shown.</p>
  </item>
  <item>
    <p>Click the menu button and select <gui>SMART Data &amp; Self-Tests…</gui>.
    The <gui>Overall Assessment</gui> should say “Disk is OK”.</p>
  </item>
  <item>
    <p><gui>SMART गुणधर्म</gui> अंतर्गत अधिक माहिती पहा, किंवा स्व-चाचणी चालवण्याकरिता <gui style="button">स्व-चाचणी सुरू करा</gui> बटन क्लिक करा.</p>
  </item>

</steps>

</section>

<section id="disk-not-healthy">

 <title>What if the disk isn’t healthy?</title>

  <p>Even if the <gui>Overall Assessment</gui> indicates that the disk
  <em>isn’t</em> healthy, there may be no cause for alarm. However, it’s better
  to be prepared with a <link xref="backup-why">backup</link> to prevent data
  loss.</p>

  <p>If the status says “Pre-fail”, the disk is still reasonably healthy but
 signs of wear have been detected which mean it might fail in the near future.
 If your hard disk (or computer) is a few years old, you are likely to see
 this message on at least some of the health checks. You should
 <link xref="backup-how">backup your important files regularly</link> and check
 the disk status periodically to see if it gets worse.</p>

  <p>अधिक वाईट होत असल्यास, संगणक किंवा हार्ड डिस्कला तज्ञकडे पुढील विश्लेषण किंवा दुरूस्तीकरिता न्या.</p>

</section>

</page>
