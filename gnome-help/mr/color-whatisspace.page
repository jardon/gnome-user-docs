<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="color-whatisspace" xml:lang="mr">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>रंग क्षेत्र ठरवलेल्या रंगांची व्याप्ति आहे.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>रंग जागा काय असते?</title>

  <p>रंग क्षेत्र ठरवलेल्या रंगांची व्याप्ति आहे. परिचीत रंग क्षेत्रांमध्ये sRGB, AdobeRGB आणि ProPhotoRGB समाविष्टीत आहे.</p>

  <p>मानवीय दृश्य प्रणाली सोपे RGB सेंसर नाही, परंतु CIE 1931 क्रोमॅटिसिटि आकृतीसह डाळा कसे प्रतिसाद पुरवतो हे जाणून घेणे शक्य आहे जे मानवी दृश्य प्रतिसादला हॉर्स-शू आकार म्हणून दाखवते. मानवीय व्हिजनमध्ये निळ्या किंवा लाल ऐवजी हिरव्या रंगाची जास्त छटा असते. RGB सारख्या ट्रायक्रोमॅटिक रंग क्षेत्रसह संगणकावरील रंगांना तीन मूल्यांसह प्रस्तुत केले जाते जे रंगाचे <em>ट्रायअँगल</em> स्वरूपातील एंकोडिंग पर्यंत मर्यादीत असते.</p>

  <note>
    <p>मॉडल जसे कि CIE 1931 क्रोमॅटिसिटि आकृतीचा वापर मानवी दृश्य प्रणालीची सुलभीकरण आहे, आणि रिअल गॅमुट्सला 3D हल्स म्हणून प्रस्तुत केले जाते, 2D प्रोजेक्शन्स ऐवजी. 3D आकाराचे 2D प्रोजेक्शन गोंधळास्पद असू शकते, जेणेकरून तुम्हाला 3D हल पहायचे असल्यास, <code>gcm-viewer</code> ॲप्लिकेशनचा वापर करा.</p>
  </note>

  <figure>
    <desc>sRGB, AdobeRGB आणि ProPhotoRGB पांढऱ्या त्रिकोणांनी दर्शविले आहेत</desc>
    <media its:translate="no" type="image" mime="image/png" src="figures/color-space.png"/>
  </figure>

  <p>पहिले, sRGB कडे लक्ष देताना, ज्याची छोटी व्याप्ति आहे आणि किमान रंग एंकोड करते. ते १० वर्ष जुणे CRT डिस्पलेचे अप्रॉक्सीमेशन आहे, आणि आधुनिक मॉनिटर्स यापेक्षा जास्त रंग दाखवू शकतात. sRGB <em>लिस्ट-कॉमन-डिनॉमिनेटर</em> मानक आहे आणि मोठ्या प्रमाणात ॲप्लिकेशन्सतर्फे वापरले जाते (इंटरनेट समाविष्टीत).</p>
  <p>AdobeRGB याचा वापर <em>एडिटिंग स्पेस</em> म्हणून केला जातो. sRGB पेक्षा जास्त रंग एंकोड करणे शक्य आहे, याचा अर्थ बुहतांश रंग क्लिप्ड किंवा ब्लॅक्स क्रश केले जातात, यांची काळजी न करता फोटोग्राफमधील रंग बदलणे शक्य आहे.</p>
  <p>ProPhoto सर्वात मोठे स्पेस उपलब्ध आहे आणि दस्तऐवज साठवण्याकरिता त्याचा वारंवार वापर केला जातो. मानवीय डोळ्यातर्फे आढळलेली रंगांची व्याप्ति एंकोड करणे शक्य आहे, डोळ्यांतर्फे न आढळणारी रंग देखील एंकोड केली जातात!</p>

  <p>
    Now, if ProPhoto is clearly better, why don’t we use it for everything?
    The answer is to do with <em>quantization</em>.
    If you only have 8 bits (256 levels) to encode each channel, then a
    larger range is going to have bigger steps between each value.
  </p>
  <p> बिगर स्टेप्स म्हणजे कॅपचर्ड रंग आणि साठवलेल्या रंग अंतर्गत मोठ्या त्रुटी, आणि काही रंगांकरिता ही खूप मोठी अडचण आहे. की रंग, जसे कि स्किन कलर खूप महत्वाचे आहेत, आणि फोटोग्राफमधील छोटी त्रुटी देखील विनाप्रशिक्षीत श्रोत्यांना पटकन आढळून येते.</p>
  <p>होय, १६ बिट प्रतिमेला खूप जास्त टप्पे आणि छोटे काँटायजेशन त्रुटी आवश्यक असेल, परंतु यामुळे प्रत्येक प्रतिमा फाइलचे आकार दुहेरी होते. आज अस्तित्वात असलेली बहुतांश अंतर्भुत माहिती ८bpp आहे, म्हणजेच ८ बिट्स-पर-पिक्सेल.</p>
  <p>रंग व्यवस्थापन म्हणजे एक रंग क्षेत्रातून दुसऱ्यामध्ये रूपांतरीत करणे, जेथे रंग क्षेत्र परिचीत क्षेत्र जसे कि sRGB, किंवा पसंतीचे क्षेत्र जसे कि मॉनिटर किंवा प्रिंटर प्रोफाइल असू शकते.</p>

</page>
