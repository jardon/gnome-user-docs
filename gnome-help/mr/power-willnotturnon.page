<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="mr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <desc>सैल केबल्स आणि हार्डवेअर अडचणी संभाव्य कारण आहेत.</desc>
    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>माझे संगणक सुरू होणार नाही</title>

<p>तुमचे संगणक सुरू न होण्याचे असंख्य कारण आहेत. हे शिर्षक तुम्हाला संभाव्य कारणांचे संक्षिप्त अवलोकन पुरवते.</p>
	
<section id="nopower">
  <title>संगणक जोडले नाही, रिकामी बॅटरि, किंवा सैल केबल</title>
  <p>संगणाच्या पावर केबल्स घट्टपणे जोडलेले आहेत आणि पावर आउटलेट्स सुरू आहे याची खात्री करा. मॉनिटर जोडले आहे आणि सुर आहे याची देखील खात्री करा. तुमच्याकडे लॅपटॉप असल्यास, चार्जिंग केबलची जोडणी करा (बॅटरी रिकामी झाल्यास). बॅटरी काढून टाकण्याजोगी असल्यास, ती योग्यरित्या एका ठिकाणी जोडली आहे याचीही खात्री (लॅपटॉपचे खालील भाग पहा) करा.</p>
</section>

<section id="hardwareproblem">
  <title>संगणकाच्या हार्डवेअरसह अडचण</title>
  <p>A component of your computer may be broken or malfunctioning. If this is
  the case, you will need to get your computer repaired. Common faults include
  a broken power supply unit, incorrectly-fitted components (such as the
  memory or RAM) and a faulty motherboard.</p>
</section>

<section id="beeps">
  <title>संगणक बीप करते आणि नंतर बंद होते</title>
  <p>If the computer beeps several times when you turn it on and then turns off
  (or fails to start), it may be indicating that it has detected a problem.
  These beeps are sometimes referred to as <em>beep codes</em>, and the pattern
  of beeps is intended to tell you what the problem with the computer is.
  Different manufacturers use different beep codes, so you will have to consult
  the manual for your computer’s motherboard, or take your computer in for
  repairs.</p>
</section>

<section id="fans">
  <title>संगणकातील फॅन्स फिरत आहेत परंतु पडद्यावरच काहीच आढळत नाही</title>
  <p>सर्वप्रथम मॉनिटरची जोडणी झाली आहे आणि ते सुरू आहे, याची चाचणी करा.</p>
  <p>ही अडचण हार्डवेअर व्यत्ययमुळे देखील होऊ शकते. पावर बटन दाबल्यानंतर फॅन्स सुरू होऊ शकतात, परंतु संगणकाचे इतर महत्वाचे भाग सुरू होण्यास अपयशी ठरतील. अशा घटनांमध्ये, संगणकाला दुरूस्तीकरिता न्या.</p>
</section>

</page>
