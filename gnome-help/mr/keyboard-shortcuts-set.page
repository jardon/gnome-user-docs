<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="mr">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision version="gnome:40" date="2021-03-02" status="review"/>
    <revision version="gnome:42" status="final" date="2022-04-05"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email>juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email>tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Andre Klapper</name>
      <email>ak-47@gmx.net</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc><gui>कळफलक</gui> सेटिंग्जमध्ये कळफल शार्टक्ट्स ठरवा किंवा बदला.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

  <title>कीबोर्ड शॉर्टकटस् ठरवा</title>

<p>कळफलक शार्टकटकरिता दाबण्याजोगी कि किंवा किज बदला:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> 
      overview and start typing <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui> in the sidebar to open the panel.</p>
    </item>
    <item>
      <p>In the <gui>Keyboard Shortcuts</gui> section, select <gui>Customize Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Select the desired category, or enter a search term.</p>
    </item>
    <item>
      <p>Click the row for the desired action. The <gui>Set shortcut</gui>
      window will be shown.</p>
    </item>
    <item>
      <p>Hold down the desired key combination, or press <key>Backspace</key> to
      reset, or press <key>Esc</key> to cancel.</p>
    </item>
  </steps>


<section id="defined">
<title>पुर्वनिर्धारित शॉर्टकटस्</title>
  <p>बदलण्याजोगी, विभागांमध्ये एकत्र गट करण्याजोगी असंख्य पूर्व-संरचीत शार्टक्ट्स उपलब्ध आहेत:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accessibility</title>
  <tr>
	<td><p>मजकुराचा आकार कमी करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>उच्च विभिन्नता सुरु किंवा बंद करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>मजकुराचा आकार वाढवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>स्क्रीनवरचा कीबोर्ड सुरु किंवा बंद करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>स्क्रीन वाचक सुरु किंवा बंद करा</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>झुम सुरु किंवा बंद करा</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>झुम करा</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>झुम काढा</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>लाँचर्स</title>
  <tr>
	<td><p>घर फोल्डर</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-folder.svg"> <key>Explorer</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-computer.svg"> <key>Explorer</key> key symbol</media> or <key>Explorer</key></p></td>
  </tr>
  <tr>
	<td><p>लाँच गणक</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-calculator.svg"> <key>Calculator</key> key symbol</media> or <key>Calculator</key></p></td>
  </tr>
  <tr>
	<td><p>इमेल क्लाइंट लाँच करा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mail.svg"> <key>Mail</key> key symbol</media> or <key>Mail</key></p></td>
  </tr>
  <tr>
	<td><p>लाँच मदत ब्राउजर</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>वेब ब्राउजर लाँच करा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-world.svg"> <key>WWW</key> key symbol</media> or <media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-home.svg"> <key>WWW</key> key symbol</media> or <key>WWW</key></p></td>
  </tr>
  <tr>
	<td><p>शोधा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-search.svg"> <key>Search</key> key symbol</media> or <key>Search</key></p></td>
  </tr>
  <tr>
	<td><p>सेटिंग</p></td>
	<td><p><key>Tools</key></p></td>
  </tr>

</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>मार्गदर्शन</title>
  <tr>
	<td><p>सगळे सामान्य पटल लपवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the left</p></td>
	<td><p><keyseq><key>Super</key><key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move to workspace on the right</p></td>
	<td><p><keyseq><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor down</p></td>
	<td><p><keyseq><key>Shift</key><key xref="keyboard-key-super">Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the left</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor to the right</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window one monitor up</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पटल एक वर्कस्पेस डावीकडे हलवा</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key>
  <key>Page Up</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पटल एक वर्कस्पेस उजवीकडे हलवा</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Page Down</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Move window to last workspace</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>End</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>पटल वर्कस्पेस १वर हलवा</p></td>
	<td><p><keyseq><key>Shift</key><key>Super</key><key>Home</key></keyseq>
  </p></td>
  </tr>
  <tr>
	<td><p>पटल वर्कस्पेस २वर हलवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल वर्कस्पेस ३वर हलवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल वर्कस्पेस ४वर हलवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>ॲप्लिकेशन्स बदला</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>सिस्टीम नियंत्रण बदला</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>सिस्टीम नियंत्रण थेट बदला</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Switch to last workspace</p></td>
	<td><p><keyseq><key>Super</key><key>End</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>वर्कस्पेस १वर या</p></td>
	<td><p><keyseq><key>Super</key><key>Home</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>वर्कस्पेस २वर या</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>वर्कस्पेस ३वर या</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>वर्कस्पेस ४वर या</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
        <td><p>पटल बदला</p></td>
        <td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल थेट बदला</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>ॲपचा पटल थेट बदला</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>ॲप्लिकेशनचा पटल बदला</p></td>
	<td><p>बंद</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>स्क्रीनशॉटस्</title>
  <tr>
	<td><p>Save a screenshot of a window</p></td>
	<td><p><keyseq><key>Alt</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Save a screenshot of the entire screen</p></td>
	<td><p><keyseq><key>Shift</key><key>Print</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Launch the screenshot tool</p></td>
	<td><p><key>छापा</key></p></td>
  </tr>
  <tr>
        <td><p>Record a short screencast</p></td>
        <td><p><keyseq><key>Shift</key><key>Ctrl</key><key>Alt</key><key>R</key> </keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>आवाज आणि मिडीया</title>
  <tr>
	<td><p>निष्कसित करा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-eject.svg"> <key>Eject</key> key symbol</media> (Eject)</p></td>
  </tr>
  <tr>
	<td><p>मिडीया प्लेयर लाँच करा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-media.svg"> <key>Media</key> key symbol</media> (Audio media)</p></td>
  </tr>
  <tr>
	<td><p>Microphone mute/unmute</p></td>
	<td/>
  </tr>
  <tr>
	<td><p>पुढचा ट्रॅक</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-next.svg"> <key>Next</key> key symbol</media> (Audio next)</p></td>
  </tr>
  <tr>
	<td><p>पॉझ प्लेबॅक</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-pause.svg"> <key>Pause</key> key symbol</media> (Audio pause)</p></td>
  </tr>
  <tr>
	<td><p>प्ले (किंवा प्ले/पॉझ्)</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-play.svg"> <key>Play</key> key symbol</media> (Audio play)</p></td>
  </tr>
  <tr>
	<td><p>आधीचा ट्रॅक</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-previous.svg"> <key>Previous</key> key symbol</media> (Audio previous)</p></td>
  </tr>
  <tr>
	<td><p>प्लेबॅक थांबवा</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-stop.svg"> <key>Stop</key> key symbol</media> (Audio stop)</p></td>
  </tr>
  <tr>
	<td><p>आवाज कमी</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-voldown.svg"> <key>Volume Down</key> key symbol</media> (Audio lower volume)</p></td>
  </tr>
  <tr>
	<td><p>आवाज बंद</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-mute.svg"> <key>Mute</key> key symbol</media> (Audio mute)</p></td>
  </tr>
  <tr>
	<td><p>आवाज जास्ती</p></td>
	<td><p><media its:translate="no" type="image" mime="image/svg" src="figures/keyboard-key-volup.svg"> <key>Volume Up</key> key symbol</media> (Audio raise volume)</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>सिस्टीम</title>
  <tr>
        <td><p>सक्रीय सूचनाकरिता फोकस करा</p></td>
        <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>स्क्रीन लॉक करा</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the Power Off dialog</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>ॲप्लिकेशन मेनु उघडा</p></td>
        <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>Restore the keyboard shortcuts</p></td>
        <td><p><keyseq><key>Super</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>सर्व ॲप्लिकेशन्स दाखवा</p></td>
        <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>कार्य सारांश दाखवा</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the notification list</p></td>
	<td><p><keyseq><key>Super</key><key>V</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Show the overview</p></td>
	<td><p><keyseq><key>Super</key><key>S</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>रन कमांड प्राम्प्ट दाखवा</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>टाइपिंग</title>
  <tr>
  <td><p>पुढील इंपुट स्रोतचा वापर करा</p></td>
  <td><p><keyseq><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>मागील इंपुट स्रोतचा वापर करा</p></td>
  <td><p><keyseq><key>Shift</key><key>Super</key><key>Space</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>पटले</title>
  <tr>
	<td><p>पटल मेनु लागू करा</p></td>
	<td><p><keyseq><key>Alt</key><key>स्पेस</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पटल बंद करा</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>पटल लपवा</p></td>
        <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>इतर पटलांच्या तुलनेत पटल खाली करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल मोठे करा</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पटल आडवेरित्या वाढवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल उभेरित्या वाढवा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटल हलवा</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>इतर पटलांच्या तुलनेत पटल उंच करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>समाविष्टीत असल्यास पटल उंच करा, नाहीतर कमी करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>पटलाचे आकार बदला</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पटल पूर्वस्थितीत आणा</p></td>
        <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>पडदाभर मोड टॉगल करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
	<td><p>कमाल स्तर टॉगल करा</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>सर्व कार्यक्षेत्रांवर किंवा एकावर पटल टॉगल करा</p></td>
	<td><p>बंद</p></td>
  </tr>
  <tr>
        <td><p>डावीकडील विभाजनचे अवलोकन</p></td>
        <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
        <td><p>उजवीकडील विभाजनचे अवलोकन</p></td>
        <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>मनपसंत शॉर्टकटस्</title>

  <p>To create your own application keyboard shortcut in the
  <gui>Keyboard</gui> settings:</p>

  <steps>
    <item>
      <p>Select <gui>Custom Shortcuts</gui>.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut</gui> button if no custom
      shortcut is set yet. Otherwise click the <gui style="button">+</gui>
      button. The <gui>Add Custom Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Click the <gui style="button">Add Shortcut…</gui> button. In the
      <gui>Add Custom Shortcut</gui> window, hold down the desired
      shortcut key combination.</p>
    </item>
    <item>
      <p><gui>समाविष्ट</gui> क्लिक करा.</p>
    </item>
  </steps>

  <p>टाइप केलेले आदेश नाव वैध प्रणाली आदेश पाहिजे. टर्मिनल उघडून आणि टाइप करून आदेश कार्यरत होते. ॲप्लिकेशन उघडणाऱ्या आदेशकडे ॲप्लिकेशन प्रमाणेच समान नाव असू शकत नाही.</p>

  <p>If you want to change the command that is associated with a custom
  keyboard shortcut, click the row of the shortcut. The
  <gui>Set Custom Shortcut</gui> window will appear, and you can edit the
  command.</p>

</section>

</page>
