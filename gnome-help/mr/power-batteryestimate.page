<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="power-batteryestimate" xml:lang="mr">

  <info>

    <link type="guide" xref="power#faq"/>
    <link type="guide" xref="status-icons#batteryicons"/>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.20" date="2016-06-15" status="final"/>
    <revision version="gnome:40" date="2021-03-22" status="candidate"/>

    <desc><gui>बॅटरी चिन्हावर</gui> क्लिक करून अंदाजे बॅटरी आयु दाखवले जाते.</desc>

    <credit type="author">
      <name>GNOME डॉक्युमेंटेशन प्रकल्प</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aniket Deshpande &lt;djaniketster@gmail.com&gt;, 2013; संदिप शेडमाके</mal:name>
      <mal:email>sshedmak@redhat.com</mal:email>
      <mal:years>२०१३.</mal:years>
    </mal:credit>
  </info>

<title>अंदाजे बॅटरी आयु चुकिची आहे</title>

<p>उर्वरित बँटरि आयुची तपासणी करतेवेळी, सादर केलेला उर्वरित वेळ आणि बॅटरी किती वेळ कार्यरत राहते यात फारक आढळून येईल. याचे कारण म्हणजे उर्वरित बॅटरी आयुचा फक्त अंदाज शक्य आहे. साधारणतया, अंदाज वेळोवेळी सुधारित होते.</p>

<p>In order to estimate the remaining battery life, a number of factors must be
taken into account. One is the amount of power currently being used by the
computer: power consumption varies depending on how many programs you have
open, which devices are plugged in, and whether you are running any intensive
tasks (like watching high-definition video or converting music files, for
example). This changes from moment to moment, and is difficult to predict.</p>

<p>दुसरे कारण म्हणजे बॅटरी कशी डिसचार्ज होते. काही बॅटरीज पटकन चार्ज गमवतात. बॅटरी कशी डिसचार्ज होते याविषयी योग्य माहितीविना, फक्त उर्वरित बॅटरी आयुचा अंदाज लावणे शक्य आहे.</p>

<p>बॅटरी डिसचार्ज होतेवेळी, पावर व्यवस्थापक त्याचे डिसचार्ज गुणधर्म शोधतो आणि बॅटरी आयुचे उत्तम अंदाज घेण्यास शिकतो. तरी, ते कधिही संपूर्णतया अचूक नसतात.</p>

<note>
  <p>अगदी हास्यास्पद बॅटरी आयु अंदाज आढळले असल्यास (जसे कि, शंभर दिवस), पावर व्यवस्थापकाकडे योग्य अंदाजकरिता कदाचित डाटा नसावे.</p>
  <p>पावर काढून टाकल्यानंतर आणि थोड्यावेळकरिता बॅटरीवर लॅपटॉप चालवत असल्यास, जोडणी करा आणि पुन्हा चार्ज करा, पावर व्यवस्थापकाकडे आवश्यक डाटा प्राप्त होईल.</p>
</note>

</page>
