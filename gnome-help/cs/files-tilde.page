<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="files-tilde" xml:lang="cs">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="files-hidden"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jedná se o záložní soubory. Standardně bývají skryté.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Co je to za soubor s <file>~</file> na konci názvu?</title>

  <p>Soubory s <file>~</file> na konci názvu (například <file>příklad.txt~</file>) jsou automaticky vytvořené záložní kopie dokumentů upravovaných v textovém editoru <app>gedit</app> a v dalších aplikacích. Můžete je bez následků smazat, ale když je v počítači necháte, také se nic nestane.</p>

  <p>Tyto soubory jsou ve výchozím stavu skryté. Pokud je vidíte, je to proto, že jste buď vybrali <gui>Zobrazovat skryté soubory</gui> (v nabídce zobrazení na nástrojové liště aplikace <app>Soubory</app>) nebo jste zmáčkli <keyseq><key>Ctrl</key> <key>H</key></keyseq>. Znovu je skrýt můžete zopakováním jednoho ze dvou právě zmíněných postupů.</p>

  <p>S těmito soubory je nakládáno stejně, jako s normálními skrytými soubory. Viz <link xref="files-hidden"/> ohledně rady, jak smazat skryté soubory.</p>

</page>
