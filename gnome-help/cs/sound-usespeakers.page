<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="cs">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision version="gnome:40" date="2021-02-26" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak připojit reproduktory nebo sluchátka a jak vybrat výchozí výstupní zvukové zařízení.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Používání rozličných reproduktorů a sluchátek</title>

  <p>Se svým počítačem můžete používat externí reproduktory nebo sluchátka. Obvykle se připojují konektorem s kruhovým průřezem (<em>jack, kolík</em>) nebo do USB.</p>

  <p>Pokud mají vaše reproduktory nebo sluchátka konektor s kulatým průřezem (jack), zastrčte jej do příslušného zvukového vstupu počítače. Většina počítačů má dvě zvukové zdířky: jednu pro mikrofon a druhou pro sluchátka/reproduktory. Zdířka pro sluchátka má obvykle světle zelenou barvu nebo je označena symbolem sluchátek. Po zastrčení by měly být reproduktory nebo sluchátka obvykle použity jako výchozí. Když se tak nestane, podívejte se níže jak vybrat výchozí výstupní zvukové zařízení.</p>

  <p>Některé počítače mají vícekanálový výstup kvůli vytváření prostorového efektu zvuku (surround). Používají k tomu obvykle více konektorů typu stereo jack, které jsou rozlišeny barevně. Pokud si nejste jisti, kterou zdířku na co použít, můžete otestovat zvukový výstup v nastaveních zvuku.</p>

  <p>V případě, že máte reproduktory nebo sluchátka do USB nebo analogová sluchátka ve zvukové karty do USB, použijte k připojení libovolný port USB na počítači. Zařízení do USB budou vystupovat jako samostatné zvukové zařízení a můžete určit, které se má používat jako výchozí.</p>

  <steps>
    <title>Výběr výchozího výstupního zvukového zařízení</title>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Zvuk</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Zvuk</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Výstup</gui> vyberte zařízení, které chcete používat.</p>
    </item>
  </steps>

  <p>Použijte tlačítko <gui style="button">Testovat</gui>, abyste si ověřili, že jsou všechny připojené do správných zdířek a fungují.</p>

</page>
