<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="about-hardware" xml:lang="cs">

  <info>
    <link type="guide" xref="about" group="hardware"/>

    <revision pkgversion="44.0" date="2023-02-04" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Discover information about the hardware installed on your
    system.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Discover information about your system hardware</title>

  <p>Knowing about what hardware is installed may help you understand if new
  software or hardware will be compatible with your system.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>O systému</gui>.</p>
    </item>
    <item>
      <p>Press <gui>About</gui> to open the panel.</p>
    </item>
    <item>
      <p>Look at the information that is listed under <gui>Hardware
      Model</gui>, <gui>Memory</gui>, <gui>Processor</gui> and so on.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Information can be copied from each item by selecting it and then
    copying to the clipboard. This makes it easy to share information about
    your system with others.</p>
  </note>

</page>
