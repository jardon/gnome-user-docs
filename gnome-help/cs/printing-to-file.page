<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="printing-to-file" xml:lang="cs">

  <info>
    <link type="guide" xref="printing" group="#last"/>

    <revision pkgversion="3.8" date="2013-03-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author copyright">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak uložit dokument v podobě souboru PDF, PostScript nebo SVG místo aby odešel na tiskárnu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Tisk do souboru</title>

  <p>Místo abyste dokument vytiskli na tiskárně, můžete jej vytisknout do souboru. Tím se vytvoří soubor <sys>PDF</sys>, <sys>PostScript</sys> nebo <sys>SVG</sys> obsahující dokument. To se může hodit, když chcete dokument přesunout na jiný počítač nebo jej s někým sdílet.</p>

  <steps>
    <title>Když chcete tisknout do souboru:</title>
    <item>
      <p>Otevřete dialogové okno tisku zmáčknátím <keyseq><key>Ctrl</key><key>P</key></keyseq>.</p>
    </item>
    <item>
      <p>V <gui>Tiskárnách</gui> na kartě <gui style="tab">Obecné</gui> vyberte <gui>Tisknout do souboru</gui>.</p>
    </item>
    <item>
      <p>Abyste mohli změnit výchozí název souboru a místo, kam se má uložit, klikněte na název souboru pod výběrem tiskárny. Po dokončení volby klikněte na <gui style="button">Vybrat</gui>.</p>
    </item>
    <item>
      <p>Výchozím typem souboru pro dokument je <sys>PDF</sys>. Když chcete použít jiný <gui>Výstupní formát</gui>, vyberte <sys>PostSkript</sys> nebo <sys>SVG</sys>.</p>
    </item>
    <item>
      <p>Nastavte si další předvolby stránky.</p>
    </item>
    <item>
      <p>Zmáčknutím <gui style="button">Tisk</gui> soubor uložíte.</p>
    </item>
  </steps>

</page>
