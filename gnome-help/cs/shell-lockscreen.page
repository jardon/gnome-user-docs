<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-lockscreen" xml:lang="cs">

  <info>
    <link type="guide" xref="shell-overview#apps"/>
    <link type="guide" xref="shell-notifications#lock-screen-notifications"/>

    <revision pkgversion="3.6.1" date="2012-11-11" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.36.1" date="2020-04-18" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak okrasná a zároveň funkční uzamknutá obrazovka zprostředkovává užitečné informace.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Uzamknutá obrazovka</title>

  <p>Zamknutá obrazovka znamená, že můžete vidět, co se děje po dobu, co je počítač zamknutý a máte možnost získat souhrnné informace o tom, co se událo, když jste byli pryč. Uzamknutá obrazovka poskytuje užitečné informace:</p>

  <list>
<!--<item><p>the name of the logged-in user</p></item> -->
    <item><p>datum a čas a některá upozornění</p></item>
    <item><p>stav baterie a sítě</p></item>
<!-- No media control anymore on lock screen, see BZ #747787: 
    <item><p>the ability to control media playback — change the volume, skip a
    track or pause your music without having to enter a password</p></item> -->
  </list>

  <p>Když chcete počítač odemknout, klikněte jednou myší nebo touchpadem, nebo zmáčkněte <key>Esc</key> nebo <key>Enter</key>. Tím se odhalí přihlašovací obrazovka, na které můžete zadat své heslo a počítač tím odemknout. Případně stačí začít heslo rovnou psát a přihlašovací obrazovka se během psaní sama objeví. Pokud je počítač nastaven pro používání více uživateli, máte také možnost přepnout se na jiného uživatele vpravo dole na přihlašovací obrazovce.</p>

  <p>Jak skrýt upozornění na uzamknuté obrazovce najdete v kapitole <link xref="shell-notifications#lock-screen-notifications"/>.</p>

</page>
