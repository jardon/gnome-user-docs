<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="cs">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Kalibrace fotoaparátu je důležitá, aby snímal věrné barvy.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Jak můžu zkalibrovat svůj fotoaparát?</title>

  <p>Fotoaparáty se kalibrují pořízením fotografie kalibračního obrazce za stanovených světelných podmínek. Výsledek se převede ze souboru RAW do souboru TIFF a ten se pak použije ke kalibraci fotoaparátu v ovládacím panelu <gui>Barevnost</gui>.</p>
  <p>Obrázek TIFF musíte oříznout tak, by byl vidět jen kalibrační obrazec. Dejte při tom pozor, aby zůstaly viditelné bílé a černé okraje. Kalibrace nebude fungovat, když bude obrázek vzhůru nohama nebo příliš zdeformovaný.</p>

  <note style="tip">
    <p>Výsledný profil bude platný jen za světelných podmínek, za kterých jste získali původní snímek. To znamená, že budete asi potřebovat více profilů, pro světelné podmínky <em>studia</em>, <em>slunečního dne</em> a <em>zamračeného dne</em>.</p>
  </note>

</page>
