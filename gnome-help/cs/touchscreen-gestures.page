<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="touchscreen-gestures" xml:lang="cs">

  <info>
    <link type="guide" xref="mouse" group="#last"/>

    <revision pkgversion="3.33" date="2019-07-20" status="candidate"/>

    <!--
    For 41: https://gitlab.gnome.org/GNOME/gnome-user-docs/-/issues/121
    -->
    <revision version="gnome:40" date="2021-03-18" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zacházet s uživatelským prostředím pomocí gest na touchpadu nebo dotykové obrazovce.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Používání gest na touchpadu a dotykové obrazovce</title>

  <p>Vícedotyková gesta se dají používat na touchpadu nebo dotykové obrazovce jak pro ovládání systému, tak i aplikací.</p>

  <p>Gesta využívá řada aplikací. V <app>Prohlížeči dokumentů</app> se dá pomocí gest měnit přiblížení dokumentu a listovat v něm a <app>Prohlížeč obrázků</app> umožňuje gesty měnit přiblížení, otáčet a procházet obrázky.</p>

<section id="system">
  <title>Gesta pro celý systém</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-overview.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Otevřít přehled Činnosti a zobrazení aplikací</em></p>
    <p>Pro otevření přehledu činností přiložte tři prsty na touchpad nebo dotykovou obrazovku a udělejte pohyb směren nahoru.</p>
    <p>Pro zobrazení aplikací přiložte tři prsty a udělejte znovu pohyb vzhůru.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-workspaces.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Přepnout na jinou pracovní plochu</em></p>
    <p>Přiložte tři prsty na touchpad nebo dotykovou obrazovku a udělejte pohyb doleva nebo doprava.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-unfullscreen.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Opustit celou obrazovku</em></p>
    <p>Pro opuštění režimu celé obrazovky libovolného okna táhněte prstem od horní hrany směrem dolů.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-osk.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Zobrazit klávesnici na obrazovce</em></p>
    <p>Pro zobrazení <link xref="keyboard-osk">klávesnice na obrazovce</link> táhněte na dotykové obrazovce od spodní hrany obrazovky. Předpokladem je, že je klávesnice na obrazovce povolená v nastaveních.</p></td>
  </tr>
</table>

</section>

<section id="apps">
  <title>Gesta pro aplikace</title>

<table rules="rows" frame="bottom">
  <tr>
    <td><p><media type="image" src="figures/touch-tap.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Otevřít položku, spustit aplikaci, přehrát hudbu</em></p>
    <p>Klepněte na položku.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-hold.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Vybrat položku a vypsat činnosti, které se dají provést</em></p>
    <p>Přitiskněte prst a držte po dobu jedné dvou sekund.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-scroll.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Posunout obsah plochy na obrazovce</em></p>
    <p>Táhněte prstem za soustavného dotyku.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-pinch-to-zoom.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Změnit úroveň přiblížení zobrazení (<app>Mapy</app>, <app>Fotky</app>)</em></p>
    <p>Semkněte nebo roztáhněte dva prsty – dotkněte se povrchu dvěma prsty a posuňte je blíže k sobě nebo dále od sebe.</p></td>
  </tr>
  <tr>
    <td><p><media type="image" src="figures/touch-rotate.svg" width="128" its:translate="no"/></p></td>
    <td><p><em>Otočit fotku</em></p>
    <p>Kružte dvěma prsty – dotkněte se povrchu dvěma prsty a otáčejte s nim po kružnici.</p></td>
  </tr>
</table>

</section>

</page>
