<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="color-calibrate-screen" xml:lang="cs">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-camera"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-04" status="candidate"/>
    <revision pkgversion="3.28" date="2018-04-05" status="review"/>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kalibrace obrazovky je důležitá, aby zobrazovala věrné barvy.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Jak můžu zkalibrovat svoji obrazovku?</title>

  <p>Můžete si zkalibrovat obrazovku, aby zobrazovala barvy věrněji. To je obzvláště vhodné, pokud se zabýváte digitální fotografií, designem nebo grafikou.</p>

  <p>K tomu potřebujete buď kolorimetr nebo spektrometr. Obě zařízení se používají k vytvoření profilu obrazovky, ale fungují trochu odlišným způsobem.</p>

  <steps>
    <item>
      <p>Ujistěte se, že kalibrovací zařízení je připojené k vašemu počítači.</p>
    </item>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Klikněte na <gui>Nastavení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Barvy</gui> v postranním panelu otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Vyberte svoji obrazovku.</p>
    </item>
    <item>
      <p>Kalibraci zahájíte zmáčknutím <gui style="button">Kalibrovat…</gui></p>
    </item>
  </steps>

  <p>Parametry obrazovky se postupem času mění: zadní podsvětlení displeje LCD pomocí světelných trubic poklesne na polovinu přibližně každých 18 měsíců a jak stárne, získává odstín víc do žluta. To znamená, že když se na panelu <gui>Barvy</gui> objeví ikona [!], měli byste svoji obrazovku znovu zkalibrovat.</p>

  <p>Obrazovky s podsvětlením pomocí LED se s postupem času také mění, ale ne tak rychle jako u světelných trubic.</p>

</page>
