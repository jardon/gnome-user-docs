<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="user-delete" xml:lang="cs">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <link type="seealso" xref="user-add"/>
    <link type="seealso" xref="user-admin-explain"/>

    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak odebrat uživatele, který počítač nadále nebude používat.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Smazání uživatelského účtu</title>

  <p>Do počítače můžete <link xref="user-add">přidat více uživatelských účtu</link>. Naopak, když někdo počítač již nadále nebudu používat, můžete jeho uživatelský účet smazat.</p>

  <p>Abyste mohli odstranit účet uživatele, potřebujete <link xref="user-admin-explain">oprávnění správce</link>.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Uživatelé</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Uživatelé</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>Zmáčkněte <gui style="button">Odemknout</gui> v pravém horním rohu a po vyzvání zadejte heslo.</p>
    </item>
    <item>
      <p>Click on the user account that you want to delete under
      <gui>Other Users</gui>.</p>
    </item>
    <item>
      <p>Press the <gui style="button">Remove User...</gui> button to delete
      that user account.</p>
    </item>
    <item>
      <p>Každý uživatel má svoji vlastní domovskou složku pro své soubory a nastavení. Můžete si vybrat, jestli se má domovská složka uživatele zachovat nebo smazat. Na <gui>Smazat soubory</gui> klikněte, když si jste jisti, že již nebudou zapotřebí a chcete uvolnit místo na disku. Soubory pak bude nevratně smazány a nepůjde je obnovit. Zvažte také jejich zazálohování na externí úložné zařízení, než je smažete.</p>
    </item>
  </steps>

</page>
