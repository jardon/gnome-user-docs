<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="cs">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Jak a kde hlásit problémy s těmito nápovědami.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Petr Kovář</name>
      <email>pknbe@volny.cz</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>
  <title>Účast na vylepšení této příručky</title>

  <section id="submit-issue">

   <title>Hlášení záležitostí</title>

   <p>Tato dokumentace s nápovědou byla vytvořena komunitou dobrovolníků. Pokud byste se chtěli přidat, budete vítáni. V případě, že jste v těchto stránkách s nápovědou narazili na nějaký problém (překlep, nesprávné instrukce nebo téma, o kterém není žádná zmínka), můžete nahlásit <em>novou záležitost</em>. To se dělá na <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">v systému pro sledování záležitostí</link>.</p>

   <p>Abyste mohli hlásit zálažitosti a dostávat e-mailem informace o jejich řešení, musíte se nejdříve zaregistrovat. Pokud ještě účet nemáte, kliknutím na <gui><link href="https://gitlab.gnome.org/users/sign_in">Sign in / Register</link></gui> si jej vytvořte.</p>

   <p>Když jej máte založený, přihlaste se, vraťte se do <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">systému pro sledování záležitostí dokumentace</link> a klikněte na <gui><link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues/new">New issue</link></gui>. Než novou záležitost nahlásíte, <link href="https://gitlab.gnome.org/GNOME/gnome-user-docs/issues">projděte si</link> prosím ty, co existují, jestli mezi nimi již nějaká podobná není.</p>

   <p>Než záležitost odešlete, zvolte v nabídce <gui>Labels</gui> vhodný štítek. Pokud hlásíte chybu v této dokumentaci, vyberte štítek <gui>gnome-help</gui>. Pokud si nejste jisti, co přesně vybrat, ponechte pole nevyplněné.</p>

   <p>Jestliže budete žádat o nápovědu k nějakému tématu, u kterého máte pocit, že není pokryté, vyberte v nabídce štítek <gui>Feature</gui>. Vyplňte název <gui>Title</gui> a popis <gui>Description</gui> a klikněte na <gui>Submit issue</gui>.</p>

   <p>Vaše záležitost dostane přiděleno identifikační číslo a její stav se bude postupně měnit podle toho, jak bude postupovat její řešení. Děkujeme, že pomáháte GNOME vylepšovat!</p>

   </section>

   <section id="contact-us">
   <title>Jak nás kontaktovat</title>

   <p>Jestli se chcete dozvědět více o tom, jak se zapojit do dokumentačního týmu, můžete poslat <link href="mailto:gnome-doc-list@gnome.org">e-mail</link> do poštovní konference o dokumentaci GNOME.</p>

   </section>
</page>
