<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-mag" xml:lang="cs">

  <info>
    <link type="guide" xref="a11y#vision" group="lowvision"/>

    <revision pkgversion="3.7.1" date="2012-11-10" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-21" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <desc>Jak si přiblížit svoji obrazovku, abyste lépe viděli věci na ní.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Zvětšení části obrazovky</title>

  <p>Zvětšení obrazovky je něco jiného než jen zvětšení <link xref="a11y-font-size">velikosti textu</link>. Tato funkce funguje jako zvětšovací lupa, s kterou můžete pohybovat po obrazovce a zvětšovat její jednotlivé části.</p>

  <steps>
    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Přístupnost</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na položku <gui>Přístupnost</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Zrak</gui> zmáčkněte <gui>Přiblížení</gui>.</p>
    </item>
    <item>
      <p>V pravém horním rohu okna <gui>Volby přiblížení</gui> přepněte vypínač <gui>Přiblížit</gui> do polohy zapnuto.</p>
      <!--<note>
        <p>The <gui>Zoom</gui> section lists the current settings for the
        shortcut keys, which can be set in the <gui>Accessibility</gui>
        section of the <link xref="keyboard-shortcuts-set">Shortcuts</link> tab
        on the <gui>Keyboard</gui> panel.</p>
      </note>-->
    </item>
  </steps>

  <p>Nyní se můžete posouvat po ploše obrazovky. Pohybem myší k okrajům obrazovky posouváte přiblíženou oblast v příslušném směru, takže si můžete zobrazit požadovanou část.</p>

  <note style="tip">
    <p>Funkci lupy můžete rychle zapnout a vypnout kliknutím na <link xref="a11y-icon">ikonu zpřístupnění</link> na horní liště a následným vybráním <gui>Přiblížit</gui>.</p>
  </note>

  <p>Můžete měnit úroveň přiblížení, sledování ukazatele myši a umístění zvětšeného zobrazení na obrazovce. To vše se mění na kartě <gui>Lupa</gui> v okně s nastavením <gui>Volby přiblížení</gui>.</p>

  <p>Můžete si aktivovat zaměřovací kříž, který vám pomůže najít ukazatel myši nebo touchapdu. Zapněte si jej a upravte si jeho délku, barvu a tloušťku na kartě <gui>Zaměřovací kříž</gui> v okně s nastavením <gui>Volby přiblížení</gui>.</p>

  <p>Můžete se přepnout do inverzního zobrazení nebo do <gui>Bílé na černém</gui> a upravit si jas, kontrast a sytost při zvětšení lupou. Kombinace těchto voleb je užitečná pro lidi se slabozrakostí, různými stupni světloplachosti nebo jen pro používání počítače za nepříznivých světelných podmínek. Abyste mohli zapnout a měnit tyto volby, vyberte kartu <gui>Barevné efekty</gui> v okně s nastavením <gui>Volby přiblížení</gui>.</p>

</page>
