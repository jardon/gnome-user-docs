<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-mode" xml:lang="cs">

  <info>
    <revision pkgversion="3.33" date="2019-07-21" status="candidate"/>
    <revision version="gnome:42" status="final" date="2022-04-02"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přepínat grafický tablet mezi režimem tabletu a myši.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Nastavení režimu sledování stopy u grafického tabletu</title>

<p><gui>Tablet Mode</gui> determines how the stylus is mapped to the screen.</p>

<steps>
  <item>
    <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Grafický tablet</gui>.</p>
  </item>
  <item>
    <p>Kliknutím na položku <gui>Grafický tablet</gui> otevřete příslušný panel.</p>
    <note style="tip"><p>If no tablet is detected, you’ll be asked to
    <gui>Please plug in or turn on your Wacom tablet</gui>. Click
    <gui>Bluetooth</gui> in the sidebar to connect a wireless tablet.</p></note>
  </item>
  <item>
    <p>Choose between tablet (or absolute) mode and touchpad (or relative)
    mode. For tablet mode, switch <gui>Tablet Mode</gui> to on.</p>
  </item>
</steps>

  <note style="info"><p>V <em>absolutním</em> režimu je každý bod na tabletu namapován na bod na obrazovce. Například levý horní roh na obrazovce vždy odpovídá tomu stejnému bodu na tabletu.</p>
  <p>V <em>relativním</em> režimu, když zvednete stylus nad tablet a přiložíte jej v jiném místě, tak se ukazatel na obrazovce nepřemístí. Je to stejný způsob, jakým funguje myš.</p>
  </note>

</page>
