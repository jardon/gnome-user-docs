<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-inklevel" xml:lang="cs">

  <info>
    <link type="guide" xref="printing"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Anita Reitere</name>
      <email>nitalynx@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak zkontrolovat zbývající množství inkoustu nebo toneru v tiskové kazetě.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Jak mohu zkontrolovat stav inkoustu nebo toneru v své tiskárně?</title>

  <p>To, jak zkontrolovat zbývající množství inkoustu nebo toneru v tiskárně, závisí na značce a modelu tiskárny a ovladačích a aplikacích nainstalovaných v počítači.</p>

  <p>Některé tiskárny mají vestavěný displej pro zobrazování úrovně zbývajícího inkoustu a dalších informací.</p>

  <p>Některé tiskárny hlásí úroveň zbývajícího toneru nebo inkoustu počítači a údaje pak můžete najít v panelu <gui>Tiskárny</gui> v <app>Nastaveních</app>. Pokud jsou informace dostupné, uvidíte je v podrobnostech o tiskárně.</p>

  <p>Ovladače a nástroj pro oznamování stavu pro většinu tiskáren HP je poskytován projektem HP Linux Imaging and Printing. Ostatní výrobci mohou poskytovat vlastní ovladače s podobnými funkcemi.</p>

  <p>Volitelně si můžete nainstalovat aplikaci kontrolující a sledujíc úroveň hladiny inkoustů. <app>Inkblot</app> ukazuje stav pro většinu tiskáren HP, Epson a Canon. Podívejte se na <link href="http://libinklevel.sourceforge.net/#supported">seznam podporovaných modelů</link>, jestli je mezi nimi i vaše tiskárna. Jinou aplikací se stejným účelem pro tiskárny Epson a některé jiné je <app>mtink</app>.</p>

  <p>Některé tiskárny nejsou zatím v Linuxu plnohodnotně podporovány a další zase nejsou navržené tak, aby úroveň iknoustu nebo toneru hlásily.</p>

</page>
