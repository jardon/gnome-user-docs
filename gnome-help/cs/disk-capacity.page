<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="cs">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Rafael Fontenelle</name>
      <email>rafaelff@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Jak použít <app>Analyzátor využití disku</app>, <app>Sledování systému</app> nebo <app>Využití</app> ke zjištění volného místa a kapacity.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

<title>Kontrola volného místa na disku</title>

  <p>Kolik vám na disku zbývá volného místa můžete zjistit pomocí <app>Analyzátoru využití disku</app>, <app>Sledování systému</app> nebo <app>Využití</app>.</p>

<section id="disk-usage-analyzer">
<title>Kontrola pomocí Analyzátoru využití disku</title>

  <p>Když chcete zjistit volné místo na disku a jeho kapacitu pomocí <app>Analyzátoru využití disku</app>: </p>

  <list>
    <item>
      <p>Z přehledu <gui>Činnosti</gui> otevřete <app>Analyzátor využití disku</app>. Okno zobrazí seznam umístění souborů společně s použitým místem a celkovou kapacitou u každého.</p>
    </item>
    <item>
      <p>Kliknutím na položku v seznamu se zobrazí podrobný přehled využití pro tuto položku. Kliknutím na tlačítko nabídky a pak volbou <gui>Projít složku…</gui> můžete provést analýzu jiného umístění.</p>
    </item>
  </list>
  <p>Informace jsou zobrazeny podle <gui>Složky</gui>, <gui>Velikosti</gui>, <gui>Obsahu</gui> a toho, kdy byla data naposledy <gui>Změněna</gui>. Podrobnější informace viz <link href="help:baobab"><app>Analyzátor využití disku</app></link>.</p>

</section>

<section id="system-monitor">

<title>Kontrola pomocí Sledování systému</title>

  <p>Když chcete zjistit volné místo na disku a jeho kapacitu pomocí <app>Sledování systému</app>: </p>

<steps>
 <item>
  <p>Z přehledu <gui>Činnosti</gui> otevřete aplikaci <app>Sledování systému</app>.</p>
 </item>
 <item>
  <p>Vyberte kartu <gui>Souborové systémy</gui>, aby se zobrazily diskové oddíly a využití místa na nich. Zobrazeny jsou údaje <gui>Celkem</gui>, <gui>Volné</gui>, <gui>K dispozici</gui> a <gui>Použito</gui>.</p>
 </item>
</steps>
</section>

<section id="usage">
<title>Kontrola pomocí aplikace Využití</title>

  <p>Když chcete zjistit volné místo na disku a jeho kapacitu pomocí <app>Využití</app>: </p>

<steps>
  <item>
    <p>Z přehledu <gui>Činnosti</gui> otevřete aplikaci <app>Využití</app>.</p>
  </item>
  <item>
    <p>Vyberte kartu <gui>Úložiště</gui>, abyste viděli celkové <gui>Použité</gui> a <gui>Dostupné</gui> místo na disku, stejně jako použité <gui>Operačním systémem</gui> a běžnými uživatelskými složkami.</p>
  </item>
</steps>

<note style="tip">
  <p>Místo je možné uvolnit v uživatelských složkách a podsložkách zaškrtnutím políčka vedle názvu složky.</p>
</note>
</section>

<section id="disk-full">

<title>Co dělat, když je disk plný?</title>

  <p>Pokud je disk příliš zaplněný, měli byste:</p>

 <list>
  <item>
   <p>Smazat soubory, které nejsou důležité nebo je již nebudete nikdy potřebovat.</p>
  </item>
  <item>
   <p>Provést <link xref="backup-why">zálohu</link> důležitých souborů, které běžně nepotřebujete a z disku je smazat.</p>
  </item>
 </list>
</section>

</page>
