<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-mobile" xml:lang="cs">

  <info>
    <link type="guide" xref="hardware-phone#setup"/>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.14" date="2014-11-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>
    <revision pkgversion="3.24" date="2017-03-26" status="final"/>
    <revision pkgversion="3.33.3" date="2019-07-19" status="candidate"/>

    <credit type="author">
      <name>Dokumentační projekt GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <desc>Jak použít telefon nebo modem k připojení k širokopásmové mobilní síti.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Mobilní širokopásmové připojení</title>

  <p>Nastavte si připojení k mobilní síti (3G) pomocí modemu 3G vestavěného ve vašem počítači, mobilního telefonu nebo externího modemu.</p>

  <note style="tip">
  <p>Většina mobilních telefonů má nastavení nazývané <link xref="net-tethering">Sdílené připojení přes USB</link> (USB tethering), které nevyžaduje na počítači žádná nastavení a je obecně lepším způsobem, jak se připojit k mobilní síti.</p>
  </note>

  <steps>
    <item><p>Pokud nemáte vestavěný modem 3G, připojte svůj telefon nebo modem do portu USB na počítači.</p>
    </item>
    <item>
    <p>Otevřete <gui xref="shell-introduction#systemmenu">systémovou nabídku</gui> vpravo na horní liště.</p>
  </item>
  <item>
    <p>Vyberte <gui>Mobilní připojení je vypnuto</gui>. Oddíl <gui>Mobilní připojení</gui> se v nabídce rozbalí.</p>
      <note>
        <p>Když se <gui>Mobilní širokopásmové</gui> v nabídce systému neobjeví, ujistěte se, jestli není zařízení nastavené, aby se připojovalo jako „Mass Storage“.</p>
      </note>
    </item>
    <item><p>Vyberte <gui>Připojit</gui>. Když se připojujete poprvé, spustí se průvodce <gui>Nastavení mobilního širokopásmového připojení</gui>. Na otevřené obrazovce je zobrazen seznam požadovaných informací. Klikněte na <gui style="button">Další</gui>.</p></item>
    <item><p>V seznamu vyberte zemi nebo region svého poskytovatele. Klikněte na <gui style="button">Následující</gui>.</p></item>
    <item><p>V seznamu vyberte svého poskytovatele. Klikněte na <gui style="button">Následující</gui>.</p></item>
    <item><p>Vyberte plán odpovídající typu zařízení, které připojujete. Tím se určí APN (Acces Point Name – název přístupového bodu). Klikněte na <gui style="button">Další</gui>.</p></item>
    <item><p>Kliknutím na tlačítko <gui style="button">Použít</gui> potvrďte nastavení, které jste vybrali. Průvodce se zavře a panel <gui>Síť</gui> zobrazí vlastnosti vašeho připojení.</p></item>
  </steps>

</page>
