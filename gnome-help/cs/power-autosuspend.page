<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="power-autosuspend" xml:lang="cs">

  <info>
     <link type="guide" xref="power#saving"/>
     <link type="seealso" xref="power-suspend"/>
     <link type="seealso" xref="shell-exit#suspend"/>

    <revision version="gnome:3.38.3" date="2021-03-07" status="candidate"/>
    <revision pkgversion="gnome:41" date="2021-09-08" status="candidate"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2016</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak počítač nastavit, aby se automaticky uspával do paměti.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adam Matoušek</mal:name>
      <mal:email>adamatousek@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Nastavení automatického uspávání</title>

  <p>Můžete počítač nastavit, aby se při nečinnosti automaticky uspával do paměti. Lze určit jiný čas pro běh z baterie a jiný pro běh z elektrické sítě.</p>

  <steps>

    <item>
      <p>Otevřete přehled <gui xref="shell-introduction#activities">Činnosti</gui> a začněte psát <gui>Napájení</gui>.</p>
    </item>
    <item>
      <p>Kliknutím na <gui>Napájení</gui> otevřete příslušný panel.</p>
    </item>
    <item>
      <p>V části <gui>Volby šetření energií</gui> klikněte na <gui>Automaticky uspat</gui>.</p>
    </item>
    <item>
      <p>Zvolte <gui>Při napájení z baterie</gui> nebo <gui>Při napájení ze sítě</gui>, přepněte vypínač do polohy zapnuto a vyberte čas <gui>Po uplynutí</gui>. Nastavit můžete obě situace.</p>

      <note style="tip">
        <p>U stolního počítače je jedna volba označená jako <gui>Při nečinnosti</gui>.</p>
      </note>
    </item>

  </steps>

</page>
